# Real data provided by the CBVE
fixed_address_type = AddressType.create!(name: "Fijo")
cbve_addr = Address.create!(name: 'Gral. Cabañas, Encarnacion, Paraguay', latitude: -27.33992128969605, longitude: -55.867838859558105, address_type: fixed_address_type)
cbve = FireDepartment.create!(name: 'Cuerpo de Bomberos Voluntarios de Encarnación', code: 'CBVE')
p1 = Place.create!(name: '1ra Compañia', address: cbve_addr)
k1 = Division.create!(place: p1, fire_department: cbve, code: 'k1')
vehicle_codes =
    %w(AE01 CE02 AE03 AE04 AE05 EE06 AE07 RE08 CE09 RE10 AE11 RE12 AE13 AE14 RE16 RE17 RE18 RP19 EE20 RA11 RA12 RA13 CG01 RE15)
vehicle_codes.each { |code| Vehicle.create!(code: code, division: k1) }

Admin.create!(username: 'admin', password: 'password', password_confirmation: 'password')
fire = nil
[{ name: "Incendio", icon: "fa-fire-extinguisher" },
{ name: "Podado", icon: "fa-tree" },
{ name: "Traslado", icon: "fa-ambulance" },
{ name: "Alimañas", icon: "fa-paw" },
{ name: "Cobertura de seguridad", icon: "fa-shield" },
{ name: "Otros", icon: "fa-bomb" }].each_with_index do |incident_type, index|
  it = IncidentType.create!(incident_type)
  fire = it if index == 0
end
['Dirección fija', 'Trabajo', 'Casa'].each do |address|
  AddressType.create!(name: address)
end  
general = FiremanHierarchy.create!(name: 'General')
FiremanHierarchy.create!(name: 'Comandante', superior: general)

['Activo', 'Inactivo', 'Con permiso', 'En reserva'].each do |classification|
  FiremanClassification.create!(name: classification)
end
