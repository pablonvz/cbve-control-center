# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141115114922) do

  create_table "address_types", force: true do |t|
    t.string   "name"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "addresses", force: true do |t|
    t.string   "name"
    t.float    "latitude"
    t.float    "longitude"
    t.text     "references"
    t.integer  "address_type_id"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "addresses", ["address_type_id"], name: "index_addresses_on_address_type_id", using: :btree

  create_table "complainants", force: true do |t|
    t.string   "name"
    t.string   "phone_number"
    t.string   "document"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "complaints", force: true do |t|
    t.integer  "address_id"
    t.integer  "complainant_id"
    t.text     "situation"
    t.datetime "deleted_at"
    t.integer  "incident_type_id"
    t.boolean  "is_fake",          default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "complaints", ["address_id"], name: "index_complaints_on_address_id", using: :btree
  add_index "complaints", ["complainant_id"], name: "index_complaints_on_complainant_id", using: :btree
  add_index "complaints", ["incident_type_id"], name: "index_complaints_on_incident_type_id", using: :btree

  create_table "dispatches", force: true do |t|
    t.integer  "complaint_id"
    t.integer  "incident_type_id"
    t.datetime "closed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dispatches", ["complaint_id"], name: "index_dispatches_on_complaint_id", using: :btree
  add_index "dispatches", ["incident_type_id"], name: "index_dispatches_on_incident_type_id", using: :btree

  create_table "dispatches_vehicles", force: true do |t|
    t.integer  "dispatch_id"
    t.integer  "vehicle_id"
    t.integer  "fireman_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dispatches_vehicles", ["dispatch_id"], name: "index_dispatches_vehicles_on_dispatch_id", using: :btree
  add_index "dispatches_vehicles", ["fireman_id"], name: "index_dispatches_vehicles_on_fireman_id", using: :btree
  add_index "dispatches_vehicles", ["vehicle_id"], name: "index_dispatches_vehicles_on_vehicle_id", using: :btree

  create_table "divisions", force: true do |t|
    t.string   "code"
    t.integer  "place_id"
    t.integer  "fire_department_id"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
  end

  add_index "divisions", ["fire_department_id"], name: "index_divisions_on_fire_department_id", using: :btree
  add_index "divisions", ["place_id"], name: "index_divisions_on_place_id", using: :btree

  create_table "fire_departments", force: true do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
  end

  create_table "fireman_classifications", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fireman_hierarchies", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "superior_id"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "fireman_hierarchies", ["superior_id"], name: "fireman_hierarchies_superior_id_fk", using: :btree

  create_table "firemen", force: true do |t|
    t.integer  "division_id"
    t.integer  "fireman_hierarchy_id"
    t.integer  "fireman_classification_id"
    t.integer  "person_id"
    t.string   "code"
    t.boolean  "available",                 default: true
    t.boolean  "dispatched",                default: false
    t.datetime "deleted_at"
    t.integer  "current_address_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "firemen", ["division_id"], name: "index_firemen_on_division_id", using: :btree
  add_index "firemen", ["fireman_classification_id"], name: "index_firemen_on_fireman_classification_id", using: :btree
  add_index "firemen", ["fireman_hierarchy_id"], name: "index_firemen_on_fireman_hierarchy_id", using: :btree
  add_index "firemen", ["person_id"], name: "index_firemen_on_person_id", using: :btree

  create_table "firemen_guard_groups", force: true do |t|
    t.integer  "fireman_id"
    t.integer  "guard_group_id"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "firemen_guard_groups", ["fireman_id"], name: "index_firemen_guard_groups_on_fireman_id", using: :btree
  add_index "firemen_guard_groups", ["guard_group_id"], name: "index_firemen_guard_groups_on_guard_group_id", using: :btree

  create_table "guard_groups", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "guards", force: true do |t|
    t.datetime "started_at"
    t.datetime "closed_at"
    t.integer  "fireman_id"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "guards", ["fireman_id"], name: "index_guards_on_fireman_id", using: :btree

  create_table "incident_types", force: true do |t|
    t.string   "name"
    t.string   "icon"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notes", force: true do |t|
    t.string   "type"
    t.text     "title"
    t.text     "description"
    t.integer  "dispatch_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "notes", ["dispatch_id"], name: "index_notes_on_dispatch_id", using: :btree

  create_table "people", force: true do |t|
    t.string   "name"
    t.string   "last_name"
    t.date     "birth_date"
    t.string   "phone_number"
    t.string   "document"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "gender"
  end

  create_table "person_addresses", force: true do |t|
    t.integer  "person_id"
    t.integer  "address_id"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "person_addresses", ["address_id"], name: "index_person_addresses_on_address_id", using: :btree
  add_index "person_addresses", ["person_id"], name: "index_person_addresses_on_person_id", using: :btree

  create_table "places", force: true do |t|
    t.string   "name"
    t.integer  "address_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "places", ["address_id"], name: "index_places_on_address_id", using: :btree

  create_table "transference_histories", force: true do |t|
    t.integer  "transference_id"
    t.integer  "transferable_id"
    t.string   "transferable_type"
    t.boolean  "originally_available"
    t.integer  "original_address_id"
    t.integer  "original_division_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "transference_histories", ["original_division_id"], name: "transference_histories_original_division_id_fk", using: :btree
  add_index "transference_histories", ["transferable_id", "transferable_type"], name: "th_transferable_polymorphic_index", using: :btree
  add_index "transference_histories", ["transference_id"], name: "index_transference_histories_on_transference_id", using: :btree

  create_table "transferences", force: true do |t|
    t.text     "reason"
    t.integer  "place_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "available",  default: true
  end

  add_index "transferences", ["place_id"], name: "index_transferences_on_place_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "deleted_at"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username"
    t.integer  "fireman_id"
  end

  add_index "users", ["fireman_id"], name: "index_users_on_fireman_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "vehicles", force: true do |t|
    t.string   "code"
    t.integer  "division_id"
    t.boolean  "available",          default: true
    t.boolean  "dispatched",         default: false
    t.integer  "current_address_id"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "vehicles", ["division_id"], name: "index_vehicles_on_division_id", using: :btree

  create_table "vehicles_firemen", force: true do |t|
    t.integer  "dispatches_vehicles_id"
    t.integer  "fireman_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "vehicles_firemen", ["dispatches_vehicles_id"], name: "index_vehicles_firemen_on_dispatches_vehicles_id", using: :btree
  add_index "vehicles_firemen", ["fireman_id"], name: "index_vehicles_firemen_on_fireman_id", using: :btree

  create_table "versions", force: true do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  add_foreign_key "addresses", "address_types", name: "addresses_address_type_id_fk"

  add_foreign_key "complaints", "addresses", name: "complaints_address_id_fk"
  add_foreign_key "complaints", "complainants", name: "complaints_complainant_id_fk"
  add_foreign_key "complaints", "incident_types", name: "complaints_incident_type_id_fk"

  add_foreign_key "divisions", "fire_departments", name: "divisions_fire_department_id_fk"

  add_foreign_key "fireman_hierarchies", "fireman_hierarchies", name: "fireman_hierarchies_superior_id_fk", column: "superior_id"

  add_foreign_key "firemen", "divisions", name: "firemen_division_id_fk"
  add_foreign_key "firemen", "fireman_classifications", name: "firemen_fireman_classification_id_fk"
  add_foreign_key "firemen", "fireman_hierarchies", name: "firemen_fireman_hierarchy_id_fk"
  add_foreign_key "firemen", "people", name: "firemen_person_id_fk"

  add_foreign_key "firemen_guard_groups", "firemen", name: "firemen_guard_groups_fireman_id_fk"
  add_foreign_key "firemen_guard_groups", "guard_groups", name: "firemen_guard_groups_guard_group_id_fk"

  add_foreign_key "guards", "firemen", name: "guards_fireman_id_fk"

  add_foreign_key "notes", "dispatches", name: "notes_dispatch_id_fk"

  add_foreign_key "person_addresses", "addresses", name: "person_addresses_address_id_fk"
  add_foreign_key "person_addresses", "people", name: "person_addresses_person_id_fk"

  add_foreign_key "transference_histories", "divisions", name: "transference_histories_original_division_id_fk", column: "original_division_id"

end
