# Real data provided by the CBVE
fixed_address_type = AddressType.create!(name: "Fijo")
cbve_addr = Address.create!(name: 'Gral. Cabañas, Encarnacion, Paraguay', latitude: -27.33992128969605, longitude: -55.867838859558105, address_type: fixed_address_type)
cbve = FireDepartment.create!(name: 'Cuerpo de Bomberos Voluntarios de Encarnación', code: 'CBVE')
p1 = Place.create!(name: '1ra Compañia', address: cbve_addr)
k1 = Division.create!(place: p1, fire_department: cbve, code: 'k1')
p2 = Place.create!(name: '2da Compañia', address: cbve_addr.clone)
k2 = Division.create!(place: p2, fire_department: cbve, code: 'k2')

vehicle_codes =
    %w(AE01 CE02 AE03 AE04 AE05 EE06 AE07 RE08 CE09 RE10 AE11 RE12 AE13 AE14 RE16 RE17 RE18 RP19 EE20 RA11 RA12 RA13 CG01)

vehicle_codes.each { |code| Vehicle.create!(code: code, division: rand > 0.5 ? k1 : k2) }

# Fake data
Admin.create!(username: 'admin', password: 'password', password_confirmation: 'password')
fire = nil
[{ name: "Incendio", icon: "fa-fire-extinguisher" },
{ name: "Podado", icon: "fa-tree" },
{ name: "Traslado", icon: "fa-ambulance" },
{ name: "Alimañas", icon: "fa-paw" },
{ name: "Cobertura de seguridad", icon: "fa-shield" },
{ name: "Otros", icon: "fa-bomb" }].each_with_index do |incident_type, index|
  it = IncidentType.create!(incident_type)
  fire = it if index == 0
end
c = Complaint.create!({
  "complainant_attributes"=>{
    "name"=>"José Lopara",
    "phone_number"=>"+595985123456"
  },
  "situation"=>"La casa de pepito se está quemando y chocho está adentro",
  incident_type: fire
});
c.address = cbve_addr
c.save!

['Dirección fija', 'Trabajo', 'Casa'].each do |address|
  AddressType.create!(name: address)
end  


general = FiremanHierarchy.create!(name: 'General')
com = FiremanHierarchy.create!(name: 'Comandante', superior: general)


['Activo', 'Inactivo', 'Con permiso', 'En reserva'].each do |classification|
  FiremanClassification.create!(name: classification)
end

active_fireman_classification = FiremanClassification.first

[
  { 
    name: "José", 
    last_name: "Ruiz Diaz",
    birth_date: Date.parse("1975-06-17"), 
    phone_number: "+595985402395", 
    document: "CI Nro 2356942", 
    addresses: [cbve_addr.clone],
    gender: "male"
  },
  { 
    name: "Pepe", 
    last_name: "Lopez", 
    birth_date: Date.parse("1966-03-06"), 
    phone_number: "071206593", 
    document: "CI Nro 3452681", 
    addresses: [cbve_addr.clone],
    gender: "male"
  },
  { 
    name: "Ramón", 
    last_name: "Benitez", 
    birth_date: Date.parse("1978-11-29"), 
    phone_number: "+59575423695", 
    document: "CI Nro 2365821", 
    addresses: [cbve_addr.clone],
    gender: "male"
  },
  { 
    name: "Jorge", 
    last_name: "Cáceres", 
    birth_date: Date.parse("1965-01-19"), 
    phone_number: "+595995423651", 
    document: "CI Nro 2351348", 
    addresses: [cbve_addr.clone],
    gender: "male"
  }
].each do |person_hash|
  p = Person.create!(person_hash)
  Fireman.create!( division: k1, 
    fireman_classification: active_fireman_classification, 
    fireman_hierarchy: (p.id == 3 ? general : com), 
    person: p, 
    code: "B10#{p.id}" )
end


