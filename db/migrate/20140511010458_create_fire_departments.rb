class CreateFireDepartments < ActiveRecord::Migration
  def change
    create_table :fire_departments do |t|
      t.string :name
      t.string :code
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
