class CreateComplaints < ActiveRecord::Migration
  def change
    create_table :complaints do |t|
      t.references :address, index: true
      t.references :complainant, index: true
      t.text :situation
      t.datetime :deleted_at, index: true
      t.references :incident_type, index: true
      t.boolean :is_fake, default: false
      t.timestamps
    end
  end
end
