class CreateDispatches < ActiveRecord::Migration
  def change
    create_table :dispatches do |t|
      t.references :complaint, index: true
      t.references :incident_type, index: true
      t.datetime :closed_at, index: true
      t.timestamps
    end
  end
end
