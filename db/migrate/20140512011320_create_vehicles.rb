class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.string :code
      t.references :division, index: true

      t.boolean :available, default: true # used by transference
      t.boolean :dispatched, default: false # used by dispatch
      t.integer :current_address_id, index: true
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
