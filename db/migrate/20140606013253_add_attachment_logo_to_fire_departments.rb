class AddAttachmentLogoToFireDepartments < ActiveRecord::Migration
  def self.up
    change_table :fire_departments do |t|
      t.attachment :logo
    end
  end

  def self.down
    drop_attached_file :fire_departments, :logo
  end
end
