class CreateFiremanHierarchies < ActiveRecord::Migration
  def change
    create_table :fireman_hierarchies do |t|
      t.string :name
      t.text :description
      t.integer :superior_id

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
