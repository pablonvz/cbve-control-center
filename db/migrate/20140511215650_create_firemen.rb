class CreateFiremen < ActiveRecord::Migration
  def change
    create_table :firemen do |t|
      t.references :division, index: true
      t.references :fireman_hierarchy, index: true
      t.references :fireman_classification, index: true
      t.references :person, index: true
      t.string :code
      t.boolean :available, default: true # used by transference
      t.boolean :dispatched, default: false # used by dispatch      t.datetime :deleted_at
      t.datetime :deleted_at
      t.integer :current_address_id, index: true
      t.timestamps
    end
  end
end
