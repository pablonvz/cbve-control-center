class CreateVehiclesFiremen < ActiveRecord::Migration
  def change
    create_table :vehicles_firemen do |t|
      t.references :dispatches_vehicles, index: true
      t.references :fireman, index: true

      t.timestamps
    end
  end
end
