class CreateFiremenGuardGroups < ActiveRecord::Migration
  def change
    create_table :firemen_guard_groups do |t|
      t.references :fireman, index: true
      t.references :guard_group, index: true
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
