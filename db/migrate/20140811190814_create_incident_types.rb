class CreateIncidentTypes < ActiveRecord::Migration
  def change
    create_table :incident_types do |t|
      t.string :name
      t.string :icon
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
