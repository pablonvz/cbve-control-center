class AddAttachmentLogoToDivisions < ActiveRecord::Migration
  def self.up
    change_table :divisions do |t|
      t.attachment :logo
    end
  end

  def self.down
    drop_attached_file :divisions, :logo
  end
end
