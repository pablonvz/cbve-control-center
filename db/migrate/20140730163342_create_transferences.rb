class CreateTransferences < ActiveRecord::Migration
  def change
    create_table :transferences do |t|
      t.text :reason
      t.references :place, index: true
      t.timestamps
    end
  end
end
