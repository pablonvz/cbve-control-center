class CreatePersonAddresses < ActiveRecord::Migration
  def change
    create_table :person_addresses do |t|
      t.references :person, index: true
      t.references :address, index: true

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
