class AddUsernameAndFiremanToUsers < ActiveRecord::Migration
  def change
    add_column :users, :username, :string
    add_index :users, :username, unique: true
    add_column :users, :fireman_id,  :integer
    add_index :users, :fireman_id#, unique: true
  end
end
