class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :name
      t.float :latitude
      t.float :longitude
      t.text :references
      t.references :address_type, index: true

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
