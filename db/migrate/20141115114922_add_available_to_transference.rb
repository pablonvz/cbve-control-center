class AddAvailableToTransference < ActiveRecord::Migration
  def change
    add_column :transferences, :available, :boolean, default: true
  end
end
