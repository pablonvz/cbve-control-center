class CreateDispatchesVehicles < ActiveRecord::Migration
  def change
    create_table :dispatches_vehicles do |t|
      t.references :dispatch, index: true
      t.references :vehicle, index: true
      t.references :fireman, index: true

      t.timestamps
    end
  end
end
