class CreateGuardGroups < ActiveRecord::Migration
  def change
    create_table :guard_groups do |t|
      t.string :name
      t.text :description
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
