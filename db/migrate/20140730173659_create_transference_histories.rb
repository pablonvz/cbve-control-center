class CreateTransferenceHistories < ActiveRecord::Migration
  def change
    create_table :transference_histories do |t|
      t.references :transference, index: true
      t.references :transferable, polymorphic: true, index: false#true
      t.boolean :originally_available

      t.integer :original_address_id, index: true
      t.integer :original_division_id, index: true

      t.timestamps
    end
    add_index :transference_histories, ['transferable_id', 'transferable_type'], name: 'th_transferable_polymorphic_index'
  end
end
