class CreateDivisions < ActiveRecord::Migration
  def change
    create_table :divisions do |t|
      
      # t.string :name
      t.string :code
      # t.references :address, index: true
      t.references :place, index: true

      t.references :fire_department, index: true

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
