class CreateGuards < ActiveRecord::Migration
  def change
    create_table :guards do |t|
      t.datetime :started_at, index: true
      t.datetime :closed_at, index: true
      t.references :fireman, index: true
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
