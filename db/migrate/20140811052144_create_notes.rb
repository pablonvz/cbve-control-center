class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.string :type
      t.text :title
      t.text :description
      t.references :dispatch, index: true

      t.timestamps
    end
  end
end
