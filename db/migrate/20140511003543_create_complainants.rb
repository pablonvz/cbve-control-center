class CreateComplainants < ActiveRecord::Migration
  def change
    create_table :complainants do |t|
      t.string :name
      t.string :phone_number
      t.string :document
      t.datetime :deleted_at, index: true
      t.timestamps
    end
  end
end
