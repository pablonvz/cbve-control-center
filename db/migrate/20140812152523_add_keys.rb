class AddKeys < ActiveRecord::Migration
  def change
    add_foreign_key "addresses", "address_types", name: "addresses_address_type_id_fk"
    add_foreign_key "complaints", "addresses", name: "complaints_address_id_fk"
    add_foreign_key "complaints", "complainants", name: "complaints_complainant_id_fk"
    add_foreign_key "complaints", "incident_types", name: "complaints_incident_type_id_fk"
    add_foreign_key "divisions", "fire_departments", name: "divisions_fire_department_id_fk"
    add_foreign_key "fireman_hierarchies", "fireman_hierarchies", name: "fireman_hierarchies_superior_id_fk", column: "superior_id"
    add_foreign_key "firemen", "divisions", name: "firemen_division_id_fk"
    add_foreign_key "firemen", "fireman_classifications", name: "firemen_fireman_classification_id_fk"
    add_foreign_key "firemen", "fireman_hierarchies", name: "firemen_fireman_hierarchy_id_fk"
    add_foreign_key "firemen_guard_groups", "firemen", name: "firemen_guard_groups_fireman_id_fk"
    add_foreign_key "firemen_guard_groups", "guard_groups", name: "firemen_guard_groups_guard_group_id_fk"
    add_foreign_key "firemen", "people", name: "firemen_person_id_fk"
    add_foreign_key "guards", "firemen", name: "guards_fireman_id_fk"
    add_foreign_key "notes", "dispatches", name: "notes_dispatch_id_fk"
    add_foreign_key "person_addresses", "addresses", name: "person_addresses_address_id_fk"
    add_foreign_key "person_addresses", "people", name: "person_addresses_person_id_fk"
    add_foreign_key "transference_histories", "divisions", name: "transference_histories_original_division_id_fk", column: "original_division_id"
  end
end
