class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :name
      t.string :last_name
      t.date :birth_date
      t.string :phone_number
      t.string :document
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
