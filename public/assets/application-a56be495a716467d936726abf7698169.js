/*!
 * jQuery JavaScript Library v1.11.0
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright 2005, 2014 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2014-01-23T21:02Z
 */


(function( global, factory ) {

	if ( typeof module === "object" && typeof module.exports === "object" ) {
		// For CommonJS and CommonJS-like environments where a proper window is present,
		// execute the factory and get jQuery
		// For environments that do not inherently posses a window with a document
		// (such as Node.js), expose a jQuery-making factory as module.exports
		// This accentuates the need for the creation of a real window
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
}(typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Can't do this because several apps including ASP.NET trace
// the stack via arguments.caller.callee and Firefox dies if
// you try to trace through "use strict" call chains. (#13335)
// Support: Firefox 18+
//

var deletedIds = [];

var slice = deletedIds.slice;

var concat = deletedIds.concat;

var push = deletedIds.push;

var indexOf = deletedIds.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var trim = "".trim;

var support = {};



var
	version = "1.11.0",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {
		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	},

	// Make sure we trim BOM and NBSP (here's looking at you, Safari 5.0 and IE)
	rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,

	// Matches dashed string for camelizing
	rmsPrefix = /^-ms-/,
	rdashAlpha = /-([\da-z])/gi,

	// Used by jQuery.camelCase as callback to replace()
	fcamelCase = function( all, letter ) {
		return letter.toUpperCase();
	};

jQuery.fn = jQuery.prototype = {
	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// Start with an empty selector
	selector: "",

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {
		return num != null ?

			// Return a 'clean' array
			( num < 0 ? this[ num + this.length ] : this[ num ] ) :

			// Return just the object
			slice.call( this );
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;
		ret.context = this.context;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	// (You can seed the arguments with an array of args, but this is
	// only used internally.)
	each: function( callback, args ) {
		return jQuery.each( this, callback, args );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map(this, function( elem, i ) {
			return callback.call( elem, i, elem );
		}));
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[j] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor(null);
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: deletedIds.sort,
	splice: deletedIds.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var src, copyIsArray, copy, name, options, clone,
		target = arguments[0] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !jQuery.isFunction(target) ) {
		target = {};
	}

	// extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {
		// Only deal with non-null/undefined values
		if ( (options = arguments[ i ]) != null ) {
			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)) ) ) {
					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && jQuery.isArray(src) ? src : [];

					} else {
						clone = src && jQuery.isPlainObject(src) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend({
	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	// See test/unit/core.js for details concerning isFunction.
	// Since version 1.3, DOM methods and functions like alert
	// aren't supported. They return false on IE (#2968).
	isFunction: function( obj ) {
		return jQuery.type(obj) === "function";
	},

	isArray: Array.isArray || function( obj ) {
		return jQuery.type(obj) === "array";
	},

	isWindow: function( obj ) {
		/* jshint eqeqeq: false */
		return obj != null && obj == obj.window;
	},

	isNumeric: function( obj ) {
		// parseFloat NaNs numeric-cast false positives (null|true|false|"")
		// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
		// subtraction forces infinities to NaN
		return obj - parseFloat( obj ) >= 0;
	},

	isEmptyObject: function( obj ) {
		var name;
		for ( name in obj ) {
			return false;
		}
		return true;
	},

	isPlainObject: function( obj ) {
		var key;

		// Must be an Object.
		// Because of IE, we also have to check the presence of the constructor property.
		// Make sure that DOM nodes and window objects don't pass through, as well
		if ( !obj || jQuery.type(obj) !== "object" || obj.nodeType || jQuery.isWindow( obj ) ) {
			return false;
		}

		try {
			// Not own constructor property must be Object
			if ( obj.constructor &&
				!hasOwn.call(obj, "constructor") &&
				!hasOwn.call(obj.constructor.prototype, "isPrototypeOf") ) {
				return false;
			}
		} catch ( e ) {
			// IE8,9 Will throw exceptions on certain host objects #9897
			return false;
		}

		// Support: IE<9
		// Handle iteration over inherited properties before own properties.
		if ( support.ownLast ) {
			for ( key in obj ) {
				return hasOwn.call( obj, key );
			}
		}

		// Own properties are enumerated firstly, so to speed up,
		// if last one is own, then all properties are own.
		for ( key in obj ) {}

		return key === undefined || hasOwn.call( obj, key );
	},

	type: function( obj ) {
		if ( obj == null ) {
			return obj + "";
		}
		return typeof obj === "object" || typeof obj === "function" ?
			class2type[ toString.call(obj) ] || "object" :
			typeof obj;
	},

	// Evaluates a script in a global context
	// Workarounds based on findings by Jim Driscoll
	// http://weblogs.java.net/blog/driscoll/archive/2009/09/08/eval-javascript-global-context
	globalEval: function( data ) {
		if ( data && jQuery.trim( data ) ) {
			// We use execScript on Internet Explorer
			// We use an anonymous function so that context is window
			// rather than jQuery in Firefox
			( window.execScript || function( data ) {
				window[ "eval" ].call( window, data );
			} )( data );
		}
	},

	// Convert dashed to camelCase; used by the css and data modules
	// Microsoft forgot to hump their vendor prefix (#9572)
	camelCase: function( string ) {
		return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
	},

	nodeName: function( elem, name ) {
		return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
	},

	// args is for internal usage only
	each: function( obj, callback, args ) {
		var value,
			i = 0,
			length = obj.length,
			isArray = isArraylike( obj );

		if ( args ) {
			if ( isArray ) {
				for ( ; i < length; i++ ) {
					value = callback.apply( obj[ i ], args );

					if ( value === false ) {
						break;
					}
				}
			} else {
				for ( i in obj ) {
					value = callback.apply( obj[ i ], args );

					if ( value === false ) {
						break;
					}
				}
			}

		// A special, fast, case for the most common use of each
		} else {
			if ( isArray ) {
				for ( ; i < length; i++ ) {
					value = callback.call( obj[ i ], i, obj[ i ] );

					if ( value === false ) {
						break;
					}
				}
			} else {
				for ( i in obj ) {
					value = callback.call( obj[ i ], i, obj[ i ] );

					if ( value === false ) {
						break;
					}
				}
			}
		}

		return obj;
	},

	// Use native String.trim function wherever possible
	trim: trim && !trim.call("\uFEFF\xA0") ?
		function( text ) {
			return text == null ?
				"" :
				trim.call( text );
		} :

		// Otherwise use our own trimming functionality
		function( text ) {
			return text == null ?
				"" :
				( text + "" ).replace( rtrim, "" );
		},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArraylike( Object(arr) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		var len;

		if ( arr ) {
			if ( indexOf ) {
				return indexOf.call( arr, elem, i );
			}

			len = arr.length;
			i = i ? i < 0 ? Math.max( 0, len + i ) : i : 0;

			for ( ; i < len; i++ ) {
				// Skip accessing in sparse arrays
				if ( i in arr && arr[ i ] === elem ) {
					return i;
				}
			}
		}

		return -1;
	},

	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		while ( j < len ) {
			first[ i++ ] = second[ j++ ];
		}

		// Support: IE<9
		// Workaround casting of .length to NaN on otherwise arraylike objects (e.g., NodeLists)
		if ( len !== len ) {
			while ( second[j] !== undefined ) {
				first[ i++ ] = second[ j++ ];
			}
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var value,
			i = 0,
			length = elems.length,
			isArray = isArraylike( elems ),
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArray ) {
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// Bind a function to a context, optionally partially applying any
	// arguments.
	proxy: function( fn, context ) {
		var args, proxy, tmp;

		if ( typeof context === "string" ) {
			tmp = fn[ context ];
			context = fn;
			fn = tmp;
		}

		// Quick check to determine if target is callable, in the spec
		// this throws a TypeError, but we will just return undefined.
		if ( !jQuery.isFunction( fn ) ) {
			return undefined;
		}

		// Simulated bind
		args = slice.call( arguments, 2 );
		proxy = function() {
			return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
		};

		// Set the guid of unique handler to the same of original handler, so it can be removed
		proxy.guid = fn.guid = fn.guid || jQuery.guid++;

		return proxy;
	},

	now: function() {
		return +( new Date() );
	},

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
});

// Populate the class2type map
jQuery.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(i, name) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
});

function isArraylike( obj ) {
	var length = obj.length,
		type = jQuery.type( obj );

	if ( type === "function" || jQuery.isWindow( obj ) ) {
		return false;
	}

	if ( obj.nodeType === 1 && length ) {
		return true;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v1.10.16
 * http://sizzlejs.com/
 *
 * Copyright 2013 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2014-01-13
 */
(function( window ) {

var i,
	support,
	Expr,
	getText,
	isXML,
	compile,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + -(new Date()),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// General-purpose constants
	strundefined = typeof undefined,
	MAX_NEGATIVE = 1 << 31,

	// Instance methods
	hasOwn = ({}).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	push_native = arr.push,
	push = arr.push,
	slice = arr.slice,
	// Use a stripped-down indexOf if we can't use a native one
	indexOf = arr.indexOf || function( elem ) {
		var i = 0,
			len = this.length;
		for ( ; i < len; i++ ) {
			if ( this[i] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// Whitespace characters http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",
	// http://www.w3.org/TR/css3-syntax/#characters
	characterEncoding = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",

	// Loosely modeled on CSS identifier characters
	// An unquoted value should be a CSS identifier http://www.w3.org/TR/css3-selectors/#attribute-selectors
	// Proper syntax: http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
	identifier = characterEncoding.replace( "w", "w#" ),

	// Acceptable operators http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + characterEncoding + ")" + whitespace +
		"*(?:([*^$|!~]?=)" + whitespace + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + identifier + ")|)|)" + whitespace + "*\\]",

	// Prefer arguments quoted,
	//   then not containing pseudos/brackets,
	//   then attribute selectors/non-parenthetical expressions,
	//   then anything else
	// These preferences are here to reduce the number of selectors
	//   needing tokenize in the PSEUDO preFilter
	pseudos = ":(" + characterEncoding + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + attributes.replace( 3, 8 ) + ")*)|.*)\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

	rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + characterEncoding + ")" ),
		"CLASS": new RegExp( "^\\.(" + characterEncoding + ")" ),
		"TAG": new RegExp( "^(" + characterEncoding.replace( "w", "w*" ) + ")" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
			"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
			"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
			whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,
	rescape = /'|\\/g,

	// CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
	funescape = function( _, escaped, escapedWhitespace ) {
		var high = "0x" + escaped - 0x10000;
		// NaN means non-codepoint
		// Support: Firefox
		// Workaround erroneous numeric interpretation of +"0x"
		return high !== high || escapedWhitespace ?
			escaped :
			high < 0 ?
				// BMP codepoint
				String.fromCharCode( high + 0x10000 ) :
				// Supplemental Plane codepoint (surrogate pair)
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	};

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		(arr = slice.call( preferredDoc.childNodes )),
		preferredDoc.childNodes
	);
	// Support: Android<4.0
	// Detect silently failing push.apply
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			push_native.apply( target, slice.call(els) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;
			// Can't trust NodeList.length
			while ( (target[j++] = els[i++]) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var match, elem, m, nodeType,
		// QSA vars
		i, groups, old, nid, newContext, newSelector;

	if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
		setDocument( context );
	}

	context = context || document;
	results = results || [];

	if ( !selector || typeof selector !== "string" ) {
		return results;
	}

	if ( (nodeType = context.nodeType) !== 1 && nodeType !== 9 ) {
		return [];
	}

	if ( documentIsHTML && !seed ) {

		// Shortcuts
		if ( (match = rquickExpr.exec( selector )) ) {
			// Speed-up: Sizzle("#ID")
			if ( (m = match[1]) ) {
				if ( nodeType === 9 ) {
					elem = context.getElementById( m );
					// Check parentNode to catch when Blackberry 4.6 returns
					// nodes that are no longer in the document (jQuery #6963)
					if ( elem && elem.parentNode ) {
						// Handle the case where IE, Opera, and Webkit return items
						// by name instead of ID
						if ( elem.id === m ) {
							results.push( elem );
							return results;
						}
					} else {
						return results;
					}
				} else {
					// Context is not a document
					if ( context.ownerDocument && (elem = context.ownerDocument.getElementById( m )) &&
						contains( context, elem ) && elem.id === m ) {
						results.push( elem );
						return results;
					}
				}

			// Speed-up: Sizzle("TAG")
			} else if ( match[2] ) {
				push.apply( results, context.getElementsByTagName( selector ) );
				return results;

			// Speed-up: Sizzle(".CLASS")
			} else if ( (m = match[3]) && support.getElementsByClassName && context.getElementsByClassName ) {
				push.apply( results, context.getElementsByClassName( m ) );
				return results;
			}
		}

		// QSA path
		if ( support.qsa && (!rbuggyQSA || !rbuggyQSA.test( selector )) ) {
			nid = old = expando;
			newContext = context;
			newSelector = nodeType === 9 && selector;

			// qSA works strangely on Element-rooted queries
			// We can work around this by specifying an extra ID on the root
			// and working up from there (Thanks to Andrew Dupont for the technique)
			// IE 8 doesn't work on object elements
			if ( nodeType === 1 && context.nodeName.toLowerCase() !== "object" ) {
				groups = tokenize( selector );

				if ( (old = context.getAttribute("id")) ) {
					nid = old.replace( rescape, "\\$&" );
				} else {
					context.setAttribute( "id", nid );
				}
				nid = "[id='" + nid + "'] ";

				i = groups.length;
				while ( i-- ) {
					groups[i] = nid + toSelector( groups[i] );
				}
				newContext = rsibling.test( selector ) && testContext( context.parentNode ) || context;
				newSelector = groups.join(",");
			}

			if ( newSelector ) {
				try {
					push.apply( results,
						newContext.querySelectorAll( newSelector )
					);
					return results;
				} catch(qsaError) {
				} finally {
					if ( !old ) {
						context.removeAttribute("id");
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {Function(string, Object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {
		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {
			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return (cache[ key + " " ] = value);
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created div and expects a boolean result
 */
function assert( fn ) {
	var div = document.createElement("div");

	try {
		return !!fn( div );
	} catch (e) {
		return false;
	} finally {
		// Remove from its parent by default
		if ( div.parentNode ) {
			div.parentNode.removeChild( div );
		}
		// release memory in IE
		div = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split("|"),
		i = attrs.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[i] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			( ~b.sourceIndex || MAX_NEGATIVE ) -
			( ~a.sourceIndex || MAX_NEGATIVE );

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( (cur = cur.nextSibling) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return (name === "input" || name === "button") && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction(function( argument ) {
		argument = +argument;
		return markFunction(function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ (j = matchIndexes[i]) ] ) {
					seed[j] = !(matches[j] = seed[j]);
				}
			}
		});
	});
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== strundefined && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	// documentElement is verified for cases where it doesn't yet exist
	// (such as loading iframes in IE - #4833)
	var documentElement = elem && (elem.ownerDocument || elem).documentElement;
	return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare,
		doc = node ? node.ownerDocument || node : preferredDoc,
		parent = doc.defaultView;

	// If no document and documentElement is available, return
	if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Set our document
	document = doc;
	docElem = doc.documentElement;

	// Support tests
	documentIsHTML = !isXML( doc );

	// Support: IE>8
	// If iframe document is assigned to "document" variable and if iframe has been reloaded,
	// IE will throw "permission denied" error when accessing "document" variable, see jQuery #13936
	// IE6-8 do not support the defaultView property so parent will be undefined
	if ( parent && parent !== parent.top ) {
		// IE11 does not have attachEvent, so all must suffer
		if ( parent.addEventListener ) {
			parent.addEventListener( "unload", function() {
				setDocument();
			}, false );
		} else if ( parent.attachEvent ) {
			parent.attachEvent( "onunload", function() {
				setDocument();
			});
		}
	}

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties (excepting IE8 booleans)
	support.attributes = assert(function( div ) {
		div.className = "i";
		return !div.getAttribute("className");
	});

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert(function( div ) {
		div.appendChild( doc.createComment("") );
		return !div.getElementsByTagName("*").length;
	});

	// Check if getElementsByClassName can be trusted
	support.getElementsByClassName = rnative.test( doc.getElementsByClassName ) && assert(function( div ) {
		div.innerHTML = "<div class='a'></div><div class='a i'></div>";

		// Support: Safari<4
		// Catch class over-caching
		div.firstChild.className = "i";
		// Support: Opera<10
		// Catch gEBCN failure to find non-leading classes
		return div.getElementsByClassName("i").length === 2;
	});

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert(function( div ) {
		docElem.appendChild( div ).id = expando;
		return !doc.getElementsByName || !doc.getElementsByName( expando ).length;
	});

	// ID find and filter
	if ( support.getById ) {
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== strundefined && documentIsHTML ) {
				var m = context.getElementById( id );
				// Check parentNode to catch when Blackberry 4.6 returns
				// nodes that are no longer in the document #6963
				return m && m.parentNode ? [m] : [];
			}
		};
		Expr.filter["ID"] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute("id") === attrId;
			};
		};
	} else {
		// Support: IE6/7
		// getElementById is not reliable as a find shortcut
		delete Expr.find["ID"];

		Expr.filter["ID"] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== strundefined && elem.getAttributeNode("id");
				return node && node.value === attrId;
			};
		};
	}

	// Tag
	Expr.find["TAG"] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== strundefined ) {
				return context.getElementsByTagName( tag );
			}
		} :
		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( (elem = results[i++]) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
		if ( typeof context.getElementsByClassName !== strundefined && documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See http://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( (support.qsa = rnative.test( doc.querySelectorAll )) ) {
		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert(function( div ) {
			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// http://bugs.jquery.com/ticket/12359
			div.innerHTML = "<select t=''><option selected=''></option></select>";

			// Support: IE8, Opera 10-12
			// Nothing should be selected when empty strings follow ^= or $= or *=
			if ( div.querySelectorAll("[t^='']").length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !div.querySelectorAll("[selected]").length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":checked").length ) {
				rbuggyQSA.push(":checked");
			}
		});

		assert(function( div ) {
			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = doc.createElement("input");
			input.setAttribute( "type", "hidden" );
			div.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( div.querySelectorAll("[name=d]").length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":enabled").length ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Opera 10-11 does not throw on post-comma invalid pseudos
			div.querySelectorAll("*,:x");
			rbuggyQSA.push(",.*:");
		});
	}

	if ( (support.matchesSelector = rnative.test( (matches = docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector) )) ) {

		assert(function( div ) {
			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( div, "div" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( div, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		});
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully does not implement inclusive descendent
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			));
		} :
		function( a, b ) {
			if ( b ) {
				while ( (b = b.parentNode) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

			// Choose the first element that is related to our preferred document
			if ( a === doc || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
				return -1;
			}
			if ( b === doc || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf.call( sortInput, a ) - indexOf.call( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {
		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {
			return a === doc ? -1 :
				b === doc ? 1 :
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf.call( sortInput, a ) - indexOf.call( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( (cur = cur.parentNode) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( (cur = cur.parentNode) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[i] === bp[i] ) {
			i++;
		}

		return i ?
			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[i], bp[i] ) :

			// Otherwise nodes in our document sort first
			ap[i] === preferredDoc ? -1 :
			bp[i] === preferredDoc ? 1 :
			0;
	};

	return doc;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	// Make sure that attribute selectors are quoted
	expr = expr.replace( rattributeQuotes, "='$1']" );

	if ( support.matchesSelector && documentIsHTML &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||
					// As well, disconnected nodes are said to be in a document
					// fragment in IE 9
					elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch(e) {}
	}

	return Sizzle( expr, document, null, [elem] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
	// Set document vars if needed
	if ( ( context.ownerDocument || context ) !== document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],
		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			(val = elem.getAttributeNode(name)) && val.specified ?
				val.value :
				null;
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( (elem = results[i++]) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {
		// If no nodeType, this is expected to be an array
		while ( (node = elem[i++]) ) {
			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {
			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}
	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[1] = match[1].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[3] = ( match[4] || match[5] || "" ).replace( runescape, funescape );

			if ( match[2] === "~=" ) {
				match[3] = " " + match[3] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {
			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[1] = match[1].toLowerCase();

			if ( match[1].slice( 0, 3 ) === "nth" ) {
				// nth-* requires argument
				if ( !match[3] ) {
					Sizzle.error( match[0] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
				match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

			// other types prohibit arguments
			} else if ( match[3] ) {
				Sizzle.error( match[0] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[5] && match[2];

			if ( matchExpr["CHILD"].test( match[0] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[3] && match[4] !== undefined ) {
				match[2] = match[4];

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&
				// Get excess from tokenize (recursively)
				(excess = tokenize( unquoted, true )) &&
				// advance to the next closing parenthesis
				(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

				// excess is a negative index
				match[0] = match[0].slice( 0, excess );
				match[2] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() { return true; } :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
				classCache( className, function( elem ) {
					return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== strundefined && elem.getAttribute("class") || "" );
				});
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
			};
		},

		"CHILD": function( type, what, argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, context, xml ) {
					var cache, outerCache, node, diff, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( (node = node[ dir ]) ) {
									if ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) {
										return false;
									}
								}
								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {
							// Seek `elem` from a previously-cached index
							outerCache = parent[ expando ] || (parent[ expando ] = {});
							cache = outerCache[ type ] || [];
							nodeIndex = cache[0] === dirruns && cache[1];
							diff = cache[0] === dirruns && cache[2];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( (node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								(diff = nodeIndex = 0) || start.pop()) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									outerCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						// Use previously-cached element index if available
						} else if ( useCache && (cache = (elem[ expando ] || (elem[ expando ] = {}))[ type ]) && cache[0] === dirruns ) {
							diff = cache[1];

						// xml :nth-child(...) or :nth-last-child(...) or :nth(-last)?-of-type(...)
						} else {
							// Use the same loop as above to seek `elem` from the start
							while ( (node = ++nodeIndex && node && node[ dir ] ||
								(diff = nodeIndex = 0) || start.pop()) ) {

								if ( ( ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1 ) && ++diff ) {
									// Cache the index of each encountered element
									if ( useCache ) {
										(node[ expando ] || (node[ expando ] = {}))[ type ] = [ dirruns, diff ];
									}

									if ( node === elem ) {
										break;
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {
			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction(function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf.call( seed, matched[i] );
							seed[ idx ] = !( matches[ idx ] = matched[i] );
						}
					}) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {
		// Potentially complex pseudos
		"not": markFunction(function( selector ) {
			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction(function( seed, matches, context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( (elem = unmatched[i]) ) {
							seed[i] = !(matches[i] = elem);
						}
					}
				}) :
				function( elem, context, xml ) {
					input[0] = elem;
					matcher( input, null, xml, results );
					return !results.pop();
				};
		}),

		"has": markFunction(function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		}),

		"contains": markFunction(function( text ) {
			return function( elem ) {
				return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
			};
		}),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {
			// lang value must be a valid identifier
			if ( !ridentifier.test(lang || "") ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( (elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
				return false;
			};
		}),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
		},

		// Boolean properties
		"enabled": function( elem ) {
			return elem.disabled === false;
		},

		"disabled": function( elem ) {
			return elem.disabled === true;
		},

		"checked": function( elem ) {
			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
		},

		"selected": function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {
			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos["empty"]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo(function() {
			return [ 0 ];
		}),

		"last": createPositionalPseudo(function( matchIndexes, length ) {
			return [ length - 1 ];
		}),

		"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		}),

		"even": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"odd": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		})
	}
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

function tokenize( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || (match = rcomma.exec( soFar )) ) {
			if ( match ) {
				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[0].length ) || soFar;
			}
			groups.push( (tokens = []) );
		}

		matched = false;

		// Combinators
		if ( (match = rcombinators.exec( soFar )) ) {
			matched = match.shift();
			tokens.push({
				value: matched,
				// Cast descendant combinators to space
				type: match[0].replace( rtrim, " " )
			});
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
				(match = preFilters[ type ]( match ))) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					type: type,
					matches: match
				});
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :
			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
}

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[i].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		checkNonElements = base && dir === "parentNode",
		doneName = done++;

	return combinator.first ?
		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( (elem = elem[ dir ]) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from dir caching
			if ( xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || (elem[ expando ] = {});
						if ( (oldCache = outerCache[ dir ]) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return (newCache[ 2 ] = oldCache[ 2 ]);
						} else {
							// Reuse newcache so results back-propagate to previous elements
							outerCache[ dir ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
								return true;
							}
						}
					}
				}
			}
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[i]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[0];
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( (elem = unmatched[i]) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction(function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?
				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( (elem = temp[i]) ) {
					matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {
					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) ) {
							// Restore matcherIn since elem is not yet a final match
							temp.push( (matcherIn[i] = elem) );
						}
					}
					postFinder( null, (matcherOut = []), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( (elem = matcherOut[i]) &&
						(temp = postFinder ? indexOf.call( seed, elem ) : preMap[i]) > -1 ) {

						seed[temp] = !(results[temp] = elem);
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	});
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[0].type ],
		implicitRelative = leadingRelative || Expr.relative[" "],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf.call( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			return ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				(checkContext = context).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );
		} ];

	for ( ; i < len; i++ ) {
		if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
			matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
		} else {
			matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {
				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[j].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(
						// If the preceding token was a descendant combinator, insert an implicit any-element `*`
						tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,
				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
				len = elems.length;

			if ( outermost ) {
				outermostContext = context !== document && context;
			}

			// Add elements passing elementMatchers directly to results
			// Keep `i` a string if there are no elements so `matchedCount` will be "00" below
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;
					while ( (matcher = elementMatchers[j++]) ) {
						if ( matcher( elem, context, xml ) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {
					// They will have gone through all possible matchers
					if ( (elem = !matcher && elem) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// Apply set filters to unmatched elements
			matchedCount += i;
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( (matcher = setMatchers[j++]) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {
					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !(unmatched[i] || setMatched[i]) ) {
								setMatched[i] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, group /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {
		// Generate a function of recursive functions that can be used to check each element
		if ( !group ) {
			group = tokenize( selector );
		}
		i = group.length;
		while ( i-- ) {
			cached = matcherFromTokens( group[i] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );
	}
	return cached;
};

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[i], results );
	}
	return results;
}

function select( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		match = tokenize( selector );

	if ( !seed ) {
		// Try to minimize operations if there is only one group
		if ( match.length === 1 ) {

			// Take a shortcut and set the context if the root selector is an ID
			tokens = match[0] = match[0].slice( 0 );
			if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
					support.getById && context.nodeType === 9 && documentIsHTML &&
					Expr.relative[ tokens[1].type ] ) {

				context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
				if ( !context ) {
					return results;
				}
				selector = selector.slice( tokens.shift().value.length );
			}

			// Fetch a seed set for right-to-left matching
			i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
			while ( i-- ) {
				token = tokens[i];

				// Abort if we hit a combinator
				if ( Expr.relative[ (type = token.type) ] ) {
					break;
				}
				if ( (find = Expr.find[ type ]) ) {
					// Search, expanding context for leading sibling combinators
					if ( (seed = find(
						token.matches[0].replace( runescape, funescape ),
						rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
					)) ) {

						// If seed is empty or no tokens remain, we can return early
						tokens.splice( i, 1 );
						selector = seed.length && toSelector( tokens );
						if ( !selector ) {
							push.apply( results, seed );
							return results;
						}

						break;
					}
				}
			}
		}
	}

	// Compile and execute a filtering function
	// Provide `match` to avoid retokenization if we modified the selector above
	compile( selector, match )(
		seed,
		context,
		!documentIsHTML,
		results,
		rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
}

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome<14
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( div1 ) {
	// Should return 1, but returns 4 (following)
	return div1.compareDocumentPosition( document.createElement("div") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( div ) {
	div.innerHTML = "<a href='#'></a>";
	return div.firstChild.getAttribute("href") === "#" ;
}) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	});
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( div ) {
	div.innerHTML = "<input/>";
	div.firstChild.setAttribute( "value", "" );
	return div.firstChild.getAttribute( "value" ) === "";
}) ) {
	addHandle( "value", function( elem, name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	});
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( div ) {
	return div.getAttribute("disabled") == null;
}) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
					(val = elem.getAttributeNode( name )) && val.specified ?
					val.value :
				null;
		}
	});
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;
jQuery.expr[":"] = jQuery.expr.pseudos;
jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;



var rneedsContext = jQuery.expr.match.needsContext;

var rsingleTag = (/^<(\w+)\s*\/?>(?:<\/\1>|)$/);



var risSimple = /^.[^:#\[\.,]*$/;

// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( jQuery.isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			/* jshint -W018 */
			return !!qualifier.call( elem, i, elem ) !== not;
		});

	}

	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		});

	}

	if ( typeof qualifier === "string" ) {
		if ( risSimple.test( qualifier ) ) {
			return jQuery.filter( qualifier, elements, not );
		}

		qualifier = jQuery.filter( qualifier, elements );
	}

	return jQuery.grep( elements, function( elem ) {
		return ( jQuery.inArray( elem, qualifier ) >= 0 ) !== not;
	});
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	return elems.length === 1 && elem.nodeType === 1 ?
		jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [] :
		jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
			return elem.nodeType === 1;
		}));
};

jQuery.fn.extend({
	find: function( selector ) {
		var i,
			ret = [],
			self = this,
			len = self.length;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter(function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			}) );
		}

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		// Needed because $( selector, context ) becomes $( context ).find( selector )
		ret = this.pushStack( len > 1 ? jQuery.unique( ret ) : ret );
		ret.selector = this.selector ? this.selector + " " + selector : selector;
		return ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow(this, selector || [], false) );
	},
	not: function( selector ) {
		return this.pushStack( winnow(this, selector || [], true) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
});


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// Use the correct document accordingly with window argument (sandbox)
	document = window.document,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,

	init = jQuery.fn.init = function( selector, context ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector.charAt(0) === "<" && selector.charAt( selector.length - 1 ) === ">" && selector.length >= 3 ) {
				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && (match[1] || !context) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[1] ) {
					context = context instanceof jQuery ? context[0] : context;

					// scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[1],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[1] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {
							// Properties of context are called as methods if possible
							if ( jQuery.isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[2] );

					// Check parentNode to catch when Blackberry 4.6 returns
					// nodes that are no longer in the document #6963
					if ( elem && elem.parentNode ) {
						// Handle the case where IE and Opera return items
						// by name instead of ID
						if ( elem.id !== match[2] ) {
							return rootjQuery.find( selector );
						}

						// Otherwise, we inject the element directly into the jQuery object
						this.length = 1;
						this[0] = elem;
					}

					this.context = document;
					this.selector = selector;
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || rootjQuery ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this.context = this[0] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( jQuery.isFunction( selector ) ) {
			return typeof rootjQuery.ready !== "undefined" ?
				rootjQuery.ready( selector ) :
				// Execute immediately if ready is not present
				selector( jQuery );
		}

		if ( selector.selector !== undefined ) {
			this.selector = selector.selector;
			this.context = selector.context;
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,
	// methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.extend({
	dir: function( elem, dir, until ) {
		var matched = [],
			cur = elem[ dir ];

		while ( cur && cur.nodeType !== 9 && (until === undefined || cur.nodeType !== 1 || !jQuery( cur ).is( until )) ) {
			if ( cur.nodeType === 1 ) {
				matched.push( cur );
			}
			cur = cur[dir];
		}
		return matched;
	},

	sibling: function( n, elem ) {
		var r = [];

		for ( ; n; n = n.nextSibling ) {
			if ( n.nodeType === 1 && n !== elem ) {
				r.push( n );
			}
		}

		return r;
	}
});

jQuery.fn.extend({
	has: function( target ) {
		var i,
			targets = jQuery( target, this ),
			len = targets.length;

		return this.filter(function() {
			for ( i = 0; i < len; i++ ) {
				if ( jQuery.contains( this, targets[i] ) ) {
					return true;
				}
			}
		});
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			pos = rneedsContext.test( selectors ) || typeof selectors !== "string" ?
				jQuery( selectors, context || this.context ) :
				0;

		for ( ; i < l; i++ ) {
			for ( cur = this[i]; cur && cur !== context; cur = cur.parentNode ) {
				// Always skip document fragments
				if ( cur.nodeType < 11 && (pos ?
					pos.index(cur) > -1 :

					// Don't pass non-elements to Sizzle
					cur.nodeType === 1 &&
						jQuery.find.matchesSelector(cur, selectors)) ) {

					matched.push( cur );
					break;
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.unique( matched ) : matched );
	},

	// Determine the position of an element within
	// the matched set of elements
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[0] && this[0].parentNode ) ? this.first().prevAll().length : -1;
		}

		// index in selector
		if ( typeof elem === "string" ) {
			return jQuery.inArray( this[0], jQuery( elem ) );
		}

		// Locate the position of the desired element
		return jQuery.inArray(
			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[0] : elem, this );
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.unique(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter(selector)
		);
	}
});

function sibling( cur, dir ) {
	do {
		cur = cur[ dir ];
	} while ( cur && cur.nodeType !== 1 );

	return cur;
}

jQuery.each({
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return jQuery.dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return jQuery.dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return jQuery.dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return jQuery.sibling( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return jQuery.sibling( elem.firstChild );
	},
	contents: function( elem ) {
		return jQuery.nodeName( elem, "iframe" ) ?
			elem.contentDocument || elem.contentWindow.document :
			jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var ret = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			ret = jQuery.filter( selector, ret );
		}

		if ( this.length > 1 ) {
			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				ret = jQuery.unique( ret );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				ret = ret.reverse();
			}
		}

		return this.pushStack( ret );
	};
});
var rnotwhite = (/\S+/g);



// String to Object options format cache
var optionsCache = {};

// Convert String-formatted options into Object-formatted ones and store in cache
function createOptions( options ) {
	var object = optionsCache[ options ] = {};
	jQuery.each( options.match( rnotwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	});
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		( optionsCache[ options ] || createOptions( options ) ) :
		jQuery.extend( {}, options );

	var // Flag to know if list is currently firing
		firing,
		// Last fire value (for non-forgettable lists)
		memory,
		// Flag to know if list was already fired
		fired,
		// End of the loop when firing
		firingLength,
		// Index of currently firing callback (modified by remove if needed)
		firingIndex,
		// First callback to fire (used internally by add and fireWith)
		firingStart,
		// Actual callback list
		list = [],
		// Stack of fire calls for repeatable lists
		stack = !options.once && [],
		// Fire callbacks
		fire = function( data ) {
			memory = options.memory && data;
			fired = true;
			firingIndex = firingStart || 0;
			firingStart = 0;
			firingLength = list.length;
			firing = true;
			for ( ; list && firingIndex < firingLength; firingIndex++ ) {
				if ( list[ firingIndex ].apply( data[ 0 ], data[ 1 ] ) === false && options.stopOnFalse ) {
					memory = false; // To prevent further calls using add
					break;
				}
			}
			firing = false;
			if ( list ) {
				if ( stack ) {
					if ( stack.length ) {
						fire( stack.shift() );
					}
				} else if ( memory ) {
					list = [];
				} else {
					self.disable();
				}
			}
		},
		// Actual Callbacks object
		self = {
			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {
					// First, we save the current length
					var start = list.length;
					(function add( args ) {
						jQuery.each( args, function( _, arg ) {
							var type = jQuery.type( arg );
							if ( type === "function" ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && type !== "string" ) {
								// Inspect recursively
								add( arg );
							}
						});
					})( arguments );
					// Do we need to add the callbacks to the
					// current firing batch?
					if ( firing ) {
						firingLength = list.length;
					// With memory, if we're not firing then
					// we should call right away
					} else if ( memory ) {
						firingStart = start;
						fire( memory );
					}
				}
				return this;
			},
			// Remove a callback from the list
			remove: function() {
				if ( list ) {
					jQuery.each( arguments, function( _, arg ) {
						var index;
						while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
							list.splice( index, 1 );
							// Handle firing indexes
							if ( firing ) {
								if ( index <= firingLength ) {
									firingLength--;
								}
								if ( index <= firingIndex ) {
									firingIndex--;
								}
							}
						}
					});
				}
				return this;
			},
			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ? jQuery.inArray( fn, list ) > -1 : !!( list && list.length );
			},
			// Remove all callbacks from the list
			empty: function() {
				list = [];
				firingLength = 0;
				return this;
			},
			// Have the list do nothing anymore
			disable: function() {
				list = stack = memory = undefined;
				return this;
			},
			// Is it disabled?
			disabled: function() {
				return !list;
			},
			// Lock the list in its current state
			lock: function() {
				stack = undefined;
				if ( !memory ) {
					self.disable();
				}
				return this;
			},
			// Is it locked?
			locked: function() {
				return !stack;
			},
			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( list && ( !fired || stack ) ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					if ( firing ) {
						stack.push( args );
					} else {
						fire( args );
					}
				}
				return this;
			},
			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},
			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


jQuery.extend({

	Deferred: function( func ) {
		var tuples = [
				// action, add listener, listener list, final state
				[ "resolve", "done", jQuery.Callbacks("once memory"), "resolved" ],
				[ "reject", "fail", jQuery.Callbacks("once memory"), "rejected" ],
				[ "notify", "progress", jQuery.Callbacks("memory") ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				then: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;
					return jQuery.Deferred(function( newDefer ) {
						jQuery.each( tuples, function( i, tuple ) {
							var fn = jQuery.isFunction( fns[ i ] ) && fns[ i ];
							// deferred[ done | fail | progress ] for forwarding actions to newDefer
							deferred[ tuple[1] ](function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && jQuery.isFunction( returned.promise ) ) {
									returned.promise()
										.done( newDefer.resolve )
										.fail( newDefer.reject )
										.progress( newDefer.notify );
								} else {
									newDefer[ tuple[ 0 ] + "With" ]( this === promise ? newDefer.promise() : this, fn ? [ returned ] : arguments );
								}
							});
						});
						fns = null;
					}).promise();
				},
				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Keep pipe for back-compat
		promise.pipe = promise.then;

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 3 ];

			// promise[ done | fail | progress ] = list.add
			promise[ tuple[1] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add(function() {
					// state = [ resolved | rejected ]
					state = stateString;

				// [ reject_list | resolve_list ].disable; progress_list.lock
				}, tuples[ i ^ 1 ][ 2 ].disable, tuples[ 2 ][ 2 ].lock );
			}

			// deferred[ resolve | reject | notify ]
			deferred[ tuple[0] ] = function() {
				deferred[ tuple[0] + "With" ]( this === deferred ? promise : this, arguments );
				return this;
			};
			deferred[ tuple[0] + "With" ] = list.fireWith;
		});

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( subordinate /* , ..., subordinateN */ ) {
		var i = 0,
			resolveValues = slice.call( arguments ),
			length = resolveValues.length,

			// the count of uncompleted subordinates
			remaining = length !== 1 || ( subordinate && jQuery.isFunction( subordinate.promise ) ) ? length : 0,

			// the master Deferred. If resolveValues consist of only a single Deferred, just use that.
			deferred = remaining === 1 ? subordinate : jQuery.Deferred(),

			// Update function for both resolve and progress values
			updateFunc = function( i, contexts, values ) {
				return function( value ) {
					contexts[ i ] = this;
					values[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( values === progressValues ) {
						deferred.notifyWith( contexts, values );

					} else if ( !(--remaining) ) {
						deferred.resolveWith( contexts, values );
					}
				};
			},

			progressValues, progressContexts, resolveContexts;

		// add listeners to Deferred subordinates; treat others as resolved
		if ( length > 1 ) {
			progressValues = new Array( length );
			progressContexts = new Array( length );
			resolveContexts = new Array( length );
			for ( ; i < length; i++ ) {
				if ( resolveValues[ i ] && jQuery.isFunction( resolveValues[ i ].promise ) ) {
					resolveValues[ i ].promise()
						.done( updateFunc( i, resolveContexts, resolveValues ) )
						.fail( deferred.reject )
						.progress( updateFunc( i, progressContexts, progressValues ) );
				} else {
					--remaining;
				}
			}
		}

		// if we're not waiting on anything, resolve the master
		if ( !remaining ) {
			deferred.resolveWith( resolveContexts, resolveValues );
		}

		return deferred.promise();
	}
});


// The deferred used on DOM ready
var readyList;

jQuery.fn.ready = function( fn ) {
	// Add the callback
	jQuery.ready.promise().done( fn );

	return this;
};

jQuery.extend({
	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Hold (or release) the ready event
	holdReady: function( hold ) {
		if ( hold ) {
			jQuery.readyWait++;
		} else {
			jQuery.ready( true );
		}
	},

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
		if ( !document.body ) {
			return setTimeout( jQuery.ready );
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );

		// Trigger any bound ready events
		if ( jQuery.fn.trigger ) {
			jQuery( document ).trigger("ready").off("ready");
		}
	}
});

/**
 * Clean-up method for dom ready events
 */
function detach() {
	if ( document.addEventListener ) {
		document.removeEventListener( "DOMContentLoaded", completed, false );
		window.removeEventListener( "load", completed, false );

	} else {
		document.detachEvent( "onreadystatechange", completed );
		window.detachEvent( "onload", completed );
	}
}

/**
 * The ready event handler and self cleanup method
 */
function completed() {
	// readyState === "complete" is good enough for us to call the dom ready in oldIE
	if ( document.addEventListener || event.type === "load" || document.readyState === "complete" ) {
		detach();
		jQuery.ready();
	}
}

jQuery.ready.promise = function( obj ) {
	if ( !readyList ) {

		readyList = jQuery.Deferred();

		// Catch cases where $(document).ready() is called after the browser event has already occurred.
		// we once tried to use readyState "interactive" here, but it caused issues like the one
		// discovered by ChrisS here: http://bugs.jquery.com/ticket/12282#comment:15
		if ( document.readyState === "complete" ) {
			// Handle it asynchronously to allow scripts the opportunity to delay ready
			setTimeout( jQuery.ready );

		// Standards-based browsers support DOMContentLoaded
		} else if ( document.addEventListener ) {
			// Use the handy event callback
			document.addEventListener( "DOMContentLoaded", completed, false );

			// A fallback to window.onload, that will always work
			window.addEventListener( "load", completed, false );

		// If IE event model is used
		} else {
			// Ensure firing before onload, maybe late but safe also for iframes
			document.attachEvent( "onreadystatechange", completed );

			// A fallback to window.onload, that will always work
			window.attachEvent( "onload", completed );

			// If IE and not a frame
			// continually check to see if the document is ready
			var top = false;

			try {
				top = window.frameElement == null && document.documentElement;
			} catch(e) {}

			if ( top && top.doScroll ) {
				(function doScrollCheck() {
					if ( !jQuery.isReady ) {

						try {
							// Use the trick by Diego Perini
							// http://javascript.nwbox.com/IEContentLoaded/
							top.doScroll("left");
						} catch(e) {
							return setTimeout( doScrollCheck, 50 );
						}

						// detach all dom ready events
						detach();

						// and execute any waiting functions
						jQuery.ready();
					}
				})();
			}
		}
	}
	return readyList.promise( obj );
};


var strundefined = typeof undefined;



// Support: IE<9
// Iteration over object's inherited properties before its own
var i;
for ( i in jQuery( support ) ) {
	break;
}
support.ownLast = i !== "0";

// Note: most support tests are defined in their respective modules.
// false until the test is run
support.inlineBlockNeedsLayout = false;

jQuery(function() {
	// We need to execute this one support test ASAP because we need to know
	// if body.style.zoom needs to be set.

	var container, div,
		body = document.getElementsByTagName("body")[0];

	if ( !body ) {
		// Return for frameset docs that don't have a body
		return;
	}

	// Setup
	container = document.createElement( "div" );
	container.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px";

	div = document.createElement( "div" );
	body.appendChild( container ).appendChild( div );

	if ( typeof div.style.zoom !== strundefined ) {
		// Support: IE<8
		// Check if natively block-level elements act like inline-block
		// elements when setting their display to 'inline' and giving
		// them layout
		div.style.cssText = "border:0;margin:0;width:1px;padding:1px;display:inline;zoom:1";

		if ( (support.inlineBlockNeedsLayout = ( div.offsetWidth === 3 )) ) {
			// Prevent IE 6 from affecting layout for positioned elements #11048
			// Prevent IE from shrinking the body in IE 7 mode #12869
			// Support: IE<8
			body.style.zoom = 1;
		}
	}

	body.removeChild( container );

	// Null elements to avoid leaks in IE
	container = div = null;
});




(function() {
	var div = document.createElement( "div" );

	// Execute the test only if not already executed in another module.
	if (support.deleteExpando == null) {
		// Support: IE<9
		support.deleteExpando = true;
		try {
			delete div.test;
		} catch( e ) {
			support.deleteExpando = false;
		}
	}

	// Null elements to avoid leaks in IE.
	div = null;
})();


/**
 * Determines whether an object can have data
 */
jQuery.acceptData = function( elem ) {
	var noData = jQuery.noData[ (elem.nodeName + " ").toLowerCase() ],
		nodeType = +elem.nodeType || 1;

	// Do not set data on non-element DOM nodes because it will not be cleared (#8335).
	return nodeType !== 1 && nodeType !== 9 ?
		false :

		// Nodes accept data unless otherwise specified; rejection can be conditional
		!noData || noData !== true && elem.getAttribute("classid") === noData;
};


var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /([A-Z])/g;

function dataAttr( elem, key, data ) {
	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {

		var name = "data-" + key.replace( rmultiDash, "-$1" ).toLowerCase();

		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = data === "true" ? true :
					data === "false" ? false :
					data === "null" ? null :
					// Only convert to a number if it doesn't change the string
					+data + "" === data ? +data :
					rbrace.test( data ) ? jQuery.parseJSON( data ) :
					data;
			} catch( e ) {}

			// Make sure we set the data so it isn't changed later
			jQuery.data( elem, key, data );

		} else {
			data = undefined;
		}
	}

	return data;
}

// checks a cache object for emptiness
function isEmptyDataObject( obj ) {
	var name;
	for ( name in obj ) {

		// if the public data object is empty, the private is still empty
		if ( name === "data" && jQuery.isEmptyObject( obj[name] ) ) {
			continue;
		}
		if ( name !== "toJSON" ) {
			return false;
		}
	}

	return true;
}

function internalData( elem, name, data, pvt /* Internal Use Only */ ) {
	if ( !jQuery.acceptData( elem ) ) {
		return;
	}

	var ret, thisCache,
		internalKey = jQuery.expando,

		// We have to handle DOM nodes and JS objects differently because IE6-7
		// can't GC object references properly across the DOM-JS boundary
		isNode = elem.nodeType,

		// Only DOM nodes need the global jQuery cache; JS object data is
		// attached directly to the object so GC can occur automatically
		cache = isNode ? jQuery.cache : elem,

		// Only defining an ID for JS objects if its cache already exists allows
		// the code to shortcut on the same path as a DOM node with no cache
		id = isNode ? elem[ internalKey ] : elem[ internalKey ] && internalKey;

	// Avoid doing any more work than we need to when trying to get data on an
	// object that has no data at all
	if ( (!id || !cache[id] || (!pvt && !cache[id].data)) && data === undefined && typeof name === "string" ) {
		return;
	}

	if ( !id ) {
		// Only DOM nodes need a new unique ID for each element since their data
		// ends up in the global cache
		if ( isNode ) {
			id = elem[ internalKey ] = deletedIds.pop() || jQuery.guid++;
		} else {
			id = internalKey;
		}
	}

	if ( !cache[ id ] ) {
		// Avoid exposing jQuery metadata on plain JS objects when the object
		// is serialized using JSON.stringify
		cache[ id ] = isNode ? {} : { toJSON: jQuery.noop };
	}

	// An object can be passed to jQuery.data instead of a key/value pair; this gets
	// shallow copied over onto the existing cache
	if ( typeof name === "object" || typeof name === "function" ) {
		if ( pvt ) {
			cache[ id ] = jQuery.extend( cache[ id ], name );
		} else {
			cache[ id ].data = jQuery.extend( cache[ id ].data, name );
		}
	}

	thisCache = cache[ id ];

	// jQuery data() is stored in a separate object inside the object's internal data
	// cache in order to avoid key collisions between internal data and user-defined
	// data.
	if ( !pvt ) {
		if ( !thisCache.data ) {
			thisCache.data = {};
		}

		thisCache = thisCache.data;
	}

	if ( data !== undefined ) {
		thisCache[ jQuery.camelCase( name ) ] = data;
	}

	// Check for both converted-to-camel and non-converted data property names
	// If a data property was specified
	if ( typeof name === "string" ) {

		// First Try to find as-is property data
		ret = thisCache[ name ];

		// Test for null|undefined property data
		if ( ret == null ) {

			// Try to find the camelCased property
			ret = thisCache[ jQuery.camelCase( name ) ];
		}
	} else {
		ret = thisCache;
	}

	return ret;
}

function internalRemoveData( elem, name, pvt ) {
	if ( !jQuery.acceptData( elem ) ) {
		return;
	}

	var thisCache, i,
		isNode = elem.nodeType,

		// See jQuery.data for more information
		cache = isNode ? jQuery.cache : elem,
		id = isNode ? elem[ jQuery.expando ] : jQuery.expando;

	// If there is already no cache entry for this object, there is no
	// purpose in continuing
	if ( !cache[ id ] ) {
		return;
	}

	if ( name ) {

		thisCache = pvt ? cache[ id ] : cache[ id ].data;

		if ( thisCache ) {

			// Support array or space separated string names for data keys
			if ( !jQuery.isArray( name ) ) {

				// try the string as a key before any manipulation
				if ( name in thisCache ) {
					name = [ name ];
				} else {

					// split the camel cased version by spaces unless a key with the spaces exists
					name = jQuery.camelCase( name );
					if ( name in thisCache ) {
						name = [ name ];
					} else {
						name = name.split(" ");
					}
				}
			} else {
				// If "name" is an array of keys...
				// When data is initially created, via ("key", "val") signature,
				// keys will be converted to camelCase.
				// Since there is no way to tell _how_ a key was added, remove
				// both plain key and camelCase key. #12786
				// This will only penalize the array argument path.
				name = name.concat( jQuery.map( name, jQuery.camelCase ) );
			}

			i = name.length;
			while ( i-- ) {
				delete thisCache[ name[i] ];
			}

			// If there is no data left in the cache, we want to continue
			// and let the cache object itself get destroyed
			if ( pvt ? !isEmptyDataObject(thisCache) : !jQuery.isEmptyObject(thisCache) ) {
				return;
			}
		}
	}

	// See jQuery.data for more information
	if ( !pvt ) {
		delete cache[ id ].data;

		// Don't destroy the parent cache unless the internal data object
		// had been the only thing left in it
		if ( !isEmptyDataObject( cache[ id ] ) ) {
			return;
		}
	}

	// Destroy the cache
	if ( isNode ) {
		jQuery.cleanData( [ elem ], true );

	// Use delete when supported for expandos or `cache` is not a window per isWindow (#10080)
	/* jshint eqeqeq: false */
	} else if ( support.deleteExpando || cache != cache.window ) {
		/* jshint eqeqeq: true */
		delete cache[ id ];

	// When all else fails, null
	} else {
		cache[ id ] = null;
	}
}

jQuery.extend({
	cache: {},

	// The following elements (space-suffixed to avoid Object.prototype collisions)
	// throw uncatchable exceptions if you attempt to set expando properties
	noData: {
		"applet ": true,
		"embed ": true,
		// ...but Flash objects (which have this classid) *can* handle expandos
		"object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
	},

	hasData: function( elem ) {
		elem = elem.nodeType ? jQuery.cache[ elem[jQuery.expando] ] : elem[ jQuery.expando ];
		return !!elem && !isEmptyDataObject( elem );
	},

	data: function( elem, name, data ) {
		return internalData( elem, name, data );
	},

	removeData: function( elem, name ) {
		return internalRemoveData( elem, name );
	},

	// For internal use only.
	_data: function( elem, name, data ) {
		return internalData( elem, name, data, true );
	},

	_removeData: function( elem, name ) {
		return internalRemoveData( elem, name, true );
	}
});

jQuery.fn.extend({
	data: function( key, value ) {
		var i, name, data,
			elem = this[0],
			attrs = elem && elem.attributes;

		// Special expections of .data basically thwart jQuery.access,
		// so implement the relevant behavior ourselves

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = jQuery.data( elem );

				if ( elem.nodeType === 1 && !jQuery._data( elem, "parsedAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {
						name = attrs[i].name;

						if ( name.indexOf("data-") === 0 ) {
							name = jQuery.camelCase( name.slice(5) );

							dataAttr( elem, name, data[ name ] );
						}
					}
					jQuery._data( elem, "parsedAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each(function() {
				jQuery.data( this, key );
			});
		}

		return arguments.length > 1 ?

			// Sets one value
			this.each(function() {
				jQuery.data( this, key, value );
			}) :

			// Gets one value
			// Try to fetch any internally stored data first
			elem ? dataAttr( elem, key, jQuery.data( elem, key ) ) : undefined;
	},

	removeData: function( key ) {
		return this.each(function() {
			jQuery.removeData( this, key );
		});
	}
});


jQuery.extend({
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = jQuery._data( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || jQuery.isArray(data) ) {
					queue = jQuery._data( elem, type, jQuery.makeArray(data) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// not intended for public consumption - generates a queueHooks object, or returns the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return jQuery._data( elem, key ) || jQuery._data( elem, key, {
			empty: jQuery.Callbacks("once memory").add(function() {
				jQuery._removeData( elem, type + "queue" );
				jQuery._removeData( elem, key );
			})
		});
	}
});

jQuery.fn.extend({
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[0], type );
		}

		return data === undefined ?
			this :
			this.each(function() {
				var queue = jQuery.queue( this, type, data );

				// ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[0] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			});
	},
	dequeue: function( type ) {
		return this.each(function() {
			jQuery.dequeue( this, type );
		});
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},
	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = jQuery._data( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
});
var pnum = (/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;

var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var isHidden = function( elem, el ) {
		// isHidden might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;
		return jQuery.css( elem, "display" ) === "none" || !jQuery.contains( elem.ownerDocument, elem );
	};



// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = jQuery.access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		length = elems.length,
		bulk = key == null;

	// Sets many values
	if ( jQuery.type( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			jQuery.access( elems, fn, i, key[i], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !jQuery.isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {
			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < length; i++ ) {
				fn( elems[i], key, raw ? value : value.call( elems[i], i, fn( elems[i], key ) ) );
			}
		}
	}

	return chainable ?
		elems :

		// Gets
		bulk ?
			fn.call( elems ) :
			length ? fn( elems[0], key ) : emptyGet;
};
var rcheckableType = (/^(?:checkbox|radio)$/i);



(function() {
	var fragment = document.createDocumentFragment(),
		div = document.createElement("div"),
		input = document.createElement("input");

	// Setup
	div.setAttribute( "className", "t" );
	div.innerHTML = "  <link/><table></table><a href='/a'>a</a>";

	// IE strips leading whitespace when .innerHTML is used
	support.leadingWhitespace = div.firstChild.nodeType === 3;

	// Make sure that tbody elements aren't automatically inserted
	// IE will insert them into empty tables
	support.tbody = !div.getElementsByTagName( "tbody" ).length;

	// Make sure that link elements get serialized correctly by innerHTML
	// This requires a wrapper element in IE
	support.htmlSerialize = !!div.getElementsByTagName( "link" ).length;

	// Makes sure cloning an html5 element does not cause problems
	// Where outerHTML is undefined, this still works
	support.html5Clone =
		document.createElement( "nav" ).cloneNode( true ).outerHTML !== "<:nav></:nav>";

	// Check if a disconnected checkbox will retain its checked
	// value of true after appended to the DOM (IE6/7)
	input.type = "checkbox";
	input.checked = true;
	fragment.appendChild( input );
	support.appendChecked = input.checked;

	// Make sure textarea (and checkbox) defaultValue is properly cloned
	// Support: IE6-IE11+
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;

	// #11217 - WebKit loses check when the name is after the checked attribute
	fragment.appendChild( div );
	div.innerHTML = "<input type='radio' checked='checked' name='t'/>";

	// Support: Safari 5.1, iOS 5.1, Android 4.x, Android 2.3
	// old WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE<9
	// Opera does not clone events (and typeof div.attachEvent === undefined).
	// IE9-10 clones events bound via attachEvent, but they don't trigger with .click()
	support.noCloneEvent = true;
	if ( div.attachEvent ) {
		div.attachEvent( "onclick", function() {
			support.noCloneEvent = false;
		});

		div.cloneNode( true ).click();
	}

	// Execute the test only if not already executed in another module.
	if (support.deleteExpando == null) {
		// Support: IE<9
		support.deleteExpando = true;
		try {
			delete div.test;
		} catch( e ) {
			support.deleteExpando = false;
		}
	}

	// Null elements to avoid leaks in IE.
	fragment = div = input = null;
})();


(function() {
	var i, eventName,
		div = document.createElement( "div" );

	// Support: IE<9 (lack submit/change bubble), Firefox 23+ (lack focusin event)
	for ( i in { submit: true, change: true, focusin: true }) {
		eventName = "on" + i;

		if ( !(support[ i + "Bubbles" ] = eventName in window) ) {
			// Beware of CSP restrictions (https://developer.mozilla.org/en/Security/CSP)
			div.setAttribute( eventName, "t" );
			support[ i + "Bubbles" ] = div.attributes[ eventName ].expando === false;
		}
	}

	// Null elements to avoid leaks in IE.
	div = null;
})();


var rformElems = /^(?:input|select|textarea)$/i,
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|contextmenu)|click/,
	rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)$/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {
		var tmp, events, t, handleObjIn,
			special, eventHandle, handleObj,
			handlers, type, namespaces, origType,
			elemData = jQuery._data( elem );

		// Don't attach events to noData or text/comment nodes (but allow plain objects)
		if ( !elemData ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !(events = elemData.events) ) {
			events = elemData.events = {};
		}
		if ( !(eventHandle = elemData.handle) ) {
			eventHandle = elemData.handle = function( e ) {
				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== strundefined && (!e || jQuery.event.triggered !== e.type) ?
					jQuery.event.dispatch.apply( eventHandle.elem, arguments ) :
					undefined;
			};
			// Add elem as a property of the handle fn to prevent a memory leak with IE non-native events
			eventHandle.elem = elem;
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[t] ) || [];
			type = origType = tmp[1];
			namespaces = ( tmp[2] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend({
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join(".")
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !(handlers = events[ type ]) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener/attachEvent if the special events handler returns false
				if ( !special.setup || special.setup.call( elem, data, namespaces, eventHandle ) === false ) {
					// Bind the global event handler to the element
					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle, false );

					} else if ( elem.attachEvent ) {
						elem.attachEvent( "on" + type, eventHandle );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

		// Nullify elem to prevent memory leaks in IE
		elem = null;
	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {
		var j, handleObj, tmp,
			origCount, t, events,
			special, handlers, type,
			namespaces, origType,
			elemData = jQuery.hasData( elem ) && jQuery._data( elem );

		if ( !elemData || !(events = elemData.events) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[t] ) || [];
			type = origType = tmp[1];
			namespaces = ( tmp[2] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[2] && new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector || selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown || special.teardown.call( elem, namespaces, elemData.handle ) === false ) {
					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			delete elemData.handle;

			// removeData also checks for emptiness and clears the expando if empty
			// so use it instead of delete
			jQuery._removeData( elem, "events" );
		}
	},

	trigger: function( event, data, elem, onlyHandlers ) {
		var handle, ontype, cur,
			bubbleType, special, tmp, i,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split(".") : [];

		cur = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf(".") >= 0 ) {
			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split(".");
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf(":") < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join(".");
		event.namespace_re = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === (elem.ownerDocument || document) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( (cur = eventPath[i++]) && !event.isPropagationStopped() ) {

			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = ( jQuery._data( cur, "events" ) || {} )[ event.type ] && jQuery._data( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && jQuery.acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( (!special._default || special._default.apply( eventPath.pop(), data ) === false) &&
				jQuery.acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name name as the event.
				// Can't use an .isFunction() check here because IE6/7 fails that test.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && elem[ type ] && !jQuery.isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;
					try {
						elem[ type ]();
					} catch ( e ) {
						// IE<9 dies on focus/blur to hidden element (#1486,#12518)
						// only reproducible on winXP IE8 native, not IE9 in IE8 mode
					}
					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	dispatch: function( event ) {

		// Make a writable jQuery.Event from the native event object
		event = jQuery.event.fix( event );

		var i, ret, handleObj, matched, j,
			handlerQueue = [],
			args = slice.call( arguments ),
			handlers = ( jQuery._data( this, "events" ) || {} )[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[0] = event;
		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( (matched = handlerQueue[ i++ ]) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( (handleObj = matched.handlers[ j++ ]) && !event.isImmediatePropagationStopped() ) {

				// Triggered event must either 1) have no namespace, or
				// 2) have namespace(s) a subset or equal to those in the bound event (both can have no namespace).
				if ( !event.namespace_re || event.namespace_re.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( (jQuery.event.special[ handleObj.origType ] || {}).handle || handleObj.handler )
							.apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( (event.result = ret) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var sel, handleObj, matches, i,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Find delegate handlers
		// Black-hole SVG <use> instance trees (#13180)
		// Avoid non-left-click bubbling in Firefox (#3861)
		if ( delegateCount && cur.nodeType && (!event.button || event.type !== "click") ) {

			/* jshint eqeqeq: false */
			for ( ; cur != this; cur = cur.parentNode || this ) {
				/* jshint eqeqeq: true */

				// Don't check non-elements (#13208)
				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.nodeType === 1 && (cur.disabled !== true || event.type !== "click") ) {
					matches = [];
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matches[ sel ] === undefined ) {
							matches[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) >= 0 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matches[ sel ] ) {
							matches.push( handleObj );
						}
					}
					if ( matches.length ) {
						handlerQueue.push({ elem: cur, handlers: matches });
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		if ( delegateCount < handlers.length ) {
			handlerQueue.push({ elem: this, handlers: handlers.slice( delegateCount ) });
		}

		return handlerQueue;
	},

	fix: function( event ) {
		if ( event[ jQuery.expando ] ) {
			return event;
		}

		// Create a writable copy of the event object and normalize some properties
		var i, prop, copy,
			type = event.type,
			originalEvent = event,
			fixHook = this.fixHooks[ type ];

		if ( !fixHook ) {
			this.fixHooks[ type ] = fixHook =
				rmouseEvent.test( type ) ? this.mouseHooks :
				rkeyEvent.test( type ) ? this.keyHooks :
				{};
		}
		copy = fixHook.props ? this.props.concat( fixHook.props ) : this.props;

		event = new jQuery.Event( originalEvent );

		i = copy.length;
		while ( i-- ) {
			prop = copy[ i ];
			event[ prop ] = originalEvent[ prop ];
		}

		// Support: IE<9
		// Fix target property (#1925)
		if ( !event.target ) {
			event.target = originalEvent.srcElement || document;
		}

		// Support: Chrome 23+, Safari?
		// Target should not be a text node (#504, #13143)
		if ( event.target.nodeType === 3 ) {
			event.target = event.target.parentNode;
		}

		// Support: IE<9
		// For mouse/key events, metaKey==false if it's undefined (#3368, #11328)
		event.metaKey = !!event.metaKey;

		return fixHook.filter ? fixHook.filter( event, originalEvent ) : event;
	},

	// Includes some event props shared by KeyEvent and MouseEvent
	props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),

	fixHooks: {},

	keyHooks: {
		props: "char charCode key keyCode".split(" "),
		filter: function( event, original ) {

			// Add which for key events
			if ( event.which == null ) {
				event.which = original.charCode != null ? original.charCode : original.keyCode;
			}

			return event;
		}
	},

	mouseHooks: {
		props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
		filter: function( event, original ) {
			var body, eventDoc, doc,
				button = original.button,
				fromElement = original.fromElement;

			// Calculate pageX/Y if missing and clientX/Y available
			if ( event.pageX == null && original.clientX != null ) {
				eventDoc = event.target.ownerDocument || document;
				doc = eventDoc.documentElement;
				body = eventDoc.body;

				event.pageX = original.clientX + ( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) - ( doc && doc.clientLeft || body && body.clientLeft || 0 );
				event.pageY = original.clientY + ( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) - ( doc && doc.clientTop  || body && body.clientTop  || 0 );
			}

			// Add relatedTarget, if necessary
			if ( !event.relatedTarget && fromElement ) {
				event.relatedTarget = fromElement === event.target ? original.toElement : fromElement;
			}

			// Add which for click: 1 === left; 2 === middle; 3 === right
			// Note: button is not normalized, so don't use it
			if ( !event.which && button !== undefined ) {
				event.which = ( button & 1 ? 1 : ( button & 2 ? 3 : ( button & 4 ? 2 : 0 ) ) );
			}

			return event;
		}
	},

	special: {
		load: {
			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		focus: {
			// Fire native event if possible so blur/focus sequence is correct
			trigger: function() {
				if ( this !== safeActiveElement() && this.focus ) {
					try {
						this.focus();
						return false;
					} catch ( e ) {
						// Support: IE<9
						// If we error on focus to hidden element (#1486, #12518),
						// let .trigger() run the handlers
					}
				}
			},
			delegateType: "focusin"
		},
		blur: {
			trigger: function() {
				if ( this === safeActiveElement() && this.blur ) {
					this.blur();
					return false;
				}
			},
			delegateType: "focusout"
		},
		click: {
			// For checkbox, fire native event so checked state will be right
			trigger: function() {
				if ( jQuery.nodeName( this, "input" ) && this.type === "checkbox" && this.click ) {
					this.click();
					return false;
				}
			},

			// For cross-browser consistency, don't fire native .click() on links
			_default: function( event ) {
				return jQuery.nodeName( event.target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Even when returnValue equals to undefined Firefox will still show alert
				if ( event.result !== undefined ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	},

	simulate: function( type, elem, event, bubble ) {
		// Piggyback on a donor event to simulate a different one.
		// Fake originalEvent to avoid donor's stopPropagation, but if the
		// simulated event prevents default then we do the same on the donor.
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true,
				originalEvent: {}
			}
		);
		if ( bubble ) {
			jQuery.event.trigger( e, null, elem );
		} else {
			jQuery.event.dispatch.call( elem, e );
		}
		if ( e.isDefaultPrevented() ) {
			event.preventDefault();
		}
	}
};

jQuery.removeEvent = document.removeEventListener ?
	function( elem, type, handle ) {
		if ( elem.removeEventListener ) {
			elem.removeEventListener( type, handle, false );
		}
	} :
	function( elem, type, handle ) {
		var name = "on" + type;

		if ( elem.detachEvent ) {

			// #8545, #7054, preventing memory leaks for custom events in IE6-8
			// detachEvent needed property on element, by name of that event, to properly expose it to GC
			if ( typeof elem[ name ] === strundefined ) {
				elem[ name ] = null;
			}

			elem.detachEvent( name, handle );
		}
	};

jQuery.Event = function( src, props ) {
	// Allow instantiation without the 'new' keyword
	if ( !(this instanceof jQuery.Event) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined && (
				// Support: IE < 9
				src.returnValue === false ||
				// Support: Android < 4.0
				src.getPreventDefault && src.getPreventDefault() ) ?
			returnTrue :
			returnFalse;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || jQuery.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;
		if ( !e ) {
			return;
		}

		// If preventDefault exists, run it on the original event
		if ( e.preventDefault ) {
			e.preventDefault();

		// Support: IE
		// Otherwise set the returnValue property of the original event to false
		} else {
			e.returnValue = false;
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;
		if ( !e ) {
			return;
		}
		// If stopPropagation exists, run it on the original event
		if ( e.stopPropagation ) {
			e.stopPropagation();
		}

		// Support: IE
		// Set the cancelBubble property of the original event to true
		e.cancelBubble = true;
	},
	stopImmediatePropagation: function() {
		this.isImmediatePropagationStopped = returnTrue;
		this.stopPropagation();
	}
};

// Create mouseenter/leave events using mouseover/out and event-time checks
jQuery.each({
	mouseenter: "mouseover",
	mouseleave: "mouseout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mousenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || (related !== target && !jQuery.contains( target, related )) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
});

// IE submit delegation
if ( !support.submitBubbles ) {

	jQuery.event.special.submit = {
		setup: function() {
			// Only need this for delegated form submit events
			if ( jQuery.nodeName( this, "form" ) ) {
				return false;
			}

			// Lazy-add a submit handler when a descendant form may potentially be submitted
			jQuery.event.add( this, "click._submit keypress._submit", function( e ) {
				// Node name check avoids a VML-related crash in IE (#9807)
				var elem = e.target,
					form = jQuery.nodeName( elem, "input" ) || jQuery.nodeName( elem, "button" ) ? elem.form : undefined;
				if ( form && !jQuery._data( form, "submitBubbles" ) ) {
					jQuery.event.add( form, "submit._submit", function( event ) {
						event._submit_bubble = true;
					});
					jQuery._data( form, "submitBubbles", true );
				}
			});
			// return undefined since we don't need an event listener
		},

		postDispatch: function( event ) {
			// If form was submitted by the user, bubble the event up the tree
			if ( event._submit_bubble ) {
				delete event._submit_bubble;
				if ( this.parentNode && !event.isTrigger ) {
					jQuery.event.simulate( "submit", this.parentNode, event, true );
				}
			}
		},

		teardown: function() {
			// Only need this for delegated form submit events
			if ( jQuery.nodeName( this, "form" ) ) {
				return false;
			}

			// Remove delegated handlers; cleanData eventually reaps submit handlers attached above
			jQuery.event.remove( this, "._submit" );
		}
	};
}

// IE change delegation and checkbox/radio fix
if ( !support.changeBubbles ) {

	jQuery.event.special.change = {

		setup: function() {

			if ( rformElems.test( this.nodeName ) ) {
				// IE doesn't fire change on a check/radio until blur; trigger it on click
				// after a propertychange. Eat the blur-change in special.change.handle.
				// This still fires onchange a second time for check/radio after blur.
				if ( this.type === "checkbox" || this.type === "radio" ) {
					jQuery.event.add( this, "propertychange._change", function( event ) {
						if ( event.originalEvent.propertyName === "checked" ) {
							this._just_changed = true;
						}
					});
					jQuery.event.add( this, "click._change", function( event ) {
						if ( this._just_changed && !event.isTrigger ) {
							this._just_changed = false;
						}
						// Allow triggered, simulated change events (#11500)
						jQuery.event.simulate( "change", this, event, true );
					});
				}
				return false;
			}
			// Delegated event; lazy-add a change handler on descendant inputs
			jQuery.event.add( this, "beforeactivate._change", function( e ) {
				var elem = e.target;

				if ( rformElems.test( elem.nodeName ) && !jQuery._data( elem, "changeBubbles" ) ) {
					jQuery.event.add( elem, "change._change", function( event ) {
						if ( this.parentNode && !event.isSimulated && !event.isTrigger ) {
							jQuery.event.simulate( "change", this.parentNode, event, true );
						}
					});
					jQuery._data( elem, "changeBubbles", true );
				}
			});
		},

		handle: function( event ) {
			var elem = event.target;

			// Swallow native change events from checkbox/radio, we already triggered them above
			if ( this !== elem || event.isSimulated || event.isTrigger || (elem.type !== "radio" && elem.type !== "checkbox") ) {
				return event.handleObj.handler.apply( this, arguments );
			}
		},

		teardown: function() {
			jQuery.event.remove( this, "._change" );

			return !rformElems.test( this.nodeName );
		}
	};
}

// Create "bubbling" focus and blur events
if ( !support.focusinBubbles ) {
	jQuery.each({ focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
				jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ), true );
			};

		jQuery.event.special[ fix ] = {
			setup: function() {
				var doc = this.ownerDocument || this,
					attaches = jQuery._data( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				jQuery._data( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this,
					attaches = jQuery._data( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					jQuery._removeData( doc, fix );
				} else {
					jQuery._data( doc, fix, attaches );
				}
			}
		};
	});
}

jQuery.fn.extend({

	on: function( types, selector, data, fn, /*INTERNAL*/ one ) {
		var type, origFn;

		// Types can be a map of types/handlers
		if ( typeof types === "object" ) {
			// ( types-Object, selector, data )
			if ( typeof selector !== "string" ) {
				// ( types-Object, data )
				data = data || selector;
				selector = undefined;
			}
			for ( type in types ) {
				this.on( type, selector, data, types[ type ], one );
			}
			return this;
		}

		if ( data == null && fn == null ) {
			// ( types, fn )
			fn = selector;
			data = selector = undefined;
		} else if ( fn == null ) {
			if ( typeof selector === "string" ) {
				// ( types, selector, fn )
				fn = data;
				data = undefined;
			} else {
				// ( types, data, fn )
				fn = data;
				data = selector;
				selector = undefined;
			}
		}
		if ( fn === false ) {
			fn = returnFalse;
		} else if ( !fn ) {
			return this;
		}

		if ( one === 1 ) {
			origFn = fn;
			fn = function( event ) {
				// Can use an empty set, since event contains the info
				jQuery().off( event );
				return origFn.apply( this, arguments );
			};
			// Use same guid so caller can remove using origFn
			fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
		}
		return this.each( function() {
			jQuery.event.add( this, types, fn, data, selector );
		});
	},
	one: function( types, selector, data, fn ) {
		return this.on( types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {
			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {
			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {
			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each(function() {
			jQuery.event.remove( this, types, fn, selector );
		});
	},

	trigger: function( type, data ) {
		return this.each(function() {
			jQuery.event.trigger( type, data, this );
		});
	},
	triggerHandler: function( type, data ) {
		var elem = this[0];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
});


function createSafeFragment( document ) {
	var list = nodeNames.split( "|" ),
		safeFrag = document.createDocumentFragment();

	if ( safeFrag.createElement ) {
		while ( list.length ) {
			safeFrag.createElement(
				list.pop()
			);
		}
	}
	return safeFrag;
}

var nodeNames = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|" +
		"header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
	rinlinejQuery = / jQuery\d+="(?:null|\d+)"/g,
	rnoshimcache = new RegExp("<(?:" + nodeNames + ")[\\s/>]", "i"),
	rleadingWhitespace = /^\s+/,
	rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
	rtagName = /<([\w:]+)/,
	rtbody = /<tbody/i,
	rhtml = /<|&#?\w+;/,
	rnoInnerhtml = /<(?:script|style|link)/i,
	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rscriptType = /^$|\/(?:java|ecma)script/i,
	rscriptTypeMasked = /^true\/(.*)/,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,

	// We have to close these tags to support XHTML (#13200)
	wrapMap = {
		option: [ 1, "<select multiple='multiple'>", "</select>" ],
		legend: [ 1, "<fieldset>", "</fieldset>" ],
		area: [ 1, "<map>", "</map>" ],
		param: [ 1, "<object>", "</object>" ],
		thead: [ 1, "<table>", "</table>" ],
		tr: [ 2, "<table><tbody>", "</tbody></table>" ],
		col: [ 2, "<table><tbody></tbody><colgroup>", "</colgroup></table>" ],
		td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

		// IE6-8 can't serialize link, script, style, or any html5 (NoScope) tags,
		// unless wrapped in a div with non-breaking characters in front of it.
		_default: support.htmlSerialize ? [ 0, "", "" ] : [ 1, "X<div>", "</div>"  ]
	},
	safeFragment = createSafeFragment( document ),
	fragmentDiv = safeFragment.appendChild( document.createElement("div") );

wrapMap.optgroup = wrapMap.option;
wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;

function getAll( context, tag ) {
	var elems, elem,
		i = 0,
		found = typeof context.getElementsByTagName !== strundefined ? context.getElementsByTagName( tag || "*" ) :
			typeof context.querySelectorAll !== strundefined ? context.querySelectorAll( tag || "*" ) :
			undefined;

	if ( !found ) {
		for ( found = [], elems = context.childNodes || context; (elem = elems[i]) != null; i++ ) {
			if ( !tag || jQuery.nodeName( elem, tag ) ) {
				found.push( elem );
			} else {
				jQuery.merge( found, getAll( elem, tag ) );
			}
		}
	}

	return tag === undefined || tag && jQuery.nodeName( context, tag ) ?
		jQuery.merge( [ context ], found ) :
		found;
}

// Used in buildFragment, fixes the defaultChecked property
function fixDefaultChecked( elem ) {
	if ( rcheckableType.test( elem.type ) ) {
		elem.defaultChecked = elem.checked;
	}
}

// Support: IE<8
// Manipulating tables requires a tbody
function manipulationTarget( elem, content ) {
	return jQuery.nodeName( elem, "table" ) &&
		jQuery.nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ?

		elem.getElementsByTagName("tbody")[0] ||
			elem.appendChild( elem.ownerDocument.createElement("tbody") ) :
		elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = (jQuery.find.attr( elem, "type" ) !== null) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	var match = rscriptTypeMasked.exec( elem.type );
	if ( match ) {
		elem.type = match[1];
	} else {
		elem.removeAttribute("type");
	}
	return elem;
}

// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var elem,
		i = 0;
	for ( ; (elem = elems[i]) != null; i++ ) {
		jQuery._data( elem, "globalEval", !refElements || jQuery._data( refElements[i], "globalEval" ) );
	}
}

function cloneCopyEvent( src, dest ) {

	if ( dest.nodeType !== 1 || !jQuery.hasData( src ) ) {
		return;
	}

	var type, i, l,
		oldData = jQuery._data( src ),
		curData = jQuery._data( dest, oldData ),
		events = oldData.events;

	if ( events ) {
		delete curData.handle;
		curData.events = {};

		for ( type in events ) {
			for ( i = 0, l = events[ type ].length; i < l; i++ ) {
				jQuery.event.add( dest, type, events[ type ][ i ] );
			}
		}
	}

	// make the cloned public data object a copy from the original
	if ( curData.data ) {
		curData.data = jQuery.extend( {}, curData.data );
	}
}

function fixCloneNodeIssues( src, dest ) {
	var nodeName, e, data;

	// We do not need to do anything for non-Elements
	if ( dest.nodeType !== 1 ) {
		return;
	}

	nodeName = dest.nodeName.toLowerCase();

	// IE6-8 copies events bound via attachEvent when using cloneNode.
	if ( !support.noCloneEvent && dest[ jQuery.expando ] ) {
		data = jQuery._data( dest );

		for ( e in data.events ) {
			jQuery.removeEvent( dest, e, data.handle );
		}

		// Event data gets referenced instead of copied if the expando gets copied too
		dest.removeAttribute( jQuery.expando );
	}

	// IE blanks contents when cloning scripts, and tries to evaluate newly-set text
	if ( nodeName === "script" && dest.text !== src.text ) {
		disableScript( dest ).text = src.text;
		restoreScript( dest );

	// IE6-10 improperly clones children of object elements using classid.
	// IE10 throws NoModificationAllowedError if parent is null, #12132.
	} else if ( nodeName === "object" ) {
		if ( dest.parentNode ) {
			dest.outerHTML = src.outerHTML;
		}

		// This path appears unavoidable for IE9. When cloning an object
		// element in IE9, the outerHTML strategy above is not sufficient.
		// If the src has innerHTML and the destination does not,
		// copy the src.innerHTML into the dest.innerHTML. #10324
		if ( support.html5Clone && ( src.innerHTML && !jQuery.trim(dest.innerHTML) ) ) {
			dest.innerHTML = src.innerHTML;
		}

	} else if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		// IE6-8 fails to persist the checked state of a cloned checkbox
		// or radio button. Worse, IE6-7 fail to give the cloned element
		// a checked appearance if the defaultChecked value isn't also set

		dest.defaultChecked = dest.checked = src.checked;

		// IE6-7 get confused and end up setting the value of a cloned
		// checkbox/radio button to an empty string instead of "on"
		if ( dest.value !== src.value ) {
			dest.value = src.value;
		}

	// IE6-8 fails to return the selected option to the default selected
	// state when cloning options
	} else if ( nodeName === "option" ) {
		dest.defaultSelected = dest.selected = src.defaultSelected;

	// IE6-8 fails to set the defaultValue to the correct value when
	// cloning other types of input fields
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

jQuery.extend({
	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var destElements, node, clone, i, srcElements,
			inPage = jQuery.contains( elem.ownerDocument, elem );

		if ( support.html5Clone || jQuery.isXMLDoc(elem) || !rnoshimcache.test( "<" + elem.nodeName + ">" ) ) {
			clone = elem.cloneNode( true );

		// IE<=8 does not properly clone detached, unknown element nodes
		} else {
			fragmentDiv.innerHTML = elem.outerHTML;
			fragmentDiv.removeChild( clone = fragmentDiv.firstChild );
		}

		if ( (!support.noCloneEvent || !support.noCloneChecked) &&
				(elem.nodeType === 1 || elem.nodeType === 11) && !jQuery.isXMLDoc(elem) ) {

			// We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			// Fix all IE cloning issues
			for ( i = 0; (node = srcElements[i]) != null; ++i ) {
				// Ensure that the destination node is not null; Fixes #9587
				if ( destElements[i] ) {
					fixCloneNodeIssues( node, destElements[i] );
				}
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0; (node = srcElements[i]) != null; i++ ) {
					cloneCopyEvent( node, destElements[i] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		destElements = srcElements = node = null;

		// Return the cloned set
		return clone;
	},

	buildFragment: function( elems, context, scripts, selection ) {
		var j, elem, contains,
			tmp, tag, tbody, wrap,
			l = elems.length,

			// Ensure a safe fragment
			safe = createSafeFragment( context ),

			nodes = [],
			i = 0;

		for ( ; i < l; i++ ) {
			elem = elems[ i ];

			if ( elem || elem === 0 ) {

				// Add nodes directly
				if ( jQuery.type( elem ) === "object" ) {
					jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

				// Convert non-html into a text node
				} else if ( !rhtml.test( elem ) ) {
					nodes.push( context.createTextNode( elem ) );

				// Convert html into DOM nodes
				} else {
					tmp = tmp || safe.appendChild( context.createElement("div") );

					// Deserialize a standard representation
					tag = (rtagName.exec( elem ) || [ "", "" ])[ 1 ].toLowerCase();
					wrap = wrapMap[ tag ] || wrapMap._default;

					tmp.innerHTML = wrap[1] + elem.replace( rxhtmlTag, "<$1></$2>" ) + wrap[2];

					// Descend through wrappers to the right content
					j = wrap[0];
					while ( j-- ) {
						tmp = tmp.lastChild;
					}

					// Manually add leading whitespace removed by IE
					if ( !support.leadingWhitespace && rleadingWhitespace.test( elem ) ) {
						nodes.push( context.createTextNode( rleadingWhitespace.exec( elem )[0] ) );
					}

					// Remove IE's autoinserted <tbody> from table fragments
					if ( !support.tbody ) {

						// String was a <table>, *may* have spurious <tbody>
						elem = tag === "table" && !rtbody.test( elem ) ?
							tmp.firstChild :

							// String was a bare <thead> or <tfoot>
							wrap[1] === "<table>" && !rtbody.test( elem ) ?
								tmp :
								0;

						j = elem && elem.childNodes.length;
						while ( j-- ) {
							if ( jQuery.nodeName( (tbody = elem.childNodes[j]), "tbody" ) && !tbody.childNodes.length ) {
								elem.removeChild( tbody );
							}
						}
					}

					jQuery.merge( nodes, tmp.childNodes );

					// Fix #12392 for WebKit and IE > 9
					tmp.textContent = "";

					// Fix #12392 for oldIE
					while ( tmp.firstChild ) {
						tmp.removeChild( tmp.firstChild );
					}

					// Remember the top-level container for proper cleanup
					tmp = safe.lastChild;
				}
			}
		}

		// Fix #11356: Clear elements from fragment
		if ( tmp ) {
			safe.removeChild( tmp );
		}

		// Reset defaultChecked for any radios and checkboxes
		// about to be appended to the DOM in IE 6/7 (#8060)
		if ( !support.appendChecked ) {
			jQuery.grep( getAll( nodes, "input" ), fixDefaultChecked );
		}

		i = 0;
		while ( (elem = nodes[ i++ ]) ) {

			// #4087 - If origin and destination elements are the same, and this is
			// that element, do not do anything
			if ( selection && jQuery.inArray( elem, selection ) !== -1 ) {
				continue;
			}

			contains = jQuery.contains( elem.ownerDocument, elem );

			// Append to fragment
			tmp = getAll( safe.appendChild( elem ), "script" );

			// Preserve script evaluation history
			if ( contains ) {
				setGlobalEval( tmp );
			}

			// Capture executables
			if ( scripts ) {
				j = 0;
				while ( (elem = tmp[ j++ ]) ) {
					if ( rscriptType.test( elem.type || "" ) ) {
						scripts.push( elem );
					}
				}
			}
		}

		tmp = null;

		return safe;
	},

	cleanData: function( elems, /* internal */ acceptData ) {
		var elem, type, id, data,
			i = 0,
			internalKey = jQuery.expando,
			cache = jQuery.cache,
			deleteExpando = support.deleteExpando,
			special = jQuery.event.special;

		for ( ; (elem = elems[i]) != null; i++ ) {
			if ( acceptData || jQuery.acceptData( elem ) ) {

				id = elem[ internalKey ];
				data = id && cache[ id ];

				if ( data ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}

					// Remove cache only if it was not already removed by jQuery.event.remove
					if ( cache[ id ] ) {

						delete cache[ id ];

						// IE does not allow us to delete expando properties from nodes,
						// nor does it have a removeAttribute function on Document nodes;
						// we must handle all of these cases
						if ( deleteExpando ) {
							delete elem[ internalKey ];

						} else if ( typeof elem.removeAttribute !== strundefined ) {
							elem.removeAttribute( internalKey );

						} else {
							elem[ internalKey ] = null;
						}

						deletedIds.push( id );
					}
				}
			}
		}
	}
});

jQuery.fn.extend({
	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().append( ( this[0] && this[0].ownerDocument || document ).createTextNode( value ) );
		}, null, value, arguments.length );
	},

	append: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		});
	},

	prepend: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		});
	},

	before: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		});
	},

	after: function() {
		return this.domManip( arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		});
	},

	remove: function( selector, keepData /* Internal Use Only */ ) {
		var elem,
			elems = selector ? jQuery.filter( selector, this ) : this,
			i = 0;

		for ( ; (elem = elems[i]) != null; i++ ) {

			if ( !keepData && elem.nodeType === 1 ) {
				jQuery.cleanData( getAll( elem ) );
			}

			if ( elem.parentNode ) {
				if ( keepData && jQuery.contains( elem.ownerDocument, elem ) ) {
					setGlobalEval( getAll( elem, "script" ) );
				}
				elem.parentNode.removeChild( elem );
			}
		}

		return this;
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; (elem = this[i]) != null; i++ ) {
			// Remove element nodes and prevent memory leaks
			if ( elem.nodeType === 1 ) {
				jQuery.cleanData( getAll( elem, false ) );
			}

			// Remove any remaining nodes
			while ( elem.firstChild ) {
				elem.removeChild( elem.firstChild );
			}

			// If this is a select, ensure that it displays empty (#12336)
			// Support: IE<9
			if ( elem.options && jQuery.nodeName( elem, "select" ) ) {
				elem.options.length = 0;
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map(function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		});
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined ) {
				return elem.nodeType === 1 ?
					elem.innerHTML.replace( rinlinejQuery, "" ) :
					undefined;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				( support.htmlSerialize || !rnoshimcache.test( value )  ) &&
				( support.leadingWhitespace || !rleadingWhitespace.test( value ) ) &&
				!wrapMap[ (rtagName.exec( value ) || [ "", "" ])[ 1 ].toLowerCase() ] ) {

				value = value.replace( rxhtmlTag, "<$1></$2>" );

				try {
					for (; i < l; i++ ) {
						// Remove element nodes and prevent memory leaks
						elem = this[i] || {};
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch(e) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var arg = arguments[ 0 ];

		// Make the changes, replacing each context element with the new content
		this.domManip( arguments, function( elem ) {
			arg = this.parentNode;

			jQuery.cleanData( getAll( this ) );

			if ( arg ) {
				arg.replaceChild( elem, this );
			}
		});

		// Force removal if there was no new content (e.g., from empty arguments)
		return arg && (arg.length || arg.nodeType) ? this : this.remove();
	},

	detach: function( selector ) {
		return this.remove( selector, true );
	},

	domManip: function( args, callback ) {

		// Flatten any nested arrays
		args = concat.apply( [], args );

		var first, node, hasScripts,
			scripts, doc, fragment,
			i = 0,
			l = this.length,
			set = this,
			iNoClone = l - 1,
			value = args[0],
			isFunction = jQuery.isFunction( value );

		// We can't cloneNode fragments that contain checked, in WebKit
		if ( isFunction ||
				( l > 1 && typeof value === "string" &&
					!support.checkClone && rchecked.test( value ) ) ) {
			return this.each(function( index ) {
				var self = set.eq( index );
				if ( isFunction ) {
					args[0] = value.call( this, index, self.html() );
				}
				self.domManip( args, callback );
			});
		}

		if ( l ) {
			fragment = jQuery.buildFragment( args, this[ 0 ].ownerDocument, false, this );
			first = fragment.firstChild;

			if ( fragment.childNodes.length === 1 ) {
				fragment = first;
			}

			if ( first ) {
				scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
				hasScripts = scripts.length;

				// Use the original fragment for the last item instead of the first because it can end up
				// being emptied incorrectly in certain situations (#8070).
				for ( ; i < l; i++ ) {
					node = fragment;

					if ( i !== iNoClone ) {
						node = jQuery.clone( node, true, true );

						// Keep references to cloned scripts for later restoration
						if ( hasScripts ) {
							jQuery.merge( scripts, getAll( node, "script" ) );
						}
					}

					callback.call( this[i], node, i );
				}

				if ( hasScripts ) {
					doc = scripts[ scripts.length - 1 ].ownerDocument;

					// Reenable scripts
					jQuery.map( scripts, restoreScript );

					// Evaluate executable scripts on first document insertion
					for ( i = 0; i < hasScripts; i++ ) {
						node = scripts[ i ];
						if ( rscriptType.test( node.type || "" ) &&
							!jQuery._data( node, "globalEval" ) && jQuery.contains( doc, node ) ) {

							if ( node.src ) {
								// Optional AJAX dependency, but won't run scripts if not present
								if ( jQuery._evalUrl ) {
									jQuery._evalUrl( node.src );
								}
							} else {
								jQuery.globalEval( ( node.text || node.textContent || node.innerHTML || "" ).replace( rcleanScript, "" ) );
							}
						}
					}
				}

				// Fix #11809: Avoid leaking memory
				fragment = first = null;
			}
		}

		return this;
	}
});

jQuery.each({
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			i = 0,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone(true);
			jQuery( insert[i] )[ original ]( elems );

			// Modern browsers can apply jQuery collections as arrays, but oldIE needs a .get()
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
});


var iframe,
	elemdisplay = {};

/**
 * Retrieve the actual display of a element
 * @param {String} name nodeName of the element
 * @param {Object} doc Document object
 */
// Called only from within defaultDisplay
function actualDisplay( name, doc ) {
	var elem = jQuery( doc.createElement( name ) ).appendTo( doc.body ),

		// getDefaultComputedStyle might be reliably used only on attached element
		display = window.getDefaultComputedStyle ?

			// Use of this method is a temporary fix (more like optmization) until something better comes along,
			// since it was removed from specification and supported only in FF
			window.getDefaultComputedStyle( elem[ 0 ] ).display : jQuery.css( elem[ 0 ], "display" );

	// We don't have any data stored on the element,
	// so use "detach" method as fast way to get rid of the element
	elem.detach();

	return display;
}

/**
 * Try to determine the default display value of an element
 * @param {String} nodeName
 */
function defaultDisplay( nodeName ) {
	var doc = document,
		display = elemdisplay[ nodeName ];

	if ( !display ) {
		display = actualDisplay( nodeName, doc );

		// If the simple way fails, read from inside an iframe
		if ( display === "none" || !display ) {

			// Use the already-created iframe if possible
			iframe = (iframe || jQuery( "<iframe frameborder='0' width='0' height='0'/>" )).appendTo( doc.documentElement );

			// Always write a new HTML skeleton so Webkit and Firefox don't choke on reuse
			doc = ( iframe[ 0 ].contentWindow || iframe[ 0 ].contentDocument ).document;

			// Support: IE
			doc.write();
			doc.close();

			display = actualDisplay( nodeName, doc );
			iframe.detach();
		}

		// Store the correct default display
		elemdisplay[ nodeName ] = display;
	}

	return display;
}


(function() {
	var a, shrinkWrapBlocksVal,
		div = document.createElement( "div" ),
		divReset =
			"-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;" +
			"display:block;padding:0;margin:0;border:0";

	// Setup
	div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
	a = div.getElementsByTagName( "a" )[ 0 ];

	a.style.cssText = "float:left;opacity:.5";

	// Make sure that element opacity exists
	// (IE uses filter instead)
	// Use a regex to work around a WebKit issue. See #5145
	support.opacity = /^0.5/.test( a.style.opacity );

	// Verify style float existence
	// (IE uses styleFloat instead of cssFloat)
	support.cssFloat = !!a.style.cssFloat;

	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	// Null elements to avoid leaks in IE.
	a = div = null;

	support.shrinkWrapBlocks = function() {
		var body, container, div, containerStyles;

		if ( shrinkWrapBlocksVal == null ) {
			body = document.getElementsByTagName( "body" )[ 0 ];
			if ( !body ) {
				// Test fired too early or in an unsupported environment, exit.
				return;
			}

			containerStyles = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px";
			container = document.createElement( "div" );
			div = document.createElement( "div" );

			body.appendChild( container ).appendChild( div );

			// Will be changed later if needed.
			shrinkWrapBlocksVal = false;

			if ( typeof div.style.zoom !== strundefined ) {
				// Support: IE6
				// Check if elements with layout shrink-wrap their children
				div.style.cssText = divReset + ";width:1px;padding:1px;zoom:1";
				div.innerHTML = "<div></div>";
				div.firstChild.style.width = "5px";
				shrinkWrapBlocksVal = div.offsetWidth !== 3;
			}

			body.removeChild( container );

			// Null elements to avoid leaks in IE.
			body = container = div = null;
		}

		return shrinkWrapBlocksVal;
	};

})();
var rmargin = (/^margin/);

var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );



var getStyles, curCSS,
	rposition = /^(top|right|bottom|left)$/;

if ( window.getComputedStyle ) {
	getStyles = function( elem ) {
		return elem.ownerDocument.defaultView.getComputedStyle( elem, null );
	};

	curCSS = function( elem, name, computed ) {
		var width, minWidth, maxWidth, ret,
			style = elem.style;

		computed = computed || getStyles( elem );

		// getPropertyValue is only needed for .css('filter') in IE9, see #12537
		ret = computed ? computed.getPropertyValue( name ) || computed[ name ] : undefined;

		if ( computed ) {

			if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
				ret = jQuery.style( elem, name );
			}

			// A tribute to the "awesome hack by Dean Edwards"
			// Chrome < 17 and Safari 5.0 uses "computed value" instead of "used value" for margin-right
			// Safari 5.1.7 (at least) returns percentage for a larger set of values, but width seems to be reliably pixels
			// this is against the CSSOM draft spec: http://dev.w3.org/csswg/cssom/#resolved-values
			if ( rnumnonpx.test( ret ) && rmargin.test( name ) ) {

				// Remember the original values
				width = style.width;
				minWidth = style.minWidth;
				maxWidth = style.maxWidth;

				// Put in the new values to get a computed value out
				style.minWidth = style.maxWidth = style.width = ret;
				ret = computed.width;

				// Revert the changed values
				style.width = width;
				style.minWidth = minWidth;
				style.maxWidth = maxWidth;
			}
		}

		// Support: IE
		// IE returns zIndex value as an integer.
		return ret === undefined ?
			ret :
			ret + "";
	};
} else if ( document.documentElement.currentStyle ) {
	getStyles = function( elem ) {
		return elem.currentStyle;
	};

	curCSS = function( elem, name, computed ) {
		var left, rs, rsLeft, ret,
			style = elem.style;

		computed = computed || getStyles( elem );
		ret = computed ? computed[ name ] : undefined;

		// Avoid setting ret to empty string here
		// so we don't default to auto
		if ( ret == null && style && style[ name ] ) {
			ret = style[ name ];
		}

		// From the awesome hack by Dean Edwards
		// http://erik.eae.net/archives/2007/07/27/18.54.15/#comment-102291

		// If we're not dealing with a regular pixel number
		// but a number that has a weird ending, we need to convert it to pixels
		// but not position css attributes, as those are proportional to the parent element instead
		// and we can't measure the parent instead because it might trigger a "stacking dolls" problem
		if ( rnumnonpx.test( ret ) && !rposition.test( name ) ) {

			// Remember the original values
			left = style.left;
			rs = elem.runtimeStyle;
			rsLeft = rs && rs.left;

			// Put in the new values to get a computed value out
			if ( rsLeft ) {
				rs.left = elem.currentStyle.left;
			}
			style.left = name === "fontSize" ? "1em" : ret;
			ret = style.pixelLeft + "px";

			// Revert the changed values
			style.left = left;
			if ( rsLeft ) {
				rs.left = rsLeft;
			}
		}

		// Support: IE
		// IE returns zIndex value as an integer.
		return ret === undefined ?
			ret :
			ret + "" || "auto";
	};
}




function addGetHookIf( conditionFn, hookFn ) {
	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			var condition = conditionFn();

			if ( condition == null ) {
				// The test was not ready at this point; screw the hook this time
				// but check again when needed next time.
				return;
			}

			if ( condition ) {
				// Hook not needed (or it's not possible to use it due to missing dependency),
				// remove it.
				// Since there are no other hooks for marginRight, remove the whole object.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.

			return (this.get = hookFn).apply( this, arguments );
		}
	};
}


(function() {
	var a, reliableHiddenOffsetsVal, boxSizingVal, boxSizingReliableVal,
		pixelPositionVal, reliableMarginRightVal,
		div = document.createElement( "div" ),
		containerStyles = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px",
		divReset =
			"-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;" +
			"display:block;padding:0;margin:0;border:0";

	// Setup
	div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
	a = div.getElementsByTagName( "a" )[ 0 ];

	a.style.cssText = "float:left;opacity:.5";

	// Make sure that element opacity exists
	// (IE uses filter instead)
	// Use a regex to work around a WebKit issue. See #5145
	support.opacity = /^0.5/.test( a.style.opacity );

	// Verify style float existence
	// (IE uses styleFloat instead of cssFloat)
	support.cssFloat = !!a.style.cssFloat;

	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	// Null elements to avoid leaks in IE.
	a = div = null;

	jQuery.extend(support, {
		reliableHiddenOffsets: function() {
			if ( reliableHiddenOffsetsVal != null ) {
				return reliableHiddenOffsetsVal;
			}

			var container, tds, isSupported,
				div = document.createElement( "div" ),
				body = document.getElementsByTagName( "body" )[ 0 ];

			if ( !body ) {
				// Return for frameset docs that don't have a body
				return;
			}

			// Setup
			div.setAttribute( "className", "t" );
			div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";

			container = document.createElement( "div" );
			container.style.cssText = containerStyles;

			body.appendChild( container ).appendChild( div );

			// Support: IE8
			// Check if table cells still have offsetWidth/Height when they are set
			// to display:none and there are still other visible table cells in a
			// table row; if so, offsetWidth/Height are not reliable for use when
			// determining if an element has been hidden directly using
			// display:none (it is still safe to use offsets if a parent element is
			// hidden; don safety goggles and see bug #4512 for more information).
			div.innerHTML = "<table><tr><td></td><td>t</td></tr></table>";
			tds = div.getElementsByTagName( "td" );
			tds[ 0 ].style.cssText = "padding:0;margin:0;border:0;display:none";
			isSupported = ( tds[ 0 ].offsetHeight === 0 );

			tds[ 0 ].style.display = "";
			tds[ 1 ].style.display = "none";

			// Support: IE8
			// Check if empty table cells still have offsetWidth/Height
			reliableHiddenOffsetsVal = isSupported && ( tds[ 0 ].offsetHeight === 0 );

			body.removeChild( container );

			// Null elements to avoid leaks in IE.
			div = body = null;

			return reliableHiddenOffsetsVal;
		},

		boxSizing: function() {
			if ( boxSizingVal == null ) {
				computeStyleTests();
			}
			return boxSizingVal;
		},

		boxSizingReliable: function() {
			if ( boxSizingReliableVal == null ) {
				computeStyleTests();
			}
			return boxSizingReliableVal;
		},

		pixelPosition: function() {
			if ( pixelPositionVal == null ) {
				computeStyleTests();
			}
			return pixelPositionVal;
		},

		reliableMarginRight: function() {
			var body, container, div, marginDiv;

			// Use window.getComputedStyle because jsdom on node.js will break without it.
			if ( reliableMarginRightVal == null && window.getComputedStyle ) {
				body = document.getElementsByTagName( "body" )[ 0 ];
				if ( !body ) {
					// Test fired too early or in an unsupported environment, exit.
					return;
				}

				container = document.createElement( "div" );
				div = document.createElement( "div" );
				container.style.cssText = containerStyles;

				body.appendChild( container ).appendChild( div );

				// Check if div with explicit width and no margin-right incorrectly
				// gets computed margin-right based on width of container. (#3333)
				// Fails in WebKit before Feb 2011 nightlies
				// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
				marginDiv = div.appendChild( document.createElement( "div" ) );
				marginDiv.style.cssText = div.style.cssText = divReset;
				marginDiv.style.marginRight = marginDiv.style.width = "0";
				div.style.width = "1px";

				reliableMarginRightVal =
					!parseFloat( ( window.getComputedStyle( marginDiv, null ) || {} ).marginRight );

				body.removeChild( container );
			}

			return reliableMarginRightVal;
		}
	});

	function computeStyleTests() {
		var container, div,
			body = document.getElementsByTagName( "body" )[ 0 ];

		if ( !body ) {
			// Test fired too early or in an unsupported environment, exit.
			return;
		}

		container = document.createElement( "div" );
		div = document.createElement( "div" );
		container.style.cssText = containerStyles;

		body.appendChild( container ).appendChild( div );

		div.style.cssText =
			"-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;" +
				"position:absolute;display:block;padding:1px;border:1px;width:4px;" +
				"margin-top:1%;top:1%";

		// Workaround failing boxSizing test due to offsetWidth returning wrong value
		// with some non-1 values of body zoom, ticket #13543
		jQuery.swap( body, body.style.zoom != null ? { zoom: 1 } : {}, function() {
			boxSizingVal = div.offsetWidth === 4;
		});

		// Will be changed later if needed.
		boxSizingReliableVal = true;
		pixelPositionVal = false;
		reliableMarginRightVal = true;

		// Use window.getComputedStyle because jsdom on node.js will break without it.
		if ( window.getComputedStyle ) {
			pixelPositionVal = ( window.getComputedStyle( div, null ) || {} ).top !== "1%";
			boxSizingReliableVal =
				( window.getComputedStyle( div, null ) || { width: "4px" } ).width === "4px";
		}

		body.removeChild( container );

		// Null elements to avoid leaks in IE.
		div = body = null;
	}

})();


// A method for quickly swapping in/out CSS properties to get correct calculations.
jQuery.swap = function( elem, options, callback, args ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};


var
		ralpha = /alpha\([^)]*\)/i,
	ropacity = /opacity\s*=\s*([^)]*)/,

	// swappable if display is none or starts with table except "table", "table-cell", or "table-caption"
	// see here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,
	rnumsplit = new RegExp( "^(" + pnum + ")(.*)$", "i" ),
	rrelNum = new RegExp( "^([+-])=(" + pnum + ")", "i" ),

	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: 0,
		fontWeight: 400
	},

	cssPrefixes = [ "Webkit", "O", "Moz", "ms" ];


// return a css property mapped to a potentially vendor prefixed property
function vendorPropName( style, name ) {

	// shortcut for names that are not vendor prefixed
	if ( name in style ) {
		return name;
	}

	// check for vendor prefixed names
	var capName = name.charAt(0).toUpperCase() + name.slice(1),
		origName = name,
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in style ) {
			return name;
		}
	}

	return origName;
}

function showHide( elements, show ) {
	var display, elem, hidden,
		values = [],
		index = 0,
		length = elements.length;

	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		values[ index ] = jQuery._data( elem, "olddisplay" );
		display = elem.style.display;
		if ( show ) {
			// Reset the inline display of this element to learn if it is
			// being hidden by cascaded rules or not
			if ( !values[ index ] && display === "none" ) {
				elem.style.display = "";
			}

			// Set elements which have been overridden with display: none
			// in a stylesheet to whatever the default browser style is
			// for such an element
			if ( elem.style.display === "" && isHidden( elem ) ) {
				values[ index ] = jQuery._data( elem, "olddisplay", defaultDisplay(elem.nodeName) );
			}
		} else {

			if ( !values[ index ] ) {
				hidden = isHidden( elem );

				if ( display && display !== "none" || !hidden ) {
					jQuery._data( elem, "olddisplay", hidden ? display : jQuery.css( elem, "display" ) );
				}
			}
		}
	}

	// Set the display of most of the elements in a second loop
	// to avoid the constant reflow
	for ( index = 0; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}
		if ( !show || elem.style.display === "none" || elem.style.display === "" ) {
			elem.style.display = show ? values[ index ] || "" : "none";
		}
	}

	return elements;
}

function setPositiveNumber( elem, value, subtract ) {
	var matches = rnumsplit.exec( value );
	return matches ?
		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 1 ] - ( subtract || 0 ) ) + ( matches[ 2 ] || "px" ) :
		value;
}

function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
	var i = extra === ( isBorderBox ? "border" : "content" ) ?
		// If we already have the right measurement, avoid augmentation
		4 :
		// Otherwise initialize for horizontal or vertical properties
		name === "width" ? 1 : 0,

		val = 0;

	for ( ; i < 4; i += 2 ) {
		// both box models exclude margin, so add it if we want it
		if ( extra === "margin" ) {
			val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
		}

		if ( isBorderBox ) {
			// border-box includes padding, so remove it if we want content
			if ( extra === "content" ) {
				val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// at this point, extra isn't border nor margin, so remove border
			if ( extra !== "margin" ) {
				val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		} else {
			// at this point, extra isn't content, so add padding
			val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// at this point, extra isn't content nor padding, so add border
			if ( extra !== "padding" ) {
				val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	return val;
}

function getWidthOrHeight( elem, name, extra ) {

	// Start with offset property, which is equivalent to the border-box value
	var valueIsBorderBox = true,
		val = name === "width" ? elem.offsetWidth : elem.offsetHeight,
		styles = getStyles( elem ),
		isBorderBox = support.boxSizing() && jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

	// some non-html elements return undefined for offsetWidth, so check for null/undefined
	// svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
	// MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
	if ( val <= 0 || val == null ) {
		// Fall back to computed then uncomputed css if necessary
		val = curCSS( elem, name, styles );
		if ( val < 0 || val == null ) {
			val = elem.style[ name ];
		}

		// Computed unit is not pixels. Stop here and return.
		if ( rnumnonpx.test(val) ) {
			return val;
		}

		// we need the check for style in case a browser which returns unreliable values
		// for getComputedStyle silently falls back to the reliable elem.style
		valueIsBorderBox = isBorderBox && ( support.boxSizingReliable() || val === elem.style[ name ] );

		// Normalize "", auto, and prepare for extra
		val = parseFloat( val ) || 0;
	}

	// use the active box-sizing model to add/subtract irrelevant styles
	return ( val +
		augmentWidthOrHeight(
			elem,
			name,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles
		)
	) + "px";
}

jQuery.extend({
	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {
					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"columnCount": true,
		"fillOpacity": true,
		"fontWeight": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {
		// normalize float css property
		"float": support.cssFloat ? "cssFloat" : "styleFloat"
	},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {
		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = jQuery.camelCase( name ),
			style = elem.style;

		name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( style, origName ) );

		// gets hook for the prefixed version
		// followed by the unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// convert relative number strings (+= or -=) to relative numbers. #7345
			if ( type === "string" && (ret = rrelNum.exec( value )) ) {
				value = ( ret[1] + 1 ) * ret[2] + parseFloat( jQuery.css( elem, name ) );
				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set. See: #7116
			if ( value == null || value !== value ) {
				return;
			}

			// If a number was passed in, add 'px' to the (except for certain CSS properties)
			if ( type === "number" && !jQuery.cssNumber[ origName ] ) {
				value += "px";
			}

			// Fixes #8908, it can be done more correctly by specifing setters in cssHooks,
			// but it would mean to define eight (for every problematic property) identical functions
			if ( !support.clearCloneStyle && value === "" && name.indexOf("background") === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !("set" in hooks) || (value = hooks.set( elem, value, extra )) !== undefined ) {

				// Support: IE
				// Swallow errors from 'invalid' CSS values (#5509)
				try {
					// Support: Chrome, Safari
					// Setting style to blank string required to delete "style: x !important;"
					style[ name ] = "";
					style[ name ] = value;
				} catch(e) {}
			}

		} else {
			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks && (ret = hooks.get( elem, false, extra )) !== undefined ) {
				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var num, val, hooks,
			origName = jQuery.camelCase( name );

		// Make sure that we're working with the right name
		name = jQuery.cssProps[ origName ] || ( jQuery.cssProps[ origName ] = vendorPropName( elem.style, origName ) );

		// gets hook for the prefixed version
		// followed by the unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		//convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Return, converting to number if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || jQuery.isNumeric( num ) ? num || 0 : val;
		}
		return val;
	}
});

jQuery.each([ "height", "width" ], function( i, name ) {
	jQuery.cssHooks[ name ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {
				// certain elements can have dimension info if we invisibly show them
				// however, it must have a current display style that would benefit from this
				return elem.offsetWidth === 0 && rdisplayswap.test( jQuery.css( elem, "display" ) ) ?
					jQuery.swap( elem, cssShow, function() {
						return getWidthOrHeight( elem, name, extra );
					}) :
					getWidthOrHeight( elem, name, extra );
			}
		},

		set: function( elem, value, extra ) {
			var styles = extra && getStyles( elem );
			return setPositiveNumber( elem, value, extra ?
				augmentWidthOrHeight(
					elem,
					name,
					extra,
					support.boxSizing() && jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
					styles
				) : 0
			);
		}
	};
});

if ( !support.opacity ) {
	jQuery.cssHooks.opacity = {
		get: function( elem, computed ) {
			// IE uses filters for opacity
			return ropacity.test( (computed && elem.currentStyle ? elem.currentStyle.filter : elem.style.filter) || "" ) ?
				( 0.01 * parseFloat( RegExp.$1 ) ) + "" :
				computed ? "1" : "";
		},

		set: function( elem, value ) {
			var style = elem.style,
				currentStyle = elem.currentStyle,
				opacity = jQuery.isNumeric( value ) ? "alpha(opacity=" + value * 100 + ")" : "",
				filter = currentStyle && currentStyle.filter || style.filter || "";

			// IE has trouble with opacity if it does not have layout
			// Force it by setting the zoom level
			style.zoom = 1;

			// if setting opacity to 1, and no other filters exist - attempt to remove filter attribute #6652
			// if value === "", then remove inline opacity #12685
			if ( ( value >= 1 || value === "" ) &&
					jQuery.trim( filter.replace( ralpha, "" ) ) === "" &&
					style.removeAttribute ) {

				// Setting style.filter to null, "" & " " still leave "filter:" in the cssText
				// if "filter:" is present at all, clearType is disabled, we want to avoid this
				// style.removeAttribute is IE Only, but so apparently is this code path...
				style.removeAttribute( "filter" );

				// if there is no filter style applied in a css rule or unset inline opacity, we are done
				if ( value === "" || currentStyle && !currentStyle.filter ) {
					return;
				}
			}

			// otherwise, set new filter values
			style.filter = ralpha.test( filter ) ?
				filter.replace( ralpha, opacity ) :
				filter + " " + opacity;
		}
	};
}

jQuery.cssHooks.marginRight = addGetHookIf( support.reliableMarginRight,
	function( elem, computed ) {
		if ( computed ) {
			// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
			// Work around by temporarily setting element display to inline-block
			return jQuery.swap( elem, { "display": "inline-block" },
				curCSS, [ elem, "marginRight" ] );
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each({
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// assumes a single number if not a string
				parts = typeof value === "string" ? value.split(" ") : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( !rmargin.test( prefix ) ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
});

jQuery.fn.extend({
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( jQuery.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	},
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each(function() {
			if ( isHidden( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		});
	}
});


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || "swing";
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			if ( tween.elem[ tween.prop ] != null &&
				(!tween.elem.style || tween.elem.style[ tween.prop ] == null) ) {
				return tween.elem[ tween.prop ];
			}

			// passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails
			// so, simple values such as "10px" are parsed to Float.
			// complex values such as "rotate(1rad)" are returned as is.
			result = jQuery.css( tween.elem, tween.prop, "" );
			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {
			// use step hook for back compat - use cssHook if its there - use .style if its
			// available and use plain properties where available
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.style && ( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null || jQuery.cssHooks[ tween.prop ] ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE <=9
// Panic based approach to setting things on disconnected nodes

Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	}
};

jQuery.fx = Tween.prototype.init;

// Back Compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, timerId,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rfxnum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" ),
	rrun = /queueHooks$/,
	animationPrefilters = [ defaultPrefilter ],
	tweeners = {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value ),
				target = tween.cur(),
				parts = rfxnum.exec( value ),
				unit = parts && parts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

				// Starting value computation is required for potential unit mismatches
				start = ( jQuery.cssNumber[ prop ] || unit !== "px" && +target ) &&
					rfxnum.exec( jQuery.css( tween.elem, prop ) ),
				scale = 1,
				maxIterations = 20;

			if ( start && start[ 3 ] !== unit ) {
				// Trust units reported by jQuery.css
				unit = unit || start[ 3 ];

				// Make sure we update the tween properties later on
				parts = parts || [];

				// Iteratively approximate from a nonzero starting point
				start = +target || 1;

				do {
					// If previous iteration zeroed out, double until we get *something*
					// Use a string for doubling factor so we don't accidentally see scale as unchanged below
					scale = scale || ".5";

					// Adjust and apply
					start = start / scale;
					jQuery.style( tween.elem, prop, start + unit );

				// Update scale, tolerating zero or NaN from tween.cur()
				// And breaking the loop if scale is unchanged or perfect, or if we've just had enough
				} while ( scale !== (scale = tween.cur() / target) && scale !== 1 && --maxIterations );
			}

			// Update tween properties
			if ( parts ) {
				start = tween.start = +start || +target || 0;
				tween.unit = unit;
				// If a +=/-= token was provided, we're doing a relative animation
				tween.end = parts[ 1 ] ?
					start + ( parts[ 1 ] + 1 ) * parts[ 2 ] :
					+parts[ 2 ];
			}

			return tween;
		} ]
	};

// Animations created synchronously will run synchronously
function createFxNow() {
	setTimeout(function() {
		fxNow = undefined;
	});
	return ( fxNow = jQuery.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		attrs = { height: type },
		i = 0;

	// if we include width, step value is 1 to do all cssExpand values,
	// if we don't include width, step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4 ; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( tweeners[ prop ] || [] ).concat( tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( (tween = collection[ index ].call( animation, prop, value )) ) {

			// we're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	/* jshint validthis: true */
	var prop, value, toggle, tween, hooks, oldfire, display, dDisplay,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHidden( elem ),
		dataShow = jQuery._data( elem, "fxshow" );

	// handle queue: false promises
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always(function() {
			// doing this makes sure that the complete handler will be called
			// before this completes
			anim.always(function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			});
		});
	}

	// height/width overflow pass
	if ( elem.nodeType === 1 && ( "height" in props || "width" in props ) ) {
		// Make sure that nothing sneaks out
		// Record all 3 overflow attributes because IE does not
		// change the overflow attribute when overflowX and
		// overflowY are set to the same value
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Set display property to inline-block for height/width
		// animations on inline elements that are having width/height animated
		display = jQuery.css( elem, "display" );
		dDisplay = defaultDisplay( elem.nodeName );
		if ( display === "none" ) {
			display = dDisplay;
		}
		if ( display === "inline" &&
				jQuery.css( elem, "float" ) === "none" ) {

			// inline-level elements accept inline-block;
			// block-level elements need to be inline with layout
			if ( !support.inlineBlockNeedsLayout || dDisplay === "inline" ) {
				style.display = "inline-block";
			} else {
				style.zoom = 1;
			}
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		if ( !support.shrinkWrapBlocks() ) {
			anim.always(function() {
				style.overflow = opts.overflow[ 0 ];
				style.overflowX = opts.overflow[ 1 ];
				style.overflowY = opts.overflow[ 2 ];
			});
		}
	}

	// show/hide pass
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.exec( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// If there is dataShow left over from a stopped hide or show and we are going to proceed with show, we should pretend to be hidden
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );
		}
	}

	if ( !jQuery.isEmptyObject( orig ) ) {
		if ( dataShow ) {
			if ( "hidden" in dataShow ) {
				hidden = dataShow.hidden;
			}
		} else {
			dataShow = jQuery._data( elem, "fxshow", {} );
		}

		// store state if its toggle - enables .stop().toggle() to "reverse"
		if ( toggle ) {
			dataShow.hidden = !hidden;
		}
		if ( hidden ) {
			jQuery( elem ).show();
		} else {
			anim.done(function() {
				jQuery( elem ).hide();
			});
		}
		anim.done(function() {
			var prop;
			jQuery._removeData( elem, "fxshow" );
			for ( prop in orig ) {
				jQuery.style( elem, prop, orig[ prop ] );
			}
		});
		for ( prop in orig ) {
			tween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );

			if ( !( prop in dataShow ) ) {
				dataShow[ prop ] = tween.start;
				if ( hidden ) {
					tween.end = tween.start;
					tween.start = prop === "width" || prop === "height" ? 1 : 0;
				}
			}
		}
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = jQuery.camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( jQuery.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// not quite $.extend, this wont overwrite keys already present.
			// also - reusing 'index' from above because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = animationPrefilters.length,
		deferred = jQuery.Deferred().always( function() {
			// don't match elem in the :animated selector
			delete tick.elem;
		}),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),
				// archaic crash bug won't allow us to use 1 - ( 0.5 || 0 ) (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length ; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ]);

			if ( percent < 1 && length ) {
				return remaining;
			} else {
				deferred.resolveWith( elem, [ animation ] );
				return false;
			}
		},
		animation = deferred.promise({
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, { specialEasing: {} }, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,
					// if we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length ; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// resolve when we played the last frame
				// otherwise, reject
				if ( gotoEnd ) {
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		}),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length ; index++ ) {
		result = animationPrefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( jQuery.isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		})
	);

	// attach callbacks from options
	return animation.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );
}

jQuery.Animation = jQuery.extend( Animation, {
	tweener: function( props, callback ) {
		if ( jQuery.isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.split(" ");
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length ; index++ ) {
			prop = props[ index ];
			tweeners[ prop ] = tweeners[ prop ] || [];
			tweeners[ prop ].unshift( callback );
		}
	},

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			animationPrefilters.unshift( callback );
		} else {
			animationPrefilters.push( callback );
		}
	}
});

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			jQuery.isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
	};

	opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ? opt.duration :
		opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[ opt.duration ] : jQuery.fx.speeds._default;

	// normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( jQuery.isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend({
	fadeTo: function( speed, to, easing, callback ) {

		// show any hidden elements after setting opacity to 0
		return this.filter( isHidden ).css( "opacity", 0 ).show()

			// animate to the value specified
			.end().animate({ opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {
				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || jQuery._data( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue && type !== false ) {
			this.queue( type || "fx", [] );
		}

		return this.each(function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = jQuery._data( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && (type == null || timers[ index ].queue === type) ) {
					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// start the next in the queue if the last step wasn't forced
			// timers currently will call their complete callbacks, which will dequeue
			// but only if they were gotoEnd
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		});
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each(function() {
			var index,
				data = jQuery._data( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// enable finishing flag on private data
			data.finish = true;

			// empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// turn off finishing flag
			delete data.finish;
		});
	}
});

jQuery.each([ "toggle", "show", "hide" ], function( i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
});

// Generate shortcuts for custom animations
jQuery.each({
	slideDown: genFx("show"),
	slideUp: genFx("hide"),
	slideToggle: genFx("toggle"),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
});

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		timers = jQuery.timers,
		i = 0;

	fxNow = jQuery.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];
		// Checks the timer has not already been removed
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	if ( timer() ) {
		jQuery.fx.start();
	} else {
		jQuery.timers.pop();
	}
};

jQuery.fx.interval = 13;

jQuery.fx.start = function() {
	if ( !timerId ) {
		timerId = setInterval( jQuery.fx.tick, jQuery.fx.interval );
	}
};

jQuery.fx.stop = function() {
	clearInterval( timerId );
	timerId = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,
	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = setTimeout( next, time );
		hooks.stop = function() {
			clearTimeout( timeout );
		};
	});
};


(function() {
	var a, input, select, opt,
		div = document.createElement("div" );

	// Setup
	div.setAttribute( "className", "t" );
	div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
	a = div.getElementsByTagName("a")[ 0 ];

	// First batch of tests.
	select = document.createElement("select");
	opt = select.appendChild( document.createElement("option") );
	input = div.getElementsByTagName("input")[ 0 ];

	a.style.cssText = "top:1px";

	// Test setAttribute on camelCase class. If it works, we need attrFixes when doing get/setAttribute (ie6/7)
	support.getSetAttribute = div.className !== "t";

	// Get the style information from getAttribute
	// (IE uses .cssText instead)
	support.style = /top/.test( a.getAttribute("style") );

	// Make sure that URLs aren't manipulated
	// (IE normalizes it by default)
	support.hrefNormalized = a.getAttribute("href") === "/a";

	// Check the default checkbox/radio value ("" on WebKit; "on" elsewhere)
	support.checkOn = !!input.value;

	// Make sure that a selected-by-default option has a working selected property.
	// (WebKit defaults to false instead of true, IE too, if it's in an optgroup)
	support.optSelected = opt.selected;

	// Tests for enctype support on a form (#6743)
	support.enctype = !!document.createElement("form").enctype;

	// Make sure that the options inside disabled selects aren't marked as disabled
	// (WebKit marks them as disabled)
	select.disabled = true;
	support.optDisabled = !opt.disabled;

	// Support: IE8 only
	// Check if we can trust getAttribute("value")
	input = document.createElement( "input" );
	input.setAttribute( "value", "" );
	support.input = input.getAttribute( "value" ) === "";

	// Check if an input maintains its value after becoming a radio
	input.value = "t";
	input.setAttribute( "type", "radio" );
	support.radioValue = input.value === "t";

	// Null elements to avoid leaks in IE.
	a = input = select = opt = div = null;
})();


var rreturn = /\r/g;

jQuery.fn.extend({
	val: function( value ) {
		var hooks, ret, isFunction,
			elem = this[0];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] || jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks && "get" in hooks && (ret = hooks.get( elem, "value" )) !== undefined ) {
					return ret;
				}

				ret = elem.value;

				return typeof ret === "string" ?
					// handle most common string cases
					ret.replace(rreturn, "") :
					// handle cases where value is null/undef or number
					ret == null ? "" : ret;
			}

			return;
		}

		isFunction = jQuery.isFunction( value );

		return this.each(function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( isFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";
			} else if ( typeof val === "number" ) {
				val += "";
			} else if ( jQuery.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				});
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !("set" in hooks) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		});
	}
});

jQuery.extend({
	valHooks: {
		option: {
			get: function( elem ) {
				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :
					jQuery.text( elem );
			}
		},
		select: {
			get: function( elem ) {
				var value, option,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one" || index < 0,
					values = one ? null : [],
					max = one ? index + 1 : options.length,
					i = index < 0 ?
						max :
						one ? index : 0;

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// oldIE doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&
							// Don't return options that are disabled or in a disabled optgroup
							( support.optDisabled ? !option.disabled : option.getAttribute("disabled") === null ) &&
							( !option.parentNode.disabled || !jQuery.nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];

					if ( jQuery.inArray( jQuery.valHooks.option.get( option ), values ) >= 0 ) {

						// Support: IE6
						// When new option element is added to select box we need to
						// force reflow of newly added node in order to workaround delay
						// of initialization properties
						try {
							option.selected = optionSet = true;

						} catch ( _ ) {

							// Will be executed only in IE6
							option.scrollHeight;
						}

					} else {
						option.selected = false;
					}
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}

				return options;
			}
		}
	}
});

// Radios and checkboxes getter/setter
jQuery.each([ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( jQuery.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery(elem).val(), value ) >= 0 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			// Support: Webkit
			// "" is returned instead of "on" if a value isn't specified
			return elem.getAttribute("value") === null ? "on" : elem.value;
		};
	}
});




var nodeHook, boolHook,
	attrHandle = jQuery.expr.attrHandle,
	ruseDefault = /^(?:checked|selected)$/i,
	getSetAttribute = support.getSetAttribute,
	getSetInput = support.input;

jQuery.fn.extend({
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each(function() {
			jQuery.removeAttr( this, name );
		});
	}
});

jQuery.extend({
	attr: function( elem, name, value ) {
		var hooks, ret,
			nType = elem.nodeType;

		// don't get/set attributes on text, comment and attribute nodes
		if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === strundefined ) {
			return jQuery.prop( elem, name, value );
		}

		// All attributes are lowercase
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			name = name.toLowerCase();
			hooks = jQuery.attrHooks[ name ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : nodeHook );
		}

		if ( value !== undefined ) {

			if ( value === null ) {
				jQuery.removeAttr( elem, name );

			} else if ( hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ) {
				return ret;

			} else {
				elem.setAttribute( name, value + "" );
				return value;
			}

		} else if ( hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ) {
			return ret;

		} else {
			ret = jQuery.find.attr( elem, name );

			// Non-existent attributes return null, we normalize to undefined
			return ret == null ?
				undefined :
				ret;
		}
	},

	removeAttr: function( elem, value ) {
		var name, propName,
			i = 0,
			attrNames = value && value.match( rnotwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( (name = attrNames[i++]) ) {
				propName = jQuery.propFix[ name ] || name;

				// Boolean attributes get special treatment (#10870)
				if ( jQuery.expr.match.bool.test( name ) ) {
					// Set corresponding property to false
					if ( getSetInput && getSetAttribute || !ruseDefault.test( name ) ) {
						elem[ propName ] = false;
					// Support: IE<9
					// Also clear defaultChecked/defaultSelected (if appropriate)
					} else {
						elem[ jQuery.camelCase( "default-" + name ) ] =
							elem[ propName ] = false;
					}

				// See #9699 for explanation of this approach (setting first, then removal)
				} else {
					jQuery.attr( elem, name, "" );
				}

				elem.removeAttribute( getSetAttribute ? name : propName );
			}
		}
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" && jQuery.nodeName(elem, "input") ) {
					// Setting the type on a radio button after the value resets the value in IE6-9
					// Reset value to default in case type is set after value during creation
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	}
});

// Hook for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {
			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else if ( getSetInput && getSetAttribute || !ruseDefault.test( name ) ) {
			// IE<8 needs the *property* name
			elem.setAttribute( !getSetAttribute && jQuery.propFix[ name ] || name, name );

		// Use defaultChecked and defaultSelected for oldIE
		} else {
			elem[ jQuery.camelCase( "default-" + name ) ] = elem[ name ] = true;
		}

		return name;
	}
};

// Retrieve booleans specially
jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {

	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = getSetInput && getSetAttribute || !ruseDefault.test( name ) ?
		function( elem, name, isXML ) {
			var ret, handle;
			if ( !isXML ) {
				// Avoid an infinite loop by temporarily removing this function from the getter
				handle = attrHandle[ name ];
				attrHandle[ name ] = ret;
				ret = getter( elem, name, isXML ) != null ?
					name.toLowerCase() :
					null;
				attrHandle[ name ] = handle;
			}
			return ret;
		} :
		function( elem, name, isXML ) {
			if ( !isXML ) {
				return elem[ jQuery.camelCase( "default-" + name ) ] ?
					name.toLowerCase() :
					null;
			}
		};
});

// fix oldIE attroperties
if ( !getSetInput || !getSetAttribute ) {
	jQuery.attrHooks.value = {
		set: function( elem, value, name ) {
			if ( jQuery.nodeName( elem, "input" ) ) {
				// Does not return so that setAttribute is also used
				elem.defaultValue = value;
			} else {
				// Use nodeHook if defined (#1954); otherwise setAttribute is fine
				return nodeHook && nodeHook.set( elem, value, name );
			}
		}
	};
}

// IE6/7 do not support getting/setting some attributes with get/setAttribute
if ( !getSetAttribute ) {

	// Use this for any attribute in IE6/7
	// This fixes almost every IE6/7 issue
	nodeHook = {
		set: function( elem, value, name ) {
			// Set the existing or create a new attribute node
			var ret = elem.getAttributeNode( name );
			if ( !ret ) {
				elem.setAttributeNode(
					(ret = elem.ownerDocument.createAttribute( name ))
				);
			}

			ret.value = value += "";

			// Break association with cloned elements by also using setAttribute (#9646)
			if ( name === "value" || value === elem.getAttribute( name ) ) {
				return value;
			}
		}
	};

	// Some attributes are constructed with empty-string values when not defined
	attrHandle.id = attrHandle.name = attrHandle.coords =
		function( elem, name, isXML ) {
			var ret;
			if ( !isXML ) {
				return (ret = elem.getAttributeNode( name )) && ret.value !== "" ?
					ret.value :
					null;
			}
		};

	// Fixing value retrieval on a button requires this module
	jQuery.valHooks.button = {
		get: function( elem, name ) {
			var ret = elem.getAttributeNode( name );
			if ( ret && ret.specified ) {
				return ret.value;
			}
		},
		set: nodeHook.set
	};

	// Set contenteditable to false on removals(#10429)
	// Setting to empty string throws an error as an invalid value
	jQuery.attrHooks.contenteditable = {
		set: function( elem, value, name ) {
			nodeHook.set( elem, value === "" ? false : value, name );
		}
	};

	// Set width and height to auto instead of 0 on empty string( Bug #8150 )
	// This is for removals
	jQuery.each([ "width", "height" ], function( i, name ) {
		jQuery.attrHooks[ name ] = {
			set: function( elem, value ) {
				if ( value === "" ) {
					elem.setAttribute( name, "auto" );
					return value;
				}
			}
		};
	});
}

if ( !support.style ) {
	jQuery.attrHooks.style = {
		get: function( elem ) {
			// Return undefined in the case of empty string
			// Note: IE uppercases css property names, but if we were to .toLowerCase()
			// .cssText, that would destroy case senstitivity in URL's, like in "background"
			return elem.style.cssText || undefined;
		},
		set: function( elem, value ) {
			return ( elem.style.cssText = value + "" );
		}
	};
}




var rfocusable = /^(?:input|select|textarea|button|object)$/i,
	rclickable = /^(?:a|area)$/i;

jQuery.fn.extend({
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		name = jQuery.propFix[ name ] || name;
		return this.each(function() {
			// try/catch handles cases where IE balks (such as removing a property on window)
			try {
				this[ name ] = undefined;
				delete this[ name ];
			} catch( e ) {}
		});
	}
});

jQuery.extend({
	propFix: {
		"for": "htmlFor",
		"class": "className"
	},

	prop: function( elem, name, value ) {
		var ret, hooks, notxml,
			nType = elem.nodeType;

		// don't get/set properties on text, comment and attribute nodes
		if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		notxml = nType !== 1 || !jQuery.isXMLDoc( elem );

		if ( notxml ) {
			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			return hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ?
				ret :
				( elem[ name ] = value );

		} else {
			return hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ?
				ret :
				elem[ name ];
		}
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {
				// elem.tabIndex doesn't always return the correct value when it hasn't been explicitly set
				// http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
				// Use proper attribute retrieval(#12072)
				var tabindex = jQuery.find.attr( elem, "tabindex" );

				return tabindex ?
					parseInt( tabindex, 10 ) :
					rfocusable.test( elem.nodeName ) || rclickable.test( elem.nodeName ) && elem.href ?
						0 :
						-1;
			}
		}
	}
});

// Some attributes require a special call on IE
// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !support.hrefNormalized ) {
	// href/src property should get the full normalized URL (#10299/#12915)
	jQuery.each([ "href", "src" ], function( i, name ) {
		jQuery.propHooks[ name ] = {
			get: function( elem ) {
				return elem.getAttribute( name, 4 );
			}
		};
	});
}

// Support: Safari, IE9+
// mis-reports the default selected property of an option
// Accessing the parent's selectedIndex property fixes it
if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {
			var parent = elem.parentNode;

			if ( parent ) {
				parent.selectedIndex;

				// Make sure that it also works with optgroups, see #5701
				if ( parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
			}
			return null;
		}
	};
}

jQuery.each([
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
});

// IE6/7 call enctype encoding
if ( !support.enctype ) {
	jQuery.propFix.enctype = "encoding";
}




var rclass = /[\t\r\n\f]/g;

jQuery.fn.extend({
	addClass: function( value ) {
		var classes, elem, cur, clazz, j, finalValue,
			i = 0,
			len = this.length,
			proceed = typeof value === "string" && value;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).addClass( value.call( this, j, this.className ) );
			});
		}

		if ( proceed ) {
			// The disjunction here is for better compressibility (see removeClass)
			classes = ( value || "" ).match( rnotwhite ) || [];

			for ( ; i < len; i++ ) {
				elem = this[ i ];
				cur = elem.nodeType === 1 && ( elem.className ?
					( " " + elem.className + " " ).replace( rclass, " " ) :
					" "
				);

				if ( cur ) {
					j = 0;
					while ( (clazz = classes[j++]) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// only assign if different to avoid unneeded rendering.
					finalValue = jQuery.trim( cur );
					if ( elem.className !== finalValue ) {
						elem.className = finalValue;
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, clazz, j, finalValue,
			i = 0,
			len = this.length,
			proceed = arguments.length === 0 || typeof value === "string" && value;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).removeClass( value.call( this, j, this.className ) );
			});
		}
		if ( proceed ) {
			classes = ( value || "" ).match( rnotwhite ) || [];

			for ( ; i < len; i++ ) {
				elem = this[ i ];
				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 && ( elem.className ?
					( " " + elem.className + " " ).replace( rclass, " " ) :
					""
				);

				if ( cur ) {
					j = 0;
					while ( (clazz = classes[j++]) ) {
						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) >= 0 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// only assign if different to avoid unneeded rendering.
					finalValue = value ? jQuery.trim( cur ) : "";
					if ( elem.className !== finalValue ) {
						elem.className = finalValue;
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value;

		if ( typeof stateVal === "boolean" && type === "string" ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( i ) {
				jQuery( this ).toggleClass( value.call(this, i, this.className, stateVal), stateVal );
			});
		}

		return this.each(function() {
			if ( type === "string" ) {
				// toggle individual class names
				var className,
					i = 0,
					self = jQuery( this ),
					classNames = value.match( rnotwhite ) || [];

				while ( (className = classNames[ i++ ]) ) {
					// check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( type === strundefined || type === "boolean" ) {
				if ( this.className ) {
					// store className if set
					jQuery._data( this, "__className__", this.className );
				}

				// If the element has a class name or if we're passed "false",
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				this.className = this.className || value === false ? "" : jQuery._data( this, "__className__" ) || "";
			}
		});
	},

	hasClass: function( selector ) {
		var className = " " + selector + " ",
			i = 0,
			l = this.length;
		for ( ; i < l; i++ ) {
			if ( this[i].nodeType === 1 && (" " + this[i].className + " ").replace(rclass, " ").indexOf( className ) >= 0 ) {
				return true;
			}
		}

		return false;
	}
});




// Return jQuery for attributes-only inclusion


jQuery.each( ("blur focus focusin focusout load resize scroll unload click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup error contextmenu").split(" "), function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		return arguments.length > 0 ?
			this.on( name, null, data, fn ) :
			this.trigger( name );
	};
});

jQuery.fn.extend({
	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	},

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {
		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ? this.off( selector, "**" ) : this.off( types, selector || "**", fn );
	}
});


var nonce = jQuery.now();

var rquery = (/\?/);



var rvalidtokens = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;

jQuery.parseJSON = function( data ) {
	// Attempt to parse using the native JSON parser first
	if ( window.JSON && window.JSON.parse ) {
		// Support: Android 2.3
		// Workaround failure to string-cast null input
		return window.JSON.parse( data + "" );
	}

	var requireNonComma,
		depth = null,
		str = jQuery.trim( data + "" );

	// Guard against invalid (and possibly dangerous) input by ensuring that nothing remains
	// after removing valid tokens
	return str && !jQuery.trim( str.replace( rvalidtokens, function( token, comma, open, close ) {

		// Force termination if we see a misplaced comma
		if ( requireNonComma && comma ) {
			depth = 0;
		}

		// Perform no more replacements after returning to outermost depth
		if ( depth === 0 ) {
			return token;
		}

		// Commas must not follow "[", "{", or ","
		requireNonComma = open || comma;

		// Determine new depth
		// array/object open ("[" or "{"): depth += true - false (increment)
		// array/object close ("]" or "}"): depth += false - true (decrement)
		// other cases ("," or primitive): depth += true - true (numeric cast)
		depth += !close - !open;

		// Remove this token
		return "";
	}) ) ?
		( Function( "return " + str ) )() :
		jQuery.error( "Invalid JSON: " + data );
};


// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml, tmp;
	if ( !data || typeof data !== "string" ) {
		return null;
	}
	try {
		if ( window.DOMParser ) { // Standard
			tmp = new DOMParser();
			xml = tmp.parseFromString( data, "text/xml" );
		} else { // IE
			xml = new ActiveXObject( "Microsoft.XMLDOM" );
			xml.async = "false";
			xml.loadXML( data );
		}
	} catch( e ) {
		xml = undefined;
	}
	if ( !xml || !xml.documentElement || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	// Document location
	ajaxLocParts,
	ajaxLocation,

	rhash = /#.*$/,
	rts = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg, // IE leaves an \r character at EOL
	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,
	rurl = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat("*");

// #8138, IE may throw an exception when accessing
// a field from window.location if document.domain has been set
try {
	ajaxLocation = location.href;
} catch( e ) {
	// Use the href attribute of an A element
	// since IE will modify it given document.location
	ajaxLocation = document.createElement( "a" );
	ajaxLocation.href = "";
	ajaxLocation = ajaxLocation.href;
}

// Segment location into parts
ajaxLocParts = rurl.exec( ajaxLocation.toLowerCase() ) || [];

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnotwhite ) || [];

		if ( jQuery.isFunction( func ) ) {
			// For each dataType in the dataTypeExpression
			while ( (dataType = dataTypes[i++]) ) {
				// Prepend if requested
				if ( dataType.charAt( 0 ) === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					(structure[ dataType ] = structure[ dataType ] || []).unshift( func );

				// Otherwise append
				} else {
					(structure[ dataType ] = structure[ dataType ] || []).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" && !seekingTransport && !inspected[ dataTypeOrTransport ] ) {
				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		});
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var deep, key,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || (deep = {}) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {
	var firstDataType, ct, finalDataType, type,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {
		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[0] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}
		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},
		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

			// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {
								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s[ "throws" ] ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return { state: "parsererror", error: conv ? e : "No conversion from " + prev + " to " + current };
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend({

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: ajaxLocation,
		type: "GET",
		isLocal: rlocalProtocol.test( ajaxLocParts[ 1 ] ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /xml/,
			html: /html/,
			json: /json/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": jQuery.parseJSON,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var // Cross-domain detection vars
			parts,
			// Loop variable
			i,
			// URL without anti-cache param
			cacheURL,
			// Response headers as string
			responseHeadersString,
			// timeout handle
			timeoutTimer,

			// To know if global events are to be dispatched
			fireGlobals,

			transport,
			// Response headers
			responseHeaders,
			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),
			// Callbacks context
			callbackContext = s.context || s,
			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context && ( callbackContext.nodeType || callbackContext.jquery ) ?
				jQuery( callbackContext ) :
				jQuery.event,
			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks("once memory"),
			// Status-dependent callbacks
			statusCode = s.statusCode || {},
			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},
			// The jqXHR state
			state = 0,
			// Default abort message
			strAbort = "canceled",
			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( state === 2 ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( (match = rheaders.exec( responseHeadersString )) ) {
								responseHeaders[ match[1].toLowerCase() ] = match[ 2 ];
							}
						}
						match = responseHeaders[ key.toLowerCase() ];
					}
					return match == null ? null : match;
				},

				// Raw string
				getAllResponseHeaders: function() {
					return state === 2 ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					var lname = name.toLowerCase();
					if ( !state ) {
						name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( !state ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( state < 2 ) {
							for ( code in map ) {
								// Lazy-add the new callback in a way that preserves old ones
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						} else {
							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR ).complete = completeDeferred.add;
		jqXHR.success = jqXHR.done;
		jqXHR.error = jqXHR.fail;

		// Remove hash character (#7531: and string promotion)
		// Add protocol if not provided (#5866: IE7 issue with protocol-less urls)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || ajaxLocation ) + "" ).replace( rhash, "" ).replace( rprotocol, ajaxLocParts[ 1 ] + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = jQuery.trim( s.dataType || "*" ).toLowerCase().match( rnotwhite ) || [ "" ];

		// A cross-domain request is in order when we have a protocol:host:port mismatch
		if ( s.crossDomain == null ) {
			parts = rurl.exec( s.url.toLowerCase() );
			s.crossDomain = !!( parts &&
				( parts[ 1 ] !== ajaxLocParts[ 1 ] || parts[ 2 ] !== ajaxLocParts[ 2 ] ||
					( parts[ 3 ] || ( parts[ 1 ] === "http:" ? "80" : "443" ) ) !==
						( ajaxLocParts[ 3 ] || ( ajaxLocParts[ 1 ] === "http:" ? "80" : "443" ) ) )
			);
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( state === 2 ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		fireGlobals = s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger("ajaxStart");
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		cacheURL = s.url;

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// If data is available, append data to url
			if ( s.data ) {
				cacheURL = ( s.url += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data );
				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add anti-cache in url if needed
			if ( s.cache === false ) {
				s.url = rts.test( cacheURL ) ?

					// If there is already a '_' parameter, set its value
					cacheURL.replace( rts, "$1_=" + nonce++ ) :

					// Otherwise add one to the end
					cacheURL + ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + nonce++;
			}
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[0] ] ?
				s.accepts[ s.dataTypes[0] ] + ( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend && ( s.beforeSend.call( callbackContext, jqXHR, s ) === false || state === 2 ) ) {
			// Abort if not done already and return
			return jqXHR.abort();
		}

		// aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		for ( i in { success: 1, error: 1, complete: 1 } ) {
			jqXHR[ i ]( s[ i ] );
		}

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}
			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = setTimeout(function() {
					jqXHR.abort("timeout");
				}, s.timeout );
			}

			try {
				state = 1;
				transport.send( requestHeaders, done );
			} catch ( e ) {
				// Propagate exception as error if not done
				if ( state < 2 ) {
					done( -1, e );
				// Simply rethrow otherwise
				} else {
					throw e;
				}
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Called once
			if ( state === 2 ) {
				return;
			}

			// State is "done" now
			state = 2;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader("Last-Modified");
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader("etag");
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {
				// We extract error from statusText
				// then normalize statusText and status for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );
				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger("ajaxStop");
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
});

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {
		// shift arguments if data argument was omitted
		if ( jQuery.isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		return jQuery.ajax({
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		});
	};
});

// Attach a bunch of functions for handling common AJAX events
jQuery.each( [ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function( i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
});


jQuery._evalUrl = function( url ) {
	return jQuery.ajax({
		url: url,
		type: "GET",
		dataType: "script",
		async: false,
		global: false,
		"throws": true
	});
};


jQuery.fn.extend({
	wrapAll: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each(function(i) {
				jQuery(this).wrapAll( html.call(this, i) );
			});
		}

		if ( this[0] ) {
			// The elements to wrap the target around
			var wrap = jQuery( html, this[0].ownerDocument ).eq(0).clone(true);

			if ( this[0].parentNode ) {
				wrap.insertBefore( this[0] );
			}

			wrap.map(function() {
				var elem = this;

				while ( elem.firstChild && elem.firstChild.nodeType === 1 ) {
					elem = elem.firstChild;
				}

				return elem;
			}).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each(function(i) {
				jQuery(this).wrapInner( html.call(this, i) );
			});
		}

		return this.each(function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		});
	},

	wrap: function( html ) {
		var isFunction = jQuery.isFunction( html );

		return this.each(function(i) {
			jQuery( this ).wrapAll( isFunction ? html.call(this, i) : html );
		});
	},

	unwrap: function() {
		return this.parent().each(function() {
			if ( !jQuery.nodeName( this, "body" ) ) {
				jQuery( this ).replaceWith( this.childNodes );
			}
		}).end();
	}
});


jQuery.expr.filters.hidden = function( elem ) {
	// Support: Opera <= 12.12
	// Opera reports offsetWidths and offsetHeights less than zero on some elements
	return elem.offsetWidth <= 0 && elem.offsetHeight <= 0 ||
		(!support.reliableHiddenOffsets() &&
			((elem.style && elem.style.display) || jQuery.css( elem, "display" )) === "none");
};

jQuery.expr.filters.visible = function( elem ) {
	return !jQuery.expr.filters.hidden( elem );
};




var r20 = /%20/g,
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( jQuery.isArray( obj ) ) {
		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {
				// Treat each array item as a scalar.
				add( prefix, v );

			} else {
				// Item is non-scalar (array or object), encode its numeric index.
				buildParams( prefix + "[" + ( typeof v === "object" ? i : "" ) + "]", v, traditional, add );
			}
		});

	} else if ( !traditional && jQuery.type( obj ) === "object" ) {
		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {
		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, value ) {
			// If value is a function, invoke it and return its value
			value = jQuery.isFunction( value ) ? value() : ( value == null ? "" : value );
			s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
		};

	// Set traditional to true for jQuery <= 1.3.2 behavior.
	if ( traditional === undefined ) {
		traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
	}

	// If an array was passed in, assume that it is an array of form elements.
	if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {
		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		});

	} else {
		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" ).replace( r20, "+" );
};

jQuery.fn.extend({
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map(function() {
			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		})
		.filter(function() {
			var type = this.type;
			// Use .is(":disabled") so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		})
		.map(function( i, elem ) {
			var val = jQuery( this ).val();

			return val == null ?
				null :
				jQuery.isArray( val ) ?
					jQuery.map( val, function( val ) {
						return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
					}) :
					{ name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		}).get();
	}
});


// Create the request object
// (This is still attached to ajaxSettings for backward compatibility)
jQuery.ajaxSettings.xhr = window.ActiveXObject !== undefined ?
	// Support: IE6+
	function() {

		// XHR cannot access local files, always use ActiveX for that case
		return !this.isLocal &&

			// Support: IE7-8
			// oldIE XHR does not support non-RFC2616 methods (#13240)
			// See http://msdn.microsoft.com/en-us/library/ie/ms536648(v=vs.85).aspx
			// and http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9
			// Although this check for six methods instead of eight
			// since IE also does not support "trace" and "connect"
			/^(get|post|head|put|delete|options)$/i.test( this.type ) &&

			createStandardXHR() || createActiveXHR();
	} :
	// For all other browsers, use the standard XMLHttpRequest object
	createStandardXHR;

var xhrId = 0,
	xhrCallbacks = {},
	xhrSupported = jQuery.ajaxSettings.xhr();

// Support: IE<10
// Open requests must be manually aborted on unload (#5280)
if ( window.ActiveXObject ) {
	jQuery( window ).on( "unload", function() {
		for ( var key in xhrCallbacks ) {
			xhrCallbacks[ key ]( undefined, true );
		}
	});
}

// Determine support properties
support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
xhrSupported = support.ajax = !!xhrSupported;

// Create transport if the browser can provide an xhr
if ( xhrSupported ) {

	jQuery.ajaxTransport(function( options ) {
		// Cross domain only allowed if supported through XMLHttpRequest
		if ( !options.crossDomain || support.cors ) {

			var callback;

			return {
				send: function( headers, complete ) {
					var i,
						xhr = options.xhr(),
						id = ++xhrId;

					// Open the socket
					xhr.open( options.type, options.url, options.async, options.username, options.password );

					// Apply custom fields if provided
					if ( options.xhrFields ) {
						for ( i in options.xhrFields ) {
							xhr[ i ] = options.xhrFields[ i ];
						}
					}

					// Override mime type if needed
					if ( options.mimeType && xhr.overrideMimeType ) {
						xhr.overrideMimeType( options.mimeType );
					}

					// X-Requested-With header
					// For cross-domain requests, seeing as conditions for a preflight are
					// akin to a jigsaw puzzle, we simply never set it to be sure.
					// (it can always be set on a per-request basis or even using ajaxSetup)
					// For same-domain requests, won't change header if already provided.
					if ( !options.crossDomain && !headers["X-Requested-With"] ) {
						headers["X-Requested-With"] = "XMLHttpRequest";
					}

					// Set headers
					for ( i in headers ) {
						// Support: IE<9
						// IE's ActiveXObject throws a 'Type Mismatch' exception when setting
						// request header to a null-value.
						//
						// To keep consistent with other XHR implementations, cast the value
						// to string and ignore `undefined`.
						if ( headers[ i ] !== undefined ) {
							xhr.setRequestHeader( i, headers[ i ] + "" );
						}
					}

					// Do send the request
					// This may raise an exception which is actually
					// handled in jQuery.ajax (so no try/catch here)
					xhr.send( ( options.hasContent && options.data ) || null );

					// Listener
					callback = function( _, isAbort ) {
						var status, statusText, responses;

						// Was never called and is aborted or complete
						if ( callback && ( isAbort || xhr.readyState === 4 ) ) {
							// Clean up
							delete xhrCallbacks[ id ];
							callback = undefined;
							xhr.onreadystatechange = jQuery.noop;

							// Abort manually if needed
							if ( isAbort ) {
								if ( xhr.readyState !== 4 ) {
									xhr.abort();
								}
							} else {
								responses = {};
								status = xhr.status;

								// Support: IE<10
								// Accessing binary-data responseText throws an exception
								// (#11426)
								if ( typeof xhr.responseText === "string" ) {
									responses.text = xhr.responseText;
								}

								// Firefox throws an exception when accessing
								// statusText for faulty cross-domain requests
								try {
									statusText = xhr.statusText;
								} catch( e ) {
									// We normalize with Webkit giving an empty statusText
									statusText = "";
								}

								// Filter status for non standard behaviors

								// If the request is local and we have data: assume a success
								// (success with no data won't get notified, that's the best we
								// can do given current implementations)
								if ( !status && options.isLocal && !options.crossDomain ) {
									status = responses.text ? 200 : 404;
								// IE - #1450: sometimes returns 1223 when it should be 204
								} else if ( status === 1223 ) {
									status = 204;
								}
							}
						}

						// Call complete if needed
						if ( responses ) {
							complete( status, statusText, responses, xhr.getAllResponseHeaders() );
						}
					};

					if ( !options.async ) {
						// if we're in sync mode we fire the callback
						callback();
					} else if ( xhr.readyState === 4 ) {
						// (IE6 & IE7) if it's in cache and has been
						// retrieved directly we need to fire the callback
						setTimeout( callback );
					} else {
						// Add to the list of active xhr callbacks
						xhr.onreadystatechange = xhrCallbacks[ id ] = callback;
					}
				},

				abort: function() {
					if ( callback ) {
						callback( undefined, true );
					}
				}
			};
		}
	});
}

// Functions to create xhrs
function createStandardXHR() {
	try {
		return new window.XMLHttpRequest();
	} catch( e ) {}
}

function createActiveXHR() {
	try {
		return new window.ActiveXObject( "Microsoft.XMLHTTP" );
	} catch( e ) {}
}




// Install script dataType
jQuery.ajaxSetup({
	accepts: {
		script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /(?:java|ecma)script/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
});

// Handle cache's special case and global
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
		s.global = false;
	}
});

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function(s) {

	// This transport only deals with cross domain requests
	if ( s.crossDomain ) {

		var script,
			head = document.head || jQuery("head")[0] || document.documentElement;

		return {

			send: function( _, callback ) {

				script = document.createElement("script");

				script.async = true;

				if ( s.scriptCharset ) {
					script.charset = s.scriptCharset;
				}

				script.src = s.url;

				// Attach handlers for all browsers
				script.onload = script.onreadystatechange = function( _, isAbort ) {

					if ( isAbort || !script.readyState || /loaded|complete/.test( script.readyState ) ) {

						// Handle memory leak in IE
						script.onload = script.onreadystatechange = null;

						// Remove the script
						if ( script.parentNode ) {
							script.parentNode.removeChild( script );
						}

						// Dereference the script
						script = null;

						// Callback if not abort
						if ( !isAbort ) {
							callback( 200, "success" );
						}
					}
				};

				// Circumvent IE6 bugs with base elements (#2709 and #4378) by prepending
				// Use native DOM manipulation to avoid our domManip AJAX trickery
				head.insertBefore( script, head.firstChild );
			},

			abort: function() {
				if ( script ) {
					script.onload( undefined, true );
				}
			}
		};
	}
});




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup({
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
		this[ callback ] = true;
		return callback;
	}
});

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" && !( s.contentType || "" ).indexOf("application/x-www-form-urlencoded") && rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters["script json"] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always(function() {
			// Restore preexisting value
			window[ callbackName ] = overwritten;

			// Save back as free
			if ( s[ callbackName ] ) {
				// make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && jQuery.isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		});

		// Delegate to script
		return "script";
	}
});




// data: string of html
// context (optional): If specified, the fragment will be created in this context, defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( !data || typeof data !== "string" ) {
		return null;
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}
	context = context || document;

	var parsed = rsingleTag.exec( data ),
		scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[1] ) ];
	}

	parsed = jQuery.buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


// Keep a copy of the old load method
var _load = jQuery.fn.load;

/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	if ( typeof url !== "string" && _load ) {
		return _load.apply( this, arguments );
	}

	var selector, response, type,
		self = this,
		off = url.indexOf(" ");

	if ( off >= 0 ) {
		selector = url.slice( off, url.length );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( jQuery.isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax({
			url: url,

			// if "type" variable is undefined, then "GET" method will be used
			type: type,
			dataType: "html",
			data: params
		}).done(function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery("<div>").append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		}).complete( callback && function( jqXHR, status ) {
			self.each( callback, response || [ jqXHR.responseText, status, jqXHR ] );
		});
	}

	return this;
};




jQuery.expr.filters.animated = function( elem ) {
	return jQuery.grep(jQuery.timers, function( fn ) {
		return elem === fn.elem;
	}).length;
};





var docElem = window.document.documentElement;

/**
 * Gets a window from an element
 */
function getWindow( elem ) {
	return jQuery.isWindow( elem ) ?
		elem :
		elem.nodeType === 9 ?
			elem.defaultView || elem.parentWindow :
			false;
}

jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			jQuery.inArray("auto", [ curCSSTop, curCSSLeft ] ) > -1;

		// need to be able to calculate position if either top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;
		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( jQuery.isFunction( options ) ) {
			options = options.call( elem, i, curOffset );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );
		} else {
			curElem.css( props );
		}
	}
};

jQuery.fn.extend({
	offset: function( options ) {
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each(function( i ) {
					jQuery.offset.setOffset( this, options, i );
				});
		}

		var docElem, win,
			box = { top: 0, left: 0 },
			elem = this[ 0 ],
			doc = elem && elem.ownerDocument;

		if ( !doc ) {
			return;
		}

		docElem = doc.documentElement;

		// Make sure it's not a disconnected DOM node
		if ( !jQuery.contains( docElem, elem ) ) {
			return box;
		}

		// If we don't have gBCR, just use 0,0 rather than error
		// BlackBerry 5, iOS 3 (original iPhone)
		if ( typeof elem.getBoundingClientRect !== strundefined ) {
			box = elem.getBoundingClientRect();
		}
		win = getWindow( doc );
		return {
			top: box.top  + ( win.pageYOffset || docElem.scrollTop )  - ( docElem.clientTop  || 0 ),
			left: box.left + ( win.pageXOffset || docElem.scrollLeft ) - ( docElem.clientLeft || 0 )
		};
	},

	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset,
			parentOffset = { top: 0, left: 0 },
			elem = this[ 0 ];

		// fixed elements are offset from window (parentOffset = {top:0, left: 0}, because it is its only offset parent
		if ( jQuery.css( elem, "position" ) === "fixed" ) {
			// we assume that getBoundingClientRect is available when computed position is fixed
			offset = elem.getBoundingClientRect();
		} else {
			// Get *real* offsetParent
			offsetParent = this.offsetParent();

			// Get correct offsets
			offset = this.offset();
			if ( !jQuery.nodeName( offsetParent[ 0 ], "html" ) ) {
				parentOffset = offsetParent.offset();
			}

			// Add offsetParent borders
			parentOffset.top  += jQuery.css( offsetParent[ 0 ], "borderTopWidth", true );
			parentOffset.left += jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true );
		}

		// Subtract parent offsets and element margins
		// note: when an element has margin: auto the offsetLeft and marginLeft
		// are the same in Safari causing offset.left to incorrectly be 0
		return {
			top:  offset.top  - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true)
		};
	},

	offsetParent: function() {
		return this.map(function() {
			var offsetParent = this.offsetParent || docElem;

			while ( offsetParent && ( !jQuery.nodeName( offsetParent, "html" ) && jQuery.css( offsetParent, "position" ) === "static" ) ) {
				offsetParent = offsetParent.offsetParent;
			}
			return offsetParent || docElem;
		});
	}
});

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = /Y/.test( prop );

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {
			var win = getWindow( elem );

			if ( val === undefined ) {
				return win ? (prop in win) ? win[ prop ] :
					win.document.documentElement[ method ] :
					elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : jQuery( win ).scrollLeft(),
					top ? val : jQuery( win ).scrollTop()
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length, null );
	};
});

// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// getComputedStyle returns percent when specified for top/left/bottom/right
// rather than make the css module depend on the offset module, we just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );
				// if curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
});


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name }, function( defaultExtra, funcName ) {
		// margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( jQuery.isWindow( elem ) ) {
					// As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there
					// isn't a whole lot we can do. See pull request at this URL for discussion:
					// https://github.com/jquery/jquery/pull/764
					return elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height], whichever is greatest
					// unfortunately, this causes bug #3838 in IE6/8 only, but there is currently no good, small way to fix it.
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?
					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable, null );
		};
	});
});


// The number of elements contained in the matched element set
jQuery.fn.size = function() {
	return this.length;
};

jQuery.fn.andSelf = jQuery.fn.addBack;




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.
if ( typeof define === "function" && define.amd ) {
	define( "jquery", [], function() {
		return jQuery;
	});
}




var
	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in
// AMD (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( typeof noGlobal === strundefined ) {
	window.jQuery = window.$ = jQuery;
}




return jQuery;

}));
(function($, undefined) {

/**
 * Unobtrusive scripting adapter for jQuery
 * https://github.com/rails/jquery-ujs
 *
 * Requires jQuery 1.7.0 or later.
 *
 * Released under the MIT license
 *
 */

  // Cut down on the number of issues from people inadvertently including jquery_ujs twice
  // by detecting and raising an error when it happens.
  if ( $.rails !== undefined ) {
    $.error('jquery-ujs has already been loaded!');
  }

  // Shorthand to make it a little easier to call public rails functions from within rails.js
  var rails;
  var $document = $(document);

  $.rails = rails = {
    // Link elements bound by jquery-ujs
    linkClickSelector: 'a[data-confirm], a[data-method], a[data-remote], a[data-disable-with]',

    // Button elements bound by jquery-ujs
    buttonClickSelector: 'button[data-remote]',

    // Select elements bound by jquery-ujs
    inputChangeSelector: 'select[data-remote], input[data-remote], textarea[data-remote]',

    // Form elements bound by jquery-ujs
    formSubmitSelector: 'form',

    // Form input elements bound by jquery-ujs
    formInputClickSelector: 'form input[type=submit], form input[type=image], form button[type=submit], form button:not([type])',

    // Form input elements disabled during form submission
    disableSelector: 'input[data-disable-with], button[data-disable-with], textarea[data-disable-with]',

    // Form input elements re-enabled after form submission
    enableSelector: 'input[data-disable-with]:disabled, button[data-disable-with]:disabled, textarea[data-disable-with]:disabled',

    // Form required input elements
    requiredInputSelector: 'input[name][required]:not([disabled]),textarea[name][required]:not([disabled])',

    // Form file input elements
    fileInputSelector: 'input[type=file]',

    // Link onClick disable selector with possible reenable after remote submission
    linkDisableSelector: 'a[data-disable-with]',

    // Make sure that every Ajax request sends the CSRF token
    CSRFProtection: function(xhr) {
      var token = $('meta[name="csrf-token"]').attr('content');
      if (token) xhr.setRequestHeader('X-CSRF-Token', token);
    },

    // making sure that all forms have actual up-to-date token(cached forms contain old one)
    refreshCSRFTokens: function(){
      var csrfToken = $('meta[name=csrf-token]').attr('content');
      var csrfParam = $('meta[name=csrf-param]').attr('content');
      $('form input[name="' + csrfParam + '"]').val(csrfToken);
    },

    // Triggers an event on an element and returns false if the event result is false
    fire: function(obj, name, data) {
      var event = $.Event(name);
      obj.trigger(event, data);
      return event.result !== false;
    },

    // Default confirm dialog, may be overridden with custom confirm dialog in $.rails.confirm
    confirm: function(message) {
      return confirm(message);
    },

    // Default ajax function, may be overridden with custom function in $.rails.ajax
    ajax: function(options) {
      return $.ajax(options);
    },

    // Default way to get an element's href. May be overridden at $.rails.href.
    href: function(element) {
      return element.attr('href');
    },

    // Submits "remote" forms and links with ajax
    handleRemote: function(element) {
      var method, url, data, elCrossDomain, crossDomain, withCredentials, dataType, options;

      if (rails.fire(element, 'ajax:before')) {
        elCrossDomain = element.data('cross-domain');
        crossDomain = elCrossDomain === undefined ? null : elCrossDomain;
        withCredentials = element.data('with-credentials') || null;
        dataType = element.data('type') || ($.ajaxSettings && $.ajaxSettings.dataType);

        if (element.is('form')) {
          method = element.attr('method');
          url = element.attr('action');
          data = element.serializeArray();
          // memoized value from clicked submit button
          var button = element.data('ujs:submit-button');
          if (button) {
            data.push(button);
            element.data('ujs:submit-button', null);
          }
        } else if (element.is(rails.inputChangeSelector)) {
          method = element.data('method');
          url = element.data('url');
          data = element.serialize();
          if (element.data('params')) data = data + "&" + element.data('params');
        } else if (element.is(rails.buttonClickSelector)) {
          method = element.data('method') || 'get';
          url = element.data('url');
          data = element.serialize();
          if (element.data('params')) data = data + "&" + element.data('params');
        } else {
          method = element.data('method');
          url = rails.href(element);
          data = element.data('params') || null;
        }

        options = {
          type: method || 'GET', data: data, dataType: dataType,
          // stopping the "ajax:beforeSend" event will cancel the ajax request
          beforeSend: function(xhr, settings) {
            if (settings.dataType === undefined) {
              xhr.setRequestHeader('accept', '*/*;q=0.5, ' + settings.accepts.script);
            }
            return rails.fire(element, 'ajax:beforeSend', [xhr, settings]);
          },
          success: function(data, status, xhr) {
            element.trigger('ajax:success', [data, status, xhr]);
          },
          complete: function(xhr, status) {
            element.trigger('ajax:complete', [xhr, status]);
          },
          error: function(xhr, status, error) {
            element.trigger('ajax:error', [xhr, status, error]);
          },
          crossDomain: crossDomain
        };

        // There is no withCredentials for IE6-8 when
        // "Enable native XMLHTTP support" is disabled
        if (withCredentials) {
          options.xhrFields = {
            withCredentials: withCredentials
          };
        }

        // Only pass url to `ajax` options if not blank
        if (url) { options.url = url; }

        var jqxhr = rails.ajax(options);
        element.trigger('ajax:send', jqxhr);
        return jqxhr;
      } else {
        return false;
      }
    },

    // Handles "data-method" on links such as:
    // <a href="/users/5" data-method="delete" rel="nofollow" data-confirm="Are you sure?">Delete</a>
    handleMethod: function(link) {
      var href = rails.href(link),
        method = link.data('method'),
        target = link.attr('target'),
        csrfToken = $('meta[name=csrf-token]').attr('content'),
        csrfParam = $('meta[name=csrf-param]').attr('content'),
        form = $('<form method="post" action="' + href + '"></form>'),
        metadataInput = '<input name="_method" value="' + method + '" type="hidden" />';

      if (csrfParam !== undefined && csrfToken !== undefined) {
        metadataInput += '<input name="' + csrfParam + '" value="' + csrfToken + '" type="hidden" />';
      }

      if (target) { form.attr('target', target); }

      form.hide().append(metadataInput).appendTo('body');
      form.submit();
    },

    /* Disables form elements:
      - Caches element value in 'ujs:enable-with' data store
      - Replaces element text with value of 'data-disable-with' attribute
      - Sets disabled property to true
    */
    disableFormElements: function(form) {
      form.find(rails.disableSelector).each(function() {
        var element = $(this), method = element.is('button') ? 'html' : 'val';
        element.data('ujs:enable-with', element[method]());
        element[method](element.data('disable-with'));
        element.prop('disabled', true);
      });
    },

    /* Re-enables disabled form elements:
      - Replaces element text with cached value from 'ujs:enable-with' data store (created in `disableFormElements`)
      - Sets disabled property to false
    */
    enableFormElements: function(form) {
      form.find(rails.enableSelector).each(function() {
        var element = $(this), method = element.is('button') ? 'html' : 'val';
        if (element.data('ujs:enable-with')) element[method](element.data('ujs:enable-with'));
        element.prop('disabled', false);
      });
    },

   /* For 'data-confirm' attribute:
      - Fires `confirm` event
      - Shows the confirmation dialog
      - Fires the `confirm:complete` event

      Returns `true` if no function stops the chain and user chose yes; `false` otherwise.
      Attaching a handler to the element's `confirm` event that returns a `falsy` value cancels the confirmation dialog.
      Attaching a handler to the element's `confirm:complete` event that returns a `falsy` value makes this function
      return false. The `confirm:complete` event is fired whether or not the user answered true or false to the dialog.
   */
    allowAction: function(element) {
      var message = element.data('confirm'),
          answer = false, callback;
      if (!message) { return true; }

      if (rails.fire(element, 'confirm')) {
        answer = rails.confirm(message);
        callback = rails.fire(element, 'confirm:complete', [answer]);
      }
      return answer && callback;
    },

    // Helper function which checks for blank inputs in a form that match the specified CSS selector
    blankInputs: function(form, specifiedSelector, nonBlank) {
      var inputs = $(), input, valueToCheck,
          selector = specifiedSelector || 'input,textarea',
          allInputs = form.find(selector);

      allInputs.each(function() {
        input = $(this);
        valueToCheck = input.is('input[type=checkbox],input[type=radio]') ? input.is(':checked') : input.val();
        // If nonBlank and valueToCheck are both truthy, or nonBlank and valueToCheck are both falsey
        if (!valueToCheck === !nonBlank) {

          // Don't count unchecked required radio if other radio with same name is checked
          if (input.is('input[type=radio]') && allInputs.filter('input[type=radio]:checked[name="' + input.attr('name') + '"]').length) {
            return true; // Skip to next input
          }

          inputs = inputs.add(input);
        }
      });
      return inputs.length ? inputs : false;
    },

    // Helper function which checks for non-blank inputs in a form that match the specified CSS selector
    nonBlankInputs: function(form, specifiedSelector) {
      return rails.blankInputs(form, specifiedSelector, true); // true specifies nonBlank
    },

    // Helper function, needed to provide consistent behavior in IE
    stopEverything: function(e) {
      $(e.target).trigger('ujs:everythingStopped');
      e.stopImmediatePropagation();
      return false;
    },

    //  replace element's html with the 'data-disable-with' after storing original html
    //  and prevent clicking on it
    disableElement: function(element) {
      element.data('ujs:enable-with', element.html()); // store enabled state
      element.html(element.data('disable-with')); // set to disabled state
      element.bind('click.railsDisable', function(e) { // prevent further clicking
        return rails.stopEverything(e);
      });
    },

    // restore element to its original state which was disabled by 'disableElement' above
    enableElement: function(element) {
      if (element.data('ujs:enable-with') !== undefined) {
        element.html(element.data('ujs:enable-with')); // set to old enabled state
        element.removeData('ujs:enable-with'); // clean up cache
      }
      element.unbind('click.railsDisable'); // enable element
    }

  };

  if (rails.fire($document, 'rails:attachBindings')) {

    $.ajaxPrefilter(function(options, originalOptions, xhr){ if ( !options.crossDomain ) { rails.CSRFProtection(xhr); }});

    $document.delegate(rails.linkDisableSelector, 'ajax:complete', function() {
        rails.enableElement($(this));
    });

    $document.delegate(rails.linkClickSelector, 'click.rails', function(e) {
      var link = $(this), method = link.data('method'), data = link.data('params'), metaClick = e.metaKey || e.ctrlKey;
      if (!rails.allowAction(link)) return rails.stopEverything(e);

      if (!metaClick && link.is(rails.linkDisableSelector)) rails.disableElement(link);

      if (link.data('remote') !== undefined) {
        if (metaClick && (!method || method === 'GET') && !data) { return true; }

        var handleRemote = rails.handleRemote(link);
        // response from rails.handleRemote() will either be false or a deferred object promise.
        if (handleRemote === false) {
          rails.enableElement(link);
        } else {
          handleRemote.error( function() { rails.enableElement(link); } );
        }
        return false;

      } else if (link.data('method')) {
        rails.handleMethod(link);
        return false;
      }
    });

    $document.delegate(rails.buttonClickSelector, 'click.rails', function(e) {
      var button = $(this);
      if (!rails.allowAction(button)) return rails.stopEverything(e);

      rails.handleRemote(button);
      return false;
    });

    $document.delegate(rails.inputChangeSelector, 'change.rails', function(e) {
      var link = $(this);
      if (!rails.allowAction(link)) return rails.stopEverything(e);

      rails.handleRemote(link);
      return false;
    });

    $document.delegate(rails.formSubmitSelector, 'submit.rails', function(e) {
      var form = $(this),
        remote = form.data('remote') !== undefined,
        blankRequiredInputs = rails.blankInputs(form, rails.requiredInputSelector),
        nonBlankFileInputs = rails.nonBlankInputs(form, rails.fileInputSelector);

      if (!rails.allowAction(form)) return rails.stopEverything(e);

      // skip other logic when required values are missing or file upload is present
      if (blankRequiredInputs && form.attr("novalidate") == undefined && rails.fire(form, 'ajax:aborted:required', [blankRequiredInputs])) {
        return rails.stopEverything(e);
      }

      if (remote) {
        if (nonBlankFileInputs) {
          // slight timeout so that the submit button gets properly serialized
          // (make it easy for event handler to serialize form without disabled values)
          setTimeout(function(){ rails.disableFormElements(form); }, 13);
          var aborted = rails.fire(form, 'ajax:aborted:file', [nonBlankFileInputs]);

          // re-enable form elements if event bindings return false (canceling normal form submission)
          if (!aborted) { setTimeout(function(){ rails.enableFormElements(form); }, 13); }

          return aborted;
        }

        rails.handleRemote(form);
        return false;

      } else {
        // slight timeout so that the submit button gets properly serialized
        setTimeout(function(){ rails.disableFormElements(form); }, 13);
      }
    });

    $document.delegate(rails.formInputClickSelector, 'click.rails', function(event) {
      var button = $(this);

      if (!rails.allowAction(button)) return rails.stopEverything(event);

      // register the pressed submit button
      var name = button.attr('name'),
        data = name ? {name:name, value:button.val()} : null;

      button.closest('form').data('ujs:submit-button', data);
    });

    $document.delegate(rails.formSubmitSelector, 'ajax:beforeSend.rails', function(event) {
      if (this == event.target) rails.disableFormElements($(this));
    });

    $document.delegate(rails.formSubmitSelector, 'ajax:complete.rails', function(event) {
      if (this == event.target) rails.enableFormElements($(this));
    });

    $(function(){
      rails.refreshCSRFTokens();
    });
  }

})( jQuery );
(function() {
  var CSRFToken, allowLinkExtensions, anchoredLink, browserCompatibleDocumentParser, browserIsntBuggy, browserSupportsCustomEvents, browserSupportsPushState, browserSupportsTurbolinks, bypassOnLoadPopstate, cacheCurrentPage, cacheSize, changePage, constrainPageCacheTo, createDocument, crossOriginLink, currentState, enableTransitionCache, executeScriptTags, extractLink, extractTitleAndBody, fetch, fetchHistory, fetchReplacement, handleClick, historyStateIsDefined, htmlExtensions, ignoreClick, initializeTurbolinks, installClickHandlerLast, installDocumentReadyPageEventTriggers, installHistoryChangeHandler, installJqueryAjaxSuccessPageUpdateTrigger, loadedAssets, noTurbolink, nonHtmlLink, nonStandardClick, pageCache, pageChangePrevented, pagesCached, popCookie, processResponse, recallScrollPosition, referer, reflectNewUrl, reflectRedirectedUrl, rememberCurrentState, rememberCurrentUrl, rememberReferer, removeHash, removeHashForIE10compatiblity, removeNoscriptTags, requestMethodIsSafe, resetScrollPosition, targetLink, transitionCacheEnabled, transitionCacheFor, triggerEvent, visit, xhr, _ref,
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; },
    __slice = [].slice;

  pageCache = {};

  cacheSize = 10;

  transitionCacheEnabled = false;

  currentState = null;

  loadedAssets = null;

  htmlExtensions = ['html'];

  referer = null;

  createDocument = null;

  xhr = null;

  fetch = function(url) {
    var cachedPage;
    rememberReferer();
    cacheCurrentPage();
    reflectNewUrl(url);
    if (transitionCacheEnabled && (cachedPage = transitionCacheFor(url))) {
      fetchHistory(cachedPage);
      return fetchReplacement(url);
    } else {
      return fetchReplacement(url, resetScrollPosition);
    }
  };

  transitionCacheFor = function(url) {
    var cachedPage;
    cachedPage = pageCache[url];
    if (cachedPage && !cachedPage.transitionCacheDisabled) {
      return cachedPage;
    }
  };

  enableTransitionCache = function(enable) {
    if (enable == null) {
      enable = true;
    }
    return transitionCacheEnabled = enable;
  };

  fetchReplacement = function(url, onLoadFunction) {
    if (onLoadFunction == null) {
      onLoadFunction = (function(_this) {
        return function() {};
      })(this);
    }
    triggerEvent('page:fetch', {
      url: url
    });
    if (xhr != null) {
      xhr.abort();
    }
    xhr = new XMLHttpRequest;
    xhr.open('GET', removeHashForIE10compatiblity(url), true);
    xhr.setRequestHeader('Accept', 'text/html, application/xhtml+xml, application/xml');
    xhr.setRequestHeader('X-XHR-Referer', referer);
    xhr.onload = function() {
      var doc;
      triggerEvent('page:receive');
      if (doc = processResponse()) {
        changePage.apply(null, extractTitleAndBody(doc));
        reflectRedirectedUrl();
        onLoadFunction();
        return triggerEvent('page:load');
      } else {
        return document.location.href = url;
      }
    };
    xhr.onloadend = function() {
      return xhr = null;
    };
    xhr.onerror = function() {
      return document.location.href = url;
    };
    return xhr.send();
  };

  fetchHistory = function(cachedPage) {
    if (xhr != null) {
      xhr.abort();
    }
    changePage(cachedPage.title, cachedPage.body);
    recallScrollPosition(cachedPage);
    return triggerEvent('page:restore');
  };

  cacheCurrentPage = function() {
    pageCache[currentState.url] = {
      url: document.location.href,
      body: document.body,
      title: document.title,
      positionY: window.pageYOffset,
      positionX: window.pageXOffset,
      cachedAt: new Date().getTime(),
      transitionCacheDisabled: document.querySelector('[data-no-transition-cache]') != null
    };
    return constrainPageCacheTo(cacheSize);
  };

  pagesCached = function(size) {
    if (size == null) {
      size = cacheSize;
    }
    if (/^[\d]+$/.test(size)) {
      return cacheSize = parseInt(size);
    }
  };

  constrainPageCacheTo = function(limit) {
    var cacheTimesRecentFirst, key, pageCacheKeys, _i, _len, _results;
    pageCacheKeys = Object.keys(pageCache);
    cacheTimesRecentFirst = pageCacheKeys.map(function(url) {
      return pageCache[url].cachedAt;
    }).sort(function(a, b) {
      return b - a;
    });
    _results = [];
    for (_i = 0, _len = pageCacheKeys.length; _i < _len; _i++) {
      key = pageCacheKeys[_i];
      if (!(pageCache[key].cachedAt <= cacheTimesRecentFirst[limit])) {
        continue;
      }
      triggerEvent('page:expire', pageCache[key]);
      _results.push(delete pageCache[key]);
    }
    return _results;
  };

  changePage = function(title, body, csrfToken, runScripts) {
    document.title = title;
    document.documentElement.replaceChild(body, document.body);
    if (csrfToken != null) {
      CSRFToken.update(csrfToken);
    }
    if (runScripts) {
      executeScriptTags();
    }
    currentState = window.history.state;
    triggerEvent('page:change');
    return triggerEvent('page:update');
  };

  executeScriptTags = function() {
    var attr, copy, nextSibling, parentNode, script, scripts, _i, _j, _len, _len1, _ref, _ref1;
    scripts = Array.prototype.slice.call(document.body.querySelectorAll('script:not([data-turbolinks-eval="false"])'));
    for (_i = 0, _len = scripts.length; _i < _len; _i++) {
      script = scripts[_i];
      if (!((_ref = script.type) === '' || _ref === 'text/javascript')) {
        continue;
      }
      copy = document.createElement('script');
      _ref1 = script.attributes;
      for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
        attr = _ref1[_j];
        copy.setAttribute(attr.name, attr.value);
      }
      copy.appendChild(document.createTextNode(script.innerHTML));
      parentNode = script.parentNode, nextSibling = script.nextSibling;
      parentNode.removeChild(script);
      parentNode.insertBefore(copy, nextSibling);
    }
  };

  removeNoscriptTags = function(node) {
    node.innerHTML = node.innerHTML.replace(/<noscript[\S\s]*?<\/noscript>/ig, '');
    return node;
  };

  reflectNewUrl = function(url) {
    if (url !== referer) {
      return window.history.pushState({
        turbolinks: true,
        url: url
      }, '', url);
    }
  };

  reflectRedirectedUrl = function() {
    var location, preservedHash;
    if (location = xhr.getResponseHeader('X-XHR-Redirected-To')) {
      preservedHash = removeHash(location) === location ? document.location.hash : '';
      return window.history.replaceState(currentState, '', location + preservedHash);
    }
  };

  rememberReferer = function() {
    return referer = document.location.href;
  };

  rememberCurrentUrl = function() {
    return window.history.replaceState({
      turbolinks: true,
      url: document.location.href
    }, '', document.location.href);
  };

  rememberCurrentState = function() {
    return currentState = window.history.state;
  };

  recallScrollPosition = function(page) {
    return window.scrollTo(page.positionX, page.positionY);
  };

  resetScrollPosition = function() {
    if (document.location.hash) {
      return document.location.href = document.location.href;
    } else {
      return window.scrollTo(0, 0);
    }
  };

  removeHashForIE10compatiblity = function(url) {
    return removeHash(url);
  };

  removeHash = function(url) {
    var link;
    link = url;
    if (url.href == null) {
      link = document.createElement('A');
      link.href = url;
    }
    return link.href.replace(link.hash, '');
  };

  popCookie = function(name) {
    var value, _ref;
    value = ((_ref = document.cookie.match(new RegExp(name + "=(\\w+)"))) != null ? _ref[1].toUpperCase() : void 0) || '';
    document.cookie = name + '=; expires=Thu, 01-Jan-70 00:00:01 GMT; path=/';
    return value;
  };

  triggerEvent = function(name, data) {
    var event;
    event = document.createEvent('Events');
    if (data) {
      event.data = data;
    }
    event.initEvent(name, true, true);
    return document.dispatchEvent(event);
  };

  pageChangePrevented = function() {
    return !triggerEvent('page:before-change');
  };

  processResponse = function() {
    var assetsChanged, clientOrServerError, doc, extractTrackAssets, intersection, validContent;
    clientOrServerError = function() {
      var _ref;
      return (400 <= (_ref = xhr.status) && _ref < 600);
    };
    validContent = function() {
      return xhr.getResponseHeader('Content-Type').match(/^(?:text\/html|application\/xhtml\+xml|application\/xml)(?:;|$)/);
    };
    extractTrackAssets = function(doc) {
      var node, _i, _len, _ref, _results;
      _ref = doc.head.childNodes;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        node = _ref[_i];
        if ((typeof node.getAttribute === "function" ? node.getAttribute('data-turbolinks-track') : void 0) != null) {
          _results.push(node.getAttribute('src') || node.getAttribute('href'));
        }
      }
      return _results;
    };
    assetsChanged = function(doc) {
      var fetchedAssets;
      loadedAssets || (loadedAssets = extractTrackAssets(document));
      fetchedAssets = extractTrackAssets(doc);
      return fetchedAssets.length !== loadedAssets.length || intersection(fetchedAssets, loadedAssets).length !== loadedAssets.length;
    };
    intersection = function(a, b) {
      var value, _i, _len, _ref, _results;
      if (a.length > b.length) {
        _ref = [b, a], a = _ref[0], b = _ref[1];
      }
      _results = [];
      for (_i = 0, _len = a.length; _i < _len; _i++) {
        value = a[_i];
        if (__indexOf.call(b, value) >= 0) {
          _results.push(value);
        }
      }
      return _results;
    };
    if (!clientOrServerError() && validContent()) {
      doc = createDocument(xhr.responseText);
      if (doc && !assetsChanged(doc)) {
        return doc;
      }
    }
  };

  extractTitleAndBody = function(doc) {
    var title;
    title = doc.querySelector('title');
    return [title != null ? title.textContent : void 0, removeNoscriptTags(doc.body), CSRFToken.get(doc).token, 'runScripts'];
  };

  CSRFToken = {
    get: function(doc) {
      var tag;
      if (doc == null) {
        doc = document;
      }
      return {
        node: tag = doc.querySelector('meta[name="csrf-token"]'),
        token: tag != null ? typeof tag.getAttribute === "function" ? tag.getAttribute('content') : void 0 : void 0
      };
    },
    update: function(latest) {
      var current;
      current = this.get();
      if ((current.token != null) && (latest != null) && current.token !== latest) {
        return current.node.setAttribute('content', latest);
      }
    }
  };

  browserCompatibleDocumentParser = function() {
    var createDocumentUsingDOM, createDocumentUsingParser, createDocumentUsingWrite, e, testDoc, _ref;
    createDocumentUsingParser = function(html) {
      return (new DOMParser).parseFromString(html, 'text/html');
    };
    createDocumentUsingDOM = function(html) {
      var doc;
      doc = document.implementation.createHTMLDocument('');
      doc.documentElement.innerHTML = html;
      return doc;
    };
    createDocumentUsingWrite = function(html) {
      var doc;
      doc = document.implementation.createHTMLDocument('');
      doc.open('replace');
      doc.write(html);
      doc.close();
      return doc;
    };
    try {
      if (window.DOMParser) {
        testDoc = createDocumentUsingParser('<html><body><p>test');
        return createDocumentUsingParser;
      }
    } catch (_error) {
      e = _error;
      testDoc = createDocumentUsingDOM('<html><body><p>test');
      return createDocumentUsingDOM;
    } finally {
      if ((testDoc != null ? (_ref = testDoc.body) != null ? _ref.childNodes.length : void 0 : void 0) !== 1) {
        return createDocumentUsingWrite;
      }
    }
  };

  installClickHandlerLast = function(event) {
    if (!event.defaultPrevented) {
      document.removeEventListener('click', handleClick, false);
      return document.addEventListener('click', handleClick, false);
    }
  };

  handleClick = function(event) {
    var link;
    if (!event.defaultPrevented) {
      link = extractLink(event);
      if (link.nodeName === 'A' && !ignoreClick(event, link)) {
        if (!pageChangePrevented()) {
          visit(link.href);
        }
        return event.preventDefault();
      }
    }
  };

  extractLink = function(event) {
    var link;
    link = event.target;
    while (!(!link.parentNode || link.nodeName === 'A')) {
      link = link.parentNode;
    }
    return link;
  };

  crossOriginLink = function(link) {
    return location.protocol !== link.protocol || location.host !== link.host;
  };

  anchoredLink = function(link) {
    return ((link.hash && removeHash(link)) === removeHash(location)) || (link.href === location.href + '#');
  };

  nonHtmlLink = function(link) {
    var url;
    url = removeHash(link);
    return url.match(/\.[a-z]+(\?.*)?$/g) && !url.match(new RegExp("\\.(?:" + (htmlExtensions.join('|')) + ")?(\\?.*)?$", 'g'));
  };

  noTurbolink = function(link) {
    var ignore;
    while (!(ignore || link === document)) {
      ignore = link.getAttribute('data-no-turbolink') != null;
      link = link.parentNode;
    }
    return ignore;
  };

  targetLink = function(link) {
    return link.target.length !== 0;
  };

  nonStandardClick = function(event) {
    return event.which > 1 || event.metaKey || event.ctrlKey || event.shiftKey || event.altKey;
  };

  ignoreClick = function(event, link) {
    return crossOriginLink(link) || anchoredLink(link) || nonHtmlLink(link) || noTurbolink(link) || targetLink(link) || nonStandardClick(event);
  };

  allowLinkExtensions = function() {
    var extension, extensions, _i, _len;
    extensions = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    for (_i = 0, _len = extensions.length; _i < _len; _i++) {
      extension = extensions[_i];
      htmlExtensions.push(extension);
    }
    return htmlExtensions;
  };

  bypassOnLoadPopstate = function(fn) {
    return setTimeout(fn, 500);
  };

  installDocumentReadyPageEventTriggers = function() {
    return document.addEventListener('DOMContentLoaded', (function() {
      triggerEvent('page:change');
      return triggerEvent('page:update');
    }), true);
  };

  installJqueryAjaxSuccessPageUpdateTrigger = function() {
    if (typeof jQuery !== 'undefined') {
      return jQuery(document).on('ajaxSuccess', function(event, xhr, settings) {
        if (!jQuery.trim(xhr.responseText)) {
          return;
        }
        return triggerEvent('page:update');
      });
    }
  };

  installHistoryChangeHandler = function(event) {
    var cachedPage, _ref;
    if ((_ref = event.state) != null ? _ref.turbolinks : void 0) {
      if (cachedPage = pageCache[event.state.url]) {
        cacheCurrentPage();
        return fetchHistory(cachedPage);
      } else {
        return visit(event.target.location.href);
      }
    }
  };

  initializeTurbolinks = function() {
    rememberCurrentUrl();
    rememberCurrentState();
    createDocument = browserCompatibleDocumentParser();
    document.addEventListener('click', installClickHandlerLast, true);
    return bypassOnLoadPopstate(function() {
      return window.addEventListener('popstate', installHistoryChangeHandler, false);
    });
  };

  historyStateIsDefined = window.history.state !== void 0 || navigator.userAgent.match(/Firefox\/2[6|7]/);

  browserSupportsPushState = window.history && window.history.pushState && window.history.replaceState && historyStateIsDefined;

  browserIsntBuggy = !navigator.userAgent.match(/CriOS\//);

  requestMethodIsSafe = (_ref = popCookie('request_method')) === 'GET' || _ref === '';

  browserSupportsTurbolinks = browserSupportsPushState && browserIsntBuggy && requestMethodIsSafe;

  browserSupportsCustomEvents = document.addEventListener && document.createEvent;

  if (browserSupportsCustomEvents) {
    installDocumentReadyPageEventTriggers();
    installJqueryAjaxSuccessPageUpdateTrigger();
  }

  if (browserSupportsTurbolinks) {
    visit = fetch;
    initializeTurbolinks();
  } else {
    visit = function(url) {
      return document.location.href = url;
    };
  }

  this.Turbolinks = {
    visit: visit,
    pagesCached: pagesCached,
    enableTransitionCache: enableTransitionCache,
    allowLinkExtensions: allowLinkExtensions,
    supported: browserSupportsTurbolinks
  };

}).call(this);
//     Underscore.js 1.6.0
//     http://underscorejs.org
//     (c) 2009-2014 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.
(function(){var n=this,t=n._,r={},e=Array.prototype,u=Object.prototype,i=Function.prototype,a=e.push,o=e.slice,c=e.concat,l=u.toString,f=u.hasOwnProperty,s=e.forEach,p=e.map,h=e.reduce,v=e.reduceRight,g=e.filter,d=e.every,m=e.some,y=e.indexOf,b=e.lastIndexOf,x=Array.isArray,w=Object.keys,_=i.bind,j=function(n){return n instanceof j?n:this instanceof j?void(this._wrapped=n):new j(n)};"undefined"!=typeof exports?("undefined"!=typeof module&&module.exports&&(exports=module.exports=j),exports._=j):n._=j,j.VERSION="1.6.0";var A=j.each=j.forEach=function(n,t,e){if(null==n)return n;if(s&&n.forEach===s)n.forEach(t,e);else if(n.length===+n.length){for(var u=0,i=n.length;i>u;u++)if(t.call(e,n[u],u,n)===r)return}else for(var a=j.keys(n),u=0,i=a.length;i>u;u++)if(t.call(e,n[a[u]],a[u],n)===r)return;return n};j.map=j.collect=function(n,t,r){var e=[];return null==n?e:p&&n.map===p?n.map(t,r):(A(n,function(n,u,i){e.push(t.call(r,n,u,i))}),e)};var O="Reduce of empty array with no initial value";j.reduce=j.foldl=j.inject=function(n,t,r,e){var u=arguments.length>2;if(null==n&&(n=[]),h&&n.reduce===h)return e&&(t=j.bind(t,e)),u?n.reduce(t,r):n.reduce(t);if(A(n,function(n,i,a){u?r=t.call(e,r,n,i,a):(r=n,u=!0)}),!u)throw new TypeError(O);return r},j.reduceRight=j.foldr=function(n,t,r,e){var u=arguments.length>2;if(null==n&&(n=[]),v&&n.reduceRight===v)return e&&(t=j.bind(t,e)),u?n.reduceRight(t,r):n.reduceRight(t);var i=n.length;if(i!==+i){var a=j.keys(n);i=a.length}if(A(n,function(o,c,l){c=a?a[--i]:--i,u?r=t.call(e,r,n[c],c,l):(r=n[c],u=!0)}),!u)throw new TypeError(O);return r},j.find=j.detect=function(n,t,r){var e;return k(n,function(n,u,i){return t.call(r,n,u,i)?(e=n,!0):void 0}),e},j.filter=j.select=function(n,t,r){var e=[];return null==n?e:g&&n.filter===g?n.filter(t,r):(A(n,function(n,u,i){t.call(r,n,u,i)&&e.push(n)}),e)},j.reject=function(n,t,r){return j.filter(n,function(n,e,u){return!t.call(r,n,e,u)},r)},j.every=j.all=function(n,t,e){t||(t=j.identity);var u=!0;return null==n?u:d&&n.every===d?n.every(t,e):(A(n,function(n,i,a){return(u=u&&t.call(e,n,i,a))?void 0:r}),!!u)};var k=j.some=j.any=function(n,t,e){t||(t=j.identity);var u=!1;return null==n?u:m&&n.some===m?n.some(t,e):(A(n,function(n,i,a){return u||(u=t.call(e,n,i,a))?r:void 0}),!!u)};j.contains=j.include=function(n,t){return null==n?!1:y&&n.indexOf===y?n.indexOf(t)!=-1:k(n,function(n){return n===t})},j.invoke=function(n,t){var r=o.call(arguments,2),e=j.isFunction(t);return j.map(n,function(n){return(e?t:n[t]).apply(n,r)})},j.pluck=function(n,t){return j.map(n,j.property(t))},j.where=function(n,t){return j.filter(n,j.matches(t))},j.findWhere=function(n,t){return j.find(n,j.matches(t))},j.max=function(n,t,r){if(!t&&j.isArray(n)&&n[0]===+n[0]&&n.length<65535)return Math.max.apply(Math,n);var e=-1/0,u=-1/0;return A(n,function(n,i,a){var o=t?t.call(r,n,i,a):n;o>u&&(e=n,u=o)}),e},j.min=function(n,t,r){if(!t&&j.isArray(n)&&n[0]===+n[0]&&n.length<65535)return Math.min.apply(Math,n);var e=1/0,u=1/0;return A(n,function(n,i,a){var o=t?t.call(r,n,i,a):n;u>o&&(e=n,u=o)}),e},j.shuffle=function(n){var t,r=0,e=[];return A(n,function(n){t=j.random(r++),e[r-1]=e[t],e[t]=n}),e},j.sample=function(n,t,r){return null==t||r?(n.length!==+n.length&&(n=j.values(n)),n[j.random(n.length-1)]):j.shuffle(n).slice(0,Math.max(0,t))};var E=function(n){return null==n?j.identity:j.isFunction(n)?n:j.property(n)};j.sortBy=function(n,t,r){return t=E(t),j.pluck(j.map(n,function(n,e,u){return{value:n,index:e,criteria:t.call(r,n,e,u)}}).sort(function(n,t){var r=n.criteria,e=t.criteria;if(r!==e){if(r>e||r===void 0)return 1;if(e>r||e===void 0)return-1}return n.index-t.index}),"value")};var F=function(n){return function(t,r,e){var u={};return r=E(r),A(t,function(i,a){var o=r.call(e,i,a,t);n(u,o,i)}),u}};j.groupBy=F(function(n,t,r){j.has(n,t)?n[t].push(r):n[t]=[r]}),j.indexBy=F(function(n,t,r){n[t]=r}),j.countBy=F(function(n,t){j.has(n,t)?n[t]++:n[t]=1}),j.sortedIndex=function(n,t,r,e){r=E(r);for(var u=r.call(e,t),i=0,a=n.length;a>i;){var o=i+a>>>1;r.call(e,n[o])<u?i=o+1:a=o}return i},j.toArray=function(n){return n?j.isArray(n)?o.call(n):n.length===+n.length?j.map(n,j.identity):j.values(n):[]},j.size=function(n){return null==n?0:n.length===+n.length?n.length:j.keys(n).length},j.first=j.head=j.take=function(n,t,r){return null==n?void 0:null==t||r?n[0]:0>t?[]:o.call(n,0,t)},j.initial=function(n,t,r){return o.call(n,0,n.length-(null==t||r?1:t))},j.last=function(n,t,r){return null==n?void 0:null==t||r?n[n.length-1]:o.call(n,Math.max(n.length-t,0))},j.rest=j.tail=j.drop=function(n,t,r){return o.call(n,null==t||r?1:t)},j.compact=function(n){return j.filter(n,j.identity)};var M=function(n,t,r){return t&&j.every(n,j.isArray)?c.apply(r,n):(A(n,function(n){j.isArray(n)||j.isArguments(n)?t?a.apply(r,n):M(n,t,r):r.push(n)}),r)};j.flatten=function(n,t){return M(n,t,[])},j.without=function(n){return j.difference(n,o.call(arguments,1))},j.partition=function(n,t){var r=[],e=[];return A(n,function(n){(t(n)?r:e).push(n)}),[r,e]},j.uniq=j.unique=function(n,t,r,e){j.isFunction(t)&&(e=r,r=t,t=!1);var u=r?j.map(n,r,e):n,i=[],a=[];return A(u,function(r,e){(t?e&&a[a.length-1]===r:j.contains(a,r))||(a.push(r),i.push(n[e]))}),i},j.union=function(){return j.uniq(j.flatten(arguments,!0))},j.intersection=function(n){var t=o.call(arguments,1);return j.filter(j.uniq(n),function(n){return j.every(t,function(t){return j.contains(t,n)})})},j.difference=function(n){var t=c.apply(e,o.call(arguments,1));return j.filter(n,function(n){return!j.contains(t,n)})},j.zip=function(){for(var n=j.max(j.pluck(arguments,"length").concat(0)),t=new Array(n),r=0;n>r;r++)t[r]=j.pluck(arguments,""+r);return t},j.object=function(n,t){if(null==n)return{};for(var r={},e=0,u=n.length;u>e;e++)t?r[n[e]]=t[e]:r[n[e][0]]=n[e][1];return r},j.indexOf=function(n,t,r){if(null==n)return-1;var e=0,u=n.length;if(r){if("number"!=typeof r)return e=j.sortedIndex(n,t),n[e]===t?e:-1;e=0>r?Math.max(0,u+r):r}if(y&&n.indexOf===y)return n.indexOf(t,r);for(;u>e;e++)if(n[e]===t)return e;return-1},j.lastIndexOf=function(n,t,r){if(null==n)return-1;var e=null!=r;if(b&&n.lastIndexOf===b)return e?n.lastIndexOf(t,r):n.lastIndexOf(t);for(var u=e?r:n.length;u--;)if(n[u]===t)return u;return-1},j.range=function(n,t,r){arguments.length<=1&&(t=n||0,n=0),r=arguments[2]||1;for(var e=Math.max(Math.ceil((t-n)/r),0),u=0,i=new Array(e);e>u;)i[u++]=n,n+=r;return i};var R=function(){};j.bind=function(n,t){var r,e;if(_&&n.bind===_)return _.apply(n,o.call(arguments,1));if(!j.isFunction(n))throw new TypeError;return r=o.call(arguments,2),e=function(){if(!(this instanceof e))return n.apply(t,r.concat(o.call(arguments)));R.prototype=n.prototype;var u=new R;R.prototype=null;var i=n.apply(u,r.concat(o.call(arguments)));return Object(i)===i?i:u}},j.partial=function(n){var t=o.call(arguments,1);return function(){for(var r=0,e=t.slice(),u=0,i=e.length;i>u;u++)e[u]===j&&(e[u]=arguments[r++]);for(;r<arguments.length;)e.push(arguments[r++]);return n.apply(this,e)}},j.bindAll=function(n){var t=o.call(arguments,1);if(0===t.length)throw new Error("bindAll must be passed function names");return A(t,function(t){n[t]=j.bind(n[t],n)}),n},j.memoize=function(n,t){var r={};return t||(t=j.identity),function(){var e=t.apply(this,arguments);return j.has(r,e)?r[e]:r[e]=n.apply(this,arguments)}},j.delay=function(n,t){var r=o.call(arguments,2);return setTimeout(function(){return n.apply(null,r)},t)},j.defer=function(n){return j.delay.apply(j,[n,1].concat(o.call(arguments,1)))},j.throttle=function(n,t,r){var e,u,i,a=null,o=0;r||(r={});var c=function(){o=r.leading===!1?0:j.now(),a=null,i=n.apply(e,u),e=u=null};return function(){var l=j.now();o||r.leading!==!1||(o=l);var f=t-(l-o);return e=this,u=arguments,0>=f?(clearTimeout(a),a=null,o=l,i=n.apply(e,u),e=u=null):a||r.trailing===!1||(a=setTimeout(c,f)),i}},j.debounce=function(n,t,r){var e,u,i,a,o,c=function(){var l=j.now()-a;t>l?e=setTimeout(c,t-l):(e=null,r||(o=n.apply(i,u),i=u=null))};return function(){i=this,u=arguments,a=j.now();var l=r&&!e;return e||(e=setTimeout(c,t)),l&&(o=n.apply(i,u),i=u=null),o}},j.once=function(n){var t,r=!1;return function(){return r?t:(r=!0,t=n.apply(this,arguments),n=null,t)}},j.wrap=function(n,t){return j.partial(t,n)},j.compose=function(){var n=arguments;return function(){for(var t=arguments,r=n.length-1;r>=0;r--)t=[n[r].apply(this,t)];return t[0]}},j.after=function(n,t){return function(){return--n<1?t.apply(this,arguments):void 0}},j.keys=function(n){if(!j.isObject(n))return[];if(w)return w(n);var t=[];for(var r in n)j.has(n,r)&&t.push(r);return t},j.values=function(n){for(var t=j.keys(n),r=t.length,e=new Array(r),u=0;r>u;u++)e[u]=n[t[u]];return e},j.pairs=function(n){for(var t=j.keys(n),r=t.length,e=new Array(r),u=0;r>u;u++)e[u]=[t[u],n[t[u]]];return e},j.invert=function(n){for(var t={},r=j.keys(n),e=0,u=r.length;u>e;e++)t[n[r[e]]]=r[e];return t},j.functions=j.methods=function(n){var t=[];for(var r in n)j.isFunction(n[r])&&t.push(r);return t.sort()},j.extend=function(n){return A(o.call(arguments,1),function(t){if(t)for(var r in t)n[r]=t[r]}),n},j.pick=function(n){var t={},r=c.apply(e,o.call(arguments,1));return A(r,function(r){r in n&&(t[r]=n[r])}),t},j.omit=function(n){var t={},r=c.apply(e,o.call(arguments,1));for(var u in n)j.contains(r,u)||(t[u]=n[u]);return t},j.defaults=function(n){return A(o.call(arguments,1),function(t){if(t)for(var r in t)n[r]===void 0&&(n[r]=t[r])}),n},j.clone=function(n){return j.isObject(n)?j.isArray(n)?n.slice():j.extend({},n):n},j.tap=function(n,t){return t(n),n};var S=function(n,t,r,e){if(n===t)return 0!==n||1/n==1/t;if(null==n||null==t)return n===t;n instanceof j&&(n=n._wrapped),t instanceof j&&(t=t._wrapped);var u=l.call(n);if(u!=l.call(t))return!1;switch(u){case"[object String]":return n==String(t);case"[object Number]":return n!=+n?t!=+t:0==n?1/n==1/t:n==+t;case"[object Date]":case"[object Boolean]":return+n==+t;case"[object RegExp]":return n.source==t.source&&n.global==t.global&&n.multiline==t.multiline&&n.ignoreCase==t.ignoreCase}if("object"!=typeof n||"object"!=typeof t)return!1;for(var i=r.length;i--;)if(r[i]==n)return e[i]==t;var a=n.constructor,o=t.constructor;if(a!==o&&!(j.isFunction(a)&&a instanceof a&&j.isFunction(o)&&o instanceof o)&&"constructor"in n&&"constructor"in t)return!1;r.push(n),e.push(t);var c=0,f=!0;if("[object Array]"==u){if(c=n.length,f=c==t.length)for(;c--&&(f=S(n[c],t[c],r,e)););}else{for(var s in n)if(j.has(n,s)&&(c++,!(f=j.has(t,s)&&S(n[s],t[s],r,e))))break;if(f){for(s in t)if(j.has(t,s)&&!c--)break;f=!c}}return r.pop(),e.pop(),f};j.isEqual=function(n,t){return S(n,t,[],[])},j.isEmpty=function(n){if(null==n)return!0;if(j.isArray(n)||j.isString(n))return 0===n.length;for(var t in n)if(j.has(n,t))return!1;return!0},j.isElement=function(n){return!(!n||1!==n.nodeType)},j.isArray=x||function(n){return"[object Array]"==l.call(n)},j.isObject=function(n){return n===Object(n)},A(["Arguments","Function","String","Number","Date","RegExp"],function(n){j["is"+n]=function(t){return l.call(t)=="[object "+n+"]"}}),j.isArguments(arguments)||(j.isArguments=function(n){return!(!n||!j.has(n,"callee"))}),"function"!=typeof/./&&(j.isFunction=function(n){return"function"==typeof n}),j.isFinite=function(n){return isFinite(n)&&!isNaN(parseFloat(n))},j.isNaN=function(n){return j.isNumber(n)&&n!=+n},j.isBoolean=function(n){return n===!0||n===!1||"[object Boolean]"==l.call(n)},j.isNull=function(n){return null===n},j.isUndefined=function(n){return n===void 0},j.has=function(n,t){return f.call(n,t)},j.noConflict=function(){return n._=t,this},j.identity=function(n){return n},j.constant=function(n){return function(){return n}},j.property=function(n){return function(t){return t[n]}},j.matches=function(n){return function(t){if(t===n)return!0;for(var r in n)if(n[r]!==t[r])return!1;return!0}},j.times=function(n,t,r){for(var e=Array(Math.max(0,n)),u=0;n>u;u++)e[u]=t.call(r,u);return e},j.random=function(n,t){return null==t&&(t=n,n=0),n+Math.floor(Math.random()*(t-n+1))},j.now=Date.now||function(){return(new Date).getTime()};var T={escape:{"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;"}};T.unescape=j.invert(T.escape);var I={escape:new RegExp("["+j.keys(T.escape).join("")+"]","g"),unescape:new RegExp("("+j.keys(T.unescape).join("|")+")","g")};j.each(["escape","unescape"],function(n){j[n]=function(t){return null==t?"":(""+t).replace(I[n],function(t){return T[n][t]})}}),j.result=function(n,t){if(null==n)return void 0;var r=n[t];return j.isFunction(r)?r.call(n):r},j.mixin=function(n){A(j.functions(n),function(t){var r=j[t]=n[t];j.prototype[t]=function(){var n=[this._wrapped];return a.apply(n,arguments),z.call(this,r.apply(j,n))}})};var N=0;j.uniqueId=function(n){var t=++N+"";return n?n+t:t},j.templateSettings={evaluate:/<%([\s\S]+?)%>/g,interpolate:/<%=([\s\S]+?)%>/g,escape:/<%-([\s\S]+?)%>/g};var q=/(.)^/,B={"'":"'","\\":"\\","\r":"r","\n":"n","	":"t","\u2028":"u2028","\u2029":"u2029"},D=/\\|'|\r|\n|\t|\u2028|\u2029/g;j.template=function(n,t,r){var e;r=j.defaults({},r,j.templateSettings);var u=new RegExp([(r.escape||q).source,(r.interpolate||q).source,(r.evaluate||q).source].join("|")+"|$","g"),i=0,a="__p+='";n.replace(u,function(t,r,e,u,o){return a+=n.slice(i,o).replace(D,function(n){return"\\"+B[n]}),r&&(a+="'+\n((__t=("+r+"))==null?'':_.escape(__t))+\n'"),e&&(a+="'+\n((__t=("+e+"))==null?'':__t)+\n'"),u&&(a+="';\n"+u+"\n__p+='"),i=o+t.length,t}),a+="';\n",r.variable||(a="with(obj||{}){\n"+a+"}\n"),a="var __t,__p='',__j=Array.prototype.join,"+"print=function(){__p+=__j.call(arguments,'');};\n"+a+"return __p;\n";try{e=new Function(r.variable||"obj","_",a)}catch(o){throw o.source=a,o}if(t)return e(t,j);var c=function(n){return e.call(this,n,j)};return c.source="function("+(r.variable||"obj")+"){\n"+a+"}",c},j.chain=function(n){return j(n).chain()};var z=function(n){return this._chain?j(n).chain():n};j.mixin(j),A(["pop","push","reverse","shift","sort","splice","unshift"],function(n){var t=e[n];j.prototype[n]=function(){var r=this._wrapped;return t.apply(r,arguments),"shift"!=n&&"splice"!=n||0!==r.length||delete r[0],z.call(this,r)}}),A(["concat","join","slice"],function(n){var t=e[n];j.prototype[n]=function(){return z.call(this,t.apply(this._wrapped,arguments))}}),j.extend(j.prototype,{chain:function(){return this._chain=!0,this},value:function(){return this._wrapped}}),"function"==typeof define&&define.amd&&define("underscore",[],function(){return j})}).call(this);
(function() {
  this.Gmaps = {
    build: function(type, options) {
      var model;
      if (options == null) {
        options = {};
      }
      model = _.isFunction(options.handler) ? options.handler : Gmaps.Objects.Handler;
      return new model(type, options);
    },
    Builders: {},
    Objects: {},
    Google: {
      Objects: {},
      Builders: {}
    }
  };

}).call(this);
(function() {
  var moduleKeywords,
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  moduleKeywords = ['extended', 'included'];

  this.Gmaps.Base = (function() {
    function Base() {}

    Base.extend = function(obj) {
      var key, value, _ref;
      for (key in obj) {
        value = obj[key];
        if (__indexOf.call(moduleKeywords, key) < 0) {
          this[key] = value;
        }
      }
      if ((_ref = obj.extended) != null) {
        _ref.apply(this);
      }
      return this;
    };

    Base.include = function(obj) {
      var key, value, _ref;
      for (key in obj) {
        value = obj[key];
        if (__indexOf.call(moduleKeywords, key) < 0) {
          this.prototype[key] = value;
        }
      }
      if ((_ref = obj.included) != null) {
        _ref.apply(this);
      }
      return this;
    };

    return Base;

  })();

}).call(this);
(function() {
  this.Gmaps.Objects.BaseBuilder = (function() {
    function BaseBuilder() {}

    BaseBuilder.prototype.build = function() {
      return new (this.model_class())(this.serviceObject);
    };

    BaseBuilder.prototype.before_init = function() {};

    BaseBuilder.prototype.after_init = function() {};

    BaseBuilder.prototype.addListener = function(action, fn) {
      return this.primitives().addListener(this.getServiceObject(), action, fn);
    };

    BaseBuilder.prototype.getServiceObject = function() {
      return this.serviceObject;
    };

    BaseBuilder.prototype.primitives = function() {
      return this.constructor.PRIMITIVES;
    };

    BaseBuilder.prototype.model_class = function() {
      return this.constructor.OBJECT;
    };

    return BaseBuilder;

  })();

}).call(this);
(function() {
  this.Gmaps.Objects.Builders = function(builderClass, objectClass, primitivesProvider) {
    return {
      build: function(args, provider_options, internal_options) {
        var builder;
        objectClass.PRIMITIVES = primitivesProvider;
        builderClass.OBJECT = objectClass;
        builderClass.PRIMITIVES = primitivesProvider;
        builder = new builderClass(args, provider_options, internal_options);
        return builder.build();
      }
    };
  };

}).call(this);
(function() {
  this.Gmaps.Objects.Handler = (function() {
    function Handler(type, options) {
      this.type = type;
      if (options == null) {
        options = {};
      }
      this.setPrimitives(options);
      this.setOptions(options);
      this._cacheAllBuilders();
      this.resetBounds();
    }

    Handler.prototype.buildMap = function(options, onMapLoad) {
      if (onMapLoad == null) {
        onMapLoad = function() {};
      }
      return this.map = this._builder('Map').build(options, (function(_this) {
        return function() {
          _this._createClusterer();
          return onMapLoad();
        };
      })(this));
    };

    Handler.prototype.addMarkers = function(markers_data, provider_options) {
      return _.map(markers_data, (function(_this) {
        return function(marker_data) {
          return _this.addMarker(marker_data, provider_options);
        };
      })(this));
    };

    Handler.prototype.addMarker = function(marker_data, provider_options) {
      var marker;
      marker = this._builder('Marker').build(marker_data, provider_options, this.marker_options);
      marker.setMap(this.getMap());
      this.clusterer.addMarker(marker);
      return marker;
    };

    Handler.prototype.addCircles = function(circles_data, provider_options) {
      return _.map(circles_data, (function(_this) {
        return function(circle_data) {
          return _this.addCircle(circle_data, provider_options);
        };
      })(this));
    };

    Handler.prototype.addCircle = function(circle_data, provider_options) {
      return this._addResource('circle', circle_data, provider_options);
    };

    Handler.prototype.addPolylines = function(polylines_data, provider_options) {
      return _.map(polylines_data, (function(_this) {
        return function(polyline_data) {
          return _this.addPolyline(polyline_data, provider_options);
        };
      })(this));
    };

    Handler.prototype.addPolyline = function(polyline_data, provider_options) {
      return this._addResource('polyline', polyline_data, provider_options);
    };

    Handler.prototype.addPolygons = function(polygons_data, provider_options) {
      return _.map(polygons_data, (function(_this) {
        return function(polygon_data) {
          return _this.addPolygon(polygon_data, provider_options);
        };
      })(this));
    };

    Handler.prototype.addPolygon = function(polygon_data, provider_options) {
      return this._addResource('polygon', polygon_data, provider_options);
    };

    Handler.prototype.addKmls = function(kmls_data, provider_options) {
      return _.map(kmls_data, (function(_this) {
        return function(kml_data) {
          return _this.addKml(kml_data, provider_options);
        };
      })(this));
    };

    Handler.prototype.addKml = function(kml_data, provider_options) {
      return this._addResource('kml', kml_data, provider_options);
    };

    Handler.prototype.removeMarkers = function(gem_markers) {
      return _.map(gem_markers, (function(_this) {
        return function(gem_marker) {
          return _this.removeMarker(gem_marker);
        };
      })(this));
    };

    Handler.prototype.removeMarker = function(gem_marker) {
      gem_marker.clear();
      return this.clusterer.removeMarker(gem_marker);
    };

    Handler.prototype.fitMapToBounds = function() {
      return this.map.fitToBounds(this.bounds.getServiceObject());
    };

    Handler.prototype.getMap = function() {
      return this.map.getServiceObject();
    };

    Handler.prototype.setOptions = function(options) {
      this.marker_options = _.extend(this._default_marker_options(), options.markers);
      this.builders = _.extend(this._default_builders(), options.builders);
      return this.models = _.extend(this._default_models(), options.models);
    };

    Handler.prototype.resetBounds = function() {
      return this.bounds = this._builder('Bound').build();
    };

    Handler.prototype.setPrimitives = function(options) {
      return this.primitives = options.primitives === void 0 ? this._rootModule().Primitives() : _.isFunction(options.primitives) ? options.primitives() : options.primitives;
    };

    Handler.prototype.currentInfowindow = function() {
      return this.builders.Marker.CURRENT_INFOWINDOW;
    };

    Handler.prototype._addResource = function(resource_name, resource_data, provider_options) {
      var resource;
      resource = this._builder(resource_name).build(resource_data, provider_options);
      resource.setMap(this.getMap());
      return resource;
    };

    Handler.prototype._cacheAllBuilders = function() {
      var that;
      that = this;
      return _.each(['Bound', 'Circle', 'Clusterer', 'Kml', 'Map', 'Marker', 'Polygon', 'Polyline'], function(kind) {
        return that._builder(kind);
      });
    };

    Handler.prototype._clusterize = function() {
      return _.isObject(this.marker_options.clusterer);
    };

    Handler.prototype._createClusterer = function() {
      return this.clusterer = this._builder('Clusterer').build({
        map: this.getMap()
      }, this.marker_options.clusterer);
    };

    Handler.prototype._default_marker_options = function() {
      return _.clone({
        singleInfowindow: true,
        maxRandomDistance: 0,
        clusterer: {
          maxZoom: 5,
          gridSize: 50
        }
      });
    };

    Handler.prototype._builder = function(name) {
      var _name;
      name = this._capitalize(name);
      if (this[_name = "__builder" + name] == null) {
        this[_name] = Gmaps.Objects.Builders(this.builders[name], this.models[name], this.primitives);
      }
      return this["__builder" + name];
    };

    Handler.prototype._default_models = function() {
      var models;
      models = _.clone(this._rootModule().Objects);
      if (this._clusterize()) {
        return models;
      } else {
        models.Clusterer = Gmaps.Objects.NullClusterer;
        return models;
      }
    };

    Handler.prototype._capitalize = function(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    };

    Handler.prototype._default_builders = function() {
      return _.clone(this._rootModule().Builders);
    };

    Handler.prototype._rootModule = function() {
      if (this.__rootModule == null) {
        this.__rootModule = Gmaps[this.type];
      }
      return this.__rootModule;
    };

    return Handler;

  })();

}).call(this);
(function() {
  this.Gmaps.Objects.NullClusterer = (function() {
    function NullClusterer() {}

    NullClusterer.prototype.addMarkers = function() {};

    NullClusterer.prototype.addMarker = function() {};

    NullClusterer.prototype.clear = function() {};

    NullClusterer.prototype.removeMarker = function() {};

    return NullClusterer;

  })();

}).call(this);
(function() {
  this.Gmaps.Google.Objects.Common = {
    getServiceObject: function() {
      return this.serviceObject;
    },
    setMap: function(map) {
      return this.getServiceObject().setMap(map);
    },
    clear: function() {
      return this.getServiceObject().setMap(null);
    },
    show: function() {
      return this.getServiceObject().setVisible(true);
    },
    hide: function() {
      return this.getServiceObject().setVisible(false);
    },
    isVisible: function() {
      return this.getServiceObject().getVisible();
    },
    primitives: function() {
      return this.constructor.PRIMITIVES;
    }
  };

}).call(this);
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Builders.Bound = (function(_super) {
    __extends(Bound, _super);

    function Bound(options) {
      this.before_init();
      this.serviceObject = new (this.primitives().latLngBounds);
      this.after_init();
    }

    return Bound;

  })(Gmaps.Objects.BaseBuilder);

}).call(this);
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Builders.Circle = (function(_super) {
    __extends(Circle, _super);

    function Circle(args, provider_options) {
      this.args = args;
      this.provider_options = provider_options != null ? provider_options : {};
      this.before_init();
      this.serviceObject = this.create_circle();
      this.after_init();
    }

    Circle.prototype.create_circle = function() {
      return new (this.primitives().circle)(this.circle_options());
    };

    Circle.prototype.circle_options = function() {
      var base_options;
      base_options = {
        center: new (this.primitives().latLng)(this.args.lat, this.args.lng),
        radius: this.args.radius
      };
      return _.defaults(base_options, this.provider_options);
    };

    return Circle;

  })(Gmaps.Objects.BaseBuilder);

}).call(this);
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Builders.Clusterer = (function(_super) {
    __extends(Clusterer, _super);

    function Clusterer(args, options) {
      this.args = args;
      this.options = options;
      this.before_init();
      this.serviceObject = new (this.primitives().clusterer)(this.args.map, [], this.options);
      this.after_init();
    }

    return Clusterer;

  })(Gmaps.Objects.BaseBuilder);

}).call(this);
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Builders.Kml = (function(_super) {
    __extends(Kml, _super);

    function Kml(args, provider_options) {
      this.args = args;
      this.provider_options = provider_options != null ? provider_options : {};
      this.before_init();
      this.serviceObject = this.create_kml();
      this.after_init();
    }

    Kml.prototype.create_kml = function() {
      return new (this.primitives().kml)(this.args.url, this.kml_options());
    };

    Kml.prototype.kml_options = function() {
      var base_options;
      base_options = {};
      return _.defaults(base_options, this.provider_options);
    };

    return Kml;

  })(Gmaps.Objects.BaseBuilder);

}).call(this);
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Builders.Map = (function(_super) {
    __extends(Map, _super);

    function Map(options, onMapLoad) {
      var provider_options;
      this.before_init();
      provider_options = _.extend(this.default_options(), options.provider);
      this.internal_options = options.internal;
      this.serviceObject = new (this.primitives().map)(document.getElementById(this.internal_options.id), provider_options);
      this.on_map_load(onMapLoad);
      this.after_init();
    }

    Map.prototype.build = function() {
      return new (this.model_class())(this.serviceObject, this.primitives());
    };

    Map.prototype.on_map_load = function(onMapLoad) {
      return this.primitives().addListenerOnce(this.serviceObject, 'idle', onMapLoad);
    };

    Map.prototype.default_options = function() {
      return {
        mapTypeId: this.primitives().mapTypes('ROADMAP'),
        center: new (this.primitives().latLng)(0, 0),
        zoom: 8
      };
    };

    return Map;

  })(Gmaps.Objects.BaseBuilder);

}).call(this);
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Builders.Marker = (function(_super) {
    __extends(Marker, _super);

    Marker.CURRENT_INFOWINDOW = void 0;

    Marker.CACHE_STORE = {};

    function Marker(args, provider_options, internal_options) {
      this.args = args;
      this.provider_options = provider_options != null ? provider_options : {};
      this.internal_options = internal_options != null ? internal_options : {};
      this.infowindow_binding = __bind(this.infowindow_binding, this);
      this.before_init();
      this.create_marker();
      this.create_infowindow_on_click();
      this.after_init();
    }

    Marker.prototype.build = function() {
      return this.marker = new (this.model_class())(this.serviceObject);
    };

    Marker.prototype.create_marker = function() {
      return this.serviceObject = new (this.primitives().marker)(this.marker_options());
    };

    Marker.prototype.create_infowindow = function() {
      if (!_.isString(this.args.infowindow)) {
        return null;
      }
      return new (this.primitives().infowindow)({
        content: this.args.infowindow
      });
    };

    Marker.prototype.marker_options = function() {
      var base_options, coords;
      coords = this._randomized_coordinates();
      base_options = {
        title: this.args.marker_title,
        position: new (this.primitives().latLng)(coords[0], coords[1]),
        icon: this._get_picture('picture'),
        shadow: this._get_picture('shadow')
      };
      return _.extend(this.provider_options, base_options);
    };

    Marker.prototype.create_infowindow_on_click = function() {
      return this.addListener('click', this.infowindow_binding);
    };

    Marker.prototype.infowindow_binding = function() {
      var _base;
      if (this._should_close_infowindow()) {
        this.constructor.CURRENT_INFOWINDOW.close();
      }
      this.marker.panTo();
      if (this.infowindow == null) {
        this.infowindow = this.create_infowindow();
      }
      if (this.infowindow == null) {
        return;
      }
      this.infowindow.open(this.getServiceObject().getMap(), this.getServiceObject());
      if ((_base = this.marker).infowindow == null) {
        _base.infowindow = this.infowindow;
      }
      return this.constructor.CURRENT_INFOWINDOW = this.infowindow;
    };

    Marker.prototype._get_picture = function(picture_name) {
      if (!_.isObject(this.args[picture_name]) || !_.isString(this.args[picture_name].url)) {
        return null;
      }
      return this._create_or_retrieve_image(this._picture_args(picture_name));
    };

    Marker.prototype._create_or_retrieve_image = function(picture_args) {
      if (this.constructor.CACHE_STORE[picture_args.url] === void 0) {
        this.constructor.CACHE_STORE[picture_args.url] = new (this.primitives().markerImage)(picture_args.url, picture_args.size, picture_args.origin, picture_args.anchor, picture_args.scaledSize);
      }
      return this.constructor.CACHE_STORE[picture_args.url];
    };

    Marker.prototype._picture_args = function(picture_name) {
      return {
        url: this.args[picture_name].url,
        anchor: this._createImageAnchorPosition(this.args[picture_name].anchor),
        size: new (this.primitives().size)(this.args[picture_name].width, this.args[picture_name].height),
        scaledSize: null,
        origin: null
      };
    };

    Marker.prototype._createImageAnchorPosition = function(anchorLocation) {
      if (!_.isArray(anchorLocation)) {
        return null;
      }
      return new (this.primitives().point)(anchorLocation[0], anchorLocation[1]);
    };

    Marker.prototype._should_close_infowindow = function() {
      return this.internal_options.singleInfowindow && (this.constructor.CURRENT_INFOWINDOW != null);
    };

    Marker.prototype._randomized_coordinates = function() {
      var Lat, Lng, dx, dy, random;
      if (!_.isNumber(this.internal_options.maxRandomDistance)) {
        return [this.args.lat, this.args.lng];
      }
      random = function() {
        return Math.random() * 2 - 1;
      };
      dx = this.internal_options.maxRandomDistance * random();
      dy = this.internal_options.maxRandomDistance * random();
      Lat = parseFloat(this.args.lat) + (180 / Math.PI) * (dy / 6378137);
      Lng = parseFloat(this.args.lng) + (90 / Math.PI) * (dx / 6378137) / Math.cos(this.args.lat);
      return [Lat, Lng];
    };

    return Marker;

  })(Gmaps.Objects.BaseBuilder);

}).call(this);
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Builders.Polygon = (function(_super) {
    __extends(Polygon, _super);

    function Polygon(args, provider_options) {
      this.args = args;
      this.provider_options = provider_options != null ? provider_options : {};
      this.before_init();
      this.serviceObject = this.create_polygon();
      this.after_init();
    }

    Polygon.prototype.create_polygon = function() {
      return new (this.primitives().polygon)(this.polygon_options());
    };

    Polygon.prototype.polygon_options = function() {
      var base_options;
      base_options = {
        path: this._build_path()
      };
      return _.defaults(base_options, this.provider_options);
    };

    Polygon.prototype._build_path = function() {
      return _.map(this.args, (function(_this) {
        return function(arg) {
          return new (_this.primitives().latLng)(arg.lat, arg.lng);
        };
      })(this));
    };

    return Polygon;

  })(Gmaps.Objects.BaseBuilder);

}).call(this);
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Builders.Polyline = (function(_super) {
    __extends(Polyline, _super);

    function Polyline(args, provider_options) {
      this.args = args;
      this.provider_options = provider_options != null ? provider_options : {};
      this.before_init();
      this.serviceObject = this.create_polyline();
      this.after_init();
    }

    Polyline.prototype.create_polyline = function() {
      return new (this.primitives().polyline)(this.polyline_options());
    };

    Polyline.prototype.polyline_options = function() {
      var base_options;
      base_options = {
        path: this._build_path()
      };
      return _.defaults(base_options, this.provider_options);
    };

    Polyline.prototype._build_path = function() {
      return _.map(this.args, (function(_this) {
        return function(arg) {
          return new (_this.primitives().latLng)(arg.lat, arg.lng);
        };
      })(this));
    };

    return Polyline;

  })(Gmaps.Objects.BaseBuilder);

}).call(this);
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Objects.Bound = (function(_super) {
    __extends(Bound, _super);

    Bound.include(Gmaps.Google.Objects.Common);

    function Bound(serviceObject) {
      this.serviceObject = serviceObject;
    }

    Bound.prototype.extendWith = function(array_or_object) {
      var collection;
      collection = _.isArray(array_or_object) ? array_or_object : [array_or_object];
      return _.each(collection, (function(_this) {
        return function(object) {
          return object.updateBounds(_this);
        };
      })(this));
    };

    Bound.prototype.extend = function(value) {
      return this.getServiceObject().extend(this.primitives().latLngFromPosition(value));
    };

    return Bound;

  })(Gmaps.Base);

}).call(this);
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Objects.Circle = (function(_super) {
    __extends(Circle, _super);

    Circle.include(Gmaps.Google.Objects.Common);

    function Circle(serviceObject) {
      this.serviceObject = serviceObject;
    }

    Circle.prototype.updateBounds = function(bounds) {
      bounds.extend(this.getServiceObject().getBounds().getNorthEast());
      return bounds.extend(this.getServiceObject().getBounds().getSouthWest());
    };

    return Circle;

  })(Gmaps.Base);

}).call(this);
(function() {
  this.Gmaps.Google.Objects.Clusterer = (function() {
    function Clusterer(serviceObject) {
      this.serviceObject = serviceObject;
    }

    Clusterer.prototype.addMarkers = function(markers) {
      return _.each(markers, (function(_this) {
        return function(marker) {
          return _this.addMarker(marker);
        };
      })(this));
    };

    Clusterer.prototype.addMarker = function(marker) {
      return this.getServiceObject().addMarker(marker.getServiceObject());
    };

    Clusterer.prototype.clear = function() {
      return this.getServiceObject().clearMarkers();
    };

    Clusterer.prototype.removeMarker = function(marker) {
      return this.getServiceObject().removeMarker(marker.getServiceObject());
    };

    Clusterer.prototype.getServiceObject = function() {
      return this.serviceObject;
    };

    return Clusterer;

  })();

}).call(this);
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Objects.Kml = (function(_super) {
    __extends(Kml, _super);

    function Kml(serviceObject) {
      this.serviceObject = serviceObject;
    }

    Kml.prototype.updateBounds = function(bounds) {};

    Kml.prototype.setMap = function(map) {
      return this.getServiceObject().setMap(map);
    };

    Kml.prototype.getServiceObject = function() {
      return this.serviceObject;
    };

    Kml.prototype.primitives = function() {
      return this.constructor.PRIMITIVES;
    };

    return Kml;

  })(Gmaps.Base);

}).call(this);
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Objects.Map = (function(_super) {
    __extends(Map, _super);

    function Map(serviceObject) {
      this.serviceObject = serviceObject;
    }

    Map.prototype.getServiceObject = function() {
      return this.serviceObject;
    };

    Map.prototype.centerOn = function(position) {
      return this.getServiceObject().setCenter(this.primitives().latLngFromPosition(position));
    };

    Map.prototype.fitToBounds = function(boundsObject) {
      if (!boundsObject.isEmpty()) {
        return this.getServiceObject().fitBounds(boundsObject);
      }
    };

    Map.prototype.primitives = function() {
      return this.constructor.PRIMITIVES;
    };

    return Map;

  })(Gmaps.Base);

}).call(this);
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Objects.Marker = (function(_super) {
    __extends(Marker, _super);

    Marker.include(Gmaps.Google.Objects.Common);

    function Marker(serviceObject) {
      this.serviceObject = serviceObject;
    }

    Marker.prototype.updateBounds = function(bounds) {
      return bounds.extend(this.getServiceObject().position);
    };

    Marker.prototype.panTo = function() {
      return this.getServiceObject().getMap().panTo(this.getServiceObject().getPosition());
    };

    return Marker;

  })(Gmaps.Base);

}).call(this);
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Objects.Polygon = (function(_super) {
    __extends(Polygon, _super);

    Polygon.include(Gmaps.Google.Objects.Common);

    function Polygon(serviceObject) {
      this.serviceObject = serviceObject;
    }

    Polygon.prototype.updateBounds = function(bounds) {
      var ll, _i, _len, _ref, _results;
      _ref = this.serviceObject.getPath().getArray();
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        ll = _ref[_i];
        _results.push(bounds.extend(ll));
      }
      return _results;
    };

    return Polygon;

  })(Gmaps.Base);

}).call(this);
(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Gmaps.Google.Objects.Polyline = (function(_super) {
    __extends(Polyline, _super);

    Polyline.include(Gmaps.Google.Objects.Common);

    function Polyline(serviceObject) {
      this.serviceObject = serviceObject;
    }

    Polyline.prototype.updateBounds = function(bounds) {
      var ll, _i, _len, _ref, _results;
      _ref = this.serviceObject.getPath().getArray();
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        ll = _ref[_i];
        _results.push(bounds.extend(ll));
      }
      return _results;
    };

    return Polyline;

  })(Gmaps.Base);

}).call(this);
(function() {
  this.Gmaps.Google.Primitives = function() {
    var factory;
    factory = {
      point: google.maps.Point,
      size: google.maps.Size,
      circle: google.maps.Circle,
      latLng: google.maps.LatLng,
      latLngBounds: google.maps.LatLngBounds,
      map: google.maps.Map,
      mapTypez: google.maps.MapTypeId,
      markerImage: google.maps.MarkerImage,
      marker: google.maps.Marker,
      infowindow: google.maps.InfoWindow,
      listener: google.maps.event.addListener,
      clusterer: MarkerClusterer,
      listenerOnce: google.maps.event.addListenerOnce,
      polyline: google.maps.Polyline,
      polygon: google.maps.Polygon,
      kml: google.maps.KmlLayer,
      addListener: function(object, event_name, fn) {
        return factory.listener(object, event_name, fn);
      },
      addListenerOnce: function(object, event_name, fn) {
        return factory.listenerOnce(object, event_name, fn);
      },
      mapTypes: function(type) {
        return factory.mapTypez[type];
      },
      latLngFromPosition: function(position) {
        if (_.isArray(position)) {
          return new factory.latLng(position[0], position[1]);
        } else {
          if (_.isNumber(position.lat) && _.isNumber(position.lng)) {
            return new factory.latLng(position.lat, position.lng);
          } else {
            if (_.isFunction(position.getServiceObject)) {
              return position.getServiceObject().getPosition();
            } else {
              return position;
            }
          }
        }
      }
    };
    return factory;
  };

}).call(this);
(function() {


}).call(this);
(function($) {
  window.NestedFormEvents = function() {
    this.addFields = $.proxy(this.addFields, this);
    this.removeFields = $.proxy(this.removeFields, this);
  };

  NestedFormEvents.prototype = {
    addFields: function(e) {
      // Setup
      var link      = e.currentTarget;
      var assoc     = $(link).data('association');                // Name of child
      var blueprint = $('#' + $(link).data('blueprint-id'));
      var content   = blueprint.data('blueprint');                // Fields template

      // Make the context correct by replacing <parents> with the generated ID
      // of each of the parent objects
      var context = ($(link).closest('.fields').closestChild('input, textarea, select').eq(0).attr('name') || '').replace(new RegExp('\[[a-z_]+\]$'), '');

      // context will be something like this for a brand new form:
      // project[tasks_attributes][1255929127459][assignments_attributes][1255929128105]
      // or for an edit form:
      // project[tasks_attributes][0][assignments_attributes][1]
      if (context) {
        var parentNames = context.match(/[a-z_]+_attributes(?=\]\[(new_)?\d+\])/g) || [];
        var parentIds   = context.match(/[0-9]+/g) || [];

        for(var i = 0; i < parentNames.length; i++) {
          if(parentIds[i]) {
            content = content.replace(
              new RegExp('(_' + parentNames[i] + ')_.+?_', 'g'),
              '$1_' + parentIds[i] + '_');

            content = content.replace(
              new RegExp('(\\[' + parentNames[i] + '\\])\\[.+?\\]', 'g'),
              '$1[' + parentIds[i] + ']');
          }
        }
      }

      // Make a unique ID for the new child
      var regexp  = new RegExp('new_' + assoc, 'g');
      var new_id  = this.newId();
      content     = $.trim(content.replace(regexp, new_id));

      var field = this.insertFields(content, assoc, link);
      // bubble up event upto document (through form)
      field
        .trigger({ type: 'nested:fieldAdded', field: field })
        .trigger({ type: 'nested:fieldAdded:' + assoc, field: field });
      return false;
    },
    newId: function() {
      return new Date().getTime();
    },
    insertFields: function(content, assoc, link) {
      var target = $(link).data('target');
      if (target) {
        return $(content).appendTo($(target));
      } else {
        return $(content).insertBefore(link);
      }
    },
    removeFields: function(e) {
      var $link = $(e.currentTarget),
          assoc = $link.data('association'); // Name of child to be removed
      
      var hiddenField = $link.prev('input[type=hidden]');
      hiddenField.val('1');
      
      var field = $link.closest('.fields');
      field.hide();
      
      field
        .trigger({ type: 'nested:fieldRemoved', field: field })
        .trigger({ type: 'nested:fieldRemoved:' + assoc, field: field });
      return false;
    }
  };

  window.nestedFormEvents = new NestedFormEvents();
  $(document)
    .delegate('form a.add_nested_fields',    'click', nestedFormEvents.addFields)
    .delegate('form a.remove_nested_fields', 'click', nestedFormEvents.removeFields);
})(jQuery);

// http://plugins.jquery.com/project/closestChild
/*
 * Copyright 2011, Tobias Lindig
 *
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 */
(function($) {
        $.fn.closestChild = function(selector) {
                // breadth first search for the first matched node
                if (selector && selector != '') {
                        var queue = [];
                        queue.push(this);
                        while(queue.length > 0) {
                                var node = queue.shift();
                                var children = node.children();
                                for(var i = 0; i < children.length; ++i) {
                                        var child = $(children[i]);
                                        if (child.is(selector)) {
                                                return child; //well, we found one
                                        }
                                        queue.push(child);
                                }
                        }
                }
                return $();//nothing found
        };
})(jQuery);
(function() {
  var $, validateElement, validateForm, validatorsFor,
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  $ = jQuery;

  $.fn.disableClientSideValidations = function() {
    ClientSideValidations.disable(this);
    return this;
  };

  $.fn.enableClientSideValidations = function() {
    this.filter(ClientSideValidations.selectors.forms).each(function() {
      return ClientSideValidations.enablers.form(this);
    });
    this.filter(ClientSideValidations.selectors.inputs).each(function() {
      return ClientSideValidations.enablers.input(this);
    });
    return this;
  };

  $.fn.resetClientSideValidations = function() {
    this.filter(ClientSideValidations.selectors.forms).each(function() {
      return ClientSideValidations.reset(this);
    });
    return this;
  };

  $.fn.validate = function() {
    this.filter(ClientSideValidations.selectors.forms).each(function() {
      return $(this).enableClientSideValidations();
    });
    return this;
  };

  $.fn.isValid = function(validators) {
    var obj;
    obj = $(this[0]);
    if (obj.is('form')) {
      return validateForm(obj, validators);
    } else {
      return validateElement(obj, validatorsFor(this[0].name, validators));
    }
  };

  validatorsFor = function(name, validators) {
    name = name.replace(/_attributes\]\[\w+\]\[(\w+)\]/g, "_attributes][][$1]");
    return validators[name] || {};
  };

  validateForm = function(form, validators) {
    var valid;
    form.trigger('form:validate:before.ClientSideValidations');
    valid = true;
    form.find(ClientSideValidations.selectors.validate_inputs).each(function() {
      if (!$(this).isValid(validators)) {
        valid = false;
      }
      return true;
    });
    if (valid) {
      form.trigger('form:validate:pass.ClientSideValidations');
    } else {
      form.trigger('form:validate:fail.ClientSideValidations');
    }
    form.trigger('form:validate:after.ClientSideValidations');
    return valid;
  };

  validateElement = function(element, validators) {
    var afterValidate, destroyInputName, executeValidators, failElement, local, passElement, remote;
    element.trigger('element:validate:before.ClientSideValidations');
    passElement = function() {
      return element.trigger('element:validate:pass.ClientSideValidations').data('valid', null);
    };
    failElement = function(message) {
      element.trigger('element:validate:fail.ClientSideValidations', message).data('valid', false);
      return false;
    };
    afterValidate = function() {
      return element.trigger('element:validate:after.ClientSideValidations').data('valid') !== false;
    };
    executeValidators = function(context) {
      var fn, kind, message, valid, validator, _i, _len, _ref;
      valid = true;
      for (kind in context) {
        fn = context[kind];
        if (validators[kind]) {
          _ref = validators[kind];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            validator = _ref[_i];
            if (message = fn.call(context, element, validator)) {
              valid = failElement(message);
              break;
            }
          }
          if (!valid) {
            break;
          }
        }
      }
      return valid;
    };
    destroyInputName = element.attr('name').replace(/\[([^\]]*?)\]$/, '[_destroy]');
    if ($("input[name='" + destroyInputName + "']").val() === "1") {
      passElement();
      return afterValidate();
    }
    if (element.data('changed') === false) {
      return afterValidate();
    }
    element.data('changed', false);
    local = ClientSideValidations.validators.local;
    remote = ClientSideValidations.validators.remote;
    if (executeValidators(local) && executeValidators(remote)) {
      passElement();
    }
    return afterValidate();
  };

  if (window.ClientSideValidations === void 0) {
    window.ClientSideValidations = {};
  }

  if (window.ClientSideValidations.forms === void 0) {
    window.ClientSideValidations.forms = {};
  }

  window.ClientSideValidations.selectors = {
    inputs: ':input:not(button):not([type="submit"])[name]:visible:enabled',
    validate_inputs: ':input:enabled:visible[data-validate]',
    forms: 'form[data-validate]'
  };

  window.ClientSideValidations.reset = function(form) {
    var $form, key;
    $form = $(form);
    ClientSideValidations.disable(form);
    for (key in form.ClientSideValidations.settings.validators) {
      form.ClientSideValidations.removeError($form.find("[name='" + key + "']"));
    }
    return ClientSideValidations.enablers.form(form);
  };

  window.ClientSideValidations.disable = function(target) {
    var $target;
    $target = $(target);
    $target.off('.ClientSideValidations');
    if ($target.is('form')) {
      return ClientSideValidations.disable($target.find(':input'));
    } else {
      $target.removeData('valid');
      $target.removeData('changed');
      return $target.filter(':input').each(function() {
        return $(this).removeAttr('data-validate');
      });
    }
  };

  window.ClientSideValidations.enablers = {
    form: function(form) {
      var $form, binding, event, _ref;
      $form = $(form);
      form.ClientSideValidations = {
        settings: window.ClientSideValidations.forms[$form.attr('id')],
        addError: function(element, message) {
          return ClientSideValidations.formBuilders[form.ClientSideValidations.settings.type].add(element, form.ClientSideValidations.settings, message);
        },
        removeError: function(element) {
          return ClientSideValidations.formBuilders[form.ClientSideValidations.settings.type].remove(element, form.ClientSideValidations.settings);
        }
      };
      _ref = {
        'submit.ClientSideValidations': function(eventData) {
          if (!$form.isValid(form.ClientSideValidations.settings.validators)) {
            eventData.preventDefault();
            return eventData.stopImmediatePropagation();
          }
        },
        'ajax:beforeSend.ClientSideValidations': function(eventData) {
          if (eventData.target === this) {
            return $form.isValid(form.ClientSideValidations.settings.validators);
          }
        },
        'form:validate:after.ClientSideValidations': function(eventData) {
          return ClientSideValidations.callbacks.form.after($form, eventData);
        },
        'form:validate:before.ClientSideValidations': function(eventData) {
          return ClientSideValidations.callbacks.form.before($form, eventData);
        },
        'form:validate:fail.ClientSideValidations': function(eventData) {
          return ClientSideValidations.callbacks.form.fail($form, eventData);
        },
        'form:validate:pass.ClientSideValidations': function(eventData) {
          return ClientSideValidations.callbacks.form.pass($form, eventData);
        }
      };
      for (event in _ref) {
        binding = _ref[event];
        $form.on(event, binding);
      }
      return $form.find(ClientSideValidations.selectors.inputs).each(function() {
        return ClientSideValidations.enablers.input(this);
      });
    },
    input: function(input) {
      var $form, $input, binding, event, form, _ref;
      $input = $(input);
      form = input.form;
      $form = $(form);
      _ref = {
        'focusout.ClientSideValidations': function() {
          return $(this).isValid(form.ClientSideValidations.settings.validators);
        },
        'change.ClientSideValidations': function() {
          return $(this).data('changed', true);
        },
        'element:validate:after.ClientSideValidations': function(eventData) {
          return ClientSideValidations.callbacks.element.after($(this), eventData);
        },
        'element:validate:before.ClientSideValidations': function(eventData) {
          return ClientSideValidations.callbacks.element.before($(this), eventData);
        },
        'element:validate:fail.ClientSideValidations': function(eventData, message) {
          var element;
          element = $(this);
          return ClientSideValidations.callbacks.element.fail(element, message, function() {
            return form.ClientSideValidations.addError(element, message);
          }, eventData);
        },
        'element:validate:pass.ClientSideValidations': function(eventData) {
          var element;
          element = $(this);
          return ClientSideValidations.callbacks.element.pass(element, function() {
            return form.ClientSideValidations.removeError(element);
          }, eventData);
        }
      };
      for (event in _ref) {
        binding = _ref[event];
        $input.filter(':not(:radio):not([id$=_confirmation])').each(function() {
          return $(this).attr('data-validate', true);
        }).on(event, binding);
      }
      $input.filter(':checkbox').on('change.ClientSideValidations', function() {
        return $(this).isValid(form.ClientSideValidations.settings.validators);
      });
      return $input.filter('[id$=_confirmation]').each(function() {
        var confirmationElement, element, _ref1, _results;
        confirmationElement = $(this);
        element = $form.find("#" + (this.id.match(/(.+)_confirmation/)[1]) + ":input");
        if (element[0]) {
          _ref1 = {
            'focusout.ClientSideValidations': function() {
              return element.data('changed', true).isValid(form.ClientSideValidations.settings.validators);
            },
            'keyup.ClientSideValidations': function() {
              return element.data('changed', true).isValid(form.ClientSideValidations.settings.validators);
            }
          };
          _results = [];
          for (event in _ref1) {
            binding = _ref1[event];
            _results.push($("#" + (confirmationElement.attr('id'))).on(event, binding));
          }
          return _results;
        }
      });
    }
  };

  window.ClientSideValidations.validators = {
    all: function() {
      return jQuery.extend({}, ClientSideValidations.validators.local, ClientSideValidations.validators.remote);
    },
    local: {
      absence: function(element, options) {
        if (!/^\s*$/.test(element.val() || '')) {
          return options.message;
        }
      },
      presence: function(element, options) {
        if (/^\s*$/.test(element.val() || '')) {
          return options.message;
        }
      },
      acceptance: function(element, options) {
        var _ref;
        switch (element.attr('type')) {
          case 'checkbox':
            if (!element.prop('checked')) {
              return options.message;
            }
            break;
          case 'text':
            if (element.val() !== (((_ref = options.accept) != null ? _ref.toString() : void 0) || '1')) {
              return options.message;
            }
        }
      },
      format: function(element, options) {
        var message;
        message = this.presence(element, options);
        if (message) {
          if (options.allow_blank === true) {
            return;
          }
          return message;
        }
        if (options["with"] && !options["with"].test(element.val())) {
          return options.message;
        }
        if (options.without && options.without.test(element.val())) {
          return options.message;
        }
      },
      numericality: function(element, options) {
        var CHECKS, check, check_value, fn, form, operator, val;
        val = jQuery.trim(element.val());
        if (!ClientSideValidations.patterns.numericality.test(val)) {
          if (options.allow_blank === true && this.presence(element, {
            message: options.messages.numericality
          })) {
            return;
          }
          return options.messages.numericality;
        }
        if (options.only_integer && !/^[+-]?\d+$/.test(val)) {
          return options.messages.only_integer;
        }
        CHECKS = {
          greater_than: '>',
          greater_than_or_equal_to: '>=',
          equal_to: '==',
          less_than: '<',
          less_than_or_equal_to: '<='
        };
        form = $(element[0].form);
        for (check in CHECKS) {
          operator = CHECKS[check];
          if (!(options[check] != null)) {
            continue;
          }
          if (!isNaN(parseFloat(options[check])) && isFinite(options[check])) {
            check_value = options[check];
          } else if (form.find("[name*=" + options[check] + "]").size() === 1) {
            check_value = form.find("[name*=" + options[check] + "]").val();
          } else {
            return;
          }
          val = val.replace(new RegExp("\\" + ClientSideValidations.number_format.delimiter, 'g'), "").replace(new RegExp("\\" + ClientSideValidations.number_format.separator, 'g'), ".");
          fn = new Function("return " + val + " " + operator + " " + check_value);
          if (!fn()) {
            return options.messages[check];
          }
        }
        if (options.odd && !(parseInt(val, 10) % 2)) {
          return options.messages.odd;
        }
        if (options.even && (parseInt(val, 10) % 2)) {
          return options.messages.even;
        }
      },
      length: function(element, options) {
        var CHECKS, blankOptions, check, fn, message, operator, tokenized_length, tokenizer;
        tokenizer = options.js_tokenizer || "split('')";
        tokenized_length = new Function('element', "return (element.val()." + tokenizer + " || '').length")(element);
        CHECKS = {
          is: '==',
          minimum: '>=',
          maximum: '<='
        };
        blankOptions = {};
        blankOptions.message = options.is ? options.messages.is : options.minimum ? options.messages.minimum : void 0;
        message = this.presence(element, blankOptions);
        if (message) {
          if (options.allow_blank === true) {
            return;
          }
          return message;
        }
        for (check in CHECKS) {
          operator = CHECKS[check];
          if (!options[check]) {
            continue;
          }
          fn = new Function("return " + tokenized_length + " " + operator + " " + options[check]);
          if (!fn()) {
            return options.messages[check];
          }
        }
      },
      exclusion: function(element, options) {
        var lower, message, option, upper, _ref;
        message = this.presence(element, options);
        if (message) {
          if (options.allow_blank === true) {
            return;
          }
          return message;
        }
        if (options["in"]) {
          if (_ref = element.val(), __indexOf.call((function() {
            var _i, _len, _ref1, _results;
            _ref1 = options["in"];
            _results = [];
            for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
              option = _ref1[_i];
              _results.push(option.toString());
            }
            return _results;
          })(), _ref) >= 0) {
            return options.message;
          }
        }
        if (options.range) {
          lower = options.range[0];
          upper = options.range[1];
          if (element.val() >= lower && element.val() <= upper) {
            return options.message;
          }
        }
      },
      inclusion: function(element, options) {
        var lower, message, option, upper, _ref;
        message = this.presence(element, options);
        if (message) {
          if (options.allow_blank === true) {
            return;
          }
          return message;
        }
        if (options["in"]) {
          if (_ref = element.val(), __indexOf.call((function() {
            var _i, _len, _ref1, _results;
            _ref1 = options["in"];
            _results = [];
            for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
              option = _ref1[_i];
              _results.push(option.toString());
            }
            return _results;
          })(), _ref) >= 0) {
            return;
          }
          return options.message;
        }
        if (options.range) {
          lower = options.range[0];
          upper = options.range[1];
          if (element.val() >= lower && element.val() <= upper) {
            return;
          }
          return options.message;
        }
      },
      confirmation: function(element, options) {
        if (element.val() !== jQuery("#" + (element.attr('id')) + "_confirmation").val()) {
          return options.message;
        }
      },
      uniqueness: function(element, options) {
        var form, matches, name, name_prefix, name_suffix, valid, value;
        name = element.attr('name');
        if (/_attributes\]\[\d/.test(name)) {
          matches = name.match(/^(.+_attributes\])\[\d+\](.+)$/);
          name_prefix = matches[1];
          name_suffix = matches[2];
          value = element.val();
          if (name_prefix && name_suffix) {
            form = element.closest('form');
            valid = true;
            form.find(':input[name^="' + name_prefix + '"][name$="' + name_suffix + '"]').each(function() {
              if ($(this).attr('name') !== name) {
                if ($(this).val() === value) {
                  valid = false;
                  return $(this).data('notLocallyUnique', true);
                } else {
                  if ($(this).data('notLocallyUnique')) {
                    return $(this).removeData('notLocallyUnique').data('changed', true);
                  }
                }
              }
            });
            if (!valid) {
              return options.message;
            }
          }
        }
      }
    },
    remote: {
      uniqueness: function(element, options) {
        var data, key, message, name, scope_value, scoped_element, scoped_name, _ref;
        message = ClientSideValidations.validators.local.presence(element, options);
        if (message) {
          if (options.allow_blank === true) {
            return;
          }
          return message;
        }
        data = {};
        data.case_sensitive = !!options.case_sensitive;
        if (options.id) {
          data.id = options.id;
        }
        if (options.scope) {
          data.scope = {};
          _ref = options.scope;
          for (key in _ref) {
            scope_value = _ref[key];
            scoped_name = element.attr('name').replace(/\[\w+\]$/, "[" + key + "]");
            scoped_element = jQuery("[name='" + scoped_name + "']");
            jQuery("[name='" + scoped_name + "']:checkbox").each(function() {
              if (this.checked) {
                return scoped_element = this;
              }
            });
            if (scoped_element[0] && scoped_element.val() !== scope_value) {
              data.scope[key] = scoped_element.val();
              scoped_element.unbind("change." + element.id).bind("change." + element.id, function() {
                element.trigger('change.ClientSideValidations');
                return element.trigger('focusout.ClientSideValidations');
              });
            } else {
              data.scope[key] = scope_value;
            }
          }
        }
        if (/_attributes\]/.test(element.attr('name'))) {
          name = element.attr('name').match(/\[\w+_attributes\]/g).pop().match(/\[(\w+)_attributes\]/).pop();
          name += /(\[\w+\])$/.exec(element.attr('name'))[1];
        } else {
          name = element.attr('name');
        }
        if (options['class']) {
          name = options['class'] + '[' + name.split('[')[1];
        }
        data[name] = element.val();
        if (ClientSideValidations.remote_validators_prefix == null) {
          ClientSideValidations.remote_validators_prefix = "";
        }
        if (jQuery.ajax({
          url: "" + ClientSideValidations.remote_validators_prefix + "/validators/uniqueness",
          data: data,
          async: false,
          cache: false
        }).status === 200) {
          return options.message;
        }
      }
    }
  };

  window.ClientSideValidations.disableValidators = function() {
    var func, validator, _ref, _results;
    if (window.ClientSideValidations.disabled_validators === void 0) {
      return;
    }
    _ref = window.ClientSideValidations.validators.remote;
    _results = [];
    for (validator in _ref) {
      func = _ref[validator];
      if (window.ClientSideValidations.disabled_validators.indexOf(validator) !== -1) {
        _results.push(delete window.ClientSideValidations.validators.remote[validator]);
      } else {
        _results.push(void 0);
      }
    }
    return _results;
  };

  window.ClientSideValidations.formBuilders = {
    'ActionView::Helpers::FormBuilder': {
      add: function(element, settings, message) {
        var form, inputErrorField, label, labelErrorField;
        form = $(element[0].form);
        if (element.data('valid') !== false && (form.find("label.message[for='" + (element.attr('id')) + "']")[0] == null)) {
          inputErrorField = jQuery(settings.input_tag);
          labelErrorField = jQuery(settings.label_tag);
          label = form.find("label[for='" + (element.attr('id')) + "']:not(.message)");
          if (element.attr('autofocus')) {
            element.attr('autofocus', false);
          }
          element.before(inputErrorField);
          inputErrorField.find('span#input_tag').replaceWith(element);
          inputErrorField.find('label.message').attr('for', element.attr('id'));
          labelErrorField.find('label.message').attr('for', element.attr('id'));
          labelErrorField.insertAfter(label);
          labelErrorField.find('label#label_tag').replaceWith(label);
        }
        return form.find("label.message[for='" + (element.attr('id')) + "']").text(message);
      },
      remove: function(element, settings) {
        var errorFieldClass, form, inputErrorField, label, labelErrorField;
        form = $(element[0].form);
        errorFieldClass = jQuery(settings.input_tag).attr('class');
        inputErrorField = element.closest("." + (errorFieldClass.replace(" ", ".")));
        label = form.find("label[for='" + (element.attr('id')) + "']:not(.message)");
        labelErrorField = label.closest("." + errorFieldClass);
        if (inputErrorField[0]) {
          inputErrorField.find("#" + (element.attr('id'))).detach();
          inputErrorField.replaceWith(element);
          label.detach();
          return labelErrorField.replaceWith(label);
        }
      }
    }
  };

  window.ClientSideValidations.patterns = {
    numericality: /^(-|\+)?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d*)?$/
  };

  window.ClientSideValidations.callbacks = {
    element: {
      after: function(element, eventData) {},
      before: function(element, eventData) {},
      fail: function(element, message, addError, eventData) {
        return addError();
      },
      pass: function(element, removeError, eventData) {
        return removeError();
      }
    },
    form: {
      after: function(form, eventData) {},
      before: function(form, eventData) {},
      fail: function(form, eventData) {},
      pass: function(form, eventData) {}
    }
  };

  $(function() {
    ClientSideValidations.disableValidators();
    return $(ClientSideValidations.selectors.forms).validate();
  });

}).call(this);
ClientSideValidations.formBuilders['NestedForm::Builder'] =
    ClientSideValidations.formBuilders['ActionView::Helpers::FormBuilder'];
$('form').on('nested:fieldAdded', function(event) {
  $(event.target).find(':input').enableClientSideValidations();
});
/*
// Override NestedForm default behaviour
jQuery(function ($) {
  window.NestedFormEvents.prototype.insertFields = function(content, assoc, link) {
    $link = $(link);
    var data_attr = $link.attr('data-nested-form-wrapper');
    if (_.isString(data_attr)) 
      return $(content).insertBefore($(data_attr));
    if(_.isString($link.attr('data-nested-form-tr')))
      return $(content).insertBefore($link.closest('tr'));
    return $(content).insertBefore($link);
  };
});*/
;
/*
  Bootstrap - File Input
  ======================

  This is meant to convert all file input tags into a set of elements that displays consistently in all browsers.

  Converts all
  <input type="file">
  into Bootstrap buttons
  <a class="btn">Browse</a>

*/

(function($) {

$.fn.bootstrapFileInput = function() {

  this.each(function(i,elem){

    var $elem = $(elem);

    // Maybe some fields don't need to be standardized.
    if (typeof $elem.attr('data-bfi-disabled') != 'undefined') {
      return;
    }

    // Set the word to be displayed on the button
    var buttonWord = 'Browse';

    if (typeof $elem.attr('title') != 'undefined') {
      buttonWord = $elem.attr('title');
    }

    var className = '';

    if (!!$elem.attr('class')) {
      className = ' ' + $elem.attr('class');
    }

    // Now we're going to wrap that input field with a Bootstrap button.
    // The input will actually still be there, it will just be float above and transparent (done with the CSS).
    $elem.wrap('<a class="file-input-wrapper btn btn-default ' + className + '"></a>').parent().prepend($('<span></span>').html(buttonWord));
  })

  // After we have found all of the file inputs let's apply a listener for tracking the mouse movement.
  // This is important because the in order to give the illusion that this is a button in FF we actually need to move the button from the file input under the cursor. Ugh.
  .promise().done( function(){

    // As the cursor moves over our new Bootstrap button we need to adjust the position of the invisible file input Browse button to be under the cursor.
    // This gives us the pointer cursor that FF denies us
    $('.file-input-wrapper').mousemove(function(cursor) {

      var input, wrapper,
        wrapperX, wrapperY,
        inputWidth, inputHeight,
        cursorX, cursorY;

      // This wrapper element (the button surround this file input)
      wrapper = $(this);
      // The invisible file input element
      input = wrapper.find("input");
      // The left-most position of the wrapper
      wrapperX = wrapper.offset().left;
      // The top-most position of the wrapper
      wrapperY = wrapper.offset().top;
      // The with of the browsers input field
      inputWidth= input.width();
      // The height of the browsers input field
      inputHeight= input.height();
      //The position of the cursor in the wrapper
      cursorX = cursor.pageX;
      cursorY = cursor.pageY;

      //The positions we are to move the invisible file input
      // The 20 at the end is an arbitrary number of pixels that we can shift the input such that cursor is not pointing at the end of the Browse button but somewhere nearer the middle
      moveInputX = cursorX - wrapperX - inputWidth + 20;
      // Slides the invisible input Browse button to be positioned middle under the cursor
      moveInputY = cursorY- wrapperY - (inputHeight/2);

      // Apply the positioning styles to actually move the invisible file input
      input.css({
        left:moveInputX,
        top:moveInputY
      });
    });

    $('body').on('change', '.file-input-wrapper input[type=file]', function(){

      var fileName;
      fileName = $(this).val();

      // Remove any previous file names
      $(this).parent().next('.file-input-name').remove();
      if (!!$(this).prop('files') && $(this).prop('files').length > 1) {
        fileName = $(this)[0].files.length+' files';
      }
      else {
        fileName = fileName.substring(fileName.lastIndexOf('\\') + 1, fileName.length);
      }

      // Don't try to show the name if there is none
      if (!fileName) {
        return;
      }

      var selectedFileNamePlacement = $(this).data('filename-placement');
      if (selectedFileNamePlacement === 'inside') {
        // Print the fileName inside
        $(this).siblings('span').html(fileName);
        $(this).attr('title', fileName);
      } else {
        // Print the fileName aside (right after the the button)
        $(this).parent().after('<span class="file-input-name">'+fileName+'</span>');
      }
    });

  });

};

// Add the styles before the first stylesheet
// This ensures they can be easily overridden with developer styles
var cssHtml = '<style>'+
  '.file-input-wrapper { overflow: hidden; position: relative; cursor: pointer; z-index: 1; }'+
  '.file-input-wrapper input[type=file], .file-input-wrapper input[type=file]:focus, .file-input-wrapper input[type=file]:hover { position: absolute; top: 0; left: 0; cursor: pointer; opacity: 0; filter: alpha(opacity=0); z-index: 99; outline: 0; }'+
  '.file-input-name { margin-left: 8px; }'+
  '</style>';
$('link[rel=stylesheet]').eq(0).before(cssHtml);

})(jQuery);
/* =============================================================
* bootstrap-combobox.js v1.1.6
* =============================================================
* Copyright 2012 Daniel Farrell
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* ============================================================ */


!function( $ ) {

 "use strict";

 /* COMBOBOX PUBLIC CLASS DEFINITION
* ================================ */

  var Combobox = function ( element, options ) {
    this.options = $.extend({}, $.fn.combobox.defaults, options);
    this.$source = $(element);
    this.$container = this.setup();
    this.$element = this.$container.find('input[type=text]');
    this.$target = this.$container.find('input[type=hidden]');
    this.$button = this.$container.find('.dropdown-toggle');
    this.$menu = $(this.options.menu).appendTo('body');
    this.template = this.options.template || this.template
    this.matcher = this.options.matcher || this.matcher;
    this.sorter = this.options.sorter || this.sorter;
    this.highlighter = this.options.highlighter || this.highlighter;
    this.shown = false;
    this.selected = false;
    this.refresh();
    this.transferAttributes();
    this.listen();
  };

  Combobox.prototype = {

    constructor: Combobox

  , setup: function () {
      var combobox = $(this.template());
      this.$source.before(combobox);
      this.$source.hide();
      return combobox;
    }

  , disable: function() {
      this.$element.prop('disabled', true);
      this.$button.attr('disabled', true);
      this.disabled = true;
      this.$container.addClass('combobox-disabled');
    }

  , enable: function() {
      this.$element.prop('disabled', false);
      this.$button.attr('disabled', false);
      this.disabled = false;
      this.$container.removeClass('combobox-disabled');
    }
  , parse: function () {
      var that = this
        , map = {}
        , source = []
        , selected = false
        , selectedValue = '';
      this.$source.find('option').each(function() {
        var option = $(this);
        if (option.val() === '') {
          that.options.placeholder = option.text();
          return;
        }
        map[option.text()] = option.val();
        source.push(option.text());
        if (option.prop('selected')) {
          selected = option.text();
          selectedValue = option.val();
        }
      })
      this.map = map;
      if (selected) {
        this.$element.val(selected);
        this.$target.val(selectedValue);
        this.$container.addClass('combobox-selected');
        this.selected = true;
      }
      return source;
    }

  , transferAttributes: function() {
    this.options.placeholder = this.$source.attr('data-placeholder') || this.options.placeholder
    this.$element.attr('placeholder', this.options.placeholder)
    this.$target.prop('name', this.$source.prop('name'))
    this.$target.val(this.$source.val())
    this.$source.removeAttr('name') // Remove from source otherwise form will pass parameter twice.
    this.$element.attr('required', this.$source.attr('required'))
    this.$element.attr('rel', this.$source.attr('rel'))
    this.$element.attr('title', this.$source.attr('title'))
    this.$element.attr('class', this.$source.attr('class'))
    this.$element.attr('tabindex', this.$source.attr('tabindex'))
    this.$source.removeAttr('tabindex')
    if (this.$source.attr('disabled')!==undefined)
      this.disable();
  }

  , select: function () {
      var val = this.$menu.find('.active').attr('data-value');
      this.$element.val(this.updater(val)).trigger('change');
      this.$target.val(this.map[val]).trigger('change');
      this.$source.val(this.map[val]).trigger('change');
      this.$container.addClass('combobox-selected');
      this.selected = true;
      return this.hide();
    }

  , updater: function (item) {
      return item;
    }

  , show: function () {
      var pos = $.extend({}, this.$element.position(), {
        height: this.$element[0].offsetHeight
      });

      this.$menu
        .insertAfter(this.$element)
        .css({
          top: pos.top + pos.height
        , left: pos.left
        })
        .show();

      $('.dropdown-menu').on('mousedown', $.proxy(this.scrollSafety, this));

      this.shown = true;
      return this;
    }

  , hide: function () {
      this.$menu.hide();
      $('.dropdown-menu').off('mousedown', $.proxy(this.scrollSafety, this));
      this.$element.on('blur', $.proxy(this.blur, this));
      this.shown = false;
      return this;
    }

  , lookup: function (event) {
      this.query = this.$element.val();
      return this.process(this.source);
    }

  , process: function (items) {
      var that = this;

      items = $.grep(items, function (item) {
        return that.matcher(item);
      })

      items = this.sorter(items);

      if (!items.length) {
        return this.shown ? this.hide() : this;
      }

      return this.render(items.slice(0, this.options.items)).show();
    }

  , template: function() {
      if (this.options.bsVersion == '2') {
        return '<div class="combobox-container"><input type="hidden" /> <div class="input-append"> <input type="text" autocomplete="off" /> <span class="add-on dropdown-toggle" data-dropdown="dropdown"> <span class="caret"/> <i class="icon-remove"/> </span> </div> </div>'
      } else {
        return '<div class="combobox-container"> <input type="hidden" /> <div class="input-group"> <input type="text" autocomplete="off" /> <span class="input-group-addon dropdown-toggle" data-dropdown="dropdown"> <span class="caret" /> <span class="glyphicon glyphicon-remove" /> </span> </div> </div>'
      }
    }

  , matcher: function (item) {
      return ~item.toLowerCase().indexOf(this.query.toLowerCase());
    }

  , sorter: function (items) {
      var beginswith = []
        , caseSensitive = []
        , caseInsensitive = []
        , item;

      while (item = items.shift()) {
        if (!item.toLowerCase().indexOf(this.query.toLowerCase())) {beginswith.push(item);}
        else if (~item.indexOf(this.query)) {caseSensitive.push(item);}
        else {caseInsensitive.push(item);}
      }

      return beginswith.concat(caseSensitive, caseInsensitive);
    }

  , highlighter: function (item) {
      var query = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&');
      return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
        return '<strong>' + match + '</strong>';
      })
    }

  , render: function (items) {
      var that = this;

      items = $(items).map(function (i, item) {
        i = $(that.options.item).attr('data-value', item);
        i.find('a').html(that.highlighter(item));
        return i[0];
      })

      items.first().addClass('active');
      this.$menu.html(items);
      return this;
    }

  , next: function (event) {
      var active = this.$menu.find('.active').removeClass('active')
        , next = active.next();

      if (!next.length) {
        next = $(this.$menu.find('li')[0]);
      }

      next.addClass('active');
    }

  , prev: function (event) {
      var active = this.$menu.find('.active').removeClass('active')
        , prev = active.prev();

      if (!prev.length) {
        prev = this.$menu.find('li').last();
      }

      prev.addClass('active');
    }

  , toggle: function () {
    if (!this.disabled) {
      if (this.$container.hasClass('combobox-selected')) {
        this.clearTarget();
        this.triggerChange();
        this.clearElement();
      } else {
        if (this.shown) {
          this.hide();
        } else {
          this.clearElement();
          this.lookup();
        }
      }
    }
  }

  , scrollSafety: function(e) {
      if (e.target.tagName == 'UL') {
          this.$element.off('blur');
      }
  }
  , clearElement: function () {
    this.$element.val('').focus();
  }

  , clearTarget: function () {
    this.$source.val('');
    this.$target.val('');
    this.$container.removeClass('combobox-selected');
    this.selected = false;
  }

  , triggerChange: function () {
    this.$source.trigger('change');
  }

  , refresh: function () {
    this.source = this.parse();
    this.options.items = this.source.length;
  }

  , listen: function () {
      this.$element
        .on('focus', $.proxy(this.focus, this))
        .on('blur', $.proxy(this.blur, this))
        .on('keypress', $.proxy(this.keypress, this))
        .on('keyup', $.proxy(this.keyup, this));

      if (this.eventSupported('keydown')) {
        this.$element.on('keydown', $.proxy(this.keydown, this));
      }

      this.$menu
        .on('click', $.proxy(this.click, this))
        .on('mouseenter', 'li', $.proxy(this.mouseenter, this))
        .on('mouseleave', 'li', $.proxy(this.mouseleave, this));

      this.$button
        .on('click', $.proxy(this.toggle, this));
    }

  , eventSupported: function(eventName) {
      var isSupported = eventName in this.$element;
      if (!isSupported) {
        this.$element.setAttribute(eventName, 'return;');
        isSupported = typeof this.$element[eventName] === 'function';
      }
      return isSupported;
    }

  , move: function (e) {
      if (!this.shown) {return;}

      switch(e.keyCode) {
        case 9: // tab
        case 13: // enter
        case 27: // escape
          e.preventDefault();
          break;

        case 38: // up arrow
          e.preventDefault();
          this.prev();
          break;

        case 40: // down arrow
          e.preventDefault();
          this.next();
          break;
      }

      e.stopPropagation();
    }

  , keydown: function (e) {
      this.suppressKeyPressRepeat = ~$.inArray(e.keyCode, [40,38,9,13,27]);
      this.move(e);
    }

  , keypress: function (e) {
      if (this.suppressKeyPressRepeat) {return;}
      this.move(e);
    }

  , keyup: function (e) {
      switch(e.keyCode) {
        case 40: // down arrow
        case 39: // right arrow
        case 38: // up arrow
        case 37: // left arrow
        case 36: // home
        case 35: // end
        case 16: // shift
        case 17: // ctrl
        case 18: // alt
          break;

        case 9: // tab
        case 13: // enter
          if (!this.shown) {return;}
          this.select();
          break;

        case 27: // escape
          if (!this.shown) {return;}
          this.hide();
          break;

        default:
          this.clearTarget();
          this.lookup();
      }

      e.stopPropagation();
      e.preventDefault();
  }

  , focus: function (e) {
      this.focused = true;
    }

  , blur: function (e) {
      var that = this;
      this.focused = false;
      var val = this.$element.val();
      if (!this.selected && val !== '' ) {
        this.$element.val('');
        this.$source.val('').trigger('change');
        this.$target.val('').trigger('change');
      }
      if (!this.mousedover && this.shown) {setTimeout(function () { that.hide(); }, 200);}
    }

  , click: function (e) {
      e.stopPropagation();
      e.preventDefault();
      this.select();
      this.$element.focus();
    }

  , mouseenter: function (e) {
      this.mousedover = true;
      this.$menu.find('.active').removeClass('active');
      $(e.currentTarget).addClass('active');
    }

  , mouseleave: function (e) {
      this.mousedover = false;
    }
  };

  /* COMBOBOX PLUGIN DEFINITION
* =========================== */
  $.fn.combobox = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('combobox')
        , options = typeof option == 'object' && option;
      if(!data) {$this.data('combobox', (data = new Combobox(this, options)));}
      if (typeof option == 'string') {data[option]();}
    });
  };

  $.fn.combobox.defaults = {
    bsVersion: '3'
  , menu: '<ul class="typeahead typeahead-long dropdown-menu"></ul>'
  , item: '<li><a href="#"></a></li>'
  };

  $.fn.combobox.Constructor = Combobox;

}( window.jQuery );
/* ========================================================================
 * Bootstrap: bootstrap-iconpicker.js v1.0.0 by @recktoner
 * https://victor-valencia.github.com/bootstrap-iconpicker
 * ========================================================================
 * Copyright 2013 Victor Valencia Rico.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ======================================================================== */

+function(e){"use strict";var o=function(t,r){this.$element=e(t),this.options=e.extend({},o.DEFAULTS,this.$element.data()),this.options=e.extend({},this.options,r)};o.ICONSET={glyphicon:["adjust","align-center","align-justify","align-left","align-right","arrow-down","arrow-left","arrow-right","arrow-up","asterisk","backward","ban-circle","barcode","bell","bold","book","bookmark","briefcase","bullhorn","calendar","camera","certificate","check","chevron-down","chevron-left","chevron-right","chevron-up","circle-arrow-down","circle-arrow-left","circle-arrow-right","circle-arrow-up","cloud","cloud-download","cloud-upload","cog","collapse-down","collapse-up","comment","compressed","copyright-mark","credit-card","cutlery","dashboard","download","download-alt","earphone","edit","eject","envelope","euro","exclamation-sign","expand","export","eye-close","eye-open","facetime-video","fast-backward","fast-forward","file","film","filter","fire","flag","flash","floppy-disk","floppy-open","floppy-remove","floppy-save","floppy-saved","folder-close","folder-open","font","forward","fullscreen","gbp","gift","glass","globe","hand-down","hand-left","hand-right","hand-up","hd-video","hdd","header","headphones","heart","heart-empty","home","import","inbox","indent-left","indent-right","info-sign","italic","leaf","link","list","list-alt","lock","log-in","log-out","magnet","map-marker","minus","minus-sign","move","music","new-window","off","ok","ok-circle","ok-sign","open","paperclip","pause","pencil","phone","phone-alt","picture","plane","play","play-circle","plus","plus-sign","print","pushpin","qrcode","question-sign","random","record","refresh","registration-mark","remove","remove-circle","remove-sign","repeat","resize-full","resize-horizontal","resize-small","resize-vertical","retweet","road","save","saved","screenshot","sd-video","search","send","share","share-alt","shopping-cart","signal","sort","sort-by-alphabet","sort-by-alphabet-alt","sort-by-attributes","sort-by-attributes-alt","sort-by-order","sort-by-order-alt","sound-5-1","sound-6-1","sound-7-1","sound-dolby","sound-stereo","star","star-empty","stats","step-backward","step-forward","stop","subtitles","tag","tags","tasks","text-height","text-width","th","th-large","th-list","thumbs-down","thumbs-up","time","tint","tower","transfer","trash","tree-conifer","tree-deciduous","unchecked","upload","usd","user","volume-down","volume-off","volume-up","warning-sign","wrench","zoom-in","zoom-out"],fa:["adjust","anchor","archive","arrows","arrows-h","arrows-v","automobile","asterisk","ban","bank","bar-chart-o","barcode","bars","beer","bell","bell-o","bolt","bomb","book","bookmark","bookmark-o","briefcase","bug","building","building-o","bullhorn","bullseye","cab","calendar","calendar-o","camera","camera-retro","car","caret-square-o-down","caret-square-o-left","caret-square-o-right","caret-square-o-up","certificate","check","check-circle","check-circle-o","check-square","check-square-o","child","circle","circle-o","circle-thin","clock-o","cloud","cloud-download","cloud-upload","code","code-fork","coffee","cog","cogs","comment","comment-o","comments","comments-o","compass","credit-card","crop","crosshairs","cube","cubes","cutlery","dashboard","desktop","dashboard","database","desktop","dot-circle-o","download","edit","ellipsis-h","ellipsis-v","envelope","envelope-o","envelope-square","eraser","exchange","exclamation","exclamation-circle","exclamation-triangle","external-link","external-link-square","eye","eye-slash","fax","female","fighter-jet","file-archive-o","file-audio-o","file-code-o","file-excel-o","file-image-o","file-movie-o","file-pdf-o","file-photo-o","file-picture-o","file-powerpoint-o","file-sound-o","file-video-o","file-word-o","file-zip-o","film","filter","fire","fire-extinguisher","flag","flag-checkered","flag-o","flash","flask","folder","folder-o","folder-open","folder-open-o","frown-o","gamepad","gavel","gear","gears","gift","glass","globe","graduation-cap","group","hdd-o","headphones","heart","heart-o","history","home","image","inbox","info","info-circle","institution","key","keyboard-o","language","laptop","leaf","legal","lemon-o","level-down","level-up","life-bouy","life-ring","life-saver","lightbulb-o","location-arrow","lock","magic","magnet","mail-forward","mail-reply","mail-reply-all","male","map-marker","meh-o","microphone","microphone-slash","minus","minus-circle","minus-square","minus-square-o","mobile","mobile-phone","money","moon-o","mortar-board","music","navicon","paper-plane","paper-plane-o","paw","pencil","pencil-square","pencil-square-o","phone","phone-square","photo","picture-o","plane","plus","plus-circle","plus-square","plus-square-o","power-off","print","puzzle-piece","qrcode","question","question-circle","quote-left","quote-right","random","refresh","reorder","reply","reply-all","retweet","road","rocket","rss","rss-square","search","search-minus","search-plus","send","send-o","share","share-alt","share-alt-square","share-square","share-square-o","shield","shopping-cart","sign-in","sign-out","signal","sitemap","sliders","smile-o","sort","sort-alpha-asc","sort-alpha-desc","sort-amount-asc","sort-amount-desc","sort-asc","sort-desc","sort-down","sort-numeric-asc","sort-numeric-desc","sort-up","space-shuttle","spinner","spoon","square","square-o","star","star-half","star-half-empty","star-half-full","star-half-o","star-o","suitcase","sun-o","support","tablet","tachometer","tag","tags","tasks","taxi","terminal","thumb-tack","thumbs-down","thumbs-o-down","thumbs-o-up","thumbs-up","ticket","times","times-circle","times-circle-o","tint","toggle-down","toggle-left","toggle-right","toggle-up","trash-o","tree","trophy","truck","umbrella","university","unlock","unlock-alt","unsorted","upload","user","users","video-camera","volume-down","volume-off","volume-up","warning","wheelchair","wrench","check-square","check-square-o","circle","circle-o","dot-circle-o","minus-square","minus-square-o","plus-square","plus-square-o","square","square-o","bitcoin","btc","cny","dollar","eur","euro","gbp","inr","jpy","krw","money","rmb","rouble","rub","ruble","rupee","try","turkish-lira","usd","won","yen","align-center","align-justify","align-left","align-right","bold","chain","chain-broken","clipboard","columns","copy","cut","dedent","eraser","file","file-o","file-text","file-text-o","files-o","floppy-o","font","header","indent","italic","link","list","list-alt","list-ol","list-ul","outdent","paperclip","paragraph","paste","repeat","rotate-left","rotate-right","save","scissors","strikethrough","subscript","superscript","table","text-height","text-width","th","th-large","th-list","underline","undo","unlink","angle-double-down","angle-double-left","angle-double-right","angle-double-up","angle-down","angle-left","angle-right","angle-up","arrow-circle-down","arrow-circle-left","arrow-circle-o-down","arrow-circle-o-left","arrow-circle-o-right","arrow-circle-o-up","arrow-circle-right","arrow-circle-up","arrow-down","arrow-left","arrow-right","arrow-up","arrows","arrows-alt","arrows-h","arrows-v","caret-down","caret-left","caret-right","caret-square-o-down","caret-square-o-left","caret-square-o-right","caret-square-o-up","caret-up","chevron-circle-down","chevron-circle-left","chevron-circle-right","chevron-circle-up","chevron-down","chevron-left","chevron-right","chevron-up","hand-o-down","hand-o-left","hand-o-right","hand-o-up","long-arrow-down","long-arrow-left","long-arrow-right","long-arrow-up","toggle-down","toggle-left","toggle-right","toggle-up","arrows-alt","backward","compress","eject","expand","fast-backward","fast-forward","forward","pause","play","play-circle","play-circle-o","step-backward","step-forward","stop","youtube-play","adn","android","apple","behance","behance-square","bitbucket","bitbucket-square","bitcoin","btc","css3","delicious","digg","dribbble","dropbox","drupal","empire","facebook","facebook-square","flickr","foursquare","ge","git","git-square","github","github-alt","github-square","gittip","google","google-plus","google-plus-square","hacker-news","html5","instagram","joomla","jsfiddle","linkedin","linkedin-square","linux","maxcdn","openid","pagelines","pied-piper","pied-piper-alt","pied-piper-square","pinterest","pinterest-square","qq","ra","rebel","reddit","reddit-square","renren","share-alt","share-alt-square","skype","slack","soundcloud","spotify","stack-exchange","stack-overflow","steam","steam-square","stumbleupon","stumbleupon-circle","tencent-weibo","trello","tumblr","tumblr-square","twitter","twitter-square","vimeo-square","vine","vk","wechat","weibo","weixin","windows","wordpress","xing","xing-square","yahoo","youtube","youtube-play","youtube-square","ambulance","h-square","hospital-o","medkit","plus-square","stethoscope","user-md","wheelchair"]},o.DEFAULTS={iconset:"glyphicon",icon:"",rows:4,cols:4,placement:"right"},o.prototype.createButtonBar=function(){for(var o=this.options,t=e("<tr></tr>"),r=0;r<o.cols;r++){var a=e('<button class="btn btn-primary"><span class="glyphicon"></span></button>'),i=e('<td class="text-center"></td>');0==r||r==o.cols-1?(a.val(0==r?-1:1),a.addClass(0==r?"btn-previous":"btn-next"),a.find("span").addClass(0==r?"glyphicon-arrow-left":"glyphicon-arrow-right"),i.append(a),t.append(i)):0==t.find(".page-count").length&&(i.attr("colspan",o.cols-2).append('<span class="page-count"></span>'),t.append(i))}o.table.find("thead").append(t)},o.prototype.updateButtonBar=function(e){var o=this.options,t=Math.ceil(o.icons.length/(o.cols*o.rows));o.table.find(".page-count").html(e+" / "+t);var r=o.table.find(".btn-previous"),a=o.table.find(".btn-next");1==e?r.addClass("disabled"):r.removeClass("disabled"),e==t?a.addClass("disabled"):a.removeClass("disabled")},o.prototype.bindEvents=function(){var o=this.options,t=this;o.table.find(".btn-previous, .btn-next").off("click").on("click",function(){var r=parseInt(e(this).val());t.changeList(o.page+r)}),o.table.find(".btn-icon").off("click").on("click",function(){t.select(e(this).val()),t.$element.popover("destroy")})},o.prototype.select=function(o){var t=this.options,r=this.$element;t.selected=e.inArray(o.replace(t.iconClassFix,""),t.icons),-1==t.selected&&(t.selected=0,o=t.iconClassFix+t.icons[t.selected]),""!=o&&t.selected>=0&&(t.icon=o,r.find("input").val(o),r.find("i").attr("class","").addClass(t.iconClass).addClass(o),r.trigger({type:"change",icon:o}),t.table.find("button.btn-warning").removeClass("btn-warning"))},o.prototype.switchPage=function(o){var t=this.options;if(t.selected=e.inArray(o.replace(t.iconClassFix,""),t.icons),""!=o&&t.selected>=0){var r=Math.ceil((t.selected+1)/(t.cols*t.rows));this.changeList(r)}t.table.find("."+o).parent().addClass("btn-warning")},o.prototype.changeList=function(o){var t=this.options;this.updateButtonBar(o);for(var r=t.table.find("tbody").empty(),a=(o-1)*t.rows*t.cols,i=0;i<t.rows;i++){for(var s=e("<tr></tr>"),n=0;n<t.cols;n++){var l=a+i*t.cols+n,c=e('<button class="btn btn-default btn-icon"></button>').hide();if(l<t.icons.length){var p=t.iconClassFix+t.icons[l];c=e('<button class="btn btn-default btn-icon" value="'+p+'" title="'+p+'"><i class="'+t.iconClass+" "+p+'"></i></button>')}var d=e("<td></td>").append(c);s.append(d)}r.append(s)}t.page=o,this.bindEvents()},o.prototype.setIcon=function(e){this.select(e)};var t=e.fn.iconpicker;e.fn.iconpicker=function(t,r){return this.each(function(){var a=e(this),i=a.data("bs.iconpicker"),s="object"==typeof t&&t;if(i||a.data("bs.iconpicker",i=new o(this,s)),"string"==typeof t)i[t](r);else{var n=i.options,l="fontawesome"==n.iconset?"fa":"glyphicon";n=e.extend(n,{icons:o.ICONSET[l],iconClass:l,iconClassFix:l+"-",page:1,selected:-1,table:e('<table class="table-icons"><thead></thead><tbody></tbody></table>')});var c="undefined"!=typeof a.attr("name")?'name="'+a.attr("name")+'"':"";a.empty().append("<i></i>").append('<input type="hidden" '+c+"></input>").append('<span class="caret"></span>'),a.addClass("iconpicker"),i.createButtonBar(),i.changeList(1),a.on("click",function(e){e.preventDefault(),a.popover({animation:!1,trigger:"manual",html:!0,content:i.options.table,container:"body",placement:i.options.placement}).on("shown.bs.popover",function(){i.switchPage(n.icon),i.bindEvents()}),a.data("bs.popover").tip().addClass("iconpicker-popover"),a.popover("show")}),i.select(n.icon)}})},e.fn.iconpicker.Constructor=o,e.fn.iconpicker.noConflict=function(){return e.fn.iconpicker=t,this},e("body").on("click",function(o){e(".iconpicker").each(function(){e(this).is(o.target)||0!==e(this).has(o.target).length||0!==e(".popover").has(o.target).length||e(this).popover("destroy")})}),e('button[role="iconpicker"]').iconpicker()}(window.jQuery);
(function() {


}).call(this);
/* =========================================================
 * bootstrap-datepicker.js 
 * http://www.eyecon.ro/bootstrap-datepicker
 * =========================================================
 * Copyright 2012 Stefan Petre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */

 
!function( $ ) {
	
	// Picker object
	
	var Datepicker = function(element, options){
		this.element = $(element);
		this.format = DPGlobal.parseFormat(options.format||this.element.data('date-format')||'mm/dd/yyyy');
		this.picker = $(DPGlobal.template)
							.appendTo('body')
							.on({
								click: $.proxy(this.click, this)//,
								//mousedown: $.proxy(this.mousedown, this)
							});
		this.isInput = this.element.is('input');
		this.component = this.element.is('.date') ? this.element.find('.add-on') : false;
		
		if (this.isInput) {
			this.element.on({
				focus: $.proxy(this.show, this),
				//blur: $.proxy(this.hide, this),
				keyup: $.proxy(this.update, this)
			});
		} else {
			if (this.component){
				this.component.on('click', $.proxy(this.show, this));
			} else {
				this.element.on('click', $.proxy(this.show, this));
			}
		}
	
		this.minViewMode = options.minViewMode||this.element.data('date-minviewmode')||0;
		if (typeof this.minViewMode === 'string') {
			switch (this.minViewMode) {
				case 'months':
					this.minViewMode = 1;
					break;
				case 'years':
					this.minViewMode = 2;
					break;
				default:
					this.minViewMode = 0;
					break;
			}
		}
		this.viewMode = options.viewMode||this.element.data('date-viewmode')||0;
		if (typeof this.viewMode === 'string') {
			switch (this.viewMode) {
				case 'months':
					this.viewMode = 1;
					break;
				case 'years':
					this.viewMode = 2;
					break;
				default:
					this.viewMode = 0;
					break;
			}
		}
		this.startViewMode = this.viewMode;
		this.weekStart = options.weekStart||this.element.data('date-weekstart')||0;
		this.weekEnd = this.weekStart === 0 ? 6 : this.weekStart - 1;
		this.onRender = options.onRender;
		this.fillDow();
		this.fillMonths();
		this.update();
		this.showMode();
	};
	
	Datepicker.prototype = {
		constructor: Datepicker,
		
		show: function(e) {
			this.picker.show();
			this.height = this.component ? this.component.outerHeight() : this.element.outerHeight();
			this.place();
			$(window).on('resize', $.proxy(this.place, this));
			if (e ) {
				e.stopPropagation();
				e.preventDefault();
			}
			if (!this.isInput) {
			}
			var that = this;
			$(document).on('mousedown', function(ev){
				if ($(ev.target).closest('.datepicker').length == 0) {
					that.hide();
				}
			});
			this.element.trigger({
				type: 'show',
				date: this.date
			});
		},
		
		hide: function(){
			this.picker.hide();
			$(window).off('resize', this.place);
			this.viewMode = this.startViewMode;
			this.showMode();
			if (!this.isInput) {
				$(document).off('mousedown', this.hide);
			}
			//this.set();
			this.element.trigger({
				type: 'hide',
				date: this.date
			});
		},
		
		set: function() {
			var formated = DPGlobal.formatDate(this.date, this.format);
			if (!this.isInput) {
				if (this.component){
					this.element.find('input').prop('value', formated);
				}
				this.element.data('date', formated);
			} else {
				this.element.prop('value', formated);
			}
		},
		
		setValue: function(newDate) {
			if (typeof newDate === 'string') {
				this.date = DPGlobal.parseDate(newDate, this.format);
			} else {
				this.date = new Date(newDate);
			}
			this.set();
			this.viewDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1, 0, 0, 0, 0);
			this.fill();
		},
		
		place: function(){
			var offset = this.component ? this.component.offset() : this.element.offset();
			this.picker.css({
				top: offset.top + this.height,
				left: offset.left
			});
		},
		
		update: function(newDate){
			this.date = DPGlobal.parseDate(
				typeof newDate === 'string' ? newDate : (this.isInput ? this.element.prop('value') : this.element.data('date')),
				this.format
			);
			this.viewDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1, 0, 0, 0, 0);
			this.fill();
		},
		
		fillDow: function(){
			var dowCnt = this.weekStart;
			var html = '<tr>';
			while (dowCnt < this.weekStart + 7) {
				html += '<th class="dow">'+DPGlobal.dates.daysMin[(dowCnt++)%7]+'</th>';
			}
			html += '</tr>';
			this.picker.find('.datepicker-days thead').append(html);
		},
		
		fillMonths: function(){
			var html = '';
			var i = 0
			while (i < 12) {
				html += '<span class="month">'+DPGlobal.dates.monthsShort[i++]+'</span>';
			}
			this.picker.find('.datepicker-months td').append(html);
		},
		
		fill: function() {
			var d = new Date(this.viewDate),
				year = d.getFullYear(),
				month = d.getMonth(),
				currentDate = this.date.valueOf();
			this.picker.find('.datepicker-days th:eq(1)')
						.text(DPGlobal.dates.months[month]+' '+year);
			var prevMonth = new Date(year, month-1, 28,0,0,0,0),
				day = DPGlobal.getDaysInMonth(prevMonth.getFullYear(), prevMonth.getMonth());
			prevMonth.setDate(day);
			prevMonth.setDate(day - (prevMonth.getDay() - this.weekStart + 7)%7);
			var nextMonth = new Date(prevMonth);
			nextMonth.setDate(nextMonth.getDate() + 42);
			nextMonth = nextMonth.valueOf();
			var html = [];
			var clsName,
				prevY,
				prevM;
			while(prevMonth.valueOf() < nextMonth) {
				if (prevMonth.getDay() === this.weekStart) {
					html.push('<tr>');
				}
				clsName = this.onRender(prevMonth);
				prevY = prevMonth.getFullYear();
				prevM = prevMonth.getMonth();
				if ((prevM < month &&  prevY === year) ||  prevY < year) {
					clsName += ' old';
				} else if ((prevM > month && prevY === year) || prevY > year) {
					clsName += ' new';
				}
				if (prevMonth.valueOf() === currentDate) {
					clsName += ' active';
				}
				html.push('<td class="day '+clsName+'">'+prevMonth.getDate() + '</td>');
				if (prevMonth.getDay() === this.weekEnd) {
					html.push('</tr>');
				}
				prevMonth.setDate(prevMonth.getDate()+1);
			}
			this.picker.find('.datepicker-days tbody').empty().append(html.join(''));
			var currentYear = this.date.getFullYear();
			
			var months = this.picker.find('.datepicker-months')
						.find('th:eq(1)')
							.text(year)
							.end()
						.find('span').removeClass('active');
			if (currentYear === year) {
				months.eq(this.date.getMonth()).addClass('active');
			}
			
			html = '';
			year = parseInt(year/10, 10) * 10;
			var yearCont = this.picker.find('.datepicker-years')
								.find('th:eq(1)')
									.text(year + '-' + (year + 9))
									.end()
								.find('td');
			year -= 1;
			for (var i = -1; i < 11; i++) {
				html += '<span class="year'+(i === -1 || i === 10 ? ' old' : '')+(currentYear === year ? ' active' : '')+'">'+year+'</span>';
				year += 1;
			}
			yearCont.html(html);
		},
		
		click: function(e) {
			e.stopPropagation();
			e.preventDefault();
			var target = $(e.target).closest('span, td, th');
			if (target.length === 1) {
				switch(target[0].nodeName.toLowerCase()) {
					case 'th':
						switch(target[0].className) {
							case 'switch':
								this.showMode(1);
								break;
							case 'prev':
							case 'next':
								this.viewDate['set'+DPGlobal.modes[this.viewMode].navFnc].call(
									this.viewDate,
									this.viewDate['get'+DPGlobal.modes[this.viewMode].navFnc].call(this.viewDate) + 
									DPGlobal.modes[this.viewMode].navStep * (target[0].className === 'prev' ? -1 : 1)
								);
								this.fill();
								this.set();
								break;
						}
						break;
					case 'span':
						if (target.is('.month')) {
							var month = target.parent().find('span').index(target);
							this.viewDate.setMonth(month);
						} else {
							var year = parseInt(target.text(), 10)||0;
							this.viewDate.setFullYear(year);
						}
						if (this.viewMode !== 0) {
							this.date = new Date(this.viewDate);
							this.element.trigger({
								type: 'changeDate',
								date: this.date,
								viewMode: DPGlobal.modes[this.viewMode].clsName
							});
						}
						this.showMode(-1);
						this.fill();
						this.set();
						break;
					case 'td':
						if (target.is('.day') && !target.is('.disabled')){
							var day = parseInt(target.text(), 10)||1;
							var month = this.viewDate.getMonth();
							if (target.is('.old')) {
								month -= 1;
							} else if (target.is('.new')) {
								month += 1;
							}
							var year = this.viewDate.getFullYear();
							this.date = new Date(year, month, day,0,0,0,0);
							this.viewDate = new Date(year, month, Math.min(28, day),0,0,0,0);
							this.fill();
							this.set();
							this.element.trigger({
								type: 'changeDate',
								date: this.date,
								viewMode: DPGlobal.modes[this.viewMode].clsName
							});
						}
						break;
				}
			}
		},
		
		mousedown: function(e){
			e.stopPropagation();
			e.preventDefault();
		},
		
		showMode: function(dir) {
			if (dir) {
				this.viewMode = Math.max(this.minViewMode, Math.min(2, this.viewMode + dir));
			}
			this.picker.find('>div').hide().filter('.datepicker-'+DPGlobal.modes[this.viewMode].clsName).show();
		}
	};
	
	$.fn.datepicker = function ( option, val ) {
		return this.each(function () {
			var $this = $(this),
				data = $this.data('datepicker'),
				options = typeof option === 'object' && option;
			if (!data) {
				$this.data('datepicker', (data = new Datepicker(this, $.extend({}, $.fn.datepicker.defaults,options))));
			}
			if (typeof option === 'string') data[option](val);
		});
	};

	$.fn.datepicker.defaults = {
		onRender: function(date) {
			return '';
		}
	};
	$.fn.datepicker.Constructor = Datepicker;
	
	var DPGlobal = {
		modes: [
			{
				clsName: 'days',
				navFnc: 'Month',
				navStep: 1
			},
			{
				clsName: 'months',
				navFnc: 'FullYear',
				navStep: 1
			},
			{
				clsName: 'years',
				navFnc: 'FullYear',
				navStep: 10
		}],
		dates:{
			days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
			daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
			daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
			months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
		},
		isLeapYear: function (year) {
			return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0))
		},
		getDaysInMonth: function (year, month) {
			return [31, (DPGlobal.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month]
		},
		parseFormat: function(format){
			var separator = format.match(/[.\/\-\s].*?/),
				parts = format.split(/\W+/);
			if (!separator || !parts || parts.length === 0){
				throw new Error("Invalid date format.");
			}
			return {separator: separator, parts: parts};
		},
		parseDate: function(date, format) {
			var parts = date.split(format.separator),
				date = new Date(),
				val;
			date.setHours(0);
			date.setMinutes(0);
			date.setSeconds(0);
			date.setMilliseconds(0);
			if (parts.length === format.parts.length) {
				var year = date.getFullYear(), day = date.getDate(), month = date.getMonth();
				for (var i=0, cnt = format.parts.length; i < cnt; i++) {
					val = parseInt(parts[i], 10)||1;
					switch(format.parts[i]) {
						case 'dd':
						case 'd':
							day = val;
							date.setDate(val);
							break;
						case 'mm':
						case 'm':
							month = val - 1;
							date.setMonth(val - 1);
							break;
						case 'yy':
							year = 2000 + val;
							date.setFullYear(2000 + val);
							break;
						case 'yyyy':
							year = val;
							date.setFullYear(val);
							break;
					}
				}
				date = new Date(year, month, day, 0 ,0 ,0);
			}
			return date;
		},
		formatDate: function(date, format){
			var val = {
				d: date.getDate(),
				m: date.getMonth() + 1,
				yy: date.getFullYear().toString().substring(2),
				yyyy: date.getFullYear()
			};
			val.dd = (val.d < 10 ? '0' : '') + val.d;
			val.mm = (val.m < 10 ? '0' : '') + val.m;
			var date = [];
			for (var i=0, cnt = format.parts.length; i < cnt; i++) {
				date.push(val[format.parts[i]]);
			}
			return date.join(format.separator);
		},
		headTemplate: '<thead>'+
							'<tr>'+
								'<th class="prev">&lsaquo;</th>'+
								'<th colspan="5" class="switch"></th>'+
								'<th class="next">&rsaquo;</th>'+
							'</tr>'+
						'</thead>',
		contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>'
	};
	DPGlobal.template = '<div class="datepicker dropdown-menu">'+
							'<div class="datepicker-days">'+
								'<table class=" table-condensed">'+
									DPGlobal.headTemplate+
									'<tbody></tbody>'+
								'</table>'+
							'</div>'+
							'<div class="datepicker-months">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
								'</table>'+
							'</div>'+
							'<div class="datepicker-years">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
								'</table>'+
							'</div>'+
						'</div>';

}( window.jQuery );
/*!
 * bootstrap-typeahead.js v0.0.3 (http://www.upbootstrap.com)
 * Copyright 2012-2014 Twitter Inc.
 * Licensed under MIT (https://github.com/biggora/bootstrap-ajax-typeahead/blob/master/LICENSE)
 * See Demo: http://plugins.upbootstrap.com/bootstrap-ajax-typeahead
 * Updated: 2014-02-09 02:4:38
 *
 * Modifications by Paul Warelis and Alexey Gordeyev
 */

!function(a){"use strict";var b=function(b,c){var d=this;if(d.$element=a(b),d.options=a.extend({},a.fn.typeahead.defaults,c),d.$menu=a(d.options.menu).insertAfter(d.$element),d.eventSupported=d.options.eventSupported||d.eventSupported,d.grepper=d.options.grepper||d.grepper,d.highlighter=d.options.highlighter||d.highlighter,d.lookup=d.options.lookup||d.lookup,d.matcher=d.options.matcher||d.matcher,d.render=d.options.render||d.render,d.onSelect=d.options.onSelect||null,d.sorter=d.options.sorter||d.sorter,d.source=d.options.source||d.source,d.displayField=d.options.displayField||d.displayField,d.valueField=d.options.valueField||d.valueField,d.options.ajax){var e=d.options.ajax;"string"==typeof e?d.ajax=a.extend({},a.fn.typeahead.defaults.ajax,{url:e}):("string"==typeof e.displayField&&(d.displayField=d.options.displayField=e.displayField),"string"==typeof e.valueField&&(d.valueField=d.options.valueField=e.valueField),d.ajax=a.extend({},a.fn.typeahead.defaults.ajax,e)),d.ajax.url||(d.ajax=null),d.query=""}else d.source=d.options.source,d.ajax=null;d.shown=!1,d.listen()};b.prototype={constructor:b,eventSupported:function(a){var b=a in this.$element;return b||(this.$element.setAttribute(a,"return;"),b="function"==typeof this.$element[a]),b},select:function(){var a=this.$menu.find(".active"),b=a.attr("data-value"),c=this.$menu.find(".active a").text();return this.options.onSelect&&this.options.onSelect({value:b,text:c}),this.$element.val(this.updater(c)).change(),this.hide()},updater:function(a){return a},show:function(){var b=a.extend({},this.$element.position(),{height:this.$element[0].offsetHeight});return this.$menu.css({top:b.top+b.height,left:b.left}),this.$menu.show(),this.shown=!0,this},hide:function(){return this.$menu.hide(),this.shown=!1,this},ajaxLookup:function(){function b(){this.ajaxToggleLoadClass(!0),this.ajax.xhr&&this.ajax.xhr.abort();var b=this.ajax.preDispatch?this.ajax.preDispatch(c):{query:c};this.ajax.xhr=a.ajax({url:this.ajax.url,data:b,success:a.proxy(this.ajaxSource,this),type:this.ajax.method||"get",dataType:"json"}),this.ajax.timerId=null}var c=a.trim(this.$element.val());return c===this.query?this:(this.query=c,this.ajax.timerId&&(clearTimeout(this.ajax.timerId),this.ajax.timerId=null),!c||c.length<this.ajax.triggerLength?(this.ajax.xhr&&(this.ajax.xhr.abort(),this.ajax.xhr=null,this.ajaxToggleLoadClass(!1)),this.shown?this.hide():this):(this.ajax.timerId=setTimeout(a.proxy(b,this),this.ajax.timeout),this))},ajaxSource:function(a){this.ajaxToggleLoadClass(!1);var b,c=this;if(c.ajax.xhr)return c.ajax.preProcess&&(a=c.ajax.preProcess(a)),c.ajax.data=a,b=c.grepper(c.ajax.data)||[],b.length?(c.ajax.xhr=null,c.render(b.slice(0,c.options.items)).show()):c.shown?c.hide():c},ajaxToggleLoadClass:function(a){this.ajax.loadingClass&&this.$element.toggleClass(this.ajax.loadingClass,a)},lookup:function(){var a,b=this;return b.ajax?void b.ajaxer():(b.query=b.$element.val(),b.query?(a=b.grepper(b.source),a&&a.length?b.render(a.slice(0,b.options.items)).show():b.shown?b.hide():b):b.shown?b.hide():b)},matcher:function(a){return~a.toLowerCase().indexOf(this.query.toLowerCase())},sorter:function(a){if(this.options.ajax)return a;for(var b,c=[],d=[],e=[];b=a.shift();)b.toLowerCase().indexOf(this.query.toLowerCase())?~b.indexOf(this.query)?d.push(b):e.push(b):c.push(b);return c.concat(d,e)},highlighter:function(a){var b=this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&");return a.replace(new RegExp("("+b+")","ig"),function(a,b){return"<strong>"+b+"</strong>"})},render:function(b){var c,d=this,e="string"==typeof d.options.displayField;return b=a(b).map(function(b,f){return"object"==typeof f?(c=e?f[d.options.displayField]:d.options.displayField(f),b=a(d.options.item).attr("data-value",f[d.options.valueField])):(c=f,b=a(d.options.item).attr("data-value",f)),b.find("a").html(d.highlighter(c)),b[0]}),b.first().addClass("active"),this.$menu.html(b),this},grepper:function(b){var c,d,e=this,f="string"==typeof e.options.displayField;if(!(f&&b&&b.length))return null;if(b[0].hasOwnProperty(e.options.displayField))c=a.grep(b,function(a){return d=f?a[e.options.displayField]:e.options.displayField(a),e.matcher(d)});else{if("string"!=typeof b[0])return null;c=a.grep(b,function(a){return e.matcher(a)})}return this.sorter(c)},next:function(){var b=this.$menu.find(".active").removeClass("active"),c=b.next();c.length||(c=a(this.$menu.find("li")[0])),c.addClass("active")},prev:function(){var a=this.$menu.find(".active").removeClass("active"),b=a.prev();b.length||(b=this.$menu.find("li").last()),b.addClass("active")},listen:function(){this.$element.on("focus",a.proxy(this.focus,this)).on("blur",a.proxy(this.blur,this)).on("keypress",a.proxy(this.keypress,this)).on("keyup",a.proxy(this.keyup,this)),this.eventSupported("keydown")&&this.$element.on("keydown",a.proxy(this.keydown,this)),this.$menu.on("click",a.proxy(this.click,this)).on("mouseenter","li",a.proxy(this.mouseenter,this)).on("mouseleave","li",a.proxy(this.mouseleave,this))},move:function(a){if(this.shown){switch(a.keyCode){case 9:case 13:case 27:a.preventDefault();break;case 38:a.preventDefault(),this.prev();break;case 40:a.preventDefault(),this.next()}a.stopPropagation()}},keydown:function(b){this.suppressKeyPressRepeat=~a.inArray(b.keyCode,[40,38,9,13,27]),this.move(b)},keypress:function(a){this.suppressKeyPressRepeat||this.move(a)},keyup:function(a){switch(a.keyCode){case 40:case 38:case 16:case 17:case 18:break;case 9:case 13:if(!this.shown)return;this.select();break;case 27:if(!this.shown)return;this.hide();break;default:this.ajax?this.ajaxLookup():this.lookup()}a.stopPropagation(),a.preventDefault()},focus:function(){this.focused=!0},blur:function(){this.focused=!1,!this.mousedover&&this.shown&&this.hide()},click:function(a){a.stopPropagation(),a.preventDefault(),this.select(),this.$element.focus()},mouseenter:function(b){this.mousedover=!0,this.$menu.find(".active").removeClass("active"),a(b.currentTarget).addClass("active")},mouseleave:function(){this.mousedover=!1,!this.focused&&this.shown&&this.hide()}},a.fn.typeahead=function(c){return this.each(function(){var d=a(this),e=d.data("typeahead"),f="object"==typeof c&&c;e||d.data("typeahead",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.typeahead.defaults={source:[],items:8,menu:'<ul class="typeahead dropdown-menu"></ul>',item:'<li><a href="#"></a></li>',displayField:"name",valueField:"id",onSelect:function(){},ajax:{url:null,timeout:300,method:"get",triggerLength:1,loadingClass:null,preDispatch:null,preProcess:null}},a.fn.typeahead.Constructor=b,a(function(){a("body").on("focus.typeahead.data-api",'[data-provide="typeahead"]',function(b){var c=a(this);c.data("typeahead")||(b.preventDefault(),c.typeahead(c.data()))})})}(window.jQuery);
/*!
 * Bootstrap v3.1.0 (http://getbootstrap.com)
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

if("undefined"==typeof jQuery)throw new Error("Bootstrap requires jQuery");+function(a){"use strict";function b(){var a=document.createElement("bootstrap"),b={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var c in b)if(void 0!==a.style[c])return{end:b[c]};return!1}a.fn.emulateTransitionEnd=function(b){var c=!1,d=this;a(this).one(a.support.transition.end,function(){c=!0});var e=function(){c||a(d).trigger(a.support.transition.end)};return setTimeout(e,b),this},a(function(){a.support.transition=b()})}(jQuery),+function(a){"use strict";var b='[data-dismiss="alert"]',c=function(c){a(c).on("click",b,this.close)};c.prototype.close=function(b){function c(){f.trigger("closed.bs.alert").remove()}var d=a(this),e=d.attr("data-target");e||(e=d.attr("href"),e=e&&e.replace(/.*(?=#[^\s]*$)/,""));var f=a(e);b&&b.preventDefault(),f.length||(f=d.hasClass("alert")?d:d.parent()),f.trigger(b=a.Event("close.bs.alert")),b.isDefaultPrevented()||(f.removeClass("in"),a.support.transition&&f.hasClass("fade")?f.one(a.support.transition.end,c).emulateTransitionEnd(150):c())};var d=a.fn.alert;a.fn.alert=function(b){return this.each(function(){var d=a(this),e=d.data("bs.alert");e||d.data("bs.alert",e=new c(this)),"string"==typeof b&&e[b].call(d)})},a.fn.alert.Constructor=c,a.fn.alert.noConflict=function(){return a.fn.alert=d,this},a(document).on("click.bs.alert.data-api",b,c.prototype.close)}(jQuery),+function(a){"use strict";var b=function(c,d){this.$element=a(c),this.options=a.extend({},b.DEFAULTS,d),this.isLoading=!1};b.DEFAULTS={loadingText:"loading..."},b.prototype.setState=function(b){var c="disabled",d=this.$element,e=d.is("input")?"val":"html",f=d.data();b+="Text",f.resetText||d.data("resetText",d[e]()),d[e](f[b]||this.options[b]),setTimeout(a.proxy(function(){"loadingText"==b?(this.isLoading=!0,d.addClass(c).attr(c,c)):this.isLoading&&(this.isLoading=!1,d.removeClass(c).removeAttr(c))},this),0)},b.prototype.toggle=function(){var a=!0,b=this.$element.closest('[data-toggle="buttons"]');if(b.length){var c=this.$element.find("input");"radio"==c.prop("type")&&(c.prop("checked")&&this.$element.hasClass("active")?a=!1:b.find(".active").removeClass("active")),a&&c.prop("checked",!this.$element.hasClass("active")).trigger("change")}a&&this.$element.toggleClass("active")};var c=a.fn.button;a.fn.button=function(c){return this.each(function(){var d=a(this),e=d.data("bs.button"),f="object"==typeof c&&c;e||d.data("bs.button",e=new b(this,f)),"toggle"==c?e.toggle():c&&e.setState(c)})},a.fn.button.Constructor=b,a.fn.button.noConflict=function(){return a.fn.button=c,this},a(document).on("click.bs.button.data-api","[data-toggle^=button]",function(b){var c=a(b.target);c.hasClass("btn")||(c=c.closest(".btn")),c.button("toggle"),b.preventDefault()})}(jQuery),+function(a){"use strict";var b=function(b,c){this.$element=a(b),this.$indicators=this.$element.find(".carousel-indicators"),this.options=c,this.paused=this.sliding=this.interval=this.$active=this.$items=null,"hover"==this.options.pause&&this.$element.on("mouseenter",a.proxy(this.pause,this)).on("mouseleave",a.proxy(this.cycle,this))};b.DEFAULTS={interval:5e3,pause:"hover",wrap:!0},b.prototype.cycle=function(b){return b||(this.paused=!1),this.interval&&clearInterval(this.interval),this.options.interval&&!this.paused&&(this.interval=setInterval(a.proxy(this.next,this),this.options.interval)),this},b.prototype.getActiveIndex=function(){return this.$active=this.$element.find(".item.active"),this.$items=this.$active.parent().children(),this.$items.index(this.$active)},b.prototype.to=function(b){var c=this,d=this.getActiveIndex();return b>this.$items.length-1||0>b?void 0:this.sliding?this.$element.one("slid.bs.carousel",function(){c.to(b)}):d==b?this.pause().cycle():this.slide(b>d?"next":"prev",a(this.$items[b]))},b.prototype.pause=function(b){return b||(this.paused=!0),this.$element.find(".next, .prev").length&&a.support.transition&&(this.$element.trigger(a.support.transition.end),this.cycle(!0)),this.interval=clearInterval(this.interval),this},b.prototype.next=function(){return this.sliding?void 0:this.slide("next")},b.prototype.prev=function(){return this.sliding?void 0:this.slide("prev")},b.prototype.slide=function(b,c){var d=this.$element.find(".item.active"),e=c||d[b](),f=this.interval,g="next"==b?"left":"right",h="next"==b?"first":"last",i=this;if(!e.length){if(!this.options.wrap)return;e=this.$element.find(".item")[h]()}if(e.hasClass("active"))return this.sliding=!1;var j=a.Event("slide.bs.carousel",{relatedTarget:e[0],direction:g});return this.$element.trigger(j),j.isDefaultPrevented()?void 0:(this.sliding=!0,f&&this.pause(),this.$indicators.length&&(this.$indicators.find(".active").removeClass("active"),this.$element.one("slid.bs.carousel",function(){var b=a(i.$indicators.children()[i.getActiveIndex()]);b&&b.addClass("active")})),a.support.transition&&this.$element.hasClass("slide")?(e.addClass(b),e[0].offsetWidth,d.addClass(g),e.addClass(g),d.one(a.support.transition.end,function(){e.removeClass([b,g].join(" ")).addClass("active"),d.removeClass(["active",g].join(" ")),i.sliding=!1,setTimeout(function(){i.$element.trigger("slid.bs.carousel")},0)}).emulateTransitionEnd(1e3*d.css("transition-duration").slice(0,-1))):(d.removeClass("active"),e.addClass("active"),this.sliding=!1,this.$element.trigger("slid.bs.carousel")),f&&this.cycle(),this)};var c=a.fn.carousel;a.fn.carousel=function(c){return this.each(function(){var d=a(this),e=d.data("bs.carousel"),f=a.extend({},b.DEFAULTS,d.data(),"object"==typeof c&&c),g="string"==typeof c?c:f.slide;e||d.data("bs.carousel",e=new b(this,f)),"number"==typeof c?e.to(c):g?e[g]():f.interval&&e.pause().cycle()})},a.fn.carousel.Constructor=b,a.fn.carousel.noConflict=function(){return a.fn.carousel=c,this},a(document).on("click.bs.carousel.data-api","[data-slide], [data-slide-to]",function(b){var c,d=a(this),e=a(d.attr("data-target")||(c=d.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,"")),f=a.extend({},e.data(),d.data()),g=d.attr("data-slide-to");g&&(f.interval=!1),e.carousel(f),(g=d.attr("data-slide-to"))&&e.data("bs.carousel").to(g),b.preventDefault()}),a(window).on("load",function(){a('[data-ride="carousel"]').each(function(){var b=a(this);b.carousel(b.data())})})}(jQuery),+function(a){"use strict";var b=function(c,d){this.$element=a(c),this.options=a.extend({},b.DEFAULTS,d),this.transitioning=null,this.options.parent&&(this.$parent=a(this.options.parent)),this.options.toggle&&this.toggle()};b.DEFAULTS={toggle:!0},b.prototype.dimension=function(){var a=this.$element.hasClass("width");return a?"width":"height"},b.prototype.show=function(){if(!this.transitioning&&!this.$element.hasClass("in")){var b=a.Event("show.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.$parent&&this.$parent.find("> .panel > .in");if(c&&c.length){var d=c.data("bs.collapse");if(d&&d.transitioning)return;c.collapse("hide"),d||c.data("bs.collapse",null)}var e=this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[e](0),this.transitioning=1;var f=function(){this.$element.removeClass("collapsing").addClass("collapse in")[e]("auto"),this.transitioning=0,this.$element.trigger("shown.bs.collapse")};if(!a.support.transition)return f.call(this);var g=a.camelCase(["scroll",e].join("-"));this.$element.one(a.support.transition.end,a.proxy(f,this)).emulateTransitionEnd(350)[e](this.$element[0][g])}}},b.prototype.hide=function(){if(!this.transitioning&&this.$element.hasClass("in")){var b=a.Event("hide.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.dimension();this.$element[c](this.$element[c]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse").removeClass("in"),this.transitioning=1;var d=function(){this.transitioning=0,this.$element.trigger("hidden.bs.collapse").removeClass("collapsing").addClass("collapse")};return a.support.transition?void this.$element[c](0).one(a.support.transition.end,a.proxy(d,this)).emulateTransitionEnd(350):d.call(this)}}},b.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()};var c=a.fn.collapse;a.fn.collapse=function(c){return this.each(function(){var d=a(this),e=d.data("bs.collapse"),f=a.extend({},b.DEFAULTS,d.data(),"object"==typeof c&&c);!e&&f.toggle&&"show"==c&&(c=!c),e||d.data("bs.collapse",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.collapse.Constructor=b,a.fn.collapse.noConflict=function(){return a.fn.collapse=c,this},a(document).on("click.bs.collapse.data-api","[data-toggle=collapse]",function(b){var c,d=a(this),e=d.attr("data-target")||b.preventDefault()||(c=d.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,""),f=a(e),g=f.data("bs.collapse"),h=g?"toggle":d.data(),i=d.attr("data-parent"),j=i&&a(i);g&&g.transitioning||(j&&j.find('[data-toggle=collapse][data-parent="'+i+'"]').not(d).addClass("collapsed"),d[f.hasClass("in")?"addClass":"removeClass"]("collapsed")),f.collapse(h)})}(jQuery),+function(a){"use strict";function b(b){a(d).remove(),a(e).each(function(){var d=c(a(this)),e={relatedTarget:this};d.hasClass("open")&&(d.trigger(b=a.Event("hide.bs.dropdown",e)),b.isDefaultPrevented()||d.removeClass("open").trigger("hidden.bs.dropdown",e))})}function c(b){var c=b.attr("data-target");c||(c=b.attr("href"),c=c&&/#[A-Za-z]/.test(c)&&c.replace(/.*(?=#[^\s]*$)/,""));var d=c&&a(c);return d&&d.length?d:b.parent()}var d=".dropdown-backdrop",e="[data-toggle=dropdown]",f=function(b){a(b).on("click.bs.dropdown",this.toggle)};f.prototype.toggle=function(d){var e=a(this);if(!e.is(".disabled, :disabled")){var f=c(e),g=f.hasClass("open");if(b(),!g){"ontouchstart"in document.documentElement&&!f.closest(".navbar-nav").length&&a('<div class="dropdown-backdrop"/>').insertAfter(a(this)).on("click",b);var h={relatedTarget:this};if(f.trigger(d=a.Event("show.bs.dropdown",h)),d.isDefaultPrevented())return;f.toggleClass("open").trigger("shown.bs.dropdown",h),e.focus()}return!1}},f.prototype.keydown=function(b){if(/(38|40|27)/.test(b.keyCode)){var d=a(this);if(b.preventDefault(),b.stopPropagation(),!d.is(".disabled, :disabled")){var f=c(d),g=f.hasClass("open");if(!g||g&&27==b.keyCode)return 27==b.which&&f.find(e).focus(),d.click();var h=" li:not(.divider):visible a",i=f.find("[role=menu]"+h+", [role=listbox]"+h);if(i.length){var j=i.index(i.filter(":focus"));38==b.keyCode&&j>0&&j--,40==b.keyCode&&j<i.length-1&&j++,~j||(j=0),i.eq(j).focus()}}}};var g=a.fn.dropdown;a.fn.dropdown=function(b){return this.each(function(){var c=a(this),d=c.data("bs.dropdown");d||c.data("bs.dropdown",d=new f(this)),"string"==typeof b&&d[b].call(c)})},a.fn.dropdown.Constructor=f,a.fn.dropdown.noConflict=function(){return a.fn.dropdown=g,this},a(document).on("click.bs.dropdown.data-api",b).on("click.bs.dropdown.data-api",".dropdown form",function(a){a.stopPropagation()}).on("click.bs.dropdown.data-api",e,f.prototype.toggle).on("keydown.bs.dropdown.data-api",e+", [role=menu], [role=listbox]",f.prototype.keydown)}(jQuery),+function(a){"use strict";var b=function(b,c){this.options=c,this.$element=a(b),this.$backdrop=this.isShown=null,this.options.remote&&this.$element.find(".modal-content").load(this.options.remote,a.proxy(function(){this.$element.trigger("loaded.bs.modal")},this))};b.DEFAULTS={backdrop:!0,keyboard:!0,show:!0},b.prototype.toggle=function(a){return this[this.isShown?"hide":"show"](a)},b.prototype.show=function(b){var c=this,d=a.Event("show.bs.modal",{relatedTarget:b});this.$element.trigger(d),this.isShown||d.isDefaultPrevented()||(this.isShown=!0,this.escape(),this.$element.on("click.dismiss.bs.modal",'[data-dismiss="modal"]',a.proxy(this.hide,this)),this.backdrop(function(){var d=a.support.transition&&c.$element.hasClass("fade");c.$element.parent().length||c.$element.appendTo(document.body),c.$element.show().scrollTop(0),d&&c.$element[0].offsetWidth,c.$element.addClass("in").attr("aria-hidden",!1),c.enforceFocus();var e=a.Event("shown.bs.modal",{relatedTarget:b});d?c.$element.find(".modal-dialog").one(a.support.transition.end,function(){c.$element.focus().trigger(e)}).emulateTransitionEnd(300):c.$element.focus().trigger(e)}))},b.prototype.hide=function(b){b&&b.preventDefault(),b=a.Event("hide.bs.modal"),this.$element.trigger(b),this.isShown&&!b.isDefaultPrevented()&&(this.isShown=!1,this.escape(),a(document).off("focusin.bs.modal"),this.$element.removeClass("in").attr("aria-hidden",!0).off("click.dismiss.bs.modal"),a.support.transition&&this.$element.hasClass("fade")?this.$element.one(a.support.transition.end,a.proxy(this.hideModal,this)).emulateTransitionEnd(300):this.hideModal())},b.prototype.enforceFocus=function(){a(document).off("focusin.bs.modal").on("focusin.bs.modal",a.proxy(function(a){this.$element[0]===a.target||this.$element.has(a.target).length||this.$element.focus()},this))},b.prototype.escape=function(){this.isShown&&this.options.keyboard?this.$element.on("keyup.dismiss.bs.modal",a.proxy(function(a){27==a.which&&this.hide()},this)):this.isShown||this.$element.off("keyup.dismiss.bs.modal")},b.prototype.hideModal=function(){var a=this;this.$element.hide(),this.backdrop(function(){a.removeBackdrop(),a.$element.trigger("hidden.bs.modal")})},b.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null},b.prototype.backdrop=function(b){var c=this.$element.hasClass("fade")?"fade":"";if(this.isShown&&this.options.backdrop){var d=a.support.transition&&c;if(this.$backdrop=a('<div class="modal-backdrop '+c+'" />').appendTo(document.body),this.$element.on("click.dismiss.bs.modal",a.proxy(function(a){a.target===a.currentTarget&&("static"==this.options.backdrop?this.$element[0].focus.call(this.$element[0]):this.hide.call(this))},this)),d&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in"),!b)return;d?this.$backdrop.one(a.support.transition.end,b).emulateTransitionEnd(150):b()}else!this.isShown&&this.$backdrop?(this.$backdrop.removeClass("in"),a.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one(a.support.transition.end,b).emulateTransitionEnd(150):b()):b&&b()};var c=a.fn.modal;a.fn.modal=function(c,d){return this.each(function(){var e=a(this),f=e.data("bs.modal"),g=a.extend({},b.DEFAULTS,e.data(),"object"==typeof c&&c);f||e.data("bs.modal",f=new b(this,g)),"string"==typeof c?f[c](d):g.show&&f.show(d)})},a.fn.modal.Constructor=b,a.fn.modal.noConflict=function(){return a.fn.modal=c,this},a(document).on("click.bs.modal.data-api",'[data-toggle="modal"]',function(b){var c=a(this),d=c.attr("href"),e=a(c.attr("data-target")||d&&d.replace(/.*(?=#[^\s]+$)/,"")),f=e.data("bs.modal")?"toggle":a.extend({remote:!/#/.test(d)&&d},e.data(),c.data());c.is("a")&&b.preventDefault(),e.modal(f,this).one("hide",function(){c.is(":visible")&&c.focus()})}),a(document).on("show.bs.modal",".modal",function(){a(document.body).addClass("modal-open")}).on("hidden.bs.modal",".modal",function(){a(document.body).removeClass("modal-open")})}(jQuery),+function(a){"use strict";var b=function(a,b){this.type=this.options=this.enabled=this.timeout=this.hoverState=this.$element=null,this.init("tooltip",a,b)};b.DEFAULTS={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1},b.prototype.init=function(b,c,d){this.enabled=!0,this.type=b,this.$element=a(c),this.options=this.getOptions(d);for(var e=this.options.trigger.split(" "),f=e.length;f--;){var g=e[f];if("click"==g)this.$element.on("click."+this.type,this.options.selector,a.proxy(this.toggle,this));else if("manual"!=g){var h="hover"==g?"mouseenter":"focusin",i="hover"==g?"mouseleave":"focusout";this.$element.on(h+"."+this.type,this.options.selector,a.proxy(this.enter,this)),this.$element.on(i+"."+this.type,this.options.selector,a.proxy(this.leave,this))}}this.options.selector?this._options=a.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()},b.prototype.getDefaults=function(){return b.DEFAULTS},b.prototype.getOptions=function(b){return b=a.extend({},this.getDefaults(),this.$element.data(),b),b.delay&&"number"==typeof b.delay&&(b.delay={show:b.delay,hide:b.delay}),b},b.prototype.getDelegateOptions=function(){var b={},c=this.getDefaults();return this._options&&a.each(this._options,function(a,d){c[a]!=d&&(b[a]=d)}),b},b.prototype.enter=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type);return clearTimeout(c.timeout),c.hoverState="in",c.options.delay&&c.options.delay.show?void(c.timeout=setTimeout(function(){"in"==c.hoverState&&c.show()},c.options.delay.show)):c.show()},b.prototype.leave=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type);return clearTimeout(c.timeout),c.hoverState="out",c.options.delay&&c.options.delay.hide?void(c.timeout=setTimeout(function(){"out"==c.hoverState&&c.hide()},c.options.delay.hide)):c.hide()},b.prototype.show=function(){var b=a.Event("show.bs."+this.type);if(this.hasContent()&&this.enabled){if(this.$element.trigger(b),b.isDefaultPrevented())return;var c=this,d=this.tip();this.setContent(),this.options.animation&&d.addClass("fade");var e="function"==typeof this.options.placement?this.options.placement.call(this,d[0],this.$element[0]):this.options.placement,f=/\s?auto?\s?/i,g=f.test(e);g&&(e=e.replace(f,"")||"top"),d.detach().css({top:0,left:0,display:"block"}).addClass(e),this.options.container?d.appendTo(this.options.container):d.insertAfter(this.$element);var h=this.getPosition(),i=d[0].offsetWidth,j=d[0].offsetHeight;if(g){var k=this.$element.parent(),l=e,m=document.documentElement.scrollTop||document.body.scrollTop,n="body"==this.options.container?window.innerWidth:k.outerWidth(),o="body"==this.options.container?window.innerHeight:k.outerHeight(),p="body"==this.options.container?0:k.offset().left;e="bottom"==e&&h.top+h.height+j-m>o?"top":"top"==e&&h.top-m-j<0?"bottom":"right"==e&&h.right+i>n?"left":"left"==e&&h.left-i<p?"right":e,d.removeClass(l).addClass(e)}var q=this.getCalculatedOffset(e,h,i,j);this.applyPlacement(q,e),this.hoverState=null;var r=function(){c.$element.trigger("shown.bs."+c.type)};a.support.transition&&this.$tip.hasClass("fade")?d.one(a.support.transition.end,r).emulateTransitionEnd(150):r()}},b.prototype.applyPlacement=function(b,c){var d,e=this.tip(),f=e[0].offsetWidth,g=e[0].offsetHeight,h=parseInt(e.css("margin-top"),10),i=parseInt(e.css("margin-left"),10);isNaN(h)&&(h=0),isNaN(i)&&(i=0),b.top=b.top+h,b.left=b.left+i,a.offset.setOffset(e[0],a.extend({using:function(a){e.css({top:Math.round(a.top),left:Math.round(a.left)})}},b),0),e.addClass("in");var j=e[0].offsetWidth,k=e[0].offsetHeight;if("top"==c&&k!=g&&(d=!0,b.top=b.top+g-k),/bottom|top/.test(c)){var l=0;b.left<0&&(l=-2*b.left,b.left=0,e.offset(b),j=e[0].offsetWidth,k=e[0].offsetHeight),this.replaceArrow(l-f+j,j,"left")}else this.replaceArrow(k-g,k,"top");d&&e.offset(b)},b.prototype.replaceArrow=function(a,b,c){this.arrow().css(c,a?50*(1-a/b)+"%":"")},b.prototype.setContent=function(){var a=this.tip(),b=this.getTitle();a.find(".tooltip-inner")[this.options.html?"html":"text"](b),a.removeClass("fade in top bottom left right")},b.prototype.hide=function(){function b(){"in"!=c.hoverState&&d.detach(),c.$element.trigger("hidden.bs."+c.type)}var c=this,d=this.tip(),e=a.Event("hide.bs."+this.type);return this.$element.trigger(e),e.isDefaultPrevented()?void 0:(d.removeClass("in"),a.support.transition&&this.$tip.hasClass("fade")?d.one(a.support.transition.end,b).emulateTransitionEnd(150):b(),this.hoverState=null,this)},b.prototype.fixTitle=function(){var a=this.$element;(a.attr("title")||"string"!=typeof a.attr("data-original-title"))&&a.attr("data-original-title",a.attr("title")||"").attr("title","")},b.prototype.hasContent=function(){return this.getTitle()},b.prototype.getPosition=function(){var b=this.$element[0];return a.extend({},"function"==typeof b.getBoundingClientRect?b.getBoundingClientRect():{width:b.offsetWidth,height:b.offsetHeight},this.$element.offset())},b.prototype.getCalculatedOffset=function(a,b,c,d){return"bottom"==a?{top:b.top+b.height,left:b.left+b.width/2-c/2}:"top"==a?{top:b.top-d,left:b.left+b.width/2-c/2}:"left"==a?{top:b.top+b.height/2-d/2,left:b.left-c}:{top:b.top+b.height/2-d/2,left:b.left+b.width}},b.prototype.getTitle=function(){var a,b=this.$element,c=this.options;return a=b.attr("data-original-title")||("function"==typeof c.title?c.title.call(b[0]):c.title)},b.prototype.tip=function(){return this.$tip=this.$tip||a(this.options.template)},b.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")},b.prototype.validate=function(){this.$element[0].parentNode||(this.hide(),this.$element=null,this.options=null)},b.prototype.enable=function(){this.enabled=!0},b.prototype.disable=function(){this.enabled=!1},b.prototype.toggleEnabled=function(){this.enabled=!this.enabled},b.prototype.toggle=function(b){var c=b?a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type):this;c.tip().hasClass("in")?c.leave(c):c.enter(c)},b.prototype.destroy=function(){clearTimeout(this.timeout),this.hide().$element.off("."+this.type).removeData("bs."+this.type)};var c=a.fn.tooltip;a.fn.tooltip=function(c){return this.each(function(){var d=a(this),e=d.data("bs.tooltip"),f="object"==typeof c&&c;(e||"destroy"!=c)&&(e||d.data("bs.tooltip",e=new b(this,f)),"string"==typeof c&&e[c]())})},a.fn.tooltip.Constructor=b,a.fn.tooltip.noConflict=function(){return a.fn.tooltip=c,this}}(jQuery),+function(a){"use strict";var b=function(a,b){this.init("popover",a,b)};if(!a.fn.tooltip)throw new Error("Popover requires tooltip.js");b.DEFAULTS=a.extend({},a.fn.tooltip.Constructor.DEFAULTS,{placement:"right",trigger:"click",content:"",template:'<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'}),b.prototype=a.extend({},a.fn.tooltip.Constructor.prototype),b.prototype.constructor=b,b.prototype.getDefaults=function(){return b.DEFAULTS},b.prototype.setContent=function(){var a=this.tip(),b=this.getTitle(),c=this.getContent();a.find(".popover-title")[this.options.html?"html":"text"](b),a.find(".popover-content")[this.options.html?"string"==typeof c?"html":"append":"text"](c),a.removeClass("fade top bottom left right in"),a.find(".popover-title").html()||a.find(".popover-title").hide()},b.prototype.hasContent=function(){return this.getTitle()||this.getContent()},b.prototype.getContent=function(){var a=this.$element,b=this.options;return a.attr("data-content")||("function"==typeof b.content?b.content.call(a[0]):b.content)},b.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".arrow")},b.prototype.tip=function(){return this.$tip||(this.$tip=a(this.options.template)),this.$tip};var c=a.fn.popover;a.fn.popover=function(c){return this.each(function(){var d=a(this),e=d.data("bs.popover"),f="object"==typeof c&&c;(e||"destroy"!=c)&&(e||d.data("bs.popover",e=new b(this,f)),"string"==typeof c&&e[c]())})},a.fn.popover.Constructor=b,a.fn.popover.noConflict=function(){return a.fn.popover=c,this}}(jQuery),+function(a){"use strict";function b(c,d){var e,f=a.proxy(this.process,this);this.$element=a(a(c).is("body")?window:c),this.$body=a("body"),this.$scrollElement=this.$element.on("scroll.bs.scroll-spy.data-api",f),this.options=a.extend({},b.DEFAULTS,d),this.selector=(this.options.target||(e=a(c).attr("href"))&&e.replace(/.*(?=#[^\s]+$)/,"")||"")+" .nav li > a",this.offsets=a([]),this.targets=a([]),this.activeTarget=null,this.refresh(),this.process()}b.DEFAULTS={offset:10},b.prototype.refresh=function(){var b=this.$element[0]==window?"offset":"position";this.offsets=a([]),this.targets=a([]);{var c=this;this.$body.find(this.selector).map(function(){var d=a(this),e=d.data("target")||d.attr("href"),f=/^#./.test(e)&&a(e);return f&&f.length&&f.is(":visible")&&[[f[b]().top+(!a.isWindow(c.$scrollElement.get(0))&&c.$scrollElement.scrollTop()),e]]||null}).sort(function(a,b){return a[0]-b[0]}).each(function(){c.offsets.push(this[0]),c.targets.push(this[1])})}},b.prototype.process=function(){var a,b=this.$scrollElement.scrollTop()+this.options.offset,c=this.$scrollElement[0].scrollHeight||this.$body[0].scrollHeight,d=c-this.$scrollElement.height(),e=this.offsets,f=this.targets,g=this.activeTarget;if(b>=d)return g!=(a=f.last()[0])&&this.activate(a);if(g&&b<=e[0])return g!=(a=f[0])&&this.activate(a);for(a=e.length;a--;)g!=f[a]&&b>=e[a]&&(!e[a+1]||b<=e[a+1])&&this.activate(f[a])},b.prototype.activate=function(b){this.activeTarget=b,a(this.selector).parentsUntil(this.options.target,".active").removeClass("active");var c=this.selector+'[data-target="'+b+'"],'+this.selector+'[href="'+b+'"]',d=a(c).parents("li").addClass("active");d.parent(".dropdown-menu").length&&(d=d.closest("li.dropdown").addClass("active")),d.trigger("activate.bs.scrollspy")};var c=a.fn.scrollspy;a.fn.scrollspy=function(c){return this.each(function(){var d=a(this),e=d.data("bs.scrollspy"),f="object"==typeof c&&c;e||d.data("bs.scrollspy",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.scrollspy.Constructor=b,a.fn.scrollspy.noConflict=function(){return a.fn.scrollspy=c,this},a(window).on("load",function(){a('[data-spy="scroll"]').each(function(){var b=a(this);b.scrollspy(b.data())})})}(jQuery),+function(a){"use strict";var b=function(b){this.element=a(b)};b.prototype.show=function(){var b=this.element,c=b.closest("ul:not(.dropdown-menu)"),d=b.data("target");if(d||(d=b.attr("href"),d=d&&d.replace(/.*(?=#[^\s]*$)/,"")),!b.parent("li").hasClass("active")){var e=c.find(".active:last a")[0],f=a.Event("show.bs.tab",{relatedTarget:e});if(b.trigger(f),!f.isDefaultPrevented()){var g=a(d);this.activate(b.parent("li"),c),this.activate(g,g.parent(),function(){b.trigger({type:"shown.bs.tab",relatedTarget:e})})}}},b.prototype.activate=function(b,c,d){function e(){f.removeClass("active").find("> .dropdown-menu > .active").removeClass("active"),b.addClass("active"),g?(b[0].offsetWidth,b.addClass("in")):b.removeClass("fade"),b.parent(".dropdown-menu")&&b.closest("li.dropdown").addClass("active"),d&&d()}var f=c.find("> .active"),g=d&&a.support.transition&&f.hasClass("fade");g?f.one(a.support.transition.end,e).emulateTransitionEnd(150):e(),f.removeClass("in")};var c=a.fn.tab;a.fn.tab=function(c){return this.each(function(){var d=a(this),e=d.data("bs.tab");e||d.data("bs.tab",e=new b(this)),"string"==typeof c&&e[c]()})},a.fn.tab.Constructor=b,a.fn.tab.noConflict=function(){return a.fn.tab=c,this},a(document).on("click.bs.tab.data-api",'[data-toggle="tab"], [data-toggle="pill"]',function(b){b.preventDefault(),a(this).tab("show")})}(jQuery),+function(a){"use strict";var b=function(c,d){this.options=a.extend({},b.DEFAULTS,d),this.$window=a(window).on("scroll.bs.affix.data-api",a.proxy(this.checkPosition,this)).on("click.bs.affix.data-api",a.proxy(this.checkPositionWithEventLoop,this)),this.$element=a(c),this.affixed=this.unpin=this.pinnedOffset=null,this.checkPosition()};b.RESET="affix affix-top affix-bottom",b.DEFAULTS={offset:0},b.prototype.getPinnedOffset=function(){if(this.pinnedOffset)return this.pinnedOffset;this.$element.removeClass(b.RESET).addClass("affix");var a=this.$window.scrollTop(),c=this.$element.offset();return this.pinnedOffset=c.top-a},b.prototype.checkPositionWithEventLoop=function(){setTimeout(a.proxy(this.checkPosition,this),1)},b.prototype.checkPosition=function(){if(this.$element.is(":visible")){var c=a(document).height(),d=this.$window.scrollTop(),e=this.$element.offset(),f=this.options.offset,g=f.top,h=f.bottom;"top"==this.affixed&&(e.top+=d),"object"!=typeof f&&(h=g=f),"function"==typeof g&&(g=f.top(this.$element)),"function"==typeof h&&(h=f.bottom(this.$element));var i=null!=this.unpin&&d+this.unpin<=e.top?!1:null!=h&&e.top+this.$element.height()>=c-h?"bottom":null!=g&&g>=d?"top":!1;if(this.affixed!==i){this.unpin&&this.$element.css("top","");var j="affix"+(i?"-"+i:""),k=a.Event(j+".bs.affix");this.$element.trigger(k),k.isDefaultPrevented()||(this.affixed=i,this.unpin="bottom"==i?this.getPinnedOffset():null,this.$element.removeClass(b.RESET).addClass(j).trigger(a.Event(j.replace("affix","affixed"))),"bottom"==i&&this.$element.offset({top:c-h-this.$element.height()}))}}};var c=a.fn.affix;a.fn.affix=function(c){return this.each(function(){var d=a(this),e=d.data("bs.affix"),f="object"==typeof c&&c;e||d.data("bs.affix",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.affix.Constructor=b,a.fn.affix.noConflict=function(){return a.fn.affix=c,this},a(window).on("load",function(){a('[data-spy="affix"]').each(function(){var b=a(this),c=b.data();c.offset=c.offset||{},c.offsetBottom&&(c.offset.bottom=c.offsetBottom),c.offsetTop&&(c.offset.top=c.offsetTop),b.affix(c)})})}(jQuery);
$(document).on("page:change", function(){

  var nowTemp = new Date();
  var five_years_ago = new Date(nowTemp.getFullYear()-5, nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
   
  var fromDate = $('#criteria_from_date').datepicker({
    onRender: function(date) {
      return date.valueOf() < five_years_ago.valueOf() ? 'disabled' : '';
    }
  }).on('changeDate', function(ev) {
    if (ev.date.valueOf() > closedAt.date.valueOf()) {
      var newDate = new Date(ev.date)
      newDate.setDate(newDate.getDate());
      closedAt.setValue(newDate);
    }
    fromDate.hide();
    $('#criteria_to_date')[0].focus();
  }).data('datepicker');
  var closedAt = $('#criteria_to_date').datepicker({
    onRender: function(date) {
      return date.valueOf() < fromDate.date.valueOf() ? 'disabled' : '';
    }
  }).on('changeDate', function(ev) {
    closedAt.hide();
  }).data('datepicker');

});
(function() {


}).call(this);
(function() {


}).call(this);
(function() {


}).call(this);
(function() {


}).call(this);
(function() {


}).call(this);
var fontawesome={};fontawesome.markers={"GLASS":"M3.348 -57.743q0 -0.828 0.648 -1.314t1.368 -0.63 1.548 -0.144h50.688q0.828 0 1.548 0.144t1.368 0.63 0.648 1.314q0 1.26 -1.548 2.808l-22.752 22.752v27.648h11.52q0.936 0 1.62 0.684t0.684 1.62 -0.684 1.62 -1.62 0.684h-32.256q-0.936 0 -1.62 -0.684t-0.684 -1.62 0.684 -1.62 1.62 -0.684h11.52v-27.648l-22.752 -22.752q-1.548 -1.548 -1.548 -2.808z","MUSIC":"M0 -6.839q0 -1.8 1.224 -3.204t3.096 -2.178 3.726 -1.152 3.474 -0.378q3.78 0 6.912 1.404v-34.812q0 -1.116 0.684 -2.034t1.764 -1.278l29.952 -9.216q0.432 -0.144 1.008 -0.144 1.44 0 2.448 1.008t1.008 2.448v40.32q0 1.8 -1.224 3.204t-3.096 2.178 -3.726 1.152 -3.474 0.378 -3.474 -0.378 -3.726 -1.152 -3.096 -2.178 -1.224 -3.204 1.224 -3.204 3.096 -2.178 3.726 -1.152 3.474 -0.378q3.78 0 6.912 1.404v-19.332l-27.648 8.532v25.524q0 1.8 -1.224 3.204t-3.096 2.178 -3.726 1.152 -3.474 0.378 -3.474 -0.378 -3.726 -1.152 -3.096 -2.178 -1.224 -3.204z","SEARCH":"M0 -34.487q0 -5.148 1.998 -9.846t5.4 -8.1 8.1 -5.4 9.846 -1.998 9.846 1.998 8.1 5.4 5.4 8.1 1.998 9.846q0 7.92 -4.464 14.364l12.348 12.348q1.332 1.332 1.332 3.24 0 1.872 -1.368 3.24t-3.24 1.368q-1.944 0 -3.24 -1.368l-12.348 -12.312q-6.444 4.464 -14.364 4.464 -5.148 0 -9.846 -1.998t-8.1 -5.4 -5.4 -8.1 -1.998 -9.846zm9.216 0q0 6.66 4.734 11.394t11.394 4.734 11.394 -4.734 4.734 -11.394 -4.734 -11.394 -11.394 -4.734 -11.394 4.734 -4.734 11.394z","ENVELOPE_O":"M0 -10.295v-39.168q0 -2.376 1.692 -4.068t4.068 -1.692h52.992q2.376 0 4.068 1.692t1.692 4.068v39.168q0 2.376 -1.692 4.068t-4.068 1.692h-52.992q-2.376 0 -4.068 -1.692t-1.692 -4.068zm4.608 0q0 0.468 0.342 0.81t0.81 0.342h52.992q0.468 0 0.81 -0.342t0.342 -0.81v-27.648q-1.152 1.296 -2.484 2.376 -9.648 7.416 -15.336 12.168 -1.836 1.548 -2.988 2.412t-3.114 1.746 -3.69 0.882h-0.072q-1.728 0 -3.69 -0.882t-3.114 -1.746 -2.988 -2.412q-5.688 -4.752 -15.336 -12.168 -1.332 -1.08 -2.484 -2.376v27.648zm0 -39.168q0 6.048 5.292 10.224 6.948 5.472 14.436 11.412 0.216 0.18 1.26 1.062t1.656 1.35 1.602 1.134 1.818 0.99 1.548 0.324h0.072q0.72 0 1.548 -0.324t1.818 -0.99 1.602 -1.134 1.656 -1.35 1.26 -1.062q7.488 -5.94 14.436 -11.412 1.944 -1.548 3.618 -4.158t1.674 -4.734v-0.882l-0.018 -0.468 -0.108 -0.45 -0.198 -0.324 -0.324 -0.27 -0.504 -0.09h-52.992q-0.468 0 -0.81 0.342t-0.342 0.81z","HEART":"M0 -42.983q0 -7.92 4.572 -12.384t12.636 -4.464q2.232 0 4.554 0.774t4.32 2.088 3.438 2.466 2.736 2.448q1.296 -1.296 2.736 -2.448t3.438 -2.466 4.32 -2.088 4.554 -0.774q8.064 0 12.636 4.464t4.572 12.384q0 7.956 -8.244 16.2l-22.428 21.6q-0.648 0.648 -1.584 0.648t-1.584 -0.648l-22.464 -21.672q-0.36 -0.288 -0.99 -0.936t-1.998 -2.358 -2.448 -3.51 -1.926 -4.356 -0.846 -4.968z","STAR":"M0 -41.147q0 -1.332 2.016 -1.656l18.072 -2.628 8.1 -16.38q0.684 -1.476 1.764 -1.476t1.764 1.476l8.1 16.38 18.072 2.628q2.016 0.324 2.016 1.656 0 0.792 -0.936 1.728l-13.068 12.744 3.096 18q0.036 0.252 0.036 0.72 0 0.756 -0.378 1.278t-1.098 0.522q-0.684 0 -1.44 -0.432l-16.164 -8.496 -16.164 8.496q-0.792 0.432 -1.44 0.432 -0.756 0 -1.134 -0.522t-0.378 -1.278q0 -0.216 0.072 -0.72l3.096 -18 -13.104 -12.744q-0.9 -0.972 -0.9 -1.728z","STAR_O":"M0 -41.147q0 -1.332 2.016 -1.656l18.072 -2.628 8.1 -16.38q0.684 -1.476 1.764 -1.476t1.764 1.476l8.1 16.38 18.072 2.628q2.016 0.324 2.016 1.656 0 0.792 -0.936 1.728l-13.068 12.744 3.096 18q0.036 0.252 0.036 0.72 0 1.8 -1.476 1.8 -0.684 0 -1.44 -0.432l-16.164 -8.496 -16.164 8.496q-0.792 0.432 -1.44 0.432 -0.756 0 -1.134 -0.522t-0.378 -1.278q0 -0.216 0.072 -0.72l3.096 -18 -13.104 -12.744q-0.9 -0.972 -0.9 -1.728zm7.956 2.16l11.016 10.692 -2.628 15.156 13.608 -7.164 13.572 7.164 -2.592 -15.156 11.016 -10.692 -15.192 -2.232 -6.804 -13.752 -6.804 13.752z","USER":"M0 -13.859q0 -1.908 0.126 -3.726t0.504 -3.924 0.954 -3.906 1.548 -3.51 2.232 -2.916 3.078 -1.926 4.014 -0.72q0.324 0 1.512 0.774t2.682 1.728 3.888 1.728 4.806 0.774 4.806 -0.774 3.888 -1.728 2.682 -1.728 1.512 -0.774q2.196 0 4.014 0.72t3.078 1.926 2.232 2.916 1.548 3.51 0.954 3.906 0.504 3.924 0.126 3.726q0 4.32 -2.628 6.822t-6.984 2.502h-31.464q-4.356 0 -6.984 -2.502t-2.628 -6.822zm11.52 -32.148q0 -5.724 4.05 -9.774t9.774 -4.05 9.774 4.05 4.05 9.774 -4.05 9.774 -9.774 4.05 -9.774 -4.05 -4.05 -9.774z","FILM":"M0 -5.687v-48.384q0 -2.376 1.692 -4.068t4.068 -1.692h57.6q2.376 0 4.068 1.692t1.692 4.068v48.384q0 2.376 -1.692 4.068t-4.068 1.692h-57.6q-2.376 0 -4.068 -1.692t-1.692 -4.068zm4.608 -5.76v4.608q0 0.936 0.684 1.62t1.62 0.684h4.608q0.936 0 1.62 -0.684t0.684 -1.62v-4.608q0 -0.936 -0.684 -1.62t-1.62 -0.684h-4.608q-0.936 0 -1.62 0.684t-0.684 1.62zm0 -9.216q0 0.936 0.684 1.62t1.62 0.684h4.608q0.936 0 1.62 -0.684t0.684 -1.62v-4.608q0 -0.936 -0.684 -1.62t-1.62 -0.684h-4.608q-0.936 0 -1.62 0.684t-0.684 1.62v4.608zm0 -13.824q0 0.936 0.684 1.62t1.62 0.684h4.608q0.936 0 1.62 -0.684t0.684 -1.62v-4.608q0 -0.936 -0.684 -1.62t-1.62 -0.684h-4.608q-0.936 0 -1.62 0.684t-0.684 1.62v4.608zm0 -13.824q0 0.936 0.684 1.62t1.62 0.684h4.608q0.936 0 1.62 -0.684t0.684 -1.62v-4.608q0 -0.936 -0.684 -1.62t-1.62 -0.684h-4.608q-0.936 0 -1.62 0.684t-0.684 1.62v4.608zm13.824 41.472q0 0.936 0.684 1.62t1.62 0.684h27.648q0.936 0 1.62 -0.684t0.684 -1.62v-18.432q0 -0.936 -0.684 -1.62t-1.62 -0.684h-27.648q-0.936 0 -1.62 0.684t-0.684 1.62v18.432zm0 -27.648q0 0.936 0.684 1.62t1.62 0.684h27.648q0.936 0 1.62 -0.684t0.684 -1.62v-18.432q0 -0.936 -0.684 -1.62t-1.62 -0.684h-27.648q-0.936 0 -1.62 0.684t-0.684 1.62v18.432zm36.864 23.04v4.608q0 0.936 0.684 1.62t1.62 0.684h4.608q0.936 0 1.62 -0.684t0.684 -1.62v-4.608q0 -0.936 -0.684 -1.62t-1.62 -0.684h-4.608q-0.936 0 -1.62 0.684t-0.684 1.62zm0 -9.216q0 0.936 0.684 1.62t1.62 0.684h4.608q0.936 0 1.62 -0.684t0.684 -1.62v-4.608q0 -0.936 -0.684 -1.62t-1.62 -0.684h-4.608q-0.936 0 -1.62 0.684t-0.684 1.62v4.608zm0 -13.824q0 0.936 0.684 1.62t1.62 0.684h4.608q0.936 0 1.62 -0.684t0.684 -1.62v-4.608q0 -0.936 -0.684 -1.62t-1.62 -0.684h-4.608q-0.936 0 -1.62 0.684t-0.684 1.62v4.608zm0 -13.824q0 0.936 0.684 1.62t1.62 0.684h4.608q0.936 0 1.62 -0.684t0.684 -1.62v-4.608q0 -0.936 -0.684 -1.62t-1.62 -0.684h-4.608q-0.936 0 -1.62 0.684t-0.684 1.62v4.608z","TH_LARGE":"M0 -13.751v-13.824q0 -1.872 1.368 -3.24t3.24 -1.368h18.432q1.872 0 3.24 1.368t1.368 3.24v13.824q0 1.872 -1.368 3.24t-3.24 1.368h-18.432q-1.872 0 -3.24 -1.368t-1.368 -3.24zm0 -27.648v-13.824q0 -1.872 1.368 -3.24t3.24 -1.368h18.432q1.872 0 3.24 1.368t1.368 3.24v13.824q0 1.872 -1.368 3.24t-3.24 1.368h-18.432q-1.872 0 -3.24 -1.368t-1.368 -3.24zm32.256 27.648v-13.824q0 -1.872 1.368 -3.24t3.24 -1.368h18.432q1.872 0 3.24 1.368t1.368 3.24v13.824q0 1.872 -1.368 3.24t-3.24 1.368h-18.432q-1.872 0 -3.24 -1.368t-1.368 -3.24zm0 -27.648v-13.824q0 -1.872 1.368 -3.24t3.24 -1.368h18.432q1.872 0 3.24 1.368t1.368 3.24v13.824q0 1.872 -1.368 3.24t-3.24 1.368h-18.432q-1.872 0 -3.24 -1.368t-1.368 -3.24z","TH":"M0 -12.599v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448zm0 -18.432v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448zm0 -18.432v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448zm23.04 36.864v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448zm0 -18.432v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448zm0 -18.432v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448zm23.04 36.864v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448zm0 -18.432v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448zm0 -18.432v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448z","TH_LIST":"M0 -12.599v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448zm0 -18.432v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448zm0 -18.432v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448zm23.04 36.864v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h34.56q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-34.56q-1.44 0 -2.448 -1.008t-1.008 -2.448zm0 -18.432v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h34.56q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-34.56q-1.44 0 -2.448 -1.008t-1.008 -2.448zm0 -18.432v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h34.56q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-34.56q-1.44 0 -2.448 -1.008t-1.008 -2.448z","CHECK":"M4.356 -31.031q0 -1.44 1.008 -2.448l4.896 -4.896q1.008 -1.008 2.448 -1.008t2.448 1.008l10.584 10.62 23.616 -23.652q1.008 -1.008 2.448 -1.008t2.448 1.008l4.896 4.896q1.008 1.008 1.008 2.448t-1.008 2.448l-26.064 26.064 -4.896 4.896q-1.008 1.008 -2.448 1.008t-2.448 -1.008l-4.896 -4.896 -13.032 -13.032q-1.008 -1.008 -1.008 -2.448z","TIMES":"M3.96 -16.847q0 -1.44 1.008 -2.448l10.584 -10.584 -10.584 -10.584q-1.008 -1.008 -1.008 -2.448t1.008 -2.448l4.896 -4.896q1.008 -1.008 2.448 -1.008t2.448 1.008l10.584 10.584 10.584 -10.584q1.008 -1.008 2.448 -1.008t2.448 1.008l4.896 4.896q1.008 1.008 1.008 2.448t-1.008 2.448l-10.584 10.584 10.584 10.584q1.008 1.008 1.008 2.448t-1.008 2.448l-4.896 4.896q-1.008 1.008 -2.448 1.008t-2.448 -1.008l-10.584 -10.584 -10.584 10.584q-1.008 1.008 -2.448 1.008t-2.448 -1.008l-4.896 -4.896q-1.008 -1.008 -1.008 -2.448z","SEARCH_PLUS":"M0 -34.487q0 -5.148 1.998 -9.846t5.4 -8.1 8.1 -5.4 9.846 -1.998 9.846 1.998 8.1 5.4 5.4 8.1 1.998 9.846q0 7.92 -4.464 14.364l12.348 12.348q1.332 1.332 1.332 3.24t-1.35 3.258 -3.258 1.35q-1.944 0 -3.24 -1.368l-12.348 -12.312q-6.444 4.464 -14.364 4.464 -5.148 0 -9.846 -1.998t-8.1 -5.4 -5.4 -8.1 -1.998 -9.846zm9.216 0q0 6.66 4.734 11.394t11.394 4.734 11.394 -4.734 4.734 -11.394 -4.734 -11.394 -11.394 -4.734 -11.394 4.734 -4.734 11.394zm4.608 1.152v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h8.064v-8.064q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v8.064h8.064q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-8.064v8.064q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81v-8.064h-8.064q-0.468 0 -0.81 -0.342t-0.342 -0.81z","SEARCH_MINUS":"M0 -34.487q0 -5.148 1.998 -9.846t5.4 -8.1 8.1 -5.4 9.846 -1.998 9.846 1.998 8.1 5.4 5.4 8.1 1.998 9.846q0 7.92 -4.464 14.364l12.348 12.348q1.332 1.332 1.332 3.24t-1.35 3.258 -3.258 1.35q-1.944 0 -3.24 -1.368l-12.348 -12.312q-6.444 4.464 -14.364 4.464 -5.148 0 -9.846 -1.998t-8.1 -5.4 -5.4 -8.1 -1.998 -9.846zm9.216 0q0 6.66 4.734 11.394t11.394 4.734 11.394 -4.734 4.734 -11.394 -4.734 -11.394 -11.394 -4.734 -11.394 4.734 -4.734 11.394zm4.608 1.152v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h20.736q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-20.736q-0.468 0 -0.81 -0.342t-0.342 -0.81z","POWER_OFF":"M0 -32.183q0 -6.552 2.898 -12.348t8.154 -9.72q1.548 -1.152 3.438 -0.9t3.006 1.8q1.152 1.512 0.882 3.402t-1.782 3.042q-3.528 2.664 -5.454 6.516t-1.926 8.208q0 3.744 1.458 7.146t3.942 5.886 5.886 3.942 7.146 1.458 7.146 -1.458 5.886 -3.942 3.942 -5.886 1.458 -7.146q0 -4.356 -1.926 -8.208t-5.454 -6.516q-1.512 -1.152 -1.782 -3.042t0.882 -3.402q1.116 -1.548 3.024 -1.8t3.42 0.9q5.256 3.924 8.154 9.72t2.898 12.348q0 5.616 -2.196 10.728t-5.904 8.82 -8.82 5.904 -10.728 2.196 -10.728 -2.196 -8.82 -5.904 -5.904 -8.82 -2.196 -10.728zm23.04 -4.608v-23.04q0 -1.872 1.368 -3.24t3.24 -1.368 3.24 1.368 1.368 3.24v23.04q0 1.872 -1.368 3.24t-3.24 1.368 -3.24 -1.368 -1.368 -3.24z","SIGNAL":"M0 -5.687v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v6.912q0 0.504 -0.324 0.828t-0.828 0.324h-6.912q-0.504 0 -0.828 -0.324t-0.324 -0.828zm13.824 0v-11.52q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v11.52q0 0.504 -0.324 0.828t-0.828 0.324h-6.912q-0.504 0 -0.828 -0.324t-0.324 -0.828zm13.824 0v-20.736q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v20.736q0 0.504 -0.324 0.828t-0.828 0.324h-6.912q-0.504 0 -0.828 -0.324t-0.324 -0.828zm13.824 0v-34.56q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v34.56q0 0.504 -0.324 0.828t-0.828 0.324h-6.912q-0.504 0 -0.828 -0.324t-0.324 -0.828zm13.824 0v-52.992q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v52.992q0 0.504 -0.324 0.828t-0.828 0.324h-6.912q-0.504 0 -0.828 -0.324t-0.324 -0.828z","GEAR":"M0 -28.259v-7.992q0 -0.432 0.288 -0.828t0.684 -0.468l6.696 -1.008q0.504 -1.656 1.404 -3.312 -1.44 -2.052 -3.852 -4.968 -0.36 -0.432 -0.36 -0.864 0 -0.36 0.324 -0.828 0.936 -1.296 3.546 -3.87t3.402 -2.574q0.468 0 0.936 0.36l4.968 3.852q1.584 -0.828 3.276 -1.368 0.576 -4.896 1.044 -6.696 0.252 -1.008 1.296 -1.008h7.992q0.504 0 0.882 0.306t0.414 0.774l1.008 6.624q1.764 0.576 3.24 1.332l5.112 -3.852q0.324 -0.324 0.864 -0.324 0.468 0 0.9 0.36 4.644 4.284 5.94 6.12 0.252 0.288 0.252 0.792 0 0.432 -0.288 0.828 -0.54 0.756 -1.836 2.394t-1.944 2.538q0.936 1.8 1.476 3.528l6.588 1.008q0.468 0.072 0.756 0.45t0.288 0.846v7.992q0 0.432 -0.288 0.828t-0.72 0.468l-6.66 1.008q-0.684 1.944 -1.404 3.276 1.26 1.8 3.852 4.968 0.36 0.432 0.36 0.9t-0.324 0.828q-0.972 1.332 -3.564 3.888t-3.384 2.556q-0.432 0 -0.936 -0.324l-4.968 -3.888q-1.584 0.828 -3.276 1.368 -0.576 4.896 -1.044 6.696 -0.252 1.008 -1.296 1.008h-7.992q-0.504 0 -0.882 -0.306t-0.414 -0.774l-1.008 -6.624q-1.764 -0.576 -3.24 -1.332l-5.076 3.852q-0.36 0.324 -0.9 0.324 -0.504 0 -0.9 -0.396 -4.536 -4.104 -5.94 -6.048 -0.252 -0.36 -0.252 -0.828 0 -0.432 0.288 -0.828 0.54 -0.756 1.836 -2.394t1.944 -2.538q-0.972 -1.8 -1.476 -3.564l-6.588 -0.972q-0.468 -0.072 -0.756 -0.45t-0.288 -0.846zm18.432 -3.924q0 3.816 2.7 6.516t6.516 2.7 6.516 -2.7 2.7 -6.516 -2.7 -6.516 -6.516 -2.7 -6.516 2.7 -2.7 6.516z","TRASH_O":"M0 -47.159v-2.304q0 -0.504 0.324 -0.828t0.828 -0.324h11.124l2.52 -6.012q0.54 -1.332 1.944 -2.268t2.844 -0.936h11.52q1.44 0 2.844 0.936t1.944 2.268l2.52 6.012h11.124q0.504 0 0.828 0.324t0.324 0.828v2.304q0 0.504 -0.324 0.828t-0.828 0.324h-3.456v34.128q0 2.988 -1.692 5.166t-4.068 2.178h-29.952q-2.376 0 -4.068 -2.106t-1.692 -5.094v-34.272h-3.456q-0.504 0 -0.828 -0.324t-0.324 -0.828zm9.216 35.28q0 0.792 0.252 1.458t0.522 0.972 0.378 0.306h29.952q0.108 0 0.378 -0.306t0.522 -0.972 0.252 -1.458v-34.128h-32.256v34.128zm4.608 -5.328v-20.736q0 -0.504 0.324 -0.828t0.828 -0.324h2.304q0.504 0 0.828 0.324t0.324 0.828v20.736q0 0.504 -0.324 0.828t-0.828 0.324h-2.304q-0.504 0 -0.828 -0.324t-0.324 -0.828zm3.456 -33.408h16.128l-1.728 -4.212q-0.252 -0.324 -0.612 -0.396h-11.412q-0.36 0.072 -0.612 0.396zm5.76 33.408v-20.736q0 -0.504 0.324 -0.828t0.828 -0.324h2.304q0.504 0 0.828 0.324t0.324 0.828v20.736q0 0.504 -0.324 0.828t-0.828 0.324h-2.304q-0.504 0 -0.828 -0.324t-0.324 -0.828zm9.216 0v-20.736q0 -0.504 0.324 -0.828t0.828 -0.324h2.304q0.504 0 0.828 0.324t0.324 0.828v20.736q0 0.504 -0.324 0.828t-0.828 0.324h-2.304q-0.504 0 -0.828 -0.324t-0.324 -0.828z","HOME":"M0.936 -32.057q0.036 -0.486 0.396 -0.774l25.884 -21.564q1.152 -0.936 2.736 -0.936t2.736 0.936l8.784 7.344v-7.02q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v14.688l7.884 6.552q0.36 0.288 0.396 0.774t-0.252 0.846l-2.232 2.664q-0.288 0.324 -0.756 0.396h-0.108q-0.468 0 -0.756 -0.252l-24.912 -20.772 -24.912 20.772q-0.432 0.288 -0.864 0.252 -0.468 -0.072 -0.756 -0.396l-2.232 -2.664q-0.288 -0.36 -0.252 -0.846zm8.28 20.61v-17.28l0.018 -0.108 0.018 -0.108 20.7 -17.064 20.7 17.064q0.036 0.072 0.036 0.216v17.28q0 0.936 -0.684 1.62t-1.62 0.684h-13.824v-13.824h-9.216v13.824h-13.824q-0.936 0 -1.62 -0.684t-0.684 -1.62z","FILE_O":"M0 -3.383v-57.6q0 -1.44 1.008 -2.448t2.448 -1.008h32.256q1.44 0 3.168 0.72t2.736 1.728l11.232 11.232q1.008 1.008 1.728 2.736t0.72 3.168v41.472q0 1.44 -1.008 2.448t-2.448 1.008h-48.384q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 -1.152h46.08v-36.864h-14.976q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-27.648v55.296zm32.256 -41.472h13.536q-0.36 -1.044 -0.792 -1.476l-11.268 -11.268q-0.432 -0.432 -1.476 -0.792v13.536z","CLOCK_O":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm8.064 0q0 5.328 2.628 9.828t7.128 7.128 9.828 2.628 9.828 -2.628 7.128 -7.128 2.628 -9.828 -2.628 -9.828 -7.128 -7.128 -9.828 -2.628 -9.828 2.628 -7.128 7.128 -2.628 9.828zm10.368 3.456v-2.304q0 -0.504 0.324 -0.828t0.828 -0.324h8.064v-12.672q0 -0.504 0.324 -0.828t0.828 -0.324h2.304q0.504 0 0.828 0.324t0.324 0.828v16.128q0 0.504 -0.324 0.828t-0.828 0.324h-11.52q-0.504 0 -0.828 -0.324t-0.324 -0.828z","ROAD":"M1.8 -11.771q0 -1.944 0.936 -4.176l15.012 -37.584q0.288 -0.684 0.936 -1.188t1.368 -0.504h12.204q-0.468 0 -0.828 0.342t-0.396 0.81l-0.54 6.912q-0.036 0.504 0.288 0.828t0.792 0.324h5.976q0.468 0 0.792 -0.324t0.288 -0.828l-0.54 -6.912q-0.036 -0.468 -0.396 -0.81t-0.828 -0.342h12.204q0.72 0 1.368 0.504t0.936 1.188l15.012 37.584q0.936 2.232 0.936 4.176 0 2.628 -1.656 2.628h-25.344q0.468 0 0.792 -0.342t0.288 -0.81l-0.72 -9.216q-0.036 -0.468 -0.396 -0.81t-0.828 -0.342h-9.792q-0.468 0 -0.828 0.342t-0.396 0.81l-0.72 9.216q-0.036 0.468 0.288 0.81t0.792 0.342h-25.344q-1.656 0 -1.656 -2.628zm27.324 -16.812q-0.036 0.432 0.288 0.72t0.756 0.288h8.784q0.432 0 0.756 -0.288t0.288 -0.72v-0.144l-0.864 -11.52q-0.036 -0.468 -0.396 -0.81t-0.828 -0.342h-6.696q-0.468 0 -0.828 0.342t-0.396 0.81l-0.864 11.52v0.144z","DOWNLOAD":"M0 -12.599v-11.52q0 -1.44 1.008 -2.448t2.448 -1.008h16.74l4.86 4.896q2.088 2.016 4.896 2.016t4.896 -2.016l4.896 -4.896h16.704q1.44 0 2.448 1.008t1.008 2.448v11.52q0 1.44 -1.008 2.448t-2.448 1.008h-52.992q-1.44 0 -2.448 -1.008t-1.008 -2.448zm11.7 -32.004q0.612 -1.404 2.124 -1.404h9.216v-16.128q0 -0.936 0.684 -1.62t1.62 -0.684h9.216q0.936 0 1.62 0.684t0.684 1.62v16.128h9.216q1.512 0 2.124 1.404 0.612 1.476 -0.504 2.52l-16.128 16.128q-0.648 0.684 -1.62 0.684t-1.62 -0.684l-16.128 -16.128q-1.116 -1.044 -0.504 -2.52zm29.772 28.548q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62zm9.216 0q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62z","ARROW_CIRCLE_O_DOWN":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm8.064 0q0 5.328 2.628 9.828t7.128 7.128 9.828 2.628 9.828 -2.628 7.128 -7.128 2.628 -9.828 -2.628 -9.828 -7.128 -7.128 -9.828 -2.628 -9.828 2.628 -7.128 7.128 -2.628 9.828zm6.984 0.72q0.288 -0.72 1.08 -0.72h6.912v-12.672q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v12.672h6.912q0.504 0 0.828 0.324t0.324 0.828q0 0.432 -0.36 0.864l-11.484 11.484q-0.396 0.324 -0.828 0.324t-0.828 -0.324l-11.52 -11.52q-0.54 -0.576 -0.252 -1.26z","ARROW_CIRCLE_O_UP":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm8.064 0q0 5.328 2.628 9.828t7.128 7.128 9.828 2.628 9.828 -2.628 7.128 -7.128 2.628 -9.828 -2.628 -9.828 -7.128 -7.128 -9.828 -2.628 -9.828 2.628 -7.128 7.128 -2.628 9.828zm6.912 -1.152q0 -0.432 0.36 -0.864l11.484 -11.484q0.396 -0.324 0.828 -0.324t0.828 0.324l11.52 11.52q0.54 0.576 0.252 1.26 -0.288 0.72 -1.08 0.72h-6.912v12.672q0 0.504 -0.324 0.828t-0.828 0.324h-6.912q-0.504 0 -0.828 -0.324t-0.324 -0.828v-12.672h-6.912q-0.504 0 -0.828 -0.324t-0.324 -0.828z","INBOX":"M0 -11.447v-17.352q0 -2.232 0.9 -4.428l8.568 -19.872q0.36 -0.9 1.314 -1.512t1.89 -0.612h29.952q0.936 0 1.89 0.612t1.314 1.512l8.568 19.872q0.9 2.196 0.9 4.428v17.352q0 0.936 -0.684 1.62t-1.62 0.684h-50.688q-0.936 0 -1.62 -0.684t-0.684 -1.62zm7.092 -18.432h11.376l3.42 6.912h11.52l3.42 -6.912h11.376l-0.09 -0.288 -0.09 -0.288 -7.632 -17.856h-25.488l-7.632 17.856 -0.09 0.288 -0.09 0.288z","PLAY_CIRCLE_O":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm8.064 0q0 5.328 2.628 9.828t7.128 7.128 9.828 2.628 9.828 -2.628 7.128 -7.128 2.628 -9.828 -2.628 -9.828 -7.128 -7.128 -9.828 -2.628 -9.828 2.628 -7.128 7.128 -2.628 9.828zm10.368 11.52v-23.04q0 -1.332 1.152 -2.016 1.188 -0.648 2.304 0.036l19.584 11.52q1.152 0.648 1.152 1.98t-1.152 1.98l-19.584 11.52q-0.54 0.324 -1.152 0.324 -0.576 0 -1.152 -0.288 -1.152 -0.684 -1.152 -2.016z","ROTATE_RIGHT":"M0 -32.183q0 -5.616 2.196 -10.728t5.904 -8.82 8.82 -5.904 10.728 -2.196q5.292 0 10.242 1.998t8.802 5.634l4.68 -4.644q1.044 -1.116 2.52 -0.504 1.404 0.612 1.404 2.124v16.128q0 0.936 -0.684 1.62t-1.62 0.684h-16.128q-1.512 0 -2.124 -1.44 -0.612 -1.404 0.504 -2.484l4.968 -4.968q-5.328 -4.932 -12.564 -4.932 -3.744 0 -7.146 1.458t-5.886 3.942 -3.942 5.886 -1.458 7.146 1.458 7.146 3.942 5.886 5.886 3.942 7.146 1.458q4.284 0 8.1 -1.872t6.444 -5.292q0.252 -0.36 0.828 -0.432 0.504 0 0.9 0.324l4.932 4.968q0.324 0.288 0.342 0.738t-0.27 0.81q-3.924 4.752 -9.504 7.362t-11.772 2.61q-5.616 0 -10.728 -2.196t-8.82 -5.904 -5.904 -8.82 -2.196 -10.728z","REFRESH":"M0 -9.143v-16.128q0 -0.936 0.684 -1.62t1.62 -0.684h16.128q0.936 0 1.62 0.684t0.684 1.62 -0.684 1.62l-4.932 4.932q2.556 2.376 5.796 3.672t6.732 1.296q4.824 0 9 -2.34t6.696 -6.444q0.396 -0.612 1.908 -4.212 0.288 -0.828 1.08 -0.828h6.912q0.468 0 0.81 0.342t0.342 0.81q0 0.18 -0.036 0.252 -2.304 9.648 -9.648 15.642t-17.208 5.994q-5.256 0 -10.17 -1.98t-8.766 -5.652l-4.644 4.644q-0.684 0.684 -1.62 0.684t-1.62 -0.684 -0.684 -1.62zm0.648 -28.8v-0.252q2.34 -9.648 9.72 -15.642t17.28 -5.994q5.256 0 10.224 1.998t8.82 5.634l4.68 -4.644q0.684 -0.684 1.62 -0.684t1.62 0.684 0.684 1.62v16.128q0 0.936 -0.684 1.62t-1.62 0.684h-16.128q-0.936 0 -1.62 -0.684t-0.684 -1.62 0.684 -1.62l4.968 -4.968q-5.328 -4.932 -12.564 -4.932 -4.824 0 -9 2.34t-6.696 6.444q-0.396 0.612 -1.908 4.212 -0.288 0.828 -1.08 0.828h-7.164q-0.468 0 -0.81 -0.342t-0.342 -0.81z","LIST_ALT":"M0 -14.903v-39.168q0 -2.376 1.692 -4.068t4.068 -1.692h52.992q2.376 0 4.068 1.692t1.692 4.068v39.168q0 2.376 -1.692 4.068t-4.068 1.692h-52.992q-2.376 0 -4.068 -1.692t-1.692 -4.068zm4.608 0q0 0.468 0.342 0.81t0.81 0.342h52.992q0.468 0 0.81 -0.342t0.342 -0.81v-29.952q0 -0.468 -0.342 -0.81t-0.81 -0.342h-52.992q-0.468 0 -0.81 0.342t-0.342 0.81v29.952zm4.608 -4.608v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm9.216 18.432v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h34.56q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-34.56q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h34.56q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-34.56q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h34.56q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-34.56q-0.468 0 -0.81 -0.342t-0.342 -0.81z","LOCK":"M0 -12.599v-20.736q0 -1.44 1.008 -2.448t2.448 -1.008h1.152v-6.912q0 -6.624 4.752 -11.376t11.376 -4.752 11.376 4.752 4.752 11.376v6.912h1.152q1.44 0 2.448 1.008t1.008 2.448v20.736q0 1.44 -1.008 2.448t-2.448 1.008h-34.56q-1.44 0 -2.448 -1.008t-1.008 -2.448zm11.52 -24.192h18.432v-6.912q0 -3.816 -2.7 -6.516t-6.516 -2.7 -6.516 2.7 -2.7 6.516v6.912z","FLAG":"M2.304 -55.223q0 -1.908 1.35 -3.258t3.258 -1.35 3.258 1.35 1.35 3.258q0 2.592 -2.304 3.96v45.576q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81v-45.576q-2.304 -1.368 -2.304 -3.96zm9.216 34.56v-26.712q0 -1.152 1.116 -1.98 0.756 -0.504 2.844 -1.548 8.496 -4.32 15.156 -4.32 3.852 0 7.2 1.044t7.884 3.168q1.368 0.684 3.168 0.684 1.944 0 4.23 -0.756t3.96 -1.692 3.168 -1.692 1.962 -0.756q0.936 0 1.62 0.684t0.684 1.62v27.468q0 0.9 -0.45 1.386t-1.422 0.99q-7.74 4.176 -13.284 4.176 -2.196 0 -4.446 -0.792t-3.906 -1.728 -4.158 -1.728 -5.13 -0.792q-6.912 0 -16.704 5.256 -0.612 0.324 -1.188 0.324 -0.936 0 -1.62 -0.684t-0.684 -1.62z","HEADPHONES":"M0 -32.543q0 -5.436 2.412 -10.476t6.444 -8.73 9.576 -5.886 11.52 -2.196 11.52 2.196 9.576 5.886 6.444 8.73 2.412 10.476q0 5.976 -2.16 11.304l-0.72 1.764 -6.66 1.188q-0.792 2.988 -3.258 4.914t-5.634 1.926v1.152q0 0.504 -0.324 0.828t-0.828 0.324h-2.304q-0.504 0 -0.828 -0.324t-0.324 -0.828v-20.736q0 -0.504 0.324 -0.828t0.828 -0.324h2.304q0.504 0 0.828 0.324t0.324 0.828v1.152q2.556 0 4.68 1.278t3.348 3.438l2.448 -0.432q1.044 -3.42 1.044 -6.948 0 -5.328 -3.168 -10.044t-8.514 -7.524 -11.358 -2.808 -11.358 2.808 -8.514 7.524 -3.168 10.044q0 3.528 1.044 6.948l2.448 0.432q1.224 -2.16 3.348 -3.438t4.68 -1.278v-1.152q0 -0.504 0.324 -0.828t0.828 -0.324h2.304q0.504 0 0.828 0.324t0.324 0.828v20.736q0 0.504 -0.324 0.828t-0.828 0.324h-2.304q-0.504 0 -0.828 -0.324t-0.324 -0.828v-1.152q-3.168 0 -5.634 -1.926t-3.258 -4.914l-6.66 -1.188 -0.72 -1.764q-2.16 -5.328 -2.16 -11.304z","VOLUME_OFF":"M0 -25.271v-13.824q0 -0.936 0.684 -1.62t1.62 -0.684h9.432l11.988 -11.988q0.684 -0.684 1.62 -0.684t1.62 0.684 0.684 1.62v39.168q0 0.936 -0.684 1.62t-1.62 0.684 -1.62 -0.684l-11.988 -11.988h-9.432q-0.936 0 -1.62 -0.684t-0.684 -1.62z","VOLUME_DOWN":"M0 -25.271v-13.824q0 -0.936 0.684 -1.62t1.62 -0.684h9.432l11.988 -11.988q0.684 -0.684 1.62 -0.684t1.62 0.684 0.684 1.62v39.168q0 0.936 -0.684 1.62t-1.62 0.684 -1.62 -0.684l-11.988 -11.988h-9.432q-0.936 0 -1.62 -0.684t-0.684 -1.62zm32.688 -0.576q0 -0.756 0.432 -1.278t1.044 -0.9 1.224 -0.828 1.044 -1.278 0.432 -2.052 -0.432 -2.052 -1.044 -1.278 -1.224 -0.828 -1.044 -0.9 -0.432 -1.278q0 -0.972 0.684 -1.638t1.62 -0.666q0.54 0 0.9 0.18 2.52 0.972 4.05 3.348t1.53 5.112 -1.53 5.094 -4.05 3.366q-0.36 0.18 -0.9 0.18 -0.936 0 -1.62 -0.666t-0.684 -1.638z","VOLUME_UP":"M0 -25.271v-13.824q0 -0.936 0.684 -1.62t1.62 -0.684h9.432l11.988 -11.988q0.684 -0.684 1.62 -0.684t1.62 0.684 0.684 1.62v39.168q0 0.936 -0.684 1.62t-1.62 0.684 -1.62 -0.684l-11.988 -11.988h-9.432q-0.936 0 -1.62 -0.684t-0.684 -1.62zm32.688 -0.576q0 -0.756 0.432 -1.278t1.044 -0.9 1.224 -0.828 1.044 -1.278 0.432 -2.052 -0.432 -2.052 -1.044 -1.278 -1.224 -0.828 -1.044 -0.9 -0.432 -1.278q0 -0.972 0.684 -1.638t1.62 -0.666q0.54 0 0.9 0.18 2.52 0.972 4.05 3.348t1.53 5.112 -1.53 5.094 -4.05 3.366q-0.36 0.18 -0.9 0.18 -0.936 0 -1.62 -0.666t-0.684 -1.638zm3.6 8.496q0 -1.404 1.404 -2.124 2.016 -1.044 2.736 -1.584 2.664 -1.944 4.158 -4.878t1.494 -6.246 -1.494 -6.246 -4.158 -4.878q-0.72 -0.54 -2.736 -1.584 -1.404 -0.72 -1.404 -2.124 0 -0.936 0.684 -1.62t1.62 -0.684q0.468 0 0.936 0.18 5.04 2.124 8.1 6.786t3.06 10.17 -3.06 10.17 -8.1 6.786q-0.468 0.18 -0.9 0.18 -0.972 0 -1.656 -0.684t-0.684 -1.62zm3.636 8.46q0 -1.296 1.404 -2.124 0.252 -0.144 0.81 -0.378t0.81 -0.378q1.656 -0.9 2.952 -1.836 4.428 -3.276 6.912 -8.172t2.484 -10.404 -2.484 -10.404 -6.912 -8.172q-1.296 -0.936 -2.952 -1.836 -0.252 -0.144 -0.81 -0.378t-0.81 -0.378q-1.404 -0.828 -1.404 -2.124 0 -0.936 0.684 -1.62t1.62 -0.684q0.468 0 0.936 0.18 7.596 3.276 12.168 10.206t4.572 15.21 -4.572 15.21 -12.168 10.206q-0.468 0.18 -0.936 0.18 -0.936 0 -1.62 -0.684t-0.684 -1.62z","QRCODE":"M0 -9.143v-23.04h23.04v23.04h-23.04zm0 -27.648v-23.04h23.04v23.04h-23.04zm4.608 23.004h13.824v-13.788h-13.824v13.788zm0 -27.612h13.824v-13.824h-13.824v13.824zm4.608 23.04v-4.608h4.608v4.608h-4.608zm0 -27.648v-4.608h4.608v4.608h-4.608zm18.432 36.864v-23.04h13.824v4.608h4.608v-4.608h4.608v13.824h-13.824v-4.608h-4.608v13.824h-4.608zm0 -27.648v-23.04h23.04v23.04h-23.04zm4.608 -4.608h13.824v-13.824h-13.824v13.824zm4.608 32.256v-4.608h4.608v4.608h-4.608zm0 -36.864v-4.608h4.608v4.608h-4.608zm9.216 36.864v-4.608h4.608v4.608h-4.608z","BARCODE":"M0 -9.143v-50.688h2.268v50.688h-2.268zm3.384 -0.036v-50.652h1.152v50.652h-1.152zm3.42 0v-50.652h1.116v50.652h-1.116zm5.652 0v-50.652h1.116v50.652h-1.116zm4.536 0v-50.652h2.232v50.652h-2.232zm5.652 0v-50.652h1.116v50.652h-1.116zm2.268 0v-50.652h1.116v50.652h-1.116zm2.268 0v-50.652h1.116v50.652h-1.116zm4.5 0v-50.652h2.268v50.652h-2.268zm5.652 0v-50.652h2.268v50.652h-2.268zm4.536 0v-50.652h2.268v50.652h-2.268zm4.536 0v-50.652h2.268v50.652h-2.268zm3.384 0v-50.652h2.268v50.652h-2.268zm5.688 0v-50.652h3.384v50.652h-3.384zm4.5 0v-50.652h1.152v50.652h-1.152zm2.268 0.036v-50.688h2.268v50.688h-2.268z","TAG":"M0 -40.247v-14.976q0 -1.872 1.368 -3.24t3.24 -1.368h14.976q1.908 0 4.212 0.954t3.672 2.322l25.74 25.704q1.332 1.404 1.332 3.276 0 1.908 -1.332 3.24l-17.676 17.712q-1.404 1.332 -3.276 1.332 -1.908 0 -3.24 -1.332l-25.74 -25.776q-1.368 -1.332 -2.322 -3.636t-0.954 -4.212zm6.912 -8.064q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258 -1.35 -3.258 -3.258 -1.35 -3.258 1.35 -1.35 3.258z","TAGS":"M0 -40.247v-14.976q0 -1.872 1.368 -3.24t3.24 -1.368h14.976q1.908 0 4.212 0.954t3.672 2.322l25.74 25.704q1.332 1.404 1.332 3.276 0 1.908 -1.332 3.24l-17.676 17.712q-1.404 1.332 -3.276 1.332 -1.908 0 -3.24 -1.332l-25.74 -25.776q-1.368 -1.332 -2.322 -3.636t-0.954 -4.212zm6.912 -8.064q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258 -1.35 -3.258 -3.258 -1.35 -3.258 1.35 -1.35 3.258zm18.432 -11.52h8.064q1.908 0 4.212 0.954t3.672 2.322l25.74 25.704q1.332 1.404 1.332 3.276 0 1.908 -1.332 3.24l-17.676 17.712q-1.404 1.332 -3.276 1.332 -1.296 0 -2.124 -0.504t-1.908 -1.62l16.92 -16.92q1.332 -1.332 1.332 -3.24 0 -1.872 -1.332 -3.276l-25.74 -25.704q-1.368 -1.368 -3.672 -2.322t-4.212 -0.954z","BOOK":"M0.36 -15.767q0 -0.144 0.108 -0.972t0.144 -1.332q0.036 -0.288 -0.108 -0.774t-0.108 -0.702q0.072 -0.396 0.288 -0.756t0.594 -0.846 0.594 -0.846q0.828 -1.368 1.62 -3.294t1.08 -3.294q0.108 -0.36 0.018 -1.08t-0.018 -1.008q0.108 -0.396 0.612 -1.008t0.612 -0.828q0.756 -1.296 1.512 -3.312t0.9 -3.24q0.036 -0.324 -0.09 -1.152t0.018 -1.008q0.144 -0.468 0.792 -1.098t0.792 -0.81q0.684 -0.936 1.53 -3.042t0.99 -3.474q0.036 -0.288 -0.108 -0.918t-0.072 -0.954q0.072 -0.288 0.324 -0.648t0.648 -0.828 0.612 -0.756q0.288 -0.432 0.594 -1.098t0.54 -1.26 0.576 -1.296 0.702 -1.152 0.954 -0.846 1.296 -0.414 1.71 0.198l-0.036 0.108q1.368 -0.324 1.836 -0.324h27.396q2.664 0 4.104 2.016t0.648 4.68l-9.864 32.616q-1.296 4.284 -2.574 5.526t-4.626 1.242h-31.284q-0.972 0 -1.368 0.54 -0.396 0.576 -0.036 1.548 0.864 2.52 5.184 2.52h33.228q1.044 0 2.016 -0.558t1.26 -1.494l10.8 -35.532q0.252 -0.792 0.18 -2.052 1.368 0.54 2.124 1.548 1.44 2.052 0.648 4.644l-9.9 32.616q-0.684 2.304 -2.754 3.87t-4.41 1.566h-33.228q-2.772 0 -5.346 -1.926t-3.582 -4.734q-0.864 -2.412 -0.072 -4.572zm17.352 -22.176q-0.144 0.468 0.072 0.81t0.72 0.342h21.888q0.468 0 0.918 -0.342t0.594 -0.81l0.756 -2.304q0.144 -0.468 -0.072 -0.81t-0.72 -0.342h-21.888q-0.468 0 -0.918 0.342t-0.594 0.81zm2.988 -9.216q-0.144 0.468 0.072 0.81t0.72 0.342h21.888q0.468 0 0.918 -0.342t0.594 -0.81l0.756 -2.304q0.144 -0.468 -0.072 -0.81t-0.72 -0.342h-21.888q-0.468 0 -0.918 0.342t-0.594 0.81z","BOOKMARK":"M0 -9.395v-46.404q0 -1.224 0.702 -2.232t1.89 -1.476q0.756 -0.324 1.584 -0.324h37.728q0.828 0 1.584 0.324 1.188 0.468 1.89 1.476t0.702 2.232v46.404q0 1.224 -0.702 2.232t-1.89 1.476q-0.684 0.288 -1.584 0.288 -1.728 0 -2.988 -1.152l-15.876 -15.264 -15.876 15.264q-1.296 1.188 -2.988 1.188 -0.828 0 -1.584 -0.324 -1.188 -0.468 -1.89 -1.476t-0.702 -2.232z","PRINT":"M0 -14.903v-14.976q0 -2.844 2.034 -4.878t4.878 -2.034h2.304v-19.584q0 -1.44 1.008 -2.448t2.448 -1.008h24.192q1.44 0 3.168 0.72t2.736 1.728l5.472 5.472q1.008 1.008 1.728 2.736t0.72 3.168v9.216h2.304q2.844 0 4.878 2.034t2.034 4.878v14.976q0 0.468 -0.342 0.81t-0.81 0.342h-8.064v5.76q0 1.44 -1.008 2.448t-2.448 1.008h-34.56q-1.44 0 -2.448 -1.008t-1.008 -2.448v-5.76h-8.064q-0.468 0 -0.81 -0.342t-0.342 -0.81zm13.824 5.76h32.256v-9.216h-32.256v9.216zm0 -23.04h32.256v-13.824h-5.76q-1.44 0 -2.448 -1.008t-1.008 -2.448v-5.76h-23.04v23.04zm36.864 2.304q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62z","CAMERA":"M0 -13.751v-32.256q0 -3.816 2.7 -6.516t6.516 -2.7h8.064l1.836 -4.896q0.684 -1.764 2.502 -3.042t3.726 -1.278h18.432q1.908 0 3.726 1.278t2.502 3.042l1.836 4.896h8.064q3.816 0 6.516 2.7t2.7 6.516v32.256q0 3.816 -2.7 6.516t-6.516 2.7h-50.688q-3.816 0 -6.516 -2.7t-2.7 -6.516zm18.432 -16.128q0 6.66 4.734 11.394t11.394 4.734 11.394 -4.734 4.734 -11.394 -4.734 -11.394 -11.394 -4.734 -11.394 4.734 -4.734 11.394zm5.76 0q0 -4.284 3.042 -7.326t7.326 -3.042 7.326 3.042 3.042 7.326 -3.042 7.326 -7.326 3.042 -7.326 -3.042 -3.042 -7.326z","FONT":"M0 -4.535l0.072 -2.844q0.828 -0.252 2.016 -0.45t2.052 -0.378 1.782 -0.522 1.602 -1.044 1.116 -1.818l8.532 -22.176 10.08 -26.064h4.6080000000000005q0.288 0.504 0.396 0.756l7.38 17.28q1.188 2.808 3.816 9.27t4.104 9.882q0.54 1.224 2.088 5.202t2.592 6.066q0.72 1.62 1.26 2.052 0.684 0.54 3.168 1.062t3.024 0.738q0.216 1.368 0.216 2.052l-0.018 0.468 -0.018 0.468q-2.268 0 -6.84 -0.288t-6.876 -0.288q-2.736 0 -7.74 0.252t-6.408 0.288q0 -1.548 0.144 -2.808l4.716 -1.008q0.036 0 0.45 -0.09t0.558 -0.126 0.522 -0.162 0.54 -0.234 0.396 -0.288 0.324 -0.396 0.09 -0.504q0 -0.576 -1.116 -3.474t-2.592 -6.39 -1.512 -3.6l-16.2 -0.072q-0.936 2.088 -2.754 7.038t-1.818 5.85q0 0.792 0.504 1.35t1.566 0.882 1.746 0.486 2.052 0.306 1.476 0.144q0.036 0.684 0.036 2.088 0 0.324 -0.072 0.972 -2.088 0 -6.282 -0.36t-6.282 -0.36q-0.288 0 -0.954 0.144t-0.774 0.144q-2.88 0.504 -6.768 0.504zm19.98 -23.58q1.188 0 4.914 0.072t5.778 0.072q0.684 0 2.052 -0.072 -3.132 -9.108 -6.624 -16.272z","BOLD":"M0 -4.535l0.072 -3.384q0.54 -0.144 3.06 -0.576t3.816 -0.972q0.252 -0.432 0.45 -0.972t0.306 -1.206 0.198 -1.17 0.108 -1.35 0.018 -1.224v-2.358q0 -35.352 -0.792 -36.9 -0.144 -0.288 -0.792 -0.522t-1.602 -0.396 -1.782 -0.252 -1.746 -0.162 -1.098 -0.108l-0.144 -2.988q3.528 -0.072 12.24 -0.414t13.428 -0.342q0.828 0 2.466 0.018t2.43 0.018q2.52 0 4.914 0.468t4.626 1.512 3.888 2.556 2.664 3.762 1.008 4.95q0 1.872 -0.594 3.438t-1.404 2.592 -2.322 2.07 -2.628 1.62 -3.024 1.44q5.544 1.26 9.234 4.824t3.69 8.928q0 3.6 -1.26 6.462t-3.366 4.698 -4.968 3.078 -5.886 1.746 -6.336 0.504q-1.584 0 -4.752 -0.108t-4.752 -0.108q-3.816 0 -11.052 0.396t-8.316 0.432zm19.188 -51.12q0 1.8 0.144 5.436t0.144 5.472q0 0.972 -0.018 2.88t-0.018 2.844q0 1.656 0.036 2.484 1.512 0.252 3.924 0.252 2.952 0 5.148 -0.468t3.96 -1.602 2.682 -3.222 0.918 -5.112q0 -2.52 -1.044 -4.41t-2.844 -2.952 -3.888 -1.566 -4.464 -0.504q-1.8 0 -4.68 0.468zm0.198 40.572q0.018 1.332 0.162 3.006t0.432 2.394q2.664 1.152 5.04 1.152 13.536 0 13.536 -12.06 0 -4.104 -1.476 -6.48 -0.972 -1.584 -2.214 -2.664t-2.43 -1.674 -2.898 -0.9 -3.024 -0.378 -3.402 -0.072q-2.628 0 -3.636 0.36 0 1.908 -0.018 5.724t-0.018 5.688q0 0.288 -0.036 2.43t-0.018 3.474z","ITALIC":"M0 -4.607l0.612 -3.06q0.216 -0.072 2.934 -0.774t4.014 -1.35q1.008 -1.26 1.476 -3.636 0.036 -0.252 2.232 -10.404t4.104 -19.566 1.872 -10.674v-0.9q-0.864 -0.468 -1.962 -0.666t-2.502 -0.288 -2.088 -0.198l0.684 -3.708q1.188 0.072 4.32 0.234t5.382 0.252 4.338 0.09q1.728 0 3.546 -0.09t4.356 -0.252 3.546 -0.234q-0.18 1.404 -0.684 3.204 -1.08 0.36 -3.654 1.026t-3.906 1.206q-0.288 0.684 -0.504 1.53t-0.324 1.44 -0.27 1.638 -0.234 1.512q-0.972 5.328 -3.15 15.102t-2.79 12.798q-0.072 0.324 -0.468 2.088t-0.72 3.24 -0.576 3.006 -0.216 2.07l0.036 0.648q0.612 0.144 6.66 1.116 -0.108 1.584 -0.576 3.564 -0.396 0 -1.17 0.054t-1.17 0.054q-1.044 0 -3.132 -0.36t-3.096 -0.36q-4.968 -0.072 -7.416 -0.072 -1.836 0 -5.148 0.324t-4.356 0.396z","TEXT_HEIGHT":"M0 -45.971v-13.788l2.916 -0.036 1.944 0.972q0.432 0.18 7.596 0.18 1.584 0 4.752 -0.072t4.752 -0.072q1.296 0 3.87 0.018t3.87 0.018h10.548q0.216 0 0.756 0.018t0.738 0 0.576 -0.108 0.63 -0.324 0.54 -0.63l1.512 -0.036 0.504 0.018 0.504 0.018q0.072 4.032 0.072 12.096 0 2.88 -0.18 3.924 -1.404 0.504 -2.448 0.648 -0.9 -1.584 -1.944 -4.608 -0.108 -0.324 -0.396 -1.728t-0.522 -2.646 -0.27 -1.278q-0.216 -0.288 -0.432 -0.45t-0.558 -0.216 -0.468 -0.09 -0.648 -0.018 -0.594 0.018q-0.612 0 -2.394 -0.018t-2.682 -0.018 -2.304 0.072 -2.556 0.216q-0.324 2.916 -0.288 4.896 0 3.384 0.072 13.968t0.072 16.38q0 0.576 -0.09 2.574t0 3.294 0.45 2.484q1.44 0.756 4.464 1.53t4.32 1.35q0.18 1.44 0.18 1.8 0 0.504 -0.108 1.044l-1.224 0.036q-2.736 0.072 -7.848 -0.288t-7.452 -0.36q-1.8 0 -5.436 0.324t-5.472 0.324l-0.108 -1.872v-0.324q0.612 -0.972 2.214 -1.548t3.546 -1.044 2.808 -0.972q0.684 -1.512 0.684 -13.788 0 -3.636 -0.108 -10.908t-0.108 -10.908v-4.212q0 -0.072 0.018 -0.558t0.018 -0.9 -0.036 -0.918 -0.108 -0.864 -0.18 -0.504q-0.396 -0.432 -5.832 -0.432 -1.188 0 -3.348 0.432t-2.88 0.936q-0.684 0.468 -1.224 2.61t-1.134 3.996 -1.53 1.926q-1.512 -0.936 -2.016 -1.584zm50.904 32.886q0.324 -0.666 1.512 -0.666h2.88v-36.864h-2.88q-1.188 0 -1.512 -0.666t0.396 -1.602l4.536 -5.832q0.72 -0.936 1.764 -0.936t1.764 0.936l4.536 5.832q0.72 0.936 0.396 1.602t-1.512 0.666h-2.88v36.864h2.88q1.188 0 1.512 0.666t-0.396 1.602l-4.536 5.832q-0.72 0.936 -1.764 0.936t-1.764 -0.936l-4.536 -5.832q-0.72 -0.936 -0.396 -1.602z","TEXT_WIDTH":"M0 -45.971v-13.788l2.916 -0.036 1.944 0.972q0.432 0.18 7.596 0.18 1.584 0 4.752 -0.072t4.752 -0.072q2.52 0 8.874 -0.036t10.962 -0.018 8.892 0.162q1.188 0.036 2.016 -1.116l1.512 -0.036 0.504 0.018 0.504 0.018q0.072 4.032 0.072 12.096 0 2.88 -0.18 3.924 -1.404 0.504 -2.448 0.648 -0.9 -1.584 -1.944 -4.608 -0.108 -0.324 -0.396 -1.71t-0.54 -2.646 -0.252 -1.296q-0.36 -0.468 -0.972 -0.684 -0.18 -0.072 -2.376 -0.072 -1.08 0 -3.348 -0.036t-3.708 -0.036 -3.384 0.072 -3.456 0.252q-0.324 2.916 -0.288 4.896l0.036 5.472v-1.872q0 1.98 0.036 5.544t0.054 6.48 0.018 5.508q0 0.576 -0.09 2.574t0 3.294 0.45 2.484q1.44 0.756 4.464 1.53t4.32 1.35q0.18 1.44 0.18 1.8 0 0.504 -0.108 1.044l-1.224 0.036q-2.736 0.072 -7.848 -0.288t-7.452 -0.36q-1.8 0 -5.436 0.324t-5.472 0.324l-0.108 -1.872v-0.324q0.612 -0.972 2.214 -1.548t3.546 -1.044 2.808 -0.972q0.252 -0.576 0.414 -2.664t0.216 -5.238 0.054 -5.58 -0.018 -5.526 -0.018 -3.204q0 -0.252 -0.09 -0.774t-0.09 -0.81q0 -0.252 0.018 -1.584t0.036 -2.628 0 -2.754 -0.108 -2.43 -0.234 -1.152q-0.396 -0.432 -5.832 -0.432 -1.476 0 -5.868 0.486t-4.968 0.882q-0.684 0.432 -1.224 2.574t-1.134 4.014 -1.53 1.944q-1.512 -0.936 -2.016 -1.584zm0.18 39.132q0 -1.008 0.936 -1.764 0.144 -0.108 1.296 -1.08t2.142 -1.764 2.07 -1.494 1.512 -0.702q0.468 0 0.738 0.378t0.36 1.026 0.09 1.206 -0.054 1.188 -0.054 0.702h36.864q0 -0.072 -0.054 -0.702t-0.054 -1.188 0.09 -1.206 0.36 -1.026 0.738 -0.378q0.432 0 1.512 0.702t2.07 1.494 2.142 1.764 1.296 1.08q0.936 0.756 0.936 1.764t-0.936 1.764q-0.144 0.108 -1.296 1.08t-2.142 1.764 -2.07 1.494 -1.512 0.702q-0.468 0 -0.738 -0.378t-0.36 -1.026 -0.09 -1.206 0.054 -1.188 0.054 -0.702h-36.864q0 0.072 0.054 0.702t0.054 1.188 -0.09 1.206 -0.36 1.026 -0.738 0.378q-0.432 0 -1.512 -0.702t-2.07 -1.494 -2.142 -1.764 -1.296 -1.08q-0.936 -0.756 -0.936 -1.764z","ALIGN_LEFT":"M0 -11.447v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h59.904q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-59.904q-0.936 0 -1.62 -0.684t-0.684 -1.62zm0 -13.824v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h46.08q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-46.08q-0.936 0 -1.62 -0.684t-0.684 -1.62zm0 -13.824v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h55.296q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-55.296q-0.936 0 -1.62 -0.684t-0.684 -1.62zm0 -13.824v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h41.472q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-41.472q-0.936 0 -1.62 -0.684t-0.684 -1.62z","ALIGN_CENTER":"M0 -11.447v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h59.904q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-59.904q-0.936 0 -1.62 -0.684t-0.684 -1.62zm4.608 -27.648v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h50.688q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-50.688q-0.936 0 -1.62 -0.684t-0.684 -1.62zm9.216 13.824v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h32.256q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-32.256q-0.936 0 -1.62 -0.684t-0.684 -1.62zm4.608 -27.648v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h23.04q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-23.04q-0.936 0 -1.62 -0.684t-0.684 -1.62z","ALIGN_RIGHT":"M0 -11.447v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h59.904q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-59.904q-0.936 0 -1.62 -0.684t-0.684 -1.62zm4.608 -27.648v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h55.296q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-55.296q-0.936 0 -1.62 -0.684t-0.684 -1.62zm9.216 13.824v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h46.08q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-46.08q-0.936 0 -1.62 -0.684t-0.684 -1.62zm4.608 -27.648v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h41.472q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-41.472q-0.936 0 -1.62 -0.684t-0.684 -1.62z","ALIGN_JUSTIFY":"M0 -11.447v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h59.904q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-59.904q-0.936 0 -1.62 -0.684t-0.684 -1.62zm0 -13.824v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h59.904q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-59.904q-0.936 0 -1.62 -0.684t-0.684 -1.62zm0 -13.824v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h59.904q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-59.904q-0.936 0 -1.62 -0.684t-0.684 -1.62zm0 -13.824v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h59.904q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-59.904q-0.936 0 -1.62 -0.684t-0.684 -1.62z","LIST":"M0 -10.295v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h6.912q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-6.912q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -13.824v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h6.912q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-6.912q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -13.824v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h6.912q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-6.912q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -13.824v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h6.912q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-6.912q-0.468 0 -0.81 -0.342t-0.342 -0.81zm13.824 41.472v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h48.384q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-48.384q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -13.824v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h48.384q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-48.384q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -13.824v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h48.384q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-48.384q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -13.824v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h48.384q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-48.384q-0.468 0 -0.81 -0.342t-0.342 -0.81z","OUTDENT":"M0 -10.295v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h62.208q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-62.208q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -41.472v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h62.208q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-62.208q-0.468 0 -0.81 -0.342t-0.342 -0.81zm1.152 17.28q0 -0.504 0.324 -0.828l10.368 -10.368q0.324 -0.324 0.828 -0.324 0.468 0 0.81 0.342t0.342 0.81v20.736q0 0.468 -0.342 0.81t-0.81 0.342q-0.504 0 -0.828 -0.324l-10.368 -10.368q-0.324 -0.324 -0.324 -0.828zm21.888 10.368v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h39.168q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-39.168q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -13.824v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h39.168q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-39.168q-0.468 0 -0.81 -0.342t-0.342 -0.81z","INDENT":"M0 -10.295v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h62.208q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-62.208q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -13.824v-20.736q0 -0.468 0.342 -0.81t0.81 -0.342q0.504 0 0.828 0.324l10.368 10.368q0.324 0.324 0.324 0.828t-0.324 0.828l-10.368 10.368q-0.324 0.324 -0.828 0.324 -0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -27.648v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h62.208q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-62.208q-0.468 0 -0.81 -0.342t-0.342 -0.81zm23.04 27.648v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h39.168q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-39.168q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -13.824v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h39.168q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-39.168q-0.468 0 -0.81 -0.342t-0.342 -0.81z","VIDEO_CAMERA":"M0 -19.511v-25.344q0 -4.284 3.042 -7.326t7.326 -3.042h25.344q4.284 0 7.326 3.042t3.042 7.326v5.94l14.508 -14.472q0.648 -0.684 1.62 -0.684 0.432 0 0.9 0.18 1.404 0.612 1.404 2.124v39.168q0 1.512 -1.404 2.124 -0.468 0.18 -0.9 0.18 -0.972 0 -1.62 -0.684l-14.508 -14.508v5.976q0 4.284 -3.042 7.326t-7.326 3.042h-25.344q-4.284 0 -7.326 -3.042t-3.042 -7.326z","PICTURE_O":"M0 -10.295v-43.776q0 -2.376 1.692 -4.068t4.068 -1.692h57.6q2.376 0 4.068 1.692t1.692 4.068v43.776q0 2.376 -1.692 4.068t-4.068 1.692h-57.6q-2.376 0 -4.068 -1.692t-1.692 -4.068zm4.608 0q0 0.468 0.342 0.81t0.81 0.342h57.6q0.468 0 0.81 -0.342t0.342 -0.81v-43.776q0 -0.468 -0.342 -0.81t-0.81 -0.342h-57.6q-0.468 0 -0.81 0.342t-0.342 0.81v43.776zm4.608 -3.456v-6.912l11.52 -11.52 5.76 5.76 18.432 -18.432 14.976 14.976v16.128h-50.688zm0 -29.952q0 -2.88 2.016 -4.896t4.896 -2.016 4.896 2.016 2.016 4.896 -2.016 4.896 -4.896 2.016 -4.896 -2.016 -2.016 -4.896z","PENCIL":"M0 -4.535v-14.976l29.952 -29.952 14.976 14.976 -29.952 29.952h-14.976zm4.608 -9.216h4.608v4.608h3.852l3.276 -3.276 -8.46 -8.46 -3.276 3.276v3.852zm6.12 -9.216q0 0.792 0.792 0.792 0.36 0 0.612 -0.252l19.512 -19.512q0.252 -0.252 0.252 -0.612 0 -0.792 -0.792 -0.792 -0.36 0 -0.612 0.252l-19.512 19.512q-0.252 0.252 -0.252 0.612zm21.528 -28.8l5.976 -5.94q1.296 -1.368 3.24 -1.368 1.908 0 3.276 1.368l8.46 8.424q1.332 1.404 1.332 3.276 0 1.908 -1.332 3.24l-5.976 5.976z","MAP_MARKER":"M0 -41.399q0 -7.632 5.4 -13.032t13.032 -5.4 13.032 5.4 5.4 13.032q0 3.924 -1.188 6.444l-13.104 27.864q-0.576 1.188 -1.71 1.872t-2.43 0.684 -2.43 -0.684 -1.674 -1.872l-13.14 -27.864q-1.188 -2.52 -1.188 -6.444zm9.216 0q0 3.816 2.7 6.516t6.516 2.7 6.516 -2.7 2.7 -6.516 -2.7 -6.516 -6.516 -2.7 -6.516 2.7 -2.7 6.516z","ADJUST":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm8.064 0q0 5.328 2.628 9.828t7.128 7.128 9.828 2.628v-39.168q-5.328 0 -9.828 2.628t-7.128 7.128 -2.628 9.828z","TINT":"M0 -27.575q0 -5.22 2.916 -9.9 0.216 -0.324 2.25 -3.258t3.636 -5.436 3.582 -6.408 2.988 -7.254q0.324 -1.08 1.224 -1.692t1.836 -0.612 1.854 0.612 1.206 1.692q1.008 3.348 2.988 7.254t3.582 6.408 3.636 5.436 2.25 3.258q2.916 4.572 2.916 9.9 0 7.632 -5.4 13.032t-13.032 5.4 -13.032 -5.4 -5.4 -13.032zm9.216 4.608q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258q0 -1.296 -0.72 -2.484 -0.036 -0.036 -0.558 -0.81t-0.918 -1.368 -0.9 -1.584 -0.756 -1.818q-0.144 -0.576 -0.756 -0.576t-0.756 0.576q-0.252 0.828 -0.756 1.818t-0.9 1.584 -0.918 1.368 -0.558 0.81q-0.72 1.188 -0.72 2.484z","PENCIL_SQUARE_O":"M0 -19.511v-29.952q0 -4.284 3.042 -7.326t7.326 -3.042h29.952q2.268 0 4.212 0.9 0.54 0.252 0.648 0.828 0.108 0.612 -0.324 1.044l-1.764 1.764q-0.504 0.504 -1.152 0.288 -0.828 -0.216 -1.62 -0.216h-29.952q-2.376 0 -4.068 1.692t-1.692 4.068v29.952q0 2.376 1.692 4.068t4.068 1.692h29.952q2.376 0 4.068 -1.692t1.692 -4.068v-4.536q0 -0.468 0.324 -0.792l2.304 -2.304q0.54 -0.54 1.26 -0.252t0.72 1.044v6.84q0 4.284 -3.042 7.326t-7.326 3.042h-29.952q-4.284 0 -7.326 -3.042t-3.042 -7.326zm23.04 1.152v-10.368l24.192 -24.192 10.368 10.368 -24.192 24.192h-10.368zm3.456 -6.912h3.456v3.456h2.016l4.176 -4.176 -5.472 -5.472 -4.176 4.176v2.016zm7.488 -8.64q0.576 0.576 1.188 -0.036l12.6 -12.6q0.612 -0.612 0.036 -1.188t-1.188 0.036l-12.6 12.6q-0.612 0.612 -0.036 1.188zm15.552 -21.312l3.312 -3.312q1.008 -1.008 2.448 -1.008t2.448 1.008l5.472 5.472q1.008 1.008 1.008 2.448t-1.008 2.448l-3.312 3.312z","SHARE_SQUARE_O":"M0 -19.511v-29.952q0 -4.284 3.042 -7.326t7.326 -3.042h9.18q0.468 0 0.81 0.342t0.342 0.81q0 0.972 -0.936 1.152 -2.772 0.936 -4.788 2.16 -0.36 0.144 -0.576 0.144h-4.032q-2.376 0 -4.068 1.692t-1.692 4.068v29.952q0 2.376 1.692 4.068t4.068 1.692h29.952q2.376 0 4.068 -1.692t1.692 -4.068v-7.704q0 -0.684 0.648 -1.044 1.008 -0.468 1.944 -1.332 0.576 -0.576 1.26 -0.288 0.756 0.324 0.756 1.044v9.324q0 4.284 -3.042 7.326t-7.326 3.042h-29.952q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 -14.976q0 -1.764 0.126 -3.276t0.504 -3.24 1.008 -3.168 1.692 -2.934 2.466 -2.664 3.402 -2.214 4.482 -1.746 5.742 -1.098 7.074 -0.396h5.76v-6.912q0 -1.512 1.404 -2.124 0.468 -0.18 0.9 -0.18 0.936 0 1.62 0.684l13.824 13.824q0.684 0.684 0.684 1.62t-0.684 1.62l-13.824 13.824q-0.648 0.684 -1.62 0.684 -0.432 0 -0.9 -0.18 -1.404 -0.612 -1.404 -2.124v-6.912h-5.76q-11.628 0 -15.768 4.716 -4.284 4.932 -2.664 17.028 0.108 0.828 -0.72 1.224 -0.288 0.072 -0.432 0.072 -0.576 0 -0.936 -0.468 -0.36 -0.504 -0.756 -1.116t-1.422 -2.466 -1.782 -3.582 -1.386 -4.104 -0.63 -4.392z","CHECK_SQUARE_O":"M0 -19.511v-29.952q0 -4.284 3.042 -7.326t7.326 -3.042h29.952q2.268 0 4.212 0.9 0.54 0.252 0.648 0.828 0.108 0.612 -0.324 1.044l-1.764 1.764q-0.36 0.36 -0.828 0.36 -0.108 0 -0.324 -0.072 -0.828 -0.216 -1.62 -0.216h-29.952q-2.376 0 -4.068 1.692t-1.692 4.068v29.952q0 2.376 1.692 4.068t4.068 1.692h29.952q2.376 0 4.068 -1.692t1.692 -4.068v-9.144q0 -0.468 0.324 -0.792l2.304 -2.304q0.36 -0.36 0.828 -0.36 0.216 0 0.432 0.108 0.72 0.288 0.72 1.044v11.448q0 4.284 -3.042 7.326t-7.326 3.042h-29.952q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.252 -17.28q0 -1.188 0.864 -2.052l3.96 -3.96q0.864 -0.864 2.052 -0.864t2.052 0.864l9.468 9.468 23.292 -23.292q0.864 -0.864 2.052 -0.864t2.052 0.864l3.96 3.96q0.864 0.864 0.864 2.052t-0.864 2.052l-29.304 29.304q-0.864 0.864 -2.052 0.864t-2.052 -0.864l-15.48 -15.48q-0.864 -0.864 -0.864 -2.052z","ARROWS":"M0 -32.183q0 -0.936 0.684 -1.62l9.216 -9.216q0.684 -0.684 1.62 -0.684t1.62 0.684 0.684 1.62v4.608h13.824v-13.824h-4.608q-0.936 0 -1.62 -0.684t-0.684 -1.62 0.684 -1.62l9.216 -9.216q0.684 -0.684 1.62 -0.684t1.62 0.684l9.216 9.216q0.684 0.684 0.684 1.62t-0.684 1.62 -1.62 0.684h-4.608v13.824h13.824v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684l9.216 9.216q0.684 0.684 0.684 1.62t-0.684 1.62l-9.216 9.216q-0.684 0.684 -1.62 0.684t-1.62 -0.684 -0.684 -1.62v-4.608h-13.824v13.824h4.608q0.936 0 1.62 0.684t0.684 1.62 -0.684 1.62l-9.216 9.216q-0.684 0.684 -1.62 0.684t-1.62 -0.684l-9.216 -9.216q-0.684 -0.684 -0.684 -1.62t0.684 -1.62 1.62 -0.684h4.608v-13.824h-13.824v4.608q0 0.936 -0.684 1.62t-1.62 0.684 -1.62 -0.684l-9.216 -9.216q-0.684 -0.684 -0.684 -1.62z","STEP_BACKWARD":"M0 -6.839v-50.688q0 -0.936 0.684 -1.62t1.62 -0.684h4.608q0.936 0 1.62 0.684t0.684 1.62v24.408q0.144 -0.396 0.468 -0.684l25.56 -25.56q0.684 -0.684 1.152 -0.468t0.468 1.152v52.992q0 0.936 -0.468 1.152t-1.152 -0.468l-25.56 -25.56q-0.324 -0.324 -0.468 -0.684v24.408q0 0.936 -0.684 1.62t-1.62 0.684h-4.608q-0.936 0 -1.62 -0.684t-0.684 -1.62z","FAST_BACKWARD":"M0 -6.839v-50.688q0 -0.936 0.684 -1.62t1.62 -0.684h4.608q0.936 0 1.62 0.684t0.684 1.62v24.408q0.144 -0.396 0.468 -0.684l25.56 -25.56q0.684 -0.684 1.152 -0.468t0.468 1.152v25.56q0.144 -0.396 0.468 -0.684l25.56 -25.56q0.684 -0.684 1.152 -0.468t0.468 1.152v52.992q0 0.936 -0.468 1.152t-1.152 -0.468l-25.56 -25.56q-0.324 -0.324 -0.468 -0.684v25.56q0 0.936 -0.468 1.152t-1.152 -0.468l-25.56 -25.56q-0.324 -0.324 -0.468 -0.684v24.408q0 0.936 -0.684 1.62t-1.62 0.684h-4.608q-0.936 0 -1.62 -0.684t-0.684 -1.62z","BACKWARD":"M4.392 -32.183q0 -0.936 0.684 -1.62l25.56 -25.56q0.684 -0.684 1.152 -0.468t0.468 1.152v25.56q0.18 -0.396 0.468 -0.684l25.56 -25.56q0.684 -0.684 1.152 -0.468t0.468 1.152v52.992q0 0.936 -0.468 1.152t-1.152 -0.468l-25.56 -25.56q-0.288 -0.324 -0.468 -0.684v25.56q0 0.936 -0.468 1.152t-1.152 -0.468l-25.56 -25.56q-0.684 -0.684 -0.684 -1.62z","PLAY":"M0 -5.687v-52.992q0 -0.936 0.594 -1.296t1.422 0.108l47.808 26.568q0.828 0.468 0.828 1.116t-0.828 1.116l-47.808 26.568q-0.828 0.468 -1.422 0.108t-0.594 -1.296z","PAUSE":"M0 -6.839v-50.688q0 -0.936 0.684 -1.62t1.62 -0.684h18.432q0.936 0 1.62 0.684t0.684 1.62v50.688q0 0.936 -0.684 1.62t-1.62 0.684h-18.432q-0.936 0 -1.62 -0.684t-0.684 -1.62zm32.256 0v-50.688q0 -0.936 0.684 -1.62t1.62 -0.684h18.432q0.936 0 1.62 0.684t0.684 1.62v50.688q0 0.936 -0.684 1.62t-1.62 0.684h-18.432q-0.936 0 -1.62 -0.684t-0.684 -1.62z","STOP":"M0 -6.839v-50.688q0 -0.936 0.684 -1.62t1.62 -0.684h50.688q0.936 0 1.62 0.684t0.684 1.62v50.688q0 0.936 -0.684 1.62t-1.62 0.684h-50.688q-0.936 0 -1.62 -0.684t-0.684 -1.62z","FORWARD":"M0 -5.687v-52.992q0 -0.936 0.468 -1.152t1.152 0.468l25.56 25.56q0.288 0.288 0.468 0.684v-25.56q0 -0.936 0.468 -1.152t1.152 0.468l25.56 25.56q0.684 0.684 0.684 1.62t-0.684 1.62l-25.56 25.56q-0.684 0.684 -1.152 0.468t-0.468 -1.152v-25.56q-0.18 0.36 -0.468 0.684l-25.56 25.56q-0.684 0.684 -1.152 0.468t-0.468 -1.152z","FAST_FORWARD":"M0 -5.687v-52.992q0 -0.936 0.468 -1.152t1.152 0.468l25.56 25.56q0.288 0.288 0.468 0.684v-25.56q0 -0.936 0.468 -1.152t1.152 0.468l25.56 25.56q0.288 0.288 0.468 0.684v-24.408q0 -0.936 0.684 -1.62t1.62 -0.684h4.608q0.936 0 1.62 0.684t0.684 1.62v50.688q0 0.936 -0.684 1.62t-1.62 0.684h-4.608q-0.936 0 -1.62 -0.684t-0.684 -1.62v-24.408q-0.18 0.36 -0.468 0.684l-25.56 25.56q-0.684 0.684 -1.152 0.468t-0.468 -1.152v-25.56q-0.18 0.36 -0.468 0.684l-25.56 25.56q-0.684 0.684 -1.152 0.468t-0.468 -1.152z","STEP_FORWARD":"M0 -5.687v-52.992q0 -0.936 0.468 -1.152t1.152 0.468l25.56 25.56q0.288 0.288 0.468 0.684v-24.408q0 -0.936 0.684 -1.62t1.62 -0.684h4.608q0.936 0 1.62 0.684t0.684 1.62v50.688q0 0.936 -0.684 1.62t-1.62 0.684h-4.608q-0.936 0 -1.62 -0.684t-0.684 -1.62v-24.408q-0.18 0.36 -0.468 0.684l-25.56 25.56q-0.684 0.684 -1.152 0.468t-0.468 -1.152z","EJECT":"M0.036 -11.447v-9.216q0 -0.936 0.684 -1.62t1.62 -0.684h50.688q0.936 0 1.62 0.684t0.684 1.62v9.216q0 0.936 -0.684 1.62t-1.62 0.684h-50.688q-0.936 0 -1.62 -0.684t-0.684 -1.62zm0 -16.596q-0.216 -0.468 0.468 -1.152l25.56 -25.56q0.684 -0.684 1.62 -0.684t1.62 0.684l25.56 25.56q0.684 0.684 0.468 1.152t-1.152 0.468h-52.992q-0.936 0 -1.152 -0.468z","CHEVRON_LEFT":"M5.544 -34.487q0 -0.936 0.684 -1.62l26.712 -26.712q0.684 -0.684 1.62 -0.684t1.62 0.684l5.976 5.976q0.684 0.684 0.684 1.62t-0.684 1.62l-19.116 19.116 19.116 19.116q0.684 0.684 0.684 1.62t-0.684 1.62l-5.976 5.976q-0.684 0.684 -1.62 0.684t-1.62 -0.684l-26.712 -26.712q-0.684 -0.684 -0.684 -1.62z","CHEVRON_RIGHT":"M3.24 -13.751q0 -0.936 0.684 -1.62l19.116 -19.116 -19.116 -19.116q-0.684 -0.684 -0.684 -1.62t0.684 -1.62l5.976 -5.976q0.684 -0.684 1.62 -0.684t1.62 0.684l26.712 26.712q0.684 0.684 0.684 1.62t-0.684 1.62l-26.712 26.712q-0.684 0.684 -1.62 0.684t-1.62 -0.684l-5.976 -5.976q-0.684 -0.684 -0.684 -1.62z","PLUS_CIRCLE":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm11.52 2.304q0 0.936 0.684 1.62t1.62 0.684h9.216v9.216q0 0.936 0.684 1.62t1.62 0.684h4.608q0.936 0 1.62 -0.684t0.684 -1.62v-9.216h9.216q0.936 0 1.62 -0.684t0.684 -1.62v-4.608q0 -0.936 -0.684 -1.62t-1.62 -0.684h-9.216v-9.216q0 -0.936 -0.684 -1.62t-1.62 -0.684h-4.608q-0.936 0 -1.62 0.684t-0.684 1.62v9.216h-9.216q-0.936 0 -1.62 0.684t-0.684 1.62v4.608z","MINUS_CIRCLE":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm11.52 2.304q0 0.936 0.684 1.62t1.62 0.684h27.648q0.936 0 1.62 -0.684t0.684 -1.62v-4.608q0 -0.936 -0.684 -1.62t-1.62 -0.684h-27.648q-0.936 0 -1.62 0.684t-0.684 1.62v4.608z","TIMES_CIRCLE":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm13.932 8.136q0 0.972 0.684 1.656l3.24 3.24q0.684 0.684 1.656 0.684 0.936 0 1.62 -0.684l6.516 -6.516 6.516 6.516q0.684 0.684 1.62 0.684 0.972 0 1.656 -0.684l3.24 -3.24q0.684 -0.684 0.684 -1.656 0 -0.936 -0.684 -1.62l-6.516 -6.516 6.516 -6.516q0.684 -0.684 0.684 -1.62 0 -0.972 -0.684 -1.656l-3.24 -3.24q-0.684 -0.684 -1.656 -0.684 -0.936 0 -1.62 0.684l-6.516 6.516 -6.516 -6.516q-0.684 -0.684 -1.62 -0.684 -0.972 0 -1.656 0.684l-3.24 3.24q-0.684 0.684 -0.684 1.656 0 0.936 0.684 1.62l6.516 6.516 -6.516 6.516q-0.684 0.684 -0.684 1.62z","CHECK_CIRCLE":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm9.072 0.684q0 0.972 0.648 1.62l13.032 13.032q0.684 0.684 1.62 0.684 0.972 0 1.656 -0.684l19.548 -19.548q0.648 -0.648 0.648 -1.62 0 -1.008 -0.648 -1.656l-3.276 -3.24q-0.684 -0.684 -1.62 -0.684t-1.62 0.684l-14.688 14.652 -8.136 -8.136q-0.684 -0.684 -1.62 -0.684t-1.62 0.684l-3.276 3.24q-0.648 0.648 -0.648 1.656z","QUESTION_CIRCLE":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm15.012 -10.764q-0.54 0.864 0.288 1.512l4.752 3.6q0.252 0.216 0.684 0.216 0.576 0 0.9 -0.432 1.908 -2.448 3.096 -3.312 1.224 -0.864 3.096 -0.864 1.728 0 3.078 0.936t1.35 2.124q0 1.368 -0.72 2.196t-2.448 1.62q-2.268 1.008 -4.158 3.114t-1.89 4.518v1.296q0 0.504 0.324 0.828t0.828 0.324h6.912q0.504 0 0.828 -0.324t0.324 -0.828q0 -0.684 0.774 -1.782t1.962 -1.782q1.152 -0.648 1.764 -1.026t1.656 -1.26 1.602 -1.728 1.008 -2.178 0.45 -2.916q0 -3.168 -1.998 -5.868t-4.986 -4.176 -6.12 -1.476q-8.748 0 -13.356 7.668zm8.028 28.044q0 0.504 0.324 0.828t0.828 0.324h6.912q0.504 0 0.828 -0.324t0.324 -0.828v-6.912q0 -0.504 -0.324 -0.828t-0.828 -0.324h-6.912q-0.504 0 -0.828 0.324t-0.324 0.828v6.912z","INFO_CIRCLE":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm18.432 17.28q0 0.504 0.324 0.828t0.828 0.324h16.128q0.504 0 0.828 -0.324t0.324 -0.828v-5.76q0 -0.504 -0.324 -0.828t-0.828 -0.324h-3.456v-18.432q0 -0.504 -0.324 -0.828t-0.828 -0.324h-11.52q-0.504 0 -0.828 0.324t-0.324 0.828v5.76q0 0.504 0.324 0.828t0.828 0.324h3.456v11.52h-3.456q-0.504 0 -0.828 0.324t-0.324 0.828v5.76zm4.608 -32.256q0 0.504 0.324 0.828t0.828 0.324h6.912q0.504 0 0.828 -0.324t0.324 -0.828v-5.76q0 -0.504 -0.324 -0.828t-0.828 -0.324h-6.912q-0.504 0 -0.828 0.324t-0.324 0.828v5.76z","CROSSHAIRS":"M0 -29.879v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h5.148q1.332 -5.796 5.562 -10.026t10.026 -5.562v-5.148q0 -0.936 0.684 -1.62t1.62 -0.684h4.608q0.936 0 1.62 0.684t0.684 1.62v5.148q5.796 1.332 10.026 5.562t5.562 10.026h5.148q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-5.148q-1.332 5.796 -5.562 10.026t-10.026 5.562v5.148q0 0.936 -0.684 1.62t-1.62 0.684h-4.608q-0.936 0 -1.62 -0.684t-0.684 -1.62v-5.148q-5.796 -1.332 -10.026 -5.562t-5.562 -10.026h-5.148q-0.936 0 -1.62 -0.684t-0.684 -1.62zm12.204 2.304q1.152 3.888 4.05 6.786t6.786 4.05v-3.924q0 -0.936 0.684 -1.62t1.62 -0.684h4.608q0.936 0 1.62 0.684t0.684 1.62v3.924q3.888 -1.152 6.786 -4.05t4.05 -6.786h-3.924q-0.936 0 -1.62 -0.684t-0.684 -1.62v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h3.924q-1.152 -3.888 -4.05 -6.786t-6.786 -4.05v3.924q0 0.936 -0.684 1.62t-1.62 0.684h-4.608q-0.936 0 -1.62 -0.684t-0.684 -1.62v-3.924q-3.888 1.152 -6.786 4.05t-4.05 6.786h3.924q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-3.924z","TIMES_CIRCLE_O":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm8.064 0q0 5.328 2.628 9.828t7.128 7.128 9.828 2.628 9.828 -2.628 7.128 -7.128 2.628 -9.828 -2.628 -9.828 -7.128 -7.128 -9.828 -2.628 -9.828 2.628 -7.128 7.128 -2.628 9.828zm7.38 5.76q0 -0.468 0.36 -0.828l4.932 -4.932 -4.932 -4.932q-0.36 -0.36 -0.36 -0.828t0.36 -0.828l5.256 -5.256q0.36 -0.36 0.828 -0.36t0.828 0.36l4.932 4.932 4.932 -4.932q0.36 -0.36 0.828 -0.36t0.828 0.36l5.256 5.256q0.36 0.36 0.36 0.828t-0.36 0.828l-4.932 4.932 4.932 4.932q0.36 0.36 0.36 0.828t-0.36 0.828l-5.256 5.256q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-4.932 -4.932 -4.932 4.932q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-5.256 -5.256q-0.36 -0.36 -0.36 -0.828z","CHECK_CIRCLE_O":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm8.064 0q0 5.328 2.628 9.828t7.128 7.128 9.828 2.628 9.828 -2.628 7.128 -7.128 2.628 -9.828 -2.628 -9.828 -7.128 -7.128 -9.828 -2.628 -9.828 2.628 -7.128 7.128 -2.628 9.828zm4.392 0q0 -0.936 0.684 -1.62l3.672 -3.672q0.684 -0.684 1.62 -0.684t1.62 0.684l5.292 5.292 9.9 -9.9q0.684 -0.684 1.62 -0.684t1.62 0.684l3.672 3.672q0.684 0.684 0.684 1.62t-0.684 1.62l-15.192 15.192q-0.684 0.684 -1.62 0.684t-1.62 -0.684l-10.584 -10.584q-0.684 -0.684 -0.684 -1.62z","BAN":"M0 -32.291q0 -5.652 2.196 -10.782t5.886 -8.838 8.82 -5.904 10.746 -2.196 10.746 2.196 8.82 5.904 5.886 8.838 2.196 10.782 -2.196 10.8 -5.886 8.856 -8.82 5.904 -10.746 2.196 -10.746 -2.196 -8.82 -5.904 -5.886 -8.856 -2.196 -10.8zm8.064 0q0 5.832 3.204 10.764l27.18 -27.144q-4.86 -3.276 -10.8 -3.276 -5.328 0 -9.828 2.628t-7.128 7.164 -2.628 9.864zm8.892 16.488q4.932 3.204 10.692 3.204 3.996 0 7.614 -1.566t6.246 -4.194 4.176 -6.282 1.548 -7.65q0 -5.796 -3.132 -10.62z","ARROW_LEFT":"M2.304 -29.879q0 -1.872 1.332 -3.276l23.436 -23.4q1.368 -1.368 3.276 -1.368 1.872 0 3.24 1.368l2.7 2.664q1.368 1.368 1.368 3.276t-1.368 3.276l-10.548 10.548h25.344q1.872 0 3.042 1.35t1.17 3.258v4.608q0 1.908 -1.17 3.258t-3.042 1.35h-25.344l10.548 10.584q1.368 1.296 1.368 3.24t-1.368 3.24l-2.7 2.736q-1.332 1.332 -3.24 1.332 -1.872 0 -3.276 -1.332l-23.436 -23.472q-1.332 -1.332 -1.332 -3.24z","ARROW_RIGHT":"M0 -27.575v-4.608q0 -1.908 1.17 -3.258t3.042 -1.35h25.344l-10.548 -10.584q-1.368 -1.296 -1.368 -3.24t1.368 -3.24l2.7 -2.7q1.368 -1.368 3.24 -1.368 1.908 0 3.276 1.368l23.436 23.436q1.332 1.26 1.332 3.24 0 1.944 -1.332 3.276l-23.436 23.436q-1.404 1.332 -3.276 1.332 -1.836 0 -3.24 -1.332l-2.7 -2.7q-1.368 -1.368 -1.368 -3.276t1.368 -3.276l10.548 -10.548h-25.344q-1.872 0 -3.042 -1.35t-1.17 -3.258z","ARROW_UP":"M1.908 -29.483q0 -1.908 1.368 -3.276l23.436 -23.436q1.26 -1.332 3.24 -1.332 1.944 0 3.276 1.332l23.436 23.436q1.332 1.404 1.332 3.276 0 1.836 -1.332 3.24l-2.7 2.7q-1.368 1.368 -3.276 1.368 -1.944 0 -3.24 -1.368l-10.584 -10.548v25.344q0 1.872 -1.35 3.042t-3.258 1.17h-4.608q-1.908 0 -3.258 -1.17t-1.35 -3.042v-25.344l-10.584 10.548q-1.296 1.368 -3.24 1.368t-3.24 -1.368l-2.7 -2.7q-1.368 -1.368 -1.368 -3.24z","ARROW_DOWN":"M1.908 -34.487q0 -1.908 1.368 -3.276l2.664 -2.7q1.404 -1.332 3.276 -1.332 1.908 0 3.24 1.332l10.584 10.584v-25.344q0 -1.872 1.368 -3.24t3.24 -1.368h4.608q1.872 0 3.24 1.368t1.368 3.24v25.344l10.584 -10.584q1.332 -1.332 3.24 -1.332 1.872 0 3.276 1.332l2.7 2.7q1.332 1.404 1.332 3.276 0 1.908 -1.332 3.24l-23.436 23.472q-1.404 1.332 -3.276 1.332 -1.908 0 -3.24 -1.332l-23.436 -23.472q-1.368 -1.296 -1.368 -3.24z","SHARE":"M0 -24.119q0 -7.164 1.908 -11.988 5.832 -14.508 31.5 -14.508h8.064v-9.216q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684l18.432 18.432q0.684 0.684 0.684 1.62t-0.684 1.62l-18.432 18.432q-0.684 0.684 -1.62 0.684t-1.62 -0.684 -0.684 -1.62v-9.216h-8.064q-3.528 0 -6.318 0.216t-5.544 0.774 -4.788 1.53 -3.798 2.502 -2.88 3.636 -1.746 4.986 -0.63 6.516q0 1.98 0.18 4.428 0 0.216 0.09 0.846t0.09 0.954q0 0.54 -0.306 0.9t-0.846 0.36q-0.576 0 -1.008 -0.612 -0.252 -0.324 -0.468 -0.792t-0.486 -1.08 -0.378 -0.864q-4.572 -10.26 -4.572 -16.236z","EXPAND":"M0 -6.839v-16.128q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684l5.184 5.184 11.952 -11.952q0.36 -0.36 0.828 -0.36t0.828 0.36l4.104 4.104q0.36 0.36 0.36 0.828t-0.36 0.828l-11.952 11.952 5.184 5.184q0.684 0.684 0.684 1.62t-0.684 1.62 -1.62 0.684h-16.128q-0.936 0 -1.62 -0.684t-0.684 -1.62zm28.116 -31.104q0 -0.468 0.36 -0.828l11.952 -11.952 -5.184 -5.184q-0.684 -0.684 -0.684 -1.62t0.684 -1.62 1.62 -0.684h16.128q0.936 0 1.62 0.684t0.684 1.62v16.128q0 0.936 -0.684 1.62t-1.62 0.684 -1.62 -0.684l-5.184 -5.184 -11.952 11.952q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-4.104 -4.104q-0.36 -0.36 -0.36 -0.828z","COMPRESS":"M0.468 -10.295q0 -0.468 0.36 -0.828l11.952 -11.952 -5.184 -5.184q-0.684 -0.684 -0.684 -1.62t0.684 -1.62 1.62 -0.684h16.128q0.936 0 1.62 0.684t0.684 1.62v16.128q0 0.936 -0.684 1.62t-1.62 0.684 -1.62 -0.684l-5.184 -5.184 -11.952 11.952q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-4.104 -4.104q-0.36 -0.36 -0.36 -0.828zm27.18 -24.192v-16.128q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684l5.184 5.184 11.952 -11.952q0.36 -0.36 0.828 -0.36t0.828 0.36l4.104 4.104q0.36 0.36 0.36 0.828t-0.36 0.828l-11.952 11.952 5.184 5.184q0.684 0.684 0.684 1.62t-0.684 1.62 -1.62 0.684h-16.128q-0.936 0 -1.62 -0.684t-0.684 -1.62z","PLUS":"M0 -31.031v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h14.976v-14.976q0 -1.44 1.008 -2.448t2.448 -1.008h6.912q1.44 0 2.448 1.008t1.008 2.448v14.976h14.976q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-14.976v14.976q0 1.44 -1.008 2.448t-2.448 1.008h-6.912q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-14.976q-1.44 0 -2.448 -1.008t-1.008 -2.448z","MINUS":"M0 -31.031v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h43.776q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-43.776q-1.44 0 -2.448 -1.008t-1.008 -2.448z","ASTERISK":"M4.41 -23.849q0.486 -1.854 2.142 -2.79l9.576 -5.544 -9.576 -5.544q-1.656 -0.936 -2.142 -2.79t0.45 -3.51l2.304 -3.96q0.936 -1.656 2.79 -2.142t3.51 0.45l9.576 5.508v-11.052q0 -1.872 1.368 -3.24t3.24 -1.368h4.608q1.872 0 3.24 1.368t1.368 3.24v11.052l9.576 -5.508q1.656 -0.936 3.51 -0.45t2.79 2.142l2.304 3.96q0.936 1.656 0.45 3.51t-2.142 2.79l-9.576 5.544 9.576 5.544q1.656 0.936 2.142 2.79t-0.45 3.51l-2.304 3.96q-0.936 1.656 -2.79 2.142t-3.51 -0.45l-9.576 -5.508v11.052q0 1.872 -1.368 3.24t-3.24 1.368h-4.608q-1.872 0 -3.24 -1.368t-1.368 -3.24v-11.052l-9.576 5.508q-1.656 0.936 -3.51 0.45t-2.79 -2.142l-2.304 -3.96q-0.936 -1.656 -0.45 -3.51z","EXCLAMATION_CIRCLE":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm22.464 -17.496l0.612 22.356q0 0.36 0.36 0.63t0.864 0.27h6.66q0.504 0 0.846 -0.27t0.378 -0.63l0.648 -22.356q0 -0.432 -0.36 -0.648 -0.36 -0.288 -0.864 -0.288h-7.92q-0.504 0 -0.864 0.288 -0.36 0.216 -0.36 0.648zm0.576 34.74q0 0.468 0.36 0.828t0.828 0.36h6.912q0.468 0 0.792 -0.342t0.324 -0.846v-6.84q0 -0.504 -0.324 -0.846t-0.792 -0.342h-6.912q-0.468 0 -0.828 0.36t-0.36 0.828v6.84z","GIFT":"M0 -28.727v-11.52q0 -0.504 0.324 -0.828t0.828 -0.324h15.84q-3.348 0 -5.706 -2.358t-2.358 -5.706 2.358 -5.706 5.706 -2.358q3.852 0 6.048 2.772l4.608 5.94 4.608 -5.94q2.196 -2.772 6.048 -2.772 3.348 0 5.706 2.358t2.358 5.706 -2.358 5.706 -5.706 2.358h15.84q0.504 0 0.828 0.324t0.324 0.828v11.52q0 0.504 -0.324 0.828t-0.828 0.324h-3.456v14.976q0 1.44 -1.008 2.448t-2.448 1.008h-39.168q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-3.456q-0.504 0 -0.828 -0.324t-0.324 -0.828zm13.536 -20.736q0 1.44 1.008 2.448t2.448 1.008h7.02l-4.536 -5.796q-0.936 -1.116 -2.484 -1.116 -1.44 0 -2.448 1.008t-1.008 2.448zm8.352 33.84q0 0.9 0.648 1.386t1.656 0.486h6.912q1.008 0 1.656 -0.486t0.648 -1.386v-25.775999999999996h-11.52v25.775999999999996zm9.432 -30.384h6.984q1.44 0 2.448 -1.008t1.008 -2.448 -1.008 -2.448 -2.448 -1.008q-1.548 0 -2.484 1.116z","LEAF":"M0 -13.499q0 -1.26 1.116 -2.646t2.448 -2.358 2.448 -2.016 1.116 -1.728q0 -0.144 -0.504 -1.368t-0.576 -1.584q-0.324 -1.836 -0.324 -3.744 0 -4.14 1.566 -7.92t4.284 -6.642 6.138 -5.004 7.344 -3.438q1.98 -0.648 5.22 -0.918t6.462 -0.324 6.426 -0.216 5.886 -0.864 4.086 -2.034l1.062 -1.062 1.062 -1.008 0.972 -0.72 1.314 -0.576 1.566 -0.162q1.404 0 2.538 1.656t1.71 4.032 0.864 4.464 0.288 3.456q0 3.42 -0.72 6.948 -1.656 8.064 -6.642 13.788t-12.87 9.648q-7.704 3.888 -15.768 3.888 -5.328 0 -10.296 -1.692 -0.54 -0.18 -3.168 -1.512t-3.456 -1.332q-0.576 0 -1.422 1.152t-1.62 2.52 -1.89 2.52 -2.16 1.152q-1.08 0 -1.836 -0.396t-1.116 -0.864 -0.972 -1.512l-0.216 -0.396 -0.198 -0.36 -0.108 -0.342 -0.054 -0.486zm13.824 -11.772q0 0.936 0.684 1.62t1.62 0.684q0.864 0 1.62 -0.684 0.972 -0.864 2.664 -2.556t2.412 -2.376q4.932 -4.464 9.666 -6.336t11.286 -1.872q0.936 0 1.62 -0.684t0.684 -1.62 -0.684 -1.62 -1.62 -0.684q-6.192 0 -11.448 1.782t-9.342 4.824 -8.478 7.902q-0.684 0.756 -0.684 1.62z","FIRE":"M0 -3.383q0 -0.468 0.342 -0.81t0.81 -0.342h48.384q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-48.384q-0.468 0 -0.81 -0.342t-0.342 -0.81v-2.304zm9.216 -28.8q0 -2.808 0.882 -5.184t2.304 -4.05 3.15 -3.168 3.456 -2.79 3.15 -2.592 2.304 -2.934 0.882 -3.474q0 -3.384 -2.376 -8.064l0.108 0.036 -0.036 -0.036q3.24 1.476 5.76 2.988t4.986 3.6 4.086 4.41 2.61 5.418 0.99 6.624q0 2.808 -0.882 5.184t-2.304 4.05 -3.15 3.168 -3.456 2.79 -3.15 2.592 -2.304 2.934 -0.882 3.474q0 3.456 2.412 8.064l-0.144 -0.036 0.036 0.036q-3.24 -1.476 -5.76 -2.988t-4.986 -3.6 -4.086 -4.41 -2.61 -5.418 -0.99 -6.624z","EYE":"M0 -29.879q0 -1.224 0.72 -2.484 5.04 -8.244 13.554 -13.248t17.982 -5.004 17.982 5.004 13.554 13.248q0.72 1.26 0.72 2.484t-0.72 2.484q-5.04 8.28 -13.554 13.266t-17.982 4.986 -17.982 -5.004 -13.554 -13.248q-0.72 -1.26 -0.72 -2.484zm4.608 0q4.788 7.38 12.006 11.754t15.642 4.374 15.642 -4.374 12.006 -11.754q-5.472 -8.496 -13.716 -12.708 2.196 3.744 2.196 8.1 0 6.66 -4.734 11.394t-11.394 4.734 -11.394 -4.734 -4.734 -11.394q0 -4.356 2.196 -8.1 -8.244 4.212 -13.716 12.708zm16.704 -4.608q0 0.72 0.504 1.224t1.224 0.504 1.224 -0.504 0.504 -1.224q0 -3.096 2.196 -5.292t5.292 -2.196q0.72 0 1.224 -0.504t0.504 -1.224 -0.504 -1.224 -1.224 -0.504q-4.5 0 -7.722 3.222t-3.222 7.722z","EYE_SLASH":"M0 -29.879q0 -1.368 0.72 -2.484 5.508 -8.46 13.68 -13.356t17.856 -4.896q3.204 0 6.48 0.612l1.944 -3.492q0.36 -0.576 1.008 -0.576 0.18 0 0.648 0.216t1.116 0.558 1.188 0.666 1.134 0.666 0.702 0.414q0.576 0.36 0.576 0.972 0 0.252 -0.036 0.324 -3.78 6.768 -11.34 20.376t-11.376 20.412l-1.764 3.204q-0.36 0.576 -1.008 0.576 -0.432 0 -4.824 -2.52 -0.576 -0.36 -0.576 -1.008 0 -0.432 1.584 -3.132 -5.148 -2.34 -9.486 -6.228t-7.506 -8.82q-0.72 -1.116 -0.72 -2.484zm4.608 0q6.012 9.288 15.372 13.5l2.808 -5.076q-3.132 -2.268 -4.896 -5.724t-1.764 -7.308q0 -4.356 2.196 -8.1 -8.244 4.212 -13.716 12.708zm16.704 -4.608q0 0.72 0.504 1.224t1.224 0.504 1.224 -0.504 0.504 -1.224q0 -3.096 2.196 -5.292t5.292 -2.196q0.72 0 1.224 -0.504t0.504 -1.224 -0.504 -1.224 -1.224 -0.504q-4.5 0 -7.722 3.222t-3.222 7.722zm10.944 25.344l2.664 -4.752q7.632 -0.648 14.13 -4.932t10.854 -11.052q-4.14 -6.444 -10.152 -10.584l2.268 -4.032q3.42 2.304 6.57 5.508t5.202 6.624q0.72 1.224 0.72 2.484t-0.72 2.484q-1.404 2.304 -3.924 5.22 -5.4 6.192 -12.51 9.612t-15.102 3.42zm5.76 -10.296l10.08 -18.072q0.288 1.62 0.288 3.024 0 5.004 -2.844 9.126t-7.524 5.922z","WARNING":"M0.576 -11.339l27.648 -50.688q0.612 -1.116 1.692 -1.764t2.34 -0.648 2.34 0.648 1.692 1.764l27.648 50.688q1.26 2.268 -0.072 4.536 -0.612 1.044 -1.674 1.656t-2.286 0.612h-55.296q-1.224 0 -2.286 -0.612t-1.674 -1.656q-1.332 -2.268 -0.072 -4.536zm26.496 -33.516l0.612 16.452q0 0.36 0.36 0.594t0.864 0.234h6.66q0.504 0 0.846 -0.234t0.378 -0.594l0.648 -16.524q0 -0.432 -0.36 -0.684 -0.468 -0.396 -0.864 -0.396h-7.92q-0.396 0 -0.864 0.396 -0.36 0.252 -0.36 0.756zm0.576 29.916q0 0.504 0.342 0.846t0.81 0.342h6.912q0.468 0 0.81 -0.342t0.342 -0.846v-6.84q0 -0.504 -0.342 -0.846t-0.81 -0.342h-6.912q-0.468 0 -0.81 0.342t-0.342 0.846v6.84z","PLANE":"M0 -26.315q-0.036 -0.468 0.324 -0.9l3.456 -3.492q0.324 -0.324 0.828 -0.324 0.216 0 0.288 0.036l6.984 1.908 9.324 -9.324 -18.288 -10.044q-0.504 -0.288 -0.612 -0.864 -0.072 -0.576 0.324 -0.972l4.608 -4.608q0.504 -0.468 1.08 -0.288l23.94 5.724 5.76 -5.76q2.736 -2.736 6.192 -3.888t5.328 0.432q1.584 1.872 0.432 5.328t-3.888 6.192l-5.796 5.796 5.76 25.056q0.18 0.684 -0.432 1.188l-4.608 3.456q-0.252 0.216 -0.684 0.216 -0.144 0 -0.252 -0.036 -0.54 -0.108 -0.756 -0.576l-10.044 -18.288 -9.324 9.324 1.908 6.984q0.18 0.612 -0.288 1.116l-3.456 3.456q-0.324 0.324 -0.828 0.324h-0.072q-0.54 -0.072 -0.864 -0.468l-6.804 -9.072 -9.072 -6.804q-0.396 -0.252 -0.468 -0.828z","CALENDAR":"M0 -4.535v-46.08q0 -1.872 1.368 -3.24t3.24 -1.368h4.608v-3.456q0 -2.376 1.692 -4.068t4.068 -1.692h2.304q2.376 0 4.068 1.692t1.692 4.068v3.456h13.824v-3.456q0 -2.376 1.692 -4.068t4.068 -1.692h2.304q2.376 0 4.068 1.692t1.692 4.068v3.456h4.608q1.872 0 3.24 1.368t1.368 3.24v46.08q0 1.872 -1.368 3.24t-3.24 1.368h-50.688q-1.872 0 -3.24 -1.368t-1.368 -3.24zm4.608 0h10.368v-10.368h-10.368v10.368zm0 -12.672h10.368v-11.52h-10.368v11.52zm0 -13.824h10.368v-10.368h-10.368v10.368zm9.216 -17.28q0 0.468 0.342 0.81t0.81 0.342h2.304q0.468 0 0.81 -0.342t0.342 -0.81v-10.368q0 -0.468 -0.342 -0.81t-0.81 -0.342h-2.304q-0.468 0 -0.81 0.342t-0.342 0.81v10.368zm3.456 43.776h11.52v-10.368h-11.52v10.368zm0 -12.672h11.52v-11.52h-11.52v11.52zm0 -13.824h11.52v-10.368h-11.52v10.368zm13.824 26.496h11.52v-10.368h-11.52v10.368zm0 -12.672h11.52v-11.52h-11.52v11.52zm0 -13.824h11.52v-10.368h-11.52v10.368zm10.368 -17.28q0 0.468 0.342 0.81t0.81 0.342h2.304q0.468 0 0.81 -0.342t0.342 -0.81v-10.368q0 -0.468 -0.342 -0.81t-0.81 -0.342h-2.304q-0.468 0 -0.81 0.342t-0.342 0.81v10.368zm3.456 43.776h10.368v-10.368h-10.368v10.368zm0 -12.672h10.368v-11.52h-10.368v11.52zm0 -13.824h10.368v-10.368h-10.368v10.368z","RANDOM":"M0 -14.903v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324h8.064q1.728 0 3.132 -0.54t2.484 -1.62 1.836 -2.214 1.62 -2.79q1.152 -2.232 2.808 -6.156 1.044 -2.376 1.782 -3.996t1.944 -3.78 2.304 -3.6 2.664 -2.988 3.24 -2.466 3.834 -1.512 4.608 -0.594h9.216v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324q0.432 0 0.864 0.36l11.484 11.484q0.324 0.324 0.324 0.828t-0.324 0.828l-11.52 11.52q-0.324 0.324 -0.828 0.324 -0.468 0 -0.81 -0.342t-0.342 -0.81v-6.912h-9.216q-1.728 0 -3.132 0.54t-2.484 1.62 -1.836 2.214 -1.62 2.79q-1.152 2.232 -2.808 6.156 -1.044 2.376 -1.782 3.996t-1.944 3.78 -2.304 3.6 -2.664 2.988 -3.24 2.466 -3.834 1.512 -4.608 0.594h-8.064q-0.504 0 -0.828 -0.324t-0.324 -0.828zm0 -32.256v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324h8.064q9 0 14.76 8.1 -2.16 3.312 -4.932 9.828 -0.792 -1.62 -1.332 -2.61t-1.458 -2.286 -1.836 -2.034 -2.268 -1.26 -2.934 -0.522h-8.064q-0.504 0 -0.828 -0.324t-0.324 -0.828zm26.748 25.308q2.124 -3.348 4.896 -9.828 0.792 1.62 1.332 2.61t1.458 2.286 1.836 2.034 2.268 1.26 2.934 0.522h9.216v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324q0.432 0 0.864 0.36l11.484 11.484q0.324 0.324 0.324 0.828t-0.324 0.828l-11.52 11.52q-0.324 0.324 -0.828 0.324 -0.468 0 -0.81 -0.342t-0.342 -0.81v-6.912q-1.152 0 -3.06 0.018t-2.916 0.036 -2.628 -0.036 -2.556 -0.18 -2.304 -0.378 -2.268 -0.666 -2.088 -1.026 -2.124 -1.44 -1.98 -1.926 -2.016 -2.502z","COMMENT":"M0 -32.183q0 -4.68 2.556 -8.946t6.876 -7.362 10.296 -4.914 12.528 -1.818q8.784 0 16.2 3.078t11.736 8.388 4.32 11.574 -4.32 11.574 -11.736 8.388 -16.2 3.078q-2.52 0 -5.22 -0.288 -7.128 6.3 -16.56 8.712 -1.764 0.504 -4.104 0.792 -0.612 0.072 -1.098 -0.324t-0.63 -1.044v-0.036q-0.108 -0.144 -0.018 -0.432t0.072 -0.36 0.162 -0.342l0.216 -0.324 0.252 -0.306 0.288 -0.324q0.252 -0.288 1.116 -1.242t1.242 -1.368 1.116 -1.422 1.17 -1.836 0.972 -2.124 0.936 -2.736q-5.652 -3.204 -8.91 -7.92t-3.258 -10.116z","MAGNET":"M0 -29.879v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h13.824q0.936 0 1.62 0.684t0.684 1.62v4.608q0 1.872 0.846 3.24t1.926 2.052 2.556 1.08 2.304 0.468 1.584 0.072 1.584 -0.072 2.304 -0.468 2.556 -1.08 1.926 -2.052 0.846 -3.24v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h13.824q0.936 0 1.62 0.684t0.684 1.62v4.608q0 7.236 -3.546 13.032t-9.864 9.054 -14.238 3.258 -14.238 -3.258 -9.864 -9.054 -3.546 -13.032zm0 -13.824v-13.824q0 -0.936 0.684 -1.62t1.62 -0.684h13.824q0.936 0 1.62 0.684t0.684 1.62v13.824q0 0.936 -0.684 1.62t-1.62 0.684h-13.824q-0.936 0 -1.62 -0.684t-0.684 -1.62zm36.864 0v-13.824q0 -0.936 0.684 -1.62t1.62 -0.684h13.824q0.936 0 1.62 0.684t0.684 1.62v13.824q0 0.936 -0.684 1.62t-1.62 0.684h-13.824q-0.936 0 -1.62 -0.684t-0.684 -1.62z","CHEVRON_UP":"M3.24 -18.161q0 -0.954 0.684 -1.638l26.712 -26.676q0.684 -0.684 1.62 -0.684t1.62 0.684l26.712 26.676q0.684 0.684 0.684 1.638t-0.684 1.638l-5.976 5.94q-0.684 0.684 -1.62 0.684t-1.62 -0.684l-19.116 -19.116 -19.116 19.116q-0.684 0.684 -1.62 0.684t-1.62 -0.684l-5.976 -5.94q-0.684 -0.684 -0.684 -1.638z","CHEVRON_DOWN":"M3.24 -36.989q0 -0.954 0.684 -1.638l5.976 -5.94q0.684 -0.684 1.62 -0.684t1.62 0.684l19.116 19.116 19.116 -19.116q0.684 -0.684 1.62 -0.684t1.62 0.684l5.976 5.94q0.684 0.684 0.684 1.638t-0.684 1.638l-26.712 26.676q-0.684 0.684 -1.62 0.684t-1.62 -0.684l-26.712 -26.676q-0.684 -0.684 -0.684 -1.638z","RETWEET":"M0 -34.487q0 -0.864 0.54 -1.476l11.52 -13.824q0.684 -0.792 1.764 -0.792t1.764 0.792l11.52 13.824q0.54 0.612 0.54 1.476 0 0.936 -0.684 1.62t-1.62 0.684h-6.912v13.824h20.736q0.576 0 0.9 0.396l5.76 6.912q0.252 0.396 0.252 0.756 0 0.468 -0.342 0.81t-0.81 0.342h-34.56q-0.288 0 -0.486 -0.072t-0.324 -0.252 -0.198 -0.288 -0.108 -0.414 -0.036 -0.414v-21.6h-6.912q-0.936 0 -1.62 -0.684t-0.684 -1.62zm23.04 -14.976q0 -0.468 0.342 -0.81t0.81 -0.342h34.56q0.288 0 0.486 0.072t0.324 0.252 0.198 0.288 0.108 0.414 0.036 0.414v21.6h6.912q0.936 0 1.62 0.684t0.684 1.62q0 0.864 -0.54 1.476l-11.52 13.824q-0.72 0.828 -1.764 0.828t-1.764 -0.828l-11.52 -13.824q-0.54 -0.612 -0.54 -1.476 0 -0.936 0.684 -1.62t1.62 -0.684h6.912v-13.824h-20.736q-0.576 0 -0.9 -0.432l-5.76 -6.912q-0.252 -0.324 -0.252 -0.72z","SHOPPING_CART":"M0 -52.919q0 -0.936 0.684 -1.62t1.62 -0.684h9.216q0.576 0 1.026 0.234t0.72 0.558 0.468 0.882 0.27 0.954 0.198 1.062 0.162 0.918h43.236q0.936 0 1.62 0.684t0.684 1.62v18.432q0 0.864 -0.576 1.53t-1.476 0.774l-37.584 4.392q0.036 0.252 0.162 0.774t0.216 0.954 0.09 0.792q0 0.576 -0.864 2.304h33.12q0.936 0 1.62 0.684t0.684 1.62 -0.684 1.62 -1.62 0.684h-36.864q-0.936 0 -1.62 -0.684t-0.684 -1.62q0 -0.504 0.396 -1.422t1.062 -2.142 0.738 -1.368l-6.372 -29.628h-7.344q-0.936 0 -1.62 -0.684t-0.684 -1.62zm13.824 43.776q0 -1.908 1.35 -3.258t3.258 -1.35 3.258 1.35 1.35 3.258 -1.35 3.258 -3.258 1.35 -3.258 -1.35 -1.35 -3.258zm32.256 0q0 -1.908 1.35 -3.258t3.258 -1.35 3.258 1.35 1.35 3.258 -1.35 3.258 -3.258 1.35 -3.258 -1.35 -1.35 -3.258z","FOLDER":"M0 -17.207v-34.56q0 -3.312 2.376 -5.688t5.688 -2.376h11.52q3.312 0 5.688 2.376t2.376 5.688v1.152h24.192q3.312 0 5.688 2.376t2.376 5.688v25.344q0 3.312 -2.376 5.688t-5.688 2.376h-43.776q-3.312 0 -5.688 -2.376t-2.376 -5.688z","FOLDER_OPEN":"M0 -17.207v-34.56q0 -3.312 2.376 -5.688t5.688 -2.376h11.52q3.312 0 5.688 2.376t2.376 5.688v1.152h19.584q3.312 0 5.688 2.376t2.376 5.688v5.76h-29.952q-3.384 0 -7.092 1.71t-5.904 4.302l-12.132 14.256 -0.18 0.216 -0.018 -0.45 -0.018 -0.45zm2.628 6.048q0 -1.116 1.116 -2.376l12.096 -14.256q1.548 -1.836 4.338 -3.114t5.166 -1.278h39.168q1.224 0 2.178 0.468t0.954 1.548q0 1.116 -1.116 2.376l-12.096 14.256q-1.548 1.836 -4.338 3.114t-5.166 1.278h-39.168q-1.224 0 -2.178 -0.468t-0.954 -1.548z","ARROWS_V":"M2.304 -11.447q0 -0.936 0.684 -1.62t1.62 -0.684h4.608v-36.864h-4.608q-0.936 0 -1.62 -0.684t-0.684 -1.62 0.684 -1.62l9.216 -9.216q0.684 -0.684 1.62 -0.684t1.62 0.684l9.216 9.216q0.684 0.684 0.684 1.62t-0.684 1.62 -1.62 0.684h-4.608v36.864h4.608q0.936 0 1.62 0.684t0.684 1.62 -0.684 1.62l-9.216 9.216q-0.684 0.684 -1.62 0.684t-1.62 -0.684l-9.216 -9.216q-0.684 -0.684 -0.684 -1.62z","ARROWS_H":"M0 -32.183q0 -0.936 0.684 -1.62l9.216 -9.216q0.684 -0.684 1.62 -0.684t1.62 0.684 0.684 1.62v4.608h36.864v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684l9.216 9.216q0.684 0.684 0.684 1.62t-0.684 1.62l-9.216 9.216q-0.684 0.684 -1.62 0.684t-1.62 -0.684 -0.684 -1.62v-4.608h-36.864v4.608q0 0.936 -0.684 1.62t-1.62 0.684 -1.62 -0.684l-9.216 -9.216q-0.684 -0.684 -0.684 -1.62z","BAR_CHART_O":"M0 -10.295v-43.776q0 -2.376 1.692 -4.068t4.068 -1.692h57.6q2.376 0 4.068 1.692t1.692 4.068v43.776q0 2.376 -1.692 4.068t-4.068 1.692h-57.6q-2.376 0 -4.068 -1.692t-1.692 -4.068zm4.608 0q0 0.468 0.342 0.81t0.81 0.342h57.6q0.468 0 0.81 -0.342t0.342 -0.81v-43.776q0 -0.468 -0.342 -0.81t-0.81 -0.342h-57.6q-0.468 0 -0.81 0.342t-0.342 0.81v43.776zm4.608 -3.456v-13.824h9.216v13.824h-9.216zm13.824 0v-32.256h9.216v32.256h-9.216zm13.824 0v-23.04h9.216v23.04h-9.216zm13.824 0v-36.864h9.216v36.864h-9.216z","TWITTER_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 -4.536q5.328 3.384 11.592 3.384 4.032 0 7.56 -1.278t6.048 -3.42 4.338 -4.932 2.7 -5.832 0.882 -6.066q0 -0.648 -0.036 -0.972 2.268 -1.62 3.78 -3.924 -2.016 0.9 -4.356 1.224 2.448 -1.44 3.348 -4.212 -2.34 1.368 -4.824 1.836 -2.196 -2.376 -5.508 -2.376 -3.132 0 -5.346 2.214t-2.214 5.346q0 1.044 0.18 1.728 -4.644 -0.252 -8.712 -2.34t-6.912 -5.58q-1.044 1.8 -1.044 3.816 0 4.104 3.276 6.3 -1.692 -0.036 -3.6 -0.936v0.072q0 2.7 1.8 4.806t4.428 2.61q-1.044 0.288 -1.836 0.288 -0.468 0 -1.404 -0.144 0.756 2.268 2.682 3.744t4.374 1.512q-4.176 3.24 -9.396 3.24 -0.936 0 -1.8 -0.108z","FACEBOOK_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-6.912v-21.888h7.308l1.08 -8.064h-8.388v-5.148q0 -1.944 1.008 -2.988t3.456 -1.044l4.752 -0.036v-7.452q-3.456 -0.324 -6.48 -0.324 -4.896 0 -7.848 2.898t-2.952 8.118v5.976h-8.064v8.064h8.064v21.888h-19.584q-4.284 0 -7.326 -3.042t-3.042 -7.326z","CAMERA_RETRO":"M0 -9.143v-46.08q0 -1.908 1.35 -3.258t3.258 -1.35h55.296q1.908 0 3.258 1.35t1.35 3.258v46.08q0 1.908 -1.35 3.258t-3.258 1.35h-55.296q-1.908 0 -3.258 -1.35t-1.35 -3.258zm4.608 0h55.296v-4.608h-55.296v4.608zm0 -36.864h55.296v-9.216000000000001h-29.808l-2.304 4.608h-23.184v4.608zm4.608 -6.912h13.824v-4.608h-13.824v4.608zm9.216 23.112q0 5.724 4.05 9.774t9.774 4.05 9.774 -4.05 4.05 -9.774 -4.05 -9.774 -9.774 -4.05 -9.774 4.05 -4.05 9.774zm4.608 0q0 -3.816 2.7 -6.516t6.516 -2.7 6.516 2.7 2.7 6.516 -2.7 6.516 -6.516 2.7 -6.516 -2.7 -2.7 -6.516zm3.456 -0.072q0 0.504 0.324 0.828t0.828 0.324 0.828 -0.324 0.324 -0.828q0 -1.44 1.008 -2.448t2.448 -1.008q0.504 0 0.828 -0.324t0.324 -0.828 -0.324 -0.828 -0.828 -0.324q-2.376 0 -4.068 1.692t-1.692 4.068z","KEY":"M0 -36.215q0 -5.76 3.42 -11.268t8.928 -8.928 11.268 -3.42q5.868 0 9.558 3.69t3.69 9.558q0 6.804 -4.716 13.14l12.78 12.78 3.456 -3.456q-0.108 -0.108 -0.936 -0.882t-1.44 -1.386 -1.188 -1.314 -0.576 -1.026q0 -0.612 1.764 -2.376t2.376 -1.764q0.468 0 0.828 0.36 0.216 0.216 1.656 1.602t2.952 2.862 3.114 3.096 2.628 2.808 1.026 1.476q0 0.612 -1.764 2.376t-2.376 1.764q-0.324 0 -1.026 -0.576t-1.314 -1.188 -1.386 -1.44 -0.882 -0.936l-3.456 3.456 7.92 7.92q1.008 1.008 1.008 2.448 0 1.512 -1.404 2.916t-2.916 1.404q-1.44 0 -2.448 -1.008l-24.156 -24.156q-6.336 4.716 -13.14 4.716 -5.868 0 -9.558 -3.69t-3.69 -9.558zm6.912 -0.576q0 2.88 2.016 4.896t4.896 2.016 4.896 -2.016 2.016 -4.896q0 -1.512 -0.684 -2.988 1.476 0.684 2.988 0.684 2.88 0 4.896 -2.016t2.016 -4.896 -2.016 -4.896 -4.896 -2.016 -4.896 2.016 -2.016 4.896q0 1.512 0.684 2.988 -1.476 -0.684 -2.988 -0.684 -2.88 0 -4.896 2.016t-2.016 4.896z","GEARS":"M0 -28.907v-6.66q0 -0.36 0.252 -0.702t0.576 -0.378l5.58 -0.864q0.396 -1.26 1.152 -2.736 -1.224 -1.728 -3.24 -4.14 -0.252 -0.396 -0.252 -0.72 0 -0.432 0.252 -0.72 0.792 -1.08 2.952 -3.204t2.844 -2.124q0.396 0 0.756 0.252l4.14 3.24q1.224 -0.648 2.772 -1.152 0.396 -3.888 0.828 -5.544 0.252 -0.864 1.08 -0.864h6.696q0.396 0 0.72 0.27t0.36 0.63l0.828 5.508q1.224 0.36 2.7 1.116l4.248 -3.204q0.288 -0.252 0.72 -0.252 0.396 0 0.756 0.288 5.184 4.788 5.184 5.76 0 0.324 -0.252 0.684 -0.432 0.576 -1.512 1.944t-1.62 2.16q0.828 1.728 1.224 2.952l5.472 0.828q0.36 0.072 0.612 0.378t0.252 0.702v6.66q0 0.36 -0.252 0.702t-0.576 0.378l-5.58 0.864q-0.396 1.26 -1.152 2.736 1.224 1.728 3.24 4.14 0.252 0.36 0.252 0.72 0 0.432 -0.252 0.684 -0.828 1.08 -2.97 3.222t-2.826 2.142q-0.396 0 -0.756 -0.252l-4.14 -3.24q-1.332 0.684 -2.772 1.116 -0.396 3.888 -0.828 5.58 -0.252 0.864 -1.08 0.864h-6.696q-0.396 0 -0.72 -0.27t-0.36 -0.63l-0.828 -5.508q-1.224 -0.36 -2.7 -1.116l-4.248 3.204q-0.252 0.252 -0.72 0.252 -0.396 0 -0.756 -0.288 -5.184 -4.788 -5.184 -5.76 0 -0.324 0.252 -0.684 0.36 -0.504 1.476 -1.908t1.692 -2.196q-0.828 -1.584 -1.26 -2.952l-5.472 -0.864q-0.36 -0.036 -0.612 -0.342t-0.252 -0.702zm13.824 -3.276q0 3.816 2.7 6.516t6.516 2.7 6.516 -2.7 2.7 -6.516 -2.7 -6.516 -6.516 -2.7 -6.516 2.7 -2.7 6.516zm27.648 20.952v-5.04q0 -0.576 5.364 -1.116 0.468 -1.044 1.08 -1.872 -1.836 -4.068 -1.836 -4.968 0 -0.144 0.144 -0.252 0.144 -0.072 1.26 -0.72t2.124 -1.224 1.08 -0.576q0.288 0 1.656 1.674t1.872 2.43q0.72 -0.072 1.08 -0.072t1.08 0.072q1.836 -2.556 3.312 -4.032l0.216 -0.072q0.144 0 4.464 2.52 0.144 0.108 0.144 0.252 0 0.9 -1.836 4.968 0.612 0.828 1.08 1.872 5.364 0.54 5.364 1.116v5.04q0 0.576 -5.364 1.116 -0.432 0.972 -1.08 1.872 1.836 4.068 1.836 4.968 0 0.144 -0.144 0.252 -4.392 2.556 -4.464 2.556 -0.288 0 -1.656 -1.692t-1.872 -2.448q-0.72 0.072 -1.08 0.072t-1.08 -0.072q-0.504 0.756 -1.872 2.448t-1.656 1.692q-0.072 0 -4.464 -2.556 -0.144 -0.108 -0.144 -0.252 0 -0.9 1.836 -4.968 -0.648 -0.9 -1.08 -1.872 -5.364 -0.54 -5.364 -1.116zm0 -36.864v-5.04q0 -0.576 5.364 -1.116 0.468 -1.044 1.08 -1.872 -1.836 -4.068 -1.836 -4.968 0 -0.144 0.144 -0.252 0.144 -0.072 1.26 -0.72t2.124 -1.224 1.08 -0.576q0.288 0 1.656 1.674t1.872 2.43q0.72 -0.072 1.08 -0.072t1.08 0.072q1.836 -2.556 3.312 -4.032l0.216 -0.072q0.144 0 4.464 2.52 0.144 0.108 0.144 0.252 0 0.9 -1.836 4.968 0.612 0.828 1.08 1.872 5.364 0.54 5.364 1.116v5.04q0 0.576 -5.364 1.116 -0.432 0.972 -1.08 1.872 1.836 4.068 1.836 4.968 0 0.144 -0.144 0.252 -4.392 2.556 -4.464 2.556 -0.288 0 -1.656 -1.692t-1.872 -2.448q-0.72 0.072 -1.08 0.072t-1.08 -0.072q-0.504 0.756 -1.872 2.448t-1.656 1.692q-0.072 0 -4.464 -2.556 -0.144 -0.108 -0.144 -0.252 0 -0.9 1.836 -4.968 -0.648 -0.9 -1.08 -1.872 -5.364 -0.54 -5.364 -1.116zm9.216 34.344q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258q0 -1.872 -1.368 -3.24t-3.24 -1.368 -3.24 1.368 -1.368 3.24zm0 -36.864q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258q0 -1.872 -1.368 -3.24t-3.24 -1.368 -3.24 1.368 -1.368 3.24z","COMMENTS":"M0 -36.791q0 -5.004 3.384 -9.252t9.234 -6.714 12.726 -2.466 12.726 2.466 9.234 6.714 3.384 9.252 -3.384 9.252 -9.234 6.714 -12.726 2.466q-3.096 0 -6.336 -0.576 -4.464 3.168 -10.008 4.608 -1.296 0.324 -3.096 0.576h-0.108q-0.396 0 -0.738 -0.288t-0.414 -0.756q-0.036 -0.108 -0.036 -0.234t0.018 -0.234 0.072 -0.216l0.09 -0.18 0.126 -0.198 0.144 -0.18 0.162 -0.18 0.144 -0.162q0.18 -0.216 0.828 -0.9t0.936 -1.062 0.81 -1.044 0.9 -1.386 0.738 -1.584q-4.464 -2.592 -7.02 -6.372t-2.556 -8.064zm22.176 22.896q2.088 0.144 3.168 0.144 5.796 0 11.124 -1.62t9.504 -4.644q4.5 -3.312 6.912 -7.632t2.412 -9.144q0 -2.772 -0.828 -5.472 4.644 2.556 7.344 6.408t2.7 8.28q0 4.32 -2.556 8.082t-7.02 6.354q0.36 0.864 0.738 1.584t0.9 1.386 0.81 1.044 0.936 1.062 0.828 0.9l0.144 0.162 0.162 0.18 0.144 0.18 0.126 0.198 0.09 0.18 0.072 0.216 0.018 0.234 -0.036 0.234q-0.108 0.504 -0.468 0.792t-0.792 0.252q-1.8 -0.252 -3.096 -0.576 -5.544 -1.44 -10.008 -4.608 -3.24 0.576 -6.336 0.576 -9.756 0 -16.992 -4.752z","THUMBS_O_UP":"M0 -13.751v-23.04q0 -1.908 1.35 -3.258t3.258 -1.35h9.864q1.296 -0.864 4.932 -5.58 2.088 -2.7 3.852 -4.608 0.864 -0.9 1.278 -3.078t1.098 -4.554 2.232 -3.888q1.404 -1.332 3.24 -1.332 3.024 0 5.436 1.17t3.672 3.654 1.26 6.696q0 3.348 -1.728 6.912h6.336q3.744 0 6.48 2.736t2.736 6.444q0 3.204 -1.764 5.868 0.324 1.188 0.324 2.484 0 2.772 -1.368 5.184 0.108 0.756 0.108 1.548 0 3.636 -2.16 6.408 0.036 5.004 -3.06 7.902t-8.172 2.898h-4.644q-3.456 0 -6.822 -0.81t-7.794 -2.358q-4.176 -1.44 -4.968 -1.44h-10.368q-1.908 0 -3.258 -1.35t-1.35 -3.258zm4.608 -2.304q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62zm9.216 2.304h1.152q0.468 0 1.134 0.108t1.188 0.234 1.368 0.396 1.26 0.414 1.278 0.45 1.044 0.378q7.596 2.628 12.312 2.628h4.356q6.912 0 6.912 -6.012 0 -0.936 -0.18 -2.016 1.08 -0.576 1.71 -1.89t0.63 -2.646 -0.648 -2.484q1.908 -1.8 1.908 -4.284 0 -0.9 -0.36 -1.998t-0.9 -1.71q1.152 -0.036 1.926 -1.692t0.774 -2.916q0 -1.836 -1.404 -3.222t-3.204 -1.386h-12.672q0 -2.088 1.728 -5.742t1.728 -5.778q0 -3.528 -1.152 -5.22t-4.608 -1.692q-0.936 0.936 -1.368 3.06t-1.098 4.518 -2.142 3.942q-0.792 0.828 -2.772 3.276 -0.144 0.18 -0.828 1.08t-1.134 1.476 -1.242 1.53 -1.44 1.584 -1.386 1.278 -1.44 0.972 -1.278 0.324h-1.152v23.04z","THUMBS_O_DOWN":"M0 -27.575v-23.04q0 -1.908 1.35 -3.258t3.258 -1.35h10.368q0.792 0 4.968 -1.44 4.608 -1.584 8.028 -2.376t7.2 -0.792h4.032q5.04 0 8.154 2.844t3.078 7.776v0.18q2.16 2.772 2.16 6.408 0 0.792 -0.108 1.548 1.368 2.412 1.368 5.184 0 1.296 -0.324 2.484 1.764 2.664 1.764 5.868 0 3.708 -2.736 6.444t-6.48 2.736h-6.336q1.728 3.564 1.728 6.912 0 4.248 -1.26 6.696 -1.26 2.484 -3.672 3.654t-5.436 1.17q-1.836 0 -3.24 -1.332 -1.224 -1.188 -1.944 -2.952t-0.918 -3.258 -0.63 -3.042 -1.116 -2.304q-1.728 -1.8 -3.852 -4.572 -3.636 -4.716 -4.932 -5.58h-9.864q-1.908 0 -3.258 -1.35t-1.35 -3.258zm4.608 -20.736q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62zm9.216 20.736h1.152q0.576 0 1.278 0.324t1.44 0.972 1.386 1.278 1.44 1.584 1.242 1.53 1.134 1.476 0.828 1.08q1.98 2.448 2.772 3.276 1.476 1.548 2.142 3.942t1.098 4.518 1.368 3.06q3.456 0 4.608 -1.692t1.152 -5.22q0 -2.124 -1.728 -5.778t-1.728 -5.742h12.672q1.8 0 3.204 -1.386t1.404 -3.222q0 -1.26 -0.774 -2.916t-1.926 -1.692q0.54 -0.612 0.9 -1.71t0.36 -1.998q0 -2.484 -1.908 -4.284 0.648 -1.152 0.648 -2.484t-0.63 -2.646 -1.71 -1.89q0.18 -1.08 0.18 -2.016 0 -3.06 -1.764 -4.536t-4.896 -1.476h-4.608q-4.716 0 -12.312 2.628 -0.18 0.072 -1.044 0.378t-1.278 0.45 -1.26 0.414 -1.368 0.396 -1.188 0.234 -1.134 0.108h-1.152v23.04z","STAR_HALF":"M0 -41.147q0 -1.332 2.016 -1.656l18.072 -2.628 8.1 -16.38q0.684 -1.476 1.764 -1.476v48.204l-16.164 8.496q-0.792 0.432 -1.44 0.432 -0.756 0 -1.134 -0.522t-0.378 -1.278q0 -0.216 0.072 -0.72l3.096 -18 -13.104 -12.744q-0.9 -0.972 -0.9 -1.728z","HEART_O":"M0 -42.983q0 -7.92 4.572 -12.384t12.636 -4.464q2.232 0 4.554 0.774t4.32 2.088 3.438 2.466 2.736 2.448q1.296 -1.296 2.736 -2.448t3.438 -2.466 4.32 -2.088 4.554 -0.774q8.064 0 12.636 4.464t4.572 12.384q0 7.956 -8.244 16.2l-22.428 21.6q-0.648 0.648 -1.584 0.648t-1.584 -0.648l-22.464 -21.672q-0.36 -0.288 -0.99 -0.936t-1.998 -2.358 -2.448 -3.51 -1.926 -4.356 -0.846 -4.968zm4.608 0q0 6.048 6.732 12.78l20.916 20.16 20.88 -20.124q6.768 -6.768 6.768 -12.816 0 -2.916 -0.774 -5.148t-1.98 -3.546 -2.934 -2.142 -3.384 -1.116 -3.528 -0.288 -4.032 0.918 -3.978 2.304 -3.114 2.592 -2.16 2.214q-0.648 0.792 -1.764 0.792t-1.764 -0.792q-0.864 -1.008 -2.16 -2.214t-3.114 -2.592 -3.978 -2.304 -4.032 -0.918 -3.528 0.288 -3.384 1.116 -2.934 2.142 -1.98 3.546 -0.774 5.148z","SIGN_OUT":"M0 -19.511v-25.344q0 -4.284 3.042 -7.326t7.326 -3.042h11.52q0.468 0 0.81 0.342t0.342 0.81q0 0.144 0.036 0.72t0.018 0.954 -0.108 0.846 -0.36 0.702 -0.738 0.234h-11.52q-2.376 0 -4.068 1.692t-1.692 4.068v25.344q0 2.376 1.692 4.068t4.068 1.692h11.232000000000001l0.414 0.036 0.414 0.108 0.288 0.198 0.252 0.324 0.072 0.486q0 0.144 0.036 0.72t0.018 0.954 -0.108 0.846 -0.36 0.702 -0.738 0.234h-11.52q-4.284 0 -7.326 -3.042t-3.042 -7.326zm13.824 -5.76v-13.824q0 -0.936 0.684 -1.62t1.62 -0.684h16.128v-10.368q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684l19.584 19.584q0.684 0.684 0.684 1.62t-0.684 1.62l-19.584 19.584q-0.684 0.684 -1.62 0.684t-1.62 -0.684 -0.684 -1.62v-10.368h-16.128q-0.936 0 -1.62 -0.684t-0.684 -1.62z","LINKEDIN_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm8.028 -31.32q0 1.836 1.278 3.078t3.33 1.242h0.036q2.124 0 3.42 -1.242t1.296 -3.078q-0.036 -1.872 -1.296 -3.096t-3.348 -1.224 -3.402 1.224 -1.314 3.096zm0.504 32.688h8.316v-24.984h-8.316v24.984zm12.888 0h8.316v-13.968q0 -1.368 0.252 -2.016 0.54 -1.26 1.62 -2.142t2.664 -0.882q4.176 0 4.176 5.652v13.356h8.316v-14.328q0 -5.544 -2.628 -8.388t-6.948 -2.844q-4.896 0 -7.524 4.212h0.072v-3.636h-8.316q0.108 2.376 0 24.984z","THUMB_TACK":"M0 -20.663q0 -4.428 2.826 -7.974t6.39 -3.546v-18.432q-1.872 0 -3.24 -1.368t-1.368 -3.24 1.368 -3.24 3.24 -1.368h23.04q1.872 0 3.24 1.368t1.368 3.24 -1.368 3.24 -3.24 1.368v18.432q3.564 0 6.39 3.546t2.826 7.974q0 0.936 -0.684 1.62t-1.62 0.684h-15.444l-1.836 17.388q-0.072 0.432 -0.378 0.738t-0.738 0.306h-0.036q-0.972 0 -1.152 -0.972l-2.736 -17.46h-14.544q-0.936 0 -1.62 -0.684t-0.684 -1.62zm14.976 -12.672q0 0.504 0.324 0.828t0.828 0.324 0.828 -0.324 0.324 -0.828v-16.128q0 -0.504 -0.324 -0.828t-0.828 -0.324 -0.828 0.324 -0.324 0.828v16.128z","EXTERNAL_LINK":"M0 -19.511v-29.952q0 -4.284 3.042 -7.326t7.326 -3.042h25.344q0.504 0 0.828 0.324t0.324 0.828v2.304q0 0.504 -0.324 0.828t-0.828 0.324h-25.344q-2.376 0 -4.068 1.692t-1.692 4.068v29.952q0 2.376 1.692 4.068t4.068 1.692h29.952q2.376 0 4.068 -1.692t1.692 -4.068v-11.52q0 -0.504 0.324 -0.828t0.828 -0.324h2.304q0.504 0 0.828 0.324t0.324 0.828v11.52q0 4.284 -3.042 7.326t-7.326 3.042h-29.952q-4.284 0 -7.326 -3.042t-3.042 -7.326zm24.66 -10.368q0 -0.468 0.36 -0.828l23.472 -23.472 -6.336 -6.336q-0.684 -0.684 -0.684 -1.62t0.684 -1.62 1.62 -0.684h18.432q0.936 0 1.62 0.684t0.684 1.62v18.432q0 0.936 -0.684 1.62t-1.62 0.684 -1.62 -0.684l-6.336 -6.336 -23.472 23.472q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-4.104 -4.104q-0.36 -0.36 -0.36 -0.828z","SIGN_IN":"M0 -25.271v-13.824q0 -0.936 0.684 -1.62t1.62 -0.684h16.128v-10.368q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684l19.584 19.584q0.684 0.684 0.684 1.62t-0.684 1.62l-19.584 19.584q-0.684 0.684 -1.62 0.684t-1.62 -0.684 -0.684 -1.62v-10.368h-16.128q-0.936 0 -1.62 -0.684t-0.684 -1.62zm32.202 13.302q0.018 -0.378 0.108 -0.846t0.36 -0.702 0.738 -0.234h11.52q2.376 0 4.068 -1.692t1.692 -4.068v-25.344q0 -2.376 -1.692 -4.068t-4.068 -1.692h-11.232000000000001l-0.414 -0.036 -0.414 -0.108 -0.288 -0.198 -0.252 -0.324 -0.072 -0.486q0 -0.144 -0.036 -0.72t-0.018 -0.954 0.108 -0.846 0.36 -0.702 0.738 -0.234h11.52q4.284 0 7.326 3.042t3.042 7.326v25.344q0 4.284 -3.042 7.326t-7.326 3.042h-11.52q-0.468 0 -0.81 -0.342t-0.342 -0.81q0 -0.144 -0.036 -0.72t-0.018 -0.954z","TROPHY":"M0 -42.551v-4.608q0 -1.44 1.008 -2.448t2.448 -1.008h10.368v-3.456q0 -2.376 1.692 -4.068t4.068 -1.692h20.736q2.376 0 4.068 1.692t1.692 4.068v3.456h10.368q1.44 0 2.448 1.008t1.008 2.448v4.608q0 2.556 -1.494 5.148t-4.032 4.68 -6.228 3.51 -7.758 1.602q-1.512 1.944 -3.42 3.42 -1.368 1.224 -1.89 2.61t-0.522 3.222q0 1.944 1.098 3.276t3.51 1.332q2.7 0 4.806 1.638t2.106 4.122v2.304q0 0.504 -0.324 0.828t-0.828 0.324h-29.952q-0.504 0 -0.828 -0.324t-0.324 -0.828v-2.304q0 -2.484 2.106 -4.122t4.806 -1.638q2.412 0 3.51 -1.332t1.098 -3.276q0 -1.836 -0.522 -3.222t-1.89 -2.61q-1.908 -1.476 -3.42 -3.42 -4.068 -0.18 -7.758 -1.602t-6.228 -3.51 -4.032 -4.68 -1.494 -5.148zm4.608 0q0 2.808 3.402 5.832t8.478 4.068q-2.664 -5.832 -2.664 -13.356h-9.216v3.456zm38.808 9.9q5.076 -1.044 8.478 -4.068t3.402 -5.832v-3.456h-9.216q0 7.524 -2.664 13.356z","GITHUB_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-8.064q-0.576 0 -0.882 -0.036t-0.702 -0.18 -0.576 -0.522 -0.18 -0.99v-8.604q0 -3.492 -1.872 -5.112 2.052 -0.216 3.69 -0.648t3.384 -1.404 2.916 -2.394 1.908 -3.78 0.738 -5.418q0 -4.356 -2.844 -7.416 1.332 -3.276 -0.288 -7.344 -1.008 -0.324 -2.916 0.396t-3.312 1.584l-1.368 0.864q-3.348 -0.936 -6.912 -0.936t-6.912 0.936q-0.576 -0.396 -1.53 -0.972t-3.006 -1.386 -3.096 -0.486q-1.584 4.068 -0.252 7.344 -2.844 3.06 -2.844 7.416 0 3.06 0.738 5.4t1.89 3.78 2.898 2.412 3.384 1.404 3.69 0.648q-1.44 1.296 -1.764 3.708 -0.756 0.36 -1.62 0.54t-2.052 0.18 -2.358 -0.774 -1.998 -2.25q-0.684 -1.152 -1.746 -1.872t-1.782 -0.864l-0.72 -0.108q-0.756 0 -1.044 0.162t-0.18 0.414 0.324 0.504 0.468 0.432l0.252 0.18q0.792 0.36 1.566 1.368t1.134 1.836l0.36 0.828q0.468 1.368 1.584 2.214t2.412 1.08 2.502 0.252 1.998 -0.126l0.828 -0.144q0 1.368 0.018 3.708t0.018 2.448q0 0.792 -0.396 1.206t-0.792 0.468 -1.188 0.054h-8.064q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.756 -5.58q0.108 -0.18 0.468 -0.072 0.36 0.18 0.252 0.432 -0.18 0.252 -0.468 0.072 -0.36 -0.18 -0.252 -0.432zm1.188 0.9q0.216 -0.216 0.576 0.108 0.324 0.396 0.072 0.576 -0.216 0.252 -0.576 -0.108 -0.324 -0.396 -0.072 -0.576zm1.116 2.052q-0.324 -0.468 0 -0.648 0.324 -0.252 0.612 0.216 0.324 0.432 0 0.684 -0.288 0.216 -0.612 -0.252zm1.26 0.972q0.288 -0.324 0.72 0.108 0.432 0.396 0.144 0.684 -0.288 0.324 -0.72 -0.108 -0.468 -0.396 -0.144 -0.684zm1.764 1.368q0.144 -0.396 0.684 -0.252 0.576 0.18 0.468 0.576 -0.144 0.432 -0.684 0.216 -0.612 -0.144 -0.468 -0.54zm2.232 0.504q0 -0.396 0.576 -0.396 0.612 -0.072 0.612 0.396 0 0.396 -0.576 0.396 -0.612 0.072 -0.612 -0.396zm2.124 -0.144q-0.072 -0.432 0.504 -0.54 0.576 -0.072 0.648 0.324 0.072 0.36 -0.504 0.504t-0.648 -0.288z","UPLOAD":"M0 -7.991v-11.52q0 -1.44 1.008 -2.448t2.448 -1.008h15.372q0.756 2.016 2.538 3.312t3.978 1.296h9.216q2.196 0 3.978 -1.296t2.538 -3.312h15.372q1.44 0 2.448 1.008t1.008 2.448v11.52q0 1.44 -1.008 2.448t-2.448 1.008h-52.992q-1.44 0 -2.448 -1.008t-1.008 -2.448zm11.7 -34.848q-0.612 -1.404 0.504 -2.484l16.128 -16.128q0.648 -0.684 1.62 -0.684t1.62 0.684l16.128 16.128q1.116 1.08 0.504 2.484 -0.612 1.44 -2.124 1.44h-9.216v16.128q0 0.936 -0.684 1.62t-1.62 0.684h-9.216q-0.936 0 -1.62 -0.684t-0.684 -1.62v-16.128h-9.216q-1.512 0 -2.124 -1.44zm29.772 31.392q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62zm9.216 0q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62z","LEMON_O":"M0 -24.731q0 -3.996 0.648 -7.83t1.962 -7.542 3.618 -6.984 5.4 -5.616q2.808 -2.124 8.352 -4.32 6.984 -2.808 11.376 -2.808 2.16 0 6.318 0.864t6.246 0.864q0.684 0 2.052 -0.18t2.088 -0.18q2.916 0 4.248 1.818t1.332 4.842q0 0.828 -0.18 2.448t-0.18 2.448q0 0.36 0.036 0.666t0.108 0.612 0.144 0.486 0.234 0.576 0.234 0.612q0.576 1.44 0.9 4.266t0.324 4.914q0 5.94 -2.52 11.79t-7.056 10.368 -10.116 6.498q-4.464 1.584 -11.736 1.584 -2.052 0 -6.12 -0.522t-6.084 -0.522q-0.864 0 -2.61 0.522t-2.646 0.522q-2.628 0 -4.446 -1.998t-1.818 -4.626q0 -0.864 0.396 -2.448t0.396 -2.412q0 -1.44 -0.45 -4.338t-0.45 -4.374zm4.608 -0.036q0 1.44 0.45 4.32t0.45 4.356q0 0.828 -0.396 2.394t-0.396 2.358 0.432 1.314 1.224 0.522q0.864 0 2.61 -0.396t2.646 -0.396q2.052 0 6.102 0.558t6.102 0.558q6.516 0 10.224 -1.296 4.644 -1.62 8.478 -5.49t5.976 -8.838 2.142 -9.9q0 -1.584 -0.252 -4.086t-0.648 -3.474q-0.432 -1.08 -0.612 -1.584t-0.324 -1.314 -0.144 -1.746q0 -0.828 0.18 -2.466t0.18 -2.43q0 -1.332 -0.36 -1.98 -0.144 -0.036 -0.468 -0.036 -0.684 0 -2.088 0.162t-2.124 0.162q-2.16 0 -6.336 -0.864t-6.3 -0.864q-1.548 0 -3.402 0.414t-3.06 0.846 -3.222 1.224q-4.932 1.944 -7.272 3.708 -3.456 2.628 -5.742 6.822t-3.168 8.496 -0.882 8.946z","PHONE":"M0 -47.627q0 -3.312 1.836 -6.696 2.016 -3.636 3.816 -4.392 0.9 -0.396 2.466 -0.756t2.538 -0.36q0.504 0 0.756 0.108 0.648 0.216 1.908 2.736 0.396 0.684 1.08 1.944t1.26 2.286 1.116 1.926q0.108 0.144 0.63 0.9t0.774 1.278 0.252 1.026q0 0.72 -1.026 1.8t-2.232 1.98 -2.232 1.908 -1.026 1.656q0 0.324 0.18 0.81t0.306 0.738 0.504 0.864 0.414 0.684q2.736 4.932 6.264 8.46t8.46 6.264q0.072 0.036 0.684 0.414t0.864 0.504 0.738 0.306 0.81 0.18q0.648 0 1.656 -1.026t1.908 -2.232 1.98 -2.232 1.8 -1.026q0.504 0 1.026 0.252t1.278 0.774 0.9 0.63q0.9 0.54 1.926 1.116t2.286 1.26 1.944 1.08q2.52 1.26 2.736 1.908 0.108 0.252 0.108 0.756 0 0.972 -0.36 2.538t-0.756 2.466q-0.756 1.8 -4.392 3.816 -3.384 1.836 -6.696 1.836 -0.972 0 -1.89 -0.126t-2.07 -0.45 -1.71 -0.522 -1.998 -0.738 -1.764 -0.648q-3.528 -1.26 -6.3 -2.988 -4.608 -2.844 -9.522 -7.758t-7.758 -9.522q-1.728 -2.772 -2.988 -6.3 -0.108 -0.324 -0.648 -1.764t-0.738 -1.998 -0.522 -1.71 -0.45 -2.07 -0.126 -1.89z","SQUARE_O":"M0 -19.511v-29.952q0 -4.284 3.042 -7.326t7.326 -3.042h29.952q4.284 0 7.326 3.042t3.042 7.326v29.952q0 4.284 -3.042 7.326t-7.326 3.042h-29.952q-4.284 0 -7.326 -3.042t-3.042 -7.326zm4.608 0q0 2.376 1.692 4.068t4.068 1.692h29.952q2.376 0 4.068 -1.692t1.692 -4.068v-29.952q0 -2.376 -1.692 -4.068t-4.068 -1.692h-29.952q-2.376 0 -4.068 1.692t-1.692 4.068v29.952z","BOOKMARK_O":"M0 -9.395v-46.404q0 -1.224 0.702 -2.232t1.89 -1.476q0.756 -0.324 1.584 -0.324h37.728q0.828 0 1.584 0.324 1.188 0.468 1.89 1.476t0.702 2.232v46.404q0 1.224 -0.702 2.232t-1.89 1.476q-0.684 0.288 -1.584 0.288 -1.728 0 -2.988 -1.152l-15.876 -15.264 -15.876 15.264q-1.296 1.188 -2.988 1.188 -0.828 0 -1.584 -0.324 -1.188 -0.468 -1.89 -1.476t-0.702 -2.232zm4.608 -1.116l15.228 -14.616 3.204 -3.06 3.204 3.06 15.228 14.616v-44.712h-36.864v44.712z","PHONE_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 -26.82q0 0.576 0.09 1.224t0.18 1.098 0.324 1.188 0.36 1.062 0.45 1.188 0.396 1.08q2.16 5.904 7.794 11.538t11.538 7.794q0.216 0.072 1.08 0.396t1.188 0.45 1.062 0.36 1.188 0.324 1.098 0.18 1.224 0.09q2.052 0 4.698 -1.224t3.402 -2.88q0.792 -1.908 0.792 -3.636 0 -0.396 -0.072 -0.576 -0.108 -0.288 -1.386 -1.062t-3.186 -1.782l-1.908 -1.044q-0.18 -0.108 -0.684 -0.468t-0.9 -0.54 -0.756 -0.18q-0.648 0 -1.692 1.17t-2.052 2.358 -1.584 1.188q-0.252 0 -0.594 -0.126t-0.558 -0.234 -0.612 -0.342 -0.504 -0.306q-3.564 -1.98 -6.138 -4.554t-4.554 -6.138q-0.072 -0.108 -0.306 -0.504t-0.342 -0.612 -0.234 -0.558 -0.126 -0.594q0 -0.468 0.738 -1.206t1.62 -1.386 1.62 -1.422 0.738 -1.314q0 -0.36 -0.18 -0.756t-0.54 -0.9 -0.468 -0.684q-0.108 -0.216 -0.54 -1.026t-0.9 -1.638 -0.954 -1.71 -0.9 -1.458 -0.594 -0.648 -0.576 -0.072q-1.728 0 -3.636 0.792 -1.656 0.756 -2.88 3.402t-1.224 4.698z","TWITTER":"M1.584 -14.363q1.26 0.144 2.808 0.144 8.1 0 14.436 -4.968 -3.78 -0.072 -6.768 -2.322t-4.104 -5.742q1.188 0.18 2.196 0.18 1.548 0 3.06 -0.396 -4.032 -0.828 -6.678 -4.014t-2.646 -7.398v-0.144q2.448 1.368 5.256 1.476 -2.376 -1.584 -3.78 -4.14t-1.404 -5.544q0 -3.168 1.584 -5.868 4.356 5.364 10.602 8.586t13.374 3.582q-0.288 -1.368 -0.288 -2.664 0 -4.824 3.402 -8.226t8.226 -3.402q5.04 0 8.496 3.672 3.924 -0.756 7.38 -2.808 -1.332 4.14 -5.112 6.408 3.348 -0.36 6.696 -1.8 -2.412 3.528 -5.832 6.012 0.036 0.504 0.036 1.512 0 4.68 -1.368 9.342t-4.158 8.946 -6.642 7.578 -9.288 5.256 -11.628 1.962q-9.756 0 -17.856 -5.22z","FACEBOOK":"M3.42 -31.859v-10.656h9.18v-7.848q0 -6.696 3.744 -10.386t9.972 -3.69q5.292 0 8.208 0.432v9.504h-5.652q-3.096 0 -4.176 1.296t-1.08 3.888v6.804h10.548l-1.404 10.656h-9.144v27.324h-11.016v-27.324h-9.18z","GITHUB":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878q0 9.036 -5.274 16.254t-13.626 9.99q-0.972 0.18 -1.422 -0.252t-0.45 -1.08v-7.596q0 -3.492 -1.872 -5.112 2.052 -0.216 3.69 -0.648t3.384 -1.404 2.916 -2.394 1.908 -3.78 0.738 -5.418q0 -4.356 -2.844 -7.416 1.332 -3.276 -0.288 -7.344 -1.008 -0.324 -2.916 0.396t-3.312 1.584l-1.368 0.864q-3.348 -0.936 -6.912 -0.936t-6.912 0.936q-0.576 -0.396 -1.53 -0.972t-3.006 -1.386 -3.096 -0.486q-1.584 4.068 -0.252 7.344 -2.844 3.06 -2.844 7.416 0 3.06 0.738 5.4t1.89 3.78 2.898 2.412 3.384 1.404 3.69 0.648q-1.44 1.296 -1.764 3.708 -0.756 0.36 -1.62 0.54t-2.052 0.18 -2.358 -0.774 -1.998 -2.25q-0.684 -1.152 -1.746 -1.872t-1.782 -0.864l-0.72 -0.108q-0.756 0 -1.044 0.162t-0.18 0.414 0.324 0.504 0.468 0.432l0.252 0.18q0.792 0.36 1.566 1.368t1.134 1.836l0.36 0.828q0.468 1.368 1.584 2.214t2.412 1.08 2.502 0.252 1.998 -0.126l0.828 -0.144q0 1.368 0.018 3.204t0.018 1.944q0 0.648 -0.468 1.08t-1.44 0.252q-8.352 -2.772 -13.626 -9.99t-5.274 -16.254z","UNLOCK":"M0 -12.599v-20.736q0 -1.44 1.008 -2.448t2.448 -1.008h24.192v-6.912q0 -6.66 4.734 -11.394t11.394 -4.734 11.394 4.734 4.734 11.394v9.216q0 0.936 -0.684 1.62t-1.62 0.684h-2.304q-0.936 0 -1.62 -0.684t-0.684 -1.62v-9.216q0 -3.816 -2.7 -6.516t-6.516 -2.7 -6.516 2.7 -2.7 6.516v6.912h3.456q1.44 0 2.448 1.008t1.008 2.448v20.736q0 1.44 -1.008 2.448t-2.448 1.008h-34.56q-1.44 0 -2.448 -1.008t-1.008 -2.448z","CREDIT_CARD":"M0 -10.295v-43.776q0 -2.376 1.692 -4.068t4.068 -1.692h57.6q2.376 0 4.068 1.692t1.692 4.068v43.776q0 2.376 -1.692 4.068t-4.068 1.692h-57.6q-2.376 0 -4.068 -1.692t-1.692 -4.068zm4.608 0q0 0.468 0.342 0.81t0.81 0.342h57.6q0.468 0 0.81 -0.342t0.342 -0.81v-21.888h-59.904v21.888zm0 -35.712h59.904v-8.064q0 -0.468 -0.342 -0.81t-0.81 -0.342h-57.6q-0.468 0 -0.81 0.342t-0.342 0.81v8.064zm4.608 32.256v-4.608h9.216v4.608h-9.216zm13.824 0v-4.608h13.824v4.608h-13.824z","RSS":"M0 -16.055q0 -2.88 2.016 -4.896t4.896 -2.016 4.896 2.016 2.016 4.896 -2.016 4.896 -4.896 2.016 -4.896 -2.016 -2.016 -4.896zm0 -18.18v-4.86q0 -1.044 0.756 -1.692 0.612 -0.612 1.548 -0.612h0.18q5.76 0.468 11.016 2.898t9.324 6.534q4.104 4.068 6.534 9.324t2.898 11.016q0.072 1.008 -0.612 1.728 -0.648 0.756 -1.692 0.756h-4.86q-0.9 0 -1.548 -0.594t-0.72 -1.494q-0.792 -8.244 -6.642 -14.094t-14.094 -6.642q-0.9 -0.072 -1.494 -0.72t-0.594 -1.548zm0 -18.144v-5.148q0 -1.008 0.72 -1.656 0.648 -0.648 1.584 -0.648h0.108q9.432 0.468 18.054 4.32t15.318 10.584q6.732 6.696 10.584 15.318t4.32 18.054q0.072 0.972 -0.648 1.692 -0.648 0.72 -1.656 0.72h-5.148q-0.936 0 -1.602 -0.63t-0.702 -1.53q-0.432 -7.74 -3.636 -14.706t-8.334 -12.096 -12.096 -8.334 -14.706 -3.672q-0.9 -0.036 -1.53 -0.702t-0.63 -1.566z","HDD_O":"M0 -14.903v-11.52q0 -0.9 0.576 -2.7l7.092 -21.816q0.612 -1.908 2.268 -3.096t3.636 -1.188h28.152q1.98 0 3.636 1.188t2.268 3.096l7.092 21.816q0.576 1.8 0.576 2.7v11.52q0 2.376 -1.692 4.068t-4.068 1.692h-43.776q-2.376 0 -4.068 -1.692t-1.692 -4.068zm4.608 0q0 0.468 0.342 0.81t0.81 0.342h43.776q0.468 0 0.81 -0.342t0.342 -0.81v-11.52q0 -0.468 -0.342 -0.81t-0.81 -0.342h-43.776q-0.468 0 -0.81 0.342t-0.342 0.81v11.52zm1.8 -17.28h42.48l-5.652 -17.352q-0.144 -0.468 -0.576 -0.774t-0.936 -0.306h-28.152q-0.504 0 -0.936 0.306t-0.576 0.774zm25.272 11.52q0 -1.188 0.846 -2.034t2.034 -0.846 2.034 0.846 0.846 2.034 -0.846 2.034 -2.034 0.846 -2.034 -0.846 -0.846 -2.034zm9.216 0q0 -1.188 0.846 -2.034t2.034 -0.846 2.034 0.846 0.846 2.034 -0.846 2.034 -2.034 0.846 -2.034 -0.846 -0.846 -2.034z","BULLHORN":"M0 -33.335v-6.912q0 -2.376 1.692 -4.068t4.068 -1.692h17.28q15.66 0 32.256 -13.824 1.872 0 3.24 1.368t1.368 3.24v13.824q1.908 0 3.258 1.35t1.35 3.258 -1.35 3.258 -3.258 1.35v13.824q0 1.872 -1.368 3.24t-3.24 1.368q-15.012 -12.492 -29.232 -13.68 -2.088 0.684 -3.276 2.376t-1.116 3.618 1.44 3.33q-0.72 1.188 -0.828 2.358t0.216 2.088 1.206 1.98 1.728 1.8 2.214 1.818q-1.044 2.088 -4.014 2.988t-6.066 0.414 -4.752 -1.998q-0.252 -0.828 -1.062 -3.15t-1.152 -3.402 -0.828 -3.204 -0.54 -3.636 0.126 -3.546 0.792 -3.978h-4.392q-2.376 0 -4.068 -1.692t-1.692 -4.068zm27.648 1.404q13.572 1.512 27.648 12.276v-34.344q-14.184 10.872 -27.648 12.348v9.72z","BELL_O":"M0 -13.751q6.84 -5.796 10.332 -14.31t3.492 -17.946q0 -5.94 3.456 -9.432t9.504 -4.212q-0.288 -0.648 -0.288 -1.332 0 -1.44 1.008 -2.448t2.448 -1.008 2.448 1.008 1.008 2.448q0 0.684 -0.288 1.332 6.048 0.72 9.504 4.212t3.456 9.432q0 9.432 3.492 17.946t10.332 14.31q0 1.872 -1.368 3.24t-3.24 1.368h-16.128q0 3.816 -2.7 6.516t-6.516 2.7 -6.516 -2.7 -2.7 -6.516h-16.128q-1.872 0 -3.24 -1.368t-1.368 -3.24zm6.588 0h46.728q-5.904 -6.516 -8.874 -14.814t-2.97 -17.442q0 -9.216 -11.52 -9.216t-11.52 9.216q0 9.144 -2.97 17.442t-8.874 14.814zm17.028 4.608q0 2.628 1.854 4.482t4.482 1.854q0.576 0 0.576 -0.576t-0.576 -0.576q-2.124 0 -3.654 -1.53t-1.53 -3.654q0 -0.576 -0.576 -0.576t-0.576 0.576z","CERTIFICATE":"M0.072 -24.803q-0.36 -1.512 0.72 -2.52l4.968 -4.86 -4.968 -4.86q-1.08 -1.008 -0.72 -2.52 0.432 -1.476 1.872 -1.836l6.768 -1.728 -1.908 -6.696q-0.432 -1.476 0.684 -2.52 1.044 -1.116 2.52 -0.684l6.696 1.908 1.728 -6.768q0.36 -1.476 1.836 -1.836 1.476 -0.432 2.52 0.684l4.86 5.004 4.86 -5.004q1.044 -1.08 2.52 -0.684 1.476 0.36 1.836 1.836l1.728 6.768 6.696 -1.908q1.476 -0.432 2.52 0.684 1.116 1.044 0.684 2.52l-1.908 6.696 6.768 1.728q1.44 0.36 1.872 1.836 0.36 1.512 -0.72 2.52l-4.968 4.86 4.968 4.86q1.08 1.008 0.72 2.52 -0.432 1.476 -1.872 1.836l-6.768 1.728 1.908 6.696q0.432 1.476 -0.684 2.52 -1.044 1.116 -2.52 0.684l-6.696 -1.908 -1.728 6.768q-0.36 1.44 -1.836 1.872 -0.432 0.072 -0.684 0.072 -1.116 0 -1.836 -0.792l-4.86 -4.968 -4.86 4.968q-1.008 1.08 -2.52 0.72 -1.476 -0.396 -1.836 -1.872l-1.728 -6.768 -6.696 1.908q-1.476 0.432 -2.52 -0.684 -1.116 -1.044 -0.684 -2.52l1.908 -6.696 -6.768 -1.728q-1.44 -0.36 -1.872 -1.836z","HAND_O_RIGHT":"M0 -13.751v-23.04q0 -1.908 1.35 -3.258t3.258 -1.35h10.368q0.36 0 0.774 -0.162t0.846 -0.504 0.81 -0.648 0.864 -0.81 0.738 -0.774 0.684 -0.774 0.504 -0.612q2.34 -2.664 3.6 -4.644 0.468 -0.756 1.188 -2.232t1.332 -2.592 1.458 -2.268 1.98 -1.782 2.502 -0.63q4.5 0 7.434 2.412t2.934 6.804q0 2.448 -0.792 4.608h13.464q3.744 0 6.48 2.736t2.736 6.444q0 3.78 -2.718 6.516t-6.498 2.736h-6.084q-0.144 2.232 -1.332 4.284 0.108 0.756 0.108 1.548 0 3.636 -2.16 6.408 0.036 5.004 -3.06 7.902t-8.172 2.898q-4.788 0 -11.592 -2.484 -5.904 -2.124 -8.028 -2.124h-10.368q-1.908 0 -3.258 -1.35t-1.35 -3.258zm4.608 -2.304q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62zm9.216 2.304h1.152q2.592 0 6.012 1.152t6.966 2.304 6.462 1.152q6.804 0 6.804 -6.012 0 -0.936 -0.18 -2.016 1.08 -0.576 1.71 -1.89t0.63 -2.646 -0.648 -2.484q1.908 -1.8 1.908 -4.284 0 -0.9 -0.36 -1.998t-0.9 -1.71h11.916q1.872 0 3.24 -1.368t1.368 -3.24q0 -1.836 -1.404 -3.222t-3.204 -1.386h-20.736q0 -0.72 0.54 -1.746t1.188 -1.98 1.188 -2.448 0.54 -3.042q0 -2.412 -1.602 -3.51t-4.158 -1.098q-0.864 0 -3.24 5.004 -0.864 1.584 -1.332 2.34 -1.44 2.304 -4.032 5.22 -2.556 2.916 -3.636 3.816 -2.484 2.052 -5.04 2.052h-1.152v23.04z","HAND_O_LEFT":"M0 -36.827q0 -3.708 2.736 -6.444t6.48 -2.736h13.464q-0.792 -2.16 -0.792 -4.608 0 -4.392 2.934 -6.804t7.434 -2.412q1.368 0 2.502 0.63t1.98 1.782 1.458 2.268 1.332 2.592 1.188 2.232q1.26 1.98 3.6 4.644 0.072 0.108 0.504 0.612t0.684 0.774 0.738 0.774 0.864 0.81 0.81 0.648 0.846 0.504 0.774 0.162h10.368q1.908 0 3.258 1.35t1.35 3.258v23.04q0 1.908 -1.35 3.258t-3.258 1.35h-10.368q-2.124 0 -8.028 2.124 -6.84 2.484 -11.412 2.484 -5.112 0 -8.28 -2.79t-3.132 -7.83l0.036 -0.18q-2.196 -2.736 -2.196 -6.408 0 -0.792 0.108 -1.548 -1.188 -2.052 -1.332 -4.284h-6.084q-3.78 0 -6.498 -2.736t-2.718 -6.516zm4.608 0.036q0 1.872 1.368 3.24t3.24 1.368h11.916q-0.54 0.612 -0.9 1.71t-0.36 1.998q0 2.484 1.908 4.284 -0.648 1.152 -0.648 2.484t0.63 2.646 1.71 1.89q-0.144 0.864 -0.144 2.016 0 3.06 1.746 4.536t4.878 1.476q3.024 0 6.588 -1.152t6.984 -2.304 6.012 -1.152h1.152v-23.04h-1.152q-1.26 0 -2.43 -0.432t-2.25 -1.332 -1.8 -1.656 -1.764 -1.944l-0.126 -0.162 -0.144 -0.162 -0.162 -0.18q-2.592 -2.916 -4.032 -5.22 -0.504 -0.792 -1.368 -2.448 -0.036 -0.108 -0.378 -0.81t-0.666 -1.296 -0.72 -1.278 -0.774 -1.098 -0.666 -0.414q-2.556 0 -4.158 1.098t-1.602 3.51q0 1.548 0.54 3.042t1.188 2.448 1.188 1.98 0.54 1.746h-20.736q-1.8 0 -3.204 1.386t-1.404 3.222zm50.688 20.736q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62z","HAND_O_UP":"M0 -32.183q0 -4.5 2.412 -7.434t6.804 -2.934q2.448 0 4.608 0.792v-13.464q0 -3.744 2.736 -6.48t6.444 -2.736q3.78 0 6.516 2.718t2.736 6.498v6.084q2.232 0.144 4.284 1.332 0.756 -0.108 1.548 -0.108 3.636 0 6.408 2.16 5.004 -0.036 7.902 3.06t2.898 8.172q0 4.788 -2.484 11.592 -2.124 5.904 -2.124 8.028v10.368q0 1.908 -1.35 3.258t-3.258 1.35h-23.04q-1.908 0 -3.258 -1.35t-1.35 -3.258v-10.368q0 -0.36 -0.162 -0.774t-0.504 -0.846 -0.648 -0.81 -0.81 -0.864 -0.774 -0.738 -0.774 -0.684 -0.612 -0.504q-2.664 -2.34 -4.644 -3.6 -0.756 -0.468 -2.232 -1.188t-2.592 -1.332 -2.268 -1.458 -1.782 -1.98 -0.63 -2.502zm4.608 0q0 0.864 5.004 3.24 1.584 0.864 2.34 1.332 2.304 1.44 5.22 4.032 2.916 2.556 3.816 3.636 2.052 2.484 2.052 5.04v1.152h23.04v-1.152q0 -2.592 1.152 -6.012t2.304 -6.966 1.152 -6.462q0 -6.804 -6.012 -6.804 -0.936 0 -2.016 0.18 -0.576 -1.08 -1.89 -1.71t-2.646 -0.63 -2.484 0.648q-1.8 -1.908 -4.284 -1.908 -0.9 0 -1.998 0.36t-1.71 0.9v-11.916q0 -1.872 -1.368 -3.24t-3.24 -1.368q-1.836 0 -3.222 1.404t-1.386 3.204v20.736q-0.72 0 -1.746 -0.54t-1.98 -1.188 -2.448 -1.188 -3.042 -0.54q-2.412 0 -3.51 1.602t-1.098 4.158zm36.864 25.344q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62z","HAND_O_DOWN":"M0 -32.183q0 -1.368 0.63 -2.502t1.782 -1.98 2.268 -1.458 2.592 -1.332 2.232 -1.188q1.98 -1.26 4.644 -3.6 0.108 -0.072 0.612 -0.504t0.774 -0.684 0.774 -0.738 0.81 -0.864 0.648 -0.81 0.504 -0.846 0.162 -0.774v-10.368q0 -1.908 1.35 -3.258t3.258 -1.35h23.04q1.908 0 3.258 1.35t1.35 3.258v10.368q0 2.124 2.124 8.028 2.484 6.84 2.484 11.412 0 5.112 -2.79 8.28t-7.83 3.132l-0.18 -0.036q-2.736 2.196 -6.408 2.196 -0.792 0 -1.548 -0.108 -1.944 1.08 -4.284 1.332v6.084q0 3.78 -2.736 6.498t-6.516 2.718q-3.708 0 -6.444 -2.736t-2.736 -6.48v-13.464q-1.944 0.792 -4.608 0.792 -4.356 0 -6.786 -2.934t-2.43 -7.434zm4.608 0q0 2.556 1.098 4.158t3.51 1.602q1.548 0 3.042 -0.54t2.448 -1.188 1.98 -1.188 1.746 -0.54v20.736q0 1.8 1.386 3.204t3.222 1.404q1.872 0 3.24 -1.368t1.368 -3.24v-11.916q1.656 1.26 3.708 1.26 2.484 0 4.284 -1.908 1.152 0.648 2.484 0.648t2.646 -0.63 1.89 -1.71q0.864 0.144 2.016 0.144 3.06 0 4.536 -1.746t1.476 -4.878q0 -3.024 -1.152 -6.588t-2.304 -6.984 -1.152 -6.012v-1.152h-23.04v1.152q0 1.26 -0.432 2.43t-1.332 2.25 -1.656 1.8 -1.944 1.764q-0.324 0.288 -0.504 0.432 -2.916 2.592 -5.22 4.032 -0.792 0.504 -2.448 1.368 -0.108 0.036 -0.81 0.378t-1.296 0.666 -1.278 0.72 -1.098 0.774 -0.414 0.666zm36.864 -25.344q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62z","ARROW_CIRCLE_LEFT":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm9.036 0q0 0.972 0.648 1.62l3.276 3.276 13.032 13.032q0.648 0.648 1.62 0.648t1.62 -0.648l3.276 -3.276q0.648 -0.648 0.648 -1.62t-0.648 -1.62l-6.804 -6.804h18.072q0.936 0 1.62 -0.684t0.684 -1.62v-4.608q0 -0.936 -0.684 -1.62t-1.62 -0.684h-18.072l6.804 -6.804q0.684 -0.684 0.684 -1.62t-0.684 -1.62l-3.276 -3.276q-0.648 -0.648 -1.62 -0.648t-1.62 0.648l-13.032 13.032 -3.276 3.276q-0.648 0.648 -0.648 1.62z","ARROW_CIRCLE_RIGHT":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm9.216 2.304q0 0.936 0.684 1.62t1.62 0.684h18.072l-6.804 6.804q-0.684 0.684 -0.684 1.62t0.684 1.62l3.276 3.276q0.648 0.648 1.62 0.648t1.62 -0.648l13.032 -13.032 3.276 -3.276q0.648 -0.648 0.648 -1.62t-0.648 -1.62l-3.276 -3.276 -13.032 -13.032q-0.648 -0.648 -1.62 -0.648t-1.62 0.648l-3.276 3.276q-0.648 0.648 -0.648 1.62t0.648 1.62l6.804 6.804h-18.072q-0.936 0 -1.62 0.684t-0.684 1.62v4.608z","ARROW_CIRCLE_UP":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm9.072 -0.036q0 0.972 0.648 1.62l3.276 3.276q0.648 0.648 1.62 0.648t1.62 -0.648l6.804 -6.804v18.072q0 0.936 0.684 1.62t1.62 0.684h4.608q0.936 0 1.62 -0.684t0.684 -1.62v-18.072l6.804 6.804q0.684 0.684 1.62 0.684t1.62 -0.684l3.276 -3.276q0.648 -0.648 0.648 -1.62t-0.648 -1.62l-13.032 -13.032 -3.276 -3.276q-0.648 -0.648 -1.62 -0.648t-1.62 0.648l-3.276 3.276 -13.032 13.032q-0.648 0.648 -0.648 1.62z","ARROW_CIRCLE_DOWN":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm9.072 0.036q0 0.972 0.648 1.62l13.032 13.032 3.276 3.276q0.648 0.648 1.62 0.648t1.62 -0.648l3.276 -3.276 13.032 -13.032q0.648 -0.648 0.648 -1.62t-0.648 -1.62l-3.276 -3.276q-0.648 -0.648 -1.62 -0.648t-1.62 0.648l-6.804 6.804v-18.072q0 -0.936 -0.684 -1.62t-1.62 -0.684h-4.608q-0.936 0 -1.62 0.684t-0.684 1.62v18.072l-6.804 -6.804q-0.684 -0.684 -1.62 -0.684t-1.62 0.684l-3.276 3.276q-0.648 0.648 -0.648 1.62z","GLOBE":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm8.136 -12.204q0.252 0.252 0.432 0.288 0.144 0.036 0.18 0.324t0.09 0.396 0.414 -0.108q0.324 0.288 0.108 0.684 0.036 -0.036 1.584 0.972 0.684 0.612 0.756 0.756 0.108 0.396 -0.36 0.648 -0.036 -0.072 -0.324 -0.324t-0.324 -0.144q-0.108 0.18 0.018 0.666t0.378 0.45q-0.252 0 -0.342 0.576t-0.09 1.278 -0.036 0.846l0.072 0.036q-0.108 0.432 0.198 1.242t0.774 0.702q-0.468 0.108 0.72 1.548 0.216 0.288 0.288 0.324 0.108 0.072 0.432 0.27t0.54 0.36 0.36 0.378q0.144 0.18 0.36 0.81t0.504 0.846q-0.072 0.216 0.342 0.72t0.378 0.828l-0.09 0.036 -0.09 0.036q0.108 0.252 0.558 0.504t0.558 0.468q0.036 0.108 0.072 0.36t0.108 0.396 0.288 0.072q0.072 -0.72 -0.864 -2.232 -0.54 -0.9 -0.612 -1.044 -0.108 -0.18 -0.198 -0.558t-0.162 -0.522q0.072 0 0.216 0.054t0.306 0.126 0.27 0.144 0.072 0.108q-0.108 0.252 0.072 0.63t0.432 0.666 0.612 0.684 0.432 0.468q0.216 0.216 0.504 0.702t0 0.486q0.324 0 0.72 0.36t0.612 0.72q0.18 0.288 0.288 0.936t0.18 0.864q0.072 0.252 0.306 0.486t0.45 0.342l0.576 0.288 0.468 0.252q0.18 0.072 0.666 0.378t0.774 0.414q0.36 0.144 0.576 0.144t0.522 -0.09 0.486 -0.126q0.54 -0.072 1.044 0.54t0.756 0.756q1.296 0.684 1.98 0.396 -0.072 0.036 0.018 0.27t0.288 0.558 0.324 0.522 0.198 0.306q0.18 0.216 0.648 0.54t0.648 0.54q0.216 -0.144 0.252 -0.324 -0.108 0.288 0.252 0.72t0.648 0.36q0.504 -0.108 0.504 -1.152 -1.116 0.54 -1.764 -0.648l-0.09 -0.198 -0.144 -0.306 -0.09 -0.306v-0.27l0.18 -0.108q0.324 0 0.36 -0.126t-0.072 -0.45 -0.144 -0.468q-0.036 -0.288 -0.396 -0.72t-0.432 -0.54q-0.18 0.324 -0.576 0.288t-0.576 -0.324l-0.054 0.198 -0.054 0.234q-0.468 0 -0.54 -0.036 0.036 -0.108 0.09 -0.63t0.126 -0.81q0.036 -0.144 0.198 -0.432t0.27 -0.522 0.144 -0.45 -0.162 -0.342 -0.63 -0.09q-0.684 0.036 -0.936 0.72 -0.036 0.108 -0.108 0.378t-0.18 0.414 -0.324 0.252q-0.252 0.108 -0.864 0.072t-0.864 -0.18q-0.468 -0.288 -0.81 -1.044t-0.342 -1.332q0 -0.36 0.09 -0.954t0.108 -0.9 -0.198 -0.882q0.108 -0.072 0.324 -0.342t0.36 -0.378l0.162 -0.054h0.162l0.144 -0.054 0.108 -0.216 -0.144 -0.108 -0.144 -0.108q0.252 0.108 1.026 -0.054t0.99 0.054q0.54 0.396 0.792 -0.072 0 -0.036 -0.09 -0.342t-0.018 -0.486q0.18 0.972 1.044 0.324 0.108 0.108 0.558 0.18t0.63 0.18l0.252 0.198 0.198 0.162 0.18 -0.018 0.306 -0.234q0.36 0.504 0.432 0.864 0.396 1.44 0.684 1.584 0.252 0.108 0.396 0.072t0.162 -0.342 0 -0.504 -0.054 -0.45l-0.036 -0.288v-0.648l-0.036 -0.288q-0.54 -0.108 -0.666 -0.432t0.054 -0.666 0.54 -0.666q0.036 -0.036 0.288 -0.126t0.558 -0.234 0.45 -0.288q0.756 -0.684 0.54 -1.26 0.252 0 0.396 -0.324l-0.18 -0.108 -0.27 -0.18 -0.162 -0.072q0.324 -0.18 0.072 -0.576 0.18 -0.108 0.27 -0.396t0.27 -0.36q0.324 0.432 0.756 0.072 0.252 -0.288 0.036 -0.576 0.18 -0.252 0.738 -0.378t0.666 -0.342q0.252 0.072 0.288 -0.072t0.036 -0.432 0.108 -0.432q0.144 -0.18 0.54 -0.324t0.468 -0.18l0.612 -0.396q0.108 -0.144 0 -0.144 0.648 0.072 1.116 -0.396 0.36 -0.396 -0.216 -0.72 0.108 -0.216 -0.108 -0.342t-0.54 -0.198q0.108 -0.036 0.414 -0.018t0.378 -0.054q0.54 -0.36 -0.252 -0.576 -0.612 -0.18 -1.548 0.432 -0.072 0.036 -0.342 0.342t-0.486 0.342q0.072 0 0.162 -0.18t0.18 -0.396 0.126 -0.252q0.216 -0.252 0.792 -0.54 0.504 -0.216 1.872 -0.432 1.224 -0.288 1.836 0.396 -0.072 -0.072 0.342 -0.468t0.522 -0.432q0.108 -0.072 0.54 -0.162t0.54 -0.27l0.072 -0.792q-0.432 0.036 -0.63 -0.252t-0.234 -0.756q0 0.072 -0.216 0.288 0 -0.252 -0.162 -0.288t-0.414 0.036 -0.324 0.036q-0.36 -0.108 -0.54 -0.27t-0.288 -0.594 -0.144 -0.54q-0.072 -0.18 -0.342 -0.378t-0.342 -0.378l-0.09 -0.198 -0.108 -0.234 -0.144 -0.198 -0.198 -0.09 -0.252 0.18 -0.27 0.36 -0.162 0.18q-0.108 -0.072 -0.216 -0.054t-0.162 0.036 -0.162 0.108 -0.18 0.126q-0.108 0.072 -0.306 0.108t-0.306 0.072q0.54 -0.18 -0.036 -0.396 -0.36 -0.144 -0.576 -0.108 0.324 -0.144 0.27 -0.432t-0.306 -0.504h0.18q-0.036 -0.144 -0.306 -0.306t-0.63 -0.306 -0.468 -0.216q-0.288 -0.18 -1.224 -0.342t-1.188 -0.018q-0.18 0.216 -0.162 0.378t0.144 0.504 0.126 0.45q0.036 0.216 -0.198 0.468t-0.234 0.432q0 0.252 0.504 0.558t0.36 0.774q-0.108 0.288 -0.576 0.576t-0.576 0.432q-0.18 0.288 -0.054 0.666t0.378 0.594q0.072 0.072 0.054 0.144t-0.126 0.162 -0.198 0.144 -0.234 0.126l-0.108 0.072q-0.396 0.18 -0.738 -0.216t-0.486 -0.936q-0.252 -0.9 -0.576 -1.08 -0.828 -0.288 -1.044 0.036 -0.18 -0.468 -1.476 -0.936 -0.9 -0.324 -2.088 -0.144 0.216 -0.036 0 -0.54 -0.252 -0.54 -0.684 -0.432 0.108 -0.216 0.144 -0.63t0.036 -0.486q0.108 -0.468 0.432 -0.828 0.036 -0.036 0.252 -0.306t0.342 -0.486 0.018 -0.216q1.26 0.144 1.8 -0.396 0.18 -0.18 0.414 -0.612t0.378 -0.612q0.324 -0.216 0.504 -0.198t0.522 0.198 0.522 0.18q0.504 0.036 0.558 -0.396t-0.27 -0.72q0.432 0.036 0.108 -0.612 -0.18 -0.252 -0.288 -0.324 -0.432 -0.144 -0.972 0.18 -0.288 0.144 0.072 0.288 -0.036 -0.036 -0.342 0.378t-0.594 0.63 -0.576 -0.18q-0.036 -0.036 -0.198 -0.486t-0.342 -0.486q-0.288 0 -0.576 0.54 0.108 -0.288 -0.396 -0.54t-0.864 -0.288q0.684 -0.432 -0.288 -0.972 -0.252 -0.144 -0.738 -0.18t-0.702 0.144q-0.18 0.252 -0.198 0.414t0.18 0.288 0.378 0.198 0.414 0.144 0.306 0.108q0.504 0.36 0.288 0.504l-0.306 0.126 -0.414 0.162 -0.216 0.144q-0.108 0.144 0 0.504t-0.072 0.504q-0.18 -0.18 -0.324 -0.63t-0.252 -0.594q0.252 0.324 -0.9 0.216l-0.36 -0.036q-0.144 0 -0.576 0.072t-0.738 0.036 -0.486 -0.288q-0.144 -0.288 0 -0.72 0.036 -0.144 0.144 -0.072 -0.144 -0.108 -0.396 -0.342t-0.36 -0.306q-1.656 0.54 -3.384 1.476 0.216 0.036 0.432 -0.036 0.18 -0.072 0.468 -0.234t0.36 -0.198q1.224 -0.504 1.512 -0.252l0.18 -0.18q0.504 0.576 0.72 0.9 -0.252 -0.144 -1.08 -0.036 -0.72 0.216 -0.792 0.432 0.252 0.432 0.18 0.648 -0.144 -0.108 -0.414 -0.36t-0.522 -0.396 -0.54 -0.18q-0.576 0 -0.792 0.036 -5.256 2.88 -8.46 7.992zm23.436 34.308q0 0.216 0.072 0.576 7.416 -1.296 12.636 -6.804 -0.108 -0.108 -0.45 -0.162t-0.45 -0.126q-0.648 -0.252 -0.864 -0.288 0.036 -0.252 -0.09 -0.468t-0.288 -0.324 -0.45 -0.288 -0.396 -0.252l-0.252 -0.216 -0.252 -0.198 -0.27 -0.162 -0.306 -0.072 -0.36 0.036 -0.108 0.036 -0.198 0.09 -0.198 0.108 -0.144 0.108v0.09q-0.756 -0.612 -1.296 -0.792 -0.18 -0.036 -0.396 -0.198t-0.378 -0.252 -0.36 -0.054 -0.414 0.252q-0.18 0.18 -0.216 0.54t-0.072 0.468q-0.252 -0.18 0 -0.63t0.072 -0.666q-0.108 -0.216 -0.378 -0.162t-0.432 0.162 -0.414 0.306 -0.324 0.234 -0.306 0.198 -0.306 0.27q-0.108 0.144 -0.216 0.432t-0.18 0.396q-0.072 -0.144 -0.414 -0.234t-0.342 -0.198q0.072 0.36 0.144 1.26t0.18 1.368q0.252 1.116 -0.432 1.728 -0.972 0.9 -1.044 1.44 -0.144 0.792 0.432 0.936 0 0.252 -0.288 0.738t-0.252 0.774z","WRENCH":"M0.756 -9.143q0 -1.908 1.368 -3.276l24.516 -24.516q1.404 3.528 4.122 6.246t6.246 4.122l-24.552 24.552q-1.332 1.332 -3.24 1.332 -1.872 0 -3.276 -1.332l-3.816 -3.888q-1.368 -1.296 -1.368 -3.24zm8.46 -2.304q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62zm18.432 -32.256q0 -6.66 4.734 -11.394t11.394 -4.734q2.088 0 4.374 0.594t3.87 1.674q0.576 0.396 0.576 1.008t-0.576 1.008l-10.548 6.084v8.064l6.948 3.852q0.18 -0.108 2.844 -1.746t4.878 -2.916 2.538 -1.278q0.54 0 0.846 0.36t0.306 0.9q0 1.404 -0.828 3.816 -1.692 4.824 -5.922 7.83t-9.306 3.006q-6.66 0 -11.394 -4.734t-4.734 -11.394z","TASKS":"M0 -11.447v-9.216q0 -0.936 0.684 -1.62t1.62 -0.684h59.904q0.936 0 1.62 0.684t0.684 1.62v9.216q0 0.936 -0.684 1.62t-1.62 0.684h-59.904q-0.936 0 -1.62 -0.684t-0.684 -1.62zm0 -18.432v-9.216q0 -0.936 0.684 -1.62t1.62 -0.684h59.904q0.936 0 1.62 0.684t0.684 1.62v9.216q0 0.936 -0.684 1.62t-1.62 0.684h-59.904q-0.936 0 -1.62 -0.684t-0.684 -1.62zm0 -18.432v-9.216q0 -0.936 0.684 -1.62t1.62 -0.684h59.904q0.936 0 1.62 0.684t0.684 1.62v9.216q0 0.936 -0.684 1.62t-1.62 0.684h-59.904q-0.936 0 -1.62 -0.684t-0.684 -1.62zm23.04 16.128h36.864v-4.608h-36.864v4.608zm13.824 18.432h23.04v-4.608h-23.04v4.608zm9.216 -36.864h13.824v-4.608h-13.824v4.608z","FILTER":"M0.18 -53.819q0.612 -1.404 2.124 -1.404h46.08q1.512 0 2.124 1.404 0.612 1.476 -0.504 2.52l-17.748 17.748v26.712q0 1.512 -1.404 2.124 -0.468 0.18 -0.9 0.18 -0.972 0 -1.62 -0.684l-9.216 -9.216q-0.684 -0.684 -0.684 -1.62v-17.496l-17.748 -17.748q-1.116 -1.044 -0.504 -2.52z","BRIEFCASE":"M0 -14.903v-17.28h24.192v5.76q0 0.936 0.684 1.62t1.62 0.684h11.52q0.936 0 1.62 -0.684t0.684 -1.62v-5.76h24.192v17.28q0 2.376 -1.692 4.068t-4.068 1.692h-52.992q-2.376 0 -4.068 -1.692t-1.692 -4.068zm0 -20.736v-13.824q0 -2.376 1.692 -4.068t4.068 -1.692h12.672v-5.76q0 -1.44 1.008 -2.448t2.448 -1.008h20.736q1.44 0 2.448 1.008t1.008 2.448v5.76h12.672q2.376 0 4.068 1.692t1.692 4.068v13.824h-64.512zm23.04 -19.584h18.432v-4.608h-18.432v4.608zm4.608 27.648v-4.608h9.216v4.608h-9.216z","ARROWS_ALT":"M0 -6.839v-16.128q0 -1.512 1.44 -2.124 1.404 -0.612 2.484 0.504l5.184 5.184 12.78 -12.78 -12.78 -12.78 -5.184 5.184q-0.684 0.684 -1.62 0.684 -0.432 0 -0.864 -0.18 -1.44 -0.612 -1.44 -2.124v-16.128q0 -0.936 0.684 -1.62t1.62 -0.684h16.128q1.512 0 2.124 1.44 0.612 1.404 -0.504 2.484l-5.184 5.184 12.78 12.78 12.78 -12.78 -5.184 -5.184q-1.116 -1.08 -0.504 -2.484 0.612 -1.44 2.124 -1.44h16.128q0.936 0 1.62 0.684t0.684 1.62v16.128q0 1.512 -1.404 2.124 -0.468 0.18 -0.9 0.18 -0.936 0 -1.62 -0.684l-5.184 -5.184 -12.78 12.78 12.78 12.78 5.184 -5.184q1.044 -1.116 2.52 -0.504 1.404 0.612 1.404 2.124v16.128q0 0.936 -0.684 1.62t-1.62 0.684h-16.128q-1.512 0 -2.124 -1.44 -0.612 -1.404 0.504 -2.484l5.184 -5.184 -12.78 -12.78 -12.78 12.78 5.184 5.184q1.116 1.08 0.504 2.484 -0.612 1.44 -2.124 1.44h-16.128q-0.936 0 -1.62 -0.684t-0.684 -1.62z","USERS":"M0 -33.299q0 -12.708 4.464 -12.708 0.216 0 1.566 0.756t3.51 1.53 4.284 0.774q2.412 0 4.788 -0.828 -0.18 1.332 -0.18 2.376 0 5.004 2.916 9.216 -5.832 0.18 -9.54 4.608h-4.824q-2.952 0 -4.968 -1.458t-2.016 -4.266zm4.608 -21.924q0 -3.816 2.7 -6.516t6.516 -2.7 6.516 2.7 2.7 6.516 -2.7 6.516 -6.516 2.7 -6.516 -2.7 -2.7 -6.516zm4.608 45.972q0 -1.908 0.126 -3.726t0.504 -3.924 0.954 -3.906 1.548 -3.51 2.232 -2.916 3.078 -1.926 4.014 -0.72q0.36 0 1.548 0.774t2.628 1.728 3.852 1.728 4.86 0.774 4.86 -0.774 3.852 -1.728 2.628 -1.728 1.548 -0.774q2.196 0 4.014 0.72t3.078 1.926 2.232 2.916 1.548 3.51 0.954 3.906 0.504 3.924 0.126 3.726q0 4.32 -2.628 6.822t-6.984 2.502h-31.464q-4.356 0 -6.984 -2.502t-2.628 -6.822zm11.52 -32.148q0 -5.724 4.05 -9.774t9.774 -4.05 9.774 4.05 4.05 9.774 -4.05 9.774 -9.774 4.05 -9.774 -4.05 -4.05 -9.774zm25.344 -13.824q0 -3.816 2.7 -6.516t6.516 -2.7 6.516 2.7 2.7 6.516 -2.7 6.516 -6.516 2.7 -6.516 -2.7 -2.7 -6.516zm1.692 23.04q2.916 -4.212 2.916 -9.216 0 -1.044 -0.18 -2.376 2.376 0.828 4.788 0.828 2.124 0 4.284 -0.774t3.51 -1.53 1.566 -0.756q4.464 0 4.464 12.708 0 2.808 -2.016 4.266t-4.968 1.458h-4.824q-3.708 -4.428 -9.54 -4.608z","LINK":"M0.576 -48.311q0 -4.32 3.06 -7.308l5.292 -5.256q2.988 -2.988 7.308 -2.988 4.356 0 7.344 3.06l7.416 7.452q2.988 2.988 2.988 7.308 0 4.428 -3.168 7.524l3.168 3.168q3.096 -3.168 7.488 -3.168 4.32 0 7.344 3.024l7.488 7.488q3.024 3.024 3.024 7.344t-3.06 7.308l-5.292 5.256q-2.988 2.988 -7.308 2.988 -4.356 0 -7.344 -3.06l-7.416 -7.452q-2.988 -2.988 -2.988 -7.308 0 -4.428 3.168 -7.524l-3.168 -3.168q-3.096 3.168 -7.488 3.168 -4.32 0 -7.344 -3.024l-7.488 -7.488q-3.024 -3.024 -3.024 -7.344zm6.912 0q0 1.44 1.008 2.448l7.488 7.488q0.972 0.972 2.448 0.972 1.512 0 2.592 -1.116 -0.108 -0.108 -0.684 -0.666t-0.774 -0.774 -0.54 -0.684 -0.468 -0.918 -0.126 -0.99q0 -1.44 1.008 -2.448t2.448 -1.008q0.54 0 0.99 0.126t0.918 0.468 0.684 0.54 0.774 0.774 0.666 0.684q1.188 -1.116 1.188 -2.628 0 -1.44 -1.008 -2.448l-7.416 -7.452q-1.008 -1.008 -2.448 -1.008 -1.404 0 -2.448 0.972l-5.292 5.256q-1.008 1.008 -1.008 2.412zm25.308 25.38q0 1.44 1.008 2.448l7.416 7.452q0.972 0.972 2.448 0.972 1.44 0 2.448 -0.936l5.292 -5.256q1.008 -1.008 1.008 -2.412 0 -1.44 -1.008 -2.448l-7.488 -7.488q-1.008 -1.008 -2.448 -1.008 -1.512 0 -2.592 1.152 0.108 0.108 0.684 0.666t0.774 0.774 0.54 0.684 0.468 0.918 0.126 0.99q0 1.44 -1.008 2.448t-2.448 1.008q-0.54 0 -0.99 -0.126t-0.918 -0.468 -0.684 -0.54 -0.774 -0.774 -0.666 -0.684q-1.188 1.116 -1.188 2.628z","CLOUD":"M0 -25.271q0 -4.752 2.556 -8.694t6.732 -5.886q-0.072 -1.008 -0.072 -1.548 0 -7.632 5.4 -13.032t13.032 -5.4q5.688 0 10.314 3.168t6.75 8.28q2.52 -2.232 5.976 -2.232 3.816 0 6.516 2.7t2.7 6.516q0 2.7 -1.476 4.968 4.644 1.08 7.668 4.842t3.024 8.622q0 5.724 -4.05 9.774t-9.774 4.05h-39.168q-6.66 0 -11.394 -4.734t-4.734 -11.394z","FLASK":"M4.158 -6.821q-1.242 -2.286 0.774 -5.49l18.108 -28.548v-14.364h-2.304q-0.936 0 -1.62 -0.684t-0.684 -1.62 0.684 -1.62 1.62 -0.684h18.432q0.936 0 1.62 0.684t0.684 1.62 -0.684 1.62 -1.62 0.684h-2.304v14.364l18.108 28.548q2.016 3.204 0.774 5.49t-5.058 2.286h-41.472q-3.816 0 -5.058 -2.286zm12.978 -16.146h25.632l-9.792 -15.444 -0.72 -1.116v-15.696000000000002h-4.608v15.696000000000002l-0.72 1.116z","SCISSORS":"M0.036 -14.795q0.252 -2.736 2.016 -5.292t4.716 -4.464q4.752 -3.024 10.008 -3.024 2.988 0 5.436 1.116 0.324 -0.468 0.792 -0.792l4.392 -2.628 -4.392 -2.628q-0.468 -0.324 -0.792 -0.792 -2.448 1.116 -5.436 1.116 -5.256 0 -10.008 -3.024 -2.952 -1.908 -4.716 -4.464t-2.016 -5.292q-0.18 -2.124 0.558 -4.068t2.286 -3.348q3.06 -2.844 7.992 -2.844 5.22 0 9.972 3.024 2.988 1.872 4.752 4.428t2.016 5.328q0.144 1.728 -0.36 3.492 0.144 0.036 0.432 0.18l3.96 2.376 24.84 -13.932q0.504 -0.288 1.116 -0.288 0.576 0 1.044 0.252l4.608 2.304q1.08 0.576 1.26 1.836 0.108 1.296 -0.9 2.016l-18.252 14.328 18.252 14.328q1.008 0.72 0.9 2.016 -0.18 1.26 -1.26 1.836l-4.608 2.304q-0.468 0.252 -1.044 0.252 -0.612 0 -1.116 -0.288l-24.84 -13.932 -3.96 2.376q-0.288 0.144 -0.432 0.18 0.504 1.764 0.36 3.492 -0.252 2.772 -2.016 5.31t-4.752 4.446q-4.752 3.024 -9.972 3.024 -4.896 0 -7.992 -2.808 -3.24 -3.024 -2.844 -7.452zm6.012 -0.684q-0.9 2.376 0.756 3.888 1.404 1.296 4.068 1.296 3.6 0 6.912 -2.124 2.916 -1.836 3.816 -4.212t-0.756 -3.888q-1.404 -1.296 -4.068 -1.296 -3.6 0 -6.912 2.124 -2.916 1.836 -3.816 4.212zm0 -28.8q0.9 2.376 3.816 4.212 3.312 2.124 6.912 2.124 2.664 0 4.068 -1.296 1.656 -1.512 0.756 -3.888t-3.816 -4.212q-3.312 -2.124 -6.912 -2.124 -2.664 0 -4.068 1.296 -1.656 1.512 -0.756 3.888zm18.144 19.008l0.324 0.288 0.252 0.216q0.144 0.144 0.396 0.432t0.396 0.432l0.936 0.936 5.76 -3.456 3.456 1.152 26.496 -20.736 -4.608 -2.304 -27.648 15.516v4.068zm0 -9.216l3.456 2.088v-0.396q0 -1.296 1.188 -2.016l0.504 -0.288 -2.844 -1.692 -0.936 0.936q-0.108 0.108 -0.36 0.396t-0.432 0.432l-0.144 0.126 -0.108 0.09zm8.064 4.608q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684 0.684 1.62 -0.684 1.62 -1.62 0.684 -1.62 -0.684 -0.684 -1.62zm4.392 6.66l20.952 11.772 4.608 -2.304 -18.72 -14.688 -6.372 4.968q-0.072 0.108 -0.468 0.252z","FILES_O":"M0 -17.207v-24.192q0 -1.44 0.72 -3.168t1.728 -2.736l14.688 -14.688q1.008 -1.008 2.736 -1.728t3.168 -0.72h14.976q1.44 0 2.448 1.008t1.008 2.448v11.808q2.448 -1.44 4.608 -1.44h14.976q1.44 0 2.448 1.008t1.008 2.448v43.776q0 1.44 -1.008 2.448t-2.448 1.008h-34.56q-1.44 0 -2.448 -1.008t-1.008 -2.448v-10.368h-19.584q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 -1.152h18.432v-9.216q0 -1.44 0.72 -3.168t1.728 -2.736l11.376 -11.376v-14.976h-13.824v14.976q0 1.44 -1.008 2.448t-2.448 1.008h-14.976v23.04zm3.06 -27.648h10.764v-10.764zm19.98 41.472h32.256v-41.472h-13.824v14.976q0 1.44 -1.008 2.448t-2.448 1.008h-14.976v23.04zm3.06 -27.648h10.764v-10.764z","PAPERCLIP":"M0.144 -45.971q0 -5.724 3.96 -9.72t9.684 -3.996q5.688 0 9.828 4.068l21.78 21.816q0.36 0.36 0.36 0.792 0 0.576 -1.098 1.674t-1.674 1.098q-0.468 0 -0.828 -0.36l-21.816 -21.852q-2.844 -2.772 -6.516 -2.772 -3.816 0 -6.444 2.7t-2.628 6.516q0 3.78 2.736 6.516l27.936 27.972q2.268 2.268 5.22 2.268 2.304 0 3.816 -1.512t1.512 -3.816q0 -2.952 -2.268 -5.22l-20.916 -20.916q-0.936 -0.864 -2.16 -0.864 -1.044 0 -1.728 0.684t-0.684 1.728q0 1.152 0.9 2.124l14.76 14.76q0.36 0.36 0.36 0.792 0 0.576 -1.116 1.692t-1.692 1.116q-0.432 0 -0.792 -0.36l-14.76 -14.76q-2.268 -2.196 -2.268 -5.364 0 -2.952 2.052 -5.004t5.004 -2.052q3.168 0 5.364 2.268l20.916 20.916q3.6 3.528 3.6 8.46 0 4.212 -2.844 7.056t-7.056 2.844q-4.86 0 -8.46 -3.6l-27.972 -27.936q-4.068 -4.14 -4.068 -9.756z","SAVE":"M0 -7.991v-48.384q0 -1.44 1.008 -2.448t2.448 -1.008h33.408q1.44 0 3.168 0.72t2.736 1.728l10.08 10.08q1.008 1.008 1.728 2.736t0.72 3.168v33.408q0 1.44 -1.008 2.448t-2.448 1.008h-48.384q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 -1.152h4.608v-14.976q0 -1.44 1.008 -2.448t2.448 -1.008h29.952q1.44 0 2.448 1.008t1.008 2.448v14.976h4.608v-32.256q0 -0.504 -0.36 -1.386t-0.72 -1.242l-10.116 -10.116q-0.36 -0.36 -1.224 -0.72t-1.404 -0.36v14.976q0 1.44 -1.008 2.448t-2.448 1.008h-20.736q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-4.608v46.08zm9.216 0h27.648v-13.824h-27.648v13.824zm9.216 -33.408q0 0.468 0.342 0.81t0.81 0.342h6.912q0.468 0 0.81 -0.342t0.342 -0.81v-11.52q0 -0.468 -0.342 -0.81t-0.81 -0.342h-6.912q-0.468 0 -0.81 0.342t-0.342 0.81v11.52z","SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326z","REORDER":"M0 -11.447v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h50.688q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-50.688q-0.936 0 -1.62 -0.684t-0.684 -1.62zm0 -18.432v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h50.688q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-50.688q-0.936 0 -1.62 -0.684t-0.684 -1.62zm0 -18.432v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h50.688q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-50.688q-0.936 0 -1.62 -0.684t-0.684 -1.62z","LIST_UL":"M0 -13.751q0 -2.88 2.016 -4.896t4.896 -2.016 4.896 2.016 2.016 4.896 -2.016 4.896 -4.896 2.016 -4.896 -2.016 -2.016 -4.896zm0 -18.432q0 -2.88 2.016 -4.896t4.896 -2.016 4.896 2.016 2.016 4.896 -2.016 4.896 -4.896 2.016 -4.896 -2.016 -2.016 -4.896zm0 -18.432q0 -2.88 2.016 -4.896t4.896 -2.016 4.896 2.016 2.016 4.896 -2.016 4.896 -4.896 2.016 -4.896 -2.016 -2.016 -4.896zm18.432 40.32v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h43.776q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-43.776q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -18.432v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h43.776q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-43.776q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -18.432v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h43.776q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-43.776q-0.468 0 -0.81 -0.342t-0.342 -0.81z","LIST_OL":"M0.54 -24.911q0 -1.836 0.846 -3.348t2.034 -2.448 2.376 -1.71 2.034 -1.566 0.846 -1.62q0 -0.9 -0.522 -1.386t-1.422 -0.486q-1.656 0 -2.916 2.088l-3.06 -2.124q0.864 -1.836 2.574 -2.862t3.798 -1.026q2.628 0 4.428 1.494t1.8 4.05q0 1.8 -1.224 3.294t-2.7 2.322 -2.718 1.818 -1.278 1.89h4.572v-2.16h3.78v5.724h-13.032q-0.216 -1.296 -0.216 -1.944zm0.144 22.608l2.052 -3.168q1.764 1.62 3.816 1.62 1.044 0 1.818 -0.522t0.774 -1.53q0 -2.304 -3.78 -2.016l-0.936 -2.016q0.288 -0.36 1.17 -1.566t1.53 -1.944 1.332 -1.386v-0.036q-0.576 0 -1.746 0.036t-1.746 0.036v1.908h-3.816v-5.472h11.988v3.168l-3.42 4.14q1.836 0.432 2.916 1.764t1.08 3.168q0 2.88 -1.962 4.536t-4.878 1.656q-3.816 0 -6.192 -2.376zm0.54 -57.24l4.896 -4.572h3.816v14.544h3.888v3.564h-12.06v-3.564h3.852q0 -1.476 0.018 -4.392t0.018 -4.356v-0.432h-0.072q-0.288 0.612 -1.8 1.944zm17.208 49.248v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324h43.776q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-43.776q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -18.432v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324h43.776q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-43.776q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -18.432v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h43.776q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-43.776q-0.468 0 -0.81 -0.342t-0.342 -0.81z","STRIKETHROUGH":"M0 -28.727v-2.304q0 -0.504 0.324 -0.828t0.828 -0.324h62.208q0.504 0 0.828 0.324t0.324 0.828v2.304q0 0.504 -0.324 0.828t-0.828 0.324h-62.208q-0.504 0 -0.828 -0.324t-0.324 -0.828zm13.824 -15.408q0 -6.516 4.824 -11.124 4.788 -4.572 14.148 -4.572 1.8 0 6.012 0.684 2.376 0.432 6.372 1.728 0.36 1.368 0.756 4.248 0.504 4.428 0.504 6.588 0 0.648 -0.18 1.62l-0.432 0.108 -3.024 -0.216 -0.504 -0.072q-1.8 -5.364 -3.708 -7.38 -3.168 -3.276 -7.56 -3.276 -4.104 0 -6.552 2.124 -2.412 2.088 -2.412 5.256 0 2.628 2.376 5.04t10.044 4.644q2.484 0.72 6.228 2.376 2.088 1.008 3.42 1.872h-26.748q-1.008 -1.26 -1.836 -2.88 -1.728 -3.492 -1.728 -6.768zm1.08 29.448q-0.036 -1.08 0 -2.448l0.072 -1.332v-1.584l3.672 -0.072q0.54 1.224 1.08 2.556t0.81 2.016 0.45 0.972q1.26 2.052 2.88 3.384 1.548 1.296 3.78 2.052 2.124 0.792 4.752 0.792 2.304 0 5.004 -0.972 2.772 -0.936 4.392 -3.096 1.692 -2.196 1.692 -4.644 0 -3.024 -2.916 -5.652 -1.224 -1.044 -4.932 -2.556h14.796q0.252 1.404 0.252 3.312 0 3.996 -1.476 7.632 -0.828 1.98 -2.556 3.744 -1.332 1.26 -3.924 2.916 -2.88 1.728 -5.508 2.376 -2.88 0.756 -7.308 0.756 -4.104 0 -7.02 -0.828l-5.04 -1.44q-2.052 -0.576 -2.592 -1.008 -0.288 -0.288 -0.288 -0.792v-0.468q0 -3.888 -0.072 -5.616z","UNDERLINE":"M0 -7.991v2.304q0 0.504 0.324 0.828t0.828 0.324h52.992q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-52.992q-0.504 0 -0.828 0.324t-0.324 0.828zm0 -51.732q0.468 -0.036 1.44 -0.036 2.16 0 4.032 0.144 4.752 0.252 5.976 0.252 3.096 0 6.048 -0.108 4.176 -0.144 5.256 -0.18 2.016 0 3.096 -0.072l-0.036 0.504 0.072 2.304v0.324q-2.16 0.324 -4.464 0.324 -2.16 0 -2.844 0.9 -0.468 0.504 -0.468 4.752 0 0.468 0.018 1.17t0.018 0.918l0.036 8.244 0.504 10.08q0.216 4.464 1.836 7.272 1.26 2.124 3.456 3.312 3.168 1.692 6.372 1.692 3.744 0 6.876 -1.008 2.016 -0.648 3.564 -1.836 1.728 -1.296 2.34 -2.304 1.296 -2.016 1.908 -4.104 0.756 -2.628 0.756 -8.244 0 -2.844 -0.126 -4.608t-0.396 -4.41 -0.486 -5.742l-0.144 -2.124q-0.18 -2.412 -0.864 -3.168 -1.224 -1.26 -2.772 -1.224l-3.6 0.072 -0.504 -0.108 0.072 -3.096h3.024l7.38 0.36q2.736 0.108 7.056 -0.36l0.648 0.072q0.216 1.368 0.216 1.836 0 0.252 -0.144 1.116 -1.62 0.432 -3.024 0.468 -2.628 0.396 -2.844 0.612 -0.54 0.54 -0.54 1.476 0 0.252 0.054 0.972t0.054 1.116q0.288 0.684 0.792 14.256 0.216 7.02 -0.54 10.944 -0.54 2.736 -1.476 4.392 -1.368 2.34 -4.032 4.428 -2.7 2.052 -6.552 3.204 -3.924 1.188 -9.18 1.188 -6.012 0 -10.224 -1.656 -4.284 -1.692 -6.444 -4.392 -2.196 -2.736 -2.988 -7.02 -0.576 -2.88 -0.576 -8.532v-11.988q0 -6.768 -0.612 -7.668 -0.9 -1.296 -5.292 -1.404 -1.332 -0.072 -1.62 -0.144z","TABLE":"M0 -14.903v-39.168q0 -2.376 1.692 -4.068t4.068 -1.692h48.384q2.376 0 4.068 1.692t1.692 4.068v39.168q0 2.376 -1.692 4.068t-4.068 1.692h-48.384q-2.376 0 -4.068 -1.692t-1.692 -4.068zm4.608 0q0 0.504 0.324 0.828t0.828 0.324h11.52q0.504 0 0.828 -0.324t0.324 -0.828v-6.912q0 -0.504 -0.324 -0.828t-0.828 -0.324h-11.52q-0.504 0 -0.828 0.324t-0.324 0.828v6.912zm0 -13.824q0 0.504 0.324 0.828t0.828 0.324h11.52q0.504 0 0.828 -0.324t0.324 -0.828v-6.912q0 -0.504 -0.324 -0.828t-0.828 -0.324h-11.52q-0.504 0 -0.828 0.324t-0.324 0.828v6.912zm0 -13.824q0 0.504 0.324 0.828t0.828 0.324h11.52q0.504 0 0.828 -0.324t0.324 -0.828v-6.912q0 -0.504 -0.324 -0.828t-0.828 -0.324h-11.52q-0.504 0 -0.828 0.324t-0.324 0.828v6.912zm18.432 27.648q0 0.504 0.324 0.828t0.828 0.324h11.52q0.504 0 0.828 -0.324t0.324 -0.828v-6.912q0 -0.504 -0.324 -0.828t-0.828 -0.324h-11.52q-0.504 0 -0.828 0.324t-0.324 0.828v6.912zm0 -13.824q0 0.504 0.324 0.828t0.828 0.324h11.52q0.504 0 0.828 -0.324t0.324 -0.828v-6.912q0 -0.504 -0.324 -0.828t-0.828 -0.324h-11.52q-0.504 0 -0.828 0.324t-0.324 0.828v6.912zm0 -13.824q0 0.504 0.324 0.828t0.828 0.324h11.52q0.504 0 0.828 -0.324t0.324 -0.828v-6.912q0 -0.504 -0.324 -0.828t-0.828 -0.324h-11.52q-0.504 0 -0.828 0.324t-0.324 0.828v6.912zm18.432 27.648q0 0.504 0.324 0.828t0.828 0.324h11.52q0.504 0 0.828 -0.324t0.324 -0.828v-6.912q0 -0.504 -0.324 -0.828t-0.828 -0.324h-11.52q-0.504 0 -0.828 0.324t-0.324 0.828v6.912zm0 -13.824q0 0.504 0.324 0.828t0.828 0.324h11.52q0.504 0 0.828 -0.324t0.324 -0.828v-6.912q0 -0.504 -0.324 -0.828t-0.828 -0.324h-11.52q-0.504 0 -0.828 0.324t-0.324 0.828v6.912zm0 -13.824q0 0.504 0.324 0.828t0.828 0.324h11.52q0.504 0 0.828 -0.324t0.324 -0.828v-6.912q0 -0.504 -0.324 -0.828t-0.828 -0.324h-11.52q-0.504 0 -0.828 0.324t-0.324 0.828v6.912z","MAGIC":"M0.972 -14.903q0 -0.972 0.648 -1.62l46.296 -46.296q0.648 -0.648 1.62 -0.648t1.62 0.648l7.128 7.128q0.648 0.648 0.648 1.62t-0.648 1.62l-46.296 46.296q-0.648 0.648 -1.62 0.648t-1.62 -0.648l-7.128 -7.128q-0.648 -0.648 -0.648 -1.62zm3.636 -44.928l3.528 -1.08 1.08 -3.528 1.08 3.528 3.528 1.08 -3.528 1.08 -1.08 3.528 -1.08 -3.528zm6.912 6.912l7.056 -2.16 2.16 -7.056 2.16 7.056 7.056 2.16 -7.056 2.16 -2.16 7.056 -2.16 -7.056zm16.128 -6.912l3.528 -1.08 1.08 -3.528 1.08 3.528 3.528 1.08 -3.528 1.08 -1.08 3.528 -1.08 -3.528zm11.34 12.456l3.852 3.852 10.548 -10.548 -3.852 -3.852zm11.7 10.584l3.528 -1.08 1.08 -3.528 1.08 3.528 3.528 1.08 -3.528 1.08 -1.08 3.528 -1.08 -3.528z","TRUCK":"M2.304 -16.055q0 -0.936 0.684 -1.62t1.62 -0.684v-11.52q0 -0.288 -0.018 -1.26t0 -1.368 0.09 -1.242 0.234 -1.332 0.504 -1.098 0.81 -1.08l7.128 -7.128q0.684 -0.684 1.818 -1.152t2.106 -0.468h5.76v-6.912q0 -0.936 0.684 -1.62t1.62 -0.684h36.864q0.936 0 1.62 0.684t0.684 1.62v36.864q0 0.54 -0.144 0.954t-0.486 0.666 -0.594 0.414 -0.846 0.216 -0.81 0.072 -0.918 0 -0.81 -0.018q0 3.816 -2.7 6.516t-6.516 2.7 -6.516 -2.7 -2.7 -6.516h-13.824q0 3.816 -2.7 6.516t-6.516 2.7 -6.516 -2.7 -2.7 -6.516h-2.304q-0.108 0 -0.81 0.018t-0.918 0 -0.81 -0.072 -0.846 -0.216 -0.594 -0.414 -0.486 -0.666 -0.144 -0.954zm6.912 -16.128h13.824v-9.216h-5.688q-0.468 0 -0.792 0.324l-7.02 7.02q-0.324 0.324 -0.324 0.792v1.08zm4.608 18.432q0 1.872 1.368 3.24t3.24 1.368 3.24 -1.368 1.368 -3.24 -1.368 -3.24 -3.24 -1.368 -3.24 1.368 -1.368 3.24zm32.256 0q0 1.872 1.368 3.24t3.24 1.368 3.24 -1.368 1.368 -3.24 -1.368 -3.24 -3.24 -1.368 -3.24 1.368 -1.368 3.24z","PINTEREST":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708q-3.996 0 -7.848 -1.152 2.124 -3.348 2.808 -5.904 0.324 -1.224 1.944 -7.596 0.72 1.404 2.628 2.43t4.104 1.026q4.356 0 7.776 -2.466t5.292 -6.786 1.872 -9.72q0 -4.104 -2.142 -7.704t-6.21 -5.868 -9.18 -2.268q-3.78 0 -7.056 1.044t-5.562 2.772 -3.924 3.978 -2.412 4.662 -0.774 4.824q0 3.744 1.44 6.588t4.212 3.996q1.08 0.432 1.368 -0.72 0.072 -0.252 0.288 -1.116t0.288 -1.08q0.216 -0.828 -0.396 -1.548 -1.836 -2.196 -1.836 -5.436 0 -5.436 3.762 -9.342t9.846 -3.906q5.436 0 8.478 2.952t3.042 7.668q0 6.12 -2.466 10.404t-6.318 4.284q-2.196 0 -3.528 -1.566t-0.828 -3.762q0.288 -1.26 0.954 -3.366t1.08 -3.708 0.414 -2.718q0 -1.8 -0.972 -2.988t-2.772 -1.188q-2.232 0 -3.78 2.052t-1.548 5.112q0 2.628 0.9 4.392l-3.564 15.048q-0.612 2.52 -0.468 6.372 -7.416 -3.276 -11.988 -10.116t-4.572 -15.228z","PINTEREST_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-26.1q3.06 -4.392 3.888 -7.56 0.324 -1.224 1.908 -7.524 0.756 1.404 2.646 2.412t4.05 1.008q6.516 0 10.638 -5.31t4.122 -13.446q0 -3.024 -1.26 -5.85t-3.474 -5.004 -5.49 -3.492 -7.092 -1.314q-3.744 0 -7.002 1.026t-5.508 2.754 -3.87 3.942 -2.394 4.608 -0.774 4.77q0 3.672 1.422 6.48t4.194 3.96q0.468 0.18 0.846 0t0.522 -0.684q0.36 -1.584 0.54 -2.196 0.216 -0.828 -0.396 -1.512 -1.8 -2.232 -1.8 -5.4 0 -5.4 3.726 -9.234t9.738 -3.834q5.364 0 8.37 2.916t3.006 7.56q0 6.048 -2.43 10.296t-6.246 4.248q-2.16 0 -3.492 -1.566t-0.828 -3.726q0.288 -1.224 0.954 -3.33t1.062 -3.672 0.396 -2.682q0 -1.764 -0.954 -2.934t-2.718 -1.17q-2.196 0 -3.726 2.034t-1.53 5.022q0 2.592 0.864 4.356l-3.528 14.904q-0.864 3.6 -0.252 9.144h-6.588q-4.284 0 -7.326 -3.042t-3.042 -7.326z","GOOGLE_PLUS_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.936 -5.364q0 1.548 0.666 2.79t1.746 2.034 2.484 1.332 2.79 0.756 2.754 0.216q2.16 0 4.338 -0.558t4.086 -1.656 3.096 -2.97 1.188 -4.212q0 -1.764 -0.72 -3.222t-1.764 -2.394 -2.088 -1.71 -1.764 -1.584 -0.72 -1.602 0.558 -1.53 1.35 -1.422 1.584 -1.512 1.35 -2.142 0.558 -2.97q0 -2.16 -0.81 -3.582t-2.61 -3.258h2.988l3.168 -2.304h-9.54q-3.06 0 -5.796 1.152t-4.59 3.528 -1.854 5.508q0 3.348 2.322 5.562t5.706 2.214q0.792 0 1.548 -0.108 -0.468 1.044 -0.468 1.944 0 1.584 1.44 3.384 -6.3 0.432 -9.252 2.268 -1.692 1.044 -2.718 2.628t-1.026 3.42zm4.284 -1.044q0 -1.656 0.9 -2.88t2.358 -1.854 2.952 -0.9 3.042 -0.27q0.72 0 1.116 0.072 0.072 0.036 0.828 0.594t0.936 0.684 0.828 0.648 0.882 0.792 0.684 0.81 0.612 0.936 0.324 0.954 0.162 1.134q0 2.736 -2.106 4.05t-5.022 1.314q-1.476 0 -2.898 -0.342t-2.718 -1.026 -2.088 -1.908 -0.792 -2.808zm2.412 -22.716q0 -2.196 1.152 -3.744t3.312 -1.548q1.908 0 3.366 1.62t2.088 3.636 0.63 3.852q0 2.16 -1.188 3.582t-3.312 1.422q-1.908 0 -3.348 -1.53t-2.07 -3.474 -0.63 -3.816zm17.928 10.692h4.608v5.76h2.304v-5.76h4.608v-2.304h-4.608v-4.608h-2.304v4.608h-4.608v2.304z","GOOGLE_PLUS":"M1.152 -15.695q0 -2.916 1.602 -5.4t4.266 -4.14q4.716 -2.952 14.544 -3.6 -1.152 -1.512 -1.71 -2.664t-0.558 -2.628q0 -1.296 0.756 -3.06 -1.656 0.144 -2.448 0.144 -5.328 0 -8.982 -3.474t-3.654 -8.802q0 -2.952 1.296 -5.724t3.564 -4.716q2.772 -2.376 6.57 -3.528t7.83 -1.152h15.048l-4.968 3.168h-4.716q2.664 2.268 4.032 4.788t1.368 5.76q0 2.592 -0.882 4.662t-2.124 3.348 -2.502 2.34 -2.142 2.214 -0.882 2.376q0 1.296 1.152 2.538t2.79 2.448 3.258 2.646 2.772 3.744 1.152 5.112q0 3.24 -1.728 6.228 -2.592 4.392 -7.596 6.462t-10.728 2.07q-4.752 0 -8.874 -1.494t-6.174 -4.95q-1.332 -2.16 -1.332 -4.716zm6.696 -1.656q0 2.52 1.26 4.446t3.294 2.988 4.284 1.584 4.59 0.522q2.088 0 4.014 -0.468t3.564 -1.404 2.628 -2.628 0.99 -3.924q0 -0.9 -0.252 -1.764t-0.522 -1.512 -0.972 -1.494 -1.062 -1.26 -1.386 -1.242 -1.314 -1.044 -1.494 -1.08 -1.314 -0.936q-0.576 -0.072 -1.728 -0.072 -1.908 0 -3.78 0.252t-3.87 0.9 -3.492 1.656 -2.466 2.682 -0.972 3.798zm3.816 -35.784q0 1.656 0.36 3.51t1.134 3.708 1.872 3.33 2.7 2.412 3.474 0.936q1.368 0 2.808 -0.594t2.376 -1.566q1.908 -2.052 1.908 -5.724 0 -2.088 -0.612 -4.5t-1.746 -4.662 -3.042 -3.726 -4.212 -1.476q-1.512 0 -2.97 0.702t-2.358 1.89q-1.692 2.124 -1.692 5.76zm27.36 17.676v-3.888h7.632v-7.812h3.78v7.812h7.668v3.888h-7.668v7.884h-3.78v-7.884h-7.632z","MONEY":"M0 -11.447v-41.472q0 -0.936 0.684 -1.62t1.62 -0.684h64.512q0.936 0 1.62 0.684t0.684 1.62v41.472q0 0.936 -0.684 1.62t-1.62 0.684h-64.512q-0.936 0 -1.62 -0.684t-0.684 -1.62zm4.608 -11.52q3.816 0 6.516 2.7t2.7 6.516h41.472q0 -3.816 2.7 -6.516t6.516 -2.7v-18.432q-3.816 0 -6.516 -2.7t-2.7 -6.516h-41.472q0 3.816 -2.7 6.516t-6.516 2.7v18.432zm18.432 -9.216q0 -2.52 0.756 -5.112t2.142 -4.824 3.654 -3.636 4.968 -1.404 4.968 1.404 3.654 3.636 2.142 4.824 0.756 5.112 -0.756 5.112 -2.142 4.824 -3.654 3.636 -4.968 1.404 -4.968 -1.404 -3.654 -3.636 -2.142 -4.824 -0.756 -5.112zm4.392 -5.436l2.772 2.88q1.512 -1.332 1.98 -2.052h0.072v10.368h-4.608v3.456h13.824v-3.456h-4.608v-16.128h-4.104z","CARET_DOWN":"M0 -39.095q0 -0.936 0.684 -1.62t1.62 -0.684h32.256q0.936 0 1.62 0.684t0.684 1.62 -0.684 1.62l-16.128 16.128q-0.684 0.684 -1.62 0.684t-1.62 -0.684l-16.128 -16.128q-0.684 -0.684 -0.684 -1.62z","CARET_UP":"M0 -20.663q0 -0.936 0.684 -1.62l16.128 -16.128q0.684 -0.684 1.62 -0.684t1.62 0.684l16.128 16.128q0.684 0.684 0.684 1.62t-0.684 1.62 -1.62 0.684h-32.256q-0.936 0 -1.62 -0.684t-0.684 -1.62z","CARET_LEFT":"M2.304 -32.183q0 -0.936 0.684 -1.62l16.128 -16.128q0.684 -0.684 1.62 -0.684t1.62 0.684 0.684 1.62v32.256q0 0.936 -0.684 1.62t-1.62 0.684 -1.62 -0.684l-16.128 -16.128q-0.684 -0.684 -0.684 -1.62z","CARET_RIGHT":"M0 -16.055v-32.256q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684l16.128 16.128q0.684 0.684 0.684 1.62t-0.684 1.62l-16.128 16.128q-0.684 0.684 -1.62 0.684t-1.62 -0.684 -0.684 -1.62z","COLUMNS":"M0 -10.295v-43.776q0 -2.376 1.692 -4.068t4.068 -1.692h48.384q2.376 0 4.068 1.692t1.692 4.068v43.776q0 2.376 -1.692 4.068t-4.068 1.692h-48.384q-2.376 0 -4.068 -1.692t-1.692 -4.068zm4.608 0q0 0.468 0.342 0.81t0.81 0.342h21.888v-41.472h-23.04v40.32zm27.648 1.152h21.888q0.468 0 0.81 -0.342t0.342 -0.81v-40.32h-23.04v41.472z","UNSORTED":"M0 -25.271q0 -0.936 0.684 -1.62t1.62 -0.684h32.256q0.936 0 1.62 0.684t0.684 1.62 -0.684 1.62l-16.128 16.128q-0.684 0.684 -1.62 0.684t-1.62 -0.684l-16.128 -16.128q-0.684 -0.684 -0.684 -1.62zm0 -13.824q0 -0.936 0.684 -1.62l16.128 -16.128q0.684 -0.684 1.62 -0.684t1.62 0.684l16.128 16.128q0.684 0.684 0.684 1.62t-0.684 1.62 -1.62 0.684h-32.256q-0.936 0 -1.62 -0.684t-0.684 -1.62z","SORT_DOWN":"M0 -25.271q0 -0.936 0.684 -1.62t1.62 -0.684h32.256q0.936 0 1.62 0.684t0.684 1.62 -0.684 1.62l-16.128 16.128q-0.684 0.684 -1.62 0.684t-1.62 -0.684l-16.128 -16.128q-0.684 -0.684 -0.684 -1.62z","SORT_UP":"M0 -39.095q0 -0.936 0.684 -1.62l16.128 -16.128q0.684 -0.684 1.62 -0.684t1.62 0.684l16.128 16.128q0.684 0.684 0.684 1.62t-0.684 1.62 -1.62 0.684h-32.256q-0.936 0 -1.62 -0.684t-0.684 -1.62z","ENVELOPE":"M0 -10.295v-28.584q1.584 1.764 3.636 3.132 13.032 8.856 17.892 12.42 2.052 1.512 3.33 2.358t3.402 1.728 3.96 0.882h0.072q1.836 0 3.96 -0.882t3.402 -1.728 3.33 -2.358q6.12 -4.428 17.928 -12.42 2.052 -1.404 3.6 -3.132v28.584q0 2.376 -1.692 4.068t-4.068 1.692h-52.992q-2.376 0 -4.068 -1.692t-1.692 -4.068zm0 -38.376q0 -2.808 1.494 -4.68t4.266 -1.872h52.992q2.34 0 4.05 1.692t1.71 4.068q0 2.844 -1.764 5.436t-4.392 4.428q-13.536 9.396 -16.848 11.7 -0.36 0.252 -1.53 1.098t-1.944 1.368 -1.872 1.17 -2.07 0.972 -1.8 0.324h-0.072q-0.828 0 -1.8 -0.324t-2.07 -0.972 -1.872 -1.17 -1.944 -1.368 -1.53 -1.098q-3.276 -2.304 -9.432 -6.57t-7.38 -5.13q-2.232 -1.512 -4.212 -4.158t-1.98 -4.914z","LINKEDIN":"M0 -52.955q0 -2.664 1.854 -4.41t4.842 -1.746 4.788 1.746 1.836 4.41q0.036 2.628 -1.818 4.392t-4.878 1.764h-0.072q-2.952 0 -4.752 -1.764t-1.8 -4.392zm0.684 46.692v-35.676h11.88v35.676h-11.88zm18.432 0q0.072 -14.364 0.072 -23.292t-0.036 -10.656l-0.036 -1.728h11.844v5.184h-0.072q0.72 -1.152 1.476 -2.016t2.034 -1.872 3.132 -1.566 4.122 -0.558q6.156 0 9.9 4.086t3.744 11.97v20.448h-11.844v-19.08q0 -3.78 -1.458 -5.922t-4.554 -2.142q-2.268 0 -3.798 1.242t-2.286 3.078q-0.396 1.08 -0.396 2.916v19.908h-11.844z","UNDO":"M0 -39.095v-16.128q0 -1.512 1.44 -2.124 1.404 -0.612 2.484 0.504l4.68 4.644q3.852 -3.636 8.802 -5.634t10.242 -1.998q5.616 0 10.728 2.196t8.82 5.904 5.904 8.82 2.196 10.728 -2.196 10.728 -5.904 8.82 -8.82 5.904 -10.728 2.196q-6.192 0 -11.772 -2.61t-9.504 -7.362q-0.252 -0.36 -0.234 -0.81t0.306 -0.738l4.932 -4.968q0.36 -0.324 0.9 -0.324 0.576 0.072 0.828 0.432 2.628 3.42 6.444 5.292t8.1 1.872q3.744 0 7.146 -1.458t5.886 -3.942 3.942 -5.886 1.458 -7.146 -1.458 -7.146 -3.942 -5.886 -5.886 -3.942 -7.146 -1.458q-3.528 0 -6.768 1.278t-5.76 3.654l4.932 4.968q1.116 1.08 0.504 2.484 -0.612 1.44 -2.124 1.44h-16.128q-0.936 0 -1.62 -0.684t-0.684 -1.62z","LEGAL":"M1.44 -35.639q0 -0.468 0.162 -0.936t0.324 -0.792 0.558 -0.792 0.594 -0.666 0.738 -0.684 0.648 -0.594q1.08 -1.008 2.448 -1.008 0.36 0 0.648 0.054t0.594 0.198 0.486 0.216 0.486 0.36 0.414 0.36 0.468 0.45 0.432 0.45q-0.504 -0.504 -0.504 -1.224t0.504 -1.224l12.528 -12.528q0.504 -0.504 1.224 -0.504t1.224 0.504q-0.072 -0.072 -0.45 -0.432t-0.45 -0.468 -0.36 -0.414 -0.36 -0.486 -0.216 -0.486 -0.198 -0.594 -0.054 -0.648q0 -1.368 1.008 -2.448 0.108 -0.108 0.594 -0.648t0.684 -0.738 0.666 -0.594 0.792 -0.558 0.792 -0.324 0.936 -0.162q1.44 0 2.448 1.008l14.688 14.688q1.008 1.008 1.008 2.448 0 0.468 -0.162 0.936t-0.324 0.792 -0.558 0.792 -0.594 0.666 -0.738 0.684 -0.648 0.594q-1.08 1.008 -2.448 1.008 -0.36 0 -0.648 -0.054t-0.594 -0.198 -0.486 -0.216 -0.486 -0.36 -0.414 -0.36 -0.468 -0.45 -0.432 -0.45q0.504 0.504 0.504 1.224t-0.504 1.224l-4.536 4.536 9.216 9.216q1.548 -1.548 3.456 -1.548 1.872 0 3.276 1.332l13.068 13.068q1.332 1.404 1.332 3.276 0 1.908 -1.332 3.24l-3.852 3.888q-1.404 1.332 -3.276 1.332 -1.908 0 -3.24 -1.332l-13.068 -13.104q-1.368 -1.296 -1.368 -3.24 0 -1.908 1.548 -3.456l-9.216 -9.216 -4.536 4.536q-0.504 0.504 -1.224 0.504t-1.224 -0.504q0.072 0.072 0.45 0.432t0.45 0.468 0.36 0.414 0.36 0.486 0.216 0.486 0.198 0.594 0.054 0.648q0 1.368 -1.008 2.448 -0.108 0.108 -0.594 0.648t-0.684 0.738 -0.666 0.594 -0.792 0.558 -0.792 0.324 -0.936 0.162q-1.44 0 -2.448 -1.008l-14.688 -14.688q-1.008 -1.008 -1.008 -2.448z","TACHOMETER":"M0 -22.967q0 -6.552 2.556 -12.528t6.876 -10.296 10.296 -6.876 12.528 -2.556 12.528 2.556 10.296 6.876 6.876 10.296 2.556 12.528q0 9.396 -5.076 17.388 -0.684 1.044 -1.944 1.044h-50.472q-1.26 0 -1.944 -1.044 -5.076 -7.956 -5.076 -17.388zm4.608 0q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258 -1.35 -3.258 -3.258 -1.35 -3.258 1.35 -1.35 3.258zm6.912 -16.128q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258 -1.35 -3.258 -3.258 -1.35 -3.258 1.35 -1.35 3.258zm14.04 21.276q-0.72 2.772 0.72 5.256t4.212 3.204 5.256 -0.72 3.204 -4.212q0.576 -2.16 -0.216 -4.212t-2.592 -3.276l3.636 -13.752q0.216 -0.936 -0.27 -1.746t-1.386 -1.062 -1.728 0.234 -1.08 1.422l-3.636 13.752q-2.16 0.18 -3.852 1.566t-2.268 3.546zm2.088 -28.188q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258 -1.35 -3.258 -3.258 -1.35 -3.258 1.35 -1.35 3.258zm16.128 6.912q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258 -1.35 -3.258 -3.258 -1.35 -3.258 1.35 -1.35 3.258zm6.912 16.128q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258 -1.35 -3.258 -3.258 -1.35 -3.258 1.35 -1.35 3.258z","COMMENT_O":"M0 -32.183q0 -6.264 4.32 -11.574t11.736 -8.388 16.2 -3.078 16.2 3.078 11.736 8.388 4.32 11.574 -4.32 11.574 -11.736 8.388 -16.2 3.078q-2.52 0 -5.22 -0.288 -7.128 6.3 -16.56 8.712 -1.764 0.504 -4.104 0.792h-0.18q-0.54 0 -0.972 -0.378t-0.576 -0.99v-0.036q-0.108 -0.144 -0.018 -0.432t0.072 -0.36 0.162 -0.342l0.216 -0.324 0.252 -0.306 0.288 -0.324q0.252 -0.288 1.116 -1.242t1.242 -1.368 1.116 -1.422 1.17 -1.836 0.972 -2.124 0.936 -2.736q-5.652 -3.204 -8.91 -7.92t-3.258 -10.116zm4.608 0q0 4.032 2.574 7.686t7.254 6.318l3.132 1.8 -0.972 3.456q-0.864 3.276 -2.52 6.192 5.472 -2.268 9.9 -6.156l1.548 -1.368 2.052 0.216q2.484 0.288 4.68 0.288 7.344 0 13.734 -2.502t10.152 -6.75 3.762 -9.18 -3.762 -9.18 -10.152 -6.75 -13.734 -2.502 -13.734 2.502 -10.152 6.75 -3.762 9.18z","COMMENTS_O":"M0 -36.791q0 -5.004 3.384 -9.252t9.234 -6.714 12.726 -2.466 12.726 2.466 9.234 6.714 3.384 9.252 -3.384 9.252 -9.234 6.714 -12.726 2.466q-3.096 0 -6.336 -0.576 -4.464 3.168 -10.008 4.608 -1.296 0.324 -3.096 0.576h-0.108q-0.396 0 -0.738 -0.288t-0.414 -0.756q-0.036 -0.108 -0.036 -0.234t0.018 -0.234 0.072 -0.216l0.09 -0.18 0.126 -0.198 0.144 -0.18 0.162 -0.18 0.144 -0.162q0.18 -0.216 0.828 -0.9t0.936 -1.062 0.81 -1.044 0.9 -1.386 0.738 -1.584q-4.464 -2.592 -7.02 -6.372t-2.556 -8.064zm4.608 0q0 2.952 1.908 5.688t5.364 4.752l3.492 2.016 -1.26 3.024q1.224 -0.72 2.232 -1.404l1.584 -1.116 1.908 0.36q2.808 0.504 5.508 0.504 5.508 0 10.296 -1.872t7.614 -5.076 2.826 -6.876 -2.826 -6.876 -7.614 -5.076 -10.296 -1.872 -10.296 1.872 -7.614 5.076 -2.826 6.876zm17.568 22.896q2.088 0.144 3.168 0.144 5.796 0 11.124 -1.62t9.504 -4.644q4.5 -3.312 6.912 -7.632t2.412 -9.144q0 -2.772 -0.828 -5.472 4.644 2.556 7.344 6.408t2.7 8.28q0 4.32 -2.556 8.082t-7.02 6.354q0.36 0.864 0.738 1.584t0.9 1.386 0.81 1.044 0.936 1.062 0.828 0.9l0.144 0.162 0.162 0.18 0.144 0.18 0.126 0.198 0.09 0.18 0.072 0.216 0.018 0.234 -0.036 0.234q-0.108 0.504 -0.468 0.792t-0.792 0.252q-1.8 -0.252 -3.096 -0.576 -5.544 -1.44 -10.008 -4.608 -3.24 0.576 -6.336 0.576 -9.756 0 -16.992 -4.752z","FLASH":"M0.036 -28.979l7.236 -29.7q0.144 -0.504 0.576 -0.828t1.008 -0.324h11.808q0.684 0 1.152 0.45t0.468 1.062q0 0.288 -0.18 0.648l-6.156 16.668 14.256 -3.528q0.288 -0.072 0.432 -0.072 0.684 0 1.224 0.54 0.648 0.72 0.252 1.584l-19.44 41.652q-0.468 0.9 -1.512 0.9 -0.144 0 -0.504 -0.072 -0.612 -0.18 -0.918 -0.684t-0.162 -1.08l7.092 -29.088 -14.616 3.636q-0.144 0.036 -0.432 0.036 -0.648 0 -1.116 -0.396 -0.648 -0.54 -0.468 -1.404z","SITEMAP":"M0 -7.991v-11.52q0 -1.44 1.008 -2.448t2.448 -1.008h3.456v-6.912q0 -1.872 1.368 -3.24t3.24 -1.368h18.432v-6.912h-3.456q-1.44 0 -2.448 -1.008t-1.008 -2.448v-11.52q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v11.52q0 1.44 -1.008 2.448t-2.448 1.008h-3.456v6.912h18.432q1.872 0 3.24 1.368t1.368 3.24v6.912h3.456q1.44 0 2.448 1.008t1.008 2.448v11.52q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448v-11.52q0 -1.44 1.008 -2.448t2.448 -1.008h3.456v-6.912h-18.432v6.912h3.456q1.44 0 2.448 1.008t1.008 2.448v11.52q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448v-11.52q0 -1.44 1.008 -2.448t2.448 -1.008h3.456v-6.912h-18.432v6.912h3.456q1.44 0 2.448 1.008t1.008 2.448v11.52q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448z","UMBRELLA":"M0 -33.659q0 -0.18 0.036 -0.252 1.62 -6.588 6.21 -11.502t10.728 -7.362 12.978 -2.448q5.04 0 9.882 1.44t8.874 4.086 7.002 6.732 4.158 9.054q0.036 0.072 0.036 0.252 0 0.468 -0.342 0.81t-0.81 0.342q-0.396 0 -0.828 -0.36 -1.764 -1.656 -3.348 -2.484t-3.672 -0.828q-2.448 0 -4.608 1.332t-3.708 3.492q-0.252 0.36 -0.63 1.008t-0.522 0.864q-0.396 0.612 -1.008 0.612 -0.648 0 -1.044 -0.612 -0.144 -0.216 -0.522 -0.864t-0.63 -1.008q-1.548 -2.16 -3.69 -3.492t-4.59 -1.332 -4.59 1.332 -3.69 3.492q-0.252 0.36 -0.63 1.008t-0.522 0.864q-0.396 0.612 -1.044 0.612 -0.612 0 -1.008 -0.612 -0.144 -0.216 -0.522 -0.864t-0.63 -1.008q-1.548 -2.16 -3.708 -3.492t-4.608 -1.332q-2.088 0 -3.672 0.828t-3.348 2.484q-0.432 0.36 -0.828 0.36 -0.468 0 -0.81 -0.342t-0.342 -0.81zm13.824 19.908q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684 0.684 1.62q0 1.8 1.404 3.204t3.204 1.404 3.204 -1.404 1.404 -3.204v-20.88q1.188 -0.396 2.304 -0.396t2.304 0.396v20.88q0 3.744 -2.736 6.48t-6.48 2.736 -6.48 -2.736 -2.736 -6.48zm13.824 -42.552v-3.528q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684 0.684 1.62v3.528q-1.512 -0.072 -2.304 -0.072t-2.304 0.072z","PASTE":"M0 -12.599v-48.384q0 -1.44 1.008 -2.448t2.448 -1.008h39.168q1.44 0 2.448 1.008t1.008 2.448v11.808q0.756 0.468 1.296 1.008l14.688 14.688q1.008 1.008 1.728 2.736t0.72 3.168v24.192q0 1.44 -1.008 2.448t-2.448 1.008h-34.56q-1.44 0 -2.448 -1.008t-1.008 -2.448v-5.76h-19.584q-1.44 0 -2.448 -1.008t-1.008 -2.448zm9.216 -43.776q0 0.468 0.342 0.81t0.81 0.342h25.344q0.468 0 0.81 -0.342t0.342 -0.81v-2.304q0 -0.468 -0.342 -0.81t-0.81 -0.342h-25.344q-0.468 0 -0.81 0.342t-0.342 0.81v2.304zm18.432 51.84h32.256v-23.04h-14.976q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-13.824v41.472zm18.432 -27.648h10.764l-10.764 -10.764v10.764z","LIGHTBULB_O":"M0 -43.703q0 -3.564 1.602 -6.642t4.212 -5.112 5.904 -3.204 6.714 -1.17 6.714 1.17 5.904 3.204 4.212 5.112 1.602 6.642q0 5.58 -3.708 9.648 -1.62 1.764 -2.682 3.132t-2.142 3.438 -1.224 3.87q1.692 1.008 1.692 2.952 0 1.332 -0.9 2.304 0.9 0.972 0.9 2.304 0 1.872 -1.62 2.916 0.468 0.828 0.468 1.692 0 1.656 -1.134 2.556t-2.79 0.9q-0.72 1.584 -2.16 2.52t-3.132 0.936 -3.132 -0.936 -2.16 -2.52q-1.656 0 -2.79 -0.9t-1.134 -2.556q0 -0.864 0.468 -1.692 -1.62 -1.044 -1.62 -2.916 0 -1.332 0.9 -2.304 -0.9 -0.972 -0.9 -2.304 0 -1.944 1.692 -2.952 -0.144 -1.8 -1.224 -3.87t-2.142 -3.438 -2.682 -3.132q-3.708 -4.068 -3.708 -9.648zm4.608 0q0 3.636 2.448 6.48 0.36 0.396 1.098 1.188t1.098 1.188q4.608 5.508 5.076 10.728h8.208q0.468 -5.22 5.076 -10.728 0.36 -0.396 1.098 -1.188t1.098 -1.188q2.448 -2.844 2.448 -6.48 0 -2.592 -1.242 -4.824t-3.24 -3.654 -4.428 -2.232 -4.914 -0.81 -4.914 0.81 -4.428 2.232 -3.24 3.654 -1.242 4.824zm12.672 -4.608q0 -0.468 0.342 -0.81t0.81 -0.342q1.8 0 3.582 0.576t3.132 1.944 1.35 3.24q0 0.468 -0.342 0.81t-0.81 0.342 -0.81 -0.342 -0.342 -0.81q0 -1.656 -1.944 -2.556t-3.816 -0.9q-0.468 0 -0.81 -0.342t-0.342 -0.81z","EXCHANGE":"M0 -18.359q0 -0.504 0.324 -0.828l11.52 -11.52q0.324 -0.324 0.828 -0.324 0.468 0 0.81 0.342t0.342 0.81v6.912h49.536q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-49.536v6.912q0 0.468 -0.342 0.81t-0.81 0.342q-0.432 0 -0.864 -0.36l-11.484 -11.52q-0.324 -0.324 -0.324 -0.792zm0 -19.584v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h49.536v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324q0.432 0 0.864 0.36l11.484 11.484q0.324 0.324 0.324 0.828t-0.324 0.828l-11.52 11.52q-0.324 0.324 -0.828 0.324 -0.468 0 -0.81 -0.342t-0.342 -0.81v-6.912h-49.536q-0.468 0 -0.81 -0.342t-0.342 -0.81z","CLOUD_DOWNLOAD":"M0 -25.271q0 -4.68 2.52 -8.64t6.768 -5.94q-0.072 -1.08 -0.072 -1.548 0 -7.632 5.4 -13.032t13.032 -5.4q5.616 0 10.278 3.132t6.786 8.316q2.556 -2.232 5.976 -2.232 3.816 0 6.516 2.7t2.7 6.516q0 2.736 -1.476 4.968 4.68 1.116 7.686 4.878t3.006 8.586q0 5.724 -4.05 9.774t-9.774 4.05h-39.168q-6.66 0 -11.394 -4.734t-4.734 -11.394zm18.432 -5.76q0 0.504 0.324 0.828l12.672 12.672q0.324 0.324 0.828 0.324t0.828 -0.324l12.636 -12.636q0.36 -0.432 0.36 -0.864 0 -0.504 -0.324 -0.828t-0.828 -0.324h-8.064v-12.672q0 -0.468 -0.342 -0.81t-0.81 -0.342h-6.912q-0.468 0 -0.81 0.342t-0.342 0.81v12.672h-8.064q-0.468 0 -0.81 0.342t-0.342 0.81z","CLOUD_UPLOAD":"M0 -25.271q0 -4.68 2.52 -8.64t6.768 -5.94q-0.072 -1.08 -0.072 -1.548 0 -7.632 5.4 -13.032t13.032 -5.4q5.616 0 10.278 3.132t6.786 8.316q2.556 -2.232 5.976 -2.232 3.816 0 6.516 2.7t2.7 6.516q0 2.736 -1.476 4.968 4.68 1.116 7.686 4.878t3.006 8.586q0 5.724 -4.05 9.774t-9.774 4.05h-39.168q-6.66 0 -11.394 -4.734t-4.734 -11.394zm18.432 -8.064q0 0.504 0.324 0.828t0.828 0.324h8.064v12.672q0 0.468 0.342 0.81t0.81 0.342h6.912q0.468 0 0.81 -0.342t0.342 -0.81v-12.672h8.064q0.468 0 0.81 -0.342t0.342 -0.81q0 -0.504 -0.324 -0.828l-12.672 -12.672q-0.324 -0.324 -0.828 -0.324t-0.828 0.324l-12.636 12.636q-0.36 0.432 -0.36 0.864z","USER_MD":"M0 -13.859q0 -2.448 0.198 -4.716t0.864 -4.968 1.71 -4.77 2.916 -3.708 4.32 -2.178q-0.792 1.872 -0.792 4.32v7.308q-2.088 0.72 -3.348 2.52t-1.26 3.996q0 2.88 2.016 4.896t4.896 2.016 4.896 -2.016 2.016 -4.896q0 -2.196 -1.278 -3.996t-3.33 -2.52v-7.308q0 -2.232 0.9 -3.348 4.752 3.744 10.62 3.744t10.62 -3.744q0.9 1.116 0.9 3.348v2.304q-3.816 0 -6.516 2.7t-2.7 6.516v3.204q-1.152 1.044 -1.152 2.556 0 1.44 1.008 2.448t2.448 1.008 2.448 -1.008 1.008 -2.448q0 -1.512 -1.152 -2.556v-3.204q0 -1.872 1.368 -3.24t3.24 -1.368 3.24 1.368 1.368 3.24v3.204q-1.152 1.044 -1.152 2.556 0 1.44 1.008 2.448t2.448 1.008 2.448 -1.008 1.008 -2.448q0 -1.512 -1.152 -2.556v-3.204q0 -2.448 -1.242 -4.59t-3.366 -3.366q0 -0.36 0.018 -1.53t0 -1.728 -0.09 -1.494 -0.252 -1.692 -0.468 -1.44q2.448 0.54 4.32 2.178t2.916 3.708 1.71 4.77 0.864 4.968 0.198 4.716q0 4.356 -2.628 6.84t-6.984 2.484h-31.464q-4.356 0 -6.984 -2.484t-2.628 -6.84zm9.216 -2.196q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684 0.684 1.62 -0.684 1.62 -1.62 0.684 -1.62 -0.684 -0.684 -1.62zm2.304 -29.952q0 -5.724 4.05 -9.774t9.774 -4.05 9.774 4.05 4.05 9.774 -4.05 9.774 -9.774 4.05 -9.774 -4.05 -4.05 -9.774z","STETHOSCOPE":"M0 -36.791v-18.432q0 -0.936 0.684 -1.62t1.62 -0.684q0.216 0 0.576 0.072 0.612 -1.08 1.692 -1.728t2.34 -0.648q1.908 0 3.258 1.35t1.35 3.258 -1.35 3.258 -3.258 1.35q-1.188 0 -2.304 -0.648v14.472q0 3.816 3.384 6.516t8.136 2.7 8.136 -2.7 3.384 -6.516v-14.472q-1.116 0.648 -2.304 0.648 -1.908 0 -3.258 -1.35t-1.35 -3.258 1.35 -3.258 3.258 -1.35q1.26 0 2.34 0.648t1.692 1.728q0.36 -0.072 0.576 -0.072 0.936 0 1.62 0.684t0.684 1.62v18.432q0 5.184 -3.96 9.072t-9.864 4.608v4.752q0 3.816 3.384 6.516t8.136 2.7 8.136 -2.7 3.384 -6.516v-14.22q-2.052 -0.756 -3.33 -2.52t-1.278 -3.996q0 -2.88 2.016 -4.896t4.896 -2.016 4.896 2.016 2.016 4.896q0 2.232 -1.278 3.996t-3.33 2.52v14.22q0 5.724 -4.734 9.774t-11.394 4.05 -11.394 -4.05 -4.734 -9.774v-4.752q-5.904 -0.72 -9.864 -4.608t-3.96 -9.072zm41.472 -2.304q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62z","SUITCASE":"M0 -12.599v-29.952q0 -3.312 2.376 -5.688t5.688 -2.376h2.304v46.08h-2.304q-3.312 0 -5.688 -2.376t-2.376 -5.688zm13.824 8.064v-46.08h4.608v-5.76q0 -1.44 1.008 -2.448t2.448 -1.008h20.736q1.44 0 2.448 1.008t1.008 2.448v5.76h4.608v46.08h-36.864zm9.216 -46.08h18.432v-4.608h-18.432v4.608zm31.104 46.08v-46.08h2.304q3.312 0 5.688 2.376t2.376 5.688v29.952q0 3.312 -2.376 5.688t-5.688 2.376h-2.304z","BELL":"M0 -13.751q6.84 -5.796 10.332 -14.31t3.492 -17.946q0 -5.94 3.456 -9.432t9.504 -4.212q-0.288 -0.648 -0.288 -1.332 0 -1.44 1.008 -2.448t2.448 -1.008 2.448 1.008 1.008 2.448q0 0.684 -0.288 1.332 6.048 0.72 9.504 4.212t3.456 9.432q0 9.432 3.492 17.946t10.332 14.31q0 1.872 -1.368 3.24t-3.24 1.368h-16.128q0 3.816 -2.7 6.516t-6.516 2.7 -6.516 -2.7 -2.7 -6.516h-16.128q-1.872 0 -3.24 -1.368t-1.368 -3.24zm23.616 4.608q0 2.628 1.854 4.482t4.482 1.854q0.576 0 0.576 -0.576t-0.576 -0.576q-2.124 0 -3.654 -1.53t-1.53 -3.654q0 -0.576 -0.576 -0.576t-0.576 0.576z","COFFEE":"M0 -13.751h64.512q0 3.816 -2.7 6.516t-6.516 2.7h-46.08q-3.816 0 -6.516 -2.7t-2.7 -6.516zm9.216 -12.672v-26.496q0 -0.936 0.684 -1.62t1.62 -0.684h41.472q5.724 0 9.774 4.05t4.05 9.774 -4.05 9.774 -9.774 4.05h-2.304v1.152q0 3.312 -2.376 5.688t-5.688 2.376h-25.344q-3.312 0 -5.688 -2.376t-2.376 -5.688zm41.472 -8.064h2.304q2.88 0 4.896 -2.016t2.016 -4.896 -2.016 -4.896 -4.896 -2.016h-2.304v13.824z","CUTLERY":"M0 -39.095v-23.04q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684 0.684 1.62v14.976q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62v-14.976q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684 0.684 1.62v14.976q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62v-14.976q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684 0.684 1.62v23.04q0 2.196 -1.278 3.996t-3.33 2.52v28.044q0 1.872 -1.368 3.24t-3.24 1.368h-4.608q-1.872 0 -3.24 -1.368t-1.368 -3.24v-28.044q-2.052 -0.72 -3.33 -2.52t-1.278 -3.996zm27.648 14.976v-28.8q0 -4.752 3.384 -8.136t8.136 -3.384h9.216q0.936 0 1.62 0.684t0.684 1.62v57.6q0 1.872 -1.368 3.24t-3.24 1.368h-4.608q-1.872 0 -3.24 -1.368t-1.368 -3.24v-18.432h-8.064q-0.468 0 -0.81 -0.342t-0.342 -0.81z","FILE_TEXT_O":"M0 -3.383v-57.6q0 -1.44 1.008 -2.448t2.448 -1.008h32.256q1.44 0 3.168 0.72t2.736 1.728l11.232 11.232q1.008 1.008 1.728 2.736t0.72 3.168v41.472q0 1.44 -1.008 2.448t-2.448 1.008h-48.384q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 -1.152h46.08v-36.864h-14.976q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-27.648v55.296zm9.216 -10.368v-2.304q0 -0.504 0.324 -0.828t0.828 -0.324h25.344q0.504 0 0.828 0.324t0.324 0.828v2.304q0 0.504 -0.324 0.828t-0.828 0.324h-25.344q-0.504 0 -0.828 -0.324t-0.324 -0.828zm0 -9.216v-2.304q0 -0.504 0.324 -0.828t0.828 -0.324h25.344q0.504 0 0.828 0.324t0.324 0.828v2.304q0 0.504 -0.324 0.828t-0.828 0.324h-25.344q-0.504 0 -0.828 -0.324t-0.324 -0.828zm0 -9.216v-2.304q0 -0.504 0.324 -0.828t0.828 -0.324h25.344q0.504 0 0.828 0.324t0.324 0.828v2.304q0 0.504 -0.324 0.828t-0.828 0.324h-25.344q-0.504 0 -0.828 -0.324t-0.324 -0.828zm23.04 -12.672h13.536q-0.36 -1.044 -0.792 -1.476l-11.268 -11.268q-0.432 -0.432 -1.476 -0.792v13.536z","BUILDING_O":"M0 -2.231v-59.904q0 -0.936 0.684 -1.62t1.62 -0.684h46.08q0.936 0 1.62 0.684t0.684 1.62v59.904q0 0.936 -0.684 1.62t-1.62 0.684h-46.08q-0.936 0 -1.62 -0.684t-0.684 -1.62zm4.608 -2.304h13.824v-8.064q0 -0.468 0.342 -0.81t0.81 -0.342h11.52q0.468 0 0.81 0.342t0.342 0.81v8.064h13.824v-55.296h-41.472v55.296zm4.608 -10.368v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm9.216 27.648v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm9.216 27.648v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm9.216 36.864v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81z","HOSPITAL_O":"M0 -2.231v-46.08q0 -0.936 0.684 -1.62t1.62 -0.684h11.52v-10.368q0 -1.44 1.008 -2.448t2.448 -1.008h16.128q1.44 0 2.448 1.008t1.008 2.448v10.368h11.52q0.936 0 1.62 0.684t0.684 1.62v46.08q0 0.936 -0.684 1.62t-1.62 0.684h-46.08q-0.936 0 -1.62 -0.684t-0.684 -1.62zm4.608 -2.304h13.824v-8.064q0 -0.468 0.342 -0.81t0.81 -0.342h11.52q0.468 0 0.81 0.342t0.342 0.81v8.064h13.824v-41.472h-9.216v1.152q0 1.44 -1.008 2.448t-2.448 1.008h-16.128q-1.44 0 -2.448 -1.008t-1.008 -2.448v-1.152h-9.216v41.472zm4.608 -10.368v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm9.216 9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -13.824q0 0.468 0.342 0.81t0.81 0.342h2.304q0.468 0 0.81 -0.342t0.342 -0.81v-3.456h4.608v3.456q0 0.468 0.342 0.81t0.81 0.342h2.304q0.468 0 0.81 -0.342t0.342 -0.81v-11.52q0 -0.468 -0.342 -0.81t-0.81 -0.342h-2.304q-0.468 0 -0.81 0.342t-0.342 0.81v3.456h-4.608v-3.456q0 -0.468 -0.342 -0.81t-0.81 -0.342h-2.304q-0.468 0 -0.81 0.342t-0.342 0.81v11.52zm9.216 23.04v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm9.216 18.432v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81zm0 -9.216v-2.304q0 -0.468 0.342 -0.81t0.81 -0.342h2.304q0.468 0 0.81 0.342t0.342 0.81v2.304q0 0.468 -0.342 0.81t-0.81 0.342h-2.304q-0.468 0 -0.81 -0.342t-0.342 -0.81z","AMBULANCE":"M2.304 -16.055q0 -0.936 0.684 -1.62t1.62 -0.684v-14.976q0 -0.936 0.468 -2.088t1.152 -1.836l7.128 -7.128q0.684 -0.684 1.836 -1.152t2.088 -0.468h5.76v-11.52q0 -0.936 0.684 -1.62t1.62 -0.684h41.472q0.936 0 1.62 0.684t0.684 1.62v41.472q0 0.936 -0.684 1.62t-1.62 0.684h-6.912q0 3.816 -2.7 6.516t-6.516 2.7 -6.516 -2.7 -2.7 -6.516h-13.824q0 3.816 -2.7 6.516t-6.516 2.7 -6.516 -2.7 -2.7 -6.516h-4.608q-0.936 0 -1.62 -0.684t-0.684 -1.62zm6.912 -16.128h13.824v-9.216h-5.688q-0.504 0.072 -0.792 0.324l-7.02 7.02q-0.252 0.432 -0.324 0.792v1.08zm4.608 18.432q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258 -1.35 -3.258 -3.258 -1.35 -3.258 1.35 -1.35 3.258zm18.432 -24.192q0 0.504 0.324 0.828t0.828 0.324h8.064v8.064q0 0.504 0.324 0.828t0.828 0.324h6.912q0.504 0 0.828 -0.324t0.324 -0.828v-8.064h8.064q0.504 0 0.828 -0.324t0.324 -0.828v-6.912q0 -0.504 -0.324 -0.828t-0.828 -0.324h-8.064v-8.064q0 -0.504 -0.324 -0.828t-0.828 -0.324h-6.912q-0.504 0 -0.828 0.324t-0.324 0.828v8.064h-8.064q-0.504 0 -0.828 0.324t-0.324 0.828v6.912zm13.824 24.192q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258 -1.35 -3.258 -3.258 -1.35 -3.258 1.35 -1.35 3.258z","MEDKIT":"M0 -12.599v-29.952q0 -3.312 2.376 -5.688t5.688 -2.376h1.152v46.08h-1.152q-3.312 0 -5.688 -2.376t-2.376 -5.688zm12.672 8.064v-46.08h5.76v-5.76q0 -1.44 1.008 -2.448t2.448 -1.008h20.736q1.44 0 2.448 1.008t1.008 2.448v5.76h5.76v46.08h-39.168zm5.76 -19.584q0 0.504 0.324 0.828t0.828 0.324h8.064v8.064q0 0.504 0.324 0.828t0.828 0.324h6.912q0.504 0 0.828 -0.324t0.324 -0.828v-8.064h8.064q0.504 0 0.828 -0.324t0.324 -0.828v-6.912q0 -0.504 -0.324 -0.828t-0.828 -0.324h-8.064v-8.064q0 -0.504 -0.324 -0.828t-0.828 -0.324h-6.912q-0.504 0 -0.828 0.324t-0.324 0.828v8.064h-8.064q-0.504 0 -0.828 0.324t-0.324 0.828v6.912zm4.608 -26.496h18.432v-4.608h-18.432v4.608zm32.256 46.08v-46.08h1.152q3.312 0 5.688 2.376t2.376 5.688v29.952q0 3.312 -2.376 5.688t-5.688 2.376h-1.152z","FIGHTER_JET":"M0 -27.575v-4.608l6.912 -0.864v-0.288h-4.608v-1.152h-1.152v-6.912l1.152 -1.152h3.456l6.912 8.064h5.76v-14.976h-2.304v-1.152h11.52q0.936 0 1.62 0.162t0.684 0.414 -0.684 0.414 -1.62 0.162h-2.484l10.548 12.672h2.304l8.064 2.304 12.672 1.152q9.396 2.088 10.332 3.348l0.036 0.108q-0.036 1.152 -10.368 3.456l-12.672 1.152 -8.064 2.304h-2.304l-10.548 12.672h2.484q0.936 0 1.62 0.162t0.684 0.414 -0.684 0.414 -1.62 0.162h-11.52v-1.152h2.304v-14.976h-5.76l-6.912 8.064h-3.456l-1.152 -1.152v-6.912h1.152v-1.152h4.608v-0.288z","BEER":"M2.304 -50.615l1.152 -4.608h17.28l1.152 -4.608h34.56l1.152 6.912 -2.304 1.152v28.8l4.608 6.912v6.912h-41.472v-6.912l4.608 -6.912h-4.608q-5.724 0 -9.774 -4.05t-4.05 -9.774v-11.52zm11.52 13.824q0 1.908 1.35 3.258t3.258 1.35h4.608v-13.824h-9.216v9.216z","H_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 -1.152q0 0.936 0.684 1.62t1.62 0.684h4.608q0.936 0 1.62 -0.684t0.684 -1.62v-11.52h18.432v11.52q0 0.936 0.684 1.62t1.62 0.684h4.608q0.936 0 1.62 -0.684t0.684 -1.62v-32.256q0 -0.936 -0.684 -1.62t-1.62 -0.684h-4.608q-0.936 0 -1.62 0.684t-0.684 1.62v11.52h-18.432v-11.52q0 -0.936 -0.684 -1.62t-1.62 -0.684h-4.608q-0.936 0 -1.62 0.684t-0.684 1.62v32.256z","PLUS_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 -14.976q0 0.936 0.684 1.62t1.62 0.684h11.52v11.52q0 0.936 0.684 1.62t1.62 0.684h4.608q0.936 0 1.62 -0.684t0.684 -1.62v-11.52h11.52q0.936 0 1.62 -0.684t0.684 -1.62v-4.608q0 -0.936 -0.684 -1.62t-1.62 -0.684h-11.52v-11.52q0 -0.936 -0.684 -1.62t-1.62 -0.684h-4.608q-0.936 0 -1.62 0.684t-0.684 1.62v11.52h-11.52q-0.936 0 -1.62 0.684t-0.684 1.62v4.608z","ANGLE_DOUBLE_LEFT":"M1.62 -29.879q0 -0.468 0.36 -0.828l16.776 -16.776q0.36 -0.36 0.828 -0.36t0.828 0.36l1.8 1.8q0.36 0.36 0.36 0.828t-0.36 0.828l-14.148 14.148 14.148 14.148q0.36 0.36 0.36 0.828t-0.36 0.828l-1.8 1.8q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-16.776 -16.776q-0.36 -0.36 -0.36 -0.828zm13.824 0q0 -0.468 0.36 -0.828l16.776 -16.776q0.36 -0.36 0.828 -0.36t0.828 0.36l1.8 1.8q0.36 0.36 0.36 0.828t-0.36 0.828l-14.148 14.148 14.148 14.148q0.36 0.36 0.36 0.828t-0.36 0.828l-1.8 1.8q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-16.776 -16.776q-0.36 -0.36 -0.36 -0.828z","ANGLE_DOUBLE_RIGHT":"M0.468 -14.903q0 -0.468 0.36 -0.828l14.148 -14.148 -14.148 -14.148q-0.36 -0.36 -0.36 -0.828t0.36 -0.828l1.8 -1.8q0.36 -0.36 0.828 -0.36t0.828 0.36l16.776 16.776q0.36 0.36 0.36 0.828t-0.36 0.828l-16.776 16.776q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-1.8 -1.8q-0.36 -0.36 -0.36 -0.828zm13.824 0q0 -0.468 0.36 -0.828l14.148 -14.148 -14.148 -14.148q-0.36 -0.36 -0.36 -0.828t0.36 -0.828l1.8 -1.8q0.36 -0.36 0.828 -0.36t0.828 0.36l16.776 16.776q0.36 0.36 0.36 0.828t-0.36 0.828l-16.776 16.776q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-1.8 -1.8q-0.36 -0.36 -0.36 -0.828z","ANGLE_DOUBLE_UP":"M2.772 -17.207q0 -0.468 0.36 -0.828l16.776 -16.776q0.36 -0.36 0.828 -0.36t0.828 0.36l16.776 16.776q0.36 0.36 0.36 0.828t-0.36 0.828l-1.8 1.8q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-14.148 -14.148 -14.148 14.148q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-1.8 -1.8q-0.36 -0.36 -0.36 -0.828zm0 -13.824q0 -0.468 0.36 -0.828l16.776 -16.776q0.36 -0.36 0.828 -0.36t0.828 0.36l16.776 16.776q0.36 0.36 0.36 0.828t-0.36 0.828l-1.8 1.8q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-14.148 -14.148 -14.148 14.148q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-1.8 -1.8q-0.36 -0.36 -0.36 -0.828z","ANGLE_DOUBLE_DOWN":"M2.772 -33.335q0 -0.468 0.36 -0.828l1.8 -1.8q0.36 -0.36 0.828 -0.36t0.828 0.36l14.148 14.148 14.148 -14.148q0.36 -0.36 0.828 -0.36t0.828 0.36l1.8 1.8q0.36 0.36 0.36 0.828t-0.36 0.828l-16.776 16.776q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-16.776 -16.776q-0.36 -0.36 -0.36 -0.828zm0 -13.824q0 -0.468 0.36 -0.828l1.8 -1.8q0.36 -0.36 0.828 -0.36t0.828 0.36l14.148 14.148 14.148 -14.148q0.36 -0.36 0.828 -0.36t0.828 0.36l1.8 1.8q0.36 0.36 0.36 0.828t-0.36 0.828l-16.776 16.776q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-16.776 -16.776q-0.36 -0.36 -0.36 -0.828z","ANGLE_LEFT":"M1.62 -29.879q0 -0.468 0.36 -0.828l16.776 -16.776q0.36 -0.36 0.828 -0.36t0.828 0.36l1.8 1.8q0.36 0.36 0.36 0.828t-0.36 0.828l-14.148 14.148 14.148 14.148q0.36 0.36 0.36 0.828t-0.36 0.828l-1.8 1.8q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-16.776 -16.776q-0.36 -0.36 -0.36 -0.828z","ANGLE_RIGHT":"M0.468 -14.903q0 -0.468 0.36 -0.828l14.148 -14.148 -14.148 -14.148q-0.36 -0.36 -0.36 -0.828t0.36 -0.828l1.8 -1.8q0.36 -0.36 0.828 -0.36t0.828 0.36l16.776 16.776q0.36 0.36 0.36 0.828t-0.36 0.828l-16.776 16.776q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-1.8 -1.8q-0.36 -0.36 -0.36 -0.828z","ANGLE_UP":"M2.772 -21.815q0 -0.468 0.36 -0.828l16.776 -16.776q0.36 -0.36 0.828 -0.36t0.828 0.36l16.776 16.776q0.36 0.36 0.36 0.828t-0.36 0.828l-1.8 1.8q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-14.148 -14.148 -14.148 14.148q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-1.8 -1.8q-0.36 -0.36 -0.36 -0.828z","ANGLE_DOWN":"M2.772 -37.943q0 -0.468 0.36 -0.828l1.8 -1.8q0.36 -0.36 0.828 -0.36t0.828 0.36l14.148 14.148 14.148 -14.148q0.36 -0.36 0.828 -0.36t0.828 0.36l1.8 1.8q0.36 0.36 0.36 0.828t-0.36 0.828l-16.776 16.776q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-16.776 -16.776q-0.36 -0.36 -0.36 -0.828z","DESKTOP":"M0 -19.511v-39.168q0 -2.376 1.692 -4.068t4.068 -1.692h57.6q2.376 0 4.068 1.692t1.692 4.068v39.168q0 2.376 -1.692 4.068t-4.068 1.692h-19.584q0 1.332 0.576 2.79t1.152 2.556 0.576 1.566q0 0.936 -0.684 1.62t-1.62 0.684h-18.432q-0.936 0 -1.62 -0.684t-0.684 -1.62q0 -0.504 0.576 -1.584t1.152 -2.52 0.576 -2.808h-19.584q-2.376 0 -4.068 -1.692t-1.692 -4.068zm4.608 -9.216q0 0.468 0.342 0.81t0.81 0.342h57.6q0.468 0 0.81 -0.342t0.342 -0.81v-29.952q0 -0.468 -0.342 -0.81t-0.81 -0.342h-57.6q-0.468 0 -0.81 0.342t-0.342 0.81v29.952z","LAPTOP":"M0 -12.599v-3.456h69.12v3.456q0 1.44 -1.692 2.448t-4.068 1.008h-57.6q-2.376 0 -4.068 -1.008t-1.692 -2.448zm9.216 -11.52v-25.344q0 -2.376 1.692 -4.068t4.068 -1.692h39.168q2.376 0 4.068 1.692t1.692 4.068v25.344q0 2.376 -1.692 4.068t-4.068 1.692h-39.168q-2.376 0 -4.068 -1.692t-1.692 -4.068zm4.608 0q0 0.468 0.342 0.81t0.81 0.342h39.168q0.468 0 0.81 -0.342t0.342 -0.81v-25.344q0 -0.468 -0.342 -0.81t-0.81 -0.342h-39.168q-0.468 0 -0.81 0.342t-0.342 0.81v25.344zm17.28 10.944q0 0.576 0.576 0.576h5.76q0.576 0 0.576 -0.576t-0.576 -0.576h-5.76q-0.576 0 -0.576 0.576z","TABLET":"M0 -14.903v-39.168q0 -2.376 1.692 -4.068t4.068 -1.692h29.952q2.376 0 4.068 1.692t1.692 4.068v39.168q0 2.376 -1.692 4.068t-4.068 1.692h-29.952q-2.376 0 -4.068 -1.692t-1.692 -4.068zm4.608 -4.608q0 0.468 0.342 0.81t0.81 0.342h29.952q0.468 0 0.81 -0.342t0.342 -0.81v-34.56q0 -0.468 -0.342 -0.81t-0.81 -0.342h-29.952q-0.468 0 -0.81 0.342t-0.342 0.81v34.56zm13.824 5.76q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62z","MOBILE_PHONE":"M0 -13.751v-36.864q0 -1.872 1.368 -3.24t3.24 -1.368h18.432q1.872 0 3.24 1.368t1.368 3.24v36.864q0 1.872 -1.368 3.24t-3.24 1.368h-18.432q-1.872 0 -3.24 -1.368t-1.368 -3.24zm3.456 -5.76q0 0.468 0.342 0.81t0.81 0.342h18.432q0.468 0 0.81 -0.342t0.342 -0.81v-25.344q0 -0.468 -0.342 -0.81t-0.81 -0.342h-18.432q-0.468 0 -0.81 0.342t-0.342 0.81v25.344zm6.912 -30.528q0 0.576 0.576 0.576h5.76q0.576 0 0.576 -0.576t-0.576 -0.576h-5.76q-0.576 0 -0.576 0.576zm0.576 36.288q0 1.188 0.846 2.034t2.034 0.846 2.034 -0.846 0.846 -2.034 -0.846 -2.034 -2.034 -0.846 -2.034 0.846 -0.846 2.034z","CIRCLE_O":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm8.064 0q0 5.328 2.628 9.828t7.128 7.128 9.828 2.628 9.828 -2.628 7.128 -7.128 2.628 -9.828 -2.628 -9.828 -7.128 -7.128 -9.828 -2.628 -9.828 2.628 -7.128 7.128 -2.628 9.828z","QUOTE_LEFT":"M0 -16.055v-25.344q0 -3.744 1.458 -7.146t3.942 -5.886 5.886 -3.942 7.146 -1.458h2.304q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-2.304q-3.816 0 -6.516 2.7t-2.7 6.516v1.152q0 1.44 1.008 2.448t2.448 1.008h8.064q2.88 0 4.896 2.016t2.016 4.896v13.824q0 2.88 -2.016 4.896t-4.896 2.016h-13.824q-2.88 0 -4.896 -2.016t-2.016 -4.896zm32.256 0v-25.344q0 -3.744 1.458 -7.146t3.942 -5.886 5.886 -3.942 7.146 -1.458h2.304q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-2.304q-3.816 0 -6.516 2.7t-2.7 6.516v1.152q0 1.44 1.008 2.448t2.448 1.008h8.064q2.88 0 4.896 2.016t2.016 4.896v13.824q0 2.88 -2.016 4.896t-4.896 2.016h-13.824q-2.88 0 -4.896 -2.016t-2.016 -4.896z","QUOTE_RIGHT":"M0 -39.095v-13.824q0 -2.88 2.016 -4.896t4.896 -2.016h13.824q2.88 0 4.896 2.016t2.016 4.896v25.344q0 3.744 -1.458 7.146t-3.942 5.886 -5.886 3.942 -7.146 1.458h-2.304q-0.936 0 -1.62 -0.684t-0.684 -1.62v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h2.304q3.816 0 6.516 -2.7t2.7 -6.516v-1.152q0 -1.44 -1.008 -2.448t-2.448 -1.008h-8.064q-2.88 0 -4.896 -2.016t-2.016 -4.896zm32.256 0v-13.824q0 -2.88 2.016 -4.896t4.896 -2.016h13.824q2.88 0 4.896 2.016t2.016 4.896v25.344q0 3.744 -1.458 7.146t-3.942 5.886 -5.886 3.942 -7.146 1.458h-2.304q-0.936 0 -1.62 -0.684t-0.684 -1.62v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h2.304q3.816 0 6.516 -2.7t2.7 -6.516v-1.152q0 -1.44 -1.008 -2.448t-2.448 -1.008h-8.064q-2.88 0 -4.896 -2.016t-2.016 -4.896z","SPINNER":"M0 -32.183q0 -2.376 1.692 -4.068t4.068 -1.692 4.068 1.692 1.692 4.068 -1.692 4.068 -4.068 1.692 -4.068 -1.692 -1.692 -4.068zm6.336 -16.128q0 -2.628 1.854 -4.482t4.482 -1.854 4.482 1.854 1.854 4.482 -1.854 4.482 -4.482 1.854 -4.482 -1.854 -1.854 -4.482zm1.152 32.256q0 -2.16 1.512 -3.672t3.672 -1.512q2.124 0 3.654 1.512t1.53 3.672 -1.53 3.672 -3.654 1.512q-2.16 0 -3.672 -1.512t-1.512 -3.672zm14.4 -39.168q0 -2.88 2.016 -4.896t4.896 -2.016 4.896 2.016 2.016 4.896 -2.016 4.896 -4.896 2.016 -4.896 -2.016 -2.016 -4.896zm2.304 46.08q0 -1.908 1.35 -3.258t3.258 -1.35 3.258 1.35 1.35 3.258 -1.35 3.258 -3.258 1.35 -3.258 -1.35 -1.35 -3.258zm16.704 -6.912q0 -1.656 1.188 -2.844t2.844 -1.188 2.844 1.188 1.188 2.844 -1.188 2.844 -2.844 1.188 -2.844 -1.188 -1.188 -2.844zm1.152 -32.256q0 -1.188 0.846 -2.034t2.034 -0.846 2.034 0.846 0.846 2.034 -0.846 2.034 -2.034 0.846 -2.034 -0.846 -0.846 -2.034zm6.336 16.128q0 -1.44 1.008 -2.448t2.448 -1.008 2.448 1.008 1.008 2.448 -1.008 2.448 -2.448 1.008 -2.448 -1.008 -1.008 -2.448z","CIRCLE":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878z","REPLY":"M0 -41.399q0 -0.936 0.684 -1.62l18.432 -18.432q0.684 -0.684 1.62 -0.684t1.62 0.684 0.684 1.62v9.216h8.064q25.668 0 31.5 14.508 1.908 4.824 1.908 11.988 0 5.976 -4.572 16.236 -0.108 0.252 -0.378 0.864t-0.486 1.08 -0.468 0.792q-0.432 0.612 -1.008 0.612 -0.54 0 -0.846 -0.36t-0.306 -0.9q0 -0.324 0.09 -0.954t0.09 -0.846q0.18 -2.448 0.18 -4.428 0 -3.636 -0.63 -6.516t-1.746 -4.986 -2.88 -3.636 -3.798 -2.502 -4.788 -1.53 -5.544 -0.774 -6.318 -0.216h-8.064v9.216q0 0.936 -0.684 1.62t-1.62 0.684 -1.62 -0.684l-18.432 -18.432q-0.684 -0.684 -0.684 -1.62z","GITHUB_ALT":"M0 -26.999q0 -8.532 4.896 -14.256 -0.972 -2.952 -0.972 -6.12 0 -4.176 1.836 -7.848 3.888 0 6.84 1.422t6.804 4.446q5.292 -1.26 11.124 -1.26 5.328 0 10.08 1.152 3.78 -2.952 6.732 -4.356t6.804 -1.404q1.836 3.672 1.836 7.848 0 3.132 -0.972 6.048 4.896 5.76 4.896 14.328 0 7.452 -2.196 11.916 -1.368 2.772 -3.798 4.788t-5.076 3.096 -6.12 1.71 -6.174 0.792 -6.012 0.162q-2.808 0 -5.112 -0.108t-5.31 -0.45 -5.49 -1.08 -4.932 -1.854 -4.356 -2.916 -3.096 -4.14q-2.232 -4.428 -2.232 -11.916zm8.064 6.336q0 3.168 1.152 5.526t2.916 3.708 4.392 2.16 5.04 1.062 5.364 0.252h6.048q2.952 0 5.364 -0.252t5.04 -1.062 4.392 -2.16 2.916 -3.708 1.152 -5.526q0 -4.32 -2.484 -7.344t-6.732 -3.024q-1.476 0 -7.02 0.756 -2.556 0.396 -5.652 0.396t-5.652 -0.396q-5.472 -0.756 -7.02 -0.756 -4.248 0 -6.732 3.024t-2.484 7.344zm5.76 0q0 -1.44 0.45 -2.952t1.548 -2.736 2.61 -1.224 2.61 1.224 1.548 2.736 0.45 2.952 -0.45 2.952 -1.548 2.736 -2.61 1.224 -2.61 -1.224 -1.548 -2.736 -0.45 -2.952zm23.04 0q0 -1.44 0.45 -2.952t1.548 -2.736 2.61 -1.224 2.61 1.224 1.548 2.736 0.45 2.952 -0.45 2.952 -1.548 2.736 -2.61 1.224 -2.61 -1.224 -1.548 -2.736 -0.45 -2.952z","FOLDER_O":"M0 -17.207v-34.56q0 -3.312 2.376 -5.688t5.688 -2.376h11.52q3.312 0 5.688 2.376t2.376 5.688v1.152h24.192q3.312 0 5.688 2.376t2.376 5.688v25.344q0 3.312 -2.376 5.688t-5.688 2.376h-43.776q-3.312 0 -5.688 -2.376t-2.376 -5.688zm4.608 0q0 1.44 1.008 2.448t2.448 1.008h43.776q1.44 0 2.448 -1.008t1.008 -2.448v-25.344q0 -1.44 -1.008 -2.448t-2.448 -1.008h-25.344q-1.44 0 -2.448 -1.008t-1.008 -2.448v-2.304q0 -1.44 -1.008 -2.448t-2.448 -1.008h-11.52q-1.44 0 -2.448 1.008t-1.008 2.448v34.56z","FOLDER_OPEN_O":"M0 -17.207v-34.56q0 -3.312 2.376 -5.688t5.688 -2.376h11.52q3.312 0 5.688 2.376t2.376 5.688v1.152h19.584q3.312 0 5.688 2.376t2.376 5.688v5.76h6.912q1.944 0 3.564 0.882t2.412 2.538q0.54 1.152 0.54 2.448 0 2.232 -1.656 4.32l-10.62 13.068q-1.548 1.908 -4.176 3.15t-5.04 1.242h-39.168q-3.312 0 -5.688 -2.376t-2.376 -5.688zm4.608 -3.852l9.216 -11.34q1.584 -1.908 4.176 -3.15t5.04 -1.242h27.648v-5.76q0 -1.44 -1.008 -2.448t-2.448 -1.008h-20.736q-1.44 0 -2.448 -1.008t-1.008 -2.448v-2.304q0 -1.44 -1.008 -2.448t-2.448 -1.008h-11.52q-1.44 0 -2.448 1.008t-1.008 2.448v30.708zm1.548 6.048q0 1.26 1.908 1.26h39.168q1.44 0 3.096 -0.792t2.556 -1.908l10.584 -13.068q0.648 -0.792 0.648 -1.404 0 -1.26 -1.908 -1.26h-39.168q-1.44 0 -3.078 0.774t-2.574 1.89l-10.584 13.068q-0.648 0.864 -0.648 1.44z","SMILE_O":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm4.608 0q0 4.68 1.836 8.946t4.914 7.344 7.344 4.914 8.946 1.836 8.946 -1.836 7.344 -4.914 4.914 -7.344 1.836 -8.946 -1.836 -8.946 -4.914 -7.344 -7.344 -4.914 -8.946 -1.836 -8.946 1.836 -7.344 4.914 -4.914 7.344 -1.836 8.946zm9.216 -9.216q0 -1.908 1.35 -3.258t3.258 -1.35 3.258 1.35 1.35 3.258 -1.35 3.258 -3.258 1.35 -3.258 -1.35 -1.35 -3.258zm0.648 15.66q-0.288 -0.9 0.144 -1.746t1.368 -1.134q0.9 -0.288 1.746 0.144t1.134 1.368q0.9 2.88 3.33 4.662t5.454 1.782 5.454 -1.782 3.33 -4.662q0.288 -0.936 1.152 -1.368t1.764 -0.144 1.332 1.134 0.144 1.746q-1.332 4.356 -4.968 7.02t-8.208 2.664 -8.208 -2.664 -4.968 -7.02zm17.784 -15.66q0 -1.908 1.35 -3.258t3.258 -1.35 3.258 1.35 1.35 3.258 -1.35 3.258 -3.258 1.35 -3.258 -1.35 -1.35 -3.258z","FROWN_O":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm4.608 0q0 4.68 1.836 8.946t4.914 7.344 7.344 4.914 8.946 1.836 8.946 -1.836 7.344 -4.914 4.914 -7.344 1.836 -8.946 -1.836 -8.946 -4.914 -7.344 -7.344 -4.914 -8.946 -1.836 -8.946 1.836 -7.344 4.914 -4.914 7.344 -1.836 8.946zm9.216 -9.216q0 -1.908 1.35 -3.258t3.258 -1.35 3.258 1.35 1.35 3.258 -1.35 3.258 -3.258 1.35 -3.258 -1.35 -1.35 -3.258zm0.648 21.204q1.332 -4.356 4.968 -7.02t8.208 -2.664 8.208 2.664 4.968 7.02q0.288 0.9 -0.144 1.746t-1.332 1.134 -1.764 -0.144 -1.152 -1.368q-0.9 -2.88 -3.33 -4.662t-5.454 -1.782 -5.454 1.782 -3.33 4.662q-0.288 0.936 -1.134 1.368t-1.746 0.144q-0.936 -0.288 -1.368 -1.134t-0.144 -1.746zm17.784 -21.204q0 -1.908 1.35 -3.258t3.258 -1.35 3.258 1.35 1.35 3.258 -1.35 3.258 -3.258 1.35 -3.258 -1.35 -1.35 -3.258z","MEH_O":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm4.608 0q0 4.68 1.836 8.946t4.914 7.344 7.344 4.914 8.946 1.836 8.946 -1.836 7.344 -4.914 4.914 -7.344 1.836 -8.946 -1.836 -8.946 -4.914 -7.344 -7.344 -4.914 -8.946 -1.836 -8.946 1.836 -7.344 4.914 -4.914 7.344 -1.836 8.946zm9.216 6.912q0 -0.936 0.684 -1.62t1.62 -0.684h23.04q0.936 0 1.62 0.684t0.684 1.62 -0.684 1.62 -1.62 0.684h-23.04q-0.936 0 -1.62 -0.684t-0.684 -1.62zm0 -16.128q0 -1.908 1.35 -3.258t3.258 -1.35 3.258 1.35 1.35 3.258 -1.35 3.258 -3.258 1.35 -3.258 -1.35 -1.35 -3.258zm18.432 0q0 -1.908 1.35 -3.258t3.258 -1.35 3.258 1.35 1.35 3.258 -1.35 3.258 -3.258 1.35 -3.258 -1.35 -1.35 -3.258z","GAMEPAD":"M0 -27.575q0 -7.632 5.4 -13.032t13.032 -5.4h32.256q7.632 0 13.032 5.4t5.4 13.032 -5.4 13.032 -13.032 5.4q-6.912 0 -12.168 -4.608h-7.92q-5.256 4.608 -12.168 4.608 -7.632 0 -13.032 -5.4t-5.4 -13.032zm6.912 2.304q0 0.504 0.324 0.828t0.828 0.324h6.912v6.912q0 0.504 0.324 0.828t0.828 0.324h4.608q0.504 0 0.828 -0.324t0.324 -0.828v-6.912h6.912q0.504 0 0.828 -0.324t0.324 -0.828v-4.608q0 -0.504 -0.324 -0.828t-0.828 -0.324h-6.912v-6.912q0 -0.504 -0.324 -0.828t-0.828 -0.324h-4.608q-0.504 0 -0.828 0.324t-0.324 0.828v6.912h-6.912q-0.504 0 -0.828 0.324t-0.324 0.828v4.608zm34.56 2.304q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258 -1.35 -3.258 -3.258 -1.35 -3.258 1.35 -1.35 3.258zm9.216 -9.216q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258 -1.35 -3.258 -3.258 -1.35 -3.258 1.35 -1.35 3.258z","KEYBOARD_O":"M0 -13.751v-32.256q0 -1.908 1.35 -3.258t3.258 -1.35h59.904q1.908 0 3.258 1.35t1.35 3.258v32.256q0 1.908 -1.35 3.258t-3.258 1.35h-59.904q-1.908 0 -3.258 -1.35t-1.35 -3.258zm4.608 0h59.904v-32.256h-59.904v32.256zm4.608 -5.184v-3.456q0 -0.576 0.576 -0.576h3.456q0.576 0 0.576 0.576v3.456q0 0.576 -0.576 0.576h-3.456q-0.576 0 -0.576 -0.576zm0 -9.216v-3.456q0 -0.576 0.576 -0.576h8.064q0.576 0 0.576 0.576v3.456q0 0.576 -0.576 0.576h-8.064q-0.576 0 -0.576 -0.576zm0 -9.216v-3.456q0 -0.576 0.576 -0.576h3.456q0.576 0 0.576 0.576v3.456q0 0.576 -0.576 0.576h-3.456q-0.576 0 -0.576 -0.576zm9.216 18.432v-3.456q0 -0.576 0.576 -0.576h31.104q0.576 0 0.576 0.576v3.456q0 0.576 -0.576 0.576h-31.104q-0.576 0 -0.576 -0.576zm0 -18.432v-3.456q0 -0.576 0.576 -0.576h3.456q0.576 0 0.576 0.576v3.456q0 0.576 -0.576 0.576h-3.456q-0.576 0 -0.576 -0.576zm4.608 9.216v-3.456q0 -0.576 0.576 -0.576h3.456q0.576 0 0.576 0.576v3.456q0 0.576 -0.576 0.576h-3.456q-0.576 0 -0.576 -0.576zm4.608 -9.216v-3.456q0 -0.576 0.576 -0.576h3.456q0.576 0 0.576 0.576v3.456q0 0.576 -0.576 0.576h-3.456q-0.576 0 -0.576 -0.576zm4.608 9.216v-3.456q0 -0.576 0.576 -0.576h3.456q0.576 0 0.576 0.576v3.456q0 0.576 -0.576 0.576h-3.456q-0.576 0 -0.576 -0.576zm4.608 -9.216v-3.456q0 -0.576 0.576 -0.576h3.456q0.576 0 0.576 0.576v3.456q0 0.576 -0.576 0.576h-3.456q-0.576 0 -0.576 -0.576zm4.608 9.216v-3.456q0 -0.576 0.576 -0.576h3.456q0.576 0 0.576 0.576v3.456q0 0.576 -0.576 0.576h-3.456q-0.576 0 -0.576 -0.576zm4.608 -9.216v-3.456q0 -0.576 0.576 -0.576h3.456q0.576 0 0.576 0.576v3.456q0 0.576 -0.576 0.576h-3.456q-0.576 0 -0.576 -0.576zm4.608 9.216v-3.456q0 -0.576 0.576 -0.576h4.032v-8.64q0 -0.576 0.576 -0.576h3.456q0.576 0 0.576 0.576v12.672q0 0.576 -0.576 0.576h-8.064q-0.576 0 -0.576 -0.576zm4.608 9.216v-3.456q0 -0.576 0.576 -0.576h3.456q0.576 0 0.576 0.576v3.456q0 0.576 -0.576 0.576h-3.456q-0.576 0 -0.576 -0.576z","FLAG_O":"M2.304 -55.223q0 -1.908 1.35 -3.258t3.258 -1.35 3.258 1.35 1.35 3.258q0 1.26 -0.63 2.304t-1.674 1.656v45.576q0 0.504 -0.324 0.828t-0.828 0.324h-2.304q-0.504 0 -0.828 -0.324t-0.324 -0.828v-45.576q-1.044 -0.612 -1.674 -1.656t-0.63 -2.304zm9.216 34.56v-26.712q0 -1.26 1.116 -1.98 1.26 -0.756 2.826 -1.53t4.104 -1.872 5.49 -1.782 5.58 -0.684q4.032 0 7.524 1.116t7.524 3.096q1.368 0.684 3.204 0.684 4.392 0 11.16 -4.032l1.116 -0.612q1.116 -0.576 2.232 0.072 1.116 0.72 1.116 1.98v27.468q0 1.404 -1.26 2.052 -0.36 0.18 -0.612 0.324 -7.848 4.176 -13.284 4.176 -3.168 0 -5.688 -1.26l-1.008 -0.504q-2.304 -1.188 -3.564 -1.728t-3.276 -1.044 -4.104 -0.504q-3.672 0 -8.478 1.584t-8.226 3.672q-0.54 0.324 -1.188 0.324 -0.576 0 -1.152 -0.288 -1.152 -0.684 -1.152 -2.016zm4.608 -3.816q8.82 -4.068 15.588 -4.068 1.98 0 3.726 0.27t3.528 0.936 2.772 1.116 2.97 1.422l1.008 0.504q1.584 0.792 3.636 0.792 4.32 0 10.548 -3.312v-22.176q-6.084 3.276 -11.016 3.276 -2.952 0 -5.22 -1.152 -3.6 -1.764 -6.624 -2.754t-6.408 -0.99q-6.228 0 -14.508 4.572v21.564z","FLAG_CHECKERED":"M2.304 -55.223q0 -1.908 1.35 -3.258t3.258 -1.35 3.258 1.35 1.35 3.258q0 1.26 -0.63 2.304t-1.674 1.656v45.576q0 0.504 -0.324 0.828t-0.828 0.324h-2.304q-0.504 0 -0.828 -0.324t-0.324 -0.828v-45.576q-1.044 -0.612 -1.674 -1.656t-0.63 -2.304zm9.216 34.56v-26.712q0 -1.26 1.116 -1.98 1.26 -0.756 2.826 -1.53t4.104 -1.872 5.49 -1.782 5.58 -0.684q4.032 0 7.524 1.116t7.524 3.096q1.368 0.684 3.204 0.684 4.392 0 11.16 -4.032l1.116 -0.612q1.116 -0.576 2.232 0.072 1.116 0.72 1.116 1.98v27.468q0 1.404 -1.26 2.052 -0.36 0.18 -0.612 0.324 -7.848 4.176 -13.284 4.176 -3.168 0 -5.688 -1.26l-1.008 -0.504q-2.304 -1.188 -3.564 -1.728t-3.276 -1.044 -4.104 -0.504q-3.672 0 -8.478 1.584t-8.226 3.672q-0.54 0.324 -1.188 0.324 -0.576 0 -1.152 -0.288 -1.152 -0.684 -1.152 -2.016zm4.608 -3.816q7.38 -3.456 13.824 -3.96v-6.912q-6.516 0.576 -13.824 4.212v6.66zm0 -14.76q7.74 -3.996 13.824 -4.248v-7.092q-6.192 0.288 -13.824 4.536v6.804zm13.824 3.816h0.684q3.672 0 6.93 1.044t7.11 2.952q0.684 0.324 1.404 0.54v6.768q1.512 0.612 3.276 0.612 4.32 0 10.548 -3.312v-6.624q-8.46 4.176 -13.824 2.556v-8.064q-0.72 -0.216 -1.404 -0.54 -0.18 -0.108 -1.188 -0.612t-1.242 -0.612 -1.134 -0.54 -1.242 -0.558 -1.17 -0.468 -1.296 -0.45 -1.26 -0.306 -1.422 -0.27 -1.422 -0.144 -1.584 -0.072q-0.828 0 -1.764 0.108v7.992zm16.128 -3.528q5.328 1.512 13.824 -3.24v-6.804q-6.084 3.276 -11.016 3.276 -1.62 0 -2.808 -0.288v7.056z","TERMINAL":"M0.468 -14.903q0 -0.468 0.36 -0.828l14.148 -14.148 -14.148 -14.148q-0.36 -0.36 -0.36 -0.828t0.36 -0.828l1.8 -1.8q0.36 -0.36 0.828 -0.36t0.828 0.36l16.776 16.776q0.36 0.36 0.36 0.828t-0.36 0.828l-16.776 16.776q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-1.8 -1.8q-0.36 -0.36 -0.36 -0.828zm22.572 4.608v-2.304q0 -0.504 0.324 -0.828t0.828 -0.324h34.56q0.504 0 0.828 0.324t0.324 0.828v2.304q0 0.504 -0.324 0.828t-0.828 0.324h-34.56q-0.504 0 -0.828 -0.324t-0.324 -0.828z","CODE":"M1.62 -29.879q0 -0.468 0.36 -0.828l16.776 -16.776q0.36 -0.36 0.828 -0.36t0.828 0.36l1.8 1.8q0.36 0.36 0.36 0.828t-0.36 0.828l-14.148 14.148 14.148 14.148q0.36 0.36 0.36 0.828t-0.36 0.828l-1.8 1.8q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-16.776 -16.776q-0.36 -0.36 -0.36 -0.828zm24.012 22.608l13.428 -46.476q0.144 -0.468 0.558 -0.702t0.846 -0.09l2.232 0.612q0.468 0.144 0.702 0.558t0.09 0.882l-13.428 46.476q-0.144 0.468 -0.558 0.702t-0.846 0.09l-2.232 -0.612q-0.468 -0.144 -0.702 -0.558t-0.09 -0.882zm20.916 -7.632q0 -0.468 0.36 -0.828l14.148 -14.148 -14.148 -14.148q-0.36 -0.36 -0.36 -0.828t0.36 -0.828l1.8 -1.8q0.36 -0.36 0.828 -0.36t0.828 0.36l16.776 16.776q0.36 0.36 0.36 0.828t-0.36 0.828l-16.776 16.776q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-1.8 -1.8q-0.36 -0.36 -0.36 -0.828z","REPLY_ALL":"M0 -41.399q0 -0.936 0.684 -1.62l18.432 -18.432q1.044 -1.116 2.52 -0.504 1.404 0.612 1.404 2.124v2.484l-14.292 14.328q-0.684 0.684 -0.684 1.62t0.684 1.62l14.292 14.292v2.52q0 1.512 -1.404 2.124 -0.468 0.18 -0.9 0.18 -0.972 0 -1.62 -0.684l-18.432 -18.432q-0.684 -0.684 -0.684 -1.62zm13.824 0q0 -0.936 0.684 -1.62l18.432 -18.432q1.044 -1.116 2.52 -0.504 1.404 0.612 1.404 2.124v9.432q14.796 1.008 21.564 7.956 6.084 6.228 6.084 18.324 0 2.088 -0.612 4.806t-1.386 4.968 -1.728 4.5 -1.458 3.258l-0.72 1.44q-0.288 0.612 -1.008 0.612 -0.216 0 -0.324 -0.036 -0.9 -0.288 -0.828 -1.224 1.548 -14.4 -3.816 -20.34 -2.304 -2.556 -6.138 -3.978t-9.63 -1.89v9.036q0 1.512 -1.404 2.124 -0.468 0.18 -0.9 0.18 -0.972 0 -1.62 -0.684l-18.432 -18.432q-0.684 -0.684 -0.684 -1.62z","STAR_HALF_O":"M0.072 -41.561q0.324 -0.99 1.944 -1.242l18.072 -2.628 8.1 -16.38q0.72 -1.476 1.764 -1.476 1.008 0 1.764 1.476l8.1 16.38 18.072 2.628q1.62 0.252 1.944 1.242t-0.864 2.142l-13.068 12.744 3.096 18q0.18 1.188 -0.216 1.854t-1.224 0.666q-0.612 0 -1.44 -0.432l-16.164 -8.496 -16.164 8.496q-0.828 0.432 -1.44 0.432 -0.828 0 -1.224 -0.666t-0.216 -1.854l3.096 -18 -13.104 -12.744q-1.152 -1.152 -0.828 -2.142zm29.88 21.258l2.124 1.116 11.448 6.048 -2.16 -12.78 -0.432 -2.376 1.764 -1.692 9.252 -9 -12.816 -1.872 -2.376 -0.36 -1.08 -2.16 -5.724 -11.592v34.668z","LOCATION_ARROW":"M0.072 -29.339q-0.18 -0.792 0.144 -1.512t1.044 -1.08l46.08 -23.04q0.468 -0.252 1.044 -0.252 0.972 0 1.62 0.684 0.54 0.504 0.666 1.242t-0.234 1.422l-23.04 46.08q-0.612 1.26 -2.052 1.26 -0.18 0 -0.54 -0.072 -0.792 -0.18 -1.278 -0.81t-0.486 -1.422v-20.736h-20.736q-0.792 0 -1.422 -0.486t-0.81 -1.278z","CROP":"M0 -42.551v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324h8.064v-8.064q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v8.064h30.636l8.856 -8.892q0.36 -0.324 0.828 -0.324t0.828 0.324q0.324 0.36 0.324 0.828t-0.324 0.828l-8.892 8.856v30.636h8.064q0.504 0 0.828 0.324t0.324 0.828v6.912q0 0.504 -0.324 0.828t-0.828 0.324h-8.064v8.064q0 0.504 -0.324 0.828t-0.828 0.324h-6.912q-0.504 0 -0.828 -0.324t-0.324 -0.828v-8.064h-31.104q-0.504 0 -0.828 -0.324t-0.324 -0.828v-31.104h-8.064q-0.504 0 -0.828 -0.324t-0.324 -0.828zm18.432 22.572l21.42 -21.42h-21.42v21.42zm1.62 1.62h21.42v-21.42z","CODE_FORK":"M0 -11.447q0 -1.872 0.936 -3.474t2.52 -2.502v-29.52q-1.584 -0.9 -2.52 -2.502t-0.936 -3.474q0 -2.88 2.016 -4.896t4.896 -2.016 4.896 2.016 2.016 4.896q0 1.872 -0.936 3.474t-2.52 2.502v17.892q1.944 -0.936 5.544 -2.052 1.98 -0.612 3.15 -1.062t2.538 -1.116 2.124 -1.422 1.458 -1.836 1.008 -2.502 0.306 -3.294q-1.584 -0.9 -2.52 -2.502t-0.936 -3.474q0 -2.88 2.016 -4.896t4.896 -2.016 4.896 2.016 2.016 4.896q0 1.872 -0.936 3.474t-2.52 2.502q-0.072 10.332 -8.136 14.904 -2.448 1.368 -7.308 2.916 -4.608 1.44 -6.102 2.556t-1.494 3.6v0.936q1.584 0.9 2.52 2.502t0.936 3.474q0 2.88 -2.016 4.896t-4.896 2.016 -4.896 -2.016 -2.016 -4.896zm3.456 0q0 1.44 1.008 2.448t2.448 1.008 2.448 -1.008 1.008 -2.448 -1.008 -2.448 -2.448 -1.008 -2.448 1.008 -1.008 2.448zm0 -41.472q0 1.44 1.008 2.448t2.448 1.008 2.448 -1.008 1.008 -2.448 -1.008 -2.448 -2.448 -1.008 -2.448 1.008 -1.008 2.448zm23.04 4.608q0 1.44 1.008 2.448t2.448 1.008 2.448 -1.008 1.008 -2.448 -1.008 -2.448 -2.448 -1.008 -2.448 1.008 -1.008 2.448z","UNLINK":"M0 -25.271q0 -0.504 0.324 -0.828t0.828 -0.324h11.52q0.504 0 0.828 0.324t0.324 0.828 -0.324 0.828 -0.828 0.324h-11.52q-0.504 0 -0.828 -0.324t-0.324 -0.828zm0.576 -23.04q0 -4.32 3.06 -7.308l5.292 -5.256q2.988 -2.988 7.308 -2.988 4.356 0 7.344 3.06l12.024 12.06q0.756 0.756 1.512 2.016l-8.604 0.648 -9.828 -9.864q-1.008 -1.008 -2.448 -1.008 -1.404 0 -2.448 0.972l-5.292 5.256q-1.008 1.008 -1.008 2.412 0 1.44 1.008 2.448l9.864 9.864 -0.648 8.64q-1.26 -0.756 -2.016 -1.512l-12.096 -12.096q-3.024 -3.096 -3.024 -7.344zm4.032 38.016q0 -0.468 0.324 -0.828l9.216 -9.216q0.36 -0.324 0.828 -0.324t0.828 0.324q0.324 0.36 0.324 0.828t-0.324 0.828l-9.216 9.216q-0.36 0.324 -0.828 0.324 -0.432 0 -0.828 -0.324 -0.324 -0.36 -0.324 -0.828zm14.976 4.608v-11.52q0 -0.504 0.324 -0.828t0.828 -0.324 0.828 0.324 0.324 0.828v11.52q0 0.504 -0.324 0.828t-0.828 0.324 -0.828 -0.324 -0.324 -0.828zm3.204 -16.56l8.604 -0.648 9.828 9.864q0.972 0.972 2.448 0.99t2.448 -0.954l5.292 -5.256q1.008 -1.008 1.008 -2.412 0 -1.44 -1.008 -2.448l-9.864 -9.9 0.648 -8.604q1.26 0.756 2.016 1.512l12.096 12.096q3.024 3.096 3.024 7.344 0 4.32 -3.06 7.308l-5.292 5.256q-2.988 2.988 -7.308 2.988 -4.356 0 -7.344 -3.06l-12.024 -12.06q-0.756 -0.756 -1.512 -2.016zm15.228 -29.52v-11.52q0 -0.504 0.324 -0.828t0.828 -0.324 0.828 0.324 0.324 0.828v11.52q0 0.504 -0.324 0.828t-0.828 0.324 -0.828 -0.324 -0.324 -0.828zm5.76 2.304q0 -0.468 0.324 -0.828l9.216 -9.216q0.36 -0.324 0.828 -0.324t0.828 0.324q0.324 0.36 0.324 0.828t-0.324 0.828l-9.216 9.216q-0.396 0.324 -0.828 0.324t-0.828 -0.324q-0.324 -0.36 -0.324 -0.828zm2.304 5.76q0 -0.504 0.324 -0.828t0.828 -0.324h11.52q0.504 0 0.828 0.324t0.324 0.828 -0.324 0.828 -0.828 0.324h-11.52q-0.504 0 -0.828 -0.324t-0.324 -0.828z","QUESTION":"M3.474 -44.639q-0.09 -0.54 0.198 -1.008 5.76 -9.576 16.704 -9.576 2.88 0 5.796 1.116t5.256 2.988 3.816 4.59 1.476 5.706q0 1.944 -0.558 3.636t-1.26 2.754 -1.98 2.142 -2.07 1.566 -2.196 1.278q-1.476 0.828 -2.466 2.34t-0.99 2.412q0 0.612 -0.432 1.17t-1.008 0.558h-8.64q-0.54 0 -0.918 -0.666t-0.378 -1.35v-1.62q0 -2.988 2.34 -5.634t5.148 -3.906q2.124 -0.972 3.024 -2.016t0.9 -2.736q0 -1.512 -1.674 -2.664t-3.87 -1.152q-2.34 0 -3.888 1.044 -1.26 0.9 -3.852 4.14 -0.468 0.576 -1.116 0.576 -0.432 0 -0.9 -0.288l-5.904 -4.5q-0.468 -0.36 -0.558 -0.9zm10.35 34.056v-8.64q0 -0.576 0.432 -1.008t1.008 -0.432h8.64q0.576 0 1.008 0.432t0.432 1.008v8.64q0 0.576 -0.432 1.008t-1.008 0.432h-8.64q-0.576 0 -1.008 -0.432t-0.432 -1.008z","INFO":"M0 -11.447v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h2.304v-13.824h-2.304q-0.936 0 -1.62 -0.684t-0.684 -1.62v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h13.824q0.936 0 1.62 0.684t0.684 1.62v20.736h2.304q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-18.432q-0.936 0 -1.62 -0.684t-0.684 -1.62zm4.608 -39.168v-6.912q0 -0.936 0.684 -1.62t1.62 -0.684h9.216q0.936 0 1.62 0.684t0.684 1.62v6.912q0 0.936 -0.684 1.62t-1.62 0.684h-9.216q-0.936 0 -1.62 -0.684t-0.684 -1.62z","EXCLAMATION":"M3.528 -57.527q-0.036 -0.936 0.63 -1.62t1.602 -0.684h11.52q0.936 0 1.602 0.684t0.63 1.62l-1.008 27.648q-0.036 0.936 -0.738 1.62t-1.638 0.684h-9.216q-0.936 0 -1.638 -0.684t-0.738 -1.62zm1.08 46.08v-8.064q0 -0.936 0.684 -1.62t1.62 -0.684h9.216q0.936 0 1.62 0.684t0.684 1.62v8.064q0 0.936 -0.684 1.62t-1.62 0.684h-9.216q-0.936 0 -1.62 -0.684t-0.684 -1.62z","SUPERSCRIPT":"M0.18 -9.143v-6.012h4.608l7.092 -10.476 -6.66 -9.792h-4.932v-6.048h9.936l5.004 8.208q0.072 0.144 0.828 1.512 0.288 0.324 0.396 0.756h0.108q0.108 -0.324 0.396 -0.756l0.9 -1.512 5.04 -8.208h9.252v6.048h-4.5l-6.624 9.612 7.344 10.656h3.924v6.012h-8.928l-5.724 -9.072 -0.864 -1.512q-0.288 -0.324 -0.396 -0.756h-0.108l-0.324 0.756q-0.36 0.72 -0.9 1.584l-5.58 9h-9.288zm36.288 -25.668q0 -2.304 0.936 -4.212t2.34 -3.114 3.024 -2.34 3.024 -1.962 2.34 -1.944 0.936 -2.304q0 -1.368 -1.062 -2.25t-2.538 -0.882q-1.836 0 -3.492 1.404 -0.504 0.396 -1.296 1.368l-3.78 -3.312q0.936 -1.332 2.268 -2.376 2.988 -2.34 6.768 -2.34 3.96 0 6.408 2.142t2.448 5.706q0 2.016 -0.882 3.708t-2.232 2.754 -2.934 2.106 -2.952 1.818 -2.358 1.854 -1.098 2.268h8.352v-2.88h4.536v7.416h-18.504l-0.108 -0.972q-0.144 -1.008 -0.144 -1.656z","SUBSCRIPT":"M0.18 -9.143v-6.012h4.608l7.092 -10.476 -6.66 -9.792h-4.932v-6.048h9.936l5.004 8.208q0.072 0.144 0.828 1.512 0.288 0.324 0.396 0.756h0.108q0.108 -0.324 0.396 -0.756l0.9 -1.512 5.04 -8.208h9.252v6.048h-4.5l-6.624 9.612 7.344 10.656h3.924v6.012h-8.928l-5.724 -9.072 -0.864 -1.512q-0.288 -0.324 -0.396 -0.756h-0.108l-0.324 0.756q-0.36 0.72 -0.9 1.584l-5.58 9h-9.288zm36.36 6.588q0 -2.304 0.936 -4.212t2.34 -3.114 3.024 -2.34 3.024 -1.962 2.34 -1.944 0.936 -2.304q0 -1.368 -1.062 -2.25t-2.538 -0.882q-1.836 0 -3.492 1.404 -0.504 0.396 -1.296 1.368l-3.78 -3.312q0.936 -1.332 2.268 -2.376 2.88 -2.34 6.768 -2.34 3.96 0 6.408 2.142t2.448 5.706q0 2.376 -1.242 4.266t-3.024 3.096 -3.582 2.25 -3.132 2.268 -1.476 2.628h8.352v-2.88h4.536v7.416h-18.504l-0.144 -0.972 -0.108 -1.656z","ERASER":"M0.054 -14.417q0.198 -1.35 1.098 -2.358l32.256 -36.864q1.368 -1.584 3.456 -1.584h27.648q1.368 0 2.502 0.738t1.71 1.962q0.54 1.224 0.342 2.574t-1.098 2.358l-32.256 36.864q-1.368 1.584 -3.456 1.584h-27.648q-1.368 0 -2.502 -0.738t-1.71 -1.962q-0.54 -1.224 -0.342 -2.574zm4.554 0.666h27.648l12.096 -13.824h-27.648z","PUZZLE_PIECE":"M0 -9.143v-36.864q0.072 0.036 0.63 0.126t1.224 0.18 0.774 0.126q5.4 0.864 8.82 0.864 2.88 0 4.212 -1.26 1.656 -1.584 1.656 -3.204 0 -0.792 -0.54 -1.818t-1.206 -1.908 -1.206 -2.322 -0.54 -2.988q0 -2.952 2.124 -4.59t5.184 -1.638q2.88 0 4.824 1.602t1.944 4.446q0 1.476 -0.63 2.79t-1.368 2.124 -1.368 2.034 -0.63 2.556q0 2.052 1.512 3.006t3.708 0.954q2.304 0 6.48 -0.54t5.868 -0.612v0.072q-0.036 0.072 -0.126 0.63t-0.18 1.224 -0.126 0.774q-0.864 5.4 -0.864 8.82 0 2.88 1.26 4.212 1.584 1.656 3.204 1.656 0.792 0 1.818 -0.54t1.908 -1.206 2.322 -1.206 2.988 -0.54q2.952 0 4.59 2.124t1.638 5.148q0 2.916 -1.602 4.86t-4.446 1.944q-1.476 0 -2.79 -0.63t-2.124 -1.368 -2.034 -1.368 -2.556 -0.63q-3.96 0 -3.96 4.464 0 1.404 0.576 4.14t0.54 4.14v0.18q-0.792 0 -1.188 0.036 -1.224 0.108 -3.51 0.414t-4.158 0.486 -3.528 0.18q-2.196 0 -3.708 -0.954t-1.512 -3.006q0 -1.332 0.63 -2.556t1.368 -2.034 1.368 -2.124 0.63 -2.79q0 -2.844 -1.944 -4.446t-4.86 -1.602q-3.024 0 -5.148 1.638t-2.124 4.59q0 1.548 0.54 2.988t1.206 2.322 1.206 1.908 0.54 1.818q0 1.62 -1.656 3.204 -1.332 1.26 -4.212 1.26 -3.42 0 -8.82 -0.864 -0.324 -0.072 -0.99 -0.144t-0.99 -0.144l-0.468 -0.072 -0.108 -0.036 -0.072 -0.036z","MICROPHONE":"M0 -34.487v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684 0.684 1.62v4.608q0 6.66 4.734 11.394t11.394 4.734 11.394 -4.734 4.734 -11.394v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684 0.684 1.62v4.608q0 7.956 -5.31 13.842t-13.122 6.75v4.752h9.216q0.936 0 1.62 0.684t0.684 1.62 -0.684 1.62 -1.62 0.684h-23.04q-0.936 0 -1.62 -0.684t-0.684 -1.62 0.684 -1.62 1.62 -0.684h9.216v-4.752q-7.812 -0.864 -13.122 -6.75t-5.31 -13.842zm9.216 0v-18.432q0 -4.752 3.384 -8.136t8.136 -3.384 8.136 3.384 3.384 8.136v18.432q0 4.752 -3.384 8.136t-8.136 3.384 -8.136 -3.384 -3.384 -8.136z","MICROPHONE_SLASH":"M0.468 -11.447q0 -0.468 0.36 -0.828l44.424 -44.424q0.36 -0.36 0.828 -0.36t0.828 0.36l2.952 2.952q0.36 0.36 0.36 0.828t-0.36 0.828l-12.996 12.996v4.608q0 4.752 -3.384 8.136t-8.136 3.384q-1.98 0 -3.924 -0.684l-3.456 3.456q3.492 1.836 7.38 1.836 6.66 0 11.394 -4.734t4.734 -11.394v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684 0.684 1.62v4.608q0 7.956 -5.31 13.842t-13.122 6.75v4.752h9.216q0.936 0 1.62 0.684t0.684 1.62 -0.684 1.62 -1.62 0.684h-23.04q-0.936 0 -1.62 -0.684t-0.684 -1.62 0.684 -1.62 1.62 -0.684h9.216v-4.752q-4.5 -0.468 -8.46 -2.916l-9.144 9.144q-0.36 0.36 -0.828 0.36t-0.828 -0.36l-2.952 -2.952q-0.36 -0.36 -0.36 -0.828zm4.14 -23.04v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684 1.62 0.684 0.684 1.62v4.608q0 1.908 0.54 4.068l-3.636 3.636q-1.512 -3.708 -1.512 -7.704zm9.216 0v-18.432q0 -4.752 3.384 -8.136t8.136 -3.384q3.672 0 6.642 2.124t4.194 5.472z","SHIELD":"M0 -29.879v-27.648q0 -0.936 0.684 -1.62t1.62 -0.684h41.472q0.936 0 1.62 0.684t0.684 1.62v27.648q0 3.096 -1.206 6.138t-2.988 5.4 -4.248 4.59 -4.554 3.708 -4.356 2.79 -3.222 1.782 -1.53 0.72q-0.432 0.216 -0.936 0.216t-0.936 -0.216q-0.576 -0.252 -1.53 -0.72t-3.222 -1.782 -4.356 -2.79 -4.554 -3.708 -4.248 -4.59 -2.988 -5.4 -1.206 -6.138zm23.04 17.892q4.284 -2.268 7.668 -4.932 8.46 -6.624 8.46 -12.96v-23.04h-16.128v40.932z","CALENDAR_O":"M0 -4.535v-46.08q0 -1.872 1.368 -3.24t3.24 -1.368h4.608v-3.456q0 -2.376 1.692 -4.068t4.068 -1.692h2.304q2.376 0 4.068 1.692t1.692 4.068v3.456h13.824v-3.456q0 -2.376 1.692 -4.068t4.068 -1.692h2.304q2.376 0 4.068 1.692t1.692 4.068v3.456h4.608q1.872 0 3.24 1.368t1.368 3.24v46.08q0 1.872 -1.368 3.24t-3.24 1.368h-50.688q-1.872 0 -3.24 -1.368t-1.368 -3.24zm4.608 0h50.688v-36.864h-50.688v36.864zm9.216 -43.776q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-10.368q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v10.368zm27.648 0q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-10.368q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v10.368z","FIRE_EXTINGUISHER":"M0.126 -42.983q-0.306 -0.9 0.126 -1.764 0.18 -0.36 0.522 -0.936t1.35 -1.926 2.178 -2.52 3.06 -2.412 3.906 -1.89q-0.9 -1.512 -0.9 -3.096 0 -2.376 1.692 -4.068t4.068 -1.692 4.068 1.692 1.692 4.068q0 1.188 -0.504 2.304h10.872q0 -0.396 0.252 -0.72t0.648 -0.396l16.128 -3.456q0.108 -0.036 0.252 -0.036 0.432 0 0.72 0.252 0.432 0.324 0.432 0.9v11.52q0 0.576 -0.432 0.9 -0.288 0.252 -0.72 0.252 -0.144 0 -0.252 -0.036l-16.128 -3.456q-0.396 -0.072 -0.648 -0.396t-0.252 -0.72h-9.216v3.672q3.996 0.828 6.606 3.996t2.61 7.308v28.8q0 0.936 -0.684 1.62t-1.62 0.684h-18.432q-0.936 0 -1.62 -0.684t-0.684 -1.62v-28.8q0 -3.816 2.25 -6.858t5.814 -4.122v-3.996h-1.152q-2.124 0 -4.14 0.846t-3.294 1.908 -2.376 2.394 -1.458 1.926 -0.504 0.882q-0.612 1.26 -2.052 1.26 -0.576 0 -1.044 -0.252 -0.828 -0.432 -1.134 -1.332zm13.698 -14.544q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62z","ROCKET":"M1.296 -25.847l8.064 -13.824q0.36 -0.504 0.936 -0.576l13.644 -0.72q3.456 -4.104 6.336 -7.02 6.768 -6.732 12.888 -9.288t15.516 -2.556q0.504 0 0.864 0.342t0.36 0.81q0 8.964 -2.718 15.498t-9.126 12.978q-2.916 2.88 -7.02 6.336l-0.72 13.644q-0.072 0.576 -0.576 0.936l-13.824 8.064q-0.252 0.144 -0.576 0.144 -0.432 0 -0.828 -0.324l-2.304 -2.304q-0.468 -0.504 -0.288 -1.152l3.06 -9.936 -10.116 -10.116 -9.936 3.06q-0.108 0.036 -0.324 0.036 -0.504 0 -0.828 -0.324l-2.304 -2.304q-0.612 -0.684 -0.18 -1.404zm43.632 -22.464q0 1.44 1.008 2.448t2.448 1.008 2.448 -1.008 1.008 -2.448 -1.008 -2.448 -2.448 -1.008 -2.448 1.008 -1.008 2.448z","MAXCDN":"M0 -9.143l7.344 -34.308 -5.508 -11.772h45.936q3.636 0 6.822 1.458t5.31 4.086q2.16 2.628 2.916 6.066t0 7.002l-5.904 27.468h-12.024l6.408 -29.952q0.468 -2.016 -0.54 -3.168 -0.972 -1.188 -2.988 -1.188h-6.084l-7.344 34.308h-12.024l7.344 -34.308h-10.296l-7.344 34.308h-12.024z","CHEVRON_CIRCLE_LEFT":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm12.456 0q0 0.936 0.684 1.62l16.344 16.344q0.684 0.684 1.62 0.684t1.62 -0.684l3.672 -3.672q0.684 -0.684 0.684 -1.62t-0.684 -1.62l-11.052 -11.052 11.052 -11.052q0.684 -0.684 0.684 -1.62t-0.684 -1.62l-3.672 -3.672q-0.684 -0.684 -1.62 -0.684t-1.62 0.684l-16.344 16.344q-0.684 0.684 -0.684 1.62z","CHEVRON_CIRCLE_RIGHT":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm18.216 12.672q0 0.936 0.684 1.62l3.672 3.672q0.684 0.684 1.62 0.684t1.62 -0.684l16.344 -16.344q0.684 -0.684 0.684 -1.62t-0.684 -1.62l-16.344 -16.344q-0.684 -0.684 -1.62 -0.684t-1.62 0.684l-3.672 3.672q-0.684 0.684 -0.684 1.62t0.684 1.62l11.052 11.052 -11.052 11.052q-0.684 0.684 -0.684 1.62z","CHEVRON_CIRCLE_UP":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm9 3.456q0 0.936 0.684 1.62l3.672 3.672q0.684 0.684 1.62 0.684t1.62 -0.684l11.052 -11.052 11.052 11.052q0.684 0.684 1.62 0.684t1.62 -0.684l3.672 -3.672q0.684 -0.684 0.684 -1.62t-0.684 -1.62l-16.344 -16.344q-0.684 -0.684 -1.62 -0.684t-1.62 0.684l-16.344 16.344q-0.684 0.684 -0.684 1.62z","CHEVRON_CIRCLE_DOWN":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm9 -3.456q0 0.936 0.684 1.62l16.344 16.344q0.684 0.684 1.62 0.684t1.62 -0.684l16.344 -16.344q0.684 -0.684 0.684 -1.62t-0.684 -1.62l-3.672 -3.672q-0.684 -0.684 -1.62 -0.684t-1.62 0.684l-11.052 11.052 -11.052 -11.052q-0.684 -0.684 -1.62 -0.684t-1.62 0.684l-3.672 3.672q-0.684 0.684 -0.684 1.62z","HTML5":"M0 -59.831h50.688l-4.608 51.768 -20.808 5.832 -20.664 -5.832zm9.432 10.584l1.692 19.224h22.032l-0.792 8.208 -7.092 1.908 -7.056 -1.908 -0.468 -5.04h-6.3l0.792 10.008 13.032 3.6h0.144v-0.036l12.924 -3.564 1.8 -19.584h-23.184l-0.54 -6.516h24.264l0.576 -6.3h-31.824z","CSS3":"M0.432 -11.843l2.556 -12.816h10.692l-1.044 5.292 15.192 5.796 17.496 -5.796 2.448 -12.204h-43.488l2.088 -10.692h43.524l1.368 -6.876h-43.488l2.124 -10.692h54.18l-9.576 47.988 -28.944 9.612z","ANCHOR":"M0 -9.143v-12.672q0 -0.504 0.324 -0.828t0.828 -0.324h12.672q0.792 0 1.08 0.72 0.288 0.684 -0.252 1.26l-3.6 3.6q2.412 3.276 6.822 5.526t9.774 2.97v-23.292h-6.912q-0.936 0 -1.62 -0.684t-0.684 -1.62v-4.608q0 -0.936 0.684 -1.62t1.62 -0.684h6.912v-5.868q-2.088 -1.224 -3.348 -3.33t-1.26 -4.626q0 -3.816 2.7 -6.516t6.516 -2.7 6.516 2.7 2.7 6.516q0 2.52 -1.26 4.626t-3.348 3.33v5.868h6.912q0.936 0 1.62 0.684t0.684 1.62v4.608q0 0.936 -0.684 1.62t-1.62 0.684h-6.912v23.292q5.364 -0.72 9.774 -2.97t6.822 -5.526l-3.6 -3.6q-0.54 -0.576 -0.252 -1.26 0.288 -0.72 1.08 -0.72h12.672q0.504 0 0.828 0.324t0.324 0.828v12.672q0 0.792 -0.72 1.08 -0.288 0.072 -0.432 0.072 -0.468 0 -0.828 -0.324l-3.348 -3.348q-4.284 5.148 -11.466 8.154t-15.462 3.006 -15.462 -3.006 -11.466 -8.154l-3.348 3.348q-0.324 0.324 -0.828 0.324 -0.144 0 -0.432 -0.072 -0.72 -0.288 -0.72 -1.08zm29.952 -46.08q0 0.936 0.684 1.62t1.62 0.684 1.62 -0.684 0.684 -1.62 -0.684 -1.62 -1.62 -0.684 -1.62 0.684 -0.684 1.62z","UNLOCK_ALT":"M0 -12.599v-20.736q0 -1.44 1.008 -2.448t2.448 -1.008h1.152v-11.52q0 -6.66 4.734 -11.394t11.394 -4.734 11.394 4.734 4.734 11.394q0 0.936 -0.684 1.62t-1.62 0.684h-2.304q-0.936 0 -1.62 -0.684t-0.684 -1.62q0 -3.816 -2.7 -6.516t-6.516 -2.7 -6.516 2.7 -2.7 6.516v11.52h26.496q1.44 0 2.448 1.008t1.008 2.448v20.736q0 1.44 -1.008 2.448t-2.448 1.008h-34.56q-1.44 0 -2.448 -1.008t-1.008 -2.448z","BULLSEYE":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm4.608 0q0 4.68 1.836 8.946t4.914 7.344 7.344 4.914 8.946 1.836 8.946 -1.836 7.344 -4.914 4.914 -7.344 1.836 -8.946 -1.836 -8.946 -4.914 -7.344 -7.344 -4.914 -8.946 -1.836 -8.946 1.836 -7.344 4.914 -4.914 7.344 -1.836 8.946zm4.608 0q0 -7.632 5.4 -13.032t13.032 -5.4 13.032 5.4 5.4 13.032 -5.4 13.032 -13.032 5.4 -13.032 -5.4 -5.4 -13.032zm4.608 0q0 5.724 4.05 9.774t9.774 4.05 9.774 -4.05 4.05 -9.774 -4.05 -9.774 -9.774 -4.05 -9.774 4.05 -4.05 9.774zm4.608 0q0 -3.816 2.7 -6.516t6.516 -2.7 6.516 2.7 2.7 6.516 -2.7 6.516 -6.516 2.7 -6.516 -2.7 -2.7 -6.516z","ELLIPSIS_H":"M0 -31.031v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h6.912q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-6.912q-1.44 0 -2.448 -1.008t-1.008 -2.448zm18.432 0v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h6.912q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-6.912q-1.44 0 -2.448 -1.008t-1.008 -2.448zm18.432 0v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h6.912q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-6.912q-1.44 0 -2.448 -1.008t-1.008 -2.448z","ELLIPSIS_V":"M0 -12.599v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h6.912q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-6.912q-1.44 0 -2.448 -1.008t-1.008 -2.448zm0 -18.432v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h6.912q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-6.912q-1.44 0 -2.448 -1.008t-1.008 -2.448zm0 -18.432v-6.912q0 -1.44 1.008 -2.448t2.448 -1.008h6.912q1.44 0 2.448 1.008t1.008 2.448v6.912q0 1.44 -1.008 2.448t-2.448 1.008h-6.912q-1.44 0 -2.448 -1.008t-1.008 -2.448z","RSS_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 -3.456q0 1.908 1.35 3.258t3.258 1.35 3.258 -1.35 1.35 -3.258 -1.35 -3.258 -3.258 -1.35 -3.258 1.35 -1.35 3.258zm0 -11.484q0 0.468 0.306 0.792t0.774 0.36q5.544 0.396 9.504 4.356t4.356 9.504q0.036 0.468 0.36 0.774t0.792 0.306h4.608q0.468 0 0.828 -0.36t0.324 -0.864q-0.468 -8.352 -6.372 -14.256t-14.256 -6.372q-0.504 -0.036 -0.864 0.324t-0.36 0.828v4.608zm0 -13.824q0 0.468 0.324 0.792t0.792 0.36q7.344 0.252 13.608 4.014t10.026 10.026 4.014 13.608q0.036 0.468 0.36 0.792t0.792 0.324h4.608q0.468 0 0.828 -0.36 0.396 -0.324 0.324 -0.828 -0.18 -5.544 -2.016 -10.71t-5.022 -9.36 -7.38 -7.38 -9.36 -5.022 -10.71 -2.016q-0.504 -0.036 -0.828 0.324 -0.36 0.36 -0.36 0.828v4.608z","PLAY_CIRCLE":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm18.432 11.52q0 1.332 1.152 2.016 0.576 0.288 1.152 0.288 0.612 0 1.152 -0.324l19.584 -11.52q1.152 -0.648 1.152 -1.98t-1.152 -1.98l-19.584 -11.52q-1.116 -0.684 -2.304 -0.036 -1.152 0.684 -1.152 2.016v23.04z","TICKET":"M1.944 -25.289q0 -1.926 1.332 -3.258l32.652 -32.616q1.332 -1.332 3.258 -1.332t3.258 1.332l4.5 4.5q-2.016 2.016 -2.016 4.896t2.016 4.896 4.896 2.016 4.896 -2.016l4.536 4.5q1.332 1.332 1.332 3.258t-1.332 3.258l-32.652 32.688q-1.332 1.332 -3.258 1.332t-3.258 -1.332l-4.536 -4.536q2.016 -2.016 2.016 -4.896t-2.016 -4.896 -4.896 -2.016 -4.896 2.016l-4.5 -4.536q-1.332 -1.332 -1.332 -3.258zm10.368 -2.286q0 0.936 0.684 1.62l13.032 13.032q0.648 0.648 1.62 0.648t1.62 -0.648l22.248 -22.248q0.684 -0.684 0.684 -1.62t-0.684 -1.62l-13.032 -13.032q-0.648 -0.648 -1.62 -0.648t-1.62 0.648l-22.248 22.248q-0.684 0.684 -0.684 1.62zm3.96 0l20.592 -20.592 11.376 11.376 -20.592 20.592z","MINUS_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 -14.976q0 0.936 0.684 1.62t1.62 0.684h32.256q0.936 0 1.62 -0.684t0.684 -1.62v-4.608q0 -0.936 -0.684 -1.62t-1.62 -0.684h-32.256q-0.936 0 -1.62 0.684t-0.684 1.62v4.608z","MINUS_SQUARE_O":"M0 -19.511v-29.952q0 -4.284 3.042 -7.326t7.326 -3.042h29.952q4.284 0 7.326 3.042t3.042 7.326v29.952q0 4.284 -3.042 7.326t-7.326 3.042h-29.952q-4.284 0 -7.326 -3.042t-3.042 -7.326zm4.608 0q0 2.376 1.692 4.068t4.068 1.692h29.952q2.376 0 4.068 -1.692t1.692 -4.068v-29.952q0 -2.376 -1.692 -4.068t-4.068 -1.692h-29.952q-2.376 0 -4.068 1.692t-1.692 4.068v29.952zm4.608 -13.824v-2.304q0 -0.504 0.324 -0.828t0.828 -0.324h29.952q0.504 0 0.828 0.324t0.324 0.828v2.304q0 0.504 -0.324 0.828t-0.828 0.324h-29.952q-0.504 0 -0.828 -0.324t-0.324 -0.828z","LEVEL_UP":"M0.108 -9.791q-0.288 -0.72 0.144 -1.26l5.76 -6.912q0.324 -0.396 0.9 -0.396h11.52v-23.04h-6.912q-1.44 0 -2.088 -1.332 -0.612 -1.332 0.324 -2.448l11.52 -13.824q0.648 -0.792 1.764 -0.792t1.764 0.792l11.52 13.824q0.972 1.152 0.324 2.448 -0.648 1.332 -2.088 1.332h-6.912v31.104q0 0.504 -0.324 0.828t-0.828 0.324h-25.344q-0.756 0 -1.044 -0.648z","LEVEL_DOWN":"M0.108 -54.539q0.324 -0.684 1.044 -0.684h25.344q0.468 0 0.81 0.342t0.342 0.846v31.068h6.912q1.44 0 2.088 1.332t-0.324 2.484l-11.52 13.824q-0.648 0.792 -1.764 0.792t-1.764 -0.792l-11.52 -13.824q-0.936 -1.116 -0.324 -2.484 0.648 -1.332 2.088 -1.332h6.912v-23.04h-11.52q-0.504 0 -0.9 -0.396l-5.76 -6.912q-0.468 -0.504 -0.144 -1.224z","CHECK_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm7.848 -17.28q0 0.936 0.684 1.62l12.888 12.888q0.684 0.684 1.62 0.684t1.62 -0.684l22.104 -22.104q0.684 -0.684 0.684 -1.62t-0.684 -1.62l-3.672 -3.672q-0.684 -0.684 -1.62 -0.684t-1.62 0.684l-16.812 16.812 -7.596 -7.596q-0.684 -0.684 -1.62 -0.684t-1.62 0.684l-3.672 3.672q-0.684 0.684 -0.684 1.62z","PENCIL_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 1.152h10.368l19.584 -19.584 -10.368 -10.368 -19.584 19.584v10.368zm3.456 -6.912v-2.016l1.872 -1.872 5.472 5.472 -1.872 1.872h-2.016v-3.456h-3.456zm5.112 -6.264q-0.504 -0.468 0.108 -1.08l10.476 -10.476q0.612 -0.612 1.08 -0.108 0.504 0.468 -0.108 1.08l-10.476 10.476q-0.612 0.612 -1.08 0.108zm13.32 -19.08l10.368 10.368 3.312 -3.312q1.008 -1.008 1.008 -2.448t-1.008 -2.448l-5.472 -5.472q-1.008 -1.008 -2.448 -1.008t-2.448 1.008z","EXTERNAL_LINK_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm10.152 -5.76q0 0.936 0.684 1.62l3.672 3.672q0.684 0.684 1.62 0.684t1.62 -0.684l19.224 -19.224 5.184 5.184q0.648 0.684 1.62 0.684 0.432 0 0.9 -0.18 1.404 -0.612 1.404 -2.124v-17.28q0 -0.936 -0.684 -1.62t-1.62 -0.684h-17.28q-1.512 0 -2.124 1.404 -0.612 1.476 0.504 2.52l5.184 5.184 -19.224 19.224q-0.684 0.684 -0.684 1.62z","SHARE_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 -10.368q0 6.516 6.012 14.544 0.36 0.432 0.9 0.432 0.252 0 0.468 -0.108 0.792 -0.324 0.684 -1.188 -1.584 -12.744 2.232 -17.028 1.656 -1.872 4.68 -2.718t8.064 -0.846v5.76q0 1.512 1.44 2.124 0.432 0.18 0.864 0.18 0.936 0 1.62 -0.684l12.672 -12.672q0.684 -0.684 0.684 -1.62t-0.684 -1.62l-12.672 -12.672q-1.08 -1.116 -2.484 -0.504 -1.44 0.612 -1.44 2.124v5.76q-4.284 0 -7.776 0.702t-5.85 1.836 -4.104 2.844 -2.754 3.438 -1.602 3.924 -0.774 4.014 -0.18 3.978z","COMPASS":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm8.064 0q0 5.328 2.628 9.828t7.128 7.128 9.828 2.628 9.828 -2.628 7.128 -7.128 2.628 -9.828 -2.628 -9.828 -7.128 -7.128 -9.828 -2.628 -9.828 2.628 -7.128 7.128 -2.628 9.828zm10.368 14.364v-19.512l18.432 -9.216v19.512zm4.608 -7.452l9.216 -4.608 -9.216 -4.608v9.216z","TOGGLE_DOWN":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 0q0 0.468 0.342 0.81t0.81 0.342h34.56q0.468 0 0.81 -0.342t0.342 -0.81v-34.56q0 -0.468 -0.342 -0.81t-0.81 -0.342h-34.56q-0.468 0 -0.81 0.342t-0.342 0.81v34.56zm4.86 -25.236q0.612 -1.26 2.052 -1.26h23.04q1.44 0 2.052 1.26 0.648 1.26 -0.18 2.376l-11.52 16.128q-0.684 0.972 -1.872 0.972t-1.872 -0.972l-11.52 -16.128q-0.828 -1.116 -0.18 -2.376z","TOGGLE_UP":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 0q0 0.468 0.342 0.81t0.81 0.342h34.56q0.468 0 0.81 -0.342t0.342 -0.81v-34.56q0 -0.468 -0.342 -0.81t-0.81 -0.342h-34.56q-0.468 0 -0.81 0.342t-0.342 0.81v34.56zm4.86 -9.324q-0.648 -1.26 0.18 -2.376l11.52 -16.128q0.684 -0.972 1.872 -0.972t1.872 0.972l11.52 16.128q0.828 1.116 0.18 2.376 -0.612 1.26 -2.052 1.26h-23.04q-1.44 0 -2.052 -1.26z","TOGGLE_RIGHT":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 0q0 0.504 0.324 0.828t0.828 0.324h34.56q0.504 0 0.828 -0.324t0.324 -0.828v-34.56q0 -0.504 -0.324 -0.828t-0.828 -0.324h-34.56q-0.504 0 -0.828 0.324t-0.324 0.828v34.56zm9.216 -5.76v-23.04q0 -1.44 1.26 -2.052 1.26 -0.648 2.376 0.18l16.128 11.52q0.972 0.684 0.972 1.872t-0.972 1.872l-16.128 11.52q-1.116 0.828 -2.376 0.18 -1.26 -0.612 -1.26 -2.052z","EURO":"M0 -27.647v-4.068q0 -0.468 0.342 -0.81t0.81 -0.342h2.376q-0.072 -2.052 0.036 -3.78h-2.412q-0.504 0 -0.828 -0.324t-0.324 -0.828v-4.104q0 -0.504 0.324 -0.828t0.828 -0.324h3.528q2.412 -7.56 8.766 -12.168t14.418 -4.608q3.672 0 6.984 0.828 0.396 0.108 0.72 0.54 0.216 0.396 0.108 0.864l-1.548 5.724q-0.108 0.468 -0.504 0.702t-0.864 0.09l-0.144 -0.036 -0.414 -0.09 -0.63 -0.126 -0.81 -0.126 -0.936 -0.108 -1.044 -0.09 -1.062 -0.036q-4.536 0 -8.136 2.304t-5.4 6.336h16.848q0.576 0 0.9 0.432 0.36 0.432 0.252 0.936l-0.864 4.104q-0.18 0.936 -1.152 0.936h-17.568q-0.108 1.332 0 3.78h16.524q0.54 0 0.9 0.432 0.324 0.432 0.216 0.972l-0.864 4.032q-0.072 0.396 -0.396 0.666t-0.72 0.27h-13.932q1.728 4.212 5.382 6.678t8.226 2.466q0.648 0 1.296 -0.054t1.206 -0.126 1.062 -0.162 0.882 -0.18 0.666 -0.162l0.432 -0.108 0.18 -0.072q0.468 -0.18 0.936 0.072 0.432 0.252 0.54 0.756l1.26 5.724q0.108 0.432 -0.108 0.81t-0.612 0.522l-0.18 0.036q-0.144 0.072 -0.378 0.126t-0.576 0.162 -0.774 0.198 -0.918 0.18 -1.08 0.18 -1.206 0.162 -1.314 0.108 -1.386 0.036q-8.424 0 -14.724 -4.698t-8.568 -12.654h-3.42q-0.468 0 -0.81 -0.342t-0.342 -0.81z","GBP":"M0 -10.295v-5.4q0 -0.468 0.342 -0.81t0.81 -0.342h3.492v-13.788h-3.42q-0.504 0 -0.828 -0.342t-0.324 -0.81v-4.716q0 -0.504 0.324 -0.828t0.828 -0.324h3.42v-8.028q0 -6.156 4.446 -10.152t11.322 -3.996q6.66 0 12.06 4.5 0.324 0.288 0.36 0.738t-0.252 0.81l-3.708 4.572q-0.324 0.396 -0.792 0.432 -0.468 0.072 -0.828 -0.252 -0.18 -0.18 -0.936 -0.684t-2.484 -1.152 -3.348 -0.648q-3.06 0 -4.932 1.692t-1.872 4.428v7.74h10.98q0.468 0 0.81 0.324t0.342 0.828v4.716q0 0.468 -0.342 0.81t-0.81 0.342h-10.98v13.644h14.904v-6.516q0 -0.468 0.324 -0.81t0.828 -0.342h5.832q0.504 0 0.828 0.342t0.324 0.81v13.212q0 0.504 -0.324 0.828t-0.828 0.324h-34.416q-0.504 0 -0.828 -0.324t-0.324 -0.828z","USD":"M1.872 -15.299l3.708 -4.86q0.252 -0.36 0.828 -0.432 0.54 -0.072 0.864 0.324l0.072 0.072q4.068 3.564 8.748 4.5 1.332 0.288 2.664 0.288 2.916 0 5.13 -1.548t2.214 -4.392q0 -1.008 -0.54 -1.908t-1.206 -1.512 -2.106 -1.35 -2.376 -1.152 -2.88 -1.17q-1.404 -0.576 -2.214 -0.9t-2.214 -0.954 -2.25 -1.116 -2.034 -1.278 -1.926 -1.53 -1.566 -1.764 -1.278 -2.088 -0.756 -2.394 -0.306 -2.808q0 -4.968 3.528 -8.712t9.18 -4.824v-6.48q0 -0.468 0.342 -0.81t0.81 -0.342h4.86q0.504 0 0.828 0.324t0.324 0.828v6.336q2.052 0.216 3.978 0.828t3.132 1.206 2.286 1.35 1.404 1.044 0.54 0.504q0.612 0.648 0.18 1.368l-2.916 5.256q-0.288 0.54 -0.828 0.576 -0.504 0.108 -0.972 -0.252 -0.108 -0.108 -0.522 -0.432t-1.404 -0.954 -2.106 -1.152 -2.682 -0.936 -3.078 -0.414q-3.42 0 -5.58 1.548t-2.16 3.996q0 0.936 0.306 1.728t1.062 1.494 1.422 1.188 2.016 1.116 2.178 0.972 2.52 0.99q1.908 0.72 2.916 1.134t2.736 1.26 2.718 1.53 2.232 1.8 1.908 2.286 1.134 2.754 0.468 3.384q0 5.508 -3.582 9.486t-9.306 4.914v6.3q0 0.504 -0.324 0.828t-0.828 0.324h-4.86q-0.468 0 -0.81 -0.342t-0.342 -0.81v-6.3q-2.376 -0.324 -4.59 -1.116t-3.654 -1.602 -2.664 -1.728 -1.674 -1.35 -0.63 -0.648q-0.612 -0.756 -0.072 -1.476z","RUPEE":"M0 -30.923v-4.572q0 -0.468 0.342 -0.81t0.81 -0.342h4.032q4.752 0 7.65 -1.548t3.69 -4.5h-15.372q-0.504 0 -0.828 -0.324t-0.324 -0.828v-3.672q0 -0.504 0.324 -0.828t0.828 -0.324h14.868q-2.052 -4.068 -9.648 -4.068h-5.22q-0.468 0 -0.81 -0.342t-0.342 -0.81v-4.788q0 -0.504 0.324 -0.828t0.828 -0.324h29.952q0.504 0 0.828 0.324t0.324 0.828v3.672q0 0.504 -0.324 0.828t-0.828 0.324h-8.388q1.692 2.196 2.304 5.184h6.156q0.504 0 0.828 0.324t0.324 0.828v3.672q0 0.504 -0.324 0.828t-0.828 0.324h-6.048q-0.828 5.184 -4.644 8.424t-9.936 3.96q6.012 6.408 16.524 19.296 0.504 0.576 0.144 1.224 -0.288 0.648 -1.044 0.648h-7.02q-0.576 0 -0.9 -0.432 -11.016 -13.212 -17.928 -20.556 -0.324 -0.324 -0.324 -0.792z","YEN":"M0.144 -58.103q-0.288 -0.576 0 -1.152 0.36 -0.576 1.008 -0.576h6.984q0.684 0 1.044 0.648l7.74 15.3q0.684 1.368 2.016 4.5 0.36 -0.864 1.098 -2.448t0.99 -2.196l6.876 -15.12q0.288 -0.684 1.044 -0.684h6.876q0.612 0 0.972 0.576 0.324 0.504 0.036 1.116l-11.268 20.844h7.74q0.468 0 0.81 0.342t0.342 0.81v3.744q0 0.504 -0.342 0.828t-0.81 0.324h-10.44v3.06h10.44q0.468 0 0.81 0.342t0.342 0.81v3.708q0 0.504 -0.342 0.828t-0.81 0.324h-10.44v11.88q0 0.468 -0.342 0.81t-0.81 0.342h-6.192q-0.468 0 -0.81 -0.324t-0.342 -0.828v-11.88h-10.368q-0.468 0 -0.81 -0.324t-0.342 -0.828v-3.708q0 -0.468 0.342 -0.81t0.81 -0.342h10.368v-3.06h-10.368q-0.468 0 -0.81 -0.324t-0.342 -0.828v-3.744q0 -0.468 0.342 -0.81t0.81 -0.342h7.704z","RUBLE":"M0 -18.359v-4.608q0 -0.504 0.324 -0.828t0.828 -0.324h8.064v-4.248h-8.064q-0.504 0 -0.828 -0.324t-0.324 -0.828v-5.364q0 -0.468 0.324 -0.81t0.828 -0.342h8.064v-22.644q0 -0.504 0.324 -0.828t0.828 -0.324h19.404q7.2 0 11.754 4.392t4.554 11.34 -4.554 11.34 -11.754 4.392h-12.24v4.248h18.18q0.504 0 0.828 0.324t0.324 0.828v4.608q0 0.504 -0.324 0.828t-0.828 0.324h-18.18v6.912q0 0.504 -0.342 0.828t-0.81 0.324h-6.012q-0.504 0 -0.828 -0.324t-0.324 -0.828v-6.912h-8.064q-0.504 0 -0.828 -0.324t-0.324 -0.828zm17.532 -17.676h11.52q3.816 0 6.156 -2.232t2.34 -5.832 -2.34 -5.832 -6.156 -2.232h-11.52v16.128z","WON":"M0 -33.335v-2.304q0 -0.504 0.324 -0.828t0.828 -0.324h6.3l-1.188 -4.608h-5.112q-0.504 0 -0.828 -0.324t-0.324 -0.828v-2.304q0 -0.504 0.324 -0.828t0.828 -0.324h3.924l-3.204 -12.384q-0.18 -0.54 0.18 -1.008 0.36 -0.432 0.936 -0.432h4.932q0.936 0 1.116 0.864l3.24 12.96h12.924l3.492 -12.96q0.252 -0.864 1.116 -0.864h4.536q0.864 0 1.116 0.864l3.528 12.96h13.14l3.348 -12.96q0.18 -0.864 1.116 -0.864h4.932q0.576 0 0.936 0.432 0.36 0.468 0.18 1.008l-3.276 12.384h3.996q0.504 0 0.828 0.324t0.324 0.828v2.304q0 0.504 -0.324 0.828t-0.828 0.324h-5.22l-1.224 4.608h6.444q0.504 0 0.828 0.324t0.324 0.828v2.304q0 0.504 -0.324 0.828t-0.828 0.324h-7.668l-5.904 22.176q-0.252 0.864 -1.116 0.864h-5.724q-0.864 0 -1.116 -0.864l-5.976 -22.176h-7.524l-6.012 22.176q-0.252 0.864 -1.116 0.864h-5.724q-0.396 0 -0.702 -0.252t-0.378 -0.612l-5.76 -22.176h-7.488q-0.504 0 -0.828 -0.324t-0.324 -0.828zm13.428 -8.064l1.152 4.608h8.1l1.26 -4.608h-10.512zm2.268 9.216l2.7 10.8 0.036 0.108 0.036 0.108 0.018 -0.126 0.018 -0.126 2.916 -10.764h-5.724zm13.896 -4.608h5.004l-1.26 -4.608h-2.52zm10.656 -4.608l1.224 4.608h8.28l1.188 -4.608h-10.692zm2.484 9.216l2.916 10.764 0.018 0.126 0.054 0.126 0.018 -0.108 0.018 -0.108 2.808 -10.8h-5.832z","BTC":"M2.016 -9.143l1.116 -6.588h3.996q1.8 0 2.088 -1.836v-14.472h0.576q-0.216 -0.036 -0.576 -0.036v-10.332q-0.468 -2.448 -3.204 -2.448h-3.996v-5.904l7.632 0.036q2.304 0 3.492 -0.036v-9.072h5.544v8.892q2.952 -0.072 4.392 -0.072v-8.82h5.544v9.072q2.844 0.252 5.04 0.81t4.068 1.62 2.97 2.808 1.314 4.122q0.648 6.552 -4.716 9.288 4.212 1.008 6.3 3.708t1.62 7.704q-0.252 2.556 -1.17 4.5t-2.322 3.204 -3.492 2.106 -4.374 1.242 -5.238 0.54v9.18h-5.544v-9.036q-2.88 0 -4.392 -0.036v9.072h-5.544v-9.18q-0.648 0 -1.944 -0.018t-1.98 -0.018h-7.2zm16.776 -6.552q0.288 0 1.332 0.018t1.728 0.018 1.908 -0.054 2.106 -0.144 2.052 -0.306 1.998 -0.504 1.71 -0.756 1.422 -1.08 0.882 -1.44 0.342 -1.836q0 -1.296 -0.54 -2.304t-1.332 -1.656 -2.07 -1.098 -2.358 -0.666 -2.664 -0.324 -2.484 -0.108 -2.322 0.036 -1.71 0.036v12.168zm0 -17.712q0.18 0 1.242 0.018t1.674 0 1.8 -0.072 1.98 -0.198 1.854 -0.396 1.746 -0.666 1.332 -0.972 0.972 -1.386 0.324 -1.836q0 -1.188 -0.45 -2.106t-1.098 -1.512 -1.728 -1.008 -1.98 -0.594 -2.214 -0.288 -2.088 -0.09 -1.944 0.036 -1.422 0.018v11.052z","FILE":"M0 -3.383v-57.6q0 -1.44 1.008 -2.448t2.448 -1.008h28.8v19.584q0 1.44 1.008 2.448t2.448 1.008h19.584v38.016q0 1.44 -1.008 2.448t-2.448 1.008h-48.384q-1.44 0 -2.448 -1.008t-1.008 -2.448zm36.864 -42.624v-16.992q0.792 0.504 1.296 1.008l14.688 14.688q0.504 0.504 1.008 1.296h-16.992z","FILE_TEXT":"M0 -3.383v-57.6q0 -1.44 1.008 -2.448t2.448 -1.008h28.8v19.584q0 1.44 1.008 2.448t2.448 1.008h19.584v38.016q0 1.44 -1.008 2.448t-2.448 1.008h-48.384q-1.44 0 -2.448 -1.008t-1.008 -2.448zm13.824 -11.52q0 0.504 0.324 0.828t0.828 0.324h25.344q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-25.344q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h25.344q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-25.344q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h25.344q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-25.344q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm23.04 -12.672v-16.992q0.792 0.504 1.296 1.008l14.688 14.688q0.504 0.504 1.008 1.296h-16.992z","SORT_ALPHA_ASC":"M1.224 -13.031q0.288 -0.72 1.08 -0.72h6.912v-49.536q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v49.536h6.912q0.504 0 0.828 0.324t0.324 0.828q0 0.432 -0.36 0.864l-11.484 11.484q-0.36 0.324 -0.828 0.324 -0.432 0 -0.828 -0.324l-11.52 -11.52q-0.54 -0.576 -0.252 -1.26zm31.14 -23.76v-3.816h2.52l8.28 -23.832h5.832l8.28 23.832h2.52v3.816h-10.368v-3.816h2.7l-1.692 -5.184h-8.748l-1.692 5.184h2.7v3.816h-10.332zm3.204 33.624l13.284 -19.044q0.432 -0.648 0.756 -0.972l0.396 -0.324v-0.108l-0.234 0.018 -0.27 0.018q-0.432 0.108 -1.08 0.108h-8.352v4.14h-4.32v-8.244h20.412v3.204l-13.284 19.08q-0.216 0.288 -0.756 0.936l-0.396 0.396v0.072l0.504 -0.072q0.324 -0.072 1.08 -0.072h8.928v-4.284h4.356v8.388h-21.024v-3.24zm7.308 -46.584h6.372l-2.592 -7.848 -0.432 -1.692q-0.072 -0.576 -0.072 -0.72h-0.144l-0.108 0.72q0 0.036 -0.126 0.648t-0.27 1.044z","SORT_ALPHA_DESC":"M1.224 -13.031q0.288 -0.72 1.08 -0.72h6.912v-49.536q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v49.536h6.912q0.504 0 0.828 0.324t0.324 0.828q0 0.432 -0.36 0.864l-11.484 11.484q-0.36 0.324 -0.828 0.324 -0.432 0 -0.828 -0.324l-11.52 -11.52q-0.54 -0.576 -0.252 -1.26zm31.14 9.288h2.52l8.28 -23.832h5.832l8.28 23.832h2.52v3.816h-10.368v-3.816h2.7l-1.692 -5.184h-8.748l-1.692 5.184h2.7v3.816h-10.332v-3.816zm3.204 -33.048v-3.24l13.284 -19.044q0.432 -0.648 0.756 -0.972l0.396 -0.324v-0.108l-0.234 0.018 -0.27 0.018q-0.432 0.108 -1.08 0.108h-8.352v4.14h-4.32v-8.244h20.412v3.204l-13.284 19.08q-0.216 0.288 -0.756 0.936l-0.396 0.36v0.108l0.504 -0.108q0.324 -0.036 1.08 -0.036h8.928v-4.284h4.356v8.388h-21.024zm7.308 23.904h6.372l-2.592 -7.848 -0.432 -1.692q-0.072 -0.576 -0.072 -0.72h-0.144l-0.108 0.72q0 0.036 -0.126 0.648t-0.27 1.044z","SORT_AMOUNT_ASC":"M1.224 -13.031q0.288 -0.72 1.08 -0.72h6.912v-49.536q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v49.536h6.912q0.504 0 0.828 0.324t0.324 0.828q0 0.432 -0.36 0.864l-11.484 11.484q-0.36 0.324 -0.828 0.324 -0.432 0 -0.828 -0.324l-11.52 -11.52q-0.54 -0.576 -0.252 -1.26zm31.032 5.04q0 -0.504 0.324 -0.828t0.828 -0.324h29.952q0.504 0 0.828 0.324t0.324 0.828v6.912q0 0.504 -0.324 0.828t-0.828 0.324h-29.952q-0.504 0 -0.828 -0.324t-0.324 -0.828v-6.912zm0 -11.52v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324h23.04q0.504 0 0.828 0.324t0.324 0.828v6.912q0 0.504 -0.324 0.828t-0.828 0.324h-23.04q-0.504 0 -0.828 -0.324t-0.324 -0.828zm0 -18.432v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324h16.128q0.504 0 0.828 0.324t0.324 0.828v6.912q0 0.504 -0.324 0.828t-0.828 0.324h-16.128q-0.504 0 -0.828 -0.324t-0.324 -0.828zm0 -18.432v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324h9.216q0.504 0 0.828 0.324t0.324 0.828v6.912q0 0.504 -0.324 0.828t-0.828 0.324h-9.216q-0.504 0 -0.828 -0.324t-0.324 -0.828z","SORT_AMOUNT_DESC":"M1.224 -13.031q0.288 -0.72 1.08 -0.72h6.912v-49.536q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v49.536h6.912q0.504 0 0.828 0.324t0.324 0.828q0 0.432 -0.36 0.864l-11.484 11.484q-0.36 0.324 -0.828 0.324 -0.432 0 -0.828 -0.324l-11.52 -11.52q-0.54 -0.576 -0.252 -1.26zm31.032 5.04q0 -0.504 0.324 -0.828t0.828 -0.324h9.216q0.504 0 0.828 0.324t0.324 0.828v6.912q0 0.504 -0.324 0.828t-0.828 0.324h-9.216q-0.504 0 -0.828 -0.324t-0.324 -0.828v-6.912zm0 -11.52v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324h16.128q0.504 0 0.828 0.324t0.324 0.828v6.912q0 0.504 -0.324 0.828t-0.828 0.324h-16.128q-0.504 0 -0.828 -0.324t-0.324 -0.828zm0 -18.432v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324h23.04q0.504 0 0.828 0.324t0.324 0.828v6.912q0 0.504 -0.324 0.828t-0.828 0.324h-23.04q-0.504 0 -0.828 -0.324t-0.324 -0.828zm0 -18.432v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324h29.952q0.504 0 0.828 0.324t0.324 0.828v6.912q0 0.504 -0.324 0.828t-0.828 0.324h-29.952q-0.504 0 -0.828 -0.324t-0.324 -0.828z","SORT_NUMERIC_ASC":"M1.224 -13.031q0.288 -0.72 1.08 -0.72h6.912v-49.536q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v49.536h6.912q0.504 0 0.828 0.324t0.324 0.828q0 0.432 -0.36 0.864l-11.484 11.484q-0.36 0.324 -0.828 0.324 -0.432 0 -0.828 -0.324l-11.52 -11.52q-0.54 -0.576 -0.252 -1.26zm32.832 -5.508q0 -3.78 2.592 -6.408t6.516 -2.628q4.428 0 7.38 3.402t2.952 9.09q0 2.232 -0.468 4.374t-1.476 4.104 -2.448 3.438 -3.546 2.358 -4.59 0.882q-2.232 0 -3.888 -0.576 -0.864 -0.288 -1.512 -0.54l1.404 -4.068q0.54 0.252 1.116 0.396 1.332 0.468 2.7 0.468 3.024 0 4.842 -2.106t2.394 -5.238h-0.072q-0.756 0.828 -2.214 1.332t-3.042 0.504q-3.816 0 -6.228 -2.574t-2.412 -6.21zm1.08 -39.24l6.912 -6.66h4.428v23.544h5.94v4.104h-16.884v-4.104h6.012v-15.552l0.018 -0.684 0.018 -0.612v-0.576h-0.072l-0.252 0.432q-0.288 0.468 -0.936 1.116l-2.232 2.088zm3.924 39.24q0 2.052 1.314 3.42t3.762 1.368q1.8 0 3.06 -0.972t1.26 -2.448q0 -2.268 -1.584 -4.176t-3.708 -1.908q-1.872 0 -2.988 1.332t-1.116 3.384z","SORT_NUMERIC_DESC":"M1.224 -13.031q0.288 -0.72 1.08 -0.72h6.912v-49.536q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v49.536h6.912q0.504 0 0.828 0.324t0.324 0.828q0 0.432 -0.36 0.864l-11.484 11.484q-0.36 0.324 -0.828 0.324 -0.432 0 -0.828 -0.324l-11.52 -11.52q-0.54 -0.576 -0.252 -1.26zm32.832 -42.372q0 -3.78 2.592 -6.408t6.516 -2.628q4.428 0 7.38 3.402t2.952 9.09q0 2.232 -0.468 4.374t-1.476 4.104 -2.448 3.438 -3.546 2.358 -4.59 0.882q-2.232 0 -3.888 -0.576 -0.864 -0.288 -1.512 -0.54l1.404 -4.068q0.54 0.252 1.116 0.396 1.332 0.468 2.7 0.468 3.024 0 4.842 -2.106t2.394 -5.238h-0.072q-0.756 0.828 -2.214 1.332t-3.042 0.504q-3.816 0 -6.228 -2.574t-2.412 -6.21zm1.08 34.488l6.912 -6.66h4.428v23.544h5.94v4.104h-16.884v-4.104h6.012v-15.552l0.018 -0.684 0.018 -0.612v-0.576h-0.072l-0.252 0.432q-0.288 0.468 -0.936 1.116l-2.232 2.088zm3.924 -34.488q0 2.052 1.314 3.42t3.762 1.368q1.8 0 3.06 -0.972t1.26 -2.448q0 -2.268 -1.584 -4.176t-3.708 -1.908q-1.872 0 -2.988 1.332t-1.116 3.384z","THUMBS_UP":"M0 -11.447v-23.04q0 -0.936 0.684 -1.62t1.62 -0.684h10.368q0.936 0 1.62 0.684t0.684 1.62v23.04q0 0.936 -0.684 1.62t-1.62 0.684h-10.368q-0.936 0 -1.62 -0.684t-0.684 -1.62zm4.608 -4.608q0 0.972 0.666 1.638t1.638 0.666q0.936 0 1.62 -0.666t0.684 -1.638q0 -0.936 -0.684 -1.62t-1.62 -0.684q-0.972 0 -1.638 0.684t-0.666 1.62zm12.672 4.608v-23.076q0 -0.9 0.648 -1.566t1.548 -0.738q0.864 -0.072 2.736 -2.124t3.636 -4.356q2.448 -3.132 3.636 -4.32 0.648 -0.648 1.116 -1.728t0.63 -1.746 0.486 -2.178q0.252 -1.404 0.45 -2.196t0.702 -1.872 1.224 -1.8q0.684 -0.684 1.62 -0.684 1.656 0 2.97 0.378t2.16 0.936 1.44 1.458 0.864 1.62 0.432 1.8 0.18 1.62 0.018 1.404q0 1.368 -0.342 2.736t-0.684 2.16 -0.99 2.016q-0.108 0.216 -0.36 0.648t-0.396 0.792 -0.288 0.864h9.972q2.808 0 4.86 2.052t2.052 4.86q0 3.096 -1.98 5.364 0.54 1.584 0.54 2.736 0.108 2.736 -1.548 4.932 0.612 2.016 0 4.212 -0.54 2.052 -1.944 3.384 0.324 4.032 -1.764 6.516 -2.304 2.736 -7.092 2.808h-4.644q-2.376 0 -5.184 -0.558t-4.374 -1.044 -4.338 -1.422q-4.428 -1.548 -5.688 -1.584 -0.936 -0.036 -1.62 -0.702t-0.684 -1.602z","THUMBS_DOWN":"M0 -25.271q0 0.936 0.684 1.62t1.62 0.684h10.368q0.936 0 1.62 -0.684t0.684 -1.62v-23.04q0 -0.936 -0.684 -1.62t-1.62 -0.684h-10.368q-0.936 0 -1.62 0.684t-0.684 1.62v23.04zm4.608 -18.432q0 -0.972 0.666 -1.638t1.638 -0.666q0.936 0 1.62 0.666t0.684 1.638q0 0.936 -0.684 1.62t-1.62 0.684q-0.972 0 -1.638 -0.684t-0.666 -1.62zm12.672 18.468v-23.076q0 -0.936 0.684 -1.602t1.62 -0.702q1.26 -0.036 5.688 -1.584 2.772 -0.936 4.338 -1.422t4.374 -1.044 5.184 -0.558h4.644q4.788 0.072 7.092 2.808 2.088 2.484 1.764 6.516 1.404 1.332 1.944 3.384 0.612 2.196 0 4.212 1.656 2.196 1.548 4.932 0 1.152 -0.54 2.736 1.98 2.196 1.98 5.364 -0.036 2.808 -2.07 4.86t-4.842 2.052h-9.972q0.144 0.504 0.288 0.864t0.396 0.792 0.36 0.648q0.648 1.332 0.972 2.052t0.684 2.106 0.36 2.754q0 0.864 -0.018 1.404t-0.18 1.62 -0.432 1.8 -0.864 1.62 -1.44 1.458 -2.16 0.936 -2.97 0.378q-0.936 0 -1.62 -0.684 -0.72 -0.72 -1.224 -1.8t-0.702 -1.872 -0.45 -2.196q-0.324 -1.512 -0.486 -2.178t-0.63 -1.746 -1.116 -1.728q-1.188 -1.188 -3.636 -4.32 -1.764 -2.304 -3.636 -4.356t-2.736 -2.124q-0.9 -0.072 -1.548 -0.738t-0.648 -1.566z","YOUTUBE_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm7.848 -7.416q0 6.336 0.72 9.36 0.36 1.548 1.53 2.628t2.718 1.26q4.932 0.54 14.832 0.54t14.832 -0.54q1.548 -0.18 2.718 -1.26t1.53 -2.628q0.72 -3.024 0.72 -9.36 0 -6.372 -0.684 -9.36 -0.36 -1.584 -1.548 -2.646t-2.736 -1.242q-4.896 -0.54 -14.832 -0.54 -9.9 0 -14.796 0.54 -1.584 0.18 -2.754 1.242t-1.53 2.646q-0.72 3.132 -0.72 9.36zm2.952 -6.66v-2.52h8.352v2.52h-2.88v15.228h-2.664v-15.228h-2.808zm3.456 -27.432l0.864 2.484 0.828 2.484q1.26 3.708 1.656 5.688v7.236h2.664v-7.236l3.24 -10.656h-2.7l-1.836 7.02 -1.908 -7.02h-2.808zm5.256 39.888v-10.44h2.376v9.72q0 0.864 0.036 0.936 0.036 0.54 0.54 0.54 0.72 0 1.512 -1.116v-10.08h2.412v13.212h-2.412v-1.44q-1.404 1.62 -2.736 1.62 -1.188 0 -1.512 -1.008 -0.216 -0.576 -0.216 -1.944zm4.032 -26.316q0 2.088 0.756 3.132 0.972 1.368 2.808 1.368 1.764 0 2.808 -1.368 0.756 -0.972 0.756 -3.132v-4.68q0 -2.088 -0.756 -3.132 -1.044 -1.368 -2.808 -1.368 -1.836 0 -2.808 1.368 -0.756 1.044 -0.756 3.132v4.68zm2.412 0.468v-5.616q0 -1.872 1.152 -1.872t1.152 1.872v5.616q0 1.836 -1.152 1.836t-1.152 -1.836zm2.484 28.62v-17.748h2.412v5.796q1.152 -1.44 2.448 -1.44 1.476 0 1.908 1.512 0.252 0.756 0.252 2.664v5.256q0 1.872 -0.252 2.628 -0.432 1.512 -1.908 1.512 -1.26 0 -2.448 -1.476v1.296h-2.412zm2.412 -2.592q0.576 0.576 1.188 0.576 1.044 0 1.044 -1.764v-5.652q0 -1.8 -1.044 -1.8 -0.612 0 -1.188 0.576v8.064zm1.8 -24.948q0 1.332 0.216 1.98 0.396 0.972 1.548 0.972 1.296 0 2.772 -1.62v1.44h2.412v-13.32h-2.412v10.188q-0.792 1.116 -1.512 1.116 -0.54 0 -0.576 -0.576 -0.036 -0.072 -0.036 -0.936v-9.792h-2.412v10.548zm4.68 23.256v-4.644q0 -2.124 0.72 -3.096 1.044 -1.368 2.88 -1.368t2.808 1.368q0.756 1.008 0.756 3.096v2.736h-4.788v2.34q0 1.836 1.224 1.836 0.864 0 1.08 -0.936 0 -0.036 0.018 -0.252t0.018 -0.594v-0.774h2.448v0.324q0 1.044 -0.072 1.548 -0.108 0.792 -0.54 1.44 -0.972 1.44 -2.88 1.44 -1.872 0 -2.916 -1.368 -0.756 -0.972 -0.756 -3.096zm2.376 -3.888h2.376v-1.224q0 -1.836 -1.188 -1.836t-1.188 1.836v1.224z","YOUTUBE":"M0.972 -18.503q0 -8.424 0.936 -12.6 0.504 -2.124 2.088 -3.564t3.708 -1.692q6.588 -0.72 19.944 -0.72t19.98 0.72q2.088 0.252 3.69 1.692t2.07 3.564q0.936 4.032 0.936 12.6 0 8.424 -0.936 12.6 -0.504 2.124 -2.088 3.564t-3.672 1.656q-6.624 0.756 -19.98 0.756t-19.98 -0.756q-2.088 -0.216 -3.69 -1.656t-2.07 -3.564q-0.936 -4.032 -0.936 -12.6zm3.996 -8.964h3.78v20.484h3.6v-20.484h3.852v-3.384h-11.232v3.384zm4.608 -36.972h3.816l2.556 9.468 2.448 -9.468h3.672l-4.356 14.364v9.756h-3.6v-9.756q-0.504 -2.664 -2.196 -7.632 -1.332 -3.708 -2.34 -6.732zm7.092 53.748q0 1.764 0.288 2.628 0.432 1.332 2.088 1.332 1.728 0 3.672 -2.196v1.944h3.204v-17.784h-3.204v13.608q-1.08 1.512 -2.052 1.512 -0.648 0 -0.756 -0.756 -0.036 -0.108 -0.036 -1.26v-13.104h-3.204v14.076zm5.436 -35.46v-6.3q0 -2.88 1.008 -4.212 1.368 -1.836 3.78 -1.836 2.484 0 3.816 1.836 1.008 1.332 1.008 4.212v6.3q0 2.916 -1.008 4.248 -1.332 1.836 -3.816 1.836 -2.412 0 -3.78 -1.836 -1.008 -1.368 -1.008 -4.248zm3.24 0.612q0 2.52 1.548 2.52t1.548 -2.52v-7.56q0 -2.484 -1.548 -2.484t-1.548 2.484v7.56zm3.384 38.556h3.204v-1.728q1.62 1.98 3.348 1.98 1.944 0 2.556 -1.98 0.324 -0.972 0.324 -3.6v-7.092q0 -2.628 -0.324 -3.564 -0.612 -2.016 -2.556 -2.016 -1.8 0 -3.348 1.944v-7.812h-3.204v23.868zm3.204 -3.456v-10.836q0.792 -0.792 1.62 -0.792 1.404 0 1.404 2.412v7.596q0 2.412 -1.404 2.412 -0.828 0 -1.62 -0.792zm2.448 -33.66v-14.184h3.276v13.212q0 1.188 0.036 1.26 0.108 0.792 0.756 0.792 0.972 0 2.052 -1.548v-13.716h3.276v17.964h-3.276v-1.98q-1.908 2.232 -3.708 2.232 -1.656 0 -2.124 -1.332 -0.288 -0.864 -0.288 -2.7zm6.3 31.356q0 2.844 1.044 4.176 1.404 1.836 3.888 1.836 2.592 0 3.888 -1.908 0.648 -0.972 0.756 -1.944 0.072 -0.324 0.072 -2.088v-0.468h-3.276q0 1.836 -0.072 2.196 -0.252 1.296 -1.44 1.296 -1.656 0 -1.656 -2.484v-3.132h6.444v-3.708q0 -2.844 -0.972 -4.176 -1.404 -1.836 -3.816 -1.836 -2.448 0 -3.852 1.836 -1.008 1.332 -1.008 4.176v6.228zm3.204 -5.22v-1.656q0 -2.448 1.62 -2.448t1.62 2.448v1.656h-3.24z","XING":"M0.18 -22.967q-0.36 -0.612 0 -1.296l9.108 -16.128v-0.036l-5.796 -10.044q-0.432 -0.792 -0.036 -1.332 0.324 -0.54 1.152 -0.54h8.604q1.44 0 2.376 1.62l5.904 10.296q-0.36 0.648 -9.252 16.416 -0.972 1.656 -2.34 1.656h-8.604q-0.756 0 -1.116 -0.612zm19.116 -5.58q0.648 -1.152 19.116 -33.912 0.9 -1.62 2.304 -1.62h8.676q0.792 0 1.116 0.54 0.396 0.576 0 1.332l-19.008 33.624v0.036l12.096 22.14q0.396 0.72 0.036 1.332 -0.36 0.54 -1.152 0.54h-8.604q-1.512 0 -2.376 -1.62z","XING_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm8.172 -8.496q0.288 0.468 0.864 0.468h6.66q1.116 0 1.8 -1.296l7.164 -12.672q0 -0.036 -4.536 -7.992 -0.756 -1.224 -1.872 -1.224h-6.624q-0.648 0 -0.936 0.396 -0.252 0.432 0.036 1.044l4.5 7.776v0.036l-7.056 12.456q-0.324 0.504 0 1.008zm14.796 -4.32q0.036 0.072 9.432 17.316 0.72 1.26 1.872 1.26h6.624q0.648 0 0.9 -0.432 0.288 -0.468 -0.036 -1.008l-9.36 -17.136v-0.036l14.724 -26.028q0.288 -0.576 0 -1.008 -0.252 -0.432 -0.864 -0.432h-6.732q-1.08 0 -1.764 1.26z","YOUTUBE_PLAY":"M0 -32.183q0 -3.456 0.036 -5.4t0.306 -4.914 0.81 -5.31q0.576 -2.628 2.484 -4.428t4.464 -2.088q7.992 -0.9 24.156 -0.9t24.156 0.9q2.556 0.288 4.482 2.088t2.502 4.428q0.504 2.34 0.774 5.31t0.306 4.914 0.036 5.4 -0.036 5.4 -0.306 4.914 -0.81 5.31q-0.576 2.628 -2.484 4.428t-4.464 2.088q-7.992 0.9 -24.156 0.9t-24.156 -0.9q-2.556 -0.288 -4.482 -2.088t-2.502 -4.428q-0.504 -2.34 -0.774 -5.31t-0.306 -4.914 -0.036 -5.4zm23.04 11.52q0 1.368 1.188 2.016 0.576 0.288 1.116 0.288 0.72 0 1.224 -0.36l18.432 -11.52q1.08 -0.612 1.08 -1.944t-1.08 -1.944l-18.432 -11.52q-1.116 -0.72 -2.34 -0.072 -1.188 0.648 -1.188 2.016v23.04z","DROPBOX":"M2.304 -29.231l12.168 -9.756 17.784 10.98 -12.312 10.26zm0 -19.476l17.64 -11.484 12.312 10.26 -17.784 10.944zm12.348 33.588v-3.888l5.292 3.456 12.312 -10.224v-0.072l0.036 0.036 0.036 -0.036v0.072l12.348 10.224 5.292 -3.456v3.888l-17.64 10.548v0.036l-0.036 -0.036 -0.036 0.036v-0.036zm17.604 -12.888l17.784 -10.98 12.168 9.756 -17.604 11.484zm0 -21.924l12.348 -10.26 17.604 11.484 -12.168 9.72z","STACK_OVERFLOW":"M0 0.037v-26.496h4.356v22.248h33.408v-22.248h4.32v25.236l-0.036 1.26v0.036h-40.752l-1.26 -0.036h-0.036zm7.956 -8.568v-5.436l25.452 -0.036v5.436zm0.216 -9.36l0.504 -5.4 25.344 2.34 -0.468 5.4zm1.548 -11.52l1.404 -5.256 24.588 6.588 -1.404 5.256zm4.5 -13.14l2.772 -4.68 21.924 12.96 -2.772 4.68zm11.232 -13.5l4.5 -3.096 14.328 21.06 -4.464 3.06zm15.444 -7.452l5.364 -0.936 4.356 25.092 -5.364 0.936z","INSTAGRAM":"M0 -11.627v-41.112q0 -2.916 2.088 -5.004t5.004 -2.088h41.112q2.916 0 5.004 2.088t2.088 5.004v41.112q0 2.916 -2.088 5.004t-5.004 2.088h-41.112q-2.916 0 -5.004 -2.088t-2.088 -5.004zm6.156 -1.476q0 0.936 0.63 1.566t1.566 0.63h38.484q0.9 0 1.548 -0.63t0.648 -1.566v-23.328h-4.86q0.72 2.268 0.72 4.716 0 4.536 -2.304 8.37t-6.264 6.066 -8.64 2.232q-7.092 0 -12.132 -4.878t-5.04 -11.79q0 -2.448 0.72 -4.716h-5.076v23.328zm10.44 -19.188q0 4.464 3.258 7.614t7.83 3.15q4.608 0 7.866 -3.15t3.258 -7.614 -3.258 -7.614 -7.866 -3.15q-4.572 0 -7.83 3.15t-3.258 7.614zm21.204 -12.96q0 1.044 0.72 1.764t1.764 0.72h6.264q1.044 0 1.764 -0.72t0.72 -1.764v-5.94q0 -1.008 -0.72 -1.746t-1.764 -0.738h-6.264q-1.044 0 -1.764 0.738t-0.72 1.746v5.94z","FLICKR":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.864 -17.28q0 3.168 2.232 5.4t5.4 2.232 5.4 -2.232 2.232 -5.4 -2.232 -5.4 -5.4 -2.232 -5.4 2.232 -2.232 5.4zm20.304 0q0 3.168 2.232 5.4t5.4 2.232 5.4 -2.232 2.232 -5.4 -2.232 -5.4 -5.4 -2.232 -5.4 2.232 -2.232 5.4z","ADN":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm11.124 9.216h3.384l3.744 -5.76h18.792l3.744 5.76h3.384l-16.524 -24.876zm9.288 -8.064l7.236 -11.016 7.236 11.016h-14.472z","BITBUCKET":"M0 -53.135q0.108 -0.936 0.63 -1.746t1.134 -1.35 1.62 -1.08 1.656 -0.81 1.728 -0.666q4.5 -1.656 11.268 -2.304 13.644 -1.332 24.336 1.8 5.58 1.656 7.74 4.392 0.576 0.72 0.594 1.836t-0.198 1.944q-0.936 6.012 -3.996 23.58 -0.18 1.08 -0.972 2.016t-1.566 1.44 -1.962 1.116q-9.072 4.536 -21.96 3.168 -8.928 -0.972 -14.184 -5.004 -0.54 -0.432 -0.918 -0.954t-0.612 -1.26 -0.324 -1.224 -0.216 -1.422 -0.198 -1.26q-0.324 -1.8 -0.954 -5.4t-1.008 -5.814 -0.846 -5.31 -0.792 -5.688zm6.228 33.732l0.216 -0.576 0.648 -0.324q8.028 5.328 18.234 5.328t18.27 -5.328q0.756 0.216 0.864 0.828t-0.18 1.62 -0.288 1.332q-0.288 0.936 -0.558 2.754t-0.504 3.024 -1.026 2.52 -2.088 2.034q-3.096 1.728 -6.822 2.574t-7.272 0.792 -7.254 -0.666q-1.656 -0.288 -2.934 -0.648t-2.754 -0.972 -2.628 -1.566 -1.872 -2.214q-0.9 -3.456 -2.052 -10.512zm2.52 -34.38q1.08 1.008 2.736 1.638t2.646 0.792 3.15 0.414q8.208 1.044 16.128 0.036 2.268 -0.288 3.222 -0.432t2.61 -0.774 2.7 -1.674q-0.72 -0.972 -2.016 -1.602t-2.088 -0.792 -2.556 -0.45q-10.476 -1.692 -20.376 0.072 -1.548 0.252 -2.376 0.432t-1.98 0.792 -1.8 1.548zm8.568 20.988q0.144 3.276 2.79 5.58t5.958 2.016q3.276 -0.288 5.472 -3.024t1.8 -6.048q-0.504 -3.852 -4.068 -5.904t-7.092 -0.468q-2.268 1.008 -3.618 3.186t-1.242 4.662zm4.248 -1.908q0.504 -1.476 1.872 -2.088 1.296 -0.648 2.61 -0.432t2.304 1.278 0.99 2.43q0.288 2.268 -1.818 3.636t-4.014 0.216q-1.404 -0.612 -1.926 -2.088t-0.018 -2.952z","BITBUCKET_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.36 -32.4q0.288 2.448 0.684 4.968t1.044 6.156 0.864 4.932q0.036 0.18 0.18 1.116t0.252 1.296 0.432 0.972 0.792 1.008q3.78 2.88 10.224 3.6 9.324 1.008 15.84 -2.268 0.864 -0.468 1.422 -0.828t1.116 -1.044 0.702 -1.44q1.728 -9.612 2.88 -17.028 0.324 -1.908 -0.288 -2.7 -1.548 -1.98 -5.58 -3.168 -7.776 -2.268 -17.532 -1.296 -4.752 0.432 -8.136 1.656 -1.368 0.54 -2.142 0.9t-1.692 1.224 -1.062 1.944zm4.5 24.336q0.936 5.544 1.476 7.56 1.692 2.916 7.344 3.888 8.964 1.656 15.408 -1.908 1.224 -0.684 1.764 -1.854t0.81 -3.078 0.45 -2.556q0 -0.252 0.198 -0.954t0.108 -1.152 -0.63 -0.594q-5.796 3.816 -13.14 3.816t-13.176 -3.816l-0.432 0.216zm1.836 -24.804q0.468 -0.684 1.296 -1.116t1.44 -0.558 1.692 -0.306q7.128 -1.26 14.688 -0.036 1.188 0.18 1.836 0.306t1.548 0.576 1.404 1.134q-0.72 0.756 -1.926 1.224t-1.908 0.576 -2.286 0.288q-5.58 0.72 -11.664 0 -1.584 -0.216 -2.268 -0.342t-1.89 -0.576 -1.962 -1.17zm6.156 15.12q-0.072 -1.764 0.918 -3.348t2.61 -2.304q2.52 -1.116 5.094 0.36t2.934 4.248q0.288 2.376 -1.296 4.356t-3.96 2.196 -4.284 -1.44 -2.016 -4.068zm2.898 -0.27q0.018 1.89 1.566 2.538 1.404 0.828 2.916 -0.144t1.296 -2.592q0 -1.548 -1.476 -2.376t-2.772 -0.036q-1.548 0.72 -1.53 2.61z","TUMBLR":"M2.808 -37.187v-7.812q3.276 -1.08 5.58 -3.024 2.304 -1.98 3.708 -4.752 1.404 -2.808 1.944 -7.056h7.884v13.968h13.104v8.676h-13.104v14.184q0 4.896 0.504 6.192 0.468 1.332 1.872 2.16 1.8 1.116 4.212 1.116 4.212 0 8.352 -2.736v8.712q-3.672 1.728 -6.408 2.34 -2.772 0.684 -6.228 0.684 -3.78 0 -6.696 -0.972 -2.808 -0.9 -4.968 -2.7 -2.088 -1.836 -2.844 -3.78 -0.792 -1.944 -0.792 -5.796v-19.404h-6.12z","TUMBLR_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm14.868 -21.024h4.572v14.544q0 2.808 0.612 4.356 0.612 1.512 2.124 2.808 1.548 1.332 3.744 2.052 2.232 0.72 5.04 0.72 2.412 0 4.644 -0.504 2.052 -0.468 4.824 -1.764v-6.516q-3.168 2.016 -6.264 2.016 -1.836 0 -3.168 -0.828 -1.044 -0.612 -1.404 -1.62 -0.396 -1.08 -0.396 -4.644v-10.62h9.864v-6.516h-9.864v-10.476h-5.904q-0.396 3.24 -1.44 5.292t-2.808 3.564q-1.728 1.44 -4.176 2.268v5.868z","LONG_ARROW_DOWN":"M0.108 -17.675q0.324 -0.684 1.044 -0.684h8.064v-44.928q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v44.928h8.064q0.756 0 1.044 0.684t-0.18 1.26l-12.6 13.824q-0.36 0.36 -0.828 0.36 -0.504 0 -0.864 -0.36l-12.78 -13.824q-0.468 -0.576 -0.18 -1.26z","LONG_ARROW_UP":"M0.108 -46.691q-0.288 -0.684 0.18 -1.26l12.6 -13.824q0.36 -0.36 0.828 -0.36 0.504 0 0.864 0.36l12.78 13.824q0.468 0.576 0.18 1.26 -0.324 0.684 -1.044 0.684h-8.064v44.928q0 0.504 -0.324 0.828t-0.828 0.324h-6.912q-0.504 0 -0.828 -0.324t-0.324 -0.828v-44.928h-8.064q-0.756 0 -1.044 -0.684z","LONG_ARROW_LEFT":"M2.304 -32.075q0 -0.504 0.36 -0.864l13.824 -12.744q0.576 -0.504 1.26 -0.216 0.684 0.324 0.684 1.044v8.064h44.928q0.504 0 0.828 0.324t0.324 0.828v6.912q0 0.504 -0.324 0.828t-0.828 0.324h-44.928v8.064q0 0.756 -0.684 1.044t-1.26 -0.18l-13.824 -12.6q-0.36 -0.36 -0.36 -0.828z","LONG_ARROW_RIGHT":"M0 -28.727v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324h44.928v-8.064q0 -0.756 0.684 -1.044t1.26 0.18l13.824 12.6q0.36 0.36 0.36 0.828 0 0.504 -0.36 0.864l-13.824 12.744q-0.576 0.504 -1.26 0.216 -0.684 -0.324 -0.684 -1.044v-8.064h-44.928q-0.504 0 -0.828 -0.324t-0.324 -0.828z","APPLE":"M0 -31.967q0 -8.208 4.068 -13.464 4.032 -5.184 10.224 -5.184 2.592 0 6.372 1.08 3.744 1.08 4.968 1.08 1.62 0 5.148 -1.224 3.672 -1.224 6.228 -1.224 4.284 0 7.668 2.34 1.872 1.296 3.744 3.6 -2.844 2.412 -4.104 4.248 -2.34 3.384 -2.34 7.452 0 4.464 2.484 8.028t5.688 4.536q-1.404 4.5 -4.428 9 -4.644 7.056 -9.252 7.056 -1.764 0 -5.04 -1.152 -3.096 -1.152 -5.436 -1.152 -2.196 0 -5.112 1.188 -2.916 1.224 -4.752 1.224 -5.472 0 -10.836 -9.324 -5.292 -9.396 -5.292 -18.108zm24.588 -17.892q0.108 -5.364 2.808 -9.252 2.664 -3.852 9 -5.328l0.09 0.396 0.09 0.396 0.018 0.36 0.018 0.36q0 2.196 -1.044 4.896 -1.08 2.7 -3.348 4.968 -1.944 1.944 -3.888 2.592 -1.332 0.396 -3.744 0.612z","WINDOWS":"M0 -8.171v-20.052h24.552v23.436zm0 -23.076v-20.34l24.552 -3.384v23.724h-24.552zm27.252 26.82v-23.796h32.652v28.296zm0 -26.82v-24.084l32.652 -4.5v28.584h-32.652z","ANDROID":"M0 -21.275v-15.48q0 -1.512 1.08 -2.592t2.628 -1.08q1.512 0 2.592 1.08t1.08 2.592v15.48q0 1.548 -1.062 2.628t-2.61 1.08 -2.628 -1.08 -1.08 -2.628zm8.676 -19.764q0 -4.212 2.304 -7.758t6.192 -5.526l-2.556 -4.716q-0.252 -0.468 0.18 -0.72 0.468 -0.216 0.72 0.216l2.592 4.752q3.42 -1.512 7.236 -1.512t7.236 1.512l2.592 -4.752q0.252 -0.432 0.72 -0.216 0.432 0.252 0.18 0.72l-2.556 4.716q3.852 1.98 6.156 5.526t2.304 7.758h-33.3zm0.144 25.272v-23.976h33.048v23.976q0 1.656 -1.152 2.808t-2.772 1.152h-2.7v8.172q0 1.548 -1.08 2.628t-2.628 1.08 -2.628 -1.08 -1.08 -2.628v-8.172h-4.968v8.172q0 1.548 -1.08 2.628t-2.628 1.08q-1.512 0 -2.592 -1.08t-1.08 -2.628l-0.036 -8.172h-2.664q-1.656 0 -2.808 -1.152t-1.152 -2.808zm7.56 -32.688q0 0.576 0.396 0.99t0.972 0.414 0.99 -0.414 0.414 -0.99 -0.414 -0.99 -0.99 -0.414 -0.972 0.414 -0.396 0.99zm15.156 0q0 0.576 0.414 0.99t0.99 0.414 0.972 -0.414 0.396 -0.99 -0.396 -0.99 -0.972 -0.414 -0.99 0.414 -0.414 0.99zm11.772 27.18v-15.48q0 -1.548 1.08 -2.61t2.592 -1.062q1.548 0 2.628 1.062t1.08 2.61v15.48q0 1.548 -1.08 2.628t-2.628 1.08q-1.512 0 -2.592 -1.08t-1.08 -2.628z","LINUX":"M0.396 -5.003q-0.36 -0.828 0.252 -2.394t0.648 -1.962q0.036 -0.576 -0.144 -1.44t-0.36 -1.53 -0.162 -1.314 0.378 -0.972q0.504 -0.432 2.052 -0.504t2.16 -0.432q1.08 -0.648 1.512 -1.26t0.432 -1.836q0.756 2.628 -1.152 3.816 -1.152 0.72 -2.988 0.54 -1.224 -0.108 -1.548 0.36 -0.468 0.54 0.18 2.052 0.072 0.216 0.288 0.648t0.306 0.648 0.162 0.612 0.036 0.792q0 0.54 -0.612 1.764t-0.504 1.728q0.108 0.612 1.332 0.936 0.72 0.216 3.042 0.666t3.582 0.738q0.864 0.216 2.664 0.792t2.97 0.828 1.998 0.144q1.548 -0.216 2.322 -1.008t0.828 -1.728 -0.27 -2.106 -0.684 -1.872 -0.72 -1.314q-4.356 -6.84 -6.084 -8.712 -2.448 -2.664 -4.068 -1.44 -0.396 0.324 -0.54 -0.54 -0.108 -0.576 -0.072 -1.368 0.036 -1.044 0.36 -1.872t0.864 -1.692 0.792 -1.512q0.288 -0.756 0.954 -2.592t1.062 -2.808 1.08 -2.196 1.404 -1.944q3.96 -5.148 4.464 -7.02 -0.432 -4.032 -0.576 -11.16 -0.072 -3.24 0.864 -5.454t3.816 -3.762q1.404 -0.756 3.744 -0.756 1.908 -0.036 3.816 0.486t3.204 1.494q2.052 1.512 3.294 4.374t1.062 5.31q-0.18 3.42 1.08 7.704 1.224 4.068 4.788 7.848 1.98 2.124 3.582 5.868t2.142 6.876q0.288 1.764 0.18 3.042t-0.432 1.998 -0.72 0.792q-0.36 0.072 -0.846 0.684t-0.972 1.278 -1.458 1.206 -2.196 0.504q-0.648 -0.036 -1.134 -0.18t-0.81 -0.486 -0.486 -0.558 -0.414 -0.738 -0.324 -0.702q-0.792 -1.332 -1.476 -1.08t-1.008 1.764 0.252 3.492q0.72 2.52 0.036 7.02 -0.36 2.34 0.648 3.618t2.628 1.188 3.06 -1.278q2.124 -1.764 3.222 -2.394t3.726 -1.53q1.908 -0.648 2.772 -1.314t0.666 -1.242 -0.9 -1.026 -1.854 -0.846q-1.188 -0.396 -1.782 -1.728t-0.54 -2.61 0.558 -1.71q0.036 1.116 0.288 2.034t0.522 1.458 0.738 1.026 0.756 0.684 0.774 0.468 0.594 0.342q0.72 0.432 1.116 0.882t0.432 0.864 -0.09 0.81 -0.558 0.792 -0.846 0.702 -1.08 0.666 -1.134 0.594 -1.152 0.558 -0.972 0.468q-1.368 0.684 -3.078 2.016t-2.718 2.304q-0.612 0.576 -2.448 0.702t-3.204 -0.522q-0.648 -0.324 -1.062 -0.846t-0.594 -0.918 -0.792 -0.702 -1.692 -0.342q-1.584 -0.036 -4.68 -0.036 -0.684 0 -2.052 0.054t-2.088 0.09q-1.584 0.036 -2.862 0.54t-1.926 1.08 -1.566 1.026 -1.926 0.414q-1.044 -0.036 -3.996 -1.116t-5.256 -1.548q-0.684 -0.144 -1.836 -0.342t-1.8 -0.324 -1.422 -0.342 -1.206 -0.522 -0.612 -0.702zm11.16 -21.96q-1.296 2.34 0.36 5.976 0.18 0.432 0.9 1.008t0.864 0.72q0.72 0.828 3.744 3.258t3.348 2.754q0.576 0.54 0.63 1.368t-0.504 1.548 -1.638 0.828q0.288 0.54 1.044 1.602t1.008 1.944 0.252 2.538q1.656 -0.864 0.252 -3.312 -0.144 -0.288 -0.378 -0.576t-0.342 -0.432 -0.072 -0.216q0.108 -0.18 0.468 -0.342t0.72 0.09q1.656 1.872 5.976 1.296 4.788 -0.54 6.372 -3.132 0.828 -1.368 1.224 -1.08 0.432 0.216 0.36 1.872 -0.036 0.9 -0.828 3.312 -0.324 0.828 -0.216 1.35t0.864 0.558q0.108 -0.684 0.522 -2.772t0.486 -3.24q0.072 -0.756 -0.234 -2.646t-0.27 -3.492 0.828 -2.538q0.54 -0.648 1.836 -0.648 0.036 -1.332 1.242 -1.908t2.61 -0.378 2.16 0.81q0 -0.648 -1.98 -1.512 0.144 -0.54 0.27 -0.99t0.18 -0.936 0.108 -0.774 0.018 -0.81 -0.036 -0.702 -0.126 -0.792 -0.144 -0.738 -0.18 -0.9 -0.198 -0.954q-0.36 -1.728 -1.692 -3.708t-2.592 -2.7q0.864 0.72 2.052 2.988 3.132 5.832 1.944 10.008 -0.396 1.44 -1.8 1.512 -1.116 0.144 -1.386 -0.666t-0.288 -3.006 -0.414 -3.852q-0.324 -1.404 -0.702 -2.484t-0.702 -1.638 -0.558 -0.882 -0.468 -0.54 -0.27 -0.252q-0.504 -2.232 -1.116 -3.708t-1.062 -2.016 -0.846 -1.188 -0.54 -1.44q-0.144 -0.756 0.216 -1.926t0.162 -1.782 -1.602 -0.9q-0.54 -0.108 -1.602 -0.648t-1.278 -0.576q-0.288 -0.036 -0.396 -0.936t0.288 -1.836 1.296 -0.972q1.332 -0.108 1.836 1.08t0.144 2.088q-0.396 0.684 -0.072 0.954t1.08 0.018q0.468 -0.144 0.468 -1.296v-1.332q-0.18 -1.08 -0.486 -1.8t-0.756 -1.098 -0.846 -0.54 -0.972 -0.27q-3.852 0.288 -3.204 4.824 0 0.54 -0.036 0.54 -0.324 -0.324 -1.062 -0.378t-1.188 0.018 -0.558 -0.18q0.036 -2.052 -0.576 -3.24t-1.62 -1.224q-0.972 -0.036 -1.494 0.99t-0.594 2.142q-0.036 0.54 0.126 1.332t0.468 1.35 0.558 0.486q0.36 -0.108 0.576 -0.504 0.144 -0.324 -0.252 -0.288 -0.252 0 -0.558 -0.522t-0.342 -1.206q-0.036 -0.792 0.324 -1.332t1.224 -0.504q0.612 0 0.972 0.756t0.342 1.404 -0.054 0.792q-0.792 0.54 -1.116 1.044 -0.288 0.432 -0.99 0.846t-0.738 0.45q-0.468 0.504 -0.558 0.972t0.27 0.648q0.504 0.288 0.9 0.702t0.576 0.684 0.666 0.468 1.278 0.234q1.692 0.072 3.672 -0.54 0.072 -0.036 0.828 -0.252t1.242 -0.378 1.062 -0.468 0.756 -0.63q0.324 -0.504 0.72 -0.288 0.18 0.108 0.234 0.306t-0.108 0.432 -0.594 0.342q-0.72 0.216 -2.034 0.774t-1.638 0.702q-1.584 0.684 -2.52 0.828 -0.9 0.18 -2.844 -0.072 -0.36 -0.072 -0.324 0.072t0.612 0.684q0.9 0.828 2.412 0.792 0.612 -0.036 1.296 -0.252t1.296 -0.504 1.206 -0.63 1.08 -0.612 0.882 -0.432 0.63 -0.09 0.306 0.396q0 0.072 -0.036 0.162t-0.144 0.18 -0.216 0.162 -0.306 0.18 -0.324 0.162 -0.36 0.18 -0.342 0.162q-1.008 0.504 -2.43 1.584t-2.394 1.548 -1.764 0.036q-0.756 -0.396 -2.268 -2.628 -0.792 -1.116 -0.9 -0.792 -0.036 0.108 -0.036 0.36 0 0.9 -0.54 2.034t-1.062 1.998 -0.756 2.088 0.414 2.268q-0.828 0.216 -2.25 3.24t-1.71 5.076q-0.072 0.648 -0.054 2.484t-0.198 2.124q-0.288 0.864 -1.044 0.108 -1.152 -1.116 -1.296 -3.384 -0.072 -1.008 0.144 -2.016 0.144 -0.684 -0.036 -0.648zm1.836 -4.86q0.144 0.036 0.45 -0.252t0.45 -0.648l0.072 -0.252 0.072 -0.216 0.054 -0.162 0.018 -0.144v-0.108l-0.036 -0.09 -0.108 -0.072q-0.144 -0.036 -0.216 0.108t-0.162 0.45 -0.198 0.486 -0.36 0.468q-0.252 0.36 -0.036 0.432zm8.316 -20.16q0.072 0.18 0.18 0.216 0.36 0 0.252 0.54 -0.108 0.72 0.288 0.72 0.108 0 0.108 -0.108 0.108 -0.612 -0.09 -1.08t-0.414 -0.54q-0.324 -0.072 -0.324 0.252zm1.116 2.88q0 -0.432 0.684 -0.54h0.36q-0.396 0.036 -0.558 0.378t-0.306 0.342q-0.18 0.036 -0.18 -0.18zm3.132 -0.432q0.864 -0.396 1.152 0.072 0.108 0.216 -0.108 0.324 -0.144 0.036 -0.414 -0.234t-0.63 -0.162zm4.104 -2.664l0.144 0.072q0.504 0.144 0.648 1.116 0 0.108 0.288 -0.072l0.072 -0.108q0 -0.396 -0.18 -0.702t-0.396 -0.45 -0.324 -0.108q-0.504 0.036 -0.252 0.252zm0.576 -6.678q-0.036 0.09 0.108 0.306 0.144 0.108 0.288 0t0.396 -0.324 0.54 -0.324q0.036 -0.036 0.324 -0.036t0.54 -0.072 0.324 -0.252q0 -0.072 -0.09 -0.18t-0.324 -0.252 -0.342 -0.216q-0.54 -0.54 -0.864 -0.54 -0.324 0.036 -0.414 0.27t-0.036 0.468 -0.018 0.45q-0.036 0.144 -0.216 0.378t-0.216 0.324zm4.68 13.662q-0.504 0.576 0.252 1.566t1.404 1.134q0.324 0.036 0.522 -0.288t0.126 -0.72q-0.072 -0.288 -0.234 -0.414t-0.468 -0.18 -0.522 -0.198q-0.18 -0.108 -0.342 -0.288t-0.252 -0.288 -0.198 -0.234 -0.144 -0.144 -0.144 0.054z","DRIBBBLE":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm4.032 0q0 4.464 1.584 8.514t4.464 7.254q1.8 -3.204 4.446 -5.994t5.13 -4.482 4.698 -2.916 3.582 -1.728l1.332 -0.468q0.144 -0.036 0.468 -0.126t0.468 -0.162q-0.756 -1.764 -1.908 -3.996 -11.196 3.348 -24.228 3.348 -0.036 0.252 -0.036 0.756zm0.504 -4.86q10.872 0 21.816 -2.88 -4.32 -7.668 -8.784 -13.608 -4.968 2.34 -8.424 6.696t-4.608 9.792zm8.064 23.076q6.624 5.4 15.048 5.4 4.752 0 9.216 -1.872 -1.512 -8.676 -5.04 -17.928h-0.072l-0.072 0.036q-0.576 0.216 -1.548 0.594t-3.636 1.764 -4.932 2.952 -4.716 4.122 -3.708 5.328zm9.324 -41.112l0.072 -0.036 -0.072 0.036zm0.144 -0.036q4.716 6.12 8.856 13.752 2.484 -0.936 4.68 -2.178t3.474 -2.214 2.358 -2.052 1.35 -1.458l0.45 -0.63q-6.66 -5.904 -15.588 -5.904 -2.736 0 -5.58 0.684zm10.656 17.28q0.9 1.908 1.584 3.42 0.072 0.216 0.234 0.63t0.27 0.594q1.296 -0.18 2.682 -0.252t2.646 -0.072 2.484 0.054 2.304 0.144 2.034 0.198 1.728 0.234 1.314 0.216 0.9 0.162l0.36 0.072q-0.108 -8.352 -5.364 -14.76l-0.036 0.036q-0.324 0.432 -0.684 0.882t-1.566 1.602 -2.556 2.178 -3.6 2.34 -4.734 2.322zm3.528 8.352q3.132 8.604 4.608 16.884 3.996 -2.7 6.66 -6.822t3.456 -9.018q-7.56 -2.16 -14.724 -1.044z","SKYPE":"M0 -46.007q0 -5.724 4.05 -9.774t9.774 -4.05q4.68 0 8.424 2.88 2.772 -0.576 5.4 -0.576 5.148 0 9.846 1.998t8.1 5.4 5.4 8.1 1.998 9.846q0 2.628 -0.576 5.4 2.88 3.744 2.88 8.424 0 5.724 -4.05 9.774t-9.774 4.05q-4.68 0 -8.424 -2.88 -2.772 0.576 -5.4 0.576 -5.148 0 -9.846 -1.998t-8.1 -5.4 -5.4 -8.1 -1.998 -9.846q0 -2.628 0.576 -5.4 -2.88 -3.744 -2.88 -8.424zm13.536 22.5q0 3.312 4.392 5.67t10.476 2.358q2.628 0 5.04 -0.666t4.41 -1.926 3.186 -3.366 1.188 -4.734q0 -1.8 -0.702 -3.294t-1.746 -2.466 -2.628 -1.764 -2.97 -1.224 -3.15 -0.828l-3.744 -0.864q-1.08 -0.252 -1.584 -0.378t-1.26 -0.414 -1.08 -0.576 -0.594 -0.756 -0.27 -1.08q0 -2.772 5.184 -2.772 1.548 0 2.772 0.432t1.944 1.026 1.368 1.206 1.44 1.044 1.728 0.432q1.692 0 2.718 -1.152t1.026 -2.772q0 -1.98 -2.016 -3.582t-5.112 -2.43 -6.552 -0.828q-2.448 0 -4.752 0.558t-4.302 1.692 -3.204 3.132 -1.206 4.626q0 2.196 0.684 3.834t2.016 2.718 2.88 1.746 3.708 1.17l5.256 1.296q3.24 0.792 4.032 1.296 1.152 0.72 1.152 2.16 0 1.404 -1.44 2.322t-3.78 0.918q-1.836 0 -3.294 -0.576t-2.34 -1.386 -1.638 -1.62 -1.656 -1.386 -1.944 -0.576q-1.8 0 -2.718 1.08t-0.918 2.7z","FOURSQUARE":"M0 -32.183q0 -2.7 1.908 -4.608l21.132 -21.132q1.908 -1.908 4.608 -1.908t4.608 1.908l9.54 9.54 -14.328 14.364 -6.768 -6.768q-1.512 -1.512 -3.564 -1.512 -2.124 0 -3.6 1.476l-4.32 4.356q-1.512 1.44 -1.512 3.564 0 2.088 1.512 3.6l14.616 14.688q1.08 1.008 2.412 1.332l0.216 0.144h1.008q2.16 0 3.564 -1.476l22.284 -22.284 0.072 0.108q1.908 1.908 1.908 4.608t-1.908 4.608l-21.132 21.132q-1.872 1.908 -4.59 1.908t-4.626 -1.908l-21.132 -21.132q-1.908 -1.908 -1.908 -4.608zm10.872 -0.72q0 -0.756 0.504 -1.26l4.356 -4.32q0.468 -0.54 1.26 -0.54t1.296 0.54l9.072 9.072 20.664 -20.7q0.54 -0.54 1.296 -0.54t1.296 0.54l4.32 4.32q0.504 0.54 0.504 1.296t-0.504 1.296l-26.28 26.28q-0.612 0.54 -1.332 0.54 -0.144 0 -0.216 -0.036 -0.648 -0.072 -1.08 -0.504l-14.652 -14.688q-0.504 -0.54 -0.504 -1.296z","TRELLO":"M0 -6.839v-50.688q0 -0.936 0.684 -1.62t1.62 -0.684h50.688q0.936 0 1.62 0.684t0.684 1.62v50.688q0 0.936 -0.684 1.62t-1.62 0.684h-50.688q-0.936 0 -1.62 -0.684t-0.684 -1.62zm5.76 -9.216q0 0.504 0.324 0.828t0.828 0.324h17.28q0.504 0 0.828 -0.324t0.324 -0.828v-36.864q0 -0.504 -0.324 -0.828t-0.828 -0.324h-17.28q-0.504 0 -0.828 0.324t-0.324 0.828v36.864zm24.192 -13.824q0 0.504 0.324 0.828t0.828 0.324h17.28q0.504 0 0.828 -0.324t0.324 -0.828v-23.04q0 -0.504 -0.324 -0.828t-0.828 -0.324h-17.28q-0.504 0 -0.828 0.324t-0.324 0.828v23.04z","FEMALE":"M0 -26.423q0 -1.044 0.576 -1.908l9.216 -13.824q2.628 -3.852 6.336 -3.852h13.824q3.708 0 6.336 3.852l9.216 13.824q0.576 0.864 0.576 1.908 0 1.44 -1.008 2.448t-2.448 1.008q-1.836 0 -2.88 -1.548l-8.172 -12.276h-1.62v4.752l8.892 14.796q0.324 0.54 0.324 1.188 0 0.936 -0.684 1.62t-1.62 0.684h-6.912v9.792q0 1.656 -1.188 2.844t-2.844 1.188h-5.76q-1.656 0 -2.844 -1.188t-1.188 -2.844v-9.792h-6.912q-0.936 0 -1.62 -0.684t-0.684 -1.62q0 -0.648 0.324 -1.188l8.892 -14.796v-4.752h-1.62l-8.172 12.276q-1.044 1.548 -2.88 1.548 -1.44 0 -2.448 -1.008t-1.008 -2.448zm14.976 -28.8q0 -3.348 2.358 -5.706t5.706 -2.358 5.706 2.358 2.358 5.706 -2.358 5.706 -5.706 2.358 -5.706 -2.358 -2.358 -5.706z","MALE":"M0 -24.119v-14.976q0 -2.88 2.016 -4.896t4.896 -2.016h23.04q2.88 0 4.896 2.016t2.016 4.896v14.976q0 1.44 -1.008 2.448t-2.448 1.008 -2.448 -1.008 -1.008 -2.448v-12.672h-2.304v32.832q0 1.656 -1.188 2.844t-2.844 1.188 -2.844 -1.188 -1.188 -2.844v-16.704h-2.304v16.704q0 1.656 -1.188 2.844t-2.844 1.188 -2.844 -1.188 -1.188 -2.844v-32.832h-2.304v12.672q0 1.44 -1.008 2.448t-2.448 1.008 -2.448 -1.008 -1.008 -2.448zm10.368 -31.104q0 -3.348 2.358 -5.706t5.706 -2.358 5.706 2.358 2.358 5.706 -2.358 5.706 -5.706 2.358 -5.706 -2.358 -2.358 -5.706z","GITTIP":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm14.382 -4.536q0.306 1.332 0.882 2.124l12.564 17.028 12.6 -17.028q0.576 -0.792 0.882 -2.124t-0.216 -3.06 -2.214 -2.844q-1.44 -0.936 -2.988 -0.918t-2.646 0.63 -1.962 1.62q-1.296 1.44 -3.456 1.44 -2.124 0 -3.42 -1.44 -0.864 -1.008 -1.962 -1.62t-2.646 -0.63 -3.024 0.918q-1.656 1.116 -2.178 2.844t-0.216 3.06z","SUN_O":"M1.584 -22.211q-0.18 -0.612 0.144 -1.044l6.48 -8.928 -6.48 -8.928q-0.324 -0.468 -0.144 -1.044 0.144 -0.54 0.72 -0.72l10.512 -3.456v-11.016q0 -0.576 0.468 -0.936 0.54 -0.36 1.044 -0.144l10.512 3.384 6.48 -8.928q0.324 -0.432 0.936 -0.432t0.936 0.432l6.48 8.928 10.512 -3.384q0.504 -0.216 1.044 0.144 0.468 0.36 0.468 0.936v11.016l10.512 3.456q0.576 0.18 0.72 0.72 0.18 0.576 -0.144 1.044l-6.48 8.928 6.48 8.928q0.324 0.432 0.144 1.044 -0.144 0.54 -0.72 0.72l-10.512 3.456v11.016q0 0.576 -0.468 0.936 -0.54 0.36 -1.044 0.144l-10.512 -3.384 -6.48 8.928q-0.36 0.468 -0.936 0.468t-0.936 -0.468l-6.48 -8.928 -10.512 3.384q-0.504 0.216 -1.044 -0.144 -0.468 -0.36 -0.468 -0.936v-11.016l-10.512 -3.456q-0.576 -0.18 -0.72 -0.72zm9.936 -9.972q0 4.212 1.638 8.046t4.428 6.624 6.624 4.428 8.046 1.638 8.046 -1.638 6.624 -4.428 4.428 -6.624 1.638 -8.046 -1.638 -8.046 -4.428 -6.624 -6.624 -4.428 -8.046 -1.638 -8.046 1.638 -6.624 4.428 -4.428 6.624 -1.638 8.046z","MOON_O":"M0 -32.183q0 -5.508 2.07 -10.53t5.616 -8.694 8.478 -5.922 10.44 -2.466q1.584 -0.072 2.196 1.404 0.648 1.476 -0.54 2.592 -3.096 2.808 -4.734 6.534t-1.638 7.866q0 5.328 2.628 9.828t7.128 7.128 9.828 2.628q4.248 0 8.208 -1.836 1.476 -0.648 2.592 0.468 0.504 0.504 0.63 1.224t-0.162 1.368q-3.384 7.308 -10.206 11.682t-14.886 4.374q-5.616 0 -10.728 -2.196t-8.82 -5.904 -5.904 -8.82 -2.196 -10.728zm4.608 0q0 4.68 1.836 8.946t4.914 7.344 7.344 4.914 8.946 1.836q5.184 0 9.846 -2.214t7.938 -6.174q-1.944 0.324 -3.96 0.324 -6.552 0 -12.132 -3.24t-8.82 -8.82 -3.24 -12.132q0 -6.912 3.744 -12.852 -7.236 2.16 -11.826 8.244t-4.59 13.824z","ARCHIVE":"M2.304 -48.311v-9.216q0 -0.936 0.684 -1.62t1.62 -0.684h55.296q0.936 0 1.62 0.684t0.684 1.62v9.216q0 0.936 -0.684 1.62t-1.62 0.684h-55.296q-0.936 0 -1.62 -0.684t-0.684 -1.62zm2.304 41.472v-34.56q0 -0.936 0.684 -1.62t1.62 -0.684h50.688q0.936 0 1.62 0.684t0.684 1.62v34.56q0 0.936 -0.684 1.62t-1.62 0.684h-50.688q-0.936 0 -1.62 -0.684t-0.684 -1.62zm20.736 -27.648q0 0.936 0.684 1.62t1.62 0.684h9.216q0.936 0 1.62 -0.684t0.684 -1.62 -0.684 -1.62 -1.62 -0.684h-9.216q-0.936 0 -1.62 0.684t-0.684 1.62z","BUG":"M1.152 -29.879q0 -0.936 0.684 -1.62t1.62 -0.684h8.064v-10.584l-6.228 -6.228q-0.684 -0.684 -0.684 -1.62t0.684 -1.62 1.62 -0.684 1.62 0.684l6.228 6.228h30.384l6.228 -6.228q0.684 -0.684 1.62 -0.684t1.62 0.684 0.684 1.62 -0.684 1.62l-6.228 6.228v10.584h8.064q0.936 0 1.62 0.684t0.684 1.62 -0.684 1.62 -1.62 0.684h-8.064q0 6.156 -2.412 10.44l7.488 7.524q0.684 0.684 0.684 1.62t-0.684 1.62q-0.648 0.684 -1.62 0.684t-1.62 -0.684l-7.128 -7.092q-0.18 0.18 -0.54 0.468t-1.512 1.026 -2.34 1.314 -2.952 1.044 -3.492 0.468v-32.256h-4.608v32.256q-1.836 0 -3.654 -0.486t-3.132 -1.188 -2.376 -1.404 -1.566 -1.17l-0.54 -0.504 -6.588 7.452q-0.72 0.756 -1.728 0.756 -0.864 0 -1.548 -0.576 -0.684 -0.648 -0.738 -1.602t0.558 -1.674l7.272 -8.172q-2.088 -4.104 -2.088 -9.864h-8.064q-0.936 0 -1.62 -0.684t-0.684 -1.62zm17.28 -20.736q0 -4.788 3.366 -8.154t8.154 -3.366 8.154 3.366 3.366 8.154h-23.04z","VK":"M-0.036 -45.287q0 -0.396 0.108 -0.576l0.144 -0.216q0.54 -0.684 2.052 -0.684l9.864 -0.072q0.432 0.072 0.828 0.234t0.576 0.306l0.18 0.108q0.576 0.396 0.864 1.152 0.72 1.8 1.656 3.726t1.476 2.934l0.576 1.044q1.044 2.16 2.016 3.744t1.746 2.466 1.494 1.386 1.224 0.504 0.972 -0.18q0.072 -0.036 0.18 -0.18t0.432 -0.792 0.486 -1.692 0.342 -2.916 0 -4.5q-0.072 -1.44 -0.324 -2.628t-0.504 -1.656l-0.216 -0.432q-0.9 -1.224 -3.06 -1.548 -0.468 -0.072 0.18 -0.864 0.612 -0.684 1.368 -1.08 1.908 -0.936 8.604 -0.864 2.952 0.036 4.86 0.468 0.72 0.18 1.206 0.486t0.738 0.864 0.378 1.152 0.126 1.638 -0.036 1.98 -0.09 2.538 -0.054 2.97q0 0.396 -0.036 1.512t-0.018 1.728 0.126 1.458 0.414 1.404 0.81 0.882q0.288 0.072 0.612 0.144t0.936 -0.396 1.368 -1.242 1.872 -2.412 2.448 -3.87q2.16 -3.744 3.852 -8.1 0.144 -0.36 0.36 -0.63t0.396 -0.378l0.144 -0.108 0.18 -0.09 0.468 -0.108 0.72 -0.018 10.368 -0.072q1.404 -0.18 2.304 0.09t1.116 0.594l0.216 0.36q0.828 2.304 -5.4 10.584 -0.864 1.152 -2.34 3.06 -2.808 3.6 -3.24 4.716 -0.612 1.476 0.504 2.916 0.612 0.756 2.916 2.952h0.036l0.036 0.036 0.036 0.036 0.072 0.072q5.076 4.716 6.876 7.956 0.108 0.18 0.234 0.45t0.252 0.954 -0.018 1.224 -0.9 0.99 -2.124 0.45l-9.216 0.144q-0.864 0.18 -2.016 -0.18t-1.872 -0.792l-0.72 -0.432q-1.08 -0.756 -2.52 -2.304t-2.466 -2.79 -2.196 -2.088 -2.034 -0.558q-0.108 0.036 -0.288 0.126t-0.612 0.522 -0.774 1.062 -0.612 1.872 -0.234 2.79q0 0.54 -0.126 0.99t-0.27 0.666l-0.144 0.18q-0.648 0.684 -1.908 0.792h-4.14q-2.556 0.144 -5.256 -0.594t-4.734 -1.908 -3.708 -2.376 -2.538 -2.07l-0.9 -0.864q-0.36 -0.36 -0.99 -1.08t-2.574 -3.276 -3.816 -5.436 -4.41 -7.596 -4.698 -9.792q-0.216 -0.576 -0.216 -0.972z","WEIBO":"M0 -23.219q0 -4.14 2.502 -8.82t7.11 -9.288q6.084 -6.084 12.294 -8.496t8.874 0.252q2.34 2.304 0.72 7.524 -0.144 0.504 -0.036 0.72t0.36 0.252 0.522 -0.018 0.486 -0.126l0.216 -0.072q5.004 -2.124 8.856 -2.124t5.508 2.196q1.62 2.268 0 6.408 -0.072 0.468 -0.162 0.72t0.162 0.45 0.432 0.27 0.612 0.216q2.052 0.648 3.708 1.692t2.88 2.934 1.224 4.194q0 2.448 -1.332 5.022t-3.924 4.932 -6.066 4.23 -8.136 2.988 -9.738 1.116 -9.9 -1.206 -8.658 -3.348 -6.174 -5.436 -2.34 -7.182zm6.516 2.556q0.324 3.456 3.204 6.12t7.506 3.924 9.882 0.756q8.028 -0.828 13.302 -5.094t4.77 -9.522q-0.324 -3.456 -3.204 -6.12t-7.506 -3.924 -9.882 -0.756q-8.028 0.828 -13.302 5.094t-4.77 9.522zm8.37 3.222q-1.458 -3.33 0.234 -6.75 1.692 -3.348 5.454 -5.004t7.578 -0.684q3.996 1.044 5.706 4.302t0.09 6.858q-1.62 3.672 -5.688 5.4t-8.064 0.432q-3.852 -1.224 -5.31 -4.554zm2.934 -0.972q0.324 1.242 1.548 1.818t2.682 0.09 2.25 -1.71q0.756 -1.224 0.396 -2.484t-1.62 -1.8q-1.224 -0.504 -2.628 -0.036t-2.16 1.656q-0.792 1.224 -0.468 2.466zm7.56 -5.094q-0.612 1.116 0.468 1.62 0.504 0.18 1.044 -0.018t0.792 -0.666q0.288 -0.468 0.126 -0.954t-0.63 -0.666q-0.504 -0.18 -1.026 0.018t-0.774 0.666zm16.56 -31.5q-0.216 -1.008 0.342 -1.854t1.566 -1.062q4.428 -0.936 8.784 0.414t7.488 4.842q3.132 3.456 4.05 8.01t-0.486 8.694q-0.324 0.972 -1.224 1.44t-1.872 0.144 -1.44 -1.224 -0.18 -1.872q1.008 -2.952 0.36 -6.192t-2.88 -5.688q-2.232 -2.484 -5.328 -3.438t-6.228 -0.306q-1.008 0.216 -1.872 -0.342t-1.08 -1.566zm2.124 8.172q-0.18 -0.864 0.288 -1.602t1.332 -0.918q2.16 -0.468 4.284 0.198t3.636 2.358 1.962 3.906 -0.234 4.23q-0.288 0.828 -1.062 1.224t-1.602 0.144q-0.828 -0.288 -1.224 -1.062t-0.144 -1.602q0.72 -2.268 -0.864 -3.996t-3.852 -1.26q-0.864 0.18 -1.62 -0.288t-0.9 -1.332z","RENREN":"M0 -32.111q0 -6.732 3.006 -12.582t8.262 -9.702 11.7 -4.932v17.46q0 9.072 -4.554 16.542t-11.898 11.034q-6.516 -7.74 -6.516 -17.82zm14.328 24.192q4.968 -3.132 8.478 -7.596t4.734 -9.648q1.26 5.184 4.77 9.648t8.478 7.596q-6.156 3.384 -13.248 3.384 -7.056 0 -13.212 -3.384zm18 -33.948v-17.46q6.444 1.08 11.7 4.932t8.262 9.702 3.006 12.582q0 10.08 -6.516 17.82 -7.344 -3.564 -11.898 -11.034t-4.554 -16.542z","PAGELINES":"M0 -1.547q0 -0.684 0.468 -1.134t1.152 -0.45q6.228 -0.036 11.61 -3.87t9.054 -10.602q-1.296 0.504 -2.592 0.828t-2.988 0.468 -3.276 -0.09 -3.348 -1.026 -3.312 -2.124 -3.042 -3.6 -2.682 -5.256q4.104 -1.692 7.704 -2.052t6.03 0.27 4.482 2.034 3.186 2.772 2.034 2.952q1.908 -4.716 2.844 -10.476 -0.252 0.036 -0.648 0.09t-1.674 0.09 -2.502 -0.018 -2.934 -0.36 -3.186 -0.828 -3.024 -1.53 -2.7 -2.34 -1.962 -3.402 -1.026 -4.59q2.52 -1.008 4.806 -1.314t4.05 0.036 3.312 1.08 2.646 1.8 2.016 2.196 1.512 2.268 0.99 2.016 0.576 1.422l0.144 0.576q0.432 -4.392 0.432 -7.02 -0.288 -0.216 -0.774 -0.576t-1.764 -1.602 -2.286 -2.574 -1.944 -3.348 -1.188 -4.05 0.432 -4.572 2.52 -4.986q2.628 0.9 4.59 2.214t3.042 2.754 1.728 3.06 0.738 3.204 -0.018 3.078 -0.468 2.754 -0.684 2.232 -0.612 1.512l-0.252 0.54q0.036 0.18 0.036 1.818t-0.036 2.574q0.108 -0.252 0.36 -0.666t1.098 -1.548 1.818 -2.088 2.556 -1.998 3.294 -1.602 4.032 -0.522 4.77 0.864q-0.072 2.808 -0.774 5.094t-1.8 3.762 -2.502 2.574 -2.934 1.638 -3.042 0.864 -2.88 0.342 -2.43 -0.036 -1.674 -0.162l-0.612 -0.108q-0.828 5.292 -2.628 10.188 0.216 -0.252 0.648 -0.666t1.782 -1.476 2.79 -1.89 3.582 -1.512 4.23 -0.72 4.644 0.846 4.932 2.79q-1.152 2.88 -2.736 4.968t-3.276 3.186 -3.564 1.674 -3.654 0.522 -3.474 -0.306 -3.114 -0.792 -2.502 -0.99 -1.656 -0.81l-0.612 -0.36q-4.068 8.208 -10.422 12.942t-13.842 4.77q-0.684 0 -1.152 -0.468t-0.468 -1.152z","STACK_EXCHANGE":"M0.756 -16.955v-2.376h44.568v2.376q0 3.06 -2.07 5.202t-4.986 2.142h-2.052l-9.36 9.684v-9.684h-19.044q-2.916 0 -4.986 -2.142t-2.07 -5.202zm0 -4.932v-9.18h44.568v9.18h-44.568zm0 -11.808v-9.18h44.568v9.18h-44.568zm0 -11.808v-2.412q0 -3.024 2.07 -5.166t4.986 -2.142h30.456q2.916 0 4.986 2.142t2.07 5.166v2.412h-44.568z","ARROW_CIRCLE_O_RIGHT":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm8.064 0q0 5.328 2.628 9.828t7.128 7.128 9.828 2.628 9.828 -2.628 7.128 -7.128 2.628 -9.828 -2.628 -9.828 -7.128 -7.128 -9.828 -2.628 -9.828 2.628 -7.128 7.128 -2.628 9.828zm5.76 3.456v-6.912q0 -0.468 0.342 -0.81t0.81 -0.342h12.672v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324q0.432 0 0.864 0.36l11.484 11.484q0.324 0.324 0.324 0.828t-0.324 0.828l-11.52 11.52q-0.324 0.324 -0.828 0.324 -0.468 0 -0.81 -0.342t-0.342 -0.81v-6.912h-12.672q-0.468 0 -0.81 -0.342t-0.342 -0.81z","ARROW_CIRCLE_O_LEFT":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm8.064 0q0 5.328 2.628 9.828t7.128 7.128 9.828 2.628 9.828 -2.628 7.128 -7.128 2.628 -9.828 -2.628 -9.828 -7.128 -7.128 -9.828 -2.628 -9.828 2.628 -7.128 7.128 -2.628 9.828zm5.76 0q0 -0.504 0.324 -0.828l11.52 -11.52q0.324 -0.324 0.828 -0.324 0.468 0 0.81 0.342t0.342 0.81v6.912h12.672q0.468 0 0.81 0.342t0.342 0.81v6.912q0 0.468 -0.342 0.81t-0.81 0.342h-12.672v6.912q0 0.504 -0.324 0.828t-0.828 0.324q-0.432 0 -0.864 -0.36l-11.484 -11.484q-0.324 -0.324 -0.324 -0.828z","TOGGLE_LEFT":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 0q0 0.468 0.342 0.81t0.81 0.342h34.56q0.468 0 0.81 -0.342t0.342 -0.81v-34.56q0 -0.468 -0.342 -0.81t-0.81 -0.342h-34.56q-0.468 0 -0.81 0.342t-0.342 0.81v34.56zm6.912 -17.28q0 -1.188 0.972 -1.872l16.128 -11.52q0.612 -0.432 1.332 -0.432 0.936 0 1.62 0.684t0.684 1.62v23.04q0 0.936 -0.684 1.62t-1.62 0.684q-0.72 0 -1.332 -0.432l-16.128 -11.52q-0.972 -0.684 -0.972 -1.872z","DOT_CIRCLE_O":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm8.064 0q0 5.328 2.628 9.828t7.128 7.128 9.828 2.628 9.828 -2.628 7.128 -7.128 2.628 -9.828 -2.628 -9.828 -7.128 -7.128 -9.828 -2.628 -9.828 2.628 -7.128 7.128 -2.628 9.828zm10.368 0q0 -3.816 2.7 -6.516t6.516 -2.7 6.516 2.7 2.7 6.516 -2.7 6.516 -6.516 2.7 -6.516 -2.7 -2.7 -6.516z","WHEELCHAIR":"M0 -20.663q0 -6.516 3.762 -11.88t9.882 -7.596l0.612 4.716q-4.392 1.944 -7.02 5.958t-2.628 8.802q0 6.66 4.734 11.394t11.394 4.734q4.536 0 8.37 -2.34t5.94 -6.318 1.782 -8.514l3.672 7.344q-2.088 6.444 -7.56 10.44t-12.204 3.996q-5.616 0 -10.386 -2.79t-7.56 -7.56 -2.79 -10.386zm14.976 -37.008q-0.072 -0.576 0.216 -1.512 0.504 -1.836 2.052 -2.97t3.492 -1.134q2.376 0 4.068 1.692t1.692 4.068q0 2.484 -1.872 4.23t-4.32 1.494l1.332 10.404h15.228v4.608h-14.652l0.576 4.608h16.38q1.44 0 2.052 1.26l8.208 16.38 7.128 -3.564 2.088 4.104 -9.216 4.608q-0.468 0.252 -1.044 0.252 -1.44 0 -2.052 -1.26l-8.604 -17.172h-16.992q-0.864 0 -1.53 -0.594t-0.774 -1.458z","VIMEO_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm4.608 -23.256q0.576 0.288 0.918 0.936t0.774 0.72q0.756 0.108 1.962 -0.306t2.088 -0.378 1.494 1.08q0.396 0.648 0.666 1.386t0.54 1.728 0.45 1.458q0.612 1.656 1.908 6.732 1.296 5.256 2.052 7.092 1.512 3.564 3.708 4.5 1.548 0.432 3.06 0.054t2.736 -1.134q4.716 -2.772 9 -8.532 3.744 -5.004 6.21 -10.53t2.97 -8.154q0.576 -3.06 -0.756 -4.752 -1.872 -2.34 -6.732 -1.62 -0.612 0.108 -1.476 0.45t-2.07 1.098 -2.322 1.746 -2.142 2.52 -1.602 3.294q2.88 -0.252 4.086 0.576t0.954 3.564q-0.18 1.872 -1.872 5.148 -1.548 2.808 -2.556 3.564 -1.584 1.152 -3.132 -0.504 -0.828 -0.864 -1.35 -2.322t-0.684 -2.628 -0.36 -3.024 -0.306 -2.574q-0.828 -4.644 -1.224 -5.904 -0.432 -1.332 -1.278 -2.484t-1.818 -1.44q-2.052 -0.576 -4.572 0.9 -1.944 1.152 -4.914 3.816t-4.41 3.672v0.252z","TURKISH_LIRA":"M0 -31.031v-4.608q0 -0.828 0.828 -1.116l8.388 -2.556v-3.348l-7.74 2.376q-0.108 0.036 -0.324 0.036 -0.36 0 -0.684 -0.216 -0.468 -0.36 -0.468 -0.936v-4.608q0 -0.828 0.828 -1.116l8.388 -2.556v-9q0 -0.504 0.324 -0.828t0.828 -0.324h5.76q0.504 0 0.828 0.324t0.324 0.828v6.516l13.5 -4.176q0.54 -0.18 1.008 0.18t0.468 0.936v4.608q0 0.828 -0.828 1.116l-14.148 4.356v3.348l13.5 -4.176q0.54 -0.18 1.008 0.18t0.468 0.936v4.608q0 0.828 -0.828 1.116l-14.148 4.356v17.532q6.768 -0.468 11.448 -5.436t4.68 -11.808q0 -0.504 0.324 -0.828t0.828 -0.324h5.76q0.504 0 0.828 0.324t0.324 0.828q0 6.876 -3.402 12.708t-9.234 9.234 -12.708 3.402h-5.76q-0.504 0 -0.828 -0.324t-0.324 -0.828v-21.996l-7.74 2.376q-0.108 0.036 -0.324 0.036 -0.36 0 -0.684 -0.216 -0.468 -0.36 -0.468 -0.936z","PLUS_SQUARE_O":"M0 -19.511v-29.952q0 -4.284 3.042 -7.326t7.326 -3.042h29.952q4.284 0 7.326 3.042t3.042 7.326v29.952q0 4.284 -3.042 7.326t-7.326 3.042h-29.952q-4.284 0 -7.326 -3.042t-3.042 -7.326zm4.608 0q0 2.376 1.692 4.068t4.068 1.692h29.952q2.376 0 4.068 -1.692t1.692 -4.068v-29.952q0 -2.376 -1.692 -4.068t-4.068 -1.692h-29.952q-2.376 0 -4.068 1.692t-1.692 4.068v29.952zm4.608 -13.824v-2.304q0 -0.504 0.324 -0.828t0.828 -0.324h12.672v-12.672q0 -0.504 0.324 -0.828t0.828 -0.324h2.304q0.504 0 0.828 0.324t0.324 0.828v12.672h12.672q0.504 0 0.828 0.324t0.324 0.828v2.304q0 0.504 -0.324 0.828t-0.828 0.324h-12.672v12.672q0 0.504 -0.324 0.828t-0.828 0.324h-2.304q-0.504 0 -0.828 -0.324t-0.324 -0.828v-12.672h-12.672q-0.504 0 -0.828 -0.324t-0.324 -0.828z","SPACE_SHUTTLE":"M0 -29.879q0 -0.432 1.386 -0.738t3.474 -0.378q-0.252 -0.9 -0.252 -1.764 0 -1.188 0.342 -2.034t0.81 -0.846h2.304v-2.304h4.608q5.688 0 9.648 2.304h40.068q1.512 0.252 3.834 0.648t2.898 0.504q3.204 0.54 5.4 1.458t3.006 1.71 0.81 1.44 -0.81 1.44 -3.006 1.71 -5.4 1.458q-0.576 0.108 -2.898 0.504t-3.834 0.648h-40.068q-3.96 2.304 -9.648 2.304h-4.608v-2.304h-2.304q-0.468 0 -0.81 -0.846t-0.342 -2.034q0 -0.864 0.252 -1.764 -2.088 -0.072 -3.474 -0.378t-1.386 -0.738zm11.628 8.64h1.044q5.652 0 9.828 -2.304h36.54q-7.812 1.368 -16.416 2.88 -2.052 0 -4.068 0.864t-2.988 1.728l-1.008 0.864 -10.368 10.368q-0.936 0.936 -2.538 1.62t-3.222 0.684h-3.456zm0 -17.28l3.348 -16.704h3.456q1.656 0 3.24 0.684t2.52 1.62l10.368 10.368q0.144 0.144 0.396 0.378t1.098 0.828 1.746 1.044 2.214 0.828 2.61 0.378l16.416 2.88h-36.54q-4.176 -2.304 -9.828 -2.304h-1.044zm50.976 11.952l2.916 1.08q2.448 -1.728 2.448 -4.392t-2.448 -4.392l-2.916 1.08q1.908 1.296 1.908 3.312t-1.908 3.312z","SLACK":"M0 -37.799q0 -1.692 0.99 -3.06t2.574 -1.908l5.652 -1.908 -1.908 -5.724q-0.288 -0.864 -0.288 -1.692 0 -2.16 1.512 -3.69t3.672 -1.53q1.692 0 3.06 0.972t1.908 2.592l1.944 5.76 11.16 -3.78 -1.944 -5.76q-0.288 -0.864 -0.288 -1.692 0 -2.124 1.53 -3.672t3.654 -1.548q1.692 0 3.078 0.99t1.926 2.574l1.908 5.796 5.832 -1.98q0.756 -0.216 1.548 -0.216 2.16 0 3.69 1.422t1.53 3.546q0 1.62 -1.08 2.934t-2.664 1.854l-5.652 1.944 3.78 11.376 5.904 -2.016q0.864 -0.288 1.656 -0.288 2.232 0 3.726 1.458t1.494 3.654q0 3.492 -3.348 4.68l-6.192 2.124 2.016 6.012q0.252 0.756 0.252 1.692 0 2.124 -1.512 3.672t-3.636 1.548q-1.692 0 -3.078 -0.972t-1.926 -2.592l-1.98 -5.94 -11.16 3.816 1.98 5.904q0.288 0.864 0.288 1.692 0 2.124 -1.512 3.672t-3.672 1.548q-1.692 0 -3.06 -0.972t-1.908 -2.592l-1.98 -5.868 -5.508 1.908q-1.044 0.324 -1.8 0.324 -2.196 0 -3.654 -1.44t-1.458 -3.636q0 -1.692 0.99 -3.06t2.574 -1.908l5.616 -1.908 -3.78 -11.268 -5.616 1.944q-0.936 0.288 -1.728 0.288 -2.16 0 -3.636 -1.458t-1.476 -3.618zm22.32 -0.54l3.78 11.268 11.16 -3.78 -3.78 -11.34z","ENVELOPE_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 -6.912q0 1.44 1.008 2.448t2.448 1.008h29.952q1.44 0 2.448 -1.008t1.008 -2.448v-15.696q-1.116 1.26 -2.304 1.98 -1.224 0.792 -4.77 3.06t-5.454 3.564q-3.528 2.484 -5.904 2.484t-5.904 -2.484q-1.656 -1.152 -5.094 -3.33t-5.13 -3.33q-0.432 -0.288 -1.188 -0.972t-1.116 -0.972v15.696zm0 -20.736q0 1.332 1.098 2.754t2.43 2.322q1.692 1.152 4.95 3.204t4.662 2.988l0.612 0.414 0.756 0.504 0.756 0.468 0.846 0.468 0.774 0.342 0.81 0.27 0.738 0.09 0.738 -0.09 0.81 -0.27 0.774 -0.342 0.846 -0.468 0.756 -0.468 0.756 -0.504 0.612 -0.414 9.612 -6.264q1.26 -0.828 2.394 -2.25t1.134 -2.646q0 -1.476 -0.99 -2.52t-2.466 -1.044h-29.952q-1.44 0 -2.448 1.008t-1.008 2.448z","WORDPRESS":"M0 -32.183q0 -6.552 2.556 -12.528t6.876 -10.296 10.296 -6.876 12.528 -2.556 12.528 2.556 10.296 6.876 6.876 10.296 2.556 12.528 -2.556 12.528 -6.876 10.296 -10.296 6.876 -12.528 2.556 -12.528 -2.556 -10.296 -6.876 -6.876 -10.296 -2.556 -12.528zm1.476 0q0 6.228 2.448 11.934t6.57 9.828 9.828 6.57 11.934 2.448 11.934 -2.448 9.828 -6.57 6.57 -9.828 2.448 -11.934 -2.448 -11.934 -6.57 -9.828 -9.828 -6.57 -11.934 -2.448 -11.934 2.448 -9.828 6.57 -6.57 9.828 -2.448 11.934zm3.096 0q0 -5.868 2.412 -11.268l13.212 36.18q-7.056 -3.42 -11.34 -10.116t-4.284 -14.796zm4.572 -15.192q3.78 -5.76 9.882 -9.126t13.23 -3.366q5.292 0 10.098 1.908t8.586 5.364h-0.36q-1.98 0 -3.312 1.458t-1.332 3.438q0 0.432 0.072 0.864t0.144 0.774 0.288 0.828 0.324 0.756 0.432 0.81 0.45 0.756 0.522 0.864 0.504 0.828q2.268 3.852 2.268 7.632 0 0.684 -0.09 1.386t-0.36 1.782 -0.414 1.584 -0.63 2.124 -0.63 2.088l-2.736 9.216 -10.008 -29.736q1.656 -0.108 3.168 -0.288 0.684 -0.072 0.936 -0.666t-0.09 -1.116 -1.026 -0.486l-7.38 0.36q-2.7 -0.036 -7.272 -0.36 -0.432 -0.036 -0.738 0.18t-0.414 0.54 -0.054 0.666 0.324 0.594 0.702 0.288l2.88 0.288 4.32 11.808 -6.048 18.144 -10.08 -29.952q1.656 -0.108 3.168 -0.288 0.684 -0.072 0.936 -0.666t-0.09 -1.116 -1.026 -0.486l-7.38 0.36 -0.828 -0.018 -0.936 -0.018zm15.3 41.724l8.28 -24.12 8.532 23.292q0.036 0.216 0.18 0.396 -4.536 1.584 -9.18 1.584 -4.032 0 -7.812 -1.152zm21.708 -2.628l8.46 -24.408q2.124 -6.084 2.124 -9.936 0 -1.512 -0.216 -2.844 3.42 6.264 3.42 13.284 0 7.524 -3.744 13.878t-10.044 10.026z","OPENID":"M0 -25.523q0 -5.04 3.618 -9.486t9.9 -7.398 14.094 -3.888v6.192q-7.812 1.368 -12.834 5.4t-5.022 9.18q0 5.472 5.562 9.612t13.986 5.22v-48.96l9.792 -4.788v55.296l-9.792 4.608q-8.208 -0.72 -14.904 -3.672t-10.548 -7.506 -3.852 -9.81zm40.824 -14.58v-6.192q9.972 1.188 17.316 5.652l5.04 -2.844 1.332 14.04 -18.9 -4.104 5.292 -2.988q-4.284 -2.52 -10.08 -3.564z","UNIVERSITY":"M0 -4.535q0 -0.936 0.738 -1.62t1.746 -0.684h64.152q1.008 0 1.746 0.684t0.738 1.62v4.608h-69.12v-4.608zm0 -41.472v-4.608l34.56 -13.824 34.56 13.824v4.608h-4.608q0 0.936 -0.738 1.62t-1.746 0.684h-54.936q-1.008 0 -1.746 -0.684t-0.738 -1.62h-4.608zm4.608 36.864v-2.304q0 -0.936 0.738 -1.62t1.746 -0.684h2.124v-27.648h9.216v27.648h4.608v-27.648h9.216v27.648h4.608v-27.648h9.216v27.648h4.608v-27.648h9.216v27.648h2.124q1.008 0 1.746 0.684t0.738 1.62v2.304h-59.904z","MORTAR_BOARD":"M0 -46.007q0 -0.828 0.792 -1.116l40.32 -12.672q0.144 -0.036 0.36 -0.036t0.36 0.036l40.32 12.672q0.792 0.288 0.792 1.116t-0.792 1.116l-40.32 12.672q-0.144 0.036 -0.36 0.036t-0.36 -0.036l-23.472 -7.416q-1.548 1.224 -2.556 4.014t-1.224 6.426q2.268 1.296 2.268 3.924 0 2.484 -2.088 3.852l2.088 15.588q0.072 0.504 -0.288 0.9 -0.324 0.396 -0.864 0.396h-6.912q-0.54 0 -0.864 -0.396 -0.36 -0.396 -0.288 -0.9l2.088 -15.588q-2.088 -1.368 -2.088 -3.852 0 -2.628 2.34 -3.996 0.396 -7.452 3.528 -11.88l-11.988 -3.744q-0.792 -0.288 -0.792 -1.116zm18.432 23.04l0.648 -11.376 20.664 6.516q0.792 0.252 1.728 0.252t1.728 -0.252l20.664 -6.516 0.648 11.376q0.144 2.484 -2.952 4.608t-8.46 3.366 -11.628 1.242 -11.628 -1.242 -8.46 -3.366 -2.952 -4.608z","YAHOO":"M3.924 -64.439q2.088 0.54 3.888 0.54 1.548 0 3.996 -0.54 2.268 3.996 4.806 8.262t6.012 9.954 4.986 8.172q1.332 -2.196 3.942 -6.39t4.23 -6.84 3.78 -6.336 3.852 -6.822q1.944 0.504 3.852 0.504 2.016 0 4.104 -0.504 -1.008 1.404 -2.16 3.186t-1.782 2.826 -2.034 3.456 -1.764 3.024q-5.256 8.928 -12.708 21.96l0.468 25.452q-2.232 -0.396 -3.78 -0.396 -1.476 0 -3.78 0.396l0.468 -25.452q-1.44 -2.484 -6.066 -10.638t-7.794 -13.482 -6.516 -10.332z","GOOGLE":"M3.996 -15.695q0 -2.916 1.602 -5.4t4.266 -4.14q4.716 -2.952 14.544 -3.6 -1.152 -1.476 -1.71 -2.646t-0.558 -2.646q0 -1.44 0.756 -3.06 -1.656 0.144 -2.448 0.144 -5.328 0 -8.982 -3.474t-3.654 -8.802q0 -2.952 1.296 -5.724t3.564 -4.716q2.736 -2.376 6.552 -3.528t7.848 -1.152h15.012l-4.932 3.168h-4.752q2.7 2.268 4.068 4.788t1.368 5.76q0 2.592 -0.882 4.662t-2.142 3.348 -2.502 2.34 -2.124 2.214 -0.882 2.376q0 1.296 1.152 2.538t2.772 2.448 3.258 2.646 2.79 3.744 1.152 5.112q0 3.276 -1.764 6.228 -2.556 4.392 -7.542 6.462t-10.746 2.07q-4.752 0 -8.874 -1.494t-6.21 -4.95q-1.296 -2.124 -1.296 -4.716zm6.696 -1.656q0 2.016 0.846 3.672t2.196 2.718 3.132 1.8 3.6 1.044 3.654 0.306q2.088 0 4.014 -0.468t3.564 -1.404 2.628 -2.628 0.99 -3.924q0 -0.9 -0.252 -1.764t-0.522 -1.512 -0.972 -1.494 -1.062 -1.26 -1.386 -1.242 -1.314 -1.044 -1.494 -1.08 -1.314 -0.936q-0.576 -0.072 -1.764 -0.072 -1.908 0 -3.762 0.252t-3.852 0.9 -3.492 1.656 -2.466 2.682 -0.972 3.798zm3.816 -35.784q0 1.656 0.36 3.51t1.134 3.708 1.872 3.33 2.7 2.412 3.474 0.936q1.332 0 2.79 -0.594t2.358 -1.566q1.908 -2.016 1.908 -5.724 0 -2.124 -0.612 -4.518t-1.728 -4.644 -3.024 -3.726 -4.212 -1.476q-1.512 0 -2.97 0.702t-2.394 1.89q-1.656 2.124 -1.656 5.76z","REDDIT":"M0 -35.135q0 -3.384 2.376 -5.76t5.76 -2.376q2.988 0 5.328 1.98 8.928 -5.688 21.312 -5.904l4.824 -15.228q0.144 -0.504 0.63 -0.774t1.026 -0.162l12.492 2.952q0.792 -1.8 2.466 -2.916t3.69 -1.116q2.772 0 4.734 1.962t1.962 4.734 -1.962 4.752 -4.734 1.98q-2.736 0 -4.698 -1.944t-1.998 -4.716l-11.34 -2.664 -4.176 13.176q11.772 0.504 20.16 5.976 2.304 -2.088 5.436 -2.088 3.384 0 5.76 2.376t2.376 5.76q0 2.232 -1.116 4.104t-2.988 2.952q0.18 1.188 0.18 2.196 0 4.356 -2.466 8.298t-7.11 6.966q-4.5 2.952 -10.278 4.518t-12.078 1.566q-6.336 0 -12.114 -1.566t-10.242 -4.518q-4.644 -3.024 -7.11 -6.948t-2.466 -8.316q0 -1.044 0.18 -2.376 -1.728 -1.116 -2.772 -2.934t-1.044 -3.942zm2.772 0q0 2.412 1.836 3.996 1.764 -4.716 6.48 -8.46 -1.296 -0.9 -2.952 -0.9 -2.232 0 -3.798 1.566t-1.566 3.798zm3.636 9.252q0 3.636 2.142 6.984t6.174 5.976q4.176 2.7 9.558 4.158t11.286 1.458 11.286 -1.458 9.558 -4.158q4.032 -2.628 6.174 -5.976t2.142 -6.984 -2.142 -6.966 -6.174 -5.958q-4.176 -2.7 -9.558 -4.158t-11.286 -1.458 -11.286 1.458 -9.558 4.158q-4.032 2.628 -6.174 5.958t-2.142 6.966zm13.572 -3.852q0 -2.052 1.494 -3.528t3.51 -1.476 3.474 1.476 1.458 3.528q0 2.016 -1.458 3.456t-3.474 1.44q-2.052 0 -3.528 -1.44t-1.476 -3.456zm3.816 13.05q0 -0.594 0.396 -0.99t0.972 -0.396 0.972 0.396q2.772 2.772 9.54 2.772h0.072q6.768 0 9.54 -2.772 0.396 -0.396 0.972 -0.396t0.972 0.396 0.396 0.99 -0.396 0.99q-3.564 3.564 -11.484 3.564h-0.072q-7.92 0 -11.484 -3.564 -0.396 -0.396 -0.396 -0.99zm17.712 -13.05q0 -2.052 1.494 -3.528t3.51 -1.476 3.474 1.476 1.458 3.528q0 2.016 -1.458 3.456t-3.474 1.44q-2.052 0 -3.528 -1.44t-1.476 -3.456zm14.472 -28.008q0 1.62 1.152 2.772t2.772 1.152 2.772 -1.152 1.152 -2.772 -1.152 -2.772 -2.772 -1.152 -2.772 1.152 -1.152 2.772zm4.212 18.252q4.716 3.78 6.408 8.568 2.052 -1.656 2.052 -4.212 0 -2.232 -1.566 -3.798t-3.798 -1.566q-1.764 0 -3.096 1.008z","REDDIT_SQUARE":"M0 -16.091v-32.184q0 -4.788 3.384 -8.172t8.136 -3.384h32.256q4.752 0 8.136 3.384t3.384 8.172v32.184q0 4.788 -3.384 8.172t-8.136 3.384h-32.256q-4.752 0 -8.136 -3.384t-3.384 -8.172zm5.58 -18.576q0 1.332 0.702 2.43t1.89 1.638q-0.252 0.9 -0.252 1.944 0 3.528 2.664 6.534t7.254 4.752 10.026 1.746q5.4 0 9.99 -1.746t7.254 -4.752 2.664 -6.534q0 -0.972 -0.216 -1.944 1.26 -0.504 2.052 -1.638t0.792 -2.538q0 -1.836 -1.296 -3.15t-3.132 -1.314q-2.16 0 -3.528 1.728 -5.436 -3.852 -13.5 -4.14l2.988 -9.54 7.416 1.764q0.036 1.8 1.314 3.06t3.042 1.26q1.8 0 3.096 -1.278t1.296 -3.078 -1.296 -3.096 -3.096 -1.296q-1.296 0 -2.376 0.738t-1.62 1.926l-8.172 -1.944q-0.324 -0.072 -0.63 0.09t-0.414 0.522l-3.42 10.872q-8.064 0.144 -13.716 4.068 -1.296 -1.548 -3.348 -1.548 -1.836 0 -3.132 1.314t-1.296 3.15zm12.168 3.456q0 -1.332 0.936 -2.268t2.268 -0.936 2.268 0.936 0.936 2.268 -0.936 2.304 -2.268 0.972 -2.268 -0.972 -0.936 -2.304zm2.412 8.568q0 -0.396 0.288 -0.648 0.252 -0.252 0.63 -0.252t0.63 0.252q1.764 1.836 6.192 1.836h0.072q4.392 0 6.228 -1.836 0.252 -0.252 0.63 -0.252t0.63 0.252 0.252 0.648 -0.252 0.648q-2.34 2.304 -7.488 2.304h-0.072q-5.148 0 -7.452 -2.304 -0.288 -0.252 -0.288 -0.648zm11.592 -8.568q0 -1.332 0.936 -2.268t2.268 -0.936 2.268 0.936 0.936 2.268 -0.936 2.304 -2.268 0.972 -2.268 -0.972 -0.936 -2.304zm9.396 -18.252q0 -1.08 0.756 -1.836t1.8 -0.756q1.08 0 1.836 0.756t0.756 1.836q0 1.044 -0.756 1.8t-1.836 0.756q-1.044 0 -1.8 -0.756t-0.756 -1.8z","STUMBLEUPON_CIRCLE":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm11.52 4.968q0 2.952 2.07 5.004t5.022 2.052q2.916 0 4.986 -2.034t2.07 -4.914v-10.08q0 -0.684 0.486 -1.188t1.206 -0.504q0.684 0 1.17 0.504t0.486 1.188v1.944l2.16 1.008 3.24 -0.972v-2.232q0 -2.844 -2.088 -4.86t-4.968 -2.016 -4.968 1.998 -2.088 4.842v10.188q0 0.72 -0.504 1.206t-1.188 0.486 -1.17 -0.486 -0.486 -1.206v-4.32h-5.436v4.392zm17.496 0.072q0 2.88 2.088 4.932t5.004 2.052 4.986 -2.052 2.07 -5.004v-4.392h-5.4v4.536q0 0.72 -0.486 1.206t-1.206 0.486q-0.684 0 -1.17 -0.504t-0.486 -1.188v-4.428l-3.24 0.936 -2.16 -1.008v4.428z","STUMBLEUPON":"M0 -21.239v-9.576h11.808v9.432q0 1.548 1.08 2.61t2.592 1.062 2.592 -1.062 1.08 -2.61v-22.32q0 -6.156 4.554 -10.512t10.854 -4.356q6.336 0 10.872 4.392t4.536 10.584v4.896l-7.02 2.088 -4.716 -2.196v-4.248q0 -1.512 -1.08 -2.592t-2.592 -1.08 -2.592 1.08 -1.08 2.592v22.032q0 6.3 -4.536 10.764t-10.908 4.464q-6.408 0 -10.926 -4.518t-4.518 -10.926zm38.232 0.144v-9.648l4.716 2.196 7.02 -2.088v9.72q0 1.512 1.08 2.574t2.592 1.062 2.592 -1.062 1.08 -2.574v-9.9h11.808v9.576q0 6.408 -4.518 10.926t-10.926 4.518q-6.372 0 -10.908 -4.482t-4.536 -10.818z","DELICIOUS":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm2.304 -17.28h25.344v25.344h17.28q3.348 0 5.706 -2.358t2.358 -5.706v-17.28h-25.344v-25.344h-17.28q-3.348 0 -5.706 2.358t-2.358 5.706v17.28z","DIGG":"M0 -18.899v-25.092h11.808v-10.296h7.344v35.388h-19.152zm7.38 -5.904h4.428v-13.284h-4.428v13.284zm14.724 5.904h7.38v-25.092h-7.38v25.092zm0 -28.044h7.38v-7.344h-7.38v7.344zm10.332 36.864v-5.868h11.808v-2.952h-11.808v-25.092h19.188v33.912h-19.188zm7.38 -14.724h4.428v-13.284h-4.428v13.284zm14.76 14.724v-5.868h11.772v-2.952h-11.772v-25.092h19.152v33.912h-19.152zm7.344 -14.724h4.428v-13.284h-4.428v13.284z","PIED_PIPER_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm10.548 -8.208l7.596 -1.476v-7.416q1.98 0.684 4.176 0.684 4.5 0 7.686 -3.42t3.186 -8.244 -3.186 -8.244 -7.686 -3.42q-2.664 0 -5.076 1.296h-6.696v30.24zm7.596 -14.976v-9.972q1.008 -0.612 2.52 -0.612 1.908 0 3.276 1.62t1.368 3.924 -1.368 3.942 -3.276 1.638q-1.548 0 -2.52 -0.54zm4.752 30.348l7.596 -1.476v-7.416q1.836 0.684 4.212 0.684 4.5 0 7.668 -3.42t3.168 -8.244 -3.168 -8.244 -7.668 -3.42q-0.72 0 -1.404 0.108 -0.828 2.808 -2.808 4.896 -3.132 3.42 -7.596 3.636v22.896zm7.596 -14.976v-9.972q1.008 -0.612 2.52 -0.612 1.908 0 3.276 1.638t1.368 3.942 -1.368 3.924 -3.276 1.62q-1.548 0 -2.52 -0.54z","PIED_PIPER_ALT":"M1.476 -25.523q0 -0.54 0.306 -0.954t0.81 -0.522l17.496 -3.816q-0.288 -0.504 -0.288 -0.9t0.198 -0.63 0.576 -0.414 0.72 -0.252 0.828 -0.162 0.666 -0.162q0.144 -0.036 0.558 -0.27t0.63 -0.234q0.54 0 1.008 0.576t0.72 1.188q5.868 -1.332 6.192 -1.332 0.612 0 1.062 0.396t0.45 1.008q0 0.54 -0.306 0.936t-0.846 0.504l-6.552 1.44 -0.036 0.576q-0.036 0.936 2.934 4.23t3.762 3.294q1.692 0 4.284 -2.88t2.592 -4.644q0 -1.296 -0.846 -1.908t-1.836 -0.666 -1.836 -0.414 -0.846 -1.224q0 -0.576 0.36 -1.224l-2.448 -0.684q1.548 -1.584 1.548 -4.212 0 -0.936 -0.18 -2.088 2.952 -0.576 5.184 -0.576 1.584 0 2.574 0.054t1.746 0.306 1.116 0.486 0.738 0.882 0.558 1.206 0.612 1.71 0.864 2.16l1.8 -0.9q-0.108 1.44 -0.828 2.16t-1.53 0.756 -1.44 0.234 -0.594 0.738l0.036 0.756q2.7 -0.108 5.166 0.738t4.248 2.106 3.636 3.402 3.024 3.888 2.718 4.338q1.188 2.016 2.826 3.924t2.718 2.898 3.564 3.186q-1.728 1.08 -3.906 2.07t-4.986 2.124 -4.104 1.71q-1.584 -1.332 -2.664 -4.14t-1.566 -5.922 -1.188 -6.498 -1.53 -6.066 -2.61 -4.428 -4.41 -1.746l-0.36 0.072 -0.216 0.144q0.144 0.18 0.468 0.504 0.216 0.18 1.008 0.846t0.918 0.792 0.684 0.648 0.648 0.738 0.414 0.756 0.378 0.99 0.162 1.116 0.144 1.458l0.036 1.188q0.036 0.936 -0.09 2.07t-0.27 1.872 -0.45 2.106 -0.414 1.908q-1.26 -0.036 -3.636 0.342t-3.528 0.378q-1.404 0 -2.592 -0.36 -0.072 -0.576 -0.072 -1.692 0 -2.664 0.108 -3.456 0.072 -0.468 1.134 -1.494t2.052 -2.124 0.954 -1.854q-0.864 -0.072 -1.548 0.864 -1.296 1.908 -4.014 3.582t-4.914 1.674q-0.9 0 -2.718 -2.268t-3.834 -5.022 -3.024 -3.474q-0.216 -0.144 -0.972 -1.08 -17.352 4.032 -18.468 4.032 -0.576 0 -1.008 -0.396t-0.432 -0.972zm26.028 -7.956q0.36 -0.036 1.17 -0.252t1.242 -0.216q0.684 0 1.26 0.36l-3.456 0.72zm2.088 3.888l1.728 -0.432 3.924 6.372 -2.628 1.728zm1.332 -11.376q0.576 -1.08 1.296 -1.674t1.944 -1.062 2.358 -1.296 1.656 -1.314 1.8 -1.98 1.566 -1.818q0.432 0.324 1.008 1.134t1.152 1.314 1.368 0.468l0.432 -0.036v2.736l0.792 0.036q8.892 -3.42 13.356 -6.84 1.008 -0.756 1.8 -1.404t1.53 -1.35 1.188 -1.116 1.062 -1.224 0.864 -1.116 0.882 -1.332 0.828 -1.368 0.972 -1.71 1.062 -1.908l0.252 -0.324q-0.072 1.908 -1.548 5.004 -2.844 5.94 -7.38 9.504t-11.016 5.112q-0.504 0.108 -1.512 0.27t-1.8 0.342 -1.404 0.504q0.108 0.684 0.882 1.656t0.774 1.224q0 0.396 -0.936 1.08 -0.18 -0.18 -0.486 -0.558t-0.432 -0.522 -0.378 -0.414 -0.36 -0.378l-0.288 -0.288 -0.306 -0.27 -0.288 -0.18 -0.306 -0.162q-0.252 -0.108 -0.522 -0.18t-0.738 -0.09 -0.792 -0.018h-2.52q-4.536 0 -7.812 1.548zm7.272 30.204h1.116l0.36 2.988 -1.476 0.432v-3.42zm0 4.464q1.404 -0.936 4.734 -1.71t5.274 -0.774q0.324 0 0.81 0.558t1.008 1.53 0.936 1.8 0.864 1.836 0.522 1.188q-4.356 1.62 -8.784 1.62 -2.196 0 -4.5 -0.396zm1.98 -3.888q0.756 -0.072 2.178 -0.306t2.592 -0.36 2.178 -0.126h0.504l0.108 0.576q0 0.252 -0.63 0.522t-1.656 0.468 -1.944 0.342 -1.926 0.27 -1.152 0.162zm29.916 -53.964l0.036 -0.108 0.072 -0.144 -0.036 0.18zm0.108 -0.252v-0.036 0.036zm0 0l0.036 -0.036z","DRUPAL":"M0 -27.863q0 -3.204 0.702 -6.21t1.764 -5.238 2.538 -4.266 2.826 -3.384 2.826 -2.502 2.322 -1.674 1.53 -0.882q0.504 -0.288 1.836 -0.954t1.962 -1.026 1.728 -1.08 2.178 -1.584q1.296 -1.008 2.088 -2.61t1.08 -4.518q4.644 5.58 6.696 6.948 1.584 1.044 4.68 2.448t4.644 2.376q0.756 0.468 1.404 0.9t2.178 1.674 2.736 2.538 2.7 3.42 2.484 4.392 1.692 5.346 0.702 6.39q0 5.904 -2.232 10.962t-5.976 8.496 -8.73 5.382 -10.458 1.944 -10.548 -2.07 -8.91 -5.652 -6.138 -8.694 -2.304 -10.872zm11.988 9.504q-0.072 4.032 2.664 5.904 1.044 0.72 2.25 1.026t3.726 0.306q2.052 0 4.752 -1.17t4.824 -2.556 4.32 -2.538 3.348 -1.116q0.936 0.036 2.34 1.134t2.574 2.412 2.448 2.43 1.998 1.152q1.26 0.108 2.106 -0.504t1.998 -2.268q1.008 -1.476 1.53 -3.636t0.522 -3.816q0 -0.792 -0.18 -1.602t-0.594 -1.62 -1.224 -1.314 -1.89 -0.504q-1.188 0 -3.492 1.494t-4.644 3.006 -3.636 1.512q-0.972 0.036 -2.286 -0.684t-2.736 -1.764 -3.006 -2.088 -3.6 -1.764 -3.996 -0.684q-4.14 0.036 -7.092 2.826t-3.024 6.426zm12.69 11.952q-0.018 0.36 0.27 0.72 1.224 1.152 3.15 1.656t3.69 0.45 3.564 -0.162q1.476 -0.144 3.042 -0.738t2.34 -1.08 1.026 -0.738q0.432 -0.432 0.252 -1.044 -0.18 -0.684 -0.864 -0.18 -1.08 0.792 -3.132 1.404t-4.716 0.612q-4.644 0 -6.948 -1.764 -0.18 -0.144 -0.468 -0.144 -0.396 0 -0.936 0.432 -0.252 0.216 -0.27 0.576zm5.994 -3.852q0.324 0.288 0.63 0.162t1.134 -0.846q0.108 -0.072 0.378 -0.306t0.378 -0.306 0.36 -0.252 0.414 -0.252 0.45 -0.18 0.54 -0.162 0.594 -0.09 0.738 -0.036q0.972 0 1.602 0.27t0.828 0.522 0.486 0.792q0.36 0.612 0.45 0.72t0.45 -0.036q0.828 -0.432 0.504 -1.224 -0.684 -1.692 -1.404 -2.196 -0.828 -0.54 -2.736 -0.54 -1.692 0 -2.556 0.36 -1.044 0.432 -2.808 2.016 -0.936 0.864 -0.432 1.584z","JOOMLA":"M0 -11.951q0 -2.592 1.602 -4.608t4.086 -2.592q-0.792 -3.096 0.036 -6.228t3.168 -5.472l0.432 -0.432 5.436 5.472 -0.396 0.396q-1.332 1.332 -1.332 3.204t1.332 3.24q1.332 1.332 3.204 1.332t3.204 -1.332l1.08 -1.08 5.436 -5.472 5.796 -5.76 5.436 5.472 -5.76 5.76 -5.436 5.472 -1.08 1.08q-2.34 2.304 -5.454 3.132t-6.174 0.072q-0.576 2.52 -2.592 4.14t-4.644 1.62q-3.06 0 -5.22 -2.178t-2.16 -5.238zm0.072 -40.464q0 -3.06 2.16 -5.238t5.22 -2.178q2.736 0 4.806 1.764t2.502 4.428q3.024 -0.72 6.102 0.126t5.382 3.15l0.432 0.432 -5.472 5.472 -0.432 -0.432q-1.332 -1.332 -3.204 -1.332t-3.204 1.332 -1.332 3.222 1.332 3.222l1.044 1.044 5.472 5.472 5.76 5.76 -5.436 5.472 -5.796 -5.76 -5.436 -5.472 -1.08 -1.08q-2.448 -2.412 -3.24 -5.742t0.18 -6.462q-2.52 -0.54 -4.14 -2.556t-1.62 -4.644zm15.984 14.364l5.796 -5.76 5.472 -5.472 1.044 -1.08q2.412 -2.412 5.724 -3.222t6.408 0.126q0.396 -2.7 2.466 -4.536t4.878 -1.836q3.06 0 5.22 2.178t2.16 5.238q0 2.772 -1.836 4.86t-4.572 2.484q0.936 3.06 0.108 6.354t-3.24 5.706l-0.432 0.432 -5.436 -5.472 0.432 -0.432q1.332 -1.332 1.332 -3.204t-1.332 -3.204 -3.204 -1.332 -3.204 1.332l-1.08 1.08 -5.472 5.472 -5.76 5.76zm11.88 0.36l5.472 -5.472 5.76 5.76 5.472 5.472 1.044 1.08q2.304 2.304 3.15 5.418t0.09 6.174q2.736 0.396 4.554 2.466t1.818 4.842q0 3.06 -2.16 5.238t-5.22 2.178q-2.664 0 -4.716 -1.692t-2.556 -4.248q-3.096 1.008 -6.462 0.216t-5.814 -3.24l-0.396 -0.432 5.436 -5.472 0.432 0.432q1.332 1.332 3.204 1.332t3.204 -1.332 1.332 -3.204 -1.332 -3.204l-1.08 -1.08 -5.472 -5.472z","LANGUAGE":"M0 -8.567v-38.808q0.108 -0.324 0.144 -0.36 0.18 -0.216 0.72 -0.396 3.816 -1.26 5.364 -1.8v-13.824l20.088 7.128q0.072 0 5.778 -1.98t11.376 -3.906 5.814 -1.926q0.72 0 0.72 0.756v15.048l5.292 1.692v38.844l-27.864 -8.856q-0.504 0.216 -13.5 4.59t-13.248 4.374q-0.468 0 -0.648 -0.468l-0.036 -0.108zm1.404 -1.116l24.984 -8.352v-37.152l-24.984 8.388v37.116zm3.888 -10.008q0.216 -0.144 2.952 -3.312 0.756 -0.864 3.078 -4.14t2.826 -4.248q0.612 -1.08 1.836 -3.546t1.296 -2.79q-0.288 -0.036 -3.96 1.188 -0.288 0.072 -0.99 0.27t-1.242 0.342 -0.612 0.18q-0.072 0.072 -0.072 0.378t-0.036 0.342q-0.18 0.36 -1.116 0.54 -0.828 0.252 -1.692 0 -0.648 -0.144 -1.008 -0.756 -0.144 -0.216 -0.18 -0.828 0.216 -0.072 0.882 -0.18t1.062 -0.216q2.088 -0.576 3.78 -1.152 3.6 -1.26 3.672 -1.26 0.36 -0.072 1.548 -0.702t1.584 -0.774q0.324 -0.108 0.774 -0.288t0.522 -0.198 0.216 0.018q0.072 0.432 -0.036 1.188 0 0.072 -0.45 0.972t-0.954 1.926 -0.612 1.206q-0.9 1.8 -2.772 4.716l2.304 1.008q0.432 0.216 2.682 1.152t2.43 1.008q0.144 0.036 0.378 0.918t0.162 1.098q-0.036 0.108 -0.45 -0.018t-1.134 -0.414l-0.72 -0.324q-1.584 -0.72 -3.132 -1.764 -0.252 -0.18 -1.476 -1.134t-1.368 -1.026q-2.412 3.708 -4.824 6.516 -2.916 3.42 -3.78 3.96 -0.144 0.072 -0.702 0.144t-0.666 0zm4.356 -23.04l0.036 -0.108q0.108 0.108 0.702 0.18t0.954 0 2.088 -0.576q1.296 -0.432 1.98 -0.504 0.612 0 0.756 0.612 0.108 0.54 -0.144 1.008 -0.432 0.828 -1.8 1.368 -1.08 0.432 -2.16 0.432 -0.936 -0.108 -1.764 -0.936 -0.504 -0.54 -0.648 -1.476zm1.512 37.764q0 -0.288 0.18 -0.486t0.468 -0.198q0.144 0 0.648 0.27t1.098 0.594 0.738 0.396q2.628 1.332 5.742 2.214t5.67 0.882q3.42 0 6.012 -0.522t5.652 -1.818q0.54 -0.252 1.098 -0.558t1.224 -0.684 1.026 -0.594l-1.548 -2.628 5.688 0.468 -1.944 5.76 -1.44 -2.376q-4.68 2.988 -9.936 3.888 -2.088 0.432 -3.276 0.432h-3.024q-2.844 0 -7.182 -1.404t-6.606 -3.06q-0.288 -0.252 -0.288 -0.576zm16.812 -50.76l20.628 6.624v-13.68zm3.888 30.276l3.672 1.116 1.62 -3.96 7.596 2.34 1.332 4.86 3.672 1.116 -6.516 -23.652 -3.6 -1.116zm6.696 -6.372l2.736 -6.66 2.268 8.172z","FAX":"M0 -5.687v-39.168q0 -2.376 1.692 -4.068t4.068 -1.692h4.608q2.376 0 4.068 1.692t1.692 4.068v39.168q0 2.376 -1.692 4.068t-4.068 1.692h-4.608q-2.376 0 -4.068 -1.692t-1.692 -4.068zm18.432 0v-55.296q0 -1.44 1.008 -2.448t2.448 -1.008h24.192q1.44 0 3.168 0.72t2.736 1.728l5.472 5.472q1.008 1.008 1.728 2.736t0.72 3.168v5.868q2.088 1.224 3.348 3.348t1.26 4.608v27.648q0 3.816 -2.7 6.516t-6.516 2.7h-31.104q-2.376 0 -4.068 -1.692t-1.692 -4.068zm4.608 -35.712h32.256v-9.216h-5.76q-1.44 0 -2.448 -1.008t-1.008 -2.448v-5.76h-23.04v18.432zm3.456 32.256q0 0.504 0.324 0.828t0.828 0.324h4.608q0.504 0 0.828 -0.324t0.324 -0.828v-4.608q0 -0.504 -0.324 -0.828t-0.828 -0.324h-4.608q-0.504 0 -0.828 0.324t-0.324 0.828v4.608zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h4.608q0.504 0 0.828 -0.324t0.324 -0.828v-4.608q0 -0.504 -0.324 -0.828t-0.828 -0.324h-4.608q-0.504 0 -0.828 0.324t-0.324 0.828v4.608zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h4.608q0.504 0 0.828 -0.324t0.324 -0.828v-4.608q0 -0.504 -0.324 -0.828t-0.828 -0.324h-4.608q-0.504 0 -0.828 0.324t-0.324 0.828v4.608zm9.216 18.432q0 0.504 0.324 0.828t0.828 0.324h4.608q0.504 0 0.828 -0.324t0.324 -0.828v-4.608q0 -0.504 -0.324 -0.828t-0.828 -0.324h-4.608q-0.504 0 -0.828 0.324t-0.324 0.828v4.608zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h4.608q0.504 0 0.828 -0.324t0.324 -0.828v-4.608q0 -0.504 -0.324 -0.828t-0.828 -0.324h-4.608q-0.504 0 -0.828 0.324t-0.324 0.828v4.608zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h4.608q0.504 0 0.828 -0.324t0.324 -0.828v-4.608q0 -0.504 -0.324 -0.828t-0.828 -0.324h-4.608q-0.504 0 -0.828 0.324t-0.324 0.828v4.608zm9.216 18.432q0 0.504 0.324 0.828t0.828 0.324h4.608q0.504 0 0.828 -0.324t0.324 -0.828v-4.608q0 -0.504 -0.324 -0.828t-0.828 -0.324h-4.608q-0.504 0 -0.828 0.324t-0.324 0.828v4.608zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h4.608q0.504 0 0.828 -0.324t0.324 -0.828v-4.608q0 -0.504 -0.324 -0.828t-0.828 -0.324h-4.608q-0.504 0 -0.828 0.324t-0.324 0.828v4.608zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h4.608q0.504 0 0.828 -0.324t0.324 -0.828v-4.608q0 -0.504 -0.324 -0.828t-0.828 -0.324h-4.608q-0.504 0 -0.828 0.324t-0.324 0.828v4.608z","BUILDING":"M0 -2.231v-59.904q0 -0.936 0.684 -1.62t1.62 -0.684h46.08q0.936 0 1.62 0.684t0.684 1.62v59.904q0 0.936 -0.684 1.62t-1.62 0.684h-46.08q-0.936 0 -1.62 -0.684t-0.684 -1.62zm9.216 -12.672q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm9.216 39.168v6.912q0 0.504 0.324 0.828t0.828 0.324h11.52q0.504 0 0.828 -0.324t0.324 -0.828v-6.912q0 -0.504 -0.324 -0.828t-0.828 -0.324h-11.52q-0.504 0 -0.828 0.324t-0.324 0.828zm0 -11.52q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm9.216 27.648q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm9.216 36.864q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304zm0 -9.216q0 0.504 0.324 0.828t0.828 0.324h2.304q0.504 0 0.828 -0.324t0.324 -0.828v-2.304q0 -0.504 -0.324 -0.828t-0.828 -0.324h-2.304q-0.504 0 -0.828 0.324t-0.324 0.828v2.304z","CHILD":"M2.304 -47.159q0 -1.44 1.008 -2.448t2.448 -1.008 2.448 1.008l8.208 8.208h13.248l8.208 -8.208q1.008 -1.008 2.448 -1.008t2.448 1.008 1.008 2.448 -1.008 2.448l-10.512 10.512v29.664q0 1.656 -1.188 2.844t-2.844 1.188 -2.844 -1.188 -1.188 -2.844v-13.824h-2.304v13.824q0 1.656 -1.188 2.844t-2.844 1.188 -2.844 -1.188 -1.188 -2.844v-29.664l-10.512 -10.512q-1.008 -1.008 -1.008 -2.448zm12.672 -3.456q0 -3.348 2.358 -5.706t5.706 -2.358 5.706 2.358 2.358 5.706 -2.358 5.706 -5.706 2.358 -5.706 -2.358 -2.358 -5.706z","PAW":"M0 -35.207q0 -2.88 1.512 -5.022t4.284 -2.142q2.736 0 5.094 1.998t3.618 4.824 1.26 5.49q0 2.88 -1.512 5.004t-4.284 2.124q-2.736 0 -5.094 -1.998t-3.618 -4.806 -1.26 -5.472zm9.216 25.38q0 -3.096 2.016 -6.894t5.022 -6.93 6.75 -5.256 6.948 -2.124q4.248 0 9.18 3.51t8.244 8.532 3.312 9.162q0 1.656 -0.612 2.754t-1.746 1.62 -2.322 0.72 -2.736 0.198q-2.448 0 -6.75 -1.62t-6.57 -1.62q-2.376 0 -6.93 1.602t-7.218 1.602q-6.588 0 -6.588 -5.256zm2.772 -41.184q0 -2.16 0.684 -4.086t2.268 -3.33 3.78 -1.404q2.772 0 4.986 2.07t3.294 4.86 1.08 5.454q0 2.16 -0.684 4.086t-2.268 3.33 -3.78 1.404q-2.736 0 -4.968 -2.07t-3.312 -4.878 -1.08 -5.436zm19.836 3.564q0 -2.664 1.08 -5.454t3.294 -4.86 4.986 -2.07q2.196 0 3.78 1.404t2.268 3.33 0.684 4.086q0 2.628 -1.08 5.436t-3.312 4.878 -4.968 2.07q-2.196 0 -3.78 -1.404t-2.268 -3.33 -0.684 -4.086zm12.312 17.388q0 -2.664 1.26 -5.49t3.618 -4.824 5.094 -1.998q2.772 0 4.284 2.142t1.512 5.022q0 2.664 -1.26 5.472t-3.618 4.806 -5.094 1.998q-2.772 0 -4.284 -2.124t-1.512 -5.004z","SPOON":"M2.304 -45.431q0 -4.608 1.53 -8.982t4.23 -7.2 5.76 -2.826 5.76 2.826 4.23 7.2 1.53 8.982q0 5.22 -2.052 8.766t-5.472 4.878l1.62 29.556q0.072 0.936 -0.576 1.62t-1.584 0.684h-6.912q-0.936 0 -1.584 -0.684t-0.576 -1.62l1.62 -29.556q-3.42 -1.332 -5.472 -4.878t-2.052 -8.766z","CUBE":"M0 -18.359v-27.648q0 -1.44 0.828 -2.628t2.196 -1.692l25.344 -9.216q0.792 -0.288 1.584 -0.288t1.584 0.288l25.344 9.216q1.368 0.504 2.196 1.692t0.828 2.628v27.648q0 1.26 -0.648 2.34t-1.764 1.692l-25.344 13.824q-1.008 0.576 -2.196 0.576t-2.196 -0.576l-25.344 -13.824q-1.116 -0.612 -1.764 -1.692t-0.648 -2.34zm4.824 -27.72l25.128 9.144 25.128 -9.144 -25.128 -9.144zm27.432 40.284l23.04 -12.564v-22.896l-23.04 8.388v27.072z","CUBES":"M0 -12.599v-14.976q0 -1.368 0.774 -2.52t2.034 -1.728l15.624 -6.696v-14.4q0 -1.368 0.774 -2.52t2.034 -1.728l16.128 -6.912q0.828 -0.36 1.8 -0.36t1.8 0.36l16.128 6.912q1.26 0.576 2.034 1.728t0.774 2.52v14.4l15.624 6.696q1.296 0.576 2.052 1.728t0.756 2.52v14.976q0 1.296 -0.684 2.412t-1.872 1.692l-16.128 8.064q-0.9 0.504 -2.052 0.504t-2.052 -0.504l-16.128 -8.064q-0.18 -0.072 -0.252 -0.144 -0.072 0.072 -0.252 0.144l-16.128 8.064q-0.9 0.504 -2.052 0.504t-2.052 -0.504l-16.128 -8.064q-1.188 -0.576 -1.872 -1.692t-0.684 -2.412zm6.192 -15.66l14.544 6.228 14.544 -6.228 -14.544 -6.228zm16.848 22.572l13.824 -6.912v-11.304l-13.824 5.904v12.312zm0.252 -47.34l15.876 6.804 15.876 -6.804 -15.876 -6.804zm18.18 20.448l13.824 -5.94v-9.576l-13.824 5.904v9.612zm1.584 4.32l14.544 6.228 14.544 -6.228 -14.544 -6.228zm16.848 22.572l13.824 -6.912v-11.304l-13.824 5.904v12.312z","BEHANCE":"M0 -9.935v-45.36h21.384q3.132 0 5.58 0.504t4.554 1.71 3.24 3.474 1.134 5.544q0 6.516 -6.192 9.468 4.104 1.152 6.192 4.14t2.088 7.344q0 2.7 -0.882 4.914t-2.376 3.726 -3.546 2.556 -4.356 1.512 -4.824 0.468h-21.996zm9.972 -7.704h10.656q7.38 0 7.38 -6.012 0 -6.48 -7.164 -6.48h-10.872v12.492zm0 -19.332h10.116q2.808 0 4.446 -1.314t1.638 -4.086q0 -5.184 -6.84 -5.184h-9.36v10.584zm30.96 10.656q0 -7.488 4.698 -12.438t12.114 -4.95q4.968 0 8.658 2.448t5.508 6.444 1.818 8.928q0 0.612 -0.072 1.692h-23.688q0 3.996 2.07 6.174t5.994 2.178q2.268 0 4.392 -1.152t2.736 -3.132h7.956q-3.6 11.052 -15.372 11.052 -7.704 0 -12.258 -4.752t-4.554 -12.492zm7.2 -21.456h18.396v-4.464h-18.396v4.464zm1.836 17.892h14.688q-0.648 -7.02 -7.2 -7.02 -3.24 0 -5.256 1.89t-2.232 5.13z","BEHANCE_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm4.608 -3.384h13.752q4.212 0 7.092 -2.07t2.88 -6.138q0 -5.688 -5.148 -7.2 3.852 -1.872 3.852 -5.904 0 -2.052 -0.702 -3.474t-2.034 -2.178 -2.844 -1.062 -3.492 -0.306h-13.356v28.332zm6.228 -4.824v-7.812h6.804q4.464 0 4.464 4.068 0 3.744 -4.608 3.744h-6.66zm0 -12.06v-6.624h5.868q4.284 0 4.284 3.24 0 3.384 -3.816 3.384h-6.336zm19.332 6.66q0 4.86 2.844 7.812t7.668 2.952q7.38 0 9.612 -6.876h-4.968q-0.396 1.224 -1.71 1.944t-2.718 0.72q-2.448 0 -3.744 -1.368t-1.296 -3.852h14.796q0.036 -0.36 0.036 -1.08 0 -4.752 -2.682 -7.938t-7.326 -3.186q-4.608 0 -7.56 3.096t-2.952 7.776zm4.536 -13.428v-2.772h11.484v2.772h-11.484zm1.152 11.196q0.144 -2.016 1.404 -3.204t3.276 -1.188q4.068 0 4.464 4.392h-9.144z","STEAM":"M0 -36.647q0 -3.096 2.196 -5.274t5.256 -2.178q2.628 0 4.68 1.656t2.628 4.212l28.188 11.34q1.764 -1.044 3.816 -1.044 0.504 0 0.756 0.036l6.228 -8.928q0.036 -4.104 2.952 -7.002t7.02 -2.898q4.14 0 7.074 2.916t2.934 7.056 -2.934 7.074 -7.074 2.934l-9.54 6.984q-0.288 2.88 -2.43 4.806t-4.986 1.926q-2.628 0 -4.68 -1.656t-2.628 -4.212l-28.188 -11.34q-1.836 1.08 -3.816 1.08 -3.06 0 -5.256 -2.196t-2.196 -5.292zm1.98 0q0 2.304 1.602 3.906t3.87 1.602q0.396 0 1.188 -0.144l-2.304 -0.936q-1.188 -0.504 -1.89 -1.602t-0.702 -2.394q0 -1.8 1.278 -3.078t3.078 -1.278q0.72 0 1.476 0.288v-0.036l2.736 1.116q-0.72 -1.332 -2.034 -2.124t-2.826 -0.792q-2.268 0 -3.87 1.602t-1.602 3.87zm39.924 18.72q0.684 1.332 1.998 2.124t2.862 0.792q2.268 0 3.87 -1.602t1.602 -3.87 -1.602 -3.888 -3.87 -1.62q-0.468 0 -1.188 0.144 0.072 0.036 0.72 0.288t0.774 0.306 0.666 0.306 0.684 0.36 0.576 0.396 0.558 0.486 0.396 0.522 0.36 0.648 0.18 0.756 0.09 0.9q0 1.8 -1.278 3.078t-3.078 1.278q-0.504 0 -1.134 -0.162t-1.044 -0.324 -1.134 -0.486 -1.008 -0.432zm15.12 -18.828q0 2.772 1.962 4.734t4.734 1.962 4.752 -1.962 1.98 -4.734 -1.98 -4.734 -4.752 -1.962q-2.736 0 -4.716 1.962t-1.98 4.734zm1.404 0q0 -2.232 1.566 -3.798t3.762 -1.566 3.78 1.584 1.584 3.78 -1.566 3.762 -3.798 1.566q-2.196 0 -3.762 -1.566t-1.566 -3.762z","STEAM_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm4.608 -19.188q0 1.908 1.368 3.276t3.312 1.368q1.296 0 2.376 -0.648l17.604 7.092q0.36 1.584 1.638 2.628t2.934 1.044q1.8 0 3.114 -1.224t1.494 -2.988l6.012 -4.392q2.556 0 4.392 -1.818t1.836 -4.41 -1.836 -4.428 -4.392 -1.836q-2.592 0 -4.41 1.818t-1.854 4.374l-3.888 5.58 -0.234 -0.018 -0.234 -0.018q-1.26 0 -2.412 0.684l-17.604 -7.092q-0.36 -1.584 -1.638 -2.628t-2.898 -1.044q-1.944 0 -3.312 1.368t-1.368 3.312zm1.224 0q0 -1.44 1.008 -2.448t2.448 -1.008q0.972 0 1.782 0.504t1.242 1.332l-1.728 -0.684q-1.044 -0.396 -2.034 0.072t-1.386 1.476q-0.432 1.044 -0.018 2.052t1.422 1.44v0.036l1.44 0.576q-0.504 0.072 -0.72 0.072 -1.44 0 -2.448 -0.99t-1.008 -2.43zm24.948 11.664l1.692 0.684q1.044 0.432 2.088 -0.018t1.476 -1.494q0.396 -1.044 -0.036 -2.07t-1.476 -1.458l-1.44 -0.576q0.504 -0.072 0.756 -0.072 1.404 0 2.412 0.99t1.008 2.43 -1.008 2.43 -2.412 0.99q-2.124 0 -3.06 -1.836zm9.468 -11.736q0 -1.728 1.224 -2.952t2.988 -1.224q1.728 0 2.952 1.224t1.224 2.952 -1.224 2.952 -2.952 1.224q-1.764 0 -2.988 -1.224t-1.224 -2.952zm0.864 -0.036q0 1.404 0.99 2.376t2.358 0.972 2.358 -0.972 0.99 -2.376q0 -1.368 -0.99 -2.358t-2.358 -0.99 -2.358 0.99 -0.99 2.358z","RECYCLE":"M0.576 -44.063l15.588 0.612 6.48 13.644 -5.292 -3.312q-2.268 2.592 -4.014 5.202t-2.61 4.5 -1.422 3.402 -0.666 2.268l-0.144 0.756 -6.84 -12.852q-0.612 -0.936 -0.648 -2.016t0.216 -1.692l0.288 -0.648q1.26 -2.268 4.104 -6.768zm9.162 29.232q-0.126 -1.008 0.144 -2.34t0.432 -1.98 0.774 -2.304 0.684 -1.908q2.808 0.432 18.324 1.008l-0.54 13.248 -0.072 0.792 -15.12 -1.044q-1.296 -0.108 -2.412 -1.134t-1.692 -2.358q-0.396 -0.972 -0.522 -1.98zm0.846 -34.776l8.1 -12.816q0.72 -1.116 2.16 -1.62t2.88 -0.36q0.864 0.072 1.746 0.432t1.512 0.756 1.494 1.188 1.296 1.242 1.296 1.422 1.152 1.26q-1.692 2.268 -9.54 15.66l-11.412 -6.732zm17.568 -14.4l14.58 0.036q1.116 -0.108 2.088 0.378t1.404 1.026l0.396 0.54q1.404 2.196 4.032 6.84l5.112 -2.988 -7.92 13.428 -15.084 -0.72 5.436 -3.096q-1.224 -3.204 -2.7 -5.976t-2.718 -4.446 -2.322 -2.88 -1.692 -1.674zm6.156 47.772l7.596 -13.032 0.252 6.228q6.12 0.576 10.188 0.18t6.12 -1.188l2.016 -0.792 -6.768 12.924q-0.432 1.044 -1.314 1.674t-1.566 0.738l-0.648 0.144q-2.556 0.252 -7.884 0.432l0.288 5.904zm9.54 -23.4l11.268 -7.02 0.684 -0.396 7.632 13.068q0.648 1.332 0.45 2.736t-0.99 2.664q-0.468 0.72 -1.188 1.332t-1.368 1.008 -1.746 0.792 -1.692 0.576 -1.854 0.504 -1.656 0.432q-1.224 -2.592 -9.54 -15.696z","CAR":"M0 -14.903v-13.824q0 -3.348 2.358 -5.706t5.706 -2.358h1.008l3.78 -15.084q0.828 -3.384 3.744 -5.67t6.444 -2.286h25.344q3.528 0 6.444 2.286t3.744 5.67l3.78 15.084h1.008q3.348 0 5.706 2.358t2.358 5.706v13.824q0 0.504 -0.324 0.828t-0.828 0.324h-4.608v4.608q0 2.88 -2.016 4.896t-4.896 2.016 -4.896 -2.016 -2.016 -4.896v-4.608h-33.408v4.608q0 2.88 -2.016 4.896t-4.896 2.016 -4.896 -2.016 -2.016 -4.896v-4.608h-3.456q-0.504 0 -0.828 -0.324t-0.324 -0.828zm5.76 -10.368q0 2.376 1.692 4.068t4.068 1.692 4.068 -1.692 1.692 -4.068 -1.692 -4.068 -4.068 -1.692 -4.068 1.692 -1.692 4.068zm12.816 -11.52h34.272l-3.204 -12.852q-0.072 -0.288 -0.504 -0.63t-0.756 -0.342h-25.344q-0.324 0 -0.756 0.342t-0.504 0.63zm34.416 11.52q0 2.376 1.692 4.068t4.068 1.692 4.068 -1.692 1.692 -4.068 -1.692 -4.068 -4.068 -1.692 -4.068 1.692 -1.692 4.068z","TAXI":"M0 -10.295v-13.824q0 -3.348 2.358 -5.706t5.706 -2.358h1.008l3.78 -15.084q0.828 -3.384 3.744 -5.67t6.444 -2.286h4.608v-8.064q0 -0.504 0.324 -0.828t0.828 -0.324h16.128q0.504 0 0.828 0.324t0.324 0.828v8.064h2.304q3.528 0 6.444 2.286t3.744 5.67l3.78 15.084h1.008q3.348 0 5.706 2.358t2.358 5.706v13.824q0 0.504 -0.324 0.828t-0.828 0.324h-4.608v2.304q0 2.88 -2.016 4.896t-4.896 2.016 -4.896 -2.016 -2.016 -4.896v-2.304h-33.408v2.304q0 2.88 -2.016 4.896t-4.896 2.016 -4.896 -2.016 -2.016 -4.896v-2.304h-3.456q-0.504 0 -0.828 -0.324t-0.324 -0.828zm5.76 -10.368q0 2.376 1.692 4.068t4.068 1.692 4.068 -1.692 1.692 -4.068 -1.692 -4.068 -4.068 -1.692 -4.068 1.692 -1.692 4.068zm12.816 -11.52h34.272l-3.204 -12.852q-0.072 -0.288 -0.504 -0.63t-0.756 -0.342h-25.344q-0.324 0 -0.756 0.342t-0.504 0.63zm34.416 11.52q0 2.376 1.692 4.068t4.068 1.692 4.068 -1.692 1.692 -4.068 -1.692 -4.068 -4.068 -1.692 -4.068 1.692 -1.692 4.068z","TREE":"M1.152 -11.447q0 -0.936 0.684 -1.62l14.472 -14.508h-8.244q-0.936 0 -1.62 -0.684t-0.684 -1.62 0.684 -1.62l14.472 -14.508h-7.092q-0.936 0 -1.62 -0.684t-0.684 -1.62 0.684 -1.62l13.824 -13.824q0.684 -0.684 1.62 -0.684t1.62 0.684l13.824 13.824q0.684 0.684 0.684 1.62t-0.684 1.62 -1.62 0.684h-7.092l14.472 14.508q0.684 0.684 0.684 1.62t-0.684 1.62 -1.62 0.684h-8.244l14.472 14.508q0.684 0.684 0.684 1.62t-0.684 1.62 -1.62 0.684h-16.632q0.036 0.612 0.216 3.15t0.18 3.906q0 0.9 -0.648 1.53t-1.548 0.63h-11.52q-0.9 0 -1.548 -0.63t-0.648 -1.53q0 -1.368 0.18 -3.906t0.216 -3.15h-16.632q-0.936 0 -1.62 -0.684t-0.684 -1.62z","SPOTIFY":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm8.532 -8.856q0 1.116 0.738 1.872t1.854 0.756q0.396 0 1.44 -0.288 4.788 -1.332 11.052 -1.332 5.724 0 11.142 1.224t9.126 3.42q0.756 0.432 1.44 0.432 1.044 0 1.818 -0.738t0.774 -1.854q0 -1.692 -1.44 -2.52 -4.536 -2.628 -10.548 -3.978t-12.348 -1.35q-7.344 0 -13.104 1.692 -0.828 0.252 -1.386 0.918t-0.558 1.746zm1.872 8.964q0 0.9 0.63 1.53t1.53 0.63q0.252 0 1.332 -0.288 4.392 -1.188 9.036 -1.188 10.044 0 17.568 4.464 0.864 0.468 1.368 0.468 0.9 0 1.53 -0.63t0.63 -1.53q0 -1.44 -1.26 -2.196 -8.532 -5.076 -19.728 -5.076 -5.508 0 -10.908 1.512 -1.728 0.468 -1.728 2.304zm1.152 8.316q0 0.72 0.486 1.242t1.278 0.522q0.18 0 1.332 -0.288 4.752 -0.972 8.748 -0.972 8.136 0 14.292 3.708 0.684 0.396 1.188 0.396 0.684 0 1.188 -0.486t0.504 -1.242q0 -1.152 -1.08 -1.836 -6.948 -4.14 -16.092 -4.14 -4.788 0 -10.332 1.224 -1.512 0.324 -1.512 1.872z","DEVIANTART":"M0 -9.539v-45.288q0 -2.088 1.458 -3.546t3.546 -1.458h45.288q2.088 0 3.546 1.458t1.458 3.546v45.288q0 2.088 -1.458 3.546t-3.546 1.458h-45.288q-2.088 0 -3.546 -1.458t-1.458 -3.546zm2.556 0q0 1.008 0.72 1.728t1.728 0.72h45.288q1.008 0 1.728 -0.72t0.72 -1.728v-45.288q0 -1.008 -0.72 -1.728t-1.728 -0.72h-45.288q-1.008 0 -1.728 0.72t-0.72 1.728v45.288zm1.8 0v-5.076l25.596 -7.02 -7.632 -15.804q0.144 -0.036 0.432 -0.09t0.432 -0.054q6.12 -1.152 10.926 -0.774t7.956 1.656 5.166 3.402q0.972 1.008 -0.9 1.512 -2.304 0.576 -9.216 2.232l-3.492 -7.128q-3.996 -0.252 -8.64 0.576l6.768 13.932 19.188 -5.22v17.856q0 0.252 -0.198 0.45t-0.45 0.198h-45.288q-0.252 0 -0.45 -0.198t-0.198 -0.45zm0 -25.128v-20.16q0 -0.252 0.198 -0.45t0.45 -0.198h45.288q0.252 0 0.45 0.198t0.198 0.45v15.408q-3.06 -1.08 -6.768 -1.872 -10.584 -2.304 -23.22 -0.432l-0.648 0.108 -2.34 -4.824h-8.388l3.06 6.84q-4.752 1.836 -8.28 4.932zm4.5 10.656q-0.864 -7.308 5.976 -10.98l4.644 9.72 -9.18 2.196q-0.504 0.108 -0.936 -0.162t-0.504 -0.774z","SOUNDCLOUD":"M0 -23.723l0.612 -4.608q0.072 -0.324 0.324 -0.324t0.324 0.324l0.72 4.608 -0.72 4.536q-0.072 0.324 -0.324 0.324t-0.324 -0.324zm2.844 0l0.828 -7.452q0 -0.324 0.324 -0.324 0.288 0 0.36 0.324l0.936 7.452 -0.936 7.308q-0.072 0.324 -0.36 0.324 -0.324 0 -0.324 -0.36zm3.24 0l0.756 -8.82q0.072 -0.432 0.432 -0.432 0.396 0 0.396 0.432l0.9 8.82 -0.9 8.532q0 0.396 -0.396 0.396 -0.36 0 -0.432 -0.396zm3.24 0l0.756 -9.072q0 -0.468 0.468 -0.468 0.432 0 0.504 0.468l0.828 9.072 -0.828 8.784q-0.072 0.468 -0.504 0.468 -0.468 0 -0.468 -0.468zm3.276 0l0.72 -8.424q0 -0.216 0.162 -0.378t0.378 -0.162q0.504 0 0.576 0.54l0.756 8.424 -0.756 8.856q-0.072 0.576 -0.576 0.576 -0.216 0 -0.378 -0.162t-0.162 -0.414zm1.836 8.856zm1.476 -8.856l0.648 -13.68q0.072 -0.648 0.648 -0.648 0.252 0 0.432 0.198t0.18 0.45l0.756 13.68 -0.756 8.856q0 0.252 -0.18 0.45t-0.432 0.198q-0.576 0 -0.648 -0.648zm3.312 0.072l0.576 -16.848q0.072 -0.684 0.72 -0.684 0.288 0 0.486 0.198t0.198 0.486l0.684 16.848 -0.684 8.784q0 0.288 -0.198 0.486t-0.486 0.198q-0.648 0 -0.72 -0.684zm3.384 -0.072l0.576 -18.216q0 -0.324 0.234 -0.558t0.522 -0.234q0.324 0 0.54 0.234t0.252 0.558l0.648 18.216 -0.648 8.712q-0.072 0.756 -0.792 0.756 -0.684 0 -0.756 -0.756zm3.42 0l0.504 8.676q0.036 0.36 0.27 0.594t0.558 0.234q0.792 0 0.864 -0.828l0.576 -8.676 -0.576 -18.828q-0.036 -0.36 -0.27 -0.612t-0.594 -0.252q-0.324 0 -0.576 0.252t-0.252 0.612zm2.196 8.676zm1.188 -8.676l0.504 -18.36q0 -0.396 0.27 -0.648t0.63 -0.252 0.63 0.252 0.27 0.648l0.54 18.36 -0.54 8.604q0 0.36 -0.27 0.63t-0.63 0.27 -0.612 -0.252 -0.288 -0.648zm3.456 0.036l0.432 -17.712q0.036 -0.432 0.324 -0.72t0.684 -0.288 0.666 0.288 0.306 0.72l0.504 17.712 -0.504 8.496q0 0.396 -0.288 0.684t-0.684 0.288 -0.684 -0.288 -0.324 -0.684zm3.492 -0.036q0 0.036 0.396 8.496v0.036q0 0.36 0.216 0.612 0.324 0.396 0.828 0.396 0.396 0 0.72 -0.324 0.324 -0.252 0.324 -0.72l0.036 -0.864 0.396 -7.596 -0.432 -21.096q0 -0.576 -0.468 -0.864 -0.288 -0.18 -0.576 -0.18t-0.576 0.18q-0.468 0.288 -0.468 0.864l-0.036 0.216zm2.484 8.496zm0.864 -8.46l0.432 -22.896v-0.108q0.072 -0.54 0.432 -0.864 0.324 -0.252 0.72 -0.252 0.288 0 0.54 0.18 0.504 0.288 0.576 0.936l0.504 23.004 -0.504 8.316q0 0.468 -0.324 0.792t-0.792 0.324 -0.792 -0.324 -0.36 -0.792l-0.216 -4.104zm3.636 8.28v-32.364q0 -0.828 1.008 -1.188 3.06 -1.224 6.516 -1.224 7.02 0 12.168 4.734t5.76 11.646q1.908 -0.792 3.96 -0.792 4.212 0 7.2 2.988t2.988 7.236q0 4.212 -2.988 7.182t-7.2 2.97h-28.296q-0.468 -0.072 -0.792 -0.396t-0.324 -0.792z","DATABASE":"M0 -9.143v-6.12q4.284 3.024 11.7 4.572t15.948 1.548 15.948 -1.548 11.7 -4.572v6.12q0 2.484 -3.708 4.608t-10.08 3.366 -13.86 1.242 -13.86 -1.242 -10.08 -3.366 -3.708 -4.608zm0 -13.824v-6.12q4.284 3.024 11.7 4.572t15.948 1.548 15.948 -1.548 11.7 -4.572v6.12q0 2.484 -3.708 4.608t-10.08 3.366 -13.86 1.242 -13.86 -1.242 -10.08 -3.366 -3.708 -4.608zm0 -13.824v-6.12q4.284 3.024 11.7 4.572t15.948 1.548 15.948 -1.548 11.7 -4.572v6.12q0 2.484 -3.708 4.608t-10.08 3.366 -13.86 1.242 -13.86 -1.242 -10.08 -3.366 -3.708 -4.608zm0 -13.824v-4.608q0 -2.484 3.708 -4.608t10.08 -3.366 13.86 -1.242 13.86 1.242 10.08 3.366 3.708 4.608v4.608q0 2.484 -3.708 4.608t-10.08 3.366 -13.86 1.242 -13.86 -1.242 -10.08 -3.366 -3.708 -4.608z","FILE_PDF_O":"M0 -3.383v-57.6q0 -1.44 1.008 -2.448t2.448 -1.008h32.256q1.44 0 3.168 0.72t2.736 1.728l11.232 11.232q1.008 1.008 1.728 2.736t0.72 3.168v41.472q0 1.44 -1.008 2.448t-2.448 1.008h-48.384q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 -1.152h46.08v-36.864h-14.976q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-27.648v55.296zm4.644 -6.768q0.324 -1.44 2.016 -3.294t4.752 -3.474q0.504 -0.324 0.828 0.216 0.072 0.072 0.072 0.144 1.872 -3.06 3.852 -7.092 2.448 -4.896 3.744 -9.432 -0.864 -2.952 -1.098 -5.742t0.234 -4.59q0.396 -1.44 1.512 -1.44h0.792q0.828 0 1.26 0.54 0.648 0.756 0.324 2.448 -0.072 0.216 -0.144 0.288 0.036 0.108 0.036 0.288v1.08q-0.072 4.428 -0.504 6.912 1.98 5.904 5.256 8.568 1.188 0.936 3.024 2.016 2.124 -0.252 4.212 -0.252 5.292 0 6.372 1.764 0.576 0.792 0.072 1.872l-0.036 0.072 -0.072 0.072v0.036q-0.216 1.368 -2.556 1.368 -1.728 0 -4.14 -0.72t-4.68 -1.908q-7.956 0.864 -14.112 2.988 -5.508 9.432 -8.712 9.432 -0.54 0 -1.008 -0.252l-0.864 -0.432 -0.216 -0.18q-0.36 -0.36 -0.216 -1.296zm2.196 0.216q1.872 -0.864 4.932 -5.688 -1.836 1.44 -3.15 3.024t-1.782 2.664zm9.864 -9.324q4.86 -1.944 10.224 -2.916 -0.072 -0.036 -0.468 -0.342t-0.576 -0.486q-2.736 -2.412 -4.572 -6.336 -0.972 3.096 -2.988 7.092 -1.08 2.016 -1.62 2.988zm4.392 -19.044q0.036 -0.252 0.252 -1.584 0 -0.108 0.252 -1.548 0.036 -0.144 0.144 -0.288l-0.036 -0.072 -0.018 -0.054 -0.018 -0.054q-0.036 -0.792 -0.468 -1.296l-0.036 0.072v0.072q-0.54 1.512 -0.072 4.752zm11.16 -6.552h13.536q-0.36 -1.044 -0.792 -1.476l-11.268 -11.268q-0.432 -0.432 -1.476 -0.792v13.536zm2.664 24.156q2.736 1.008 4.464 1.008 0.504 0 0.648 -0.036l-0.072 -0.108q-0.864 -0.864 -5.04 -0.864z","FILE_WORD_O":"M0 -3.383v-57.6q0 -1.44 1.008 -2.448t2.448 -1.008h32.256q1.44 0 3.168 0.72t2.736 1.728l11.232 11.232q1.008 1.008 1.728 2.736t0.72 3.168v41.472q0 1.44 -1.008 2.448t-2.448 1.008h-48.384q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 -1.152h46.08v-36.864h-14.976q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-27.648v55.296zm3.78 -28.404h2.52l5.904 23.796h5.724l4.608 -17.46q0.252 -0.72 0.36 -1.656 0.072 -0.576 0.072 -0.864h0.144l0.108 0.864q0.036 0.108 0.126 0.72t0.198 0.936l4.608 17.46h5.724l5.904 -23.796h2.52v-3.852h-10.8v3.852h3.24l-3.564 15.768q-0.18 0.72 -0.252 1.656l-0.072 0.756h-0.144l-0.108 -0.756q-0.036 -0.18 -0.144 -0.756t-0.18 -0.9l-5.184 -19.62h-4.104l-5.184 19.62q-0.072 0.324 -0.162 0.882t-0.126 0.774l-0.144 0.756h-0.144l-0.072 -0.756q-0.072 -0.936 -0.252 -1.656l-3.564 -15.768h3.24v-3.852h-10.8v3.852zm28.476 -13.068h13.536q-0.36 -1.044 -0.792 -1.476l-11.268 -11.268q-0.432 -0.432 -1.476 -0.792v13.536z","FILE_EXCEL_O":"M0 -3.383v-57.6q0 -1.44 1.008 -2.448t2.448 -1.008h32.256q1.44 0 3.168 0.72t2.736 1.728l11.232 11.232q1.008 1.008 1.728 2.736t0.72 3.168v41.472q0 1.44 -1.008 2.448t-2.448 1.008h-48.384q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 -1.152h46.08v-36.864h-14.976q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-27.648v55.296zm10.836 -4.608h10.116v-3.816h-2.7l3.708 -5.796q0.18 -0.252 0.36 -0.594t0.27 -0.486 0.126 -0.144h0.072q0.036 0.144 0.18 0.36 0.072 0.144 0.162 0.27t0.216 0.288 0.234 0.306l3.852 5.796h-2.736v3.816h10.476v-3.816h-2.448l-6.912 -9.828 7.02 -10.152h2.412v-3.852h-10.044v3.852h2.664l-3.708 5.724q-0.144 0.252 -0.36 0.594t-0.324 0.486l-0.072 0.108h-0.072q-0.036 -0.144 -0.18 -0.36 -0.216 -0.396 -0.612 -0.828l-3.816 -5.724h2.736v-3.852h-10.44v3.852h2.448l6.804 9.792 -6.984 10.188h-2.448v3.816zm21.42 -36.864h13.536q-0.36 -1.044 -0.792 -1.476l-11.268 -11.268q-0.432 -0.432 -1.476 -0.792v13.536z","FILE_POWERPOINT_O":"M0 -3.383v-57.6q0 -1.44 1.008 -2.448t2.448 -1.008h32.256q1.44 0 3.168 0.72t2.736 1.728l11.232 11.232q1.008 1.008 1.728 2.736t0.72 3.168v41.472q0 1.44 -1.008 2.448t-2.448 1.008h-48.384q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 -1.152h46.08v-36.864h-14.976q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-27.648v55.296zm10.368 -4.608h11.772v-3.816h-3.348v-6.012h4.932q2.736 0 4.248 -0.54 2.412 -0.828 3.834 -3.132t1.422 -5.256q0 -2.916 -1.332 -5.076t-3.6 -3.132q-1.728 -0.684 -4.68 -0.684h-13.248v3.852h3.312v19.98h-3.312v3.816zm8.424 -13.896v-9.648h4.32q1.872 0 2.988 0.648 2.016 1.188 2.016 4.14 0 3.204 -2.232 4.32 -1.116 0.54 -2.808 0.54h-4.284zm13.464 -22.968h13.536q-0.36 -1.044 -0.792 -1.476l-11.268 -11.268q-0.432 -0.432 -1.476 -0.792v13.536z","FILE_PICTURE_O":"M0 -3.383v-57.6q0 -1.44 1.008 -2.448t2.448 -1.008h32.256q1.44 0 3.168 0.72t2.736 1.728l11.232 11.232q1.008 1.008 1.728 2.736t0.72 3.168v41.472q0 1.44 -1.008 2.448t-2.448 1.008h-48.384q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 -1.152h46.08v-36.864h-14.976q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-27.648v55.296zm4.608 -4.608v-6.912l6.912 -6.912 4.608 4.608 13.824 -13.824 11.52 11.52v11.52h-36.864zm0 -25.344q0 -2.88 2.016 -4.896t4.896 -2.016 4.896 2.016 2.016 4.896 -2.016 4.896 -4.896 2.016 -4.896 -2.016 -2.016 -4.896zm27.648 -11.52h13.536q-0.36 -1.044 -0.792 -1.476l-11.268 -11.268q-0.432 -0.432 -1.476 -0.792v13.536z","FILE_ZIP_O":"M0 -3.383v-57.6q0 -1.44 1.008 -2.448t2.448 -1.008h32.256q1.44 0 3.168 0.72t2.736 1.728l11.232 11.232q1.008 1.008 1.728 2.736t0.72 3.168v41.472q0 1.44 -1.008 2.448t-2.448 1.008h-48.384q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 -1.152h46.08v-36.864h-14.976q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-4.608v4.608h-4.608v-4.608h-18.432v55.296zm9.216 -11.52q0 -0.9 0.288 -1.872 0.756 -2.268 4.32 -14.256v-4.608h4.608v4.608h2.844q0.792 0 1.404 0.468t0.828 1.224l3.852 12.564q0.288 0.972 0.288 1.872 0 2.988 -2.61 4.95t-6.606 1.962 -6.606 -1.962 -2.61 -4.95zm4.608 0q0 0.936 1.35 1.62t3.258 0.684 3.258 -0.684 1.35 -1.62 -1.35 -1.62 -3.258 -0.684 -3.258 0.684 -1.35 1.62zm0 -25.344h4.608v-4.608h-4.608v4.608zm0 -9.216h4.608v-4.608h-4.608v4.608zm4.608 13.824h4.608v-4.608h-4.608v4.608zm0 -9.216h4.608v-4.608h-4.608v4.608zm13.824 0h13.536q-0.36 -1.044 -0.792 -1.476l-11.268 -11.268q-0.432 -0.432 -1.476 -0.792v13.536z","FILE_SOUND_O":"M0 -3.383v-57.6q0 -1.44 1.008 -2.448t2.448 -1.008h32.256q1.44 0 3.168 0.72t2.736 1.728l11.232 11.232q1.008 1.008 1.728 2.736t0.72 3.168v41.472q0 1.44 -1.008 2.448t-2.448 1.008h-48.384q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 -1.152h46.08v-36.864h-14.976q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-27.648v55.296zm4.608 -14.976v-6.912q0 -0.504 0.324 -0.828t0.828 -0.324h4.716l5.976 -6.012q0.576 -0.54 1.26 -0.252 0.72 0.288 0.72 1.08v19.584q0 0.792 -0.72 1.08 -0.288 0.072 -0.432 0.072 -0.432 0 -0.828 -0.324l-5.976 -6.012h-4.716q-0.504 0 -0.828 -0.324t-0.324 -0.828zm18.216 2.934q0.036 0.954 0.72 1.602 0.72 0.612 1.584 0.612 0.972 0 1.692 -0.72 3.132 -3.348 3.132 -7.884t-3.132 -7.884q-0.648 -0.684 -1.62 -0.72t-1.656 0.612 -0.72 1.602 0.648 1.674q1.872 2.052 1.872 4.716t-1.872 4.716q-0.684 0.72 -0.648 1.674zm7.614 5.472q0.09 0.954 0.846 1.53 0.648 0.54 1.44 0.54 1.116 0 1.8 -0.864 4.644 -5.724 4.644 -13.068t-4.644 -13.068q-0.576 -0.756 -1.548 -0.864t-1.692 0.504q-0.756 0.612 -0.846 1.566t0.522 1.71q3.6 4.428 3.6 10.152t-3.6 10.152q-0.612 0.756 -0.522 1.71zm1.818 -34.902h13.536q-0.36 -1.044 -0.792 -1.476l-11.268 -11.268q-0.432 -0.432 -1.476 -0.792v13.536z","FILE_VIDEO_O":"M0 -3.383v-57.6q0 -1.44 1.008 -2.448t2.448 -1.008h32.256q1.44 0 3.168 0.72t2.736 1.728l11.232 11.232q1.008 1.008 1.728 2.736t0.72 3.168v41.472q0 1.44 -1.008 2.448t-2.448 1.008h-48.384q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 -1.152h46.08v-36.864h-14.976q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-27.648v55.296zm4.608 -13.824v-13.824q0 -1.872 1.368 -3.24t3.24 -1.368h13.824q1.872 0 3.24 1.368t1.368 3.24v13.824q0 1.872 -1.368 3.24t-3.24 1.368h-13.824q-1.872 0 -3.24 -1.368t-1.368 -3.24zm25.344 -5.292v-3.24l9.54 -9.576q0.324 -0.324 0.828 -0.324 0.144 0 0.432 0.072 0.72 0.288 0.72 1.08v20.736q0 0.792 -0.72 1.08 -0.288 0.072 -0.432 0.072 -0.504 0 -0.828 -0.324zm2.304 -22.356h13.536q-0.36 -1.044 -0.792 -1.476l-11.268 -11.268q-0.432 -0.432 -1.476 -0.792v13.536z","FILE_CODE_O":"M0 -3.383v-57.6q0 -1.44 1.008 -2.448t2.448 -1.008h32.256q1.44 0 3.168 0.72t2.736 1.728l11.232 11.232q1.008 1.008 1.728 2.736t0.72 3.168v41.472q0 1.44 -1.008 2.448t-2.448 1.008h-48.384q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 -1.152h46.08v-36.864h-14.976q-1.44 0 -2.448 -1.008t-1.008 -2.448v-14.976h-27.648v55.296zm4.536 -20.052q-0.504 -0.684 0 -1.368l8.136 -10.836q0.288 -0.396 0.756 -0.45t0.864 0.234l1.836 1.368q0.396 0.288 0.45 0.756t-0.234 0.864l-6.552 8.748 6.552 8.748q0.288 0.396 0.234 0.864t-0.45 0.756l-1.836 1.368q-0.396 0.288 -0.864 0.234t-0.756 -0.45zm13.752 13.896l4.968 -29.916q0.072 -0.468 0.468 -0.738t0.864 -0.198l2.268 0.36q0.468 0.072 0.738 0.468t0.198 0.864l-4.968 29.916q-0.072 0.468 -0.468 0.738t-0.864 0.198l-2.268 -0.36q-0.468 -0.072 -0.738 -0.468t-0.198 -0.864zm11.214 -4.968q-0.054 -0.468 0.234 -0.864l6.552 -8.748 -6.552 -8.748q-0.288 -0.396 -0.234 -0.864t0.45 -0.756l1.836 -1.368q0.396 -0.288 0.864 -0.234t0.756 0.45l8.136 10.836q0.504 0.684 0 1.368l-8.136 10.836q-0.288 0.396 -0.756 0.45t-0.864 -0.234l-1.836 -1.368q-0.396 -0.288 -0.45 -0.756zm2.754 -30.348h13.536q-0.36 -1.044 -0.792 -1.476l-11.268 -11.268q-0.432 -0.432 -1.476 -0.792v13.536z","VINE":"M1.404 -55.439h10.188q0.936 7.848 2.52 14.346t3.762 11.412 4.374 8.478 5.04 7.02q6.084 -6.084 10.332 -14.616 -5.112 -2.592 -8.028 -7.92t-2.916 -11.988q0 -6.912 3.744 -11.322t10.224 -4.41q6.408 0 9.828 3.798t3.42 10.71q0 5.724 -2.088 10.296 -0.252 0.036 -0.702 0.108t-1.656 0.072 -2.268 -0.216 -2.232 -0.918 -1.818 -1.854q1.116 -3.708 1.116 -6.624 0 -3.132 -1.044 -4.752t-2.844 -1.62q-1.908 0 -3.06 1.782t-1.152 5.058q0 6.696 3.78 10.566t9.612 3.87q2.232 0 4.356 -0.504v7.128q-3.636 0.828 -7.128 0.828 -2.34 4.896 -5.958 9.756t-6.534 7.758 -4.608 3.834q-2.88 1.62 -5.832 -0.108 -1.008 -0.612 -2.178 -1.566t-3.06 -3.006 -3.69 -4.626 -3.87 -6.624 -3.798 -8.784 -3.294 -11.322 -2.538 -14.04z","CODEPEN":"M0 -22.355v-19.656q0 -1.476 1.224 -2.304l29.484 -19.656q0.756 -0.468 1.548 -0.468t1.548 0.468l29.484 19.656q1.224 0.828 1.224 2.304v19.656q0 1.476 -1.224 2.304l-29.484 19.656q-0.756 0.468 -1.548 0.468t-1.548 -0.468l-29.484 -19.656q-1.224 -0.828 -1.224 -2.304zm5.544 -5.184l6.948 -4.644 -6.948 -4.644v9.288zm2.232 5.184l21.708 14.472v-12.924l-12.024 -8.028zm0 -19.656l9.684 6.48 12.024 -8.028v-12.924zm14.688 9.828l9.792 6.552 9.792 -6.552 -9.792 -6.552zm12.564 24.3l21.708 -14.472 -9.684 -6.48 -12.024 8.028v12.924zm0 -35.676l12.024 8.028 9.684 -6.48 -21.708 -14.472v12.924zm16.992 11.376l6.948 4.644v-9.288z","JSFIDDLE":"M0 -23.795q0 -3.96 1.98 -7.308t5.292 -5.292q-0.432 -1.404 -0.432 -2.952 0 -4.14 2.952 -7.056t7.164 -2.916q3.42 0 6.192 2.088 2.7 -5.544 8.01 -8.928t11.754 -3.384q5.976 0 11.016 2.898t7.974 7.866 2.934 10.836l-0.018 0.648 -0.018 0.648q3.996 1.656 6.462 5.238t2.466 7.974q0 5.904 -4.248 10.098t-10.26 4.194l-0.414 -0.018 -0.378 -0.018h-43.812000000000005q-6.12 -0.36 -10.368 -4.518t-4.248 -10.098zm16.848 -3.276q0 4.392 3.024 6.948t7.488 2.556q4.932 0 8.64 -3.564 -0.576 -0.72 -1.71 -2.034t-1.566 -1.818q-2.412 2.34 -5.184 2.34 -1.98 0 -3.366 -1.206t-1.386 -3.15q0 -1.908 1.386 -3.132t3.294 -1.224q1.584 0 3.042 0.756t2.628 1.98 2.34 2.7 2.484 2.952 2.772 2.7 3.492 1.98 4.374 0.756q4.356 0 7.362 -2.574t3.006 -6.858q0 -4.356 -3.024 -6.912t-7.452 -2.556q-5.148 0 -8.676 3.492 0.504 0.576 1.062 1.224t1.242 1.44 1.044 1.224q2.376 -2.304 5.112 -2.304 1.872 0 3.312 1.188t1.44 3.024q0 2.052 -1.332 3.294t-3.384 1.242q-1.548 0 -2.97 -0.756t-2.592 -1.98 -2.358 -2.7 -2.502 -2.952 -2.79 -2.7 -3.474 -1.98 -4.266 -0.756q-4.392 0 -7.452 2.538t-3.06 6.822z","SUPPORT":"M0 -32.183q0 -6.552 2.556 -12.528t6.876 -10.296 10.296 -6.876 12.528 -2.556 12.528 2.556 10.296 6.876 6.876 10.296 2.556 12.528 -2.556 12.528 -6.876 10.296 -10.296 6.876 -12.528 2.556 -12.528 -2.556 -10.296 -6.876 -6.876 -10.296 -2.556 -12.528zm4.608 0q0 6.84 3.24 12.996l6.984 -6.984q-1.008 -2.952 -1.008 -6.012t1.008 -6.012l-6.984 -6.984q-3.24 6.156 -3.24 12.996zm13.824 0q0 5.724 4.05 9.774t9.774 4.05 9.774 -4.05 4.05 -9.774 -4.05 -9.774 -9.774 -4.05 -9.774 4.05 -4.05 9.774zm0.828 24.408q6.156 3.24 12.996 3.24t12.996 -3.24l-6.984 -6.984q-2.952 1.008 -6.012 1.008t-6.012 -1.008zm0 -48.816l6.984 6.984q2.952 -1.008 6.012 -1.008t6.012 1.008l6.984 -6.984q-6.156 -3.24 -12.996 -3.24t-12.996 3.24zm30.42 30.42l6.984 6.984q3.24 -6.156 3.24 -12.996t-3.24 -12.996l-6.984 6.984q1.008 2.952 1.008 6.012t-1.008 6.012z","CIRCLE_O_NOTCH":"M0 -32.183q0 -7.992 3.636 -14.922t9.954 -11.412 14.058 -5.598v9.36q-7.956 1.62 -13.194 7.956t-5.238 14.616q0 4.68 1.836 8.946t4.914 7.344 7.344 4.914 8.946 1.836 8.946 -1.836 7.344 -4.914 4.914 -7.344 1.836 -8.946q0 -8.28 -5.238 -14.616t-13.194 -7.956v-9.36q7.74 1.116 14.058 5.598t9.954 11.412 3.636 14.922q0 6.552 -2.556 12.528t-6.876 10.296 -10.296 6.876 -12.528 2.556 -12.528 -2.556 -10.296 -6.876 -6.876 -10.296 -2.556 -12.528z","REBEL":"M0.684 -32.975q0.288 -7.812 4.176 -14.616t10.98 -11.448h0.18l-0.036 0.108q-0.288 0.288 -1.008 1.206t-1.872 2.754 -2.16 3.978 -1.602 4.878 -0.504 5.418 1.404 5.67 3.906 5.544q1.8 1.8 3.672 2.502t3.258 0.414 2.502 -0.846 1.692 -1.17l0.576 -0.576q1.404 -1.836 1.908 -4.194t0.234 -4.41 -0.756 -3.852 -0.954 -2.88l-0.504 -1.044q-0.36 -0.9 -1.098 -1.782t-1.548 -1.476 -1.566 -1.062 -1.26 -0.684l-0.468 -0.216 3.744 -4.14q1.404 0.612 2.808 1.872t2.124 2.196l0.684 0.972q0.036 -1.728 -0.666 -3.726t-1.458 -3.15l-0.72 -1.116 5.796 -6.588 5.76 6.516q-1.188 1.656 -1.89 3.69t-0.81 3.258l-0.144 1.188q0.792 -1.332 2.214 -2.61t2.43 -1.89l1.008 -0.612 3.708 4.14q-1.584 0.504 -3.06 1.8t-2.16 2.34l-0.684 1.044q-1.116 2.016 -1.728 4.806t-0.252 6.12 2.052 5.634q1.188 1.62 2.79 2.178t3.06 0.198 2.736 -0.954 2.07 -1.206l0.756 -0.576q2.16 -1.908 3.474 -4.14t1.746 -4.374 0.36 -4.374 -0.648 -4.248 -1.332 -3.87 -1.638 -3.348 -1.62 -2.592 -1.242 -1.71l-0.468 -0.612q-0.504 -0.468 -0.252 -0.468l0.36 0.108q1.44 1.044 2.25 1.656t2.232 1.8 2.304 2.088 2.106 2.34 1.998 2.772 1.638 3.168 1.368 3.708 0.846 4.212 0.378 4.896q0.108 9.324 -3.888 16.74t-11.232 11.556 -16.416 4.14q-6.66 0 -12.636 -2.664t-10.206 -7.128 -6.624 -10.548 -2.178 -12.708z","GE":"M0 -32.183q0 -6.552 2.556 -12.528t6.876 -10.296 10.296 -6.876 12.528 -2.556 12.528 2.556 10.296 6.876 6.876 10.296 2.556 12.528 -2.556 12.528 -6.876 10.296 -10.296 6.876 -12.528 2.556 -12.528 -2.556 -10.296 -6.876 -6.876 -10.296 -2.556 -12.528zm1.584 0q0 6.228 2.43 11.916t6.534 9.792 9.792 6.534 11.916 2.43 11.916 -2.43 9.792 -6.534 6.534 -9.792 2.43 -11.916 -2.43 -11.916 -6.534 -9.792 -9.792 -6.534 -11.916 -2.43 -11.916 2.43 -9.792 6.534 -6.534 9.792 -2.43 11.916zm1.548 0q0 -7.38 3.528 -13.86l2.052 1.188q-1.08 2.016 -1.764 4.032l2.952 1.008q-1.26 3.6 -1.26 7.632 0 3.924 1.296 7.632l-2.988 1.008q0.792 2.16 1.764 4.032l-2.052 1.188q-3.528 -6.48 -3.528 -13.86zm4.284 15.228l2.088 -1.224q1.044 1.764 2.628 3.564l2.34 -2.052q5.328 6.048 13.248 7.632l-0.612 3.096q2.34 0.432 4.356 0.468v2.376q-7.488 -0.216 -13.86 -3.942t-10.188 -9.918zm0.036 -30.456q3.816 -6.192 10.152 -9.918t13.86 -3.942v2.376q-2.34 0.072 -4.356 0.468l0.612 3.096q-7.92 1.512 -13.248 7.596l-2.34 -2.016q-1.368 1.512 -2.628 3.528zm7.488 9.288q1.188 -3.348 3.564 -6.084l6.66 5.832q2.124 -2.448 5.292 -3.096l-1.728 -8.64q1.584 -0.36 3.528 -0.36t3.528 0.36l-1.728 8.64q3.168 0.648 5.292 3.096l6.66 -5.832q2.376 2.736 3.564 6.084l-8.388 2.88q0.504 1.512 0.504 3.06t-0.504 3.06l8.352 2.88q-1.116 3.312 -3.528 6.084l-6.66 -5.832q-2.052 2.412 -5.292 3.06l1.728 8.676q-1.872 0.36 -3.528 0.36t-3.528 -0.36l1.728 -8.676q-3.24 -0.648 -5.292 -3.06l-6.66 5.832q-2.412 -2.772 -3.528 -6.084l8.352 -2.88q-0.504 -1.512 -0.504 -3.06t0.504 -3.06zm18.108 32.652q2.016 -0.036 4.356 -0.468l-0.612 -3.096q7.92 -1.584 13.248 -7.632l2.34 2.052q1.584 -1.8 2.628 -3.564l2.088 1.224q-3.816 6.192 -10.188 9.918t-13.86 3.942v-2.376zm0 -53.424v-2.376q7.524 0.216 13.86 3.942t10.152 9.918l-2.052 1.188q-1.26 -2.016 -2.628 -3.528l-2.34 2.016q-5.328 -6.084 -13.248 -7.596l0.612 -3.096q-2.016 -0.396 -4.356 -0.468zm21.528 34.344q1.296 -3.708 1.296 -7.632 0 -4.032 -1.26 -7.632l2.952 -1.008q-0.684 -2.016 -1.764 -4.032l2.052 -1.188q3.528 6.48 3.528 13.86t-3.528 13.86l-2.052 -1.188q0.972 -1.872 1.764 -4.032z","GIT_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 -2.088q0 1.62 0.72 2.826t1.944 1.836 2.592 0.918 2.916 0.288q8.064 0 8.064 -6.768 0 -2.412 -1.728 -3.564t-4.536 -1.656q-0.972 -0.18 -1.854 -0.738t-0.882 -1.422q0 -1.584 1.764 -1.872 2.772 -0.54 4.392 -2.52t1.62 -4.824q0 -0.864 -0.36 -1.872 1.332 -0.324 1.764 -0.468v-4.5q-2.808 1.044 -4.86 1.044 -1.8 -1.044 -3.96 -1.044 -3.096 0 -5.22 2.052t-2.124 5.148q0 1.8 1.062 3.672t2.646 2.412v0.108q-1.368 0.612 -1.368 3.06 0 1.908 1.476 2.772v0.108q-4.068 1.332 -4.068 5.004zm4.536 -0.252q0 -2.304 3.528 -2.304 3.672 0 3.672 2.196 0 2.376 -3.348 2.376 -3.852 0 -3.852 -2.268zm0.468 -16.848q0 -3.24 2.772 -3.24 1.296 0 1.98 0.918t0.684 2.286q0 3.06 -2.664 3.06 -2.772 0 -2.772 -3.024zm12.96 -13.644q0 1.296 0.9 2.25t2.16 0.954 2.142 -0.972 0.882 -2.232q0 -1.296 -0.864 -2.286t-2.16 -0.99 -2.178 0.972 -0.882 2.304zm0.576 25.992h4.932q-0.072 -0.972 -0.072 -2.952v-13.932q0 -1.656 0.072 -2.484h-4.932q0.108 0.828 0.108 2.556v14.112q0 1.8 -0.108 2.7zm7.02 -15.156l1.332 -0.108 0.396 0.018 0.432 0.018v0.072h-0.072v7.812q0 1.332 0.09 2.304t0.414 2.034 0.882 1.746 1.566 1.116 2.376 0.432q2.304 0 3.888 -0.864v-4.356q-1.08 0.756 -2.448 0.756 -1.908 0 -1.908 -2.952v-8.1h1.872q0.324 0 0.954 0.036t0.954 0.036v-4.212h-3.78q0 -2.952 0.108 -3.672h-5.04q0.144 0.864 0.144 1.98v1.692h-2.16v4.212z","GIT":"M2.448 -9.395q0 -5.94 6.552 -8.1v-0.144q-2.412 -1.476 -2.412 -4.536 0 -3.924 2.268 -4.932v-0.144q-2.592 -0.864 -4.302 -3.906t-1.71 -5.958q0 -5.004 3.42 -8.334t8.46 -3.33q3.456 0 6.408 1.692 3.528 0 7.848 -1.692v7.272q-1.296 0.432 -2.844 0.792 0.576 1.548 0.576 3.024 0 4.572 -2.628 7.794t-7.092 4.05q-1.44 0.288 -2.142 0.972t-0.702 2.088q0 1.116 0.81 1.854t2.088 1.152 2.826 0.792 3.096 0.918 2.826 1.35 2.088 2.304 0.81 3.546q0 10.944 -13.068 10.944 -2.484 0 -4.68 -0.45t-4.176 -1.476 -3.15 -2.952 -1.17 -4.59zm7.344 -0.396q0 3.636 6.192 3.636 5.436 0 5.436 -3.78 0 -3.6 -5.94 -3.6 -5.688 0 -5.688 3.744zm0.756 -27.252q0 4.86 4.464 4.86 4.284 0 4.284 -4.932 0 -2.196 -1.08 -3.672t-3.204 -1.476q-4.464 0 -4.464 5.22zm20.952 -22.104q0 -2.124 1.422 -3.708t3.546 -1.584q2.088 0 3.474 1.602t1.386 3.69 -1.404 3.654 -3.456 1.566q-2.088 0 -3.528 -1.566t-1.44 -3.654zm0.936 42.084q0.144 -1.62 0.144 -4.824v-21.924q0 -3.384 -0.144 -4.608h7.992q-0.144 1.188 -0.144 4.464v22.068q0 3.204 0.144 4.824h-7.992zm11.376 -24.516v-6.84h3.456v-2.736q0 -1.944 -0.216 -3.204h8.172q-0.216 1.476 -0.216 5.94h6.156v6.84q-0.54 0 -1.566 -0.072t-1.53 -0.072h-3.06v13.14q0 4.716 3.132 4.716 2.196 0 3.924 -1.188v7.056q-2.556 1.404 -6.264 1.404 -2.232 0 -3.852 -0.72t-2.52 -1.8 -1.422 -2.808 -0.666 -3.312 -0.144 -3.708v-12.636h0.072v-0.144q-0.252 0 -0.684 -0.036t-0.648 -0.036q-0.756 0 -2.124 0.216z","HACKER_NEWS":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm13.248 -35.1l11.628 21.204v15.66h4.824v-15.696l12.348 -21.168h-5.4q-0.756 1.404 -2.286 4.266t-2.448 4.626 -2.142 4.266 -2.16 4.626h-0.108q-0.756 -1.728 -1.602 -3.492t-1.872 -3.798 -1.674 -3.312 -1.944 -3.762 -1.764 -3.42h-5.4z","TENCENT_WEIBO":"M2.052 -43.451q0 -4.284 1.674 -8.172t4.482 -6.696 6.696 -4.464 8.136 -1.656q5.688 0 10.53 2.808t7.65 7.65 2.808 10.53 -2.808 10.512 -7.65 7.632 -10.53 2.808q-2.304 0 -4.716 -0.504 -0.756 -0.18 -1.17 -0.846t-0.234 -1.422q0.18 -0.72 0.828 -1.134t1.404 -0.27q1.836 0.468 3.888 0.468 3.492 0 6.696 -1.368t5.508 -3.672 3.672 -5.508 1.368 -6.696 -1.368 -6.696 -3.672 -5.508 -5.508 -3.672 -6.696 -1.368 -6.696 1.368 -5.508 3.672 -3.672 5.508 -1.368 6.696q0 4.104 1.872 7.848 0.36 0.72 0.126 1.44t-0.918 1.08 -1.422 0.108 -1.098 -0.936q-2.304 -4.428 -2.304 -9.54zm2.034 32.922q0.378 -4.374 1.062 -7.812t1.944 -6.696 2.484 -5.598 2.664 -4.5q2.196 -3.24 4.752 -5.94 -0.576 -1.26 -0.576 -2.772 0 -2.88 2.034 -4.914t4.914 -2.034 4.914 2.034 2.034 4.914 -2.052 4.914 -4.896 2.034q-2.16 0 -3.996 -1.26 -2.232 2.412 -4.14 5.256 -8.892 13.356 -7.272 30.924 0.036 0.792 -0.45 1.386t-1.242 0.666h-0.18q-0.72 0 -1.26 -0.486t-0.612 -1.206q-0.504 -4.536 -0.126 -8.91z","QQ":"M0.648 -18.647q0 -9.9 9.072 -16.776 -0.288 -0.684 -0.288 -1.872 0 -0.72 0.396 -1.764t0.864 -1.62q-0.036 -0.792 0.27 -1.908t0.81 -1.548q0 -5.004 3.33 -10.386t7.83 -7.542q5.004 -2.376 11.664 -2.376 4.788 0 9.576 1.98 1.764 0.756 3.24 1.728t2.556 2.016 1.98 2.448 1.512 2.664 1.17 3.042 0.918 3.222 0.792 3.528l0.036 0.18q1.98 2.988 1.98 5.4 0 0.504 -0.324 1.44t-0.324 1.368l0.054 0.126 0.126 0.18 0.072 0.126q2.772 4.104 4.338 7.722t1.566 7.506q0 1.548 -0.702 3.6t-1.998 2.052q-0.324 0 -0.702 -0.27t-0.684 -0.63 -0.684 -0.936 -0.576 -0.954 -0.486 -0.936 -0.324 -0.63l-0.108 -0.036 -0.18 0.144q-2.124 5.544 -4.752 8.028 0.72 0.72 2.214 1.386t2.484 1.494 1.278 2.34q-0.072 0.144 -0.144 0.576t-0.252 0.648q-2.304 3.492 -10.872 3.492 -1.908 0 -3.978 -0.324t-3.528 -0.72 -3.762 -1.08q-0.54 -0.18 -0.828 -0.252 -0.504 -0.144 -1.656 -0.162t-1.44 -0.054q-1.476 1.62 -4.59 2.34t-6.066 0.72q-1.26 0 -2.484 -0.054t-3.348 -0.324 -3.636 -0.738 -2.682 -1.44 -1.17 -2.304q0 -1.44 0.36 -2.142t1.476 -1.746q0.396 -0.072 1.458 -0.468t1.782 -0.432q0.144 0 0.504 -0.072 0.072 -0.072 0.072 -0.144l-0.072 -0.108q-1.728 -0.396 -3.888 -3.798t-2.628 -5.634l-0.18 -0.108q-0.144 0 -0.432 0.72 -0.648 1.476 -1.962 2.682t-2.79 1.35h-0.036q-0.144 0 -0.216 -0.162t-0.18 -0.198q-0.828 -1.944 -0.828 -3.6z","WEIXIN":"M0 -40.031q0 -6.084 3.51 -11.196t9.504 -8.046 13.086 -2.934q6.336 0 11.97 2.376t9.432 6.57 4.914 9.378q-1.116 -0.144 -2.52 -0.144 -6.084 0 -11.196 2.772t-8.046 7.506 -2.934 10.35q0 2.808 0.828 5.472 -1.26 0.108 -2.448 0.108 -0.936 0 -1.8 -0.054t-1.98 -0.234 -1.602 -0.252 -1.962 -0.378 -1.8 -0.378l-9.108 4.572 2.592 -7.848q-10.44 -7.308 -10.44 -17.64zm13.68 -7.812q0 1.404 1.188 2.322t2.736 0.918q1.476 0 2.376 -0.882t0.9 -2.358 -0.9 -2.376 -2.376 -0.9q-1.548 0 -2.736 0.918t-1.188 2.358zm15.696 24.156q0 -5.148 2.934 -9.504t8.046 -6.894 11.196 -2.538q5.796 0 10.908 2.538t8.19 6.912 3.078 9.486q0 4.212 -2.466 8.046t-6.678 6.966l1.98 6.516 -7.164 -3.924q-5.4 1.332 -7.848 1.332 -6.084 0 -11.196 -2.538t-8.046 -6.894 -2.934 -9.504zm2.592 -24.156q0 1.404 1.188 2.322t2.736 0.918q1.476 0 2.358 -0.882t0.882 -2.358 -0.882 -2.376 -2.358 -0.9q-1.548 0 -2.736 0.918t-1.188 2.358zm9.792 18.252q0 1.008 0.81 1.818t1.782 0.81q1.44 0 2.358 -0.792t0.918 -1.836q0 -1.008 -0.918 -1.8t-2.358 -0.792q-0.972 0 -1.782 0.81t-0.81 1.782zm14.364 0q0 1.008 0.81 1.818t1.782 0.81q1.404 0 2.34 -0.792t0.936 -1.836q0 -1.008 -0.936 -1.8t-2.34 -0.792q-0.972 0 -1.782 0.81t-0.81 1.782z","SEND":"M0 -27.431q-0.072 -1.44 1.152 -2.124l59.904 -34.56q0.54 -0.324 1.152 -0.324 0.72 0 1.296 0.396 1.188 0.864 0.972 2.304l-9.216 55.296q-0.18 1.044 -1.152 1.62 -0.504 0.288 -1.116 0.288 -0.396 0 -0.864 -0.18l-16.308 -6.66 -8.712 10.62q-0.648 0.828 -1.764 0.828 -0.468 0 -0.792 -0.144 -0.684 -0.252 -1.098 -0.846t-0.414 -1.314v-12.564l31.104 -38.124 -38.484 33.3 -14.22 -5.832q-1.332 -0.504 -1.44 -1.98z","SEND_O":"M0 -27.431q-0.108 -1.404 1.152 -2.124l59.904 -34.56q1.26 -0.756 2.448 0.072 1.188 0.864 0.972 2.304l-9.216 55.296q-0.18 1.044 -1.152 1.62 -0.504 0.288 -1.116 0.288 -0.396 0 -0.864 -0.18l-18.972 -7.74 -10.728 11.772q-0.648 0.756 -1.692 0.756 -0.504 0 -0.828 -0.144 -0.684 -0.252 -1.08 -0.846t-0.396 -1.314v-16.272l-16.992 -6.948q-1.332 -0.504 -1.44 -1.98zm7.524 -0.504l12.096 4.932 31.068 -23.004 -17.208 28.692 17.712 7.236 7.956 -47.628z","HISTORY":"M0 -39.095v-16.128q0 -1.512 1.44 -2.124 1.404 -0.612 2.484 0.504l4.68 4.644q3.852 -3.636 8.802 -5.634t10.242 -1.998q5.616 0 10.728 2.196t8.82 5.904 5.904 8.82 2.196 10.728 -2.196 10.728 -5.904 8.82 -8.82 5.904 -10.728 2.196q-6.192 0 -11.772 -2.61t-9.504 -7.362q-0.252 -0.36 -0.234 -0.81t0.306 -0.738l4.932 -4.968q0.36 -0.324 0.9 -0.324 0.576 0.072 0.828 0.432 2.628 3.42 6.444 5.292t8.1 1.872q3.744 0 7.146 -1.458t5.886 -3.942 3.942 -5.886 1.458 -7.146 -1.458 -7.146 -3.942 -5.886 -5.886 -3.942 -7.146 -1.458q-3.528 0 -6.768 1.278t-5.76 3.654l4.932 4.968q1.116 1.08 0.504 2.484 -0.612 1.44 -2.124 1.44h-16.128q-0.936 0 -1.62 -0.684t-0.684 -1.62zm18.432 12.672v-2.304q0 -0.504 0.324 -0.828t0.828 -0.324h8.064v-12.672q0 -0.504 0.324 -0.828t0.828 -0.324h2.304q0.504 0 0.828 0.324t0.324 0.828v16.128q0 0.504 -0.324 0.828t-0.828 0.324h-11.52q-0.504 0 -0.828 -0.324t-0.324 -0.828z","CIRCLE_THIN":"M0 -32.183q0 -7.524 3.708 -13.878t10.062 -10.062 13.878 -3.708 13.878 3.708 10.062 10.062 3.708 13.878 -3.708 13.878 -10.062 10.062 -13.878 3.708 -13.878 -3.708 -10.062 -10.062 -3.708 -13.878zm4.608 0q0 4.68 1.836 8.946t4.914 7.344 7.344 4.914 8.946 1.836 8.946 -1.836 7.344 -4.914 4.914 -7.344 1.836 -8.946 -1.836 -8.946 -4.914 -7.344 -7.344 -4.914 -8.946 -1.836 -8.946 1.836 -7.344 4.914 -4.914 7.344 -1.836 8.946z","HEADER":"M2.232 -57.311q0 -0.936 0.432 -1.728t1.296 -0.792q1.656 0 4.986 0.126t4.986 0.126q1.512 0 4.554 -0.126t4.554 -0.126q0.9 0 1.35 0.792t0.45 1.728q0 1.08 -0.612 1.566t-1.386 0.522 -1.782 0.144 -1.548 0.468q-1.26 0.756 -1.26 5.76l0.036 11.52q0 0.756 0.036 1.152 0.468 0.108 1.404 0.108h25.164q0.9 0 1.368 -0.108 0.036 -0.396 0.036 -1.152l0.036 -11.52q0 -5.004 -1.26 -5.76 -0.648 -0.396 -2.106 -0.45t-2.376 -0.468 -0.918 -1.782q0 -0.936 0.45 -1.728t1.35 -0.792q1.584 0 4.752 0.126t4.752 0.126q1.548 0 4.644 -0.126t4.644 -0.126q0.9 0 1.35 0.792t0.45 1.728q0 1.08 -0.63 1.584t-1.44 0.522 -1.854 0.108 -1.584 0.45q-1.26 0.828 -1.26 5.796l0.036 33.948q0 4.284 1.224 5.04 0.576 0.36 1.656 0.486t1.926 0.162 1.494 0.558 0.648 1.602q0 0.936 -0.432 1.728t-1.296 0.792q-1.584 0 -4.77 -0.126t-4.806 -0.126q-1.584 0 -4.752 0.126t-4.752 0.126q-0.864 0 -1.332 -0.738t-0.468 -1.638q0 -1.116 0.612 -1.656t1.404 -0.612 1.836 -0.252 1.62 -0.54q1.188 -0.756 1.188 -5.04l-0.036 -14.076q0 -0.756 -0.036 -1.116 -0.468 -0.144 -1.8 -0.144h-24.3q-1.368 0 -1.836 0.144 -0.036 0.36 -0.036 1.116l-0.036 13.356q0 5.112 1.332 5.904 0.576 0.36 1.728 0.468t2.052 0.126 1.62 0.54 0.72 1.638q0 0.936 -0.45 1.728t-1.314 0.792q-1.692 0 -5.022 -0.126t-4.986 -0.126q-1.548 0 -4.608 0.126t-4.572 0.126q-0.828 0 -1.278 -0.756t-0.45 -1.62q0 -1.08 0.558 -1.62t1.296 -0.63 1.71 -0.27 1.512 -0.54q1.188 -0.828 1.188 -5.148l-0.036 -2.052v-29.268q0 -0.108 0.018 -0.936t0 -1.314 -0.054 -1.386 -0.126 -1.512 -0.234 -1.314 -0.396 -1.134 -0.576 -0.648q-0.54 -0.36 -1.62 -0.432t-1.908 -0.072 -1.476 -0.504 -0.648 -1.62z","PARAGRAPH":"M0.864 -42.479q0 -5.976 3.168 -10.296 3.168 -4.248 7.524 -5.724 3.996 -1.332 15.012 -1.332h17.244q0.9 0 1.548 0.648t0.648 1.548v2.628q0 1.044 -0.666 2.196t-1.53 1.152q-1.8 0 -1.944 0.036 -0.936 0.216 -1.152 1.116 -0.108 0.396 -0.108 2.304v41.472q0 0.9 -0.648 1.548t-1.548 0.648h-3.888q-0.9 0 -1.548 -0.648t-0.648 -1.548v-43.848h-5.148v43.848q0 0.9 -0.63 1.548t-1.566 0.648h-3.888q-0.936 0 -1.566 -0.648t-0.63 -1.548v-17.856q-5.292 -0.432 -8.82 -2.124 -4.536 -2.088 -6.912 -6.444 -2.304 -4.212 -2.304 -9.324z","SLIDERS":"M0 -35.639v-2.304q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v2.304q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 29.952v-24.192h9.216v24.192q0 0.504 -0.324 0.828t-0.828 0.324h-6.912q-0.504 0 -0.828 -0.324t-0.324 -0.828zm0 -38.016v-14.976q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v14.976h-9.216zm13.824 26.496v-2.304q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v2.304q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 5.76h9.216v5.76q0 0.504 -0.324 0.828t-0.828 0.324h-6.912q-0.504 0 -0.828 -0.324t-0.324 -0.828v-5.76zm0 -13.824v-33.408q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v33.408h-9.216zm13.824 -19.584v-2.304q0 -1.44 1.008 -2.448t2.448 -1.008h11.52q1.44 0 2.448 1.008t1.008 2.448v2.304q0 1.44 -1.008 2.448t-2.448 1.008h-11.52q-1.44 0 -2.448 -1.008t-1.008 -2.448zm4.608 39.168v-33.408h9.216v33.408q0 0.504 -0.324 0.828t-0.828 0.324h-6.912q-0.504 0 -0.828 -0.324t-0.324 -0.828zm0 -47.232v-5.76q0 -0.504 0.324 -0.828t0.828 -0.324h6.912q0.504 0 0.828 0.324t0.324 0.828v5.76h-9.216z","SHARE_ALT":"M0 -32.183q0 -4.788 3.366 -8.154t8.154 -3.366q4.536 0 7.848 3.096l12.96 -6.48q-0.072 -0.792 -0.072 -1.224 0 -4.788 3.366 -8.154t8.154 -3.366 8.154 3.366 3.366 8.154 -3.366 8.154 -8.154 3.366q-4.536 0 -7.848 -3.096l-12.96 6.48q0.072 0.792 0.072 1.224t-0.072 1.224l12.96 6.48q3.312 -3.096 7.848 -3.096 4.788 0 8.154 3.366t3.366 8.154 -3.366 8.154 -8.154 3.366 -8.154 -3.366 -3.366 -8.154q0 -0.432 0.072 -1.224l-12.96 -6.48q-3.312 3.096 -7.848 3.096 -4.788 0 -8.154 -3.366t-3.366 -8.154z","SHARE_ALT_SQUARE":"M0 -14.903v-34.56q0 -4.284 3.042 -7.326t7.326 -3.042h34.56q4.284 0 7.326 3.042t3.042 7.326v34.56q0 4.284 -3.042 7.326t-7.326 3.042h-34.56q-4.284 0 -7.326 -3.042t-3.042 -7.326zm9.216 -17.28q0 3.168 2.25 5.418t5.418 2.25q2.988 0 5.22 -2.052l8.676 4.32q-0.072 0.576 -0.072 0.828 0 3.168 2.268 5.418t5.436 2.25 5.418 -2.25 2.25 -5.418 -2.25 -5.436 -5.418 -2.268q-3.024 0 -5.22 2.088l-8.676 -4.32q0.072 -0.576 0.072 -0.828t-0.072 -0.828l8.676 -4.32q2.196 2.088 5.22 2.088 3.168 0 5.418 -2.268t2.25 -5.436 -2.25 -5.418 -5.418 -2.25 -5.436 2.25 -2.268 5.418q0 0.252 0.072 0.828l-8.676 4.32q-2.232 -2.052 -5.22 -2.052 -3.168 0 -5.418 2.25t-2.25 5.418z","BOMB":"M0 -25.271q0 -5.148 1.998 -9.846t5.4 -8.1 8.1 -5.4 9.846 -1.998q6.552 0 12.348 3.204l2.304 -2.304q0.684 -0.684 1.638 -0.684t1.638 0.684l2.448 2.448 8.748 -8.784 1.656 1.656 -8.784 8.748 2.448 2.448q0.684 0.684 0.684 1.638t-0.684 1.638l-2.304 2.304q3.204 5.796 3.204 12.348 0 5.148 -1.998 9.846t-5.4 8.1 -8.1 5.4 -9.846 1.998 -9.846 -1.998 -8.1 -5.4 -5.4 -8.1 -1.998 -9.846zm6.12 -6.012q0.36 0.864 1.26 1.224 0.468 0.18 0.864 0.18 1.512 0 2.16 -1.44 1.224 -3.024 3.546 -5.346t5.346 -3.546q0.9 -0.396 1.26 -1.26t0 -1.764 -1.224 -1.26 -1.764 0q-3.888 1.584 -6.876 4.572t-4.572 6.876q-0.36 0.9 0 1.764zm43.416 -30.852q0 -0.468 0.324 -0.828 0.36 -0.324 0.828 -0.324t0.828 0.324l3.24 3.276q0.36 0.324 0.36 0.81t-0.36 0.81q-0.36 0.36 -0.792 0.36 -0.468 0 -0.828 -0.36l-3.276 -3.24q-0.324 -0.36 -0.324 -0.828zm5.76 2.304v-3.456q0 -0.504 0.324 -0.828t0.828 -0.324 0.828 0.324 0.324 0.828v3.456q0 0.504 -0.324 0.828t-0.828 0.324 -0.828 -0.324 -0.324 -0.828zm2.484 5.958q0 -0.486 0.36 -0.81 0.324 -0.36 0.81 -0.36t0.81 0.36l3.276 3.24q0.324 0.36 0.324 0.828t-0.324 0.828q-0.396 0.324 -0.828 0.324t-0.828 -0.324l-3.24 -3.276q-0.36 -0.324 -0.36 -0.81zm0 -5.004q0 -0.486 0.36 -0.81l3.24 -3.276q0.36 -0.324 0.828 -0.324t0.828 0.324q0.324 0.36 0.324 0.828t-0.324 0.828l-3.276 3.24q-0.36 0.36 -0.792 0.36 -0.468 0 -0.828 -0.36 -0.36 -0.324 -0.36 -0.81zm0.972 2.502q0 -0.504 0.324 -0.828t0.828 -0.324h3.456q0.504 0 0.828 0.324t0.324 0.828 -0.324 0.828 -0.828 0.324h-3.456q-0.504 0 -0.828 -0.324t-0.324 -0.828z"}
;


window.google = window.google || {};
google.maps = google.maps || {};
(function() {
  
  function getScript(src) {
    document.write('<' + 'script src="' + src + '"' +
                   ' type="text/javascript"><' + '/script>');
  }
  
  var modules = google.maps.modules = {};
  google.maps.__gjsload__ = function(name, text) {
    modules[name] = text;
  };
  
  google.maps.Load = function(apiLoad) {
    delete google.maps.Load;
    apiLoad([0.009999999776482582,[[["http://mt0.googleapis.com/vt?lyrs=m@271000000\u0026src=api\u0026hl=en-US\u0026","http://mt1.googleapis.com/vt?lyrs=m@271000000\u0026src=api\u0026hl=en-US\u0026"],null,null,null,null,"m@271000000",["https://mts0.google.com/vt?lyrs=m@271000000\u0026src=api\u0026hl=en-US\u0026","https://mts1.google.com/vt?lyrs=m@271000000\u0026src=api\u0026hl=en-US\u0026"]],[["http://khm0.googleapis.com/kh?v=156\u0026hl=en-US\u0026","http://khm1.googleapis.com/kh?v=156\u0026hl=en-US\u0026"],null,null,null,1,"156",["https://khms0.google.com/kh?v=156\u0026hl=en-US\u0026","https://khms1.google.com/kh?v=156\u0026hl=en-US\u0026"]],[["http://mt0.googleapis.com/vt?lyrs=h@271000000\u0026src=api\u0026hl=en-US\u0026","http://mt1.googleapis.com/vt?lyrs=h@271000000\u0026src=api\u0026hl=en-US\u0026"],null,null,null,null,"h@271000000",["https://mts0.google.com/vt?lyrs=h@271000000\u0026src=api\u0026hl=en-US\u0026","https://mts1.google.com/vt?lyrs=h@271000000\u0026src=api\u0026hl=en-US\u0026"]],[["http://mt0.googleapis.com/vt?lyrs=t@132,r@271000000\u0026src=api\u0026hl=en-US\u0026","http://mt1.googleapis.com/vt?lyrs=t@132,r@271000000\u0026src=api\u0026hl=en-US\u0026"],null,null,null,null,"t@132,r@271000000",["https://mts0.google.com/vt?lyrs=t@132,r@271000000\u0026src=api\u0026hl=en-US\u0026","https://mts1.google.com/vt?lyrs=t@132,r@271000000\u0026src=api\u0026hl=en-US\u0026"]],null,null,[["http://cbk0.googleapis.com/cbk?","http://cbk1.googleapis.com/cbk?"]],[["http://khm0.googleapis.com/kh?v=84\u0026hl=en-US\u0026","http://khm1.googleapis.com/kh?v=84\u0026hl=en-US\u0026"],null,null,null,null,"84",["https://khms0.google.com/kh?v=84\u0026hl=en-US\u0026","https://khms1.google.com/kh?v=84\u0026hl=en-US\u0026"]],[["http://mt0.googleapis.com/mapslt?hl=en-US\u0026","http://mt1.googleapis.com/mapslt?hl=en-US\u0026"]],[["http://mt0.googleapis.com/mapslt/ft?hl=en-US\u0026","http://mt1.googleapis.com/mapslt/ft?hl=en-US\u0026"]],[["http://mt0.googleapis.com/vt?hl=en-US\u0026","http://mt1.googleapis.com/vt?hl=en-US\u0026"]],[["http://mt0.googleapis.com/mapslt/loom?hl=en-US\u0026","http://mt1.googleapis.com/mapslt/loom?hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt?hl=en-US\u0026","https://mts1.googleapis.com/mapslt?hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt/ft?hl=en-US\u0026","https://mts1.googleapis.com/mapslt/ft?hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt/loom?hl=en-US\u0026","https://mts1.googleapis.com/mapslt/loom?hl=en-US\u0026"]]],["en-US","US",null,0,null,null,"http://maps.gstatic.com/mapfiles/","http://csi.gstatic.com","https://maps.googleapis.com","http://maps.googleapis.com",null,"https://maps.google.com"],["http://maps.gstatic.com/maps-api-v3/api/js/15/21","3.15.21"],[2089872029],1,null,null,null,null,null,"",["geometry"],null,0,"http://khm.googleapis.com/mz?v=156\u0026",null,"https://earthbuilder.googleapis.com","https://earthbuilder.googleapis.com",null,"http://mt.googleapis.com/vt/icon",[["http://mt0.googleapis.com/vt","http://mt1.googleapis.com/vt"],["https://mts0.googleapis.com/vt","https://mts1.googleapis.com/vt"],[null,[[0,"m",271000000]],[null,"en-US","US",null,18,null,null,null,null,null,null,[[47],[37,[["smartmaps"]]]]],0],[null,[[0,"m",271000000]],[null,"en-US","US",null,18,null,null,null,null,null,null,[[47],[37,[["smartmaps"]]]]],3],[null,[[0,"m",271000000]],[null,"en-US","US",null,18,null,null,null,null,null,null,[[50],[37,[["smartmaps"]]]]],0],[null,[[0,"m",271000000]],[null,"en-US","US",null,18,null,null,null,null,null,null,[[50],[37,[["smartmaps"]]]]],3],[null,[[4,"t",132],[0,"r",132000000]],[null,"en-US","US",null,18,null,null,null,null,null,null,[[63],[37,[["smartmaps"]]]]],0],[null,[[4,"t",132],[0,"r",132000000]],[null,"en-US","US",null,18,null,null,null,null,null,null,[[63],[37,[["smartmaps"]]]]],3],[null,null,[null,"en-US","US",null,18],0],[null,null,[null,"en-US","US",null,18],3],[null,null,[null,"en-US","US",null,18],6],[null,null,[null,"en-US","US",null,18],0],["https://mts0.google.com/vt","https://mts1.google.com/vt"],"/maps/vt",271000000,132],2,500,["http://geo0.ggpht.com/cbk","http://www.gstatic.com/landmark/tour","http://www.gstatic.com/landmark/config","/maps/preview/reveal?authuser=0","/maps/preview/log204","/gen204?tbm=map","http://static.panoramio.com.storage.googleapis.com/photos/"],["https://www.google.com/maps/api/js/widget?pb=!1m2!1u15!2s21!2sen-US!3sUS","https://www.google.com/maps/api/js/slave_widget?pb=!1m2!1u15!2s21"],0], loadScriptTime);
  };
  var loadScriptTime = (new Date).getTime();
//  getScript("http://maps.gstatic.com/cat_js/maps-api-v3/api/js/15/21/%7Bmain,geometry%7D.js");
})();
$(document).on('page:change', function(){
  $("#user_fireman").on("change", function(){
    var $this = $(this);
    var selected_option = $this.find("option:selected");
    var fireman_id = selected_option.val();
    var data_name = $this.attr("data-select-table-name");
    if(!_.isEmpty(fireman_id)){
      if($('input[type="hidden"][name="'+data_name+'"][value="'+fireman_id+'"]').length == 0){
        var wrapper = $($this.attr("data-select-table-wrapper"));
        var text = selected_option.text();
        ar = text.split(' - ')
        var fireman = { id: fireman_id, text: text, code: ar[0], name: ar[1],
         division_id: selected_option.attr("data-hidden-attribute-division-id") };
        var newTr = 
          "<tr data-select-table-removable-id='"+fireman_id+"'>"+
            "<td>"+fireman.code+"<input type='hidden' name='"+data_name+"' value='"+fireman_id+"' /></td>"+
            "<td>"+fireman.name+"</td>"+
            "<td class='action'>"+
              "<a href='#' data-fireman='"+JSON.stringify(fireman)+"' data-no-turbolink='true' data-select-table-removable-id='"+
              fireman_id+"'><span class='glyphicon glyphicon-trash'></span></a>"+
            "</td>"+
          "</tr>";
        newTr = $(newTr).hide();
        wrapper.append(newTr);
        newTr.fadeIn();
        $this.trigger("select_table.fireman.add", fireman);
      }
      selected_option.remove();
      var thisCombobox = $this.combobox("refresh").data('combobox').clearElement();
    } 
  });
  $(document).on('click', 'a[data-fireman]', function(){
    var fireman  = JSON.parse($(this).attr('data-fireman'));
    var select = $("#user_fireman");
    var selected_division = $("#division").find("option:selected").val();
    if(select.find("option[value='"+fireman.id+"']").length == 0 && selected_division == fireman.division_id){
      select.append("<option value='"+fireman.id+"' data-hidden-attribute-division-id='"+fireman.division_id+"'>"+fireman.text+"</option>")
        .combobox("refresh").data("combobox").refresh();
      $(this).trigger("select_table.fireman.remove", fireman);
    }
  });
});
var ready = function(){
  $.ajaxSetup({ cache: false });
  $(document).on('click', "a#add_guard_group", function(){
    var $this = $(this);
    $.ajaxSetup({ cache: false });
    $.getScript($this.attr('data-link')+'?group_id='+$('select#guard_group_select').find('option:selected').val(), false);
    $.ajaxSetup({ cache: false });
    // $.getScript($this.attr('data-link')+'?group_id='+$('select#guard_group_select').find('option:selected').val(), false);
  });
  $(document).on('change', "#add_single_firemen_to_guards", function(){
    var $this = $(this);  
    $.ajaxSetup({ cache: false });
    var val = $this.find("option:selected").val();
    if(!_.isEmpty(val)){
      $.getScript($this.attr('data-link')+'?fireman_id='+val, false); 
      // $.getScript($this.attr('data-link')+'?fireman_id='+val, false); 
    }
  })

  var nowTemp = new Date();
  var five_years_ago = new Date(nowTemp.getFullYear()-5, nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
   
  var startedAt = $('#criteria_started_at').datepicker({
    onRender: function(date) {
      return date.valueOf() < five_years_ago.valueOf() ? 'disabled' : '';
    }
  }).on('changeDate', function(ev) {
    if (ev.date.valueOf() > closedAt.date.valueOf()) {
      var newDate = new Date(ev.date)
      newDate.setDate(newDate.getDate());
      closedAt.setValue(newDate);
    }
    startedAt.hide();
    $('#criteria_closed_at')[0].focus();
  }).data('datepicker');
  var closedAt = $('#criteria_closed_at').datepicker({
    onRender: function(date) {
      return date.valueOf() < startedAt.date.valueOf() ? 'disabled' : '';
    }
  }).on('changeDate', function(ev) {
    closedAt.hide();
  }).data('datepicker');

};

$(document).on('page:change', ready);
(function() {


}).call(this);
;(function ($, window, document, undefined) {

    var pluginName = "metisMenu",
        defaults = {
            toggle: true
        };
        
    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {
        init: function () {

            var $this = $(this.element),
                $toggle = this.settings.toggle;

            $this.find('li.active').has('ul').children('ul').addClass('collapse in');
            $this.find('li').not('.active').has('ul').children('ul').addClass('collapse');

            $this.find('li').has('ul').children('a').on('click', function (e) {
                e.preventDefault();

                $(this).parent('li').toggleClass('active').children('ul').collapse('toggle');

                if ($toggle) {
                    $(this).parent('li').siblings().removeClass('active').children('ul.in').collapse('hide');
                }
            });
        }
    };

    $.fn[ pluginName ] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('5 J(b,a){b.18().V(J,o.n.32);4.G=b;4.2F=b.18().2r();4.P=a;4.B=s;4.q=s;4.X=s;4.1j=t;4.L(b.z())}J.6.2E=5(){7 d=4;7 g;7 f;4.q=3C.3x("2a");4.q.5m=4.2F;9(4.1j){4.27()}4.4I().4F.4A(4.q);o.n.v.1L(4.z(),"4i",5(){f=g});o.n.v.1E(4.q,"44",5(){g=K;f=t});o.n.v.1E(4.q,"2N",5(e){g=t;9(!f){7 c;7 b;7 a=d.G.18();o.n.v.W(a,"2N",d.G);o.n.v.W(a,"40",d.G);9(a.2C()){b=a.1v();c=d.G.1x();a.z().1V(c);1O(5(){a.z().1V(c);9(b!==s&&(a.z().1a()>b)){a.z().3O(b+1)}},3F)}e.3B=K;9(e.2n){e.2n()}}});o.n.v.1E(4.q,"2f",5(){7 a=d.G.18();o.n.v.W(a,"2f",d.G)});o.n.v.1E(4.q,"2B",5(){7 a=d.G.18();o.n.v.W(a,"2B",d.G)})};J.6.2G=5(){9(4.q&&4.q.2T){4.1N();o.n.v.5d(4.q);4.q.2T.55(4.q);4.q=s}};J.6.3f=5(){9(4.1j){7 a=4.28(4.B);4.q.13.1F=a.y+"A";4.q.13.1M=a.x+"A"}};J.6.1N=5(){9(4.q){4.q.13.38="2W"}4.1j=t};J.6.27=5(){9(4.q){7 a=4.28(4.B);4.q.13.4r=4.2V(a);9(4.G.12){4.q.2S="<4d 4a=\'"+4.2c+"\'><2a 13=\'21: 2Q; 1F: 2P; 1M: 2P; 1c: "+4.Z+"A;\'>"+4.X.1d+"</2a>"}C{4.q.2S=4.X.1d}9(1o 4.X.15==="1f"||4.X.15===""){4.q.15=4.G.18().2K()}C{4.q.15=4.X.15}4.q.13.38=""}4.1j=K};J.6.2I=5(a){4.X=a;7 b=w.3Y(0,a.2D-1);b=w.1X(4.P.p-1,b);7 c=4.P[b];4.2c=c.1W;4.Q=c.14;4.Z=c.1c;4.I=c.3T;4.1P=c.3Q||[1Q(4.Q/2,10),1Q(4.Z/2,10)];4.2l=c.3H||"3E";4.2i=c.3A||11;4.2h=c.3z||"2W";4.2g=c.3w||"3u";4.2A=c.3r||"3o";4.2U=c.3l||"5l,5h-5c";4.3j=c.54||"0 0"};J.6.3g=5(a){4.B=a};J.6.2V=5(b){7 a=[];9(!4.G.12){a.F(\'3e-4S:1W(\'+4.2c+\');\');a.F(\'3e-21:\'+4.3j+\';\')}9(1o 4.I===\'4P\'){9(1o 4.I[0]===\'3b\'&&4.I[0]>0&&4.I[0]<4.Q){a.F(\'14:\'+(4.Q-4.I[0])+\'A; 3a-1F:\'+4.I[0]+\'A;\')}C{a.F(\'14:\'+4.Q+\'A; 37-14:\'+4.Q+\'A;\')}9(1o 4.I[1]===\'3b\'&&4.I[1]>0&&4.I[1]<4.Z){a.F(\'1c:\'+(4.Z-4.I[1])+\'A; 3a-1M:\'+4.I[1]+\'A;\')}C{a.F(\'1c:\'+4.Z+\'A; 1d-35:1e;\')}}C{a.F(\'14:\'+4.Q+\'A; 37-14:\'+4.Q+\'A; 1c:\'+4.Z+\'A; 1d-35:1e;\')}a.F(\'4G:4E; 1F:\'+b.y+\'A; 1M:\'+b.x+\'A; 4C:\'+4.2l+\'; 21:2Q; 1I-1q:\'+4.2i+\'A; 1I-4z:\'+4.2U+\'; 1I-4y:\'+4.2g+\'; 1I-13:\'+4.2A+\'; 1d-4x:\'+4.2h+\';\');j a.4v("")};J.6.28=5(b){7 a=4.3d().22(b);a.x-=4.1P[1];a.y-=4.1P[0];j a};5 E(a){4.17=a;4.O=a.z();4.T=a.3h();4.Y=a.2R();4.1g=a.3k();4.12=a.3i();4.k=[];4.B=s;4.2b=s;4.16=H J(4,a.20())}E.6.41=5(){j 4.k.p};E.6.1D=5(){j 4.k};E.6.2O=5(){j 4.B};E.6.z=5(){j 4.O};E.6.18=5(){j 4.17};E.6.1x=5(){7 i;7 b=H o.n.1p(4.B,4.B);7 a=4.1D();u(i=0;i<a.p;i++){b.V(a[i].S())}j b};E.6.1C=5(){4.16.L(s);4.k=[];1Z 4.k};E.6.1B=5(e){7 i;7 c;7 b;9(4.2M(e)){j t}9(!4.B){4.B=e.S();4.1Y()}C{9(4.1g){7 l=4.k.p+1;7 a=(4.B.U()*(l-1)+e.S().U())/l;7 d=(4.B.19()*(l-1)+e.S().19())/l;4.B=H o.n.1n(a,d);4.1Y()}}e.1m=K;4.k.F(e);c=4.k.p;b=4.17.1v();9(b!==s&&4.O.1a()>b){9(e.z()!==4.O){e.L(4.O)}}C 9(c<4.Y){9(e.z()!==4.O){e.L(4.O)}}C 9(c===4.Y){u(i=0;i<c;i++){4.k[i].L(s)}}C{e.L(s)}4.2L();j K};E.6.2J=5(a){j 4.2b.34(a.S())};E.6.1Y=5(){7 a=H o.n.1p(4.B,4.B);4.2b=4.17.2e(a)};E.6.2L=5(){7 c=4.k.p;7 a=4.17.1v();9(a!==s&&4.O.1a()>a){4.16.1N();j}9(c<4.Y){4.16.1N();j}7 b=4.17.20().p;7 d=4.17.36()(4.k,b);4.16.3g(4.B);4.16.2I(d);4.16.27()};E.6.2M=5(a){7 i;9(4.k.1l){j 4.k.1l(a)!==-1}C{u(i=0;i<4.k.p;i++){9(a===4.k[i]){j K}}}j t};5 8(a,c,b){4.V(8,o.n.32);c=c||[];b=b||{};4.k=[];4.D=[];4.1r=[];4.1A=s;4.1s=t;4.T=b.3X||3W;4.Y=b.3V||2;4.1R=b.2z||s;4.P=b.3U||[];4.29=b.15||"";4.1K=K;9(b.2y!==1f){4.1K=b.2y}4.1g=t;9(b.2x!==1f){4.1g=b.2x}4.1b=t;9(b.2v!==1f){4.1b=b.2v}4.12=t;9(b.2u!==1f){4.12=b.2u}4.1z=b.3S||8.2s;4.1u=b.3R||8.2p;4.1t=b.3P||8.2o;4.1U=b.3J||8.2m;4.1S=b.3I||8.2k;4.1y=b.3G||8.2j;4.1T=b.3D||"N";9(3K.3L.3M().1l("3N")!==-1){4.1S=4.1y}4.2H();4.2w(c,K);4.L(a)}8.6.2E=5(){7 a=4;4.1A=4.z();4.1s=K;4.1h();4.1r=[o.n.v.1L(4.z(),"3y",5(){a.1w(t);9(4.1a()===(4.2q("3v")||0)||4.1a()===4.2q("2z")){o.n.v.W(4,"2t")}}),o.n.v.1L(4.z(),"2t",5(){a.1k()})]};8.6.2G=5(){7 i;u(i=0;i<4.k.p;i++){4.k[i].L(4.1A)}u(i=0;i<4.D.p;i++){4.D[i].1C()}4.D=[];u(i=0;i<4.1r.p;i++){o.n.v.3t(4.1r[i])}4.1r=[];4.1A=s;4.1s=t};8.6.3f=5(){};8.6.2H=5(){7 i,1q;9(4.P.p>0){j}u(i=0;i<4.1t.p;i++){1q=4.1t[i];4.P.F({1W:4.1z+(i+1)+"."+4.1u,14:1q,1c:1q})}};8.6.3s=5(){7 i;7 a=4.1D();7 b=H o.n.1p();u(i=0;i<a.p;i++){b.V(a[i].S())}4.z().1V(b)};8.6.3h=5(){j 4.T};8.6.3Z=5(a){4.T=a};8.6.2R=5(){j 4.Y};8.6.3q=5(a){4.Y=a};8.6.1v=5(){j 4.1R};8.6.3p=5(a){4.1R=a};8.6.20=5(){j 4.P};8.6.42=5(a){4.P=a};8.6.2K=5(){j 4.29};8.6.43=5(a){4.29=a};8.6.2C=5(){j 4.1K};8.6.3n=5(a){4.1K=a};8.6.3k=5(){j 4.1g};8.6.3m=5(a){4.1g=a};8.6.46=5(){j 4.1b};8.6.47=5(a){4.1b=a};8.6.5k=5(){j 4.1u};8.6.5i=5(a){4.1u=a};8.6.5g=5(){j 4.1z};8.6.5f=5(a){4.1z=a};8.6.5b=5(){j 4.1t};8.6.5a=5(a){4.1t=a};8.6.36=5(){j 4.1U};8.6.59=5(a){4.1U=a};8.6.3i=5(){j 4.12};8.6.57=5(a){4.12=a};8.6.51=5(){j 4.1y};8.6.50=5(a){4.1y=a};8.6.2r=5(){j 4.1T};8.6.4Z=5(a){4.1T=a};8.6.1D=5(){j 4.k};8.6.4X=5(){j 4.k.p};8.6.4V=5(){j 4.D};8.6.4U=5(){j 4.D.p};8.6.1B=5(b,a){4.2d(b);9(!a){4.1k()}};8.6.2w=5(b,a){7 i;u(i=0;i<b.p;i++){4.2d(b[i])}9(!a){4.1k()}};8.6.2d=5(b){9(b.4T()){7 a=4;o.n.v.1L(b,"4R",5(){9(a.1s){4.1m=t;a.1h()}})}b.1m=t;4.k.F(b)};8.6.4Q=5(c,a){7 b=4.26(c);9(!a&&b){4.1h()}j b};8.6.4O=5(a,c){7 i,r;7 b=t;u(i=0;i<a.p;i++){r=4.26(a[i]);b=b||r}9(!c&&b){4.1h()}j b};8.6.26=5(b){7 i;7 a=-1;9(4.k.1l){a=4.k.1l(b)}C{u(i=0;i<4.k.p;i++){9(b===4.k[i]){a=i;4N}}}9(a===-1){j t}b.L(s);4.k.4K(a,1);j K};8.6.4J=5(){4.1w(K);4.k=[]};8.6.1h=5(){7 a=4.D.4H();4.D=[];4.1w(t);4.1k();1O(5(){7 i;u(i=0;i<a.p;i++){a[i].1C()}},0)};8.6.2e=5(d){7 f=4.3d();7 c=H o.n.1n(d.23().U(),d.23().19());7 a=H o.n.1n(d.24().U(),d.24().19());7 e=f.22(c);e.x+=4.T;e.y-=4.T;7 g=f.22(a);g.x-=4.T;g.y+=4.T;7 b=f.2X(e);7 h=f.2X(g);d.V(b);d.V(h);j d};8.6.1k=5(){4.25(0)};8.6.1w=5(a){7 i,M;u(i=0;i<4.D.p;i++){4.D[i].1C()}4.D=[];u(i=0;i<4.k.p;i++){M=4.k[i];M.1m=t;9(a){M.L(s)}}};8.6.33=5(b,e){7 R=4D;7 g=(e.U()-b.U())*w.1H/1G;7 f=(e.19()-b.19())*w.1H/1G;7 a=w.1J(g/2)*w.1J(g/2)+w.31(b.U()*w.1H/1G)*w.31(e.U()*w.1H/1G)*w.1J(f/2)*w.1J(f/2);7 c=2*w.4B(w.30(a),w.30(1-a));7 d=R*c;j d};8.6.2Z=5(b,a){j a.34(b.S())};8.6.39=5(c){7 i,d,N,1e;7 a=4L;7 b=s;u(i=0;i<4.D.p;i++){N=4.D[i];1e=N.2O();9(1e){d=4.33(1e,c.S());9(d<a){a=d;b=N}}}9(b&&b.2J(c)){b.1B(c)}C{N=H E(4);N.1B(c);4.D.F(N)}};8.6.25=5(e){7 i,M;7 d;7 c=4;9(!4.1s){j}9(e===0){o.n.v.W(4,"4M",4);9(1o 4.1i!=="1f"){4w(4.1i);1Z 4.1i}}9(4.z().1a()>3){d=H o.n.1p(4.z().1x().24(),4.z().1x().23())}C{d=H o.n.1p(H o.n.1n(3c.4u,-2Y.4t),H o.n.1n(-3c.4s,2Y.4q))}7 a=4.2e(d);7 b=w.1X(e+4.1S,4.k.p);u(i=e;i<b;i++){M=4.k[i];9(!M.1m&&4.2Z(M,a)){9(!4.1b||(4.1b&&M.4W())){4.39(M)}}}9(b<4.k.p){4.1i=1O(5(){c.25(b)},0)}C{1Z 4.1i;o.n.v.W(4,"4p",4)}};8.6.V=5(d,c){j(5(b){7 a;u(a 4Y b.6){4.6[a]=b.6[a]}j 4}).4o(d,[c])};8.2m=5(a,c){7 f=0;7 b="";7 d=a.p.4n();7 e=d;4m(e!==0){e=1Q(e/10,10);f++}f=w.1X(f,c);j{1d:d,2D:f,15:b}};8.2k=52;8.2j=4l;8.2s="4k://o-n-4j-58-4h.4g.4f/4e/4c/5e/4b/m";8.2p="49";8.2o=[53,56,5j,48,45];',62,333,'||||this|function|prototype|var|MarkerClusterer|if||||||||||return|markers_|||maps|google|length|div_||null|false|for|event|Math|||getMap|px|center_|else|clusters_|Cluster|push|cluster_|new|anchor_|ClusterIcon|true|setMap|marker|cluster|map_|styles_|height_||getPosition|gridSize_|lat|extend|trigger|sums_|minClusterSize_|width_|||printable_|style|height|title|clusterIcon_|markerClusterer_|getMarkerClusterer|lng|getZoom|ignoreHidden_|width|text|center|undefined|averageCenter_|repaint|timerRefStatic|visible_|redraw_|indexOf|isAdded|LatLng|typeof|LatLngBounds|size|listeners_|ready_|imageSizes_|imageExtension_|getMaxZoom|resetViewport_|getBounds|batchSizeIE_|imagePath_|activeMap_|addMarker|remove|getMarkers|addDomListener|top|180|PI|font|sin|zoomOnClick_|addListener|left|hide|setTimeout|anchorIcon_|parseInt|maxZoom_|batchSize_|clusterClass_|calculator_|fitBounds|url|min|calculateBounds_|delete|getStyles|position|fromLatLngToDivPixel|getNorthEast|getSouthWest|createClusters_|removeMarker_|show|getPosFromLatLng_|title_|div|bounds_|url_|pushMarkerTo_|getExtendedBounds|mouseover|fontWeight_|textDecoration_|textSize_|BATCH_SIZE_IE|BATCH_SIZE|textColor_|CALCULATOR|stopPropagation|IMAGE_SIZES|IMAGE_EXTENSION|get|getClusterClass|IMAGE_PATH|idle|printable|ignoreHidden|addMarkers|averageCenter|zoomOnClick|maxZoom|fontStyle_|mouseout|getZoomOnClick|index|onAdd|className_|onRemove|setupStyles_|useStyle|isMarkerInClusterBounds|getTitle|updateIcon_|isMarkerAlreadyAdded_|click|getCenter|0px|absolute|getMinimumClusterSize|innerHTML|parentNode|fontFamily_|createCss|none|fromDivPixelToLatLng|178|isMarkerInBounds_|sqrt|cos|OverlayView|distanceBetweenPoints_|contains|align|getCalculator|line|display|addToClosestCluster_|padding|number|85|getProjection|background|draw|setCenter|getGridSize|getPrintable|backgroundPosition_|getAverageCenter|fontFamily|setAverageCenter|setZoomOnClick|normal|setMaxZoom|setMinimumClusterSize|fontStyle|fitMapToMarkers|removeListener|bold|minZoom|fontWeight|createElement|zoom_changed|textDecoration|textSize|cancelBubble|document|clusterClass|black|100|batchSizeIE|textColor|batchSize|calculator|navigator|userAgent|toLowerCase|msie|setZoom|imageSizes|anchorIcon|imageExtension|imagePath|anchor|styles|minimumClusterSize|60|gridSize|max|setGridSize|clusterclick|getSize|setStyles|setTitle|mousedown|90|getIgnoreHidden|setIgnoreHidden|78|png|src|images|trunk|img|svn|com|googlecode|v3|bounds_changed|utility|http|500|while|toString|apply|clusteringend|00048865625|cssText|08136444384544|48388434375|02070771743472|join|clearTimeout|decoration|weight|family|appendChild|atan2|color|6371|pointer|overlayMouseTarget|cursor|slice|getPanes|clearMarkers|splice|40000|clusteringbegin|break|removeMarkers|object|removeMarker|dragend|image|getDraggable|getTotalClusters|getClusters|getVisible|getTotalMarkers|in|setClusterClass|setBatchSizeIE|getBatchSizeIE|2000||backgroundPosition|removeChild||setPrintable|library|setCalculator|setImageSizes|getImageSizes|serif|clearInstanceListeners|markerclustererplus|setImagePath|getImagePath|sans|setImageExtension|66|getImageExtension|Arial|className'.split('|'),0,{}))
;
(function() {


}).call(this);
(function() {


}).call(this);
$(function() {

    $('#side-menu').metisMenu();

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
$(function() {
    $(window).bind("load resize", function() {
        if ($(this).width() < 768) {
            $('div.sidebar-collapse').addClass('collapse')
        } else {
            $('div.sidebar-collapse').removeClass('collapse')
        }
    })
})
;
var side_menu = function(){
  $('.sidebar-collapse').metisMenu();
  //Loads the correct sidebar on window load,
  //collapses the sidebar on window resize.
  $(function() {
      $(window).bind("load resize", function() {
          width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
          if (width < 768) {
              $('div.sidebar-collapse').addClass('collapse')
          } else {
              $('div.sidebar-collapse').removeClass('collapse')
          }
      })
  })
}
$(document).on("page:load", side_menu);
$(document).ready(side_menu);
  var animationDuration = 1500;
  $(document).on('change', "#place_select", function(){
    var $this = $(this);  
    $.ajaxSetup({ cache: false });
    var val = $this.find("option:selected").val();
    if(!_.isEmpty(val)){
      $.getScript($this.attr('data-link')+toParams({place_id: val}), false); 
      $("#"+$this.attr('data-id')).slideDown(animationDuration);
    }else{
      $("#"+$this.attr('data-id')).slideUp(animationDuration);
    }
  });
  
  function getIdsOnList($this){
    var list = $($this.attr("data-list")).find("input[type='hidden']");
    return JSON.stringify(_.map(list, function(el){ return $(el).val() }))
  }

  function onChangeRemoteSelect(subject, idAttribute){
    var $this = $(subject);
    $.ajaxSetup({ cache: false });
    var id = $this.find("option:selected").val();
    params = {};
    params[idAttribute] = id;
    if(!_.isEmpty(id))
      params['ids_on_list'] = getIdsOnList($this);
      $.getScript($this.attr('data-link')+toParams(params), false); 
  }

  $(document).on('change', '#fireman_select',function(){
    onChangeRemoteSelect(this, "fireman_id");
  });
  $(document).on('change', '#vehicle_select',function(){
    onChangeRemoteSelect(this, "vehicle_id");
  });

  $(document).on("keydown", 
    $("[name='transference[place_id]']").parent().find("input.combobox"), function(){
    var placeSection = $("#"+($("#place_select").attr('data-id')));
    if( _.isEmpty($(this).val()) ){
      placeSection.slideUp(animationDuration);
    }else{
      placeSection.slideDown(animationDuration);
    }
  });

  // remove element from list logic
  $(document).on("click", "a[data-transference-remove]", function(){
    var $this = $(this);
    var kind = $this.attr("data-transference-remove");
    var link = $this.attr("data-link");
    var params = {
      ids_on_list: getIdsOnList($this)
    }
    params[kind+'_id'] = $this.attr("data-id");
    console.log(kind+" removed: "+params[kind+'_id']+" ids_on_list: "+getIdsOnList($this));
    $.ajaxSetup({ cache: false });
    $.getScript(link+toParams(params), false); 
  });

  $(document).on("page:change", function(){
    var nowTemp = new Date();
    var five_years_ago = new Date(nowTemp.getFullYear()-5, nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    $('#criteria_created_at').datepicker({
      onRender: function(date) {
        return date.valueOf() < five_years_ago.valueOf() ? 'disabled' : '';
      }
    })
  })
;
$(document).on("page:change", function(){
  $(document).on('change','#user_of_fireman',function(){
    if($(this).is(":checked")){
      $("#fireman_data").slideDown();
      $("#non_fireman_data").slideUp();
    }else{
      $("#fireman_data").slideUp();
      $("#non_fireman_data").slideDown();
    }
  });

  function reload_select_options(select, options, cast_option){
    select.empty();
    if(select.hasClass('combobox')) select.data('combobox').clearElement();
    if(!_.isFunction(cast_option)){
      cast_option = function(option){
        return { value: option.id, text: option.name };
      };
    }
    _.each(options, function(option){
        select.append($('<option>', cast_option(option)));
    })
    if(select.hasClass('combobox')) select.combobox("refresh").data('combobox').refresh();
  }

  $(document).on('change','#fire_department',function(){
    var fire_department_id = $(this).val();
    var division_select = $("#division");
    division_select.prop('disabled', true);
    $.getJSON('/divisions/by_fire_department/'+fire_department_id+'.json', function(divisions){
      reload_select_options(division_select, divisions);
      division_select.prop('disabled', false).change();
    })
  });
  $(document).on('change','#division',function(){
    var $this = $(this)
    var division_id = $this.val();
    var firemen_select = $($this.attr("data-fireman-select"));
    firemen_select.prop('disabled', true);
    $.getJSON('/firemen/by_division/'+division_id+'.json', function(firemen){
      reload_select_options(firemen_select, firemen, function(fireman){
        return { value: fireman.id, text: fireman.code+' - '+fireman.person.name+' '+fireman.person.last_name, "data-hidden-attribute-division-id": fireman.division_id }
      });
      firemen_select.prop('disabled', false);
    })
  });
});
(function() {


}).call(this);
// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//











//= fontawesome-markers


// The following files could not be used becouse every request made by them
// returned Forbbbiden
// require google_maps
// markercluster_packed

// preventing double form submissions
// to allow double submission add the class allow-double-submission to the form
$(document).on('submit', 'form:not(.allow-double-submission)', function(eventObject){
  var form = $(this);

  if(form.data('submitted')) {
    console.error("Double submission of form is not allowed.");
    eventObject.preventDefault();
  } else {
    form.data('submitted', true);
  }
});

// clear the clearable input and submit the form
$(document).on('click', 'a.clear-btn', function(eventObject) {
  eventObject.preventDefault();

  var form = $(this).closest('form')
  form.find('input.clearable').val('');
  form.submit();
});


// data-toggle-check, toggle element's visibility by toggling check
$(document).on("change","input[data-toggle-check][type='checkbox']",function(){
  $this = $(this);
  if($this.is(":checked")){
    $($this.attr("data-toggle-check")).slideDown();
  }else{
    $($this.attr("data-toggle-check")).slideUp();
  }
});

// remove elements from table with link
$(document).on("click","a[data-select-table-removable-id]", function(){
  var removable = $("tr[data-select-table-removable-id='"+$(this).attr('data-select-table-removable-id')+"']");
  removable.fadeOut({complete: function(){ $(this).remove(); } })
});
$(document).on("click", ".print-report", function(){
  var url = $(location).attr('href').split("#");
  var main_url = url[0];
  var report_url = [main_url, "as_report=true"];
  var joiner = main_url.indexOf("?") > 0 ? '&' : '?';
  var full_url = [report_url.join(joiner), '#', url.slice(1, url.length).join('#')].join('');
  window.open(full_url, '', '');
});
function init(){

    // Client side validations + Turbolinks fix
    $('form[data-validate]').validate();
    // Fix ugly file input
    $('input[type=file]').bootstrapFileInput();
    // Cast any select with combobox css class to a bootstrap-combobox
    $("select.combobox").combobox().combobox("refresh");
    $('[role="iconpicker"]').iconpicker();


}
$(document).on('page:change', init);
//$(document).on('nested:fieldAdded', init);

function toParams(obj){
  var params = _.map(obj, function(v, k){ return k+'='+v; });
  return '?'+(params.join('&'))
}



;
