json.array!(@fire_departments) do |fire_department|
  json.extract! fire_department, :id, :name
  json.url fire_department_url(fire_department, format: :json)
end
