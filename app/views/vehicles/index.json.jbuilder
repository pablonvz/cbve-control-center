json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :code, :division_id
  json.url vehicle_url(vehicle, format: :json)
end
