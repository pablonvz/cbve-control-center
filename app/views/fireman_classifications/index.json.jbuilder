json.array!(@fireman_classifications) do |fireman_classification|
  json.extract! fireman_classification, :id, :name, :description
  json.url fireman_classification_url(fireman_classification, format: :json)
end
