json.array!(@fireman_hierarchies) do |fireman_hierarchy|
  json.extract! fireman_hierarchy, :id, :name, :description, :superior_id
  json.url fireman_hierarchy_url(fireman_hierarchy, format: :json)
end
