json.array!(@notes) do |note|
  json.extract! note, :id, :type, :description, :dispatch_id
  json.url note_url(note, format: :json)
end
