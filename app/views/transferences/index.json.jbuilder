json.array!(@transferences) do |transference|
  json.extract! transference, :id, :reason, :place_id
  json.url transference_url(transference, format: :json)
end
