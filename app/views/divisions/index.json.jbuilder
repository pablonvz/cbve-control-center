json.array!(@divisions) do |division|
  json.extract! division, :id, :name, :address_id, :fire_department_id
  json.url division_url(division, format: :json)
end
