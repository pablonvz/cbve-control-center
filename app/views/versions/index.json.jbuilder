json.array!(@versions) do |version|
  json.extract! version, :id, :item_type, :terminator
  json.url version_url(version, format: :json)
end
