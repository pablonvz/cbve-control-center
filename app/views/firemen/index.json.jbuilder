json.array!(@firemen) do |fireman|
  json.extract! fireman, :id, :division_id, :fireman_hierarchy_id, :fireman_classification_id, :person_id
  json.url fireman_url(fireman, format: :json)
end
