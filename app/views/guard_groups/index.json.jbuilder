json.array!(@guard_groups) do |guard_group|
  json.extract! guard_group, :id, :name, :description
  json.url guard_group_url(guard_group, format: :json)
end
