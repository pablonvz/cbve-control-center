$(document).on('page:change', function(){
  $("#user_fireman").on("change", function(){
    var $this = $(this);
    var selected_option = $this.find("option:selected");
    var fireman_id = selected_option.val();
    var data_name = $this.attr("data-select-table-name");
    if(!_.isEmpty(fireman_id)){
      if($('input[type="hidden"][name="'+data_name+'"][value="'+fireman_id+'"]').length == 0){
        var wrapper = $($this.attr("data-select-table-wrapper"));
        var text = selected_option.text();
        ar = text.split(' - ')
        var fireman = { id: fireman_id, text: text, code: ar[0], name: ar[1],
         division_id: selected_option.attr("data-hidden-attribute-division-id") };
        var newTr = 
          "<tr data-select-table-removable-id='"+fireman_id+"'>"+
            "<td>"+fireman.code+"<input type='hidden' name='"+data_name+"' value='"+fireman_id+"' /></td>"+
            "<td>"+fireman.name+"</td>"+
            "<td class='action'>"+
              "<a href='#' data-fireman='"+JSON.stringify(fireman)+"' data-no-turbolink='true' data-select-table-removable-id='"+
              fireman_id+"'><span class='glyphicon glyphicon-trash'></span></a>"+
            "</td>"+
          "</tr>";
        newTr = $(newTr).hide();
        wrapper.append(newTr);
        newTr.fadeIn();
        $this.trigger("select_table.fireman.add", fireman);
      }
      selected_option.remove();
      var thisCombobox = $this.combobox("refresh").data('combobox').clearElement();
    } 
  });
  $(document).on('click', 'a[data-fireman]', function(){
    var fireman  = JSON.parse($(this).attr('data-fireman'));
    var select = $("#user_fireman");
    var selected_division = $("#division").find("option:selected").val();
    if(select.find("option[value='"+fireman.id+"']").length == 0 && selected_division == fireman.division_id){
      select.append("<option value='"+fireman.id+"' data-hidden-attribute-division-id='"+fireman.division_id+"'>"+fireman.text+"</option>")
        .combobox("refresh").data("combobox").refresh();
      $(this).trigger("select_table.fireman.remove", fireman);
    }
  });
});