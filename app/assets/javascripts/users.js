$(document).on("page:change", function(){
  $(document).on('change','#user_of_fireman',function(){
    if($(this).is(":checked")){
      $("#fireman_data").slideDown();
      $("#non_fireman_data").slideUp();
    }else{
      $("#fireman_data").slideUp();
      $("#non_fireman_data").slideDown();
    }
  });

  function reload_select_options(select, options, cast_option){
    select.empty();
    if(select.hasClass('combobox')) select.data('combobox').clearElement();
    if(!_.isFunction(cast_option)){
      cast_option = function(option){
        return { value: option.id, text: option.name };
      };
    }
    _.each(options, function(option){
        select.append($('<option>', cast_option(option)));
    })
    if(select.hasClass('combobox')) select.combobox("refresh").data('combobox').refresh();
  }

  $(document).on('change','#fire_department',function(){
    var fire_department_id = $(this).val();
    var division_select = $("#division");
    division_select.prop('disabled', true);
    $.getJSON('/divisions/by_fire_department/'+fire_department_id+'.json', function(divisions){
      reload_select_options(division_select, divisions);
      division_select.prop('disabled', false).change();
    })
  });
  $(document).on('change','#division',function(){
    var $this = $(this)
    var division_id = $this.val();
    var firemen_select = $($this.attr("data-fireman-select"));
    firemen_select.prop('disabled', true);
    $.getJSON('/firemen/by_division/'+division_id+'.json', function(firemen){
      reload_select_options(firemen_select, firemen, function(fireman){
        return { value: fireman.id, text: fireman.code+' - '+fireman.person.name+' '+fireman.person.last_name, "data-hidden-attribute-division-id": fireman.division_id }
      });
      firemen_select.prop('disabled', false);
    })
  });
});