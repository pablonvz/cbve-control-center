  var animationDuration = 1500;
  $(document).on('change', "#place_select", function(){
    var $this = $(this);  
    $.ajaxSetup({ cache: false });
    var val = $this.find("option:selected").val();
    if(!_.isEmpty(val)){
      $.getScript($this.attr('data-link')+toParams({place_id: val}), false); 
      $("#"+$this.attr('data-id')).slideDown(animationDuration);
    }else{
      $("#"+$this.attr('data-id')).slideUp(animationDuration);
    }
  });
  
  function getIdsOnList($this){
    var list = $($this.attr("data-list")).find("input[type='hidden']");
    return JSON.stringify(_.map(list, function(el){ return $(el).val() }))
  }

  function onChangeRemoteSelect(subject, idAttribute){
    var $this = $(subject);
    $.ajaxSetup({ cache: false });
    var id = $this.find("option:selected").val();
    params = {};
    params[idAttribute] = id;
    if(!_.isEmpty(id))
      params['ids_on_list'] = getIdsOnList($this);
      $.getScript($this.attr('data-link')+toParams(params), false); 
  }

  $(document).on('change', '#fireman_select',function(){
    onChangeRemoteSelect(this, "fireman_id");
  });
  $(document).on('change', '#vehicle_select',function(){
    onChangeRemoteSelect(this, "vehicle_id");
  });

  $(document).on("keydown", 
    $("[name='transference[place_id]']").parent().find("input.combobox"), function(){
    var placeSection = $("#"+($("#place_select").attr('data-id')));
    if( _.isEmpty($(this).val()) ){
      placeSection.slideUp(animationDuration);
    }else{
      placeSection.slideDown(animationDuration);
    }
  });

  // remove element from list logic
  $(document).on("click", "a[data-transference-remove]", function(){
    var $this = $(this);
    var kind = $this.attr("data-transference-remove");
    var link = $this.attr("data-link");
    var params = {
      ids_on_list: getIdsOnList($this)
    }
    params[kind+'_id'] = $this.attr("data-id");
    console.log(kind+" removed: "+params[kind+'_id']+" ids_on_list: "+getIdsOnList($this));
    $.ajaxSetup({ cache: false });
    $.getScript(link+toParams(params), false); 
  });

  $(document).on("page:change", function(){
    var nowTemp = new Date();
    var five_years_ago = new Date(nowTemp.getFullYear()-5, nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    $('#criteria_created_at').datepicker({
      onRender: function(date) {
        return date.valueOf() < five_years_ago.valueOf() ? 'disabled' : '';
      }
    })
  })