$(document).on("page:change", function(){

  var nowTemp = new Date();
  var five_years_ago = new Date(nowTemp.getFullYear()-5, nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
   
  var fromDate = $('#criteria_from_date').datepicker({
    onRender: function(date) {
      return date.valueOf() < five_years_ago.valueOf() ? 'disabled' : '';
    }
  }).on('changeDate', function(ev) {
    if (ev.date.valueOf() > closedAt.date.valueOf()) {
      var newDate = new Date(ev.date)
      newDate.setDate(newDate.getDate());
      closedAt.setValue(newDate);
    }
    fromDate.hide();
    $('#criteria_to_date')[0].focus();
  }).data('datepicker');
  var closedAt = $('#criteria_to_date').datepicker({
    onRender: function(date) {
      return date.valueOf() < fromDate.date.valueOf() ? 'disabled' : '';
    }
  }).on('changeDate', function(ev) {
    closedAt.hide();
  }).data('datepicker');

});