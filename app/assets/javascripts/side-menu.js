var side_menu = function(){
  $('.sidebar-collapse').metisMenu();
  //Loads the correct sidebar on window load,
  //collapses the sidebar on window resize.
  $(function() {
      $(window).bind("load resize", function() {
          width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
          if (width < 768) {
              $('div.sidebar-collapse').addClass('collapse')
          } else {
              $('div.sidebar-collapse').removeClass('collapse')
          }
      })
  })
}
$(document).on("page:load", side_menu);
$(document).ready(side_menu);