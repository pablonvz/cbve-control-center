ClientSideValidations.formBuilders['NestedForm::Builder'] =
    ClientSideValidations.formBuilders['ActionView::Helpers::FormBuilder'];
$('form').on('nested:fieldAdded', function(event) {
  $(event.target).find(':input').enableClientSideValidations();
});
/*
// Override NestedForm default behaviour
jQuery(function ($) {
  window.NestedFormEvents.prototype.insertFields = function(content, assoc, link) {
    $link = $(link);
    var data_attr = $link.attr('data-nested-form-wrapper');
    if (_.isString(data_attr)) 
      return $(content).insertBefore($(data_attr));
    if(_.isString($link.attr('data-nested-form-tr')))
      return $(content).insertBefore($link.closest('tr'));
    return $(content).insertBefore($link);
  };
});*/