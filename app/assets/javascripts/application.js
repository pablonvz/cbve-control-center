// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require underscore
//= require gmaps/google
//= require jquery_nested_form
//= require rails.validations
//= require rails.validations.nested_form
//= require bootstrap.file-input
//= require bootstrap-combobox
//= require bootstrap-iconpicker.min
//= fontawesome-markers
//= require_tree .

// The following files could not be used becouse every request made by them
// returned Forbbbiden
// require google_maps
// markercluster_packed

// preventing double form submissions
// to allow double submission add the class allow-double-submission to the form
$(document).on('submit', 'form:not(.allow-double-submission)', function(eventObject){
  var form = $(this);

  if(form.data('submitted')) {
    console.error("Double submission of form is not allowed.");
    eventObject.preventDefault();
  } else {
    form.data('submitted', true);
  }
});

// clear the clearable input and submit the form
$(document).on('click', 'a.clear-btn', function(eventObject) {
  eventObject.preventDefault();

  var form = $(this).closest('form')
  form.find('input.clearable').val('');
  form.submit();
});


// data-toggle-check, toggle element's visibility by toggling check
$(document).on("change","input[data-toggle-check][type='checkbox']",function(){
  $this = $(this);
  if($this.is(":checked")){
    $($this.attr("data-toggle-check")).slideDown();
  }else{
    $($this.attr("data-toggle-check")).slideUp();
  }
});

// remove elements from table with link
$(document).on("click","a[data-select-table-removable-id]", function(){
  var removable = $("tr[data-select-table-removable-id='"+$(this).attr('data-select-table-removable-id')+"']");
  removable.fadeOut({complete: function(){ $(this).remove(); } })
});
$(document).on("click", ".print-report", function(){
  var url = $(location).attr('href').split("#");
  var main_url = url[0];
  var report_url = [main_url, "as_report=true"];
  var joiner = main_url.indexOf("?") > 0 ? '&' : '?';
  var full_url = [report_url.join(joiner), '#', url.slice(1, url.length).join('#')].join('');
  window.open(full_url, '', '');
});
function init(){

    // Client side validations + Turbolinks fix
    $('form[data-validate]').validate();
    // Fix ugly file input
    $('input[type=file]').bootstrapFileInput();
    // Cast any select with combobox css class to a bootstrap-combobox
    $("select.combobox").combobox().combobox("refresh");
    $('[role="iconpicker"]').iconpicker();


}
$(document).on('page:change', init);
//$(document).on('nested:fieldAdded', init);

function toParams(obj){
  var params = _.map(obj, function(v, k){ return k+'='+v; });
  return '?'+(params.join('&'))
}



