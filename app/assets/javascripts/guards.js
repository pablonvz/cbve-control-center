var ready = function(){
  $.ajaxSetup({ cache: false });
  $(document).on('click', "a#add_guard_group", function(){
    var $this = $(this);
    $.ajaxSetup({ cache: false });
    $.getScript($this.attr('data-link')+'?group_id='+$('select#guard_group_select').find('option:selected').val(), false);
    $.ajaxSetup({ cache: false });
    // $.getScript($this.attr('data-link')+'?group_id='+$('select#guard_group_select').find('option:selected').val(), false);
  });
  $(document).on('change', "#add_single_firemen_to_guards", function(){
    var $this = $(this);  
    $.ajaxSetup({ cache: false });
    var val = $this.find("option:selected").val();
    if(!_.isEmpty(val)){
      $.getScript($this.attr('data-link')+'?fireman_id='+val, false); 
      // $.getScript($this.attr('data-link')+'?fireman_id='+val, false); 
    }
  })

  var nowTemp = new Date();
  var five_years_ago = new Date(nowTemp.getFullYear()-5, nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
   
  var startedAt = $('#criteria_started_at').datepicker({
    onRender: function(date) {
      return date.valueOf() < five_years_ago.valueOf() ? 'disabled' : '';
    }
  }).on('changeDate', function(ev) {
    if (ev.date.valueOf() > closedAt.date.valueOf()) {
      var newDate = new Date(ev.date)
      newDate.setDate(newDate.getDate());
      closedAt.setValue(newDate);
    }
    startedAt.hide();
    $('#criteria_closed_at')[0].focus();
  }).data('datepicker');
  var closedAt = $('#criteria_closed_at').datepicker({
    onRender: function(date) {
      return date.valueOf() < startedAt.date.valueOf() ? 'disabled' : '';
    }
  }).on('changeDate', function(ev) {
    closedAt.hide();
  }).data('datepicker');

};

$(document).on('page:change', ready);
