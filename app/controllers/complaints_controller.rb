class ComplaintsController < ApplicationController
  load_and_authorize_resource
  before_action :set_complaint, only: [:show, :edit, :update, :destroy, :report, :mark_as_fake]
  #before_action :set_as_reportable, only: [:index, :search]
  before_action :set_incident_types, only: [:new, :edit, :update, :create, :search]
  before_action :set_as_reportable, only: [:index, :search]


  rescue_from Complaint::CannotMarkAsFake do |e|
    redirect_to @complaint, alert: t('errors.messages.complaints.cannot_mark_as_fake')
  end

  rescue_from Complaint::CannotMarkAsUserError do |e|
    redirect_to @complaint, alert: t('errors.messages.complaints.cannot_mark_as_user_error')
  end


  # GET /complaints
  # GET /complaints.json
  def index
    @query = Complaint.search(query_param)
    @complaints = @query.result
    check_as_report
  end


  def search
    @firemen = Fireman.all
    @vehicles = Vehicle.all
    @selected_status = criteria_params[:complaint_status]  || 'any' 
    @complaints = Complaint.all
    if criteria_params.present?
      @complaints = @complaints.by_status(criteria_params[:complaint_status]) if criteria_params[:complaint_status].present?
      @complaints = @complaints.by_fireman_id(criteria_params[:fireman_id]) if criteria_params[:fireman_id].present?
      @complaints = @complaints.by_vehicle_id(criteria_params[:vehicle_id]) if criteria_params[:vehicle_id].present?
      @complaints = @complaints.where(incident_type_id: criteria_params[:incident_type_id]) if criteria_params[:incident_type_id].present?
      begin 
        @complaints = @complaints.search_by_day(Time.strptime(criteria_params[:created_at],date_format)) if criteria_params[:created_at].present?
      rescue
        criteria_params[:created_at]=""
      end
      if criteria_params[:from_date].present? && criteria_params[:to_date].present?
        begin
          @complaints = @complaints.search_by_string_date_range(criteria_params[:from_date], criteria_params[:to_date])
        rescue
        end
      end
      @complaints = @complaints.with_address_name_like(criteria_params[:address_name]) if criteria_params[:address_name].present?
    end
    check_as_report
  end

  # GET /complaints/1
  # GET /complaints/1.json
  def show
  end

  def show_map
    @complaints = Complaint.where(id: params[:complaints])
  end

  # GET /complaints/new
  def new
    @complaint = Complaint.new
    @complaint.build_address
    @complaint.build_complainant
  end

  # GET /complaints/1/edit
  def edit
  end

  def mark_as_fake
    @complaint.mark_as_fake!
    redirect_to @complaint
  end

  # DELETE /divisions/1
  # DELETE /divisions/1.json
  def destroy
    @division.mark_as_user_error!
    respond_to do |format|
      format.html { redirect_to complaints_url }
      format.json { head :no_content }
    end
  end

  def report
    authorize! :print, Complaint
    render layout: 'report'
  end

  def plural_report
    authorize! :print, Complaint
    render layout: 'report'
  end

  # POST /complaints
  # POST /complaints.json
  def create
    @complaint = Complaint.new(complaint_params)

    respond_to do |format|
      if @complaint.save
        format.html { redirect_to new_dispatch_path(@complaint) }
        format.json { render action: 'show', status: :created, location: @complaint }
      else
        format.html { render action: 'new' }
        format.json { render json: @complaint.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /complaints/1
  # PATCH/PUT /complaints/1.json
  def update
    respond_to do |format|
      if @complaint.update(complaint_params)
        format.html { redirect_to @complaint, notice: 'Complaint was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @complaint.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /complaints/1
  # DELETE /complaints/1.json
  def destroy
    @complaint.destroy
    respond_to do |format|
      format.html { redirect_to complaints_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_complaint
      @complaint = Complaint.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def complaint_params
      params.require(:complaint).permit(:situation, :incident_type_id, complainant_attributes: [:name, :phone_number],
        address_attributes: [:id, :name, :references, :longitude, :latitude, :address_type_id])
    end

    def set_incident_types
      @incident_types = IncidentType.all
    end

    def query_param
      params.require(:q) if params[:q]
    end

    def criteria_params
      if params[:criteria].present?
        params.require(:criteria).permit!
      else
        {}
      end
    end

    def date_format
      "%d-%m-%Y"
    end

    def check_as_report
      if params[:as_report]
        authorize! :print, Complaint
        respond_to do |format|
          format.json{ render json: @complaints }
          format.html{ render action: 'plural_report', layout: 'report' }
        end
      else
        @complaints = @complaints.page(page_param)
      end
    end    
end
