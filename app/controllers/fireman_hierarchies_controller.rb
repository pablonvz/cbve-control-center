class FiremanHierarchiesController < ApplicationController
  load_and_authorize_resource
  before_action :set_fireman_hierarchy, only: [:show, :edit, :update, :destroy]
  before_action :set_available_hierarchies, only: [:create, :edit, :new, :update]
  # GET /fireman_hierarchies
  # GET /fireman_hierarchies.json


  rescue_from DependentStillExists do |e|
    redirect_to fireman_hierarchies_path, alert: t('errors.messages.dependent', 
      entities: t('general.models.fireman').pluralize(I18n.locale))
  end

  def index
    @query = FiremanHierarchy.search(query_param)
    @fireman_hierarchies = @query.result.page(page_param)
  end

  # GET /fireman_hierarchies/1
  # GET /fireman_hierarchies/1.json
  def show
  end

  # GET /fireman_hierarchies/new
  def new
    @fireman_hierarchy = FiremanHierarchy.new
  end

  # GET /fireman_hierarchies/1/edit
  def edit
  end

  # POST /fireman_hierarchies
  # POST /fireman_hierarchies.json
  def create
    @fireman_hierarchy = FiremanHierarchy.new(fireman_hierarchy_params)

    respond_to do |format|
      if @fireman_hierarchy.save
        format.html { redirect_to @fireman_hierarchy, notice: 'Fireman hierarchy was successfully created.' }
        format.json { render action: 'show', status: :created, location: @fireman_hierarchy }
      else
        format.html { render action: 'new' }
        format.json { render json: @fireman_hierarchy.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fireman_hierarchies/1
  # PATCH/PUT /fireman_hierarchies/1.json
  def update
    respond_to do |format|
      if @fireman_hierarchy.update(fireman_hierarchy_params)
        format.html { redirect_to @fireman_hierarchy, notice: 'Fireman hierarchy was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fireman_hierarchy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fireman_hierarchies/1
  # DELETE /fireman_hierarchies/1.json
  def destroy
    @fireman_hierarchy.destroy
    respond_to do |format|
      format.html { redirect_to fireman_hierarchies_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fireman_hierarchy
      @fireman_hierarchy = FiremanHierarchy.find(params[:id])
    end
    def set_available_hierarchies
      @available_hierarchies = FiremanHierarchy.all
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def fireman_hierarchy_params
      params.require(:fireman_hierarchy).permit(:name, :description, :superior_id)
    end
   
    def query_param
      params.require(:q) if params[:q]
    end

end