class Guard::NotStarted < StandardError; end
class Guard::SearchInvalidDate < StandardError ; end
class GuardsController < ApplicationController
  include UsersControllable
  before_action :set_variables, only: [:search]
  before_action :set_active_and_groups, only: [:active, :update]

  rescue_from Guard::NotStarted do |e|
    # Only happens on 'active' action
    _error_render_method(t('errors.guard.not_started').capitalize, "active", "update_index", :set_active_and_groups)
  end

  rescue_from Guard::SearchInvalidDate do |e|
    # Only happens on 'search' action
    @guards = safe_search
    _error_render_method(t('errors.messages.invalid_date').capitalize, "search", "search", :set_variables)
  end

  def search
    set_as_reportable
    @guards = safe_search.search_by_dates(criteria_dates_params)
    if params[:as_report]
      authorize! :print, Guard
      respond_to do |format|
        format.json{ render json: @guards }
        format.html{ render action: 'report', layout: 'report' }
      end
    else
      @guards = @guards.page(page_param)
    end
  end

  def report
    authorize! :print, Guard
    render layout: 'report'
  end

  def active
  end

  def add_guard_group
    group_found = GuardGroup.find_by(id: params[:group_id])
    merge_with_editing_list(group_found.firemen) unless group_found.nil?
    set_active_and_groups
    respond_to do |format|
      format.js{ render 'update_index' }
      format.html{ render 'active' }
    end
  end

  def add_editing
    add_to_editing_list(params[:fireman_id])
    set_active_and_groups
    respond_to do |format|
      format.js{ render 'update_index' }
      format.html{ render 'active' }
    end
  end


  def update
    
    collection_params.each do |fireman_id, guard_status|
      guard = Guard.active.find_by(fireman_id: fireman_id) || Guard.new(fireman_id: fireman_id)
      if params[:close_all] || (params[:update_all] && guard_status == 'close')
        raise Guard::NotStarted if guard.started_at.nil?
        guard.close_now!
        remove_from_editing_list(guard.fireman)
      elsif params[:open_all] || (params[:update_all] && (guard_status == 'open' || guard_status == 'kee_open'))
        guard.update_attributes!(started_at: guard.started_at || Time.now)
      end
    end
    message = t('general.success_update').capitalize
    respond_to do |format|
      format.html { redirect_to guards_active_path, notice: message }
    end
  end

  def destroy
    guard = Guard.active.find_by(fireman_id: params[:fireman_id])
    fireman = guard.nil? ? Fireman.find_by(id: params[:fireman_id]) : guard.fireman
    remove_from_editing_list(fireman)
    guard.delete unless guard.nil?
    set_active_and_groups
    respond_to do |format|
      format.js{ render 'update_index' }
      format.html{ render 'active' }
    end
  end

  private 

    def set_active_and_groups
      set_variables # of UsersControllable
      @active_firemen = Fireman.where(id: merge_with_editing_list(Fireman.with_active_guard))
      @firemen = @firemen - @active_firemen
      @guard_groups = GuardGroup.all
    end

    def collection_params
      params[:guards] || []
    end

    def page_param
      params.require(:page) if params[:page]
    end

    def criteria_fireman_params
      params.require(:criteria).permit(:fireman_id).delete_if{ |k,v| v.blank? } if params[:criteria]
    end

    def criteria_dates_params 
      if params[:criteria]
        a = params.require(:criteria).permit(:started_at, :closed_at)
        a.delete_if{ |k, v| v.blank? }.each do |k,v|
          begin
            a[k] = Time.strptime(v,"%d-%m-%Y")
          rescue 
            raise Guard::SearchInvalidDate
          end
        end
      end
    end

    def _error_render_method message, render_html_path, render_js_path, before_render=nil
      flash.now[:alert] = message
      self.send(before_render) unless before_render.nil?
      respond_to do |format|
        format.html { render render_html_path,  :status => 404 }
        format.js   { render render_js_path,  :status => 404 }
        format.all  { render :nothing => true, :status => 404 }
      end
      true
    end

    def safe_search
      Guard.started_once.where(criteria_fireman_params).page(page_param).order(started_at: :desc)
    end

    def add_to_editing_list fireman_id
      gel = _guards_editing_list
      _guards_editing_set_list(gel+[fireman_id]).uniq
    end

    def merge_with_editing_list another_list
      gel = _guards_editing_list
      ani = _get_ids(another_list)
      _guards_editing_set_list((gel+ani).uniq)
    end

    def remove_from_editing_list element
      gel = _guards_editing_list
      (_guards_editing_set_list(gel - [element.id.to_s])) unless element.nil?
    end

    def _guards_editing_list
      Rails.cache.read("guards_editing_list:#{current_user.id}") || _guards_editing_set_list(Fireman.with_active_guard)
    end

    def _guards_editing_set_list new_list
      new_list = new_list.is_a?(Array) ? new_list : _get_ids(new_list)
      Rails.cache.write("guards_editing_list:#{current_user.id}", new_list)
      Rails.cache.read("guards_editing_list:#{current_user.id}")
    end

    def _guards_editing_list_push(e)
      gel = _guards_editing_list
      (_guards_editing_set_list (gel<<e.id.to_s)) unless e.nil?
    end

    def _get_ids list
      list.collect{ |e| e.id.to_s }
    end

end