class FiremanClassificationsController < ApplicationController
  load_and_authorize_resource
  before_action :set_fireman_classification, only: [:show, :edit, :update, :destroy]



  rescue_from DependentStillExists do |e|
    redirect_to fireman_classifications_path, alert: t('errors.messages.dependent', 
      entities: t('general.models.fireman').pluralize(I18n.locale))
  end

  # GET /fireman_classifications
  # GET /fireman_classifications.json
  def index
    @query = FiremanClassification.search(query_param)
    @fireman_classifications = @query.result.page(page_param)
  end

  # GET /fireman_classifications/1
  # GET /fireman_classifications/1.json
  def show
  end

  # GET /fireman_classifications/new
  def new
    @fireman_classification = FiremanClassification.new
  end

  # GET /fireman_classifications/1/edit
  def edit
  end

  # POST /fireman_classifications
  # POST /fireman_classifications.json
  def create
    @fireman_classification = FiremanClassification.new(fireman_classification_params)

    respond_to do |format|
      if @fireman_classification.save
        format.html { redirect_to @fireman_classification, notice: 'Fireman classification was successfully created.' }
        format.json { render action: 'show', status: :created, location: @fireman_classification }
      else
        format.html { render action: 'new' }
        format.json { render json: @fireman_classification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fireman_classifications/1
  # PATCH/PUT /fireman_classifications/1.json
  def update
    respond_to do |format|
      if @fireman_classification.update(fireman_classification_params)
        format.html { redirect_to @fireman_classification, notice: 'Fireman classification was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fireman_classification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fireman_classifications/1
  # DELETE /fireman_classifications/1.json
  def destroy
    @fireman_classification.destroy
    respond_to do |format|
      format.html { redirect_to fireman_classifications_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fireman_classification
      @fireman_classification = FiremanClassification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fireman_classification_params
      params.require(:fireman_classification).permit(:name, :description)
    end

    def query_param
      params.require(:q) if params[:q]
    end

end
