class SessionsController < Devise::SessionsController
  after_action :delete_notice, only: :create
  before_filter :configure_permitted_parameters

  protected
    def delete_notice
      flash.delete(:notice) if flash.keys.include?(:notice)
    end

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation, :remember_me) }
      devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :username, :email, :password, :remember_me) }
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :email, :password, :password_confirmation, :current_password) }
    end

end
