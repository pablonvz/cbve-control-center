class VersionsController < ApplicationController
  require 'will_paginate/array'
  load_and_authorize_resource class: PaperTrail::Version

  def index
    @query = PaperTrail::Version.search(query_param)
    @versions = @query.result.not_repeated.page(page_param)
  end

  def show
    version = PaperTrail::Version.find(params[:id])
    @versions = PaperTrail::Version.where(
      item_type: version.item_type, 
      item_id: version.item_id
    ).order(created_at: :desc).map do |vrs|
      instance = vrs.reify
      {version: vrs, instance: instance} unless instance.nil? 
    end.compact
    item = version.item_even_deleted
    @model_name = item.class.to_s.underscore
    @versions.unshift({version: version, instance: item })
    @versions = @versions.paginate(page: (page_param||1), per_page: 4)
    @is_current_version = first?
  end

  private
    def query_param
      params.require(:q) if params[:q]
    end

    def first?
      page_param == "1" || page_param.nil?
    end
end
