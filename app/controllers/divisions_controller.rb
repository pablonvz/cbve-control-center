class DivisionsController < ApplicationController
  load_and_authorize_resource
  before_action :set_division, only: [:show, :edit, :update, :destroy]

  before_action :set_fire_department_list, only: [:create, :edit, :new, :update]


  rescue_from DependentStillExists do |e|
    entities = t('general.models.fireman').pluralize(I18n.locale) if e.message == 'firemen'
    entities = t('general.models.vehicle').pluralize(I18n.locale) if e.message == 'vehicles'
      redirect_to divisions_path, alert: t('errors.messages.dependent', 
        entities: entities)
  end

  # GET by_fire_department/:fire_department_id.json
  def by_fire_department
    @divisions = []
    begin 
      @divisions = Division.where fire_department_id: params.require(:fire_department_id) 
    rescue ActiveRecord::RecordNotFound
    end
    respond_to do |format|
      format.json  { render :json =>  @divisions }
    end
  end

  # GET /divisions
  # GET /divisions.json
  def index
    @query = Division.search(query_param)
    @divisions = @query.result.page(page_param)
  end

  # GET /divisions/1
  # GET /divisions/1.json
  def show
  end

  # GET /divisions/new
  def new
    @division = Division.new
    @division.build_place
    @division.place.build_address
  end

  # GET /divisions/1/edit
  def edit
  end

  # POST /divisions
  # POST /divisions.json
  def create
    @division = Division.new(division_params)

    respond_to do |format|
      if @division.save
        format.html { redirect_to @division, notice: 'Division was successfully created.' }
        format.json { render action: 'show', status: :created, location: @division }
      else
        format.html { render action: 'new' }
        format.json { render json: @division.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /divisions/1
  # PATCH/PUT /divisions/1.json
  def update
    respond_to do |format|
      if @division.update(division_params)
        format.html { redirect_to @division, notice: 'Division was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @division.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /divisions/1
  # DELETE /divisions/1.json
  def destroy
    @division.destroy
    respond_to do |format|
      format.html { redirect_to divisions_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_division
      @division = Division.find(params[:id])
    end

    def set_fire_department_list
      @fire_department_list = FireDepartment.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def division_params
      params.require(:division).permit(:name, :fire_department_id, :code, :logo, place_attributes: [:id, :name, {address_attributes: [:id, :name, :references, :longitude, :latitude, :address_type_id, :_destroy]}, :_destroy])
    end
    def query_param
      params.require(:q) if params[:q]
    end

end
