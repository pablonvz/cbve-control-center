class PlacesController < ApplicationController
  before_action :set_place, only: [:show, :edit, :update, :destroy]

  rescue_from DependentStillExists do |e|
    redirect_to places_path, alert: t('errors.messages.dependent', 
      entities: t('general.models.division').pluralize(I18n.locale))
  end

  # GET /divisions
  # GET /divisions.json
  def index
    @query = Place.search(query_param)
    @places = @query.result.page(page_param)
  end

  # GET /places/1
  # GET /places/1.json
  def show
  end

  # GET /places/new
  def new
    @place = Place.new
    @place.build_address
  end

  # GET /places/1/edit
  def edit
  end

  # POST /places
  # POST /places.json
  def create
    @place = Place.new(place_params)

    respond_to do |format|
      if @place.save
        format.html { redirect_to @place, notice: 'Place was successfully created.' }
        format.json { render action: 'show', status: :created, location: @place }
      else
        format.html { render action: 'new' }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /places/1
  # PATCH/PUT /places/1.json
  def update
    respond_to do |format|
      place_params_to_update = place_params
      place_params_to_update.delete(:name) if @place.has_division?
      if @place.update(place_params_to_update)
        format.html { redirect_to @place, notice: 'Place was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /places/1
  # DELETE /places/1.json
  def destroy
    @place.destroy
    respond_to do |format|
      format.html { redirect_to places_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_place
      @place = Place.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def place_params
      params.require(:place).permit(:name, address_attributes: [:id, :name, :references, :longitude, :latitude, :address_type_id, :_destroy])
    end
    def query_param
      params.require(:q) if params[:q]
    end

end
