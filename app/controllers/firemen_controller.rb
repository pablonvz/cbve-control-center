class FiremenController < ApplicationController
  load_and_authorize_resource
  before_action :set_fireman, only: [:show, :edit, :update, :destroy]
  before_action :set_form_variables, only: [:create, :edit, :new, :update]


  rescue_from DependentStillExists do |e|
    redirect_to users_path, alert: t('errors.messages.firemen.delete_same_user')
  end

  # GET by_division/:division_id.json
  def by_division
    @firemen = []
    begin 
      @firemen = Fireman.where(division_id: params.require(:division_id)).includes(:person)
    rescue ActiveRecord::RecordNotFound
    end
    respond_to do |format|
      format.json  { render :json =>  @firemen.as_json(include: :person) }
    end
  end

  # GET /firemen
  # GET /firemen.json
  def index
    @query = Fireman.search(query_param)
    @firemen =  @query.result.page(page_param)
  end

  # GET /firemen/1
  # GET /firemen/1.json
  def show
  end

  # GET /firemen/new
  def new
  end

  # GET /firemen/1/edit
  def edit
  end

  # POST /firemen
  # POST /firemen.json
  def create
    @fireman = Fireman.new(fireman_params)

    respond_to do |format|
      if @fireman.save
        format.html { redirect_to @fireman, notice: 'Fireman was successfully created.' }
        format.json { render action: 'show', status: :created, location: @fireman }
      else
        format.html { render action: 'new' }
        format.json { render json: @fireman.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /firemen/1
  # PATCH/PUT /firemen/1.json
  def update
    respond_to do |format|
      if @fireman.update(fireman_params)
        format.html { redirect_to @fireman, notice: 'Fireman was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fireman.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /firemen/1
  # DELETE /firemen/1.json
  def destroy
    @fireman.destroy
    respond_to do |format|
      format.html { redirect_to firemen_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fireman
      @fireman = Fireman.find(params[:id])
    end

    def set_form_variables
      @fireman ||= Fireman.new
      @person = @fireman.person || @fireman.build_person
      @division_list = Division.all
      @fireman_hierarchy_list = FiremanHierarchy.all
      @fireman_classification_list = FiremanClassification.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fireman_params
      params.require(:fireman).permit(:division_id, :fireman_hierarchy_id, 
        :fireman_classification_id, :person_id, :code,
        person_attributes: [ :id, :name, :last_name, :birth_date, :phone_number, :gender, :document,
          {addresses_attributes: [:id, :references, :name, :longitude, :latitude, :address_type_id, :_destroy]}, 
          :_destroy])
    end

    def query_param
      params.require(:q) if params[:q]
    end

end
