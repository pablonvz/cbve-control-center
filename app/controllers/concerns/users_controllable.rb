module UsersControllable
  extend ActiveSupport::Concern
  
  private 
    def set_variables
      user = @user || current_user
      user = user.fireman.nil? ? current_user : user
      @fire_departments = FireDepartment.all
      @divisions = []
      @firemen = []
      @selected_fire_department = nil
      @selected_division = nil
      @selected_fireman_id = nil
      if user.fireman.nil?
        @selected_fire_department = @fire_departments.first
        @divisions = @selected_fire_department.divisions
        @selected_division = @divisions.first
        @firemen = @selected_division.firemen
        first_fireman = @firemen.first
        @selected_fireman_id = first_fireman.nil? ? nil : first_fireman.id
      else
        @selected_fireman_id = user.fireman.id
        @selected_division = user.fireman.division
        @selected_fire_department = @selected_division.fire_department
        @divisions = @selected_fire_department.divisions
        @firemen = @selected_division.firemen
      end
    end

end