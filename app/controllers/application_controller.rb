class ApplicationController < ActionController::Base
  before_filter :cancan_hack
  before_filter :set_current_user
  before_action :authenticate_user!
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end  

  # rescue_from ActiveRecord::RecordNotFound do |e|
  #   redirect_to root_url, alert: t('errors.messages.resource_not_found').capitalize
  # end

  rescue_from ActionController::InvalidAuthenticityToken do |e|
    redirect_to root_url, alert: t('errors.messages.credentials').capitalize
  end
  
  # see: https://github.com/ryanb/cancan/issues/835
  def cancan_hack
    resource = controller_path.singularize.gsub('/', '_').to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end

  def set_as_reportable
    @current_action_is_reportable = true
  end

  private

  def set_current_user
    User.current_user = current_user
  end
  def page_param
    params.require(:page) if params[:page]
  end

end
