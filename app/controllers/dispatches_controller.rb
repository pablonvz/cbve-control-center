class DispatchesController < ApplicationController
  before_action :set_vehicle, only: [:add_vehicle, :add_fireman]
  before_action :set_fireman, only: [:add_fireman]

  def new
    # @incident_types = IncidentType.all
    @dispatch = Dispatch.new
    @complaint = Complaint.find(params[:complaint_id])
    @vehicles = Vehicle.availables.where(dispatched: false)
    @firemen = Fireman.availables.where(dispatched: false)
  end

  def create
    fireman_ids = []
    dispatch_params[:vehicles].each_pair { |key, value| fireman_ids += value[:fireman_ids] }

    @complaint = Complaint.find(dispatch_params[:complaint_id])
    authorize! :dispatch, @complaint
    @dispatch = Dispatch.new(complaint: @complaint)#, incident_type: IncidentType.find(dispatch_params[:incident_type_id]))

    @vehicles = Vehicle.availables.where(dispatched: false, id: dispatch_params[:vehicles].keys)
    @firemen = Fireman.availables.where(dispatched: false, id: fireman_ids)

    dispatch_params[:vehicles].keys.each do |vehicle_id|
      driver_id = dispatch_params[:vehicles][vehicle_id][:driver]
      vehicle_fireman_ids = dispatch_params[:vehicles][vehicle_id][:fireman_ids]
      dv = DispatchesVehicles.new(vehicle: @vehicles.find(vehicle_id), driver: @firemen.find(driver_id))
      
      vehicle_fireman_ids.each do |fireman_id|
        dv.vehicles_firemen << VehiclesFiremen.new(fireman_id: fireman_id)
      end

      @dispatch.dispatches_vehicles << dv
    end

    Dispatch.transaction do
      @dispatch.save!
      @vehicles.update_all(dispatched: true)
      @firemen.update_all(dispatched: true)
    end

    redirect_to @complaint
  end

  def add_vehicle
    # NOOP
  end

  def add_fireman
    # NOOP
  end

  def report
    @dispatch = Dispatch.find(params[:id])
    authorize! :print, @dispatch
    render layout: 'report'
  end

  private
    def set_vehicle
      @vehicle = Vehicle.find(params[:vehicle_id])
    end

    def set_fireman
      @fireman = Fireman.find(params[:fireman_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dispatch_params
      params.require(:dispatch).permit!
    end
end
