class FireDepartmentsController < ApplicationController
  load_and_authorize_resource
  before_action :set_fire_department, only: [:show, :edit, :update, :destroy]



  rescue_from DependentStillExists do |e|
    redirect_to fire_departments_path, alert: t('errors.messages.dependent', 
      entities: t('general.models.division').pluralize(I18n.locale))
  end


  # GET /fire_departments
  # GET /fire_departments.json
  def index
    @query = FireDepartment.search(query_param)
    @fire_departments = @query.result.page(page_param)
  end

  # GET /fire_departments/1
  # GET /fire_departments/1.json
  def show
  end

  # GET /fire_departments/new
  def new
    @fire_department = FireDepartment.new
  end

  # GET /fire_departments/1/edit
  def edit
  end

  # POST /fire_departments
  # POST /fire_departments.json
  def create
    @fire_department = FireDepartment.new(fire_department_params)

    respond_to do |format|
      if @fire_department.save
        format.html { redirect_to @fire_department, notice: 'Fire department was successfully created.' }
        format.json { render action: 'show', status: :created, location: @fire_department }
      else
        format.html { render action: 'new' }
        format.json { render json: @fire_department.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fire_departments/1
  # PATCH/PUT /fire_departments/1.json
  def update
    respond_to do |format|
      if @fire_department.update(fire_department_params)
        format.html { redirect_to @fire_department, notice: 'Fire department was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fire_department.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fire_departments/1
  # DELETE /fire_departments/1.json
  def destroy
    @fire_department.destroy
    respond_to do |format|
      format.html { redirect_to fire_departments_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fire_department
      @fire_department = FireDepartment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fire_department_params
      params.require(:fire_department).permit(:name,  :code, :logo)
    end
    def query_param
      params.require(:q) if params[:q]
    end

end
