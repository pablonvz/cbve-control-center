class NotesController < ApplicationController
  before_action :set_note, only: [:show, :edit, :update, :destroy]

  # GET /notes
  # GET /notes.json
  def index
    @notes = Note.page(page_param)
  end

  # GET /notes/1
  # GET /notes/1.json
  def show
  end

  # GET /notes/new
  def new
    @note = InProgressNote.new(dispatch: Dispatch.find(params[:dispatch_id]))
  end

  def new_final
    @note = FinalNote.new(dispatch: Dispatch.find(params[:dispatch_id]))
    render action: 'new'
  end

  # GET /notes/1/edit
  def edit
  end

  # POST /notes
  # POST /notes.json
  def create
    @note = Note.new(note_params)

    respond_to do |format|
      if @note.save
        format.html { redirect_to complaint_path(@note.dispatch.complaint) }
        format.json { render action: 'show', status: :created, location: @note }
      else
        format.html { render action: 'new' }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /notes/1
  # PATCH/PUT /notes/1.json
  def update
    respond_to do |format|
      if @note.update(note_params)
        format.html { redirect_to complaint_path(@note.dispatch.complaint) }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # # DELETE /notes/1
  # # DELETE /notes/1.json
  # def destroy
  #   @note.destroy
  #   respond_to do |format|
  #     format.html { redirect_to notes_url }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_note
      @note = Note.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def note_params
      kind = :note
      kind = :in_progress_note if params[:in_progress_note].present?
      kind = :final_note if params[:final_note].present?
      params.require(kind).permit(:type, :title, :description, :dispatch_id)
    end
end
