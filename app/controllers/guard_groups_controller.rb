class GuardGroupsController < ApplicationController

  include UsersControllable
  before_action :set_guard_group, only: [:show, :edit, :update, :destroy]
  before_action :set_variables, only: [:create, :edit, :new, :update]

  # GET /guard_groups
  # GET /guard_groups.json
  def index
    @query = GuardGroup.search(query_param)
    @guard_groups = @query.result.page(page_param)
  end

  # GET /guard_groups/1
  # GET /guard_groups/1.json
  def show
  end

  # GET /guard_groups/new
  def new
    @guard_group = GuardGroup.new
  end

  # GET /guard_groups/1/edit
  def edit
  end

  # POST /guard_groups
  # POST /guard_groups.json
  def create
    puts params.inspect
    @guard_group = GuardGroup.new(guard_group_params)
    @guard_group.firemen_guard_groups.destroy_all
    @guard_group.firemen = Fireman.where(["id in (?)", firemen_ids_params])
    respond_to do |format|
      if @guard_group.save
        format.html { redirect_to @guard_group, notice: 'Guard group was successfully created.' }
        format.json { render action: 'show', status: :created, location: @guard_group }
      else
        format.html { render action: 'new' }
        format.json { render json: @guard_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /guard_groups/1
  # PATCH/PUT /guard_groups/1.json
  def update
    @guard_group.firemen_guard_groups.destroy_all
    @guard_group.firemen = Fireman.where(["id in (?)", firemen_ids_params])    
    respond_to do |format|
      if @guard_group.update(guard_group_params)
        format.html { redirect_to @guard_group, notice: 'Guard group was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @guard_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /guard_groups/1
  # DELETE /guard_groups/1.json
  def destroy
    @guard_group.destroy
    respond_to do |format|
      format.html { redirect_to guard_groups_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_guard_group
      @guard_group = GuardGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def guard_group_params
      params.require(:guard_group).permit(:name, :description)
    end

    def firemen_ids_params
      params.require(:fireman_ids).compact if params[:fireman_ids]
    end

    def query_param
      params.require(:q) if params[:q]
    end

end
