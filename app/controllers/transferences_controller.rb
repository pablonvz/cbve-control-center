class Transference::SearchInvalidDate < StandardError ; end
class TransferencesController < ApplicationController
  before_action :set_transference, only: [:show, :edit, :update, :destroy]

  before_action :set_variables, only: [:create, :edit, :new, :update]

  before_action :set_as_reportable, only: [:index, :search]

  rescue_from Transference::CanNotBeUndone do |e|
    redirect_to transferences_path, alert: t('errors.messages.transference.can_not_be_undone')
  end

  # GET /transferences
  # GET /transferences.json
  def index
    @query = Transference.search(query_param)
    @transferences = @query.result
    check_as_report
  end

  def set_place
    @place = Place.find_by(id: params[:place_id])
  end

  def add_fireman
    @firemen_on_list = Fireman.where(id: _add_to_params_array(params[:ids_on_list],params[:fireman_id]) )
    @firemen_for_select = _firemen - @firemen_on_list
    respond_to { |f| f.js{ render 'update_firemen' } }
  end

  def add_vehicle
    @vehicles_on_list = Vehicle.where(id: _add_to_params_array(params[:ids_on_list],params[:vehicle_id]))
    @vehicles_for_select = _vehicles - @vehicles_on_list
    respond_to { |f| f.js{ render 'update_vehicles' } }
  end


  def remove_fireman
    @firemen_on_list = Fireman.where(id: _remove_from_params_array(params[:ids_on_list],params[:fireman_id]))
    @firemen_for_select = _firemen - @firemen_on_list
    respond_to { |f| f.js{ render 'update_firemen' } }
  end

  def remove_vehicle
    @vehicles_on_list = Vehicle.where(id: _remove_from_params_array(params[:ids_on_list],params[:vehicle_id]))
    @vehicles_for_select = _vehicles - @vehicles_on_list
    respond_to { |f| f.js{ render 'update_vehicles' } }
  end

  # GET /transferences/1
  # GET /transferences/1.json
  def show
  end

  # GET /transferences/new
  def new
    @transference = Transference.new
  end

  # POST /transferences
  # POST /transferences.json
  def create
    @transference = Transference.new(transference_params)
    @firemen_on_list = Fireman.where(id: _ids_from_params('Fireman'))
    @vehicles_on_list = Vehicle.where(id: _ids_from_params('Vehicle'))
    @transference.future_transferables = @firemen_on_list + @vehicles_on_list
    respond_to do |format|
      if @transference.save
        format.html { redirect_to @transference, notice: 'Transference was successfully created.' }
        format.json { render action: 'show', status: :created, location: @transference }
      else
        format.html { render action: 'new' }
        format.json { render json: @transference.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /transferences/1
  # DELETE /transferences/1.json
  def destroy
    @transference.undone!
    respond_to do |format|
      format.html { redirect_to transferences_url }
      format.json { head :no_content }
    end
  end


  rescue_from Transference::SearchInvalidDate do |e|
    # Only happens on 'search' action
    @transferences = safe_search
    _error_render_method(t('errors.messages.invalid_date').capitalize, "search", "search", :set_variables)
  end

  def search
    @firemen = Fireman.all
    @vehicles = Vehicle.all
    @places = Place.all
    @transferences = safe_search.search_by_day(criteria_dates_params[:created_at])
    check_as_report
  end

  def report
    authorize! :print, Transference
    render layout: 'report'
  end

  private
    def check_as_report
      if params[:as_report]
        authorize! :print, Transference
        respond_to do |format|
          format.json{ render json: @transferences }
          format.html{ render action: 'report', layout: 'report' }
        end
      else
        @transferences = @transferences.page(page_param)
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_transference
      @transference = Transference.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transference_params
      params.require(:transference).permit(:reason, :place_id, :available,
        transference_histories_attributes: [ :id, :transferable_id, :transferable_type, :_destroy])
    end

    def _firemen
      Fireman.not_dispatched
    end

    def _vehicles
      Vehicle.not_dispatched
    end

    def set_variables
      @places = Place.all
      @vehicles_for_select = _vehicles
      @firemen_for_select = _firemen
    end
    def query_param
      params.require(:q) if params[:q]
    end

    def _add_to_params_array(json_array, item)
      JSON.parse(json_array)+[item]
    end

    def _remove_from_params_array(json_array, item)
      JSON.parse(json_array)-[item]
    end

    def _ids_from_params type
      ( (transference_params[:transference_histories_attributes]||[]).map do |thp|
        thp[:transferable_id] if thp[:transferable_type] == type
      end.compact)
    end

    def safe_search
      Transference.by_fireman(criteria_model_params('fireman')[:fireman_id])
        .by_origin(criteria_places_params[:origin_id])
        .by_destination(criteria_places_params[:destination_id])
        .by_vehicle(criteria_model_params('vehicle')[:vehicle_id])
        .order(created_at: :desc)
    end

    def criteria_model_params model
      r = params.require(:criteria).permit(:"#{model}_id").delete_if{ |k,v| v.blank? } if params[:criteria]
      r || {}
    end

    def criteria_dates_params 
      if params[:criteria]
        a = params.require(:criteria).permit(:created_at)
        a.delete_if{ |k, v| v.blank? }.each do |k,v|
          begin
            a[k] = Time.strptime(v,"%d-%m-%Y")
          rescue 
            raise Transference::SearchInvalidDate
          end
        end
      else {} end
    end

    def criteria_places_params
      return params.require(:criteria).permit(:destination_id, :origin_id).delete_if{|k,v| v.blank? } if params[:criteria]
      return {}
    end

end