class UsersController < ApplicationController
  include UsersControllable
  load_and_authorize_resource
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :set_variables, only: [:create, :edit, :new, :update]
  before_action :set_user_has_fireman, only: [:create, :edit, :new, :update]

  rescue_from DependentStillExists do |e|
    redirect_to users_path, alert: t('errors.messages.users.cannot_self_delete')
  end

  # GET /users.json
  def index
    @query = User.search(query_param)
    @users = @query.result.page(page_param)
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.type ||= type_from_params
    respond_to do |format|
      if @user.save
        format.html { redirect_to user_path(@user), notice: 'User was successfully created.' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    if user_params[:password].blank?
      user_params.delete(:password)
      user_params.delete(:password_confirmation)
    end
    @user.fireman = nil unless has_fireman?
    @user.type ||= type_from_params
    # https://github.com/plataformatec/devise/wiki/How-To%3a-Allow-users-to-edit-their-account-without-providing-a-password
    successfully_updated = if needs_password?(@user, user_params)
                             @user.update(user_params)
                           else
                             @user.update_without_password(user_params)
                           end
    respond_to do |format|
      if successfully_updated
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
    @user.will_have_fireman = has_fireman?
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    valid_params = [:password, :confirm_password, :code]
    valid_params << :fireman_id if has_fireman?
    valid_params << :type if can? :edit_user_path, @user
    valid_params << :username unless has_fireman?
    params.require(sti_type).permit(valid_params)
  end

  def type_from_params
    (params.require(sti_type).permit(:type) || {})[:type] if can? :edit_user_path, @user
  end

  def sti_type
    params[:admin].nil? ? :user : :admin
  end

  def has_fireman? 
    has = params[:has_fireman]
    not(has.nil?) and not(has.include?("false"))
  end

  def needs_password?(user, params)
    params[:password].present?
  end

  def query_param
    params.require(:q) if params[:q]
  end

  def set_user_has_fireman
    @user.will_have_fireman = has_fireman?
  end
end
