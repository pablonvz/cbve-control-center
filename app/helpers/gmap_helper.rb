module GmapHelper

  include NestedForm::ViewHelper

  def load_gmap_resources
    geometry = '//maps.google.com/maps/api/js?v=3.13&sensor=false&libraries=geometry'
    markerclusterer = '//google-maps-utility-library-v3.googlecode.com/svn/tags/markerclustererplus/2.0.14/src/markerclusterer_packed.js'
    capture do 
      concat javascript_tag({}, {src: geometry})
      concat " "
      concat javascript_tag({}, {src: markerclusterer})
    end
  end

  def gmap_index(elements, options={})
    hash = Gmaps4rails.build_markers(elements) do |element, marker|
      address = element.address
      marker.lat address.latitude
      marker.lng address.longitude
      marker.title element.explanation
      marker.instance_variable_get(:@hash)[:explanation] = element.explanation
      icon_str = element.incident_type.icon.underscore.upcase
      marker.instance_variable_get(:@hash)[:icon] = icon_str[3..(icon_str.length)]
    end

    render(partial: 'gmap/gmap_index', locals: {hash: raw(hash.to_json), execute_now: options[:execute_now]})
  end

  def gmap_show(latitude, longitude, instance_hash=nil)
    capture do 
      concat render(partial: 'gmap/gmap_show', locals: {latitude: latitude, longitude: longitude, hash: instance_hash.to_s})
    end
  end

  def gmap_picker(form, instance, options={})
    capture do 
      concat render(partial: 'gmap/gmap_nested_picker', locals: build_picker_locals(form, instance, options))
    end
  end

  def gmap_nested_picker(form, instance, options={})
    capture do 
      concat render(partial: 'gmap/gmap_nested_picker', locals: build_nested_picker_locals(form, instance, options))
    end
  end

  private 

    def build_picker_locals(form, instance, options)
      build_generic_locals(form, instance, options).merge({multiple: false})
    end

    def build_nested_picker_locals(form, instance, options)
      build_generic_locals(form, instance.addresses.build, options).merge({multiple: true})
    end

    def build_generic_locals(form, instance, options)
      options = {
        longitude: {}, 
        latitude: {}, 
        address: {},
        address_type: {},
        references: {},
        #prefix: address_instance.class.name.underscore.singularize,
        nested: {}
      }.merge(options)

      locals = options.merge({
        f: form 
      })

      #locals[:prefix] += locals[:nested][:attributes] || '[addresses_attributes]'
      locals[:latitude][:name] ||= 'latitude'
      locals[:longitude][:name] ||= 'longitude'
      locals[:address][:name] ||= 'name'
      locals[:references][:name] ||= 'references'

      locals[:has_type] = locals[:has_type] || false
      if locals[:has_type]
        locals[:address_type][:name] ||= "address_type"
        locals[:address_type][:column] ||= "address_type_id"
        locals[:address_types] ||= AddressType.all
      end

      locals[:zoom_level] = 16
      # Encarnacion
      locals[:default_coordinates] = {
        latitude:  -27.339758,
        longitude: -55.86636199999998
      }
      locals[:default_city] ||= "Encarnación"

      locals[:data_attribute] ||= "data-gmap-index"
      locals[:search_dom_class] ||= "gmap-search"
      locals[:latitude][:dom_class] ||= "gmap-latitude-field"
      locals[:longitude][:dom_class] ||= "gmap-longitude-field"
      locals[:address][:dom_class] ||= "gmap-address-field"
      locals[:references][:dom_class] ||= "gmap-address-references-field"

      hash = instance.hash.abs.to_s
      locals[:index_controller_name] ||= "already_used_indexes_"+hash
      locals[:identifier_dom_class] ||= "gmap_nested_picker_togle"+hash
      locals[:geocode_handler] = 'geocode_handler_'+hash
      locals[:map_container_dom_class] ||= "gmap_coordinate_picker_container"+hash
      locals[:show_hide_dom_class] ||= "gmap-toggle-"+hash
      locals[:startup_fuction_name] ||= "gmap_nested_picker_startup_"+hash
      locals
    end
end