module ApplicationHelper
	include GmapHelper

	def results_quantity qtty
		capture do
			concat raw('<div class="alert alert-info alert-dismissable" style="padding-top:8px; padding-bottom:8px;">')
				concat raw('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>')
        if qtty == 1
        	concat t('general.single_result_found')
      	else 
      		concat t('general.results_found', {number: qtty })
    		end 
      concat raw('</div>')
    end
	end

  def _error_render_method message, render_html_path, render_js_path, before_render=nil
    flash.now[:alert] = message
    self.send(before_render) unless before_render.nil?
    respond_to do |format|
      format.html { render render_html_path,  :status => 404 }
      format.js   { render render_js_path,  :status => 404 }
      format.all  { render :nothing => true, :status => 404 }
    end
    true
  end

	def panel_heading_search_form_for(query, *fields)
		text_field_name = fields.join('_or_').concat('_cont')

		scope = begin
			query.context.klass.table_name
		rescue NoMethodError
			"general"
		end

		searchable = fields.map do |field|
			t(["attributes", scope, field].join('.'))
		end

		search_form_for(query) do |sf|
			capture do
				concat raw('<div class="form-group input-group search-group">')
					concat sf.text_field(text_field_name, class: 'form-control clearable',
						placeholder: t('general.search_field_placeholder',
							searchable: searchable.join(" #{t('words.or')} ")),
						autocomplete: :off)
					concat raw('<a href="#" class="glyphicon glyphicon-remove clear-btn"></a>') unless query.conditions.empty?
					concat raw('<span class="input-group-btn">')
						concat sf.button(raw("<i class='fa fa-search'></i> #{t('general.links.search').capitalize}"), class: 'btn btn-default')
					concat raw('</span>')
				concat raw('</div>')
			end
		end
	end

	def changes_path(object)
		version_path object.versions.last
	end

	def image_show(model, options={})
		return model.logo? ? image_tag(model.logo.url(:thumb)) : t("general.no_image_yet") if options.empty? && model.respond_to?("logo")
		options = { attribute: "logo", size: "thumb", message: "general.no_image_yet" }.merge(options)
		model.send("#{options[:attribute]}?") ? image_tag(model.send(options[:attribute]).url(options[:size])) : t(options[:message])
	end

	def short_text(text, limit = 20)
		txt = text.to_s
		txt.length > limit ? txt[0,limit]+" ... " : txt
	end

	def sub_menu(main_text, items, level)
		can_something = false
		items.each do |item|
			can_something = can?(item[:action], item[:class])
			break if can_something
		end
		return unless can_something
		content_tag(:li) do
			li_html = link_to'#', :"data-no-turbolink" => true do
				raw main_text+content_tag(:span, nil,class: "fa arrow")
			end
			li_html << content_tag(:ul, {class: "nav nav-third-level collapse", style: "height: auto"}) do 
				items.map do |item|
					content_tag(:li, (link_to item[:text], item[:path])) if can?(item[:action], item[:class])
				end.compact.join.html_safe
			end
			li_html
		end
	end
end