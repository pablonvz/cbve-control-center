module UsersHelper
  def fireman_select_options user, selected_fireman_id, action
    user.has_or_will_have_fireman? ? {selected: selected_fireman_id} : { include_blank: true }
  end
end