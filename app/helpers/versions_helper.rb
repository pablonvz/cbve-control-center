module VersionsHelper

  def version_user terminator_id
    return t("general.users.system") if terminator_id.nil?
    user = User.find(terminator_id)
    link_to user.login, user_path(user)
  end

  def item_path item_id, item_type
    invalid_types = ["Person"]
    unless invalid_types.include?(item_type)
      record_exists = false
      begin 
        record_exists = !!item_type.constantize.find(item_id)
      rescue ActiveRecord::RecordNotFound
      end
      begin 
        return link_to decorate_model_name(item_type), Rails.application.routes.url_helpers.send("#{item_type.to_s.underscore.singularize}_path", item_id) if record_exists
      rescue NoMethodError
      end
    end
    decorate_model_name(item_type)
  end

  def valid_attribute? attr
    invalid_patterns = ["password", "remember_created_at", "deleted_at"]
    invalid_patterns.each do |pattern|
      return false if attr.to_s.include?(pattern)
    end
    true
  end

  def translate_attribute attribute
    if attribute.include?("_id")
      return t("general.models.#{attribute.gsub('_id', '')}") 
    else
      return t("general.attributes.#{attribute}")
    end
  end

  def decorate_value(attribute, value, instance)
    identifier_fields = ["name", "code"]
    output = ""
    relation = attribute.gsub("_id", "")
    if attribute.include?("_id") && instance.respond_to?(relation)
      related = instance.send(relation)
      output = []
      identifier_fields.each do |id_field|
        output << related.send(id_field) if related.respond_to?(id_field)
      end
      output = output.join(" - ")
    end
    return output.blank? ? _beautify(value) : output
  end

  def decorate_model_name model_name
    model_name = model_name.underscore
    model_name = "user" if model_name == "admin"
    # t("general.models.#{model_name}").singularize(I18n.locale).titleize
    t("general.models.#{model_name}", default: t("general.attributes.#{model_name}", default: model_name.titleize)).titleize
  end

  def decorate_event version, is_current_version=false
    version = version.previous unless is_current_version
    entity = User.find_by(id: version.version_author) if version.respond_to?("version_author")
    entity ||= t("general.the_system")
    entity = link_to(entity.login, user_path(version.version_author)) if entity.respond_to?("login")
    prefix = is_current_version ? t("general.versions.current").capitalize_first_char + " - " : ""
    event = version.respond_to?("event") ? version.event : "create"
    raw prefix + t("general.versions.events.#{event == "destroy" ? "destroye" : event}d_by", {entity: entity}).capitalize_first_char
  end

  def event_class version, is_current_version=false
    prev_version = version.previous
    event = prev_version.respond_to?("event") ? prev_version.event : "create"
    return "panel-warning" if event == "update" and not is_current_version
    return "panel-danger" if event == "delete"
    return "panel-success" if event == "create" and not is_current_version
    return "panel-default"
  end

  private
    def _beautify value
      return l(value, format: :long) if value.is_a?(Date) or value.is_a?(DateTime) or value.is_a?(Time)
      return t("general.#{value}_significant") if( (!!value) == value )
      return value
    end

end
