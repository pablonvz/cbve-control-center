module GuardsHelper
  def guard_options fireman
    options = ['close', 'kee_open']
    selected = guard_selected(fireman)
    options = ['open'] if selected == 'open'
    options.map do |opt|
      [t("enumerize.actions.#{opt}"), opt]
    end
  end
  def guard_selected fireman
    fireman.active_guard? ? 'kee_open' : 'open'
  end

  def options_for_fireman_search_select params
    if (params[:criteria] || {})[:fireman_id].blank?
      content_tag(:option, 
        t('general.actions.search', entity: t('general.models.fireman')).capitalize, 
        :value=>"")+options_from_collection_for_select(@firemen, 'id', 'compound_name')
    else
      options_from_collection_for_select(@firemen, 'id', 'compound_name', params[:criteria][:fireman_id])
    end
  end

end
