module FormHelper
  def actions(entity_str)
    -> (form) do
      capture do
        link_html = link_to("javascript:history.back();", class: "btn btn-default") do
          concat raw('<span class="glyphicon glyphicon-list-alt"></span>')
          concat "\n"
          concat raw(t('general.links.back').capitalize)
        end

        concat link_html

        concat "\n"
        concat form.submit t('general.actions.save', entity: entity_str).capitalize,
            class: "btn btn-primary"
      end
    end
  end
end