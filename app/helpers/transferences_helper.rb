module TransferencesHelper
  def options_for_fireman_search_select params, name=nil
    if (params[:criteria] || {})[:fireman_id].blank?
      content_tag(:option, 
        t('general.actions.search', entity: t('general.models.fireman')).capitalize, 
        :value=>"")+options_from_collection_for_select(@firemen, 'id', 'compound_name')
    else
      options_from_collection_for_select(@firemen, 'id', 'compound_name', params[:criteria][:fireman_id])
    end
  end

  def options_for_vehicle_search_select params, name=nil
    if (params[:criteria] || {})[:vehicle_id].blank?
      content_tag(:option, 
        t('general.actions.search', entity: t('general.models.vehicle')).capitalize, 
        :value=>"")+options_from_collection_for_select(@vehicles, 'id', 'compound_name')
    else
      options_from_collection_for_select(@vehicles, 'id', 'compound_name', params[:criteria][:vehicle_id])
    end
  end
  
  def options_for_place_search_select params, name
    if (params[:criteria] || {})[:"#{name}"].blank?
      content_tag(:option, 
        t('general.actions.search', entity: t('general.models.place')).capitalize, 
        :value=>"")+options_from_collection_for_select(@places, 'id', 'name')
    else
      options_from_collection_for_select(@places, 'id', 'name', params[:criteria][:"#{name}"])
    end
  end


  def criteria_select(model, options={})
    css_class ||= "col-lg-6"
    options[:name] = "#{options[:name] || model}_id"
    name =  "criteria[#{options[:name]}]"
    translated_text = options[:translated_text] || "general.models.#{model}"
    options_method = "options_for_#{model}_search_select"
    html = <<-html
      <div class='#{css_class}'>
        <div class='form-group'>
          #{ label_tag name, t('general.actions.search', entity: t(translated_text)).capitalize }
          #{ select_tag name, self.send(options_method, params, options[:name]), { class: 'form-control combobox' } }
        </div>
      </div>
    html
    html.html_safe
  end
end
