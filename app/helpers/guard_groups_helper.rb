module GuardGroupsHelper

  def select_table list, options
    options.stringify_keys!
    html_options = { class: "form-control combobox #{options['class']}", 
      id: options['id'], data: { :"select-table-wrapper" => options['wrapper'], 
      :"select-table-attributes" => options['attributes'], 
      :"select-table-name" => options['name'], :"select-table-hidden-attributes" => options["hidden_attributes"] }  }
    select_options = ''
    select_options = content_tag(:option, options["prompt"], value: '') if options["prompt"]
    list.each do |item|  
      display = []
      data = {}
      options['attributes'].each do |attr|
        value = item.send(attr)
        data["attribute-#{attr}"] = value
        display << value
      end
      options["hidden_attributes"].each do |attr|
        data["hidden-attribute-#{attr}"] = item.send(attr)
      end
      select_options << content_tag(:option, display.join(' - '),  value: item.id, 
        data: data )
    end
    select_tag(nil, raw(select_options).html_safe, html_options)
  end
end
