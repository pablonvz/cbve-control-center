class FireDepartment < ActiveRecord::Base
  acts_as_coded
  acts_as_any
 
  has_many :divisions
  acts_as_dependent :divisions

  validates :name, presence: true, format: { with: ValidationRegexp.for(:name),
    message: ValidationRegexp.message_for(:name) }, length: { in: 3..100 }
  validates :code, presence: true, format: { with: ValidationRegexp.for(:code),
    message: ValidationRegexp.message_for(:code) }, length: { in: 2..10 }

  has_attached_file :logo, 
    styles: { medium: "300x300>", thumb: "56x56>" },
    default_url: "/images/:style/missing.png",
    url: "/system/:attachment/fire_departments/:style/:id.:extension",
    path: ":rails_root/public/system/:attachment/fire_departments/:style/:id.:extension"

  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\Z/   
end
