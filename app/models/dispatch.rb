class Dispatch < ActiveRecord::Base
	has_many :dispatches_vehicles, class_name: DispatchesVehicles
  has_many :vehicles_firemen, class_name: VehiclesFiremen, through: :dispatches_vehicles, source: :vehicles_firemen
  has_many :firemen, through: :vehicles_firemen, source: :fireman
	has_many :vehicles, through: :dispatches_vehicles, source: :vehicle
  has_many :drivers, through: :dispatches_vehicles,  source: :driver
  has_many :notes

	belongs_to :complaint
  # belongs_to :incident_type

  def closed?
    !closed_at.nil?
  end

  def close!
    now = Time.now
    raise Dispatch::AlreadyClosed if self.closed?
    self.transaction do 
      vehicles.update_all(dispatched: false)
      drivers.update_all(dispatched: false)
      firemen.update_all(dispatched: false)
      self.update_attributes!(closed_at: now)
    end
  end

  # validates :incident_type, presence: true
end
class Dispatch::AlreadyClosed < StandardError ; end