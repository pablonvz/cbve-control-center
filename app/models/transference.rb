class Transference < ActiveRecord::Base
  belongs_to :place
  has_many :transference_histories, dependent: :destroy

  def transferables 
    firemen + vehicles
  end

  has_many :fireman_transference_histories, -> { where transferable_type: 'Fireman' },
    class_name: TransferenceHistory

  def self.search_by_day time
    return all if time.nil?
    beginning_of_day = DateTime.new(time.year, time.month, time.day, 0, 0, 0, 0)
    end_of_day = DateTime.new(time.year, time.month, time.day, 24, 0, 0, 0)
    where('transferences.created_at >= :beginning_of_day AND transferences.created_at <= :end_of_day', {beginning_of_day: beginning_of_day, end_of_day: end_of_day})
  end

  def self.by_origin place_id
    return all if place_id.nil?
    includes(:transference_histories).where(transference_histories: { original_address_id: Place.find(place_id).address.id })
  end

  def self.by_destination place_id
    return all if place_id.nil?
    where(transferences: { place_id: place_id })
  end

  def self.by_fireman fireman_id
    return all if fireman_id.nil?
    includes(:transference_histories).where(transference_histories: { transferable_id: fireman_id, transferable_type: Fireman.to_s })
  end

  def self.by_vehicle vehicle_id
    return all if vehicle_id.nil?
    includes(:transference_histories).where(transference_histories: { transferable_id: vehicle_id, transferable_type: Vehicle.to_s })
  end


  has_many :firemen, through: :transference_histories, source: :transferable, source_type: 'Fireman'
  has_many :vehicles, through: :transference_histories, source: :transferable, source_type: 'Vehicle'

  has_many :vehicle_transference_histories, -> { where transferable_type: 'Vehicle' },
    class_name: TransferenceHistory
    

  validates :reason, presence: true, length: { in: 10..1000 }
  validates :place, presence: true

  delegate :name, to: :place, prefix: true
  
  accepts_nested_attributes_for :transference_histories, allow_destroy: true

  before_create :transfer!

  def transfer!
    now = Time.now
    Transference.transaction do 
      transference_histories.each do |history|
        transferable = history.transferable
        history.original_address = transferable.current_address#self.transference_histories.last.try(:transference).try(:place) || transferable.division.place
        history.originally_available = transferable.available
        history.original_division = transferable.division

        transferable.available = self.available
        transferable.current_address = self.place.address
        if self.place.has_division?
          transferable.division = self.place.division
        end      
        transferable.save!

        # close active guards
        transferable.guards.active.last.close_at!(now) if transferable.is_a?(Fireman) && transferable.active_guard?

        history.save!
      end
    end
  end

  def undone!
    raise Transference::CanNotBeUndone unless can_be_undone?
    Transference.transaction do 
      transferables.each do |transferable|
        last_thistory = transferable.transference_histories.last
        #transferable.division = last_thistory.original_place.division if last_thistory.original_place.has_division?
        transferable.current_address = last_thistory.original_address
        transferable.division = last_thistory.original_division
        transferable.available = last_thistory.originally_available?
        transferable.save!
      end
      self.destroy
    end
  end

  def can_be_done?
    future_transferables.each do |transferable|
      return false if transferable.dispatched
    end
    true
  end

  def can_be_undone?
    # TODO: a super query for better performance
    if place.has_division?
      firemen_ids = firemen.map(&:id)
      return false unless DispatchesVehicles.where(fireman_id: firemen_ids).where("created_at >= ?", self.created_at).empty?
      return false unless VehiclesFiremen.where(fireman_id: firemen_ids).where("created_at >= ?", self.created_at).empty?
      vehicles_ids = vehicles.map(&:id)
      return false unless DispatchesVehicles.where(vehicle_id: vehicles_ids).where("created_at >= ?", self.created_at).empty?
      # false
    end
    true
  end

  validate :at_least_one_transferable, :can_be_done

  attr_accessor :future_transferables
  private

  def at_least_one_transferable
    if future_transferables.nil? || future_transferables.size < 1
      self.errors.add :base, :at_least_one_transferable
    end
  end

  def can_be_done
    unless can_be_done?
      self.errors.add :base, :can_not_be_done
    end
  end

  # Cast the ID column to a string in Mysql
  # when searching with Ransack.
  # Allows searches with _cont and _eq predicates.
  # From https://github.com/ernie/ransack/issues/224
  ransacker :id do
    Arel::Nodes::SqlLiteral.new("substring(CAST(transferences.id as CHAR(12)),length(transferences.id))")
  end
end
class Transference::CanNotBeUndone < StandardError ; end