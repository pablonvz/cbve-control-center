class GuardGroup < ActiveRecord::Base
  acts_as_any
  has_many :firemen_guard_groups, class_name: ::FiremenGuardGroups
  has_many :firemen, through: :firemen_guard_groups

  validates :name, presence: true, format: { with: ValidationRegexp.for(:name),
    message: ValidationRegexp.message_for(:name) }, length: { in: 3..40 }

  accepts_nested_attributes_for :firemen_guard_groups, allow_destroy: true, reject_if: :all_blank
end
