class Place < ActiveRecord::Base
  belongs_to :address

  # when a new address relation is saved, the older one is destroyed
  accepts_nested_attributes_for :address, allow_destroy: true  
  validates :name, presence: true, format: { with: ValidationRegexp.for(:name),
      message: ValidationRegexp.message_for(:name) }, length: { in: 3..100 }
  validates :address, presence: true

  delegate :name, to: :address, allow_nil: true, prefix: true


  has_one :division
  acts_as_dependent :division

  def has_division?
    !division.nil?
  end
  
end
