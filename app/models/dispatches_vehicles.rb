class DispatchesVehicles < ActiveRecord::Base
  belongs_to :dispatch
  belongs_to :vehicle

  has_many :vehicles_firemen, class_name: VehiclesFiremen
  has_many :firemen, through: :vehicles_firemen, class_name: Fireman

  belongs_to :driver, class_name: Fireman, foreign_key: :fireman_id

end
