class FinalNote < Note
  before_create do 
    self.dispatch.close!
  end
end