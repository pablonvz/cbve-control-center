class PersonAddress < ActiveRecord::Base
  acts_as_any
  belongs_to :person
  belongs_to :address
end
