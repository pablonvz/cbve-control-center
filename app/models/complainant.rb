class Complainant < ActiveRecord::Base
  acts_as_any
  has_many :complaints

  validates :name, presence: true, length: { in: 3..40 },
    format: { with: ValidationRegexp.for(:name),
      message: ValidationRegexp.message_for(:name) }
  validates :phone_number, presence: true,
    format: { with: ValidationRegexp.for(:phone_number) },
    length: { in: 3..25 }
end
