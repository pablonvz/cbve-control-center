class IncidentType < ActiveRecord::Base
  acts_as_any
  validates :name, presence: true, format: { with: ValidationRegexp.for(:name),
    message: ValidationRegexp.message_for(:name) }, length: { in: 3..40 }
  #validates :icon, presence: false, format: { with: ValidationRegexp.for(:name),
   # message: ValidationRegexp.message_for(:icon) }, length: { in: 3..40 }
  # has_many :dispatches
  # acts_as_dependent :dispatches
  has_many :complaints
  acts_as_dependent :complaints
end
