class Address < ActiveRecord::Base
  acts_as_any
  belongs_to :address_type
  accepts_nested_attributes_for :address_type

  has_many :people_addresses
  has_many :people, through: :people_addresses #?

  geocoded_by :name
  reverse_geocoded_by :latitude, :longitude
  #acts_as_gmappable :process_geocoding => false

  # acts_as_gmappable process_geocoding: false, 
  #   # the geocode will be performed each time the model is run through a validate method
  #   # if the position of the address provided isn't found by google, an error will be displayed
  #   validation: true,
  #   #  latitude and longitude will be saved in the columns latitude and longitude
  #   lat: "latitude", 
  #   lng: "longitude",
  #   # the status of the geocode, will be checked in a method or in db column
  #   # boolean called gmaps. Note: If :check_process is set to false then 
  #   # lat & long will be updated when the model is updated.
  #   check_process: true, 
  #   checker: "gmaps" 
  # }
  
  validates :name, presence: true, length: { in: 3..80 }
  before_validation :verify_geolocation
  alias_attribute :address, :name

  private 
    def verify_geolocation
      if name.present?
        require 'concurrent'
        if name_changed?
          ::Concurrent::Future.execute{ geocode }
        elsif latitude.present? && 
          longitude.present? && (latitude_changed? || longitude_changed?)
          ::Concurrent::Future.execute{ reverse_geocode }
        end
      end
      true
    end

end
