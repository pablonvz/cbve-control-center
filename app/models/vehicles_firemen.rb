class VehiclesFiremen < ActiveRecord::Base
  belongs_to :dispatches_vehicles, class_name: DispatchesVehicles
  belongs_to :fireman, class_name: Fireman
end
