class AddressType < ActiveRecord::Base
  acts_as_any
  validates :name, presence: true, format: { with: ValidationRegexp.for(:name),
    message: ValidationRegexp.message_for(:name) }, length: { in: 3..40 }
  has_many :addresses
  acts_as_dependent :addresses
end
