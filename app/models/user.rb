class User < ActiveRecord::Base
  acts_as_any
  extend Enumerize
  devise :database_authenticatable, :trackable, :validatable, :rememberable

  belongs_to :fireman
  delegate :code, to: :fireman, allow_nil: true, prefix: true

  before_save do | user |
    user.username = nil if user.has_fireman?
  end

  acts_as_dependent do  |me|
    current_user.id == me.id
  end

  def login=(login)
    @login = login
  end

  def login
    @login || self.fireman_code || self.username
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      fireman = Fireman.where(code: login.upcase).first
      query = "lower(username) = :value"
      args = { :value => login.downcase }
      unless fireman.nil?
        query << " OR fireman_id = :fireman_id"
        args.merge!({fireman_id: fireman.id})
      end
      where(conditions).where([query, args]).first
    else
      where(conditions).first
    end
  end



  # validates :password, length: { in: 8..40 }
  # validates :password_confirmation, length: { in: 8..40 }

  validates :username, uniqueness: { case_sensitive: false }, length: { in: 3..40 },
    format: { with: ValidationRegexp.for(:name),
      message: ValidationRegexp.message_for(:name) }, allow_blank: true
  validates_presence_of :username, if: :has_not_and_wont_have_username?
  

  validates_presence_of :fireman, if: :has_or_will_have_fireman?
  attr_accessor :will_have_fireman

  attr_accessor :role
  enumerize :role, in: %w[User Admin], i18n_scope: "enumerize.role"

  validates :type, inclusion: { in: %w[User Admin] }, :presence => true

  def has_role? role
    role.to_s == type.underscore.to_s
  end

  def has_or_will_have_fireman?
     has_fireman? || !!will_have_fireman
  end

  def has_fireman?
    !(fireman.nil?)
  end
  def has_not_and_wont_have_username?
    ! has_or_will_have_fireman?
  end

  def has_username?
    !username.nil?
  end

  def self.current_user=(user)
    Thread.current[:current_user] = user
  end

  def self.current_user
    Thread.current[:current_user]
  end

  def email_required?
    false
  end

  def email_changed?
    false
  end
  # validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, allow_blank: true

end