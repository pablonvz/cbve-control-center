class Person < ActiveRecord::Base
  extend Enumerize
  acts_as_any
 
  enumerize :gender, in: %w[male female], i18n_scope: "enumerize.gender"

  has_many :people_addresses, class_name: ::PersonAddress

  has_many :addresses, through: :people_addresses

  accepts_nested_attributes_for :addresses, :allow_destroy => true, 
                                :reject_if => :all_blank

  validates :name, presence: true, format: { with: ValidationRegexp.for(:name),
    message: ValidationRegexp.message_for(:name) }, length: { in: 3..40 }


  validates :last_name, presence: true, format: { with: ValidationRegexp.for(:name),
    message: ValidationRegexp.message_for(:name) }, length: { in: 3..40 }


  validates :document, presence: true, format: { with: ValidationRegexp.for(:doc),
    message: ValidationRegexp.message_for(:doc) }, length: { in: 3..40 }

  validates :phone_number, format: { with: ValidationRegexp.for(:phone_number) }, length: { in: 3..25 }


  validates_presence_of :addresses
end
