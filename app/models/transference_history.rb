class TransferenceHistory < ActiveRecord::Base
  belongs_to :transference
  belongs_to :transferable, polymorphic: true # Fireman or Vehicle
  belongs_to :fireman, ->{ where transferable_type: 'Fireman' }
  belongs_to :vehicle, ->{ where transferable_type: 'Vehicle' }

  belongs_to :original_address, class_name: Address
  belongs_to :original_division, class_name: Division

  def self.ordered_by_date
    order(created_at: :desc)
  end

  def self.last_two
    limit(2).ordered_by_date
  end
end

