class FiremanHierarchy < ActiveRecord::Base
  acts_as_any
  belongs_to :superior, class_name: FiremanHierarchy
  delegate :name, to: :superior, allow_nil: true, prefix: true
  has_many :firemen
  acts_as_dependent :firemen

  validates :name, presence: true, format: { with: ValidationRegexp.for(:name),
    message: ValidationRegexp.message_for(:name) }, length: { in: 3..40 }
end