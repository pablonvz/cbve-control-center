class Vehicle < ActiveRecord::Base
  acts_as_coded
  acts_as_any
  acts_as_transferable

  belongs_to :division
  delegate :name, to: :division, allow_nil: true, prefix: true
   
  has_one :fire_department, through: :division
  delegate :name, to: :fire_department, allow_nil: true, prefix: true

  validates :code, format: { with: ValidationRegexp.for(:code),
    message: ValidationRegexp.message_for(:code) }, length: { in: 2..10 }

  def compound_name
    "#{code} - #{division_name}"
  end

end
