class Note < ActiveRecord::Base
  belongs_to :dispatch
  #validates :title, presence: true, length: { in: 10..100 }
  validates :description, presence: true, length: { in: 10..2500 }
  validates :dispatch, presence: true
end
