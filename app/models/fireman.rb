class Fireman < ActiveRecord::Base
  acts_as_coded
  acts_as_any
  acts_as_transferable
 
  belongs_to :division
  belongs_to :fireman_hierarchy
  belongs_to :fireman_classification
  belongs_to :person

  delegate :name, to: :division, prefix: true
  delegate :code, to: :division, prefix: true
  delegate :name, to: :fireman_hierarchy, prefix: true
  delegate :name, to: :fireman_classification, prefix: true

  validates :name, presence: true, format: { with: ValidationRegexp.for(:name),
    message: ValidationRegexp.message_for(:name) }, length: { in: 3..40 }

  validates :code, presence: true, format: { with: ValidationRegexp.for(:code),
    message: ValidationRegexp.message_for(:code) }, length: { in: 2..10 }

  accepts_nested_attributes_for :person, :allow_destroy => true

  person_attributes = [:name, :last_name, :addresses, :birth_date, :gender, :document, :phone_number]
  person_attributes.each do |attribute|
    delegate attribute, :"#{attribute.to_s}=", to: :person, allow_nil: true
  end

  validates_presence_of (person_attributes-[:phone_number])
  validates :phone_number, format: { with: ValidationRegexp.for(:phone_number) }, length: { in: 3..25 }
  
  validates_presence_of :addresses

  acts_as_dependent do  |me|
    User.current_user.fireman_id == me.id
  end
  
  validates_presence_of :division, :fireman_hierarchy, :fireman_classification

  has_many :firemen_guard_groups, class_name: ::FiremenGuardGroups
  
  has_many :guard_groups, through: :firemen_guard_groups
  has_many :guards

  scope :with_active_guard, -> { joins(:guards).merge(Guard.active) }

  def full_name
    "#{name} #{last_name}"
  end

  def active_guard?
    !Guard.active.where(fireman_id: id).first.nil?
  end

  def compound_name
    "#{code} - #{full_name}"
  end

end
