class Guard < ActiveRecord::Base
    extend Enumerize
  acts_as_any
  belongs_to :fireman

  scope :active, -> { where("guards.closed_at IS NULL AND guards.started_at IS NOT NULL") }
  scope :active_with_firemen, -> { active.includes(:fireman) }

  scope :started_once, -> { where('guards.started_at IS NOT NULL') }

  def self.active_at datetime
    where("guards.started_at <= :start_date AND (guards.closed_at >= :end_date OR guards.closed_at IS NULL)", {start_date: datetime.end_of_day, end_date: datetime.beginning_of_day})
  end

  def self.active_between_dates x_date, y_date
    after_before = "(guards.started_at >= :x_date AND guards.closed_at <= :y_date )"
    before_after = "(guards.started_at <= :y_date AND guards.closed_at >= :x_date )"
    beginning_of_day = DateTime.new(x_date.year, x_date.month, x_date.day, 0, 0, 0, 0)
    end_of_day = DateTime.new(y_date.year, y_date.month, y_date.day, 24, 0, 0, 0)
    where("#{after_before} OR #{before_after}", { x_date: beginning_of_day, y_date: end_of_day })
  end

  validates :started_at, presence: true
  validates :fireman, presence: true

  def close_now!
    close_at!(Time.now)
  end

  def close_at! time
    update_attributes!(closed_at: time)
  end

  def active?
    ( not(closed_at.nil?) and closed_at >= Time.now ) or ( not(started_at.nil?) and started_at >= Time.now )
  end

  def self.search_by_dates conditions={}
    conditions ||= {} # conditions could be nill
    return active_between_dates(conditions['started_at'], conditions['closed_at']) if conditions['started_at'].present? and conditions['closed_at'].present?
    return active_at(conditions['started_at']) if conditions['started_at'].present?
    return active_at(conditions['closed_at']) if conditions['closed_at'].present?
    return all
  end
end
