class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)
    if user.has_role? :admin
       can :manage, :all
       can :edit_user_type, User
       can :undone, Transference
       can :print, :all # Dispatch, Complaint, Transference, Guard
    else
        cannot :manage, :all
        #can :manage, :all
         # Dispatch, Complaint, Transference, Guard
        can :manage, [Dispatch, Complaint, FinalNote, Note]
        can :read, [Fireman, FiremanClassification, FiremanHierarchy, Division, FireDepartment]
        
        cannot :manage, FiremanClassification
        cannot :manage, User

        can :edit, User do |usr|
          usr.id == user.id
        end
        can :update, User do |usr|
          usr.id == user.id
        end

        cannot :manage, FiremanHierarchy
        
        #can :read, :all

        cannot :read, User
        can :show, User do |usr|
            usr.id == user.id
        end
        
        cannot :read, AddressType
        cannot :edit, FinalNote
        cannot :print, :all
    end

    cannot [:edit, :update], Complaint do |complaint|
        !complaint.dispatches.empty?
    end

    cannot :close, Dispatch do |dispatch|
        dispatch.closed?
    end

    cannot :dispatch, Complaint do |complaint|
        complaint.is_fake?
    end

    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
