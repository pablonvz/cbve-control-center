class FiremenGuardGroups < ActiveRecord::Base
  acts_as_any
  belongs_to :fireman
  belongs_to :guard_group
end
