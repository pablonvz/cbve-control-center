class Division < ActiveRecord::Base
  acts_as_coded
  acts_as_any
  acts_as_dependent :vehicles
  acts_as_dependent :firemen
 
  belongs_to :place, dependent: :delete
  belongs_to :fire_department

  has_one :address, through: :place

  has_many :firemen
  has_many :vehicles

  delegate :name, to: :place, allow_nil: true, prefix: true
  delegate :name, to: :place, allow_nil: true, prefix: false  
  delegate :name, to: :address, prefix: true
  delegate :name, to: :fire_department, prefix: true

  has_attached_file :logo, 
    styles: { medium: "300x300>", thumb: "56x56>" }, 
    default_url: "/images/:style/missing.png",
    url: "/system/:attachment/divisions/:style/:id.:extension",  
    path: ":rails_root/public/system/:attachment/divisions/:style/:id.:extension"  

  validates :place, presence: true
  validates :code, presence: true, format: { with: ValidationRegexp.for(:code),
    message: ValidationRegexp.message_for(:code) }, length: { in: 2..10 }
  validates :fire_department, presence: true
  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\Z/   

  # when a new address relation is saved, the older one is destroyed
  accepts_nested_attributes_for :place, allow_destroy: true

  def compound_name
    "#{fire_department_name} - #{name}"
  end

end
