class Complaint < ActiveRecord::Base
  acts_as_any
  default_scope order(created_at: :desc)

  has_many :dispatches
   
  belongs_to :address, dependent: :destroy
  belongs_to :complainant, dependent: :destroy
  belongs_to :incident_type

  accepts_nested_attributes_for :complainant
  accepts_nested_attributes_for :address

  delegate :name, to: :complainant, prefix: true
  delegate :name, to: :incident_type, prefix: true  
  delegate :phone_number, to: :complainant, prefix: true

  validates :situation, presence: true, length: { in: 10..1000 }
  validates :incident_type, presence: true

  scope :fake, ->{ where(is_fake: true) }
  scope :pending, ->{ includes(:dispatches).where( dispatches: { complaint_id: nil } ).where(is_fake: false) }

  scope :with_dispatches, ->{ includes(:dispatches).where(' dispatches.complaint_id IS NOT NULL ').references(:dispatches) }
  scope :attended, ->{ with_dispatches.where(' dispatches.closed_at IS NOT NULL ').references(:dispatches) }
  scope :being_attended, ->{  with_dispatches.where(' dispatches.closed_at IS NULL ').references(:dispatches) }


  def has_dispatches?
    !dispatches.empty?
  end

  def mark_as_fake!
    raise Complaint::CannotMarkAsFake if has_dispatches?
    self.update_attributes!(is_fake: true)
  end

  def mark_as_user_error!
    raise Complaint::CannotMarkAsUserError if has_dispatches?
    self.destroy
  end
  def status
    return 'fake' if is_fake
    if has_dispatches?
      return 'attended' if self.dispatches.all?(&:closed?)
      return 'being_attended' 
    end
    'pending'
  end

  def self.search_by_day time
    return all if time.nil?
    beginning_of_day = DateTime.new(time.year, time.month, time.day, 0, 0, 0, 0)
    end_of_day = DateTime.new(time.year, time.month, time.day, 24, 0, 0, 0)
    where('complaints.created_at >= :beginning_of_day AND complaints.created_at <= :end_of_day', {beginning_of_day: beginning_of_day, end_of_day: end_of_day})
  end

  def self.search_by_string_date_range from_date, to_date
    date_format = "%d-%m-%Y"
    from_date = Time.strptime(from_date, date_format)
    to_date = Time.strptime(to_date, date_format)
    self.search_by_date_range(from_date, to_date)
  end

  def self.search_by_date_range x_date, y_date
    after_before = "(complaints.created_at >= :x_date AND complaints.created_at <= :y_date )"
    before_after = "(complaints.created_at <= :y_date AND complaints.created_at >= :x_date )"
    beginning_of_day = DateTime.new(x_date.year, x_date.month, x_date.day, 0, 0, 0, 0)
    end_of_day = DateTime.new(y_date.year, y_date.month, y_date.day, 24, 0, 0, 0)
    where("#{after_before} OR #{before_after}", { x_date: beginning_of_day, y_date: end_of_day })
  end

  def self.with_address_name_like address_name
    self.includes(:address).where('addresses.name LIKE ?', "%#{address_name}%").references(:addresses)
  end

  def self.by_status status
    if status == 'complaint_fake'
      return self.fake
    elsif status == 'complaint_pending'
      return self.pending
    elsif status == 'complaint_attended'
      return self.attended
    elsif status == 'complaint_being_attended'
      return self.being_attended
    end
    all
  end


  def self.by_vehicle_id vehicle_id
    includes(dispatches:  :dispatches_vehicles).where(dispatches_vehicles: 
      { vehicle_id: vehicle_id })
  end

  def self.by_fireman_id fireman_id
    includes(dispatches: { dispatches_vehicles: { vehicles_firemen: :fireman } })
      .where("vehicles_firemen.fireman_id = :fireman_id OR dispatches_vehicles.fireman_id = :fireman_id",
       fireman_id: fireman_id).references(:vehicles_firemen, :dispatches_vehicles)
  end

  def explanation
    [ "#{created_at}", "  #{incident_type_name}",
      "#{complainant_name} (#{complainant_phone_number})", situation ].join("\n")
  end
end
class Complaint::CannotMarkAsFake < StandardError ; end
class Complaint::CannotMarkAsUserError < StandardError ; end
