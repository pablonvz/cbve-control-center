require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module FiredevsWebsystem
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    config.to_prepare do 
      Devise::SessionsController.layout "sign_in"
    end

    # prevent scaffold.css to be overwritten
    config.generators do |g|
      g.stylesheets false
    end

    config.i18n.default_locale = :es
    I18n.enforce_available_locales = false

    WillPaginate.per_page = 10    

    # cache
    # config.action_controller.perform_caching = true

     # Enable threaded mode
    config.cache_classes = true
    config.eager_load = true
    
    config.time_zone = "America/Asuncion" # set local time zone for servers which doesn't have time properly configured
    config.active_record.default_timezone = :local  
  end
end
