# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
FiredevsWebsystem::Application.config.secret_key_base = 'e4a57604c012fa56bd52940a8b1b46a686297d98c70f2ebba649c90a70998090681adf6be0f0bcad9aec9adcafdacea482d0eb6d4233cb0adb1b971f8054d904'
