class ValidationRegexp
  def self.for(attribute)
    regexps[:regex][attribute]
  end

  def self.message_for(attribute)
    regexps[:message][attribute]
  end

  private
  def self.regexps
    @@regexps ||= HashWithIndifferentAccess.new(
        YAML.load(File.read(Rails.root.join('config', 'validation_regexp.yml'))))
  end
end
