FiredevsWebsystem::Application.routes.draw do

  resources :incident_types

# resources :highlights, :controller => "ads", :type => "Highlight"
# resources :bargains, :controller => "ads", :type => "Bargain"
  resources :final_notes, except: [:new], controller: :notes, type: FinalNote.to_s
  resources :in_progress_notes, except: [:new], controller: :notes, type: InProgressNote.to_s
  
  [:notes, :final_notes, :in_progress_notes].each do |note_kind|
    resources note_kind, except: [:new, :destroy] do 
      new{ get ':dispatch_id', action: 'new', as: '', controller: :notes, type: note_kind }
      get 'new_final/:dispatch_id', action: 'new_final', as: 'new_final', controller: :notes, type: note_kind, on: :collection
    end
  end
  resources :places

  root "complaints#new"

  resources :guard_groups

  resources :vehicles

  get "versions", :to => 'versions#index', as: :paper_trail_versions
  get 'version/:id', :to => 'versions#show', as: :paper_trail_version


  resources :fireman_hierarchies

  resources :firemen do
    get 'by_division/:division_id', action: :by_division, on: :collection
  end

  resources :fireman_classifications

  resources :divisions do
    get 'by_fire_department/:fire_department_id', action: :by_fire_department, on: :collection
  end

  resources :complaints, path_names: { destroy: 'mark_as_user_error' }  do 
    get 'report', action: :report, as: 'report', on: :member
    get 'mark_as_fake', action: :mark_as_fake, as: 'mark_as_fake', on: :member
    get 'search', action: :search, on: :collection # params: see action on controller
    get 'show_map', action: :show_map, on: :collection
  end

  resources :fire_departments

  resources :address_types

  resources :transferences, except: [:edit, :update], path_names: { destroy: 'undone' } do
    get 'search', action: :search, on: :collection # params: see action on controller
    get "set_place", action: :set_place, on: :collection# params: place_id
    get "add_vehicles", action: :add_vehicle, on: :collection# params: ids_on_list, vehicle_id
    get "add_firemen", action: :add_fireman, on: :collection# params: ids_on_list, fireman_id
    get "remove_vehicle", action: :remove_vehicle, on: :collection# params: ids_on_list, vehicle_id
    get "remove_fireman", action: :remove_fireman, on: :collection# params: ids_on_list, fireman_id
  end

  resources :people

  devise_for :users, path: 'auth', skip: [:registrations], path_names: {
    sign_in: 'login',
    sign_out: 'logout'
  }, controllers: {
    sessions: 'sessions'
  }

  get "guards/history", :to => 'guards#search', as: :guards_search
  get "guards/active"
  get "guards/add_guard_group"
  get "guards/add_editing"
  post "guards/update"
  delete "guards/destroy/:fireman_id", to: 'guards#destroy', as: :guards_destroy

  resources :dispatches, except: [:edit, :update, :index, :show, :destroy, :new] do
    get 'add_vehicle/:vehicle_id', action: 'add_vehicle', on: :collection, format: :json, as: 'add_vehicle'
    get 'add_fireman/:vehicle_id/:fireman_id', action: 'add_fireman', on: :collection, format: :json, as: 'add_fireman'
    new { get ':complaint_id', action: 'new', as: '' }
    get 'report', action: :report, as: 'report', on: :member
  end

  # scope "/admin" do
    resources :users
    resources :users, path: "admins", as: "admins"
  # end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
