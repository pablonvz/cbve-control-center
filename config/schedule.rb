set :output, "log/#{@environment}.log"
every 4.hours do
  runner "Address.not_geocoded.each(&:geocode)"
end