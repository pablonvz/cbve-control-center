source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.3'

# Use sqlite3 as the database for Active Record
# gem 'sqlite3'

# MySQL
gem 'mysql2'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.1.2'

# Use unicorn as the app server
# gem 'unicorn'

# Use thin as the app server
gem 'thin'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

group :development, :test do
  gem 'rspec-rails'
  gem 'shoulda'

  # UML class diagram generator
  gem 'railroady'
end

group :test do
  gem 'timecop'
  gem 'database_cleaner'
  gem 'factory_girl_rails'
  gem 'zeus'
end

gem 'devise'
gem 'simple_form'
gem "cancan"

gem 'geocoder'
gem 'gmaps4rails'
gem "gmap_coordinates_picker"


gem 'nested_form'
# Client side validations
#gem 'client_side_validations', git: 'https://github.com/bcardarella/client_side_validations.git', branch: "4-0-beta"
git_source(:custom_github){ |repo_name| "https://github.com/#{repo_name}.git" } 
gem 'client_side_validations', custom_github: "bcardarella/client_side_validations", branch: "4-0-beta"

# ActiveRecord soft deletion (acts_as_paranoid)
gem "paranoia", "~> 2.0"
gem 'paper_trail'

# ActiveRecord enumeration
gem 'enumerize'

# Object-based searching
gem "ransack", custom_github: "activerecord-hackery/ransack", branch: "rails-4"

# pagination
gem 'will_paginate'

# File uploading
gem 'paperclip'

# To fix jquery turbolinks issue
gem 'jquery-turbolinks'

# db foreign keys helper
gem 'foreigner'

# migration generator for foreigner
gem 'immigrant'

# concurrent
gem 'concurrent-ruby'

# for scheduled jobs
gem 'whenever', group: :production
gem 'puma', group: :production
