y = Date.yesterday
ActiveRecord::Base.record_timestamps = false
begin
	Complaint.where("created_at > ?", y).each{|e| e.update_attributes(created_at: (e.created_at.to_time - 1.hours).to_datetime) }
	Note.where("created_at > ?", y).each{|e| e.update_attributes(created_at: (e.created_at.to_time - 1.hours).to_datetime) }
	Dispatch.where("created_at > ?", y).each{|e| e.update_attributes(created_at: (e.created_at.to_time - 1.hours).to_datetime) }
ensure
	ActiveRecord::Base.record_timestamps = true
end
