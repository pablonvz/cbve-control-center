Complaint.destroy_all
Note.destroy_all
Dispatch.destroy_all
Fireman.update_all(dispatched: false)
Vehicle.update_all(dispatched: false)