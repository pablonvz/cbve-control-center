require "spec_helper"

describe FiremanClassificationsController do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/fireman_classifications").to route_to("fireman_classifications#index")
    end

    it "routes to #new" do
      expect(:get => "/fireman_classifications/new").to route_to("fireman_classifications#new")
    end

    it "routes to #show" do
      expect(:get => "/fireman_classifications/1").to route_to("fireman_classifications#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/fireman_classifications/1/edit").to route_to("fireman_classifications#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/fireman_classifications").to route_to("fireman_classifications#create")
    end

    it "routes to #update" do
      expect(:put => "/fireman_classifications/1").to route_to("fireman_classifications#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/fireman_classifications/1").to route_to("fireman_classifications#destroy", :id => "1")
    end

  end
end
