require "spec_helper"

describe IncidentTypesController do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/incident_types").to route_to("incident_types#index")
    end

    it "routes to #new" do
      expect(:get => "/incident_types/new").to route_to("incident_types#new")
    end

    it "routes to #show" do
      expect(:get => "/incident_types/1").to route_to("incident_types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/incident_types/1/edit").to route_to("incident_types#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/incident_types").to route_to("incident_types#create")
    end

    it "routes to #update" do
      expect(:put => "/incident_types/1").to route_to("incident_types#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/incident_types/1").to route_to("incident_types#destroy", :id => "1")
    end

  end
end
