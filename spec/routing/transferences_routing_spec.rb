require "spec_helper"

describe TransferencesController do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/transferences").to route_to("transferences#index")
    end

    it "routes to #new" do
      expect(:get => "/transferences/new").to route_to("transferences#new")
    end

    it "routes to #show" do
      expect(:get => "/transferences/1").to route_to("transferences#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/transferences/1/edit").to route_to("transferences#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/transferences").to route_to("transferences#create")
    end

    it "routes to #update" do
      expect(:put => "/transferences/1").to route_to("transferences#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/transferences/1").to route_to("transferences#destroy", :id => "1")
    end

  end
end
