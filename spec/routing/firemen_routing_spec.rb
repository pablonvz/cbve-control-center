require "spec_helper"

describe FiremenController do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/firemen").to route_to("firemen#index")
    end

    it "routes to #new" do
      expect(:get => "/firemen/new").to route_to("firemen#new")
    end

    it "routes to #show" do
      expect(:get => "/firemen/1").to route_to("firemen#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/firemen/1/edit").to route_to("firemen#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/firemen").to route_to("firemen#create")
    end

    it "routes to #update" do
      expect(:put => "/firemen/1").to route_to("firemen#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/firemen/1").to route_to("firemen#destroy", :id => "1")
    end

  end
end
