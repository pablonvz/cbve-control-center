require "spec_helper"

describe GuardGroupsController do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/guard_groups").to route_to("guard_groups#index")
    end

    it "routes to #new" do
      expect(:get => "/guard_groups/new").to route_to("guard_groups#new")
    end

    it "routes to #show" do
      expect(:get => "/guard_groups/1").to route_to("guard_groups#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/guard_groups/1/edit").to route_to("guard_groups#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/guard_groups").to route_to("guard_groups#create")
    end

    it "routes to #update" do
      expect(:put => "/guard_groups/1").to route_to("guard_groups#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/guard_groups/1").to route_to("guard_groups#destroy", :id => "1")
    end

  end
end
