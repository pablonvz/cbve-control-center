require "spec_helper"

describe FiremanHierarchiesController do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/fireman_hierarchies").to route_to("fireman_hierarchies#index")
    end

    it "routes to #new" do
      expect(:get => "/fireman_hierarchies/new").to route_to("fireman_hierarchies#new")
    end

    it "routes to #show" do
      expect(:get => "/fireman_hierarchies/1").to route_to("fireman_hierarchies#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/fireman_hierarchies/1/edit").to route_to("fireman_hierarchies#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/fireman_hierarchies").to route_to("fireman_hierarchies#create")
    end

    it "routes to #update" do
      expect(:put => "/fireman_hierarchies/1").to route_to("fireman_hierarchies#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/fireman_hierarchies/1").to route_to("fireman_hierarchies#destroy", :id => "1")
    end

  end
end
