require 'spec_helper'

describe FireDepartment do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of :code }
  it { should have_many(:divisions) }
end
