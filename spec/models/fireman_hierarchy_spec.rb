require 'spec_helper'

describe FiremanHierarchy do
  it{ should validate_presence_of :name }
  it{ should have_many :firemen }
  it{ should belong_to :superior }
end
