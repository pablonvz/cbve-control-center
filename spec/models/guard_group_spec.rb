require 'spec_helper'

describe GuardGroup do
  it { should validate_presence_of :name }
  it { should have_many(:firemen) }
end
