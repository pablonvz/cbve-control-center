require 'spec_helper'

describe Guard do
  it { should validate_presence_of :started_at }
  it { should belong_to :fireman }
end
