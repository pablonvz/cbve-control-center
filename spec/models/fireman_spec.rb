require 'spec_helper'

describe Fireman do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of :code }
  it { should validate_presence_of(:last_name) }
  it { should validate_presence_of(:addresses) }
  it { should validate_presence_of(:birth_date) }
  it { should validate_presence_of(:gender) }
  it { should validate_presence_of(:document) }
  it { should validate_presence_of(:division) }
  it { should validate_presence_of(:fireman_classification) }
  it { should validate_presence_of(:fireman_hierarchy) }
  it { should belong_to(:division) }
  it { should belong_to(:fireman_hierarchy) }
  it { should belong_to(:fireman_classification) }
  it { should belong_to(:person) }
end
