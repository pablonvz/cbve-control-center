require 'spec_helper'

describe Division do
  # it { should validate_presence_of :name }
  # it { should belong_to :address }
  it { should belong_to :place }
  # it { should validate_presence_of :address }
  # it { should accept_nested_attributes_for(:address).allow_destroy(true) }
  it { should accept_nested_attributes_for(:place).allow_destroy(true) }
  
  it { should validate_presence_of :code }
  it { should belong_to :fire_department }
  it { should validate_presence_of :fire_department }
end

