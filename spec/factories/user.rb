FactoryGirl.define do
  factory :user do
    email "admin@virtuadevs.com"
    password "password"
    password_confirmation "password"
  end
end
