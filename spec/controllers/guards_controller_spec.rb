require 'spec_helper'

describe GuardsController do

  describe "GET 'active'" do
    it "returns http success" do
      get 'active'
      expect(response).to be_success
    end
  end

  describe "GET 'by_date'" do
    it "returns http success" do
      get 'by_date'
      expect(response).to be_success
    end
  end

  describe "GET 'between_dates'" do
    it "returns http success" do
      get 'between_dates'
      expect(response).to be_success
    end
  end

  describe "GET 'open'" do
    it "returns http success" do
      get 'open'
      expect(response).to be_success
    end
  end

  describe "GET 'close'" do
    it "returns http success" do
      get 'close'
      expect(response).to be_success
    end
  end

  describe "GET 'destroy'" do
    it "returns http success" do
      get 'destroy'
      expect(response).to be_success
    end
  end

end
