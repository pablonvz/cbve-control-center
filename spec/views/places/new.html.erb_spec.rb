require 'spec_helper'

describe "places/new" do
  before(:each) do
    assign(:place, stub_model(Place,
      :name => "MyString",
      :address => nil
    ).as_new_record)
  end

  it "renders new place form" do
    render

    assert_select "form[action=?][method=?]", places_path, "post" do
      assert_select "input#place_name[name=?]", "place[name]"
      assert_select "input#place_address[name=?]", "place[address]"
    end
  end
end
