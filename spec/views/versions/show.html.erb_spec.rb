require 'spec_helper'

describe "versions/show" do
  before(:each) do
    @version = assign(:version, stub_model(Version,
      :item_type => "Item Type",
      :terminator => "Terminator"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Item Type/)
    expect(rendered).to match(/Terminator/)
  end
end
