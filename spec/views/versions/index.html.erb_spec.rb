require 'spec_helper'

describe "versions/index" do
  before(:each) do
    assign(:versions, [
      stub_model(Version,
        :item_type => "Item Type",
        :terminator => "Terminator"
      ),
      stub_model(Version,
        :item_type => "Item Type",
        :terminator => "Terminator"
      )
    ])
  end

  it "renders a list of versions" do
    render
    assert_select "tr>td", :text => "Item Type".to_s, :count => 2
    assert_select "tr>td", :text => "Terminator".to_s, :count => 2
  end
end
