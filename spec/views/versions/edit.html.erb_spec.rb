require 'spec_helper'

describe "versions/edit" do
  before(:each) do
    @version = assign(:version, stub_model(Version,
      :item_type => "MyString",
      :terminator => "MyString"
    ))
  end

  it "renders the edit version form" do
    render

    assert_select "form[action=?][method=?]", version_path(@version), "post" do
      assert_select "input#version_item_type[name=?]", "version[item_type]"
      assert_select "input#version_terminator[name=?]", "version[terminator]"
    end
  end
end
