require 'spec_helper'

describe "versions/new" do
  before(:each) do
    assign(:version, stub_model(Version,
      :item_type => "MyString",
      :terminator => "MyString"
    ).as_new_record)
  end

  it "renders new version form" do
    render

    assert_select "form[action=?][method=?]", versions_path, "post" do
      assert_select "input#version_item_type[name=?]", "version[item_type]"
      assert_select "input#version_terminator[name=?]", "version[terminator]"
    end
  end
end
