require 'spec_helper'

describe "notes/edit" do
  before(:each) do
    @note = assign(:note, stub_model(Note,
      :type => "",
      :description => "MyText",
      :dispatch => nil
    ))
  end

  it "renders the edit note form" do
    render

    assert_select "form[action=?][method=?]", note_path(@note), "post" do
      assert_select "input#note_type[name=?]", "note[type]"
      assert_select "textarea#note_description[name=?]", "note[description]"
      assert_select "input#note_dispatch[name=?]", "note[dispatch]"
    end
  end
end
