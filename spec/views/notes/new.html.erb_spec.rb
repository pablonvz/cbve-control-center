require 'spec_helper'

describe "notes/new" do
  before(:each) do
    assign(:note, stub_model(Note,
      :type => "",
      :description => "MyText",
      :dispatch => nil
    ).as_new_record)
  end

  it "renders new note form" do
    render

    assert_select "form[action=?][method=?]", notes_path, "post" do
      assert_select "input#note_type[name=?]", "note[type]"
      assert_select "textarea#note_description[name=?]", "note[description]"
      assert_select "input#note_dispatch[name=?]", "note[dispatch]"
    end
  end
end
