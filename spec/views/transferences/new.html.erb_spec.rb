require 'spec_helper'

describe "transferences/new" do
  before(:each) do
    assign(:transference, stub_model(Transference,
      :reason => "MyString",
      :place => nil
    ).as_new_record)
  end

  it "renders new transference form" do
    render

    assert_select "form[action=?][method=?]", transferences_path, "post" do
      assert_select "input#transference_reason[name=?]", "transference[reason]"
      assert_select "input#transference_place[name=?]", "transference[place]"
    end
  end
end
