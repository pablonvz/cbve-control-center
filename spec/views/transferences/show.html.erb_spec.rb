require 'spec_helper'

describe "transferences/show" do
  before(:each) do
    @transference = assign(:transference, stub_model(Transference,
      :reason => "Reason",
      :place => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Reason/)
    expect(rendered).to match(//)
  end
end
