require 'spec_helper'

describe "transferences/index" do
  before(:each) do
    assign(:transferences, [
      stub_model(Transference,
        :reason => "Reason",
        :place => nil
      ),
      stub_model(Transference,
        :reason => "Reason",
        :place => nil
      )
    ])
  end

  it "renders a list of transferences" do
    render
    assert_select "tr>td", :text => "Reason".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
