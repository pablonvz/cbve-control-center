require 'spec_helper'

describe "transferences/edit" do
  before(:each) do
    @transference = assign(:transference, stub_model(Transference,
      :reason => "MyString",
      :place => nil
    ))
  end

  it "renders the edit transference form" do
    render

    assert_select "form[action=?][method=?]", transference_path(@transference), "post" do
      assert_select "input#transference_reason[name=?]", "transference[reason]"
      assert_select "input#transference_place[name=?]", "transference[place]"
    end
  end
end
