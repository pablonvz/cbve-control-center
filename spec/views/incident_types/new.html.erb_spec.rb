require 'spec_helper'

describe "incident_types/new" do
  before(:each) do
    assign(:incident_type, stub_model(IncidentType,
      :name => "MyString",
      :icon => "MyString"
    ).as_new_record)
  end

  it "renders new incident_type form" do
    render

    assert_select "form[action=?][method=?]", incident_types_path, "post" do
      assert_select "input#incident_type_name[name=?]", "incident_type[name]"
      assert_select "input#incident_type_icon[name=?]", "incident_type[icon]"
    end
  end
end
