require 'spec_helper'

describe "incident_types/show" do
  before(:each) do
    @incident_type = assign(:incident_type, stub_model(IncidentType,
      :name => "Name",
      :icon => "Icon"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Icon/)
  end
end
