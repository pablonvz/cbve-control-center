require 'spec_helper'

describe "incident_types/edit" do
  before(:each) do
    @incident_type = assign(:incident_type, stub_model(IncidentType,
      :name => "MyString",
      :icon => "MyString"
    ))
  end

  it "renders the edit incident_type form" do
    render

    assert_select "form[action=?][method=?]", incident_type_path(@incident_type), "post" do
      assert_select "input#incident_type_name[name=?]", "incident_type[name]"
      assert_select "input#incident_type_icon[name=?]", "incident_type[icon]"
    end
  end
end
