require 'spec_helper'

describe "incident_types/index" do
  before(:each) do
    assign(:incident_types, [
      stub_model(IncidentType,
        :name => "Name",
        :icon => "Icon"
      ),
      stub_model(IncidentType,
        :name => "Name",
        :icon => "Icon"
      )
    ])
  end

  it "renders a list of incident_types" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Icon".to_s, :count => 2
  end
end
