require 'spec_helper'

describe "guard_groups/edit" do
  before(:each) do
    @guard_group = assign(:guard_group, stub_model(GuardGroup,
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit guard_group form" do
    render

    assert_select "form[action=?][method=?]", guard_group_path(@guard_group), "post" do
      assert_select "input#guard_group_name[name=?]", "guard_group[name]"
      assert_select "textarea#guard_group_description[name=?]", "guard_group[description]"
    end
  end
end
