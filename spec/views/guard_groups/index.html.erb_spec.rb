require 'spec_helper'

describe "guard_groups/index" do
  before(:each) do
    assign(:guard_groups, [
      stub_model(GuardGroup,
        :name => "Name",
        :description => "MyText"
      ),
      stub_model(GuardGroup,
        :name => "Name",
        :description => "MyText"
      )
    ])
  end

  it "renders a list of guard_groups" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
