require 'spec_helper'

describe "guard_groups/show" do
  before(:each) do
    @guard_group = assign(:guard_group, stub_model(GuardGroup,
      :name => "Name",
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
  end
end
