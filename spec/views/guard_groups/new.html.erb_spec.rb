require 'spec_helper'

describe "guard_groups/new" do
  before(:each) do
    assign(:guard_group, stub_model(GuardGroup,
      :name => "MyString",
      :description => "MyText"
    ).as_new_record)
  end

  it "renders new guard_group form" do
    render

    assert_select "form[action=?][method=?]", guard_groups_path, "post" do
      assert_select "input#guard_group_name[name=?]", "guard_group[name]"
      assert_select "textarea#guard_group_description[name=?]", "guard_group[description]"
    end
  end
end
