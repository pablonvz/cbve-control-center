module Models
  module ActsAsAny 

    def acts_as_any(options = {})
      has_paper_trail
      acts_as_paranoid

      include InstanceMethods
    end

    module InstanceMethods
    end

  end
end

ActiveRecord::Base.extend Models::ActsAsAny