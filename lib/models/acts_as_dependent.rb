class DependentStillExists < StandardError; end
module Models
  module ActsAsDependent 

    def acts_as_dependent(relation=nil)
      unless (!relation.nil?) || block_given? 
        raise ArgumentError.new("'relation' argument or a block must be given") 
      end
      
      before_destroy do |model|
        if block_given?
          raise DependentStillExists.new if yield(model)
        end
        unless relation.nil?
          if self.send(relation).respond_to?(:count)
            raise DependentStillExists.new(relation) unless self.send(relation).count.zero?
          else
            raise DependentStillExists.new(relation) unless self.send(relation).nil?
          end
        end
        return true
      end

      include InstanceMethods
    end

    module InstanceMethods
    end

  end
end

ActiveRecord::Base.extend Models::ActsAsDependent