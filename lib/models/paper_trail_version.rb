PaperTrail::Version.class_eval do 
  def self.not_repeated
    where("id IN (select MAX(id) FROM #{PaperTrail::Version.table_name} group by item_id, item_type)").order(created_at: :desc)
  end

  def item_even_deleted
    item.nil? ? item_type.constantize.with_deleted.find(item_id) : item
  end
end