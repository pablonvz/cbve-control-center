module Models
  module ActsAsCoded 

    def acts_as_coded(options = {})
      options = {upcase: true, validate_presence: true, validate_uniqueness: { case_sensitive: false }, validate_length: { minimum: 2 }, attribute: :code, order: {code: :asc}}.merge(options)

      if options[:upcase]
        before_save do
          self.send(options[:attribute]).upcase!
        end
      end

      if options[:validate_presence]
        validates options[:attribute], presence: true
      end

      if options[:validate_uniqueness]
        validates options[:attribute], uniqueness: options[:validate_uniqueness]
      end

      if options[:validate_length]
        validates options[:attribute], length: options[:validate_length]
      end

      default_scope  { order(options[:order]) }

      include InstanceMethods
    end

    module InstanceMethods
    end

  end
end

ActiveRecord::Base.extend Models::ActsAsCoded