module Models
  module ActsAsTransferable

    def acts_as_transferable(options = {})
      has_many :transference_histories, as: :transferable
      belongs_to :current_address, class_name: Address
      
      scope :availables, ->{ where(available: true) }

      scope :not_dispatched, -> { where(dispatched: false) }
      
      def self.availables_for division_id
        self.availables.where(division_id: division_id)
      end

      include InstanceMethods
    end

    module InstanceMethods
      def available_for? pdivision
        # TODO: decide if put here logic related to FiremanClassification
        self.division == pdivision && self.available
      end

    end

  end
end

ActiveRecord::Base.extend Models::ActsAsTransferable